#!/bin/sh
trap 'echo "Control-C trap caught"; exit 1' 2 #traps Ctrl-C (signal 2)

USER=$1
HOST=dev.colonyapp.com

activator -Dconfig.file=conf/prod.conf clean compile dist
echo "Complete. Output is in target/universal"

# Start scp unlpading the file
set -x
scp target/universal/admin-1.0-SNAPSHOT.zip $USER@$HOST:/tmp/admin-1.0-SNAPSHOT.zip
set +x
# remote operation
ssh -tt $USER@$HOST "bash -s"<<'ENDSSH' 
pid=`sudo netstat -tlnp | awk '/:9000 */ {split($NF,a,"/"); print a[1]}'`
echo pid=$pid
sudo kill -9 $pid
echo pid $pid killed
sudo mv /tmp/admin-1.0-SNAPSHOT.zip /usr/local/
sudo rm -rf /usr/local/goodi-admin/admin-1.0-SNAPSHOT
sudo unzip /usr/local/admin-1.0-SNAPSHOT.zip -d /usr/local/goodi-admin
sudo /usr/local/goodi-admin/admin-1.0-SNAPSHOT/bin/admin -Dconfig.file=/usr/local/goodi-admin/admin-1.0-SNAPSHOT/conf/prod.conf -mem 256 >/dev/null 2>&1 &
sleep 5
sudo tail -f /usr/local/goodi-admin/admin-1.0-SNAPSHOT/logs/application.log
ENDSSH
