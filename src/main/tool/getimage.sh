#!/bin/bash
count=1
mkdir "/tmp/${2}_$(date +%y%m%d)/"
while read line
do
    name=$line
    url=$name
    wget "$url" -O "/tmp/${2}_$(date +%y%m%d)/$count.png"
    count=`expr $count + 1`
done < $1
