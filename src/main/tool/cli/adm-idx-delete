#!/usr/bin/env ruby

require_relative 'rest_cli'

rcli = RestCLI.new do |opts|
  opts.method = 'DELETE'
  opts.accept_params = ["index", "id"]
  opts.headers = {}
  opts.shortcut_map = {"-i"=>"id", "-in"=>"index"}
  opts.path = '/adm/indexes/:index/:id'
  opts.helpdoc = <<EOX
USAGE: adm-idx-delete [DATA]
    Delete an entry from specified index

DATA:
    -i,id        Required, string, id of the index to be deleted
    -in,index    Required, string, index name

Common options:
    -?        Show this helpdoc
    -C        Pretend cli using a specific client. This sets 'X-GD-CLIENT' header
    -F        Federate and perform the cli on behalf of specified user ID
    -G        Pretend cli at a specific location. This sets 'X-GD-GEO' header
    -H        Specify the endpoint and port. By default, localhost:8080
    -L        Pretend cli using a specific locale. This sets 'X-GD-LOCALE' header
    -T        Pretend cli on behalf of a user by the session token. This sets 'Authorization' header
EOX
end

rcli.run(ARGV)
