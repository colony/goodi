#!/usr/bin/env ruby

require_relative 'rest_cli'

rcli = RestCLI.new do |opts|
  opts.method = 'GET'
  opts.accept_params = ["_a", "_b", "_e", "_k", "_n", "_o"]
  opts.headers = {"Accept"=>"application/json"}
  opts.shortcut_map = {}
  opts.path = '/activities'
  opts.helpdoc = <<EOX
USAGE: activities-get [DATA]
    Retrieve caller's activities

DATA:
    _a    Only include items that come after the item
    _b    Only include items that come before the item
    _e    Excludes[0|1] the item specified by "_a" or "_b"
    _k    Name of the sort key
    _n    Do not include more than the specified number of items
    _o    "1" to sort in ascending order, "-1" for descending order

Common options:
    -?        Show this helpdoc
    -C        Pretend cli using a specific client. This sets 'X-GD-CLIENT' header
    -F        Federate and perform the cli on behalf of specified user ID
    -G        Pretend cli at a specific location. This sets 'X-GD-GEO' header
    -H        Specify the endpoint and port. By default, localhost:8080
    -L        Pretend cli using a specific locale. This sets 'X-GD-LOCALE' header
    -T        Pretend cli on behalf of a user by the session token. This sets 'Authorization' header
EOX
end

rcli.run(ARGV)
