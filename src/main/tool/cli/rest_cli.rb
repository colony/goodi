#!/usr/bin/env ruby
require 'rest_client'
require 'pp'
require 'json'

# environment variable
E = ENV['GOODI_CLI'] || 'LOCAL'
case E
when 'DEV'
  HOST='http://dev.colonyapp.com:8080'
  PREFIX='/gd/v1/'
when 'LOCAL'
  HOST='localhost:8080'
  PREFIX='/gd/v1/'
when 'JIAYU-UB'
  HOST='192.168.1.120:8080'
  PREFIX='/gd/v1/'
when 'PROD'
  HOST='http://server.stealthie.io:8080'
  PREFIX='/gd/v1/'
end
TOKEN='/usr/local/goodi/resources/token'

class RestCLI
  # a set of common headers will be included in all request
  @@global_headers = {'X-GD-CLIENT'=>'CLI:4','X-GD-LOCALE'=>'en_US','X-GD-GEO'=>'1|1.0|1.0'}

  attr_accessor :accept_all
  attr_accessor :accept_params
  attr_accessor :headers
  attr_accessor :helpdoc
  attr_accessor :method
  attr_accessor :path
  attr_accessor :shortcut_map

  def initialize(&b)
  	@accept_params = []
  	@headers = {}
  	@shortcut_map = {}

  	if block_given?
      yield self
  	end
  end

  def has_path_placeholder?
  	# url, like "/aa/:id/bb", has placeholder "id"
  	!@path.index('/:').nil?
  end

  def method=(m)
  	@method = m.downcase
  end

  def parse_args(args)
  	# arguments on cmd are either naked or paired (i.e. -a b or a=b)
  	i,l = 0,args.length
  	naked,paired = [],{}
  	while i < l
      arg = args[i]

      if arg.start_with?('-')
      	# incr position to pair the arg value
      	i = i + 1
        raise "cannot find argument value for \"#{arg}\"" if i>=l

        # check if it is global options (HOST, TOKEN, etc)
        if ['-C','-L','-T','-H','-F'].include?(arg)
          paired[arg] = args[i]
        # check if it is an accepted shortcut
        elsif accept_all || @accept_params.include?(@shortcut_map[arg])
          v = args[i]
          paired[@shortcut_map[arg]] = v.start_with?('@') ? File.new(v[1..-1],'rb') : v
        end
      elsif arg =~ /.+=.+/
      	k,v = arg.match(/(.*)=(.*)/).captures
        if accept_all || @accept_params.include?(k)
          paired[k] = v.start_with?('@') ? File.new(v[1..-1],'rb') : v
        end
      else
        naked << arg
      end 
      i = i + 1
  	end

  	return naked,paired
  end

  def payload_for_post(arg)
  	if arg.nil?
      nil
    elsif arg.start_with?('@')
      File.read(arg[1..-1])
    else
      # raw string
      arg
    end
  end

  def resolve_url(host, kv=nil)
    url = "#{host}#{PREFIX}#{@path.gsub(/^\//,'')}"
    return url unless has_path_placeholder?

    # apprently we have placeholder, but there is no arg to match it
    raise "cannot resolve url \"#{url}\"" if kv.nil?

  	url.gsub!(/\/:[^\/]+/) do |m|
      # m = /:placeholder
      k = m[2..-1]
      v = kv.delete(k)
      raise "cannot resolve url \"#{@path}\" for placeholder \"#{k}\"" if v.nil?

      # return the replace
      "/#{v}"
  	end

  	url
  end

  def run(args)
  	# print helpdoc
    if args.include?('-?')
      puts @helpdoc
      exit
    end

    # prepare the arguments, also override the global variable if necessary
    naked,paired = parse_args(args)

    host = HOST
    hdrs = @@global_headers.merge(@headers)
    if v = paired.delete('-C')
      hdrs['X-GD-CLIENT'] = v
    end
    if v = paired.delete('-L')
      hdrs['X-GD-LOCALE'] = v
    end
    if v = paired.delete('-F')
      # federate a user ID, find the federation token from a specific file
      # also, if -T is specified, this will be overriden
      if File.exists?(TOKEN)
        federate_token = File.readlines(TOKEN).first.chomp
        hdrs['Authorization'] = "Bearer #{federate_token}#{v}"
      end
    end
    if v = paired.delete('-G')
      hdrs['X-GD-GEO'] = v
    end    
    if v = paired.delete('-T')
      hdrs['Authorization'] = "Bearer #{v}"
    end
    if v = paired.delete('-H')
      host = v
    end

    # resolve url placeholder, add host, etc
    url = resolve_url(host, paired)

    # reusable response handler
    res_handler = Proc.new do |response, request, result|
      puts "#{response.code} #{response.size}bytes #{E}"
      s = response.to_s

      # if the response is indeed JSON, beautify it on console
      begin
        j = JSON.parse(s)
        puts JSON.pretty_generate(j)
      rescue
        puts s
      end
    end

    # if there's at least one key=val argument provided, it is kv arg_mode
    # also, note if all the kv has been used in resolve_url, it will still
    # be nake mode
    arg_mode = paired.empty? ? :naked : :kv

    if arg_mode == :naked
      # for naked argument, only take the FIRST one if the argument in format of '@blahblah', 
      # read the content and feed into the underlying restclient
      # N.B. GET/DELETE will ignore the argument
      case @method
      when 'get'
        RestClient.get(url, hdrs, &res_handler)
      when 'delete'
        RestClient.delete(url, hdrs, &res_handler)
      when 'post'
        RestClient.post(url, payload_for_post(naked.first), hdrs, &res_handler)
      when 'put'
        RestClient.put(url, payload_for_post(naked.first), hdrs, &res_handler)
      else
  		raise "unrecognized method \"#{@method}\""
      end
    else
      # for keyval argument, the provided argument must in format KEY=VAL, naked one is skipped,
      # the KEY must be in accept_params, otherwise skip. For post and put, value in format of
      # "KEY=@VAL" is assumed to be a file path, and converted to File.
      case @method
      when 'get'
        RestClient.get(url, {:params => paired}.merge(hdrs), &res_handler)
      when 'delete'
        RestClient.delete(url, {:params => paired}.merge(hdrs, &res_handler))
      when 'post'
      	if @headers.has_value?('multipart/form-data')
          paired[:multipart] = true
      	end
   	    RestClient.post(url, paired, hdrs, &res_handler)
      when 'put'
      	if @headers.has_value?('multipart/form-data')
          paired[:multipart] = true
      	end
        RestClient.put(url, paired, hdrs, &res_handler)
      end
    end
  end
end
