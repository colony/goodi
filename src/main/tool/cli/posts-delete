#!/usr/bin/env ruby

require_relative 'rest_cli'

rcli = RestCLI.new do |opts|
  opts.method = 'DELETE'
  opts.accept_params = ["id"]
  opts.headers = {"Accept"=>"application/json"}
  opts.shortcut_map = {"-i"=>"id"}
  opts.path = '/posts/:id'
  opts.helpdoc = <<EOX
USAGE: posts-delete [DATA]
    Delete a post.

DATA:
    -i,id    Required, number, ID of post to be deleted

Common options:
    -?        Show this helpdoc
    -C        Pretend cli using a specific client. This sets 'X-GD-CLIENT' header
    -F        Federate and perform the cli on behalf of specified user ID
    -G        Pretend cli at a specific location. This sets 'X-GD-GEO' header
    -H        Specify the endpoint and port. By default, localhost:8080
    -L        Pretend cli using a specific locale. This sets 'X-GD-LOCALE' header
    -T        Pretend cli on behalf of a user by the session token. This sets 'Authorization' header
EOX
end

rcli.run(ARGV)
