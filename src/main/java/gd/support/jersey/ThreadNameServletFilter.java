
package gd.support.jersey;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class ThreadNameServletFilter
    implements
        Filter
{
    @Override
    public void init(
        FilterConfig filterConfig)
        throws ServletException
    { /* unused */
    }


    @Override
    public void destroy()
    { /* unused */
    }


    @Override
    public void doFilter(
        ServletRequest request,
        ServletResponse response,
        FilterChain chain)
        throws IOException,
            ServletException
    {
        final HttpServletRequest req = (HttpServletRequest) request;
        final Thread current = Thread.currentThread();
        final String oldName = current.getName();
        try
        {
            current.setName(String.format(
                "%s-%s %s",
                oldName,
                req.getMethod(),
                getFullUrl(req)));
            chain.doFilter(request, response);
        }
        finally
        {
            current.setName(oldName);
        }
    }


    private static String getFullUrl(
        HttpServletRequest request)
    {
        if (request.getQueryString() == null) {
            return request.getRequestURI();
        }
        return request.getRequestURI() + "?" + request.getQueryString();
    }
}
