package gd.support.jersey;

public class DoubleParam extends AbstractParam<Double> {
    public DoubleParam(String input) {
        super(input);
    }
    
    
    @Override
    protected String errorMessage(String input, Exception e) {
        return '"' + input + "\" is not a double.";
    }
    
    
    @Override
    protected Double parse(String input) {
        return Double.valueOf(input);
    }
}
