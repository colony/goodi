
package gd.support.jersey;



public class BoolParam
    extends
        AbstractParam<Boolean>
{
    public BoolParam(
        String input)
    {
        super(input);
    }


    @Override
    protected String errorMessage(
        String input,
        Exception e)
    {
        return '"' + input + "\" must be \"true\" or \"false\".";
    }


    @Override
    protected Boolean parse(
        String input)

        throws Exception
    {
        for (int i=0; i<4; ++i)
        {
            if (TRUE_PARAMS[i].equalsIgnoreCase(input))
            {
                return Boolean.TRUE;
            }
            
            if (FALSE_PARAMS[i].equalsIgnoreCase(input)){
                return Boolean.FALSE;
            }
        }
        
        throw new IllegalArgumentException("Fail to parse the bool param with input \"" + input + "\"");
    }

    
    private static final String[] FALSE_PARAMS = new String[]{"0","n","f","false"};
    private static final String[] TRUE_PARAMS = new String[]{"1", "y", "t", "true"};
}
