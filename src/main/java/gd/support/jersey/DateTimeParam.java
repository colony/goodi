
package gd.support.jersey;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class DateTimeParam
    extends
        AbstractParam<DateTime>
{
    private static final DateTimeFormatter FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");


    public DateTimeParam(
        String input)
    {
        super(input);
    }


    @Override
    protected String errorMessage(
        String input,
        Exception e)
    {
        return '"' + input + "\" is not an ISO date.";
    }


    @Override
    protected DateTime parse(
        String input)
        throws Exception
    {
        return FORMAT.parseDateTime(input);
    }
}
