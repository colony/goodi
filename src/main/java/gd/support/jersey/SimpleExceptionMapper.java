
package gd.support.jersey;


import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Provider
public class SimpleExceptionMapper
    implements
        ExceptionMapper<Throwable>
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SimpleExceptionMapper.class);


    @Override
    public Response toResponse(
        Throwable ex)
    {
        if (ex instanceof WebApplicationException)
        {
            // N.B. Seems like Jersey doesn't allow us to intercept WebApplicationException
            //      The code here never been reached ??
            
            WebApplicationException waex = (WebApplicationException) ex;
            LOG.error("Caught web exception: {}", waex.getMessage(), waex);
            return waex.getResponse();
        }
        else if (ex instanceof AppException)
        {
            AppException aex = (AppException) ex;
            LOG.error("Caugh app exception: {}", aex.getMessage());
            return aex.getResponse();
        }
        else
        {
            LOG.error("Uncaught exception: {}", ex.getMessage(), ex);
            return Response.serverError().entity(ex.getMessage()).build();
        }
    }
}
