
package gd.support.jersey;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;


@Provider
public class JacksonObjectMapperProvider
    implements
        ContextResolver<ObjectMapper>
{
    private final ObjectMapper configuredMapper;


    public JacksonObjectMapperProvider()
    {
        DateFormat df =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        configuredMapper =
            new ObjectMapper()
                .registerModule(new AfterburnerModule())
                .registerModule(new FuzzyEnumModule())
                .registerModule(this.customModule1())
                .setSerializationInclusion(Include.NON_NULL)
                .setPropertyNamingStrategy(
                    new SnakeCaseNamingStrategy())
                .setDateFormat(df)
                .configure(
                    SerializationFeature.WRAP_ROOT_VALUE,
                    false)
                .configure(
                    SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
                    false)
                .configure(
                    DeserializationFeature.UNWRAP_ROOT_VALUE,
                    false)
                .setAnnotationIntrospector(
                    new JacksonAnnotationIntrospector());
//                .setAnnotationIntrospector(
//                    new AnnotationIntrospectorPair(
//                        new JacksonAnnotationIntrospector(),    // Primary
//                        new JaxbAnnotationIntrospector(         // Secondary
//                            TypeFactory.defaultInstance())));
    }


    @Override
    public ObjectMapper getContext(
        Class<?> type)
    {
        return configuredMapper;
    }


    private Module customModule1()
    {
        SimpleModule module = new SimpleModule("org.json");
        module.addSerializer(
            JSONObject.class,
            new JsonSerializer<JSONObject>() {
                @Override
                public void serialize(
                    JSONObject value,
                    JsonGenerator jgen,
                    SerializerProvider provider)
                    throws IOException
                {
                    jgen.writeRawValue(value.toString());
                }
            });
        
        // FOR NOW, i guess i don't need deserializer for JSONObject
//        module.addDeserializer(JSONObject.class, new JsonDeserializer<JSONObject>() {
//            @Override
//            public JSONObject deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
//                Map<String, Object> bean = jp.readValueAs(new TypeReference<Map<String, Object>>() {});
//                return new JSONObject(bean);
//            }
//        });
        return module;
    }
}
