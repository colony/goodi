
package gd.support.jersey;


import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.google.common.base.CharMatcher;


/**
 * A module for deserializing enums that is more permissive than the
 * default.
 * <p/>
 * This deserializer is more permissive in the following ways:
 * <ul>
 * <li>Whitespace is permitted but stripped from the input.</li>
 * <li>Matching against the enum values is case insensitive.</li>
 * </ul>
 */
public class FuzzyEnumModule
    extends
        Module
{
    private static class PermissiveEnumDeserializer
        extends
            StdScalarDeserializer<Enum<?>>
    {
        private static final long serialVersionUID =
            1027704188465685512L;
        
        private final Enum<?>[] constants;
        private final Class<Enum<?>> enumType;

        @SuppressWarnings("unchecked")
        protected PermissiveEnumDeserializer(
            Class<Enum<?>> clazz)
        {
            super(clazz);
            this.enumType = ((Class<Enum<?>>) handledType());
            this.constants = this.enumType.getEnumConstants();
        }


        @Override
        public Enum<?> deserialize(
            JsonParser jp,
            DeserializationContext ctxt)
            throws IOException
        {
            String text =
                CharMatcher.WHITESPACE.trimFrom(jp.getText());
            for (Enum<?> constant : constants)
            {
                if (constant.name().equalsIgnoreCase(text))
                {
                    return constant;
                }
            }

            throw ctxt.mappingException(text + " was not one of enum "
                + this.enumType.getName());
        }
    }



    private static class PermissiveEnumDeserializers
        extends
            Deserializers.Base
    {
        @Override
        @SuppressWarnings("unchecked")
        public JsonDeserializer<?> findEnumDeserializer(
            Class<?> type,
            DeserializationConfig config,
            BeanDescription desc)
            throws JsonMappingException
        {
            return new PermissiveEnumDeserializer(
                (Class<Enum<?>>) type);
        }
    }


    @Override
    public String getModuleName()
    {
        return "fuzzy-enum";
    }


    @Override
    public void setupModule(
        SetupContext context)
    {
        context.addDeserializers(new PermissiveEnumDeserializers());
    }


    @Override
    public Version version()
    {
        return Version.unknownVersion();
    }

}
