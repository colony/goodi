
package gd.support.jersey;


import javax.ws.rs.core.Response;


/**
 * A simple mimic of WebApplicationException, the reason we create
 * this class is because Jersey's exceptionMapper by default will
 * omit thrown WebApplicationException that has not-null entity.
 * This doesn't help our exception logging.
 */
public class AppException
    extends
        RuntimeException
{
    private static final long serialVersionUID = 6787073232812167198L;

    private final Response response;


    public AppException()
    {
        this(null, null, null);
    }


    public AppException(
        final Response.Status status,
        final String message)
    {
        this(status, message, null);
    }


    public AppException(
        final int statusCode,
        final String message)
    {
        this(statusCode, message, null);
    }


    /**
     * @param message
     *            The detail message (which is saved for later
     *            retrieval by the {@link #getMessage()} method).
     * @param cause
     *            The underlying cause of the exception.
     * @param status
     *            the response status, If null, it will build an
     *            internal server error response instead
     */
    public AppException(
        final Response.Status status,
        final String message,
        final Throwable cause)
    {
        super(message, cause);
        if (status == null) {
            this.response = Response.serverError().build();
        }
        else {
            this.response = Response.status(status).entity(message).build();
        }
    }


    public AppException(
        final int statusCode,
        final String message,
        final Throwable cause)
    {
        super(message, cause);
        if (statusCode <= 0) {
            this.response = Response.serverError().build();
        }
        else {
            this.response = Response.status(statusCode).entity(message).build();
        }
    }


    @Override
    public String getMessage()
    {
        String msg = super.getMessage();
        return "HTTP " + this.response.getStatus() + ' ' + msg;
    }


    /**
     * Get the HTTP response.
     * 
     * @return the HTTP response.
     */
    public Response getResponse()
    {
        return response;
    }
}
