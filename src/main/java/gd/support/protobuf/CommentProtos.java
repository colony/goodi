// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: src/main/resources/proto/comment.proto

package gd.support.protobuf;

public final class CommentProtos {
  private CommentProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }
  public interface CommentProtoOrBuilder
      extends com.google.protobuf.MessageLiteOrBuilder {

    // required int64 id = 1;
    /**
     * <code>required int64 id = 1;</code>
     *
     * <pre>
     * Required fields
     * </pre>
     */
    boolean hasId();
    /**
     * <code>required int64 id = 1;</code>
     *
     * <pre>
     * Required fields
     * </pre>
     */
    long getId();

    // required int64 owner_id = 2;
    /**
     * <code>required int64 owner_id = 2;</code>
     */
    boolean hasOwnerId();
    /**
     * <code>required int64 owner_id = 2;</code>
     */
    long getOwnerId();

    // required int64 post_id = 3;
    /**
     * <code>required int64 post_id = 3;</code>
     */
    boolean hasPostId();
    /**
     * <code>required int64 post_id = 3;</code>
     */
    long getPostId();

    // required int64 create_time = 4;
    /**
     * <code>required int64 create_time = 4;</code>
     */
    boolean hasCreateTime();
    /**
     * <code>required int64 create_time = 4;</code>
     */
    long getCreateTime();

    // optional string body = 5;
    /**
     * <code>optional string body = 5;</code>
     *
     * <pre>
     * Optional fields
     * </pre>
     */
    boolean hasBody();
    /**
     * <code>optional string body = 5;</code>
     *
     * <pre>
     * Optional fields
     * </pre>
     */
    java.lang.String getBody();
    /**
     * <code>optional string body = 5;</code>
     *
     * <pre>
     * Optional fields
     * </pre>
     */
    com.google.protobuf.ByteString
        getBodyBytes();

    // optional string geo = 6;
    /**
     * <code>optional string geo = 6;</code>
     */
    boolean hasGeo();
    /**
     * <code>optional string geo = 6;</code>
     */
    java.lang.String getGeo();
    /**
     * <code>optional string geo = 6;</code>
     */
    com.google.protobuf.ByteString
        getGeoBytes();

    // optional string owner_icon = 7;
    /**
     * <code>optional string owner_icon = 7;</code>
     */
    boolean hasOwnerIcon();
    /**
     * <code>optional string owner_icon = 7;</code>
     */
    java.lang.String getOwnerIcon();
    /**
     * <code>optional string owner_icon = 7;</code>
     */
    com.google.protobuf.ByteString
        getOwnerIconBytes();

    // optional int64 parent_id = 8;
    /**
     * <code>optional int64 parent_id = 8;</code>
     */
    boolean hasParentId();
    /**
     * <code>optional int64 parent_id = 8;</code>
     */
    long getParentId();

    // optional string hashtags = 9;
    /**
     * <code>optional string hashtags = 9;</code>
     */
    boolean hasHashtags();
    /**
     * <code>optional string hashtags = 9;</code>
     */
    java.lang.String getHashtags();
    /**
     * <code>optional string hashtags = 9;</code>
     */
    com.google.protobuf.ByteString
        getHashtagsBytes();
  }
  /**
   * Protobuf type {@code com.goodi.post.CommentProto}
   */
  public static final class CommentProto extends
      com.google.protobuf.GeneratedMessageLite
      implements CommentProtoOrBuilder {
    // Use CommentProto.newBuilder() to construct.
@SuppressWarnings({"rawtypes"})
    private CommentProto(com.google.protobuf.GeneratedMessageLite.Builder builder) {
      super(builder);

    }
    private CommentProto(boolean noInit) {}

    private static final CommentProto defaultInstance;
    public static CommentProto getDefaultInstance() {
      return defaultInstance;
    }

    public CommentProto getDefaultInstanceForType() {
      return defaultInstance;
    }

    private CommentProto(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      initFields();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!parseUnknownField(input,
                                     extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
            case 8: {
              bitField0_ |= 0x00000001;
              id_ = input.readInt64();
              break;
            }
            case 16: {
              bitField0_ |= 0x00000002;
              ownerId_ = input.readInt64();
              break;
            }
            case 24: {
              bitField0_ |= 0x00000004;
              postId_ = input.readInt64();
              break;
            }
            case 32: {
              bitField0_ |= 0x00000008;
              createTime_ = input.readInt64();
              break;
            }
            case 42: {
              bitField0_ |= 0x00000010;
              body_ = input.readBytes();
              break;
            }
            case 50: {
              bitField0_ |= 0x00000020;
              geo_ = input.readBytes();
              break;
            }
            case 58: {
              bitField0_ |= 0x00000040;
              ownerIcon_ = input.readBytes();
              break;
            }
            case 64: {
              bitField0_ |= 0x00000080;
              parentId_ = input.readInt64();
              break;
            }
            case 74: {
              bitField0_ |= 0x00000100;
              hashtags_ = input.readBytes();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e.getMessage()).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static com.google.protobuf.Parser<CommentProto> PARSER =
        new com.google.protobuf.AbstractParser<CommentProto>() {
      public CommentProto parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new CommentProto(input, extensionRegistry);
      }
    };

    @java.lang.Override
    public com.google.protobuf.Parser<CommentProto> getParserForType() {
      return PARSER;
    }

    private int bitField0_;
    // required int64 id = 1;
    public static final int ID_FIELD_NUMBER = 1;
    private long id_;
    /**
     * <code>required int64 id = 1;</code>
     *
     * <pre>
     * Required fields
     * </pre>
     */
    public boolean hasId() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    /**
     * <code>required int64 id = 1;</code>
     *
     * <pre>
     * Required fields
     * </pre>
     */
    public long getId() {
      return id_;
    }

    // required int64 owner_id = 2;
    public static final int OWNER_ID_FIELD_NUMBER = 2;
    private long ownerId_;
    /**
     * <code>required int64 owner_id = 2;</code>
     */
    public boolean hasOwnerId() {
      return ((bitField0_ & 0x00000002) == 0x00000002);
    }
    /**
     * <code>required int64 owner_id = 2;</code>
     */
    public long getOwnerId() {
      return ownerId_;
    }

    // required int64 post_id = 3;
    public static final int POST_ID_FIELD_NUMBER = 3;
    private long postId_;
    /**
     * <code>required int64 post_id = 3;</code>
     */
    public boolean hasPostId() {
      return ((bitField0_ & 0x00000004) == 0x00000004);
    }
    /**
     * <code>required int64 post_id = 3;</code>
     */
    public long getPostId() {
      return postId_;
    }

    // required int64 create_time = 4;
    public static final int CREATE_TIME_FIELD_NUMBER = 4;
    private long createTime_;
    /**
     * <code>required int64 create_time = 4;</code>
     */
    public boolean hasCreateTime() {
      return ((bitField0_ & 0x00000008) == 0x00000008);
    }
    /**
     * <code>required int64 create_time = 4;</code>
     */
    public long getCreateTime() {
      return createTime_;
    }

    // optional string body = 5;
    public static final int BODY_FIELD_NUMBER = 5;
    private java.lang.Object body_;
    /**
     * <code>optional string body = 5;</code>
     *
     * <pre>
     * Optional fields
     * </pre>
     */
    public boolean hasBody() {
      return ((bitField0_ & 0x00000010) == 0x00000010);
    }
    /**
     * <code>optional string body = 5;</code>
     *
     * <pre>
     * Optional fields
     * </pre>
     */
    public java.lang.String getBody() {
      java.lang.Object ref = body_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          body_ = s;
        }
        return s;
      }
    }
    /**
     * <code>optional string body = 5;</code>
     *
     * <pre>
     * Optional fields
     * </pre>
     */
    public com.google.protobuf.ByteString
        getBodyBytes() {
      java.lang.Object ref = body_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        body_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    // optional string geo = 6;
    public static final int GEO_FIELD_NUMBER = 6;
    private java.lang.Object geo_;
    /**
     * <code>optional string geo = 6;</code>
     */
    public boolean hasGeo() {
      return ((bitField0_ & 0x00000020) == 0x00000020);
    }
    /**
     * <code>optional string geo = 6;</code>
     */
    public java.lang.String getGeo() {
      java.lang.Object ref = geo_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          geo_ = s;
        }
        return s;
      }
    }
    /**
     * <code>optional string geo = 6;</code>
     */
    public com.google.protobuf.ByteString
        getGeoBytes() {
      java.lang.Object ref = geo_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        geo_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    // optional string owner_icon = 7;
    public static final int OWNER_ICON_FIELD_NUMBER = 7;
    private java.lang.Object ownerIcon_;
    /**
     * <code>optional string owner_icon = 7;</code>
     */
    public boolean hasOwnerIcon() {
      return ((bitField0_ & 0x00000040) == 0x00000040);
    }
    /**
     * <code>optional string owner_icon = 7;</code>
     */
    public java.lang.String getOwnerIcon() {
      java.lang.Object ref = ownerIcon_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          ownerIcon_ = s;
        }
        return s;
      }
    }
    /**
     * <code>optional string owner_icon = 7;</code>
     */
    public com.google.protobuf.ByteString
        getOwnerIconBytes() {
      java.lang.Object ref = ownerIcon_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        ownerIcon_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    // optional int64 parent_id = 8;
    public static final int PARENT_ID_FIELD_NUMBER = 8;
    private long parentId_;
    /**
     * <code>optional int64 parent_id = 8;</code>
     */
    public boolean hasParentId() {
      return ((bitField0_ & 0x00000080) == 0x00000080);
    }
    /**
     * <code>optional int64 parent_id = 8;</code>
     */
    public long getParentId() {
      return parentId_;
    }

    // optional string hashtags = 9;
    public static final int HASHTAGS_FIELD_NUMBER = 9;
    private java.lang.Object hashtags_;
    /**
     * <code>optional string hashtags = 9;</code>
     */
    public boolean hasHashtags() {
      return ((bitField0_ & 0x00000100) == 0x00000100);
    }
    /**
     * <code>optional string hashtags = 9;</code>
     */
    public java.lang.String getHashtags() {
      java.lang.Object ref = hashtags_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          hashtags_ = s;
        }
        return s;
      }
    }
    /**
     * <code>optional string hashtags = 9;</code>
     */
    public com.google.protobuf.ByteString
        getHashtagsBytes() {
      java.lang.Object ref = hashtags_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        hashtags_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private void initFields() {
      id_ = 0L;
      ownerId_ = 0L;
      postId_ = 0L;
      createTime_ = 0L;
      body_ = "";
      geo_ = "";
      ownerIcon_ = "";
      parentId_ = 0L;
      hashtags_ = "";
    }
    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized != -1) return isInitialized == 1;

      if (!hasId()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasOwnerId()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasPostId()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasCreateTime()) {
        memoizedIsInitialized = 0;
        return false;
      }
      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        output.writeInt64(1, id_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        output.writeInt64(2, ownerId_);
      }
      if (((bitField0_ & 0x00000004) == 0x00000004)) {
        output.writeInt64(3, postId_);
      }
      if (((bitField0_ & 0x00000008) == 0x00000008)) {
        output.writeInt64(4, createTime_);
      }
      if (((bitField0_ & 0x00000010) == 0x00000010)) {
        output.writeBytes(5, getBodyBytes());
      }
      if (((bitField0_ & 0x00000020) == 0x00000020)) {
        output.writeBytes(6, getGeoBytes());
      }
      if (((bitField0_ & 0x00000040) == 0x00000040)) {
        output.writeBytes(7, getOwnerIconBytes());
      }
      if (((bitField0_ & 0x00000080) == 0x00000080)) {
        output.writeInt64(8, parentId_);
      }
      if (((bitField0_ & 0x00000100) == 0x00000100)) {
        output.writeBytes(9, getHashtagsBytes());
      }
    }

    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;

      size = 0;
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(1, id_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(2, ownerId_);
      }
      if (((bitField0_ & 0x00000004) == 0x00000004)) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(3, postId_);
      }
      if (((bitField0_ & 0x00000008) == 0x00000008)) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(4, createTime_);
      }
      if (((bitField0_ & 0x00000010) == 0x00000010)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBytesSize(5, getBodyBytes());
      }
      if (((bitField0_ & 0x00000020) == 0x00000020)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBytesSize(6, getGeoBytes());
      }
      if (((bitField0_ & 0x00000040) == 0x00000040)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBytesSize(7, getOwnerIconBytes());
      }
      if (((bitField0_ & 0x00000080) == 0x00000080)) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(8, parentId_);
      }
      if (((bitField0_ & 0x00000100) == 0x00000100)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBytesSize(9, getHashtagsBytes());
      }
      memoizedSerializedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @java.lang.Override
    protected java.lang.Object writeReplace()
        throws java.io.ObjectStreamException {
      return super.writeReplace();
    }

    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input, extensionRegistry);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static gd.support.protobuf.CommentProtos.CommentProto parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }

    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(gd.support.protobuf.CommentProtos.CommentProto prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }

    /**
     * Protobuf type {@code com.goodi.post.CommentProto}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageLite.Builder<
          gd.support.protobuf.CommentProtos.CommentProto, Builder>
        implements gd.support.protobuf.CommentProtos.CommentProtoOrBuilder {
      // Construct using gd.support.protobuf.CommentProtos.CommentProto.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private void maybeForceBuilderInitialization() {
      }
      private static Builder create() {
        return new Builder();
      }

      public Builder clear() {
        super.clear();
        id_ = 0L;
        bitField0_ = (bitField0_ & ~0x00000001);
        ownerId_ = 0L;
        bitField0_ = (bitField0_ & ~0x00000002);
        postId_ = 0L;
        bitField0_ = (bitField0_ & ~0x00000004);
        createTime_ = 0L;
        bitField0_ = (bitField0_ & ~0x00000008);
        body_ = "";
        bitField0_ = (bitField0_ & ~0x00000010);
        geo_ = "";
        bitField0_ = (bitField0_ & ~0x00000020);
        ownerIcon_ = "";
        bitField0_ = (bitField0_ & ~0x00000040);
        parentId_ = 0L;
        bitField0_ = (bitField0_ & ~0x00000080);
        hashtags_ = "";
        bitField0_ = (bitField0_ & ~0x00000100);
        return this;
      }

      public Builder clone() {
        return create().mergeFrom(buildPartial());
      }

      public gd.support.protobuf.CommentProtos.CommentProto getDefaultInstanceForType() {
        return gd.support.protobuf.CommentProtos.CommentProto.getDefaultInstance();
      }

      public gd.support.protobuf.CommentProtos.CommentProto build() {
        gd.support.protobuf.CommentProtos.CommentProto result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public gd.support.protobuf.CommentProtos.CommentProto buildPartial() {
        gd.support.protobuf.CommentProtos.CommentProto result = new gd.support.protobuf.CommentProtos.CommentProto(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
          to_bitField0_ |= 0x00000001;
        }
        result.id_ = id_;
        if (((from_bitField0_ & 0x00000002) == 0x00000002)) {
          to_bitField0_ |= 0x00000002;
        }
        result.ownerId_ = ownerId_;
        if (((from_bitField0_ & 0x00000004) == 0x00000004)) {
          to_bitField0_ |= 0x00000004;
        }
        result.postId_ = postId_;
        if (((from_bitField0_ & 0x00000008) == 0x00000008)) {
          to_bitField0_ |= 0x00000008;
        }
        result.createTime_ = createTime_;
        if (((from_bitField0_ & 0x00000010) == 0x00000010)) {
          to_bitField0_ |= 0x00000010;
        }
        result.body_ = body_;
        if (((from_bitField0_ & 0x00000020) == 0x00000020)) {
          to_bitField0_ |= 0x00000020;
        }
        result.geo_ = geo_;
        if (((from_bitField0_ & 0x00000040) == 0x00000040)) {
          to_bitField0_ |= 0x00000040;
        }
        result.ownerIcon_ = ownerIcon_;
        if (((from_bitField0_ & 0x00000080) == 0x00000080)) {
          to_bitField0_ |= 0x00000080;
        }
        result.parentId_ = parentId_;
        if (((from_bitField0_ & 0x00000100) == 0x00000100)) {
          to_bitField0_ |= 0x00000100;
        }
        result.hashtags_ = hashtags_;
        result.bitField0_ = to_bitField0_;
        return result;
      }

      public Builder mergeFrom(gd.support.protobuf.CommentProtos.CommentProto other) {
        if (other == gd.support.protobuf.CommentProtos.CommentProto.getDefaultInstance()) return this;
        if (other.hasId()) {
          setId(other.getId());
        }
        if (other.hasOwnerId()) {
          setOwnerId(other.getOwnerId());
        }
        if (other.hasPostId()) {
          setPostId(other.getPostId());
        }
        if (other.hasCreateTime()) {
          setCreateTime(other.getCreateTime());
        }
        if (other.hasBody()) {
          bitField0_ |= 0x00000010;
          body_ = other.body_;
          
        }
        if (other.hasGeo()) {
          bitField0_ |= 0x00000020;
          geo_ = other.geo_;
          
        }
        if (other.hasOwnerIcon()) {
          bitField0_ |= 0x00000040;
          ownerIcon_ = other.ownerIcon_;
          
        }
        if (other.hasParentId()) {
          setParentId(other.getParentId());
        }
        if (other.hasHashtags()) {
          bitField0_ |= 0x00000100;
          hashtags_ = other.hashtags_;
          
        }
        return this;
      }

      public final boolean isInitialized() {
        if (!hasId()) {
          
          return false;
        }
        if (!hasOwnerId()) {
          
          return false;
        }
        if (!hasPostId()) {
          
          return false;
        }
        if (!hasCreateTime()) {
          
          return false;
        }
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        gd.support.protobuf.CommentProtos.CommentProto parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (gd.support.protobuf.CommentProtos.CommentProto) e.getUnfinishedMessage();
          throw e;
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      // required int64 id = 1;
      private long id_ ;
      /**
       * <code>required int64 id = 1;</code>
       *
       * <pre>
       * Required fields
       * </pre>
       */
      public boolean hasId() {
        return ((bitField0_ & 0x00000001) == 0x00000001);
      }
      /**
       * <code>required int64 id = 1;</code>
       *
       * <pre>
       * Required fields
       * </pre>
       */
      public long getId() {
        return id_;
      }
      /**
       * <code>required int64 id = 1;</code>
       *
       * <pre>
       * Required fields
       * </pre>
       */
      public Builder setId(long value) {
        bitField0_ |= 0x00000001;
        id_ = value;
        
        return this;
      }
      /**
       * <code>required int64 id = 1;</code>
       *
       * <pre>
       * Required fields
       * </pre>
       */
      public Builder clearId() {
        bitField0_ = (bitField0_ & ~0x00000001);
        id_ = 0L;
        
        return this;
      }

      // required int64 owner_id = 2;
      private long ownerId_ ;
      /**
       * <code>required int64 owner_id = 2;</code>
       */
      public boolean hasOwnerId() {
        return ((bitField0_ & 0x00000002) == 0x00000002);
      }
      /**
       * <code>required int64 owner_id = 2;</code>
       */
      public long getOwnerId() {
        return ownerId_;
      }
      /**
       * <code>required int64 owner_id = 2;</code>
       */
      public Builder setOwnerId(long value) {
        bitField0_ |= 0x00000002;
        ownerId_ = value;
        
        return this;
      }
      /**
       * <code>required int64 owner_id = 2;</code>
       */
      public Builder clearOwnerId() {
        bitField0_ = (bitField0_ & ~0x00000002);
        ownerId_ = 0L;
        
        return this;
      }

      // required int64 post_id = 3;
      private long postId_ ;
      /**
       * <code>required int64 post_id = 3;</code>
       */
      public boolean hasPostId() {
        return ((bitField0_ & 0x00000004) == 0x00000004);
      }
      /**
       * <code>required int64 post_id = 3;</code>
       */
      public long getPostId() {
        return postId_;
      }
      /**
       * <code>required int64 post_id = 3;</code>
       */
      public Builder setPostId(long value) {
        bitField0_ |= 0x00000004;
        postId_ = value;
        
        return this;
      }
      /**
       * <code>required int64 post_id = 3;</code>
       */
      public Builder clearPostId() {
        bitField0_ = (bitField0_ & ~0x00000004);
        postId_ = 0L;
        
        return this;
      }

      // required int64 create_time = 4;
      private long createTime_ ;
      /**
       * <code>required int64 create_time = 4;</code>
       */
      public boolean hasCreateTime() {
        return ((bitField0_ & 0x00000008) == 0x00000008);
      }
      /**
       * <code>required int64 create_time = 4;</code>
       */
      public long getCreateTime() {
        return createTime_;
      }
      /**
       * <code>required int64 create_time = 4;</code>
       */
      public Builder setCreateTime(long value) {
        bitField0_ |= 0x00000008;
        createTime_ = value;
        
        return this;
      }
      /**
       * <code>required int64 create_time = 4;</code>
       */
      public Builder clearCreateTime() {
        bitField0_ = (bitField0_ & ~0x00000008);
        createTime_ = 0L;
        
        return this;
      }

      // optional string body = 5;
      private java.lang.Object body_ = "";
      /**
       * <code>optional string body = 5;</code>
       *
       * <pre>
       * Optional fields
       * </pre>
       */
      public boolean hasBody() {
        return ((bitField0_ & 0x00000010) == 0x00000010);
      }
      /**
       * <code>optional string body = 5;</code>
       *
       * <pre>
       * Optional fields
       * </pre>
       */
      public java.lang.String getBody() {
        java.lang.Object ref = body_;
        if (!(ref instanceof java.lang.String)) {
          java.lang.String s = ((com.google.protobuf.ByteString) ref)
              .toStringUtf8();
          body_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>optional string body = 5;</code>
       *
       * <pre>
       * Optional fields
       * </pre>
       */
      public com.google.protobuf.ByteString
          getBodyBytes() {
        java.lang.Object ref = body_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          body_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string body = 5;</code>
       *
       * <pre>
       * Optional fields
       * </pre>
       */
      public Builder setBody(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000010;
        body_ = value;
        
        return this;
      }
      /**
       * <code>optional string body = 5;</code>
       *
       * <pre>
       * Optional fields
       * </pre>
       */
      public Builder clearBody() {
        bitField0_ = (bitField0_ & ~0x00000010);
        body_ = getDefaultInstance().getBody();
        
        return this;
      }
      /**
       * <code>optional string body = 5;</code>
       *
       * <pre>
       * Optional fields
       * </pre>
       */
      public Builder setBodyBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000010;
        body_ = value;
        
        return this;
      }

      // optional string geo = 6;
      private java.lang.Object geo_ = "";
      /**
       * <code>optional string geo = 6;</code>
       */
      public boolean hasGeo() {
        return ((bitField0_ & 0x00000020) == 0x00000020);
      }
      /**
       * <code>optional string geo = 6;</code>
       */
      public java.lang.String getGeo() {
        java.lang.Object ref = geo_;
        if (!(ref instanceof java.lang.String)) {
          java.lang.String s = ((com.google.protobuf.ByteString) ref)
              .toStringUtf8();
          geo_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>optional string geo = 6;</code>
       */
      public com.google.protobuf.ByteString
          getGeoBytes() {
        java.lang.Object ref = geo_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          geo_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string geo = 6;</code>
       */
      public Builder setGeo(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000020;
        geo_ = value;
        
        return this;
      }
      /**
       * <code>optional string geo = 6;</code>
       */
      public Builder clearGeo() {
        bitField0_ = (bitField0_ & ~0x00000020);
        geo_ = getDefaultInstance().getGeo();
        
        return this;
      }
      /**
       * <code>optional string geo = 6;</code>
       */
      public Builder setGeoBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000020;
        geo_ = value;
        
        return this;
      }

      // optional string owner_icon = 7;
      private java.lang.Object ownerIcon_ = "";
      /**
       * <code>optional string owner_icon = 7;</code>
       */
      public boolean hasOwnerIcon() {
        return ((bitField0_ & 0x00000040) == 0x00000040);
      }
      /**
       * <code>optional string owner_icon = 7;</code>
       */
      public java.lang.String getOwnerIcon() {
        java.lang.Object ref = ownerIcon_;
        if (!(ref instanceof java.lang.String)) {
          java.lang.String s = ((com.google.protobuf.ByteString) ref)
              .toStringUtf8();
          ownerIcon_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>optional string owner_icon = 7;</code>
       */
      public com.google.protobuf.ByteString
          getOwnerIconBytes() {
        java.lang.Object ref = ownerIcon_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          ownerIcon_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string owner_icon = 7;</code>
       */
      public Builder setOwnerIcon(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000040;
        ownerIcon_ = value;
        
        return this;
      }
      /**
       * <code>optional string owner_icon = 7;</code>
       */
      public Builder clearOwnerIcon() {
        bitField0_ = (bitField0_ & ~0x00000040);
        ownerIcon_ = getDefaultInstance().getOwnerIcon();
        
        return this;
      }
      /**
       * <code>optional string owner_icon = 7;</code>
       */
      public Builder setOwnerIconBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000040;
        ownerIcon_ = value;
        
        return this;
      }

      // optional int64 parent_id = 8;
      private long parentId_ ;
      /**
       * <code>optional int64 parent_id = 8;</code>
       */
      public boolean hasParentId() {
        return ((bitField0_ & 0x00000080) == 0x00000080);
      }
      /**
       * <code>optional int64 parent_id = 8;</code>
       */
      public long getParentId() {
        return parentId_;
      }
      /**
       * <code>optional int64 parent_id = 8;</code>
       */
      public Builder setParentId(long value) {
        bitField0_ |= 0x00000080;
        parentId_ = value;
        
        return this;
      }
      /**
       * <code>optional int64 parent_id = 8;</code>
       */
      public Builder clearParentId() {
        bitField0_ = (bitField0_ & ~0x00000080);
        parentId_ = 0L;
        
        return this;
      }

      // optional string hashtags = 9;
      private java.lang.Object hashtags_ = "";
      /**
       * <code>optional string hashtags = 9;</code>
       */
      public boolean hasHashtags() {
        return ((bitField0_ & 0x00000100) == 0x00000100);
      }
      /**
       * <code>optional string hashtags = 9;</code>
       */
      public java.lang.String getHashtags() {
        java.lang.Object ref = hashtags_;
        if (!(ref instanceof java.lang.String)) {
          java.lang.String s = ((com.google.protobuf.ByteString) ref)
              .toStringUtf8();
          hashtags_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>optional string hashtags = 9;</code>
       */
      public com.google.protobuf.ByteString
          getHashtagsBytes() {
        java.lang.Object ref = hashtags_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          hashtags_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string hashtags = 9;</code>
       */
      public Builder setHashtags(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000100;
        hashtags_ = value;
        
        return this;
      }
      /**
       * <code>optional string hashtags = 9;</code>
       */
      public Builder clearHashtags() {
        bitField0_ = (bitField0_ & ~0x00000100);
        hashtags_ = getDefaultInstance().getHashtags();
        
        return this;
      }
      /**
       * <code>optional string hashtags = 9;</code>
       */
      public Builder setHashtagsBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000100;
        hashtags_ = value;
        
        return this;
      }

      // @@protoc_insertion_point(builder_scope:com.goodi.post.CommentProto)
    }

    static {
      defaultInstance = new CommentProto(true);
      defaultInstance.initFields();
    }

    // @@protoc_insertion_point(class_scope:com.goodi.post.CommentProto)
  }


  static {
  }

  // @@protoc_insertion_point(outer_class_scope)
}
