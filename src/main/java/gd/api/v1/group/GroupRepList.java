
package gd.api.v1.group;

import gd.api.v1.PaginationRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.List;


@JsonSnakeCase
public class GroupRepList
{
    public List<GroupRep> data;

    public PaginationRep pagination;


    public GroupRepList()
    {
        this.data = new ArrayList<>(); //Empty list
    }


    public void add(
        GroupRep grp)
    {
        this.data.add(grp);
    }


    public boolean hasData()
    {
        return !this.data.isEmpty();
    }


    public int getSize()
    {
        return this.data.size();
    }
}
