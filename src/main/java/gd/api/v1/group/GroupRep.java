
package gd.api.v1.group;


import gd.support.jersey.JsonSnakeCase;

import java.util.List;


@JsonSnakeCase
public class GroupRep
{
    public String coverImageUrl;

    public Long id;

    public String name;

    /** Total num of downvotes occured in the group */
    public Long numDownvotes;

    public Long numPosts;

    /** Total num of upvotes occured in the group */
    public Long numUpvotes;

    public String ruleDescriptionImageUrl;

    public String ruleDescriptionText;


    //
    // RULE related
    //

    public List<String> allowedPostTypes;

    /**
     * If set, all the posts created in the group would be a thread
     * post of the specified type, the value must be valid
     * ThreadType enum
     */
    public String threadType;

    public Boolean isPrivate;

    /**
     * If true, the posts of group should be visited in ascending
     * order
     */
    public Boolean isReversed;
}
