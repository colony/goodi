
package gd.api.v1.group;


import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/groups")
@Singleton
public interface GroupResource
{
    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getGroup(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam);


    @GET
    @Produces("application/json")
    public Response getGroupList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam);
}
