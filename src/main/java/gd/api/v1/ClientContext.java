
package gd.api.v1;


import java.util.Locale;


;


/**
 * Caller's context whose data is fetched from GD extend headers per
 * incoming request. Note, the class is immutable and only getters
 * are available
 */
public class ClientContext
{
    private LatLng latlng;

    private Locale locale;

    private ClientType type;

    private int version;


    /**
     * Constructor for defaults
     */
    public ClientContext()
    {
        this.latlng = null;
        this.locale = Locale.US;
        this.type = ClientType.UNIDENTIFIED;
        this.version = 1;
    }


    public LatLng getLatLng()
    {
        return latlng;
    }


    public void setLatLng(
        LatLng latlng)
    {
        this.latlng = latlng;
    }


    public Locale getLocale()
    {
        return locale;
    }


    public void setLocale(
        Locale l)
    {
        this.locale = l;
    }


    public ClientType getType()
    {
        return type;
    }


    public void setType(
        ClientType type)
    {
        this.type = type;
    }


    public int getVersion()
    {
        return version;
    }


    public void setVersion(
        int v)
    {
        this.version = v;
    }
}
