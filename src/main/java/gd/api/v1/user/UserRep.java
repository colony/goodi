
package gd.api.v1.user;


import gd.api.v1.ClientDevice;
import gd.support.jersey.JsonSnakeCase;

import java.util.Date;


@JsonSnakeCase
public class UserRep
{
    public Date createTime;
    
    public ClientDevice device;

    public Long id;

    /** Admin only */
    public String idfa;
    
    public Boolean isAdmin;
    
    public Boolean isFake;
    
    public Long numLikes;

    public Long numPosts;

    public Long points;
    
    public Double signupLat;
    
    public Double signupLon;

    public Date updateTime;
}
