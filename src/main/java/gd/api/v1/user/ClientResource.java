
package gd.api.v1.user;


import gd.support.jersey.BoolParam;
import gd.support.jersey.DoubleParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;



@Path("/client")
@Singleton
public interface ClientResource
{
    @Path("/card")
    @GET
    @Produces("application/json")
    public Response drawCard(
        @Context HttpHeaders headers,
        @QueryParam("deck_id") String deckId,
        @QueryParam("is_request") @DefaultValue("0") BoolParam isRequestParam);


    @Path("/config")
    @GET
    @Produces("application/json")
    public Response getClientConfig(
        @Context HttpHeaders headers);


    /**
     * Fetch initialization data per user
     */
    @Path("/init")
    @GET
    @Produces("application/json")
    public Response getInitData(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo);


    @Path("/report/feedback")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportFeedback(
        @Context HttpHeaders headers,
        @FormParam("feedback") String feedback);


    /**
     * @param isFirst
     *            The hint sent from client that this is the first
     *            time the geo is reported, server might need update
     *            user's signup geo info
     */
    @Path("/report/geo")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportGeo(
        @Context HttpHeaders headers,
        @FormParam("lat") DoubleParam latParam,
        @FormParam("lng") DoubleParam lngParam,
        @FormParam("is_first") @DefaultValue("0") BoolParam isFirst);


    @Path("/report/invite")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportInvite(
        @Context HttpHeaders headers,
        @FormParam("platform") String platform);


    @Path("/report/notification")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportNotificationToken(
        @Context HttpHeaders headers,
        @FormParam("token") String deviceToken);


    @Path("/report/share")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportShare(
        @Context HttpHeaders headers,
        @FormParam("post_id") LongParam idParam,
        @FormParam("platform") String platform);


    @Path("/util/reversegeo")
    @GET
    @Produces("application/json")
    public Response reverseGeo(
        @Context HttpHeaders headers,
        @QueryParam("lat") DoubleParam latParam,
        @QueryParam("lon") DoubleParam lonParam);
}
