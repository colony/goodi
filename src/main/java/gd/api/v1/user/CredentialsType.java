
package gd.api.v1.user;


public enum CredentialsType
{
    FACEBOOK("fb"),

    PASSWORD("p"),

    ;

    private final String abbrev;


    CredentialsType(
        String abbrev)
    {
        this.abbrev = abbrev;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public static CredentialsType fromAbbreviation(
        String abbrev)
    {
        for (CredentialsType ec : CredentialsType.values())
        {
            if (ec.abbrev.equals(abbrev))
            {
                return ec;
            }
        }

        throw new IllegalArgumentException(
            "CredentialsType contains no "
                + "constant with abbreviation \""
                + abbrev
                + "\"");
    }
}
