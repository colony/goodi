package gd.api.v1.user;

import gd.support.jersey.JsonSnakeCase;


@JsonSnakeCase
public class StatusRep 
{
    public int numUnreadActivities = 0;
    
    public int numUnreadAnnouncements = 0;
    
    public long points = 0L;
}
