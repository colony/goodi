
package gd.api.v1.user;


import gd.api.v1.ClientDevice;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/accounts")
@Singleton
public interface AccountResource
{
    /**
     * Creates a new account with some auxiliary data for internal
     * journaling
     * 
     * @param headers
     * @param identifierForAdvertising
     *            Apple's unique (i guess user could change it)
     *            device ID
     * @param lat
     *            The latitude where user install the app first time
     * @param lon
     *            The longitude where user install the app first
     *            time
     * @param device_token
     *            The push notification token if user grant the
     *            permission at registration time
     * @return
     */
    @Path("/bind")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response bind(
        @Context HttpHeaders headers,
        @FormParam("idfa") String identifierForAdvertising,
        @FormParam("device_token") String deviceToken,
        @FormParam("device_model") ClientDevice device);
}
