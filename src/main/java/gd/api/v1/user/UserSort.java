
package gd.api.v1.user;


public enum UserSort
{
    ID("id"),

    ;

    private final String abbrev;


    private UserSort(
        String abbrev)
    {
        this.abbrev = abbrev;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public static UserSort fromAbbreviation(
        String abbrev)
    {
        for (UserSort us : UserSort.values())
        {
            if (us.abbrev.equals(abbrev))
            {
                return us;
            }
        }

        throw new IllegalArgumentException(
            "UserSort contains no constant with abbreviation \""
                + abbrev + "\"");
    }
}
