
package gd.api.v1.user;


import gd.impl.util.PageParam;
import gd.support.jersey.IntParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.model.Resource;


@Path("/users")
@Singleton
public interface UserResource
{   
    @GET
    @Produces("application/json")
    public Response getAll(
        @Context HttpHeaders headers,
        @QueryParam("n") @DefaultValue("20") IntParam count);
  
    
    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getUser(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam id);


    @Path("/{id}/followed")
    @GET
    @Produces("application/json")
    public Response getFollowed(
        @Context HttpHeaders headers,
        @BeanParam PageParam page,
        @PathParam("id") LongParam id);


    @Path("/{id}/following")
    @GET
    @Produces("application/json")
    public Response getFollowing(
        @Context HttpHeaders headers,
        @BeanParam PageParam page,
        @PathParam("id") LongParam id);
    
    
    @Path("/me")
    public Resource locateMeResource();
}
