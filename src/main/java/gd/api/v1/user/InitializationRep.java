
package gd.api.v1.user;


import gd.support.jersey.JsonSnakeCase;

import java.util.List;


@JsonSnakeCase
public class InitializationRep
{
    public String clientConfig;

    /**
     * A list of pairs of group ID and name, mainly used for user to
     * pick from a selection list (for example, when creating post)
     */
    public List<InitGroupRep> groupPickList;



    @JsonSnakeCase
    public static class InitGroupRep
    {
        public int allowedPostTypesCode;

        public Long id;

        public String name;
    }
}
