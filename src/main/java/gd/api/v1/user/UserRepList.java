package gd.api.v1.user;

import gd.api.v1.PaginationRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.List;

@JsonSnakeCase
public class UserRepList
{
    public List<UserRep> data;

    public PaginationRep pagination;


    public UserRepList()
    {
        this.data = new ArrayList<>(); //Empty list
    }
    
    
    public void add(UserRep user)
    {
        this.data.add(user);
    }
    
    
    public boolean hasData()
    {
        return !this.data.isEmpty();
    }
    
    
    public int size()
    {
        return this.data.size();
    }
}
