
package gd.api.v1.user;


import gd.impl.util.PageParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/me")
@Singleton
public interface MeResource
{
    @Path("/posts/create")
    @GET
    @Produces("application/json")
    public Response getMyPosts(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam);
    
    
    @Path("/status")
    @GET
    @Produces("application/json")
    public Response getMyStatus(
        @Context HttpHeaders headers);
}
