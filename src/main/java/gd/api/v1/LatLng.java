
package gd.api.v1;


public class LatLng
{
    /**
     * latitude in degrees
     */
    private double lat;

    /**
     * longitude in degrees
     */
    private double lon;


    public LatLng()
    {
    }


    public LatLng(
        double lat,
        double lon)
    {
        this.lat = lat;
        this.lon = lon;
    }


    public LatLng(
        String str)
    {
        if (str.startsWith("1|") || str.startsWith("2|")) {
            // This is an old version of serialized string in format of
            // (1 or 2)|lat|lon(a bunch of other info such as city, country, etc)
            
            String[] fields = str.split("\\|", 4);
            if (fields == null || fields.length < 3) {
                throw new IllegalArgumentException("Invalid latlng serialization \"" + str + "\"");
            }

            // The first field is version number, the forth one is rest of geo info that we simply ignore
            this.lat = Double.valueOf(fields[1]);
            this.lon = Double.valueOf(fields[2]);
        }
        else {
            // This is new expected version in format of
            // latitude,longitude

            String[] fields = str.split(",", 2);
            if (fields == null || fields.length != 2) {
                throw new IllegalArgumentException("Invalid latlng serialization \"" + str + "\""); 
            }
            
            this.lat = Double.valueOf(fields[0]);
            this.lon = Double.valueOf(fields[1]);
        }
    }


    public double getLat()
    {
        return lat;
    }


    public void setLat(
        double lat)
    {
        this.lat = lat;
    }


    public double getLon()
    {
        return lon;
    }


    public void setLon(
        double lon)
    {
        this.lon = lon;
    }
}
