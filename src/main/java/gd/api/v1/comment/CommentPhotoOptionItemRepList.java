
package gd.api.v1.comment;
import gd.api.v1.PaginationRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.List;
;


@JsonSnakeCase
public class CommentPhotoOptionItemRepList
{
    public List<CommentPhotoOptionItemRep> data;

    public PaginationRep pagination;


    public CommentPhotoOptionItemRepList()
    {
        this.data = new ArrayList<>(); //Empty list
    }


    public void add(
        CommentPhotoOptionItemRep item)
    {
        this.data.add(item);
    }


    public boolean hasData()
    {
        return !this.data.isEmpty();
    }


    public int getSize()
    {
        return this.data.size();
    }
}
