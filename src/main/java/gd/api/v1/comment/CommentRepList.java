
package gd.api.v1.comment;

import gd.api.v1.PaginationRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.List;


@JsonSnakeCase
public class CommentRepList
{
    public List<CommentRep> data;

    public PaginationRep pagination;
    
    
    public CommentRepList()
    {
        this.data = new ArrayList<>(); //Empty list
    }
    
    
    public void add(CommentRep cmt)
    {
        this.data.add(cmt);
    }
    
    
    public CommentRep get(int i)
    {
        return this.data.get(i);
    }
    
    
    public boolean hasData()
    {
        return !this.data.isEmpty();
    }
    
    
    public int getSize()
    {
        return this.data.size();
    }
}
