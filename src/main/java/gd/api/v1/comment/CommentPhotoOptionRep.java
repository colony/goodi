package gd.api.v1.comment;

import gd.support.jersey.JsonSnakeCase;


@JsonSnakeCase
public class CommentPhotoOptionRep
{
    public String coverImageUrl;
    
    public Integer id;
    
    public String name;
}
