package gd.api.v1.comment;

import gd.api.v1.post.MediaRep;
import gd.support.jersey.JsonSnakeCase;

@JsonSnakeCase
public class CommentPhotoOptionItemRep
{
    public Integer id;
    
    public MediaRep media;
}
