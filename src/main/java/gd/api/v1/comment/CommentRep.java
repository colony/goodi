
package gd.api.v1.comment;


import gd.api.v1.post.Geo;
import gd.api.v1.post.MediaRep;
import gd.api.v1.post.VoteStatus;
import gd.api.v1.user.UserRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.Date;
import java.util.List;


@JsonSnakeCase
public class CommentRep
{
    public String body;

    public Date createTime;

    public Geo geo;

    public List<String> hashtags;

    public Long id;
    
    public Boolean isBadReviewed;

    public Long numDownvotes;

    public Long numReplies;

    public Long numUpvotes;

    public UserRep owner;

    public MediaRep ownerIcon;

    public CommentRep parent;
    
    public Long postId;
    
    public CommentType type;

    public VoteStatus voteStatus;
    
    
    //
    // CommentType specific fields
    //
    
    // PHOTO
    public MediaRep photo;
    
    
    public CommentRep shallowCopy()
    {
        CommentRep copy = new CommentRep();
        copy.body = body;
        copy.createTime = createTime;
        copy.geo = geo;
        copy.hashtags = hashtags;
        copy.id = id;
        copy.isBadReviewed = isBadReviewed;
        copy.numDownvotes = numDownvotes;
        copy.numReplies = numReplies;
        copy.numUpvotes = numUpvotes;
        copy.owner = owner;
        copy.ownerIcon = ownerIcon;
        copy.parent = parent;
        copy.photo = photo;
        copy.postId = postId;
        copy.type = type;
        copy.voteStatus = voteStatus;
        return copy;
    }
}
