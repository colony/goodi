
package gd.api.v1.comment;


import gd.impl.util.PageParam;
import gd.support.jersey.BoolParam;
import gd.support.jersey.IntParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/comments")
@Singleton
public interface CommentResource
{
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createComment(
        @Context HttpHeaders headers,
        @BeanParam CommentForm commentForm);


    @Path("/photopicker")
    @GET
    @Produces("application/json")
    public Response getCommentPhotoPickerOptionList(
        @Context HttpHeaders headers);


    @Path("/photopicker/{id}")
    @GET
    @Produces("application/json")
    public Response getCommentPhotoPickerOption(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") IntParam optionIdParam,
        @BeanParam PageParam pageParam);


    /**
     * If 'all' is true, the size of pageParam will be ignored, the
     * API will retrieve all comments of the specified post
     */
    @GET
    @Produces("application/json")
    public Response getComments(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("post_id") LongParam idParam,
        @QueryParam("all") @DefaultValue("0") BoolParam allParam,
        @BeanParam PageParam pageParam);


    @Path("/{id}/like")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response likeComment(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("status") BoolParam statusParam);


    @Path("/{id}/vote")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response voteComment(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("down") @DefaultValue("0") BoolParam isDownParam,
        @FormParam("up") @DefaultValue("0") BoolParam isUpParam,
        @FormParam("undown") @DefaultValue("0") BoolParam isUndownParam,
        @FormParam("unup") @DefaultValue("0") BoolParam isUnupParam);
}
