package gd.api.v1.comment;

public enum CommentType
{
    /**
     * The comments attached with one or more photo medias
     */
    PHOTO,
    
    /**
     * The most basic form of comment without taking any media content
     */
    SIMPLE,

    ;
}
