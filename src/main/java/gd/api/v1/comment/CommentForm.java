
package gd.api.v1.comment;


import gd.api.v1.post.Geo;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;


public class CommentForm
{
    @FormDataParam("body")
    private String body;

    @FormDataParam("geo")
    private Geo geo;

    @FormDataParam("parent_id")
    private Long parentId;

    @FormDataParam("option_item_id")
    private Integer photoOptionItemId;

    @FormDataParam("post_id")
    private Long postId;

    @FormDataParam("type")
    private String type;


    //
    // Attach with photo media
    //
    @FormDataParam("photo_format")
    private String photoFormat;

    @FormDataParam("photo_height")
    private Integer photoHeight;

    @FormDataParam("photo_size")
    private Long photoSize;

    @FormDataParam("photo_data")
    private InputStream photoStream;

    @FormDataParam("photo_width")
    private Integer photoWidth;



    public CommentForm()
    {
    }
    
    
    public boolean hasPhotoOptionItemId()
    {
        return photoOptionItemId != null && photoOptionItemId > 0L;
    }


    //
    // Getters & Setters
    //


    public String getBody()
    {
        return body;
    }



    public void setBody(
        String body)
    {
        this.body = body;
    }



    public Geo getGeo()
    {
        return geo;
    }



    public void setGeo(
        Geo geo)
    {
        this.geo = geo;
    }



    public Long getParentId()
    {
        return parentId;
    }



    public void setParentId(
        Long parentId)
    {
        this.parentId = parentId;
    }



    public Integer getPhotoOptionItemId()
    {
        return photoOptionItemId;
    }



    public void setPhotoOptionItemId(
        Integer photoOptionItemId)
    {
        this.photoOptionItemId = photoOptionItemId;
    }



    public Long getPostId()
    {
        return postId;
    }



    public void setPostId(
        Long postId)
    {
        this.postId = postId;
    }



    public String getType()
    {
        return type;
    }



    public void setType(
        String type)
    {
        this.type = type;
    }



    public String getPhotoFormat()
    {
        return photoFormat;
    }



    public void setPhotoFormat(
        String photoFormat)
    {
        this.photoFormat = photoFormat;
    }



    public Integer getPhotoHeight()
    {
        return photoHeight;
    }



    public void setPhotoHeight(
        Integer photoHeight)
    {
        this.photoHeight = photoHeight;
    }



    public Long getPhotoSize()
    {
        return photoSize;
    }



    public void setPhotoSize(
        Long photoSize)
    {
        this.photoSize = photoSize;
    }



    public InputStream getPhotoStream()
    {
        return photoStream;
    }



    public void setPhotoStream(
        InputStream photoStream)
    {
        this.photoStream = photoStream;
    }



    public Integer getPhotoWidth()
    {
        return photoWidth;
    }



    public void setPhotoWidth(
        Integer photoWidth)
    {
        this.photoWidth = photoWidth;
    }
}
