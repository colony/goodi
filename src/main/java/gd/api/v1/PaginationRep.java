
package gd.api.v1;


import gd.support.jersey.JsonSnakeCase;


@JsonSnakeCase
public class PaginationRep
{
    public String nextUrl;

    public String prevUrl;


    public PaginationRep()
    {
    }


    public PaginationRep(
        String prevUrl,
        String nextUrl)
    {
        this.prevUrl = prevUrl;
        this.nextUrl = nextUrl;
    }
}
