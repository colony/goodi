
package gd.api.v1.post;


import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/feed")
@Singleton
public interface FeedResource
{
    @GET
    @Path("/announcement")
    @Produces("application/json")
    public Response getAnnouncementFeed(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo);
    
    
    @GET
    @Path("/featured")
    @Produces("application/json")
    public Response getFeaturedFeed(
        @Context HttpHeaders headers,
        @Context UriInfo uriInf);
    
    
    @GET
    @Produces("application/json")
    public Response getFeed(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("geo") Geo geo,
        @BeanParam PageParam pageParam);


    @GET
    @Path("/group/{id}")
    @Produces("application/json")
    public Response getFeedByGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam groupIdParam,
        @BeanParam PageParam pageParam);


    @GET
    @Path("/hashtag/{tag}")
    @Produces("application/json")
    public Response getFeedByHashtag(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("tag") String hashtag,
        @BeanParam PageParam pageParam);
}
