
package gd.api.v1.post;


import gd.support.jersey.BoolParam;

import java.io.InputStream;

import javax.ws.rs.DefaultValue;

import org.glassfish.jersey.media.multipart.FormDataParam;


public class PostForm
{
    @FormDataParam("body")
    private String body;

    @FormDataParam("geo")
    private Geo geo;

    @DefaultValue("1")
    @FormDataParam("group_id")
    private Long groupId;

    /**
     * If set, the posted post will also create a thread with the
     * specified type and the post becomes the root of the thread
     */
    @FormDataParam("create_thread_type")
    private String createThreadType;

    /**
     * If set, the post is created as child of a thread of
     * the specified id
     */
    @FormDataParam("thread_id")
    private Long threadId;

    @FormDataParam("type")
    private String type;


    //
    // Audio post
    //
    @FormDataParam("audio_duration")
    private Long audioDuration;

    @FormDataParam("audio_format")
    private String audioFormat;

    @FormDataParam("audio_size")
    private Long audioSize;

    @FormDataParam("audio_data")
    private InputStream audioStream;


    //
    //  Photo post
    //
    @FormDataParam("photo_format")
    private String photoFormat;

    @FormDataParam("photo_height")
    private Integer photoHeight;

    @FormDataParam("photo_size")
    private Long photoSize;

    @FormDataParam("photo_data")
    private InputStream photoStream;

    /**
     * True if the photo is uploaded from client's device, false if
     * it is taken shot from camera
     */
    @FormDataParam("photo_uploaded")
    private BoolParam photoUploaded;

    @FormDataParam("photo_width")
    private Integer photoWidth;


    //
    // Text post
    //
    @FormDataParam("text_data")
    private String text;

    @FormDataParam("text_background_color")
    private String textBackgroundColor;

    @FormDataParam("text_color")
    private String textColor;

    @FormDataParam("text_format")
    private String textFormat;


    //
    // Link post
    //
    @FormDataParam("link_data")
    private String link;

    @FormDataParam("link_description")
    private String linkDescription;

    @FormDataParam("link_title")
    private String linkTitle;

    @FormDataParam("link_embed_format")
    private String linkEmbedItemFormat;

    @FormDataParam("link_embed_height")
    private Integer linkEmbedItemHeight;

    @FormDataParam("link_embed_width")
    private Integer linkEmbedItemWidth;

    @FormDataParam("link_embed_url")
    private String linkEmbedItemUrl;


    //
    // Video post
    //
    @FormDataParam("video_format")
    private String videoFormat;

    @FormDataParam("video_data")
    private InputStream videoStream;

    @FormDataParam("video_size")
    private Long videoSize;


    public PostForm()
    {
    }


    //
    // Getters & Setters
    //

    public String getBody()
    {
        return body;
    }



    public void setBody(
        String body)
    {
        this.body = body;
    }


    public String getCreateThreadType()
    {
        return createThreadType;
    }


    public void setCreateThreadType(
        String createThreadType)
    {
        this.createThreadType = createThreadType;
    }


    public String getType()
    {
        return type;
    }



    public void setType(
        String type)
    {
        this.type = type;
    }



    public Geo getGeo()
    {
        return geo;
    }



    public void setGeo(
        Geo geo)
    {
        this.geo = geo;
    }


    public Long getGroupId()
    {
        return groupId;
    }


    public void setGroupId(
        Long groupId)
    {
        this.groupId = groupId;
    }


    public Long getThreadId()
    {
        return threadId;
    }


    public void setThreadId(
        Long threadId)
    {
        this.threadId = threadId;
    }


    public Long getAudioDuration()
    {
        return audioDuration;
    }



    public void setAudioDuration(
        Long audioDuration)
    {
        this.audioDuration = audioDuration;
    }



    public String getAudioFormat()
    {
        return audioFormat == null ? null : audioFormat.toUpperCase();
    }



    public void setAudioFormat(
        String audioFormat)
    {
        this.audioFormat = audioFormat;
    }



    public Long getAudioSize()
    {
        return audioSize;
    }



    public void setAudioSize(
        Long audioSize)
    {
        this.audioSize = audioSize;
    }



    public InputStream getAudioStream()
    {
        return audioStream;
    }



    public void setAudioStream(
        InputStream audioStream)
    {
        this.audioStream = audioStream;
    }



    public String getPhotoFormat()
    {
        return photoFormat == null ? null : photoFormat.toUpperCase();
    }



    public void setPhotoFormat(
        String photoFormat)
    {
        this.photoFormat = photoFormat;
    }



    public Integer getPhotoHeight()
    {
        return photoHeight;
    }



    public void setPhotoHeight(
        Integer photoHeight)
    {
        this.photoHeight = photoHeight;
    }



    public Long getPhotoSize()
    {
        return photoSize;
    }



    public void setPhotoSize(
        Long photoSize)
    {
        this.photoSize = photoSize;
    }



    public InputStream getPhotoStream()
    {
        return photoStream;
    }



    public void setPhotoStream(
        InputStream photoStream)
    {
        this.photoStream = photoStream;
    }


    public boolean isPhotoUploaded()
    {
        return photoUploaded == null ? false : photoUploaded.get();
    }


    public void setPhotoUploaded(
        BoolParam pu)
    {
        this.photoUploaded = pu;
    }


    public Integer getPhotoWidth()
    {
        return photoWidth;
    }


    public void setPhotoWidth(
        Integer photoWidth)
    {
        this.photoWidth = photoWidth;
    }


    public String getText()
    {
        return text;
    }



    public void setText(
        String text)
    {
        this.text = text;
    }



    public String getTextBackgroundColor()
    {
        return textBackgroundColor;
    }



    public void setTextBackgroundColor(
        String textBackgroundColor)
    {
        this.textBackgroundColor = textBackgroundColor;
    }



    public String getTextColor()
    {
        return textColor;
    }



    public void setTextColor(
        String textColor)
    {
        this.textColor = textColor;
    }



    public String getTextFormat()
    {
        return textFormat == null ? null : textFormat.toUpperCase();
    }



    public void setTextFormat(
        String textFormat)
    {
        this.textFormat = textFormat;
    }



    public String getLink()
    {
        return link;
    }



    public void setLink(
        String link)
    {
        this.link = link;
    }



    public String getLinkDescription()
    {
        return linkDescription;
    }



    public void setLinkDescription(
        String linkDescription)
    {
        this.linkDescription = linkDescription;
    }



    public String getLinkTitle()
    {
        return linkTitle;
    }



    public void setLinkTitle(
        String linkTitle)
    {
        this.linkTitle = linkTitle;
    }



    public String getLinkEmbedItemFormat()
    {
        return linkEmbedItemFormat == null ? null : linkEmbedItemFormat.toUpperCase();
    }



    public void setLinkEmbedItemFormat(
        String linkEmbedItemFormat)
    {
        this.linkEmbedItemFormat = linkEmbedItemFormat;
    }



    public Integer getLinkEmbedItemHeight()
    {
        return linkEmbedItemHeight;
    }



    public void setLinkEmbedItemHeight(
        Integer linkEmbedItemHeight)
    {
        this.linkEmbedItemHeight = linkEmbedItemHeight;
    }



    public Integer getLinkEmbedItemWidth()
    {
        return linkEmbedItemWidth;
    }



    public void setLinkEmbedItemWidth(
        Integer linkEmbedItemWidth)
    {
        this.linkEmbedItemWidth = linkEmbedItemWidth;
    }



    public String getLinkEmbedItemUrl()
    {
        return linkEmbedItemUrl;
    }


    public void setLinkEmbedItemUrl(
        String linkEmbedItemUrl)
    {
        this.linkEmbedItemUrl = linkEmbedItemUrl;
    }


    public String getVideoFormat()
    {
        return videoFormat;
    }


    public void setVideoFormat(
        String fmt)
    {
        this.videoFormat = fmt;
    }


    public InputStream getVideoStream()
    {
        return videoStream;
    }


    public void setVideoStream(
        InputStream videoStream)
    {
        this.videoStream = videoStream;
    }


    public Long getVideoSize()
    {
        return videoSize;
    }


    public void setVideoSize(
        Long videoSize)
    {
        this.videoSize = videoSize;
    }
}
