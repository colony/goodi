
package gd.api.v1.post;


/**
 * Enum describing a post type. Each post contains a list of medias
 * various on different type.
 */
public enum PostType
{
    /**
     * !IMPORTANT
     * <p>
     * the ordinal of each type couldn't be changed, new enum value
     * should append to the tail, both Group#getAllowedPostTypesCode
     * and client takes advantage of ordinal value, it can't be
     * changed
     */

    /**
     * <ul>
     * <li>0 - audio
     * <li>1 - icon or photo, audio background
     * </ul>
     */
    AUDIO,

    /**
     * Card post, the contained media is text media, but the post
     * body contains specific {@CardMetadata}
     * <ul>
     * <li>0 - text
     * </ul>
     */
    CARD,

    /**
     * 0 - link
     */
    LINK,

    /**
     * 0 - photo
     */
    PHOTO,

    /**
     * 0 - text
     */
    TEXT,

    /**
     * <ul>
     * <li>0 - video
     * <li>1 - photo, video thumbnail
     * </ul>
     */
    VIDEO,

    ;
}
