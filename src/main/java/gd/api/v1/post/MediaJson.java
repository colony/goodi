
package gd.api.v1.post;


import gd.support.jersey.JsonSnakeCase;


@JsonSnakeCase
public class MediaJson
{
    private MediaType type;

    /**
     * Text media
     */
    private String text;
    
    private String textFormat;


    /**
     * Photo media
     */
    private String large;
    
    private int largeH;
    
    private int largeW;

    private String medium;
    
    private int mediumH;
    
    private int mediumW;
    
    private String photoFormat;

    private String thumbnail;
    
    private int thumbnailH;
    
    private int thumbnailW;


    /**
     * Link media
     */
//    private String title;
//
//    private String url;


    public MediaJson()
    {
    }
    
    
    public boolean validate()
    {
        if (type == null)
            return false;
        
        // Other sanity check goes here
        return true;
    }
    
    
    //
    // Getters
    //
    

    public MediaType getType()
    {
        return type;
    }
    

    public String getText()
    {
        return text;
    }
    
    
    public String getTextFormat()
    {
        return textFormat;
    }

    public String getLarge()
    {
        return large;
    }
    
    
    public int getLargeH()
    {
        return largeH;
    }
    
    
    public int getLargeW()
    {
        return largeW;
    }


    public String getMedium()
    {
        return medium;
    }

    
    public int getMediumH()
    {
        return mediumH;
    }
    
    
    public int getMediumW()
    {
        return mediumW;
    }
    
    
    public String getPhotoFormat()
    {
        return photoFormat;
    }
    

    public String getThumbnail()
    {
        return thumbnail;
    }
    
    
    public int getThumbnailH()
    {
        return thumbnailH;
    }
    
    
    public int getThumbnailW()
    {
        return thumbnailW;
    }
}
