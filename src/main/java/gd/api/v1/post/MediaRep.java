package gd.api.v1.post;

import gd.impl.media.AudioMedia;
import gd.impl.media.LinkMedia;
import gd.impl.media.PhotoMedia;
import gd.impl.media.TextMedia;
import gd.impl.media.VideoMedia;
import gd.support.jersey.JsonSnakeCase;


@JsonSnakeCase
public class MediaRep
{
    public MediaType type;
    
    
    /**
     * Audio media
     */
    public AudioMedia.Format audioFormat;
    
    public Long audioDuration;
    
    public String audioUrl; 
    
    
    /**
     * Text media
     */
    public String text;
    
    public String textBackgroundColor;
    
    public String textColor;

    public TextMedia.Format textFormat;

    
    /**
     * Icon media
     */
    public String iconSystemId;
    
    public String iconColorHex;
    
    public String iconUrl;
    
    
    /**
     * Photo media
     */
    public PhotoMedia.Format photoFormat;
    
    public Integer photoH;
    
    public Integer photoW;
    
    public Boolean photoUploaded;
    
    public String photoUrl;
    
    public String photoThumbnailUrl;
    
    public Integer photoThumbnailH;
    
    public Integer photoThumbnailW;
    
    
    /**
     * Link media
     */
    public LinkMedia.Format linkEmbedItemFormat;
    
    public Integer linkEmbedItemH;
    
    public Integer linkEmbedItemW;
    
    public String linkEmbedItemUrl;
    
    public String linkDescription;
    
    public String linkHost;
    
    public String linkTitle;
    
    public String linkUrl;
    
    
    /**
     * Video media
     */
    public VideoMedia.Format videoFormat;
    
    public String videoUrl;
    
    
    
    /**
     * Avatar media
     */
//    public String avatar;
//    
//    public String avatarThumbnail;
    

    public MediaRep()
    {
        
    }
    
    
    /**
     * Default constructor, type is required
     */
    public MediaRep(MediaType type)
    {
        this.type = type;
    }
}

