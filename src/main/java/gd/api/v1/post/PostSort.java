
package gd.api.v1.post;

import com.amazonaws.util.StringUtils;


public enum PostSort 
{
    GLOBAL_POPULAR("gp"),
    
    GLOBAL_RECENT("gr"),
    
    GROUP_POPULAR("grp"),
    
    GROUP_RECENT("grr"),
    
    ID("id"),

    NEARBY_POPULAR("nbp"),

    NEARBY_RECENT("nbr")

    ;

    private final String abbrev;


    private PostSort(
        String abbrev) {
        this.abbrev = abbrev;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public static PostSort fromAbbreviation(
        String abbrev)
    {
        if (StringUtils.isNullOrEmpty(abbrev))
            throw new IllegalArgumentException("PostSort contains no empty or null constant");
        
        abbrev = abbrev.toLowerCase();
        
        // For legacy reason, old app use POPULAR to retrieve global feed
        // we replace it with GLOBAL_RECENT
        if ("p".equals(abbrev))
            return PostSort.GLOBAL_RECENT;
        
        for (PostSort ps : PostSort.values()) {
            if (ps.abbrev.equals(abbrev)) {
                return ps;
            }
        }

        throw new IllegalArgumentException(
            "PostSort contains no constant with abbreviation \""
                + abbrev + "\"");
    }
}
