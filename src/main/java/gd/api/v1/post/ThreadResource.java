package gd.api.v1.post;

import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/threads")
@Singleton
public interface ThreadResource
{
    @Path("/{id}/root")
    @GET
    @Produces("application/json")
    public Response getRootPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam);
    
    
    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getThread(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam,
        @BeanParam PageParam pageParam);
    
    
    @Path("/{id}/action")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response performAction(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("action_key") String actionKey,
        @FormParam("action_val") String actionValue);
}
