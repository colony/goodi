
package gd.api.v1.post;


import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;


@Path("/medias")
@Singleton
public interface MediaResource
{
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createMedia(
        @Context HttpHeaders headers,
        FormDataMultiPart multiPart);


    @DELETE
    public Response deleteMedia(
        @Context HttpHeaders headers,
        @QueryParam("uri") String uri);
}
