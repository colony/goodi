
package gd.api.v1.post;


import gd.api.v1.PaginationRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.List;


@JsonSnakeCase
//@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.PUBLIC_ONLY)
public class PostRepList
{
    public List<PostRep> data;

    /**
     * Only set, if the postlist is fetched by a hashtag
     */
    public String hashtag;

    public PaginationRep pagination;
    
    /**
     * Populated only for posts that return for a thread
     */
    public ThreadRep thread;


    public PostRepList()
    {
        this.data = new ArrayList<>(); //Empty list
    }


    public void add(
        PostRep post)
    {
        this.data.add(post);
    }
    

    public boolean hasData()
    {
        return !this.data.isEmpty();
    }


    public int getSize()
    {
        return this.data.size();
    }
}
