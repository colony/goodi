
package gd.api.v1.post;


public enum VoteStatus
{
    /**
     * Enum position not changeable, we persist ordinal()
     */
    
    DOWNVOTE,
    NOVOTE,
    UPVOTE,

    ;
}
