
package gd.api.v1.post;


public enum MediaType
{
    AUDIO("AU"),

    ICON("IC"),
    
    LINK("LI"),

    PHOTO("PH"),

    TEXT("TX"),
    
    VIDEO("VD"),

    ;

    /**
     * The two-letter initial
     */
    private final String initial;


    MediaType(
        String initial)
    {
        this.initial = initial;
    }


    public String getTwoLetterInitial()
    {
        return initial;
    }


    public static MediaType fromInitial(
        String initial)
    {
        for (MediaType mt : MediaType.values())
        {
            if (mt.initial.equals(initial))
                return mt;
        }

        throw new IllegalArgumentException(
            "MediaType contains no initial \"" + initial + "\"");
    }
}
