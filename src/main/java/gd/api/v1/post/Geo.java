
package gd.api.v1.post;


import gd.api.v1.LatLng;
import gd.impl.util.Stringifiable;
import gd.support.jersey.JsonSnakeCase;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;


/**
 * Serialization as string format:
 * 
 * <pre>
 * Version 1: 1|lat|lon(|neighborhood|city|state|country|countryCode)
 * Version 2: 2|lat|lon(|name|neighborhood|borough|city|state|country|countryCode)
 * Version 3: 3|lat|lon(|name|neighborhood|city|state|country|countryCode)
 * 
 * NOTE:
 * Since Ver3, we combine neighborhood and borough into one field, most of time reverse-geo 
 * service like Google and Apple will either return neighborhood or sublocality (borough),
 * it is easier just combine them into one field (the closer the winner) 
 * 
 * Info in parenthesis are optional, but if any info is provided
 * (say, city), the rest geo info (i.e. state, etc) MUST be either
 * provided or empty so that we could extract correct info at
 * correct position
 * </pre>
 */
@JsonSnakeCase
public class Geo
    implements
        Stringifiable<Geo>
{
    public static final String JOIN = "|";

    public static final String SPLIT = "\\|";

    public static final int VERSION = 3;

    /** Locality */
    private String city;

    /** The name of country */
    private String country;

    /** ISO standard country abbreviation code */
    private String countryCode;

    /** latitude in degrees */
    private double lat;

    /** longitude in degrees */
    private double lon;

    /** A human-readable place name (i.e. Blahblah university) */
    private String name;

    /** Neighborhood, Borough, Sublocality */
    private String neighborhood;

    /** Saved serialization string (if any) */
    @JsonIgnore
    private String serializationStr;

    /** Aka province, administrativeArea */
    private String state;



    public Geo()
    {
    }


    /**
     * The constructor that accepts a single string is mainly used
     * by Jersey for param conversion. The parameter must conform to
     * serialization format
     */
    public Geo(
        String geoStr)
    {
        this.fromStr(geoStr);
        this.serializationStr = geoStr;
    }


    public Geo(
        double lat,
        double lng)
    {
        this.lat = lat;
        this.lon = lng;
    }


    public Geo(
        double lat,
        double lon,
        String name,
        String neighborhood,
        String city,
        String state,
        String country,
        String countryCode)
    {
        this.name = name;
        this.neighborhood = neighborhood;
        this.city = city;
        this.state = state;
        this.country = country;
        this.countryCode = countryCode;
        this.lat = lat;
        this.lon = lon;
    }


    /**
     * @return True if any extra info (i.e. state, etc) is provided
     */
    public boolean hasExtraInfo()
    {
        return StringUtils.isNotEmpty(city) ||
            StringUtils.isNotEmpty(country) ||
            StringUtils.isNotEmpty(neighborhood) ||
            StringUtils.isNotEmpty(state) ||
            StringUtils.isNotEmpty(name);
    }


    public LatLng toLatLng()
    {
        return new LatLng(this.lat, this.lon);
    }


    //
    // Stringifiable
    //


    @Override
    public Geo fromStr(
        String s)
    {
        if (s.startsWith("1|"))
        {
            // Version 1 serialization format
            String[] fields = s.split(SPLIT, 8);
            if (fields == null || (fields.length != 3 && fields.length != 8))
            {
                // Length must be either 3 (optional fields are omitted) or 8 (all data provided)
                throw new IllegalArgumentException("Invalid geo serialization \"" + s + "\"");
            }

            // The first field is version number which we have already checked
            this.lat = Double.valueOf(fields[1]);
            this.lon = Double.valueOf(fields[2]);
            if (fields.length > 3)
            {
                // Apparently, we provide extra info
                this.neighborhood = fields[3];
                this.city = fields[4];
                this.state = fields[5];
                this.country = fields[6];
                this.countryCode = fields[7];
            }
        }
        else if (s.startsWith("2|"))
        {
            // Version 2 serialization format
            String[] fields = s.split(SPLIT, 10);
            if (fields == null || (fields.length != 3 && fields.length != 10))
            {
                // Length must be either 3 (optional fields are omitted) or 10 (all data provided)
                throw new IllegalArgumentException("Invalid geo serialization \"" + s + "\"");
            }

            // The first field is version number which we have already checked
            this.lat = Double.valueOf(fields[1]);
            this.lon = Double.valueOf(fields[2]);
            if (fields.length > 3)
            {
                // Apparently, we provide extra info
                this.name = fields[3];
                this.neighborhood = fields[4];
                if (Strings.isNullOrEmpty(this.neighborhood))
                    this.neighborhood = fields[5];
                this.city = fields[6];
                this.state = fields[7];
                this.country = fields[8];
                this.countryCode = fields[9];
            }
        }
        else if (s.startsWith("3|"))
        {
            // Version 3 serialization format
            String[] fields = s.split(SPLIT, 10);
            if (fields == null || (fields.length != 3 && fields.length != 9))
            {
                // Length must be either 3 (optional fields are omitted) or 9 (all data provided)
                throw new IllegalArgumentException("Invalid geo serialization \"" + s + "\"");
            }

            // The first field is version number which we have already checked
            this.lat = Double.valueOf(fields[1]);
            this.lon = Double.valueOf(fields[2]);
            if (fields.length > 3)
            {
                // Apparently, we provide extra info
                this.name = fields[3];
                this.neighborhood = fields[4];
                this.city = fields[5];
                this.state = fields[6];
                this.country = fields[7];
                this.countryCode = fields[8];
            }
        }
        else
        {
//            // Could be the traditional form "latitude,longitude"
//            String[] fields = s.split(",", 2);
//            if (fields == null || fields.length !=2) {
//                throw new IllegalArgumentException("Invalid geo serialization \"" + s + "\"");
//            }
//            this.lat = Double.valueOf(fields[0]);
//            this.lon = Double.valueOf(fields[1]);
            throw new IllegalArgumentException("Invalid geo serialization \"" + s + "\"");
        }

        this.serializationStr = s;
        return this;
    }


    @Override
    public String toStr()
    {
        if (StringUtils.isNotEmpty(this.serializationStr))
            return this.serializationStr;

        StringBuilder sb = new StringBuilder();
        sb.append(VERSION);
        sb.append(JOIN);
        sb.append(this.lat);
        sb.append(JOIN);
        sb.append(this.lon);
        if (this.hasExtraInfo())
        {
            sb.append(JOIN);
            sb.append(this.name);
            sb.append(JOIN);
            sb.append(this.neighborhood);
            sb.append(JOIN);
            sb.append(this.city);
            sb.append(JOIN);
            sb.append(this.state);
            sb.append(JOIN);
            sb.append(this.country);
            sb.append(JOIN);
            sb.append(this.countryCode);
        }
        this.serializationStr = sb.toString();
        return this.serializationStr;
    }


    //
    // Getters
    //

    public String getCity()
    {
        return city;
    }


    public String getCountry()
    {
        return country;
    }


    public String getCountryCode()
    {
        return countryCode;
    }


    public double getLat()
    {
        return lat;
    }


    public double getLon()
    {
        return lon;
    }


    public String getName()
    {
        return name;
    }


    public String getNeighborhood()
    {
        return neighborhood;
    }


    public String getState()
    {
        return state;
    }


    //
    // Setters
    //
    
    public void setCity(String city)
    {
        this.city = city;
    }


    public void setName(
        String name)
    {
        this.name = name;
        this.serializationStr = null;
    }
    
    
    public void setNeighborhood(String neighborhood)
    {
        this.neighborhood = neighborhood;
    }
}
