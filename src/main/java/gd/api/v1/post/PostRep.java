
package gd.api.v1.post;


import gd.api.v1.comment.CommentRepList;
import gd.api.v1.user.UserRep;
import gd.impl.post.PostClass;
import gd.support.jersey.JsonSnakeCase;

import java.util.Date;
import java.util.List;


@JsonSnakeCase
public class PostRep
{
    public String body;

    /**
     * Populated only for web request,
     * {@link PostResource#getWebPost}
     */
    public CommentRepList comments;
    
    public PostClass clazz;

    public Date createTime;
    
    /**
     * Populated only if the post is returned in featured list
     */
    public String featuredTitle;

    public Geo geo;

    public Long groupId;

    public String groupName;

    public List<String> hashtags;

    public Long id;

    public Boolean isAnnouncement;

    public Boolean isBadReviewed;

    public Boolean isFake;

    public Long numComments;

    public Long numDownvotes;

    public Long numFlags;

    public Long numShares;

    public Long numUpvotes;

    public Long numViews;

    public UserRep owner;
    
    public Long threadId;
    
    /**
     * Additional info to display if the post is thread root
     */
    public String threadAnnotation;

    public PostType type;

    public Date updateTime;

    public VoteStatus voteStatus;


    //
    // Admin specific fields
    //

    public Boolean isBlocked;

    public Boolean isGeoRestricted;

    public String sortKey;

    public String sortVal;


    //
    // PostType specific fields
    //

    // AUDIO
    public MediaRep audio;

    public MediaRep audioBackground;

    // Link
    public MediaRep link;

    // SIMPLE, PHOTO
    public MediaRep photo;

    // TEXT
    public MediaRep text;

    // VIDEO
    public MediaRep video;

    public MediaRep videoThumbnail;


    public void clear()
    {
        body = null;
        comments = null;
        createTime = null;
        geo = null;
        hashtags = null;
        id = null;
        isAnnouncement = isBlocked = isFake = isGeoRestricted = null;
        numComments = numFlags = numShares = numViews = null;
        owner = null;
        type = null;
        updateTime = null;
        audio = audioBackground = link = photo = text = video = videoThumbnail = null;
    }
}
