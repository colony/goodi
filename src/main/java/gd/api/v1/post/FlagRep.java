package gd.api.v1.post;

import gd.support.jersey.JsonSnakeCase;

import java.util.Date;


@JsonSnakeCase
public class FlagRep
{
    public String complaintMessage;
    
    public Date createTime;
    
    public Long id;
    
    public Long postId;
    
    public Long userId;
}
