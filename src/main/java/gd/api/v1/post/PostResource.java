
package gd.api.v1.post;


import gd.support.jersey.BoolParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/posts")
@Singleton
public interface PostResource
{
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createPost(
        @Context HttpHeaders headers,
        @BeanParam PostForm postForm);


    @Path("/{id}")
    @DELETE
    @Produces("application/json")
    public Response deletePost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam);


    @Path("/{id}/flag")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response flagPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("complaint_message") String complaintMessage);


    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam);


    /**
     * Return the post data from web client, NOTE, the impl will
     * validate the web session token
     */
    @Path("/{id}/web")
    @GET
    @Produces("application/json")
    public Response getWebPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam);


    @Path("/{id}/like")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response likePost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("status") BoolParam statusParam);


    @Path("/{id}/star")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response starPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("num") @DefaultValue("0") LongParam numParam);


    @Path("/{id}/vote")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response votePost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("down") @DefaultValue("0") BoolParam isDownParam,
        @FormParam("up") @DefaultValue("0") BoolParam isUpParam,
        @FormParam("undown") @DefaultValue("0") BoolParam isUndownParam,
        @FormParam("unup") @DefaultValue("0") BoolParam isUnupParam);
}
