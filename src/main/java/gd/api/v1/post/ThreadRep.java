
package gd.api.v1.post;


import gd.support.jersey.JsonSnakeCase;

import java.util.List;


@JsonSnakeCase
public class ThreadRep
{
    public String actionTerm;

    /**
     * A list of post types that are allowed when creating children
     * posts for the thread
     */
    public List<String> allowedPostTypes;

    /**
     * ThreadType-specific serialized data, check ThreadType for
     * details
     */
    public String data;

    public Long id;

    public Long numChildren;

    /**
     * The displaying name of the thread (i.e. Game, etc)
     */
    public String title;

    public ThreadType type;
}
