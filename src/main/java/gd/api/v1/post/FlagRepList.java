package gd.api.v1.post;

import gd.api.v1.PaginationRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.List;


@JsonSnakeCase
public class FlagRepList
{
    public List<FlagRep> data;

    public PaginationRep pagination;


    public FlagRepList()
    {
        this.data = new ArrayList<>(); //Empty list
    }
    
    
    public void add(FlagRep post)
    {
        this.data.add(post);
    }
    
    
    public boolean hasData()
    {
        return !this.data.isEmpty();
    }
    
    
    public int getSize()
    {
        return this.data.size();
    }
}
