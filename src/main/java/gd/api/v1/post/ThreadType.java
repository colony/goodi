
package gd.api.v1.post;


public enum ThreadType
{
    /**
     * A card game thread that has following characteristics:
     * 
     * <pre>
     *   1. All posts are CARD type, root post is REQUEST, the children are RESPONSE,
     *      check CardMetadata for details
     *   2. The root post's owner could declare ONE winner, the winner info is stored
     *      in "data" field
     * </pre>
     * 
     * The data serialization format:
     * 
     * <pre>
     *   1|DECK_ID|OWNER_ID|WIN_POST_ID
     *   
     *   DECK_ID - id of deck from which cards are draw
     *   OWNER_ID - creator of the thread, owner of the root post
     *   WIN_POST_ID - id of win post, if not elected, 0 is set
     * </pre>
     */
    CARD_GAME,

    /**
     * A normal thread has following characteristics:
     * 
     * <pre>
     *   1. Fields like actionTerm, title, allowedPostTypes could be overwritten in metadata. 
     *   If they are not specified, defaults are used (check Reps#populateThread for details)
     * </pre>
     * 
     * The data serialization format:
     * 
     * <pre>
     *   1|ACTION_TERM|TITLE|ALLOWED_POST_TYPES
     *   
     *   ALLOWED_POST_TYPES - comma-separated PostType name
     * </pre>
     */
    NORMAL,

    ;
}
