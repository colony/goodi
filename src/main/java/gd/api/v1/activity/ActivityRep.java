
package gd.api.v1.activity;


import gd.support.jersey.JsonSnakeCase;

import java.util.Date;

import org.json.JSONObject;


@JsonSnakeCase
public class ActivityRep
{
    /**
     * The actual message displayed on client
     */
    public String body;

    public Date createTime;

    public Long id;

    public Boolean read;

    public ActivityType type;


    // Version 1

    /**
     * The data is a JSON string providing auxiliary information per
     * type. Please refer to ActivityType for details
     */
    public JSONObject data;


    // Version 2
    
    public Long actorId;
    
    // ID of related comment (if any)
    public Long commentId;

    // ID of related post (if any)
    public Long postId;
    
    // ID of related thread (if any)
    public Long threadId;
}
