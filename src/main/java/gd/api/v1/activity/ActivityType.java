
package gd.api.v1.activity;



public enum ActivityType
{
    /**
     * Someone (other than owner) comments on a post, the owner of
     * this post should receive this activity
     * 
     * <ul>
     * <li>actorId - the ID of commenter
     * <li>objectId - the ID of comment
     * <li>targetId - the ID of post
     * <li>extra[0] - comment text abbrev
     * <li>extra[1] - post text abbrev
     * </ul>
     */
    COMMENT_POST("cp"),

    /**
     * The thread owner declares a winner, all the participants and
     * winner will receive this activity
     * 
     * <ul>
     * <li>actorId - the ID of owner of thread
     * <li>objectId - the ID of winning post
     * <li>targetId - the ID of thread
     * <li>extra[0] - boolean, isWinner, determine if receiver of
     * the activity is the winner
     * <li>
     */
    DECLARE_WINNER("dw"),

    /**
     * Someone else also reply to the same post that you've replied
     * to. Other commenters (except the owner) should receive this
     * activity
     * 
     * <ul>
     * <li>actorId - the ID of commenter
     * <li>objectId - the ID of comment
     * <li>targetId - the ID of post
     * <li>extra[0] - comment text abbrev
     * <li>extra[1] - post text abbrev
     * </ul>
     */
    OTHER_COMMENT_POST("ocp"),

    /**
     * Someone else also participates the thread by creating post.
     * Other participants (except the owner) should receive this
     * activity
     * 
     * <ul>
     * <li>actorId - the ID of participant
     * <li>objectId - the ID of post
     * <li>targetId - the ID of thread
     * <li>extra[0] - thread type
     * </ul>
     */
    OTHER_PARTICIPATE_THREAD("opt"),

    /**
     * Someone (other than owner) participates the thread by
     * creating post. The owner of thread should receive this
     * activity
     * 
     * <ul>
     * <li>actorId - the ID of participant
     * <li>objectId - the ID of post
     * <li>targetId - the ID of thread
     * <li>extra[0] - thread type
     * </ul>
     */
    PARTICIPATE_THREAD("pt"),

    /**
     * Someone replies to your comment. The owner of the replied
     * comment should receive this activity. If the owner is also
     * the owner of the post, COMMENT_POST activity should be
     * created instead.
     * 
     * <ul>
     * <li>actorId - the ID of commenter
     * <li>objectId - the ID of replying comment
     * <li>targetId - the ID of replied comment
     * <li>extra[0] - the ID of post containing the comments
     * <li>extra[1] - replying comment text abbrev
     * <li>extra[2] - replied comment text abbrev
     * </ul>
     */
    REPLY_COMMENT("rc"),

    /**
     * A post gains some votes (both up and down). The owner of this
     * post should receive this activity
     * 
     * <ul>
     * <li>targetId - the ID of post be voted
     * <li>extra[0] - post text abbrev
     * </ul>
     */
    VOTE_POST("vp"),


    ;

    private final String abbrev;


    ActivityType(
        String abbrev)
    {
        this.abbrev = abbrev;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public static ActivityType fromAbbreviation(
        String abbrev)
    {
        for (ActivityType at : ActivityType.values()) {
            if (at.abbrev.equals(abbrev))
                return at;
        }
        throw new IllegalArgumentException("ActivityType contains no abbrev \"" + abbrev + "\"");
    }
}
