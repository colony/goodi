package gd.api.v1.activity;

import gd.api.v1.PaginationRep;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.List;


@JsonSnakeCase
public class ActivityRepList
{
    public List<ActivityRep> data;
    
    public PaginationRep pagination;
    
    
    public ActivityRepList()
    {
        this.data = new ArrayList<>(); //Empty list
    }
    
    
    public void add(ActivityRep activity)
    {
        this.data.add(activity);
    }
    
    
    public boolean hasData()
    {
        return !this.data.isEmpty();
    }
    
    
    public int getSize()
    {
        return this.data.size();
    }
}
