package gd.api.v1;

public enum ClientType
{
    ADMIN,
    
    ANDROID,

    CLI,

    IPHONE,

    MOBILE_WEB,

    /**
     * Unidentified. Missing header value of X-GD-CLIENT is
     * treated as unidentified.
     */
    UNIDENTIFIED,

    WEB,

    ;
}