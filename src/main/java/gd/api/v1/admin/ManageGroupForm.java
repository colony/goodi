package gd.api.v1.admin;

import gd.support.jersey.BoolParam;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;

public class ManageGroupForm
{
    @FormDataParam("cover_image_data")
    private InputStream coverImageStream;
    
    @FormDataParam("cover_image_url")
    private String coverImageUrl;
    
    @FormDataParam("id")
    private Long id;
    
    @FormDataParam("name")
    private String name;
    
    @FormDataParam("rule_image_url")
    private String ruleImageUrl;

    @FormDataParam("rule_text")
    private String ruleText;
    
    @FormDataParam("allowed_post_types")
    private String allowedPostTypes;
    
    @FormDataParam("thread_type")
    private String threadType;
    
    @FormDataParam("is_private")
    private BoolParam isPrivate;

    @FormDataParam("is_reversed")
    private BoolParam isReversed;
    
    
    public ManageGroupForm()
    {
    }

    
    //
    // Getters
    //

    public InputStream getCoverImageStream()
    {
        return coverImageStream;
    }

    
    public String getCoverImageUrl()
    {
        return coverImageUrl;
    }
    

    public Long getId()
    {
        return id;
    }


    public String getName()
    {
        return name;
    }


    public String getRuleImageUrl()
    {
        return ruleImageUrl;
    }


    public String getRuleText()
    {
        return ruleText;
    }


    public String getAllowedPostTypes()
    {
        return allowedPostTypes;
    }
    
    
    public String getThreadType()
    {
        return threadType;
    }
    
    
    public BoolParam getIsPrivate()
    {
        return isPrivate;
    }


    public BoolParam getIsReversed()
    {
        return isReversed;
    }
}
