package gd.api.v1.admin;

import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/adm/groups")
@Singleton
public interface ManageGroupResource
{
    @POST
    @Path("/assign")
    @Consumes("application/x-www-form-urlencoded")
    public Response assignGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @FormParam("post_id") LongParam postIdParam,
        @FormParam("group_id") LongParam groupIdParam);


    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createOrModifyGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam ManageGroupForm groupForm);
    
    
    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deleteGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam);
    

    @GET
    @Produces("application/json")
    public Response getGroupList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo);
}
