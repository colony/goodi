
package gd.api.v1.admin;


import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/caches")
@Singleton
public interface ManageCacheResource
{
    @Path("/{cache}")
    @DELETE
    public Response deleteEntry(
        @Context HttpHeaders headers,
        @PathParam("cache") String typeStr,
        @QueryParam("key") String key);


    @Path("/{cache}")
    @GET
    @Produces("text/plain")
    public Response getEntry(
        @Context HttpHeaders headers,
        @PathParam("cache") String typeStr,
        @QueryParam("key") String key);


    @Path("/{cache}")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response putEntry(
        @Context HttpHeaders headers,
        @PathParam("cache") String typeStr,
        @FormParam("key") String key,
        @FormParam("value") String value);
}
