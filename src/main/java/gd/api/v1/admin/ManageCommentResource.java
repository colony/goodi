
package gd.api.v1.admin;


import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/adm/comments")
@Singleton
public interface ManageCommentResource
{
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createOrModifyComment(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam ManageCommentForm commentForm);


    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deleteComment(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam);


    @GET
    @Produces("application/json")
    public Response getCommentList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam);
}
