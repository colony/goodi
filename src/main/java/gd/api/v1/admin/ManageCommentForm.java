
package gd.api.v1.admin;


import gd.api.v1.post.Geo;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;


/**
 * A form used to either create or modify a comment. If id is null,
 * this is a create form. Otherwise, it is update form. ONLY
 * not-null fields of the class will be used for insertion or
 * update. Also, some fields can only be inserted but not updated
 * (i.e. ownerId)
 * 
 */
public class ManageCommentForm
{
    @FormDataParam("body")
    private String body;

    @FormDataParam("geo")
    private Geo geo;

    @FormDataParam("id")
    private Long id;

    @FormDataParam("num_downvotes_delta")
    private Long numDownvotesDelta;

    @FormDataParam("num_upvotes_delta")
    private Long numUpvotesDelta;

    /**
     * Only needed as owner of a new comment that is created from
     * admin console
     */
    @FormDataParam("owner_id")
    private Long ownerId;

    @FormDataParam("parent_id")
    private Long parentId;

    @FormDataParam("photo_format")
    private String photoFormat;

    @FormDataParam("photo_height")
    private Integer photoHeight;

    @FormDataParam("photo_size")
    private Long photoSize;

    @FormDataParam("photo_data")
    private InputStream photoStream;

    @FormDataParam("photo_width")
    private Integer photoWidth;

    @FormDataParam("post_id")
    private Long postId;

    @FormDataParam("type")
    private String type;



    public ManageCommentForm()
    {
    }
    
    
    public boolean hasNumDownvotesDelta()
    {
        return numDownvotesDelta != null && numDownvotesDelta != 0L;
    }


    public boolean hasNumUpvotesDelta()
    {
        return numUpvotesDelta != null && numUpvotesDelta != 0L;
    }


    //
    // Getters
    //


    public String getBody()
    {
        return body;
    }


    public Geo getGeo()
    {
        return geo;
    }


    public Long getId()
    {
        return id;
    }


    public Long getNumDownvotesDelta()
    {
        return numDownvotesDelta;
    }


    public Long getNumUpvotesDelta()
    {
        return numUpvotesDelta;
    }


    public Long getOwnerId()
    {
        return ownerId;
    }


    public Long getParentId()
    {
        return parentId;
    }


    public String getPhotoFormat()
    {
        return photoFormat == null ? photoFormat : photoFormat.toUpperCase();
    }


    public Integer getPhotoHeight()
    {
        return photoHeight;
    }


    public Long getPhotoSize()
    {
        return photoSize;
    }


    public InputStream getPhotoStream()
    {
        return photoStream;
    }


    public Integer getPhotoWidth()
    {
        return photoWidth;
    }


    public Long getPostId()
    {
        return postId;
    }


    public String getType()
    {
        return type;
    }

}
