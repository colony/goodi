
package gd.api.v1.admin;


import gd.support.jersey.BoolParam;
import gd.support.jersey.IntParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/adm")
@Singleton
public interface AdminResource
{
    /**
     * Destroy old posts whose ID between post_id_low and
     * post_id_high and less than num_comments_high and less than
     * num_votes_high, only if is is FAKE 
     */
    @Path("/destroy")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response destroyOldPost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @FormParam("post_id_low") @DefaultValue("0") LongParam postIdLowParam,
        @FormParam("post_id_high") @DefaultValue("0") LongParam postIdHighParam,
        @FormParam("num_comments_high") @DefaultValue("0") IntParam numCmtsHighParam,
        @FormParam("num_upvotes_high") @DefaultValue("0") IntParam numUpvotesHighParam,
        @FormParam("only_fake") @DefaultValue("0") BoolParam onlyFakeParam,
        @FormParam("group_id") LongParam groupIdParam);


    @Path("/setting")
    @GET
    @Produces("text/plain")
    public Response getSystemSetting(
        @Context HttpHeaders headers,
        @QueryParam("key") String settingKey,
        @QueryParam("variant") String variant);


    @Path("/hc")
    @GET
    @Produces("text/plain")
    public Response healthCheck();


    /**
     * Any operation could be performed. The logic of this API could
     * be changed any time in need for random administrative
     * operation
     */
    @Path("/operate")
    @POST
    @Produces("text/plain")
    public Response operate(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        String arg)

        throws Exception;


    @Path("/setting")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response putSystemSetting(
        @Context HttpHeaders headers,
        @FormParam("key") String settingKey,
        @FormParam("variant") String variant,
        @FormParam("value") String value);


    @Path("/config/client/reload")
    @POST
    @Produces("text/plain")
    public Response reloadClientConfig(
        @Context HttpHeaders headers);


    /**
     * Reload configuration by specifying the config class name.
     * NOTE, it must be reloadable
     */
    @Path("/config/system/reload")
    @POST
    @Produces("text/plain")
    public Response reloadSystemConfig(
        @Context HttpHeaders headers,
        @QueryParam("name") String name);


    @Path("/template/reload")
    @POST
    @Produces("text/plain")
    public Response reloadTemplate(
        @Context HttpHeaders headers);


    @Path("/mode")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response setMode(
        @Context HttpHeaders headers,
        @FormParam("offline") BoolParam offlineParam,
        @FormParam("shutdown") BoolParam shutdownParam);
}
