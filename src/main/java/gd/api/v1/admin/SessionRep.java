
package gd.api.v1.admin;


import gd.impl.notification.NotificationToken;
import gd.support.jersey.JsonSnakeCase;

import java.util.ArrayList;
import java.util.Date;


@JsonSnakeCase
public class SessionRep
{
    public Date createTime;
    
    public NotificationToken notificationToken;
    
    public Long ownerId;
    
    public String token;
    
    public Date updateTime;


    public static class List
        extends
            ArrayList<SessionRep>
    {
        private static final long serialVersionUID =
            -2998193478752344984L;
    }
}
