
package gd.api.v1.admin;


import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/sched")
@Singleton
public interface ManageSchedulerResource
{
    @Path("/queues/cancel")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response cancelRunJob(
        @Context HttpHeaders headers,
        @FormParam("q") String queueName);


    @Path("/queues")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response createQueue(
        @Context HttpHeaders headers,
        @FormParam("q") String queueName);


    @Path("/queues/{q}")
    @DELETE
    @Produces("text/plain")
    public Response deleteQueue(
        @Context HttpHeaders headers,
        @PathParam("q") String queueName);


    @Path("/queues/{q}")
    @GET
    @Produces("text/plain")
    public Response getQueueReport(
        @Context HttpHeaders headers,
        @PathParam("q") String queueName);


    @GET
    @Produces("text/plain")
    public Response getSchedulerSummary(
        @Context HttpHeaders headers);


    /**
     * Update the schedule of a job. Different combination of
     * parameters could lead to various operation such as, moving
     * queue, new schedule and even unschedule a job
     * 
     * @param jobId
     *            ID of the job
     * @param queueName
     *            If update the schedule of job but stay on same
     *            queue, the value should be the current queue name
     *            that job sits on. If move the specified job to a
     *            different queue, the value should be that queue
     *            name. Note, if the queue name doesn't, new queue
     *            will be also created
     * @param schedStr
     *            If set, a new schedule will be set regardless of
     *            at current queue or new queue (depends on if
     *            newQueueName is set). If unset/empty, the job will
     *            be UNSCHEDULED regardless of newQueueName is set
     *            or not
     * @return
     */
    @Path("/jobs")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response updateJobSchedule(
        @Context HttpHeaders headers,
        @FormParam("id") String jobId,
        @FormParam("q") String queueName,
        @FormParam("sched") String schedStr);
}
