
package gd.api.v1.admin;


import gd.impl.util.PageParam;
import gd.support.jersey.BoolParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/indexes")
@Singleton
public interface ManageIndexResource
{
    /**
     * Add an entry to the specified index. If the entry already
     * exists, it could be UPDATED
     */
    @Path("/{index}")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response addEntry(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @FormParam("entry") String entryStr,
        @FormParam("allow_update") @DefaultValue("0") BoolParam allowUpdateIfExistParam);


    @Path("/{index}/{id}")
    @DELETE
    public Response deleteEntry(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @PathParam("id") String idStr);


    @Path("/flush")
    @POST
    public Response flush(
        @Context HttpHeaders headers);


    @Path("/flush/{index}")
    @POST
    public Response flush(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName);


    @Path("/{index}/{id}")
    @GET
    @Produces("text/plain")
    public Response getEntry(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @PathParam("id") String idStr);


    /**
     * Return entries of the specified index, taking account into
     * pagination
     */
    @Path("/{index}")
    @GET
    @Produces("text/plain")
    public Response getIndex(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @BeanParam PageParam pageParam);


    @Path("/rebuild")
    @POST
    public Response rebuild(
        @Context HttpHeaders headers);


    @Path("/rebuild/{index}")
    @POST
    public Response rebuild(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName);
}
