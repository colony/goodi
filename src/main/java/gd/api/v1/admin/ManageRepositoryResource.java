
package gd.api.v1.admin;


import gd.impl.repository.RepositoryType;

import javax.inject.Singleton;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/repositories")
@Singleton
public interface ManageRepositoryResource
{
    @Path("/export")
    @POST
    @Produces("text/plain")
    public Response exportRepository(
        @Context HttpHeaders headers,
        @FormParam("repo_type") RepositoryType repoType,
        @FormParam("file_name") String fileName);


    @Path("/import")
    @POST
    @Produces("text/plain")
    public Response importRepository(
        @Context HttpHeaders headers,
        @FormParam("repo_type") RepositoryType repoType,
        @FormParam("file_name") String fileName);

}
