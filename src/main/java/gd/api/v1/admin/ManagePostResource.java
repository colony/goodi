
package gd.api.v1.admin;


import gd.api.v1.post.Geo;
import gd.impl.post.PostStatus;
import gd.impl.util.PageParam;
import gd.support.jersey.BoolParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/adm/posts")
@Singleton
public interface ManagePostResource
{
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createOrModifyPost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam ManagePostForm postForm);


    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deletePost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam);


    /**
     * Delete posts from the specified group, but excluding the post
     * ID if any
     * 
     * @param excludeIdsStr
     *            A comma-separated list of post IDs, nullable
     */
    @DELETE
    @Path("/group/{id}")
    @Produces("application/json")
    public Response deletePostsByGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam groupIdParam,
        @QueryParam("exclude_ids") String excludeIdsStr);


    @GET
    @Path("/flags")
    @Produces("application/json")
    public Response getFlagList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam);


    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getPost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam);


    @GET
    @Path("/{id}/comments")
    @Produces("application/json")
    public Response getPostCommentList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @BeanParam PageParam pageParam);


    @GET
    @Path("/{id}/flags")
    @Produces("application/json")
    public Response getPostFlagList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam);


    @GET
    @Produces("application/json")
    public Response getPostList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("geo") Geo geo,
        @QueryParam("group_id") LongParam groupIdParam,
        @BeanParam PageParam pageParam);


    @POST
    @Path("/{id}/status")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response setPostStatus(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @FormParam("status") PostStatus.Status status,
        @FormParam("unset") @DefaultValue("false") BoolParam unsetParam);
}
