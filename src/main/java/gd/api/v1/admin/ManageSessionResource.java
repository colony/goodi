package gd.api.v1.admin;

import gd.support.jersey.BoolParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/sessions")
@Singleton
public interface ManageSessionResource
{
    @Path("/{token}")
    @DELETE
    public Response close(
        @Context HttpHeaders headers,
        @PathParam("token") String token);
    
    
    @GET
    @Produces("application/json")
    public Response getAllSessions(
        @Context HttpHeaders headers,
        @QueryParam("user_id") LongParam userIdParam,
        @QueryParam("is_open") BoolParam isOpenParam);
}
