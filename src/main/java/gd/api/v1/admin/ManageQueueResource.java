
package gd.api.v1.admin;


import gd.impl.queue.QueueType;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/queue")
@Singleton
public interface ManageQueueResource
{
    @GET
    @Produces("text/plain")
    public Response getQueueSummary(
        @Context HttpHeaders headers);


    @Path("/{id}/start")
    @POST
    @Produces("text/plain")
    public Response startQueue(
        @Context HttpHeaders headers,
        @PathParam("id") QueueType id);


    @Path("/{id}/stop")
    @POST
    @Produces("text/plain")
    public Response stopQueue(
        @Context HttpHeaders headers,
        @PathParam("id") QueueType id);
}
