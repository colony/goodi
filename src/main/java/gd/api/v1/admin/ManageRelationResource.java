
package gd.api.v1.admin;


import gd.impl.relation.RelationType;
import gd.impl.util.PageParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/relations")
@Singleton
public interface ManageRelationResource
{
    @Path("/{relation}/s/{sid}")
    @DELETE
    public Response deleteSource(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType type,
        @PathParam("sid") String sourceId);


    @Path("/{relation}/s/{sid}/e/{did}")
    @DELETE
    public Response deleteEdge(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType type,
        @PathParam("sid") String sid,
        @PathParam("did") String did);


    @Path("/export")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response exportRelation(
        @Context HttpHeaders headers,
        @FormParam("relation") RelationType r,
        @FormParam("fname") String fileName);


    @Path("/{relation}/s/{sid}/e/{did}")
    @GET
    @Produces("text/plain")
    public Response getEdge(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @PathParam("sid") String sourceId,
        @PathParam("did") String dstId);


    @Path("/{relation}/s/{sid}/e")
    @GET
    @Produces("text/plain")
    public Response getEdgeList(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @PathParam("sid") String sourceId,
        @BeanParam PageParam pageParam);


    @Path("/{relation}/s/{sid}")
    @GET
    @Produces("text/plain")
    public Response getSource(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @PathParam("sid") String sourceId);


    @Path("/{relation}/s")
    @GET
    @Produces("text/plain")
    public Response getSourceList(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @BeanParam PageParam pageParam);


    @Path("/import")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response importRelation(
        @Context HttpHeaders headers,
        @FormParam("relation") RelationType r,
        @FormParam("fname") String fileName);
}
