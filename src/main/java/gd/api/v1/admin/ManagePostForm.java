
package gd.api.v1.admin;


import gd.api.v1.post.Geo;
import gd.api.v1.post.ThreadType;
import gd.impl.util.GDUtils;
import gd.support.jersey.BoolParam;
import gd.support.jersey.DateTimeParam;

import java.io.InputStream;
import java.util.Date;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataParam;


/**
 * A form used to either create or modify a post. If id is null,
 * this is a create form, Otherwise, it is update form.
 * <p>
 * Only not-null fields of the class will be used for insertion or
 * update. Also, some fields can only be inserted but not updated
 * (i.e. ownerId)
 * <p>
 * The counter (likes, flags, shares) are delta value that is the
 * amount will be increment/decrement from original value
 */
public class ManagePostForm
{
    @FormDataParam("body")
    private String body;

    @FormDataParam("create_time")
    private DateTimeParam createTime;

    @FormDataParam("geo")
    private Geo geo;

    @DefaultValue("1")
    @FormDataParam("group_id")
    private Long groupId;

    @FormDataParam("id")
    private Long id;

    @FormDataParam("num_downvotes_delta")
    private Long numDownvotesDelta;

    @FormDataParam("num_flags_delta")
    private Long numFlagsDelta;

    @FormDataParam("num_shares_delta")
    private Long numSharesDelta;

    @FormDataParam("num_upvotes_delta")
    private Long numUpvotesDelta;

    @FormDataParam("owner_id")
    private Long ownerId;

    @FormDataParam("type")
    private String type;


    //
    // Thread related
    //
    @FormDataParam("create_thread_data")
    private String createThreadData;

    /**
     * ThreadType#name, if set, it will create a thread post with
     * the specified type. FOR NOW, only NORMAL thread is supported
     */
    @FormDataParam("create_thread_type")
    private String createThreadType;

    /**
     * If set, the post is created as child of a thread of the
     * specified id
     */
    @FormDataParam("thread_id")
    private Long threadId;


    //
    // Featured related
    //
    /**
     * If set, we will assign the post to featured list (Relations
     * ANYTHING_FEATURED_FEED), and also associate it with a
     * featured title
     */
    @DefaultValue("0")
    @FormDataParam("is_featured")
    private BoolParam isFeatured;

    /**
     * For certain featured title (especially for thread root post),
     * it contains java format symbol
     */
    @DefaultValue("0")
    @FormDataParam("is_featured_title_format")
    private BoolParam isFeaturedTitleFormat;

    @FormDataParam("featured_title")
    private String featuredTitle;


    //
    // Audio
    //
    /**
     * Audio duration in ms
     */
    @FormDataParam("audio_duration")
    private Long audioDuration;

    @FormDataParam("audio_format")
    private String audioFormat;

    @FormDataParam("audio_size")
    private Long audioSize;

    @FormDataParam("audio_data")
    private InputStream audioStream;


    //
    // Link
    //
    @FormDataParam("link_data")
    private String link;

    @FormDataParam("link_title")
    private String linkTitle;

    @FormDataParam("link_embed_format")
    private String linkEmbedItemFormat;

    @FormDataParam("link_embed_height")
    private Integer linkEmbedItemHeight;

    @FormDataParam("link_embed_width")
    private Integer linkEmbedItemWidth;

    @FormDataParam("link_embed_url")
    private String linkEmbedItemUrl;


    //
    // Photo
    //
    @FormDataParam("photo_format")
    private String photoFormat;

    @FormDataParam("photo_height")
    private Integer photoHeight;

    @FormDataParam("photo_size")
    private Long photoSize;

    @FormDataParam("photo_data")
    private InputStream photoStream;

    @FormDataParam("photo_width")
    private Integer photoWidth;

    @FormDataParam("photo_uploaded")
    @DefaultValue("0")
    private BoolParam photoUploaded;


    //
    // Text
    //
    @FormDataParam("text_data")
    private String text;

    @FormDataParam("text_format")
    private String textFormat;


    //
    // Video
    //
    @FormDataParam("video_format")
    private String videoFormat;

    @FormDataParam("video_data")
    private InputStream videoStream;

    @FormDataParam("video_size")
    private Long videoSize;


    public ManagePostForm()
    {
    }


    //
    // Getters
    //


    public InputStream getAudioStream()
    {
        return audioStream;
    }


    public Long getAudioDuration()
    {
        return audioDuration;
    }


    public String getAudioFormat()
    {
        return audioFormat;
    }


    public Long getAudioSize()
    {
        return audioSize;
    }


    public String getBody()
    {
        return body;
    }


    public Date getCreateTime()
    {
        if (createTime == null)
            return null;
        return createTime.get().toDate();
    }


    public Geo getGeo()
    {
        return geo;
    }


    public Long getGroupId()
    {
        return groupId;
    }


    public Long getId()
    {
        return id;
    }


    public Long getNumDownvotesDelta()
    {
        return numDownvotesDelta;
    }


    public Long getNumFlagsDelta()
    {
        return numFlagsDelta;
    }


    public Long getNumSharesDelta()
    {
        return numSharesDelta;
    }


    public Long getNumUpvotesDelta()
    {
        return numUpvotesDelta;
    }


    public Long getOwnerId()
    {
        return ownerId;
    }


    public String getPhotoFormat()
    {
        return photoFormat == null ? photoFormat : photoFormat.toUpperCase();
    }


    public Integer getPhotoHeight()
    {
        return photoHeight;
    }


    public Long getPhotoSize()
    {
        return photoSize;
    }


    public InputStream getPhotoStream()
    {
        return photoStream;
    }


    public BoolParam getPhotoUploaded()
    {
        return photoUploaded;
    }


    public Integer getPhotoWidth()
    {
        return photoWidth;
    }


    public String getText()
    {
        return text;
    }


    public String getTextFormat()
    {
        return textFormat;
    }


    public Long getThreadId()
    {
        return threadId;
    }


    public String getType()
    {
        return type;
    }


    public String getLink()
    {
        return link;
    }


    public String getLinkTitle()
    {
        return linkTitle;
    }


    public String getLinkEmbedItemFormat()
    {
        return linkEmbedItemFormat;
    }


    public Integer getLinkEmbedItemHeight()
    {
        return linkEmbedItemHeight;
    }


    public Integer getLinkEmbedItemWidth()
    {
        return linkEmbedItemWidth;
    }


    public String getLinkEmbedItemUrl()
    {
        return linkEmbedItemUrl;
    }


    public String getVideoFormat()
    {
        return videoFormat;
    }


    public InputStream getVideoStream()
    {
        return videoStream;
    }


    public Long getVideoSize()
    {
        return videoSize;
    }


    public BoolParam getIsFeatured()
    {
        return isFeatured;
    }
    
    
    public BoolParam getIsFeaturedTitleFormat()
    {
        return isFeaturedTitleFormat;
    }


    public String getFeaturedTitle()
    {
        return featuredTitle;
    }


    public String getCreateThreadData()
    {
        return createThreadData;
    }


    public String getCreateThreadType()
    {
        return createThreadType;
    }


    //
    // Validators
    //


    public boolean hasDelta()
    {
        return this.hasNumDownvotesDelta() ||
            this.hasNumFlagsDelta() ||
            this.hasNumSharesDelta() ||
            this.hasNumUpvotesDelta();
    }


    public boolean hasNumDownvotesDelta()
    {
        return numDownvotesDelta != null && numDownvotesDelta != 0L;
    }


    public boolean hasNumFlagsDelta()
    {
        return numFlagsDelta != null && numFlagsDelta != 0L;
    }


    public boolean hasNumSharesDelta()
    {
        return numSharesDelta != null && numSharesDelta != 0L;
    }


    public boolean hasNumUpvotesDelta()
    {
        return numUpvotesDelta != null && numUpvotesDelta != 0L;
    }


    public void validate()
    {
        if (this.isFeatured.get()) {
            if (StringUtils.isEmpty(this.featuredTitle)) {
                GDUtils.check(false, Status.BAD_REQUEST, "FeaturedTitle is required to create featured post");
            }
        }

        if (this.createThreadType != null) {
            boolean b = ThreadType.NORMAL == ThreadType.valueOf(this.createThreadType);
            GDUtils.check(b, Status.BAD_REQUEST, "Allow NORMAL thread post allowed to create");
            GDUtils.check(
                StringUtils.isNotEmpty(this.createThreadData),
                Status.BAD_REQUEST,
                "Thread data is required to create a thread post");
        }
    }
}
