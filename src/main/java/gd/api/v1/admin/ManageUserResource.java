
package gd.api.v1.admin;


import gd.impl.user.UserStatus;
import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/adm/users")
@Singleton
public interface ManageUserResource
{
    @POST
    @Path("/fake")
    @Produces("application/json")
    public Response createFakeUser(
        @Context HttpHeaders headers);
    
    
    @GET
    @Path("/{id}/comments")
    @Produces("application/json")
    public Response getUserCommentList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @BeanParam PageParam pageParam);


    @GET
    @Produces("application/json")
    public Response getUserList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("status") UserStatus.Status statusFilter,
        @BeanParam PageParam pageParam);
    
    
    @GET
    @Path("/{id}/posts")
    @Produces("application/json")
    public Response getUserPostList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @BeanParam PageParam pageParam);
}
