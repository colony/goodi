
package gd.api.v1;




/**
 * Enum describing different device model per {@link ClientType}
 */
public enum ClientDevice
{  
    /**
     * IPHONE FAMILY
     */
    IPHONE,
    IPHONE_3G,
    IPHONE_3GS,
    IPHONE_4,
    IPHONE_4S,
    IPHONE_5,
    IPHONE_5C,
    IPHONE_5S,
    IPHONE_6,
    IPHONE_6PLUS,
    
    /**
     * IPOD FAMILY
     */
    IPOD_1G,
    IPOD_2G,
    IPOD_3G,
    IPOD_4G,
    IPOD_5G,
    
    /**
     * IPAD FAMILY
     */
    IPAD,
    IPAD2,
    IPAD3,
    IPAD4,
    IPAD_MINI,
    IPAD_MINI_RETINA,
    IPAD_MINI3,
    IPAD_AIR,
    IPAD_AIR2,
    
    /**
     * Other
     */
    UNIDENTIFIED,

    ;
}
