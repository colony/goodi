
package gd.impl;


import gd.api.v1.ClientDevice;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.session.SessionManager;
import gd.impl.session.SessionManager.InvalidSessionException;
import gd.impl.user.User;
import gd.impl.user.Users;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;


@Provider
@PreMatching
@Priority(3)
// Request filer execute in ascending order
public class ThrottleHackFilter
    implements
        ContainerRequestFilter
{
    @Override
    public void filter(
        ContainerRequestContext reqCtx)
        throws IOException
    {
        if (!GDLifecycle.isOffline())
        {
            if (!Configs.getBool(SystemSetting.THROTTLE_HACK_FILTER_ENABLED, false))
                return;
            
            String relativeUrl = StringUtils.stripStart(reqCtx.getUriInfo().getPath(), "/");
            if (relativeUrl.startsWith("account") || relativeUrl.startsWith("client"))
                return;

            String clientHeader = reqCtx.getHeaders().getFirst("X-GD-CLIENT");
            if (clientHeader == null || !clientHeader.equals("IPHONE:1"))
                return;

            String authHeader = reqCtx.getHeaders().getFirst("Authorization");
            if (Strings.isNullOrEmpty(authHeader))
                return;
            
            String token = Rests.retrieveSessionTokenFromAuthHeader(authHeader);
            long userId = 0L;
            try
            {
                userId = SessionManager.instance().validateSession(token);
            }
            catch (InvalidSessionException e)
            {
                throw new IOException(e);
            }

            if (userId > 0L)
            {
                User user = Users.get(userId);

                ClientDevice device = user.getDevice();
                if (device == ClientDevice.IPHONE_4 || 
                    device == ClientDevice.IPHONE_4S ||
                    device == ClientDevice.IPOD_3G ||
                    device == ClientDevice.IPOD_4G)
                {
                    if (relativeUrl.startsWith("feed")) {
                        UriInfo uriInfo = reqCtx.getUriInfo();
                        MultivaluedMap<String,String> qp = uriInfo.getQueryParameters();
                        String key = Rests.getValue(qp, "_k", "");
                        if (key.equals("p")) {
                            reqCtx.abortWith(
                                Response.status(Status.OK)
                                .header("Content-Type", "application/json")
                                .build());
                            return;
                        }
                    }
                    
                    reqCtx.abortWith(
                        Response.status(Status.BAD_REQUEST).build());
                }
            }
        }
    }
}
