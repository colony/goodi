
package gd.impl.media;


import org.apache.commons.lang3.StringUtils;

import gd.api.v1.post.MediaType;
import gd.impl.util.GDUtils;


/**
 * For all dimension int value (i.e. largeHeight), 0 means value
 * unset (NULL)
 */
public class PhotoMedia
    extends
        Media
{
    private static final String SPLIT = "\\|";

    private static final int VERSION = 2;



    /**
     * Supported photo compression format
     */
    public static enum Format
    {
        JPG("image/jpeg"),
        PNG("image/png"),
        GIF("image/gif");

        private final String ctype;


        Format(
            String ctype)
        {
            this.ctype = ctype;
        }


        /**
         * Convert to Content-Type HTTP header value
         */
        public String getContentType()
        {
            return ctype;
        }
    }


    private Format format;

    private String large;

    private int largeHeight;

    private int largeWidth;

    private String medium;

    private int mediumHeight;

    private int mediumWidth;

    private String thumbnail;

    private int thumbnailHeight;

    private int thumbnailWidth;

    /**
     * True if the photo media is uploaded from client. Otherwise,
     * it is shot by camera
     */
    private boolean uploaded;


    public PhotoMedia()
    {
    }


    /**
     * Copy constructor
     */
    public PhotoMedia(
        PhotoMedia pm)
    {
        this.format = pm.format;
        this.large = pm.large;
        this.largeHeight = pm.largeHeight;
        this.largeWidth = pm.largeWidth;
        this.medium = pm.medium;
        this.mediumHeight = pm.mediumHeight;
        this.mediumWidth = pm.mediumWidth;
        this.thumbnail = pm.thumbnail;
        this.thumbnailHeight = pm.thumbnailHeight;
        this.thumbnailWidth = pm.thumbnailWidth;
        this.uploaded = pm.uploaded;
    }


    public MediaType getType()
    {
        return MediaType.PHOTO;
    }


    /**
     * Figure out the format from url's extension, return null if we
     * can't figure it out
     */
    public static Format guessFormatFromUrl(
        String url)
    {
        String ext = GDUtils.getFileExtension(url);
        if (StringUtils.isNotEmpty(ext)) {
            ext = ext.toLowerCase();
            if (ext.equals("gif"))
                return PhotoMedia.Format.GIF;
            else if (ext.equals("jpg") || ext.equals("jpeg"))
                return PhotoMedia.Format.JPG;
            else if (ext.equals("png"))
                return PhotoMedia.Format.PNG;
        }
        
        return null;
    }


    //
    // Stringifiable
    //


    @Override
    public Media fromStr(
        String s)
    {
        // Proceed according to the VERSION
        if (s.startsWith("1|")) {
            // Version 1
            String[] tokens = s.split(SPLIT, 11);
            if (tokens == null || tokens.length != 11)
                throw new IllegalArgumentException(
                    "Invalid serialized string \"" + s + "\"");

            // Skip first field since it is version
            this.format = Format.valueOf(tokens[1]);
            this.large = tokens[2];
            this.largeHeight = Integer.parseInt(tokens[3]);
            this.largeWidth = Integer.parseInt(tokens[4]);
            this.medium = tokens[5];
            this.mediumHeight = Integer.parseInt(tokens[6]);
            this.mediumWidth = Integer.parseInt(tokens[7]);
            this.thumbnail = tokens[8];
            this.thumbnailHeight = Integer.parseInt(tokens[9]);
            this.thumbnailWidth = Integer.parseInt(tokens[10]);
            this.uploaded = false;
        }
        else if (s.startsWith("2|")) {
            // Version 2
            String[] tokens = s.split(SPLIT, 12);
            if (tokens == null || tokens.length != 12)
                throw new IllegalArgumentException(
                    "Invalid serialized string \"" + s + "\"");

            // Skip first field since it is version
            this.format = Format.valueOf(tokens[1]);
            this.large = tokens[2];
            this.largeHeight = Integer.parseInt(tokens[3]);
            this.largeWidth = Integer.parseInt(tokens[4]);
            this.medium = tokens[5];
            this.mediumHeight = Integer.parseInt(tokens[6]);
            this.mediumWidth = Integer.parseInt(tokens[7]);
            this.thumbnail = tokens[8];
            this.thumbnailHeight = Integer.parseInt(tokens[9]);
            this.thumbnailWidth = Integer.parseInt(tokens[10]);
            this.uploaded = Integer.parseInt(tokens[11]) == 1 ? true : false;
        }
        // Other version goes here
        return this;
    }


    @Override
    public String toStr()
    {
        // There might be unexpected char in url (large, medium, thumbnail)
        // It is much safer to split with control char
        return String.format(
            "%d|%s|%s|%d|%d|%s|%d|%d|%s|%d|%d|%d",
            VERSION,
            format.name(),
            large == null ? "" : large,
            largeHeight,
            largeWidth,
            medium == null ? "" : medium,
            mediumHeight,
            mediumWidth,
            thumbnail == null ? "" : thumbnail,
            thumbnailHeight,
            thumbnailWidth,
            uploaded ? 1 : 0);
    }


    //
    // Getters & Setters
    //

    public Format getFormat()
    {
        return format;
    }


    public void setFormat(
        Format format)
    {
        this.format = format;
    }


    public String getLarge()
    {
        return large;
    }


    public void setLarge(
        String large)
    {
        this.large = large;
    }


    public int getLargeHeight()
    {
        return largeHeight;
    }


    public void setLargeHeight(
        int largeHeight)
    {
        this.largeHeight = largeHeight;
    }


    public int getLargeWidth()
    {
        return largeWidth;
    }


    public void setLargeWidth(
        int largeWidth)
    {
        this.largeWidth = largeWidth;
    }


    public String getMedium()
    {
        return medium;
    }


    public void setMedium(
        String medium)
    {
        this.medium = medium;
    }


    public int getMediumHeight()
    {
        return mediumHeight;
    }


    public void setMediumHeight(
        int mediumHeight)
    {
        this.mediumHeight = mediumHeight;
    }


    public int getMediumWidth()
    {
        return mediumWidth;
    }


    public void setMediumWidth(
        int mediumWidth)
    {
        this.mediumWidth = mediumWidth;
    }


    public String getThumbnail()
    {
        return thumbnail;
    }


    public void setThumbnail(
        String thumbnail)
    {
        this.thumbnail = thumbnail;
    }


    public int getThumbnailHeight()
    {
        return thumbnailHeight;
    }


    public void setThumbnailHeight(
        int thumbnailHeight)
    {
        this.thumbnailHeight = thumbnailHeight;
    }


    public int getThumbnailWidth()
    {
        return thumbnailWidth;
    }


    public void setThumbnailWidth(
        int thumbnailWidth)
    {
        this.thumbnailWidth = thumbnailWidth;
    }


    public boolean isUploaded()
    {
        return uploaded;
    }


    public void setUploaded(
        boolean uploaded)
    {
        this.uploaded = uploaded;
    }
}
