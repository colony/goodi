
package gd.impl.media;


import gd.api.v1.post.MediaType;


public class LinkMedia
    extends
        Media
{
    private static final String SPLIT = "\u0002";

    private static final int VERSION = 1;



    /**
     * Supported embedded item format
     */
    public static enum Format
    {
        // The link embeds no media item
        NONE,
        
        // The link embeds a gif
        GIF,

        // The link embeds an image
        IMAGE,

        // The link embeds a YOUTUBE video
        // If it is Youtube, the embedItemUrl contains youtube id, and embedItemWidth and embedItemHeight
        // contains video hqdefaults thumbnail dimension
        YOUTUBE,
        
        ;
    }


    private Format embedItemFormat;

    private int embedItemHeight;

    private int embedItemWidth;

    private String embedItemUrl;

    private String description;

    private String host;

    private String title;

    private String url;


    public LinkMedia()
    {
    }


    /**
     * Copy constructor
     */
    public LinkMedia(
        LinkMedia lm)
    {
        this.embedItemFormat = lm.embedItemFormat;
        this.embedItemUrl = lm.embedItemUrl;
        this.embedItemHeight = lm.embedItemHeight;
        this.embedItemWidth = lm.embedItemWidth;
        this.description = lm.description;
        this.host = lm.host;
        this.title = lm.title;
        this.url = lm.url;
    }


    public MediaType getType()
    {
        return MediaType.LINK;
    }


    //
    // Stringifiable
    //

    @Override
    public Media fromStr(
        String s)
    {
        // Proceed according to the VERSION
        if (s.startsWith("1\u0002")) {
            // Version 1
            String[] tokens = s.split(SPLIT);
            if (tokens == null || (tokens.length != 9 && tokens.length != 6)) {
                throw new IllegalArgumentException("Invalid serialzied string \"" + s + "\"");
            }

            int i = 1;
            // Skip first field since it is version
            this.embedItemFormat = Format.valueOf(tokens[i++]);
            if (this.embedItemFormat != Format.NONE) {
                this.embedItemUrl = tokens[i++];
                this.embedItemHeight = Integer.parseInt(tokens[i++]);
                this.embedItemWidth = Integer.parseInt(tokens[i++]);
            }
            this.description = tokens[i++];
            this.host = tokens[i++];
            this.title = tokens[i++];
            this.url = tokens[i++];
        }
        // Other version goes here
        return this;
    }


    @Override
    public String toStr()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(VERSION);
        sb.append(SPLIT);
        sb.append(embedItemFormat.name());
        sb.append(SPLIT);

        if (this.embedItemFormat != Format.NONE) {
            sb.append(embedItemUrl);
            sb.append(SPLIT);
            sb.append(embedItemHeight);
            sb.append(SPLIT);
            sb.append(embedItemWidth);
            sb.append(SPLIT);
        }

        sb.append(description == null ? "" : description);
        sb.append(SPLIT);
        sb.append(host);
        sb.append(SPLIT);
        sb.append(title == null ? "" : title);
        sb.append(SPLIT);
        sb.append(url);

        return sb.toString();
    }


    //
    // Getters & Setters
    //

    public Format getEmbedItemFormat()
    {
        return embedItemFormat;
    }


    public void setEmbedItemFormat(
        Format embedItemFormat)
    {
        this.embedItemFormat = embedItemFormat;
    }


    public String getEmbedItemUrl()
    {
        return embedItemUrl;
    }


    public void setEmbedItemUrl(
        String embedItemUrl)
    {
        this.embedItemUrl = embedItemUrl;
    }


    public int getEmbedItemHeight()
    {
        return embedItemHeight;
    }


    public void setEmbedItemHeight(
        int embedItemHeight)
    {
        this.embedItemHeight = embedItemHeight;
    }


    public int getEmbedItemWidth()
    {
        return embedItemWidth;
    }


    public void setEmbedItemWidth(
        int embedItemWidth)
    {
        this.embedItemWidth = embedItemWidth;
    }


    public String getDescription()
    {
        return description;
    }


    public void setDescription(
        String description)
    {
        this.description = description;
    }


    public String getHost()
    {
        return host;
    }


    public void setHost(
        String host)
    {
        this.host = host;
    }


    public String getTitle()
    {
        return title;
    }


    public void setTitle(
        String title)
    {
        this.title = title;
    }


    public String getUrl()
    {
        return url;
    }


    public void setUrl(
        String url)
    {
        this.url = url;
    }
}
