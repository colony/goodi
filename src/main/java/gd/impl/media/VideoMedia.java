
package gd.impl.media;

import gd.api.v1.post.MediaType;


public class VideoMedia extends Media
{
    private static final String SPLIT = "\\|";

    private static final int VERSION = 1;


    public static enum Format
    {
        MOV("video/quicktime", "mov"),
        
        MP4("video/mp4", "mp4"),
        
        M4V("video/x-m4v", "m4v"),
        
        ;
        
        private final String ctype;

        private final String ext;


        Format(
            String ctype,
            String ext)
        {
            this.ctype = ctype;
            this.ext = ext;
        }


        public String getContentType()
        {
            return ctype;
        }


        public String getExtension()
        {
            return ext;
        }
    }
    
    private Format format;
    
    private String url;
    
    
    public VideoMedia()
    {
    }
    
    
    /**
     * Copy constructor
     */
    public VideoMedia(VideoMedia vm)
    {
        this.format = vm.format;
        this.url = vm.url;
    }
    
    
    public MediaType getType()
    {
        return MediaType.VIDEO;
    }
    
    
    //
    // Stringifiable
    //
    
    @Override
    public Media fromStr(String s)
    {
        // Proceed according to the VERSION
        if (s.startsWith("1|")) {
            // Version 1
            String[] tokens = s.split(SPLIT, 3);
            if (tokens == null || tokens.length != 3)
                throw new IllegalArgumentException(
                    "Invalid serialized string \"" + s + "\"");
            
            // Skip first field since it is version
            this.format = Format.valueOf(tokens[1]);
            this.url = tokens[2];
        }
        // Other version goes here
        return this;
    }
    
    
    @Override
    public String toStr()
    {
        return String.format(
            "%d|%s|%s",
            VERSION,
            format.name(),
            url);
    }
    
    
    //
    // Getters & Setters
    //
    
    public Format getFormat()
    {
        return format;
    }
    
    
    public void setFormat(Format format)
    {
        this.format = format;
    }
    
    
    public String getUrl()
    {
        return url;
    }
    
    
    public void setUrl(String url)
    {
        this.url = url;
    }
}
