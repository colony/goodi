package gd.impl.media;

import gd.api.v1.post.MediaType;
import gd.impl.util.Stringifiable;

public abstract class Media implements Stringifiable<Media>
{
    public abstract MediaType getType();
}
