
package gd.impl.media;


import org.apache.commons.lang3.StringUtils;

import gd.api.v1.post.MediaType;


public class IconMedia
    extends
        Media
{
    private static final String SPLIT = "\\|";

    private static final int VERSION = 1;


    private String systemId;

    private String colorHex;

    private String url;


    public IconMedia()
    {
    }


    /**
     * Copy constructor
     */
    public IconMedia(
        IconMedia im)
    {
        this.systemId = im.systemId;
        this.colorHex = im.colorHex;
        this.url = im.url;
    }



    @Override
    public MediaType getType()
    {
        return MediaType.ICON;
    }


    //
    // Stringifiable
    //

    @Override
    public Media fromStr(
        String s)
    {
        // Proceed according to the VERSION
        if (s.startsWith("1|"))
        {
            // Version 1
            String[] tokens = s.split(SPLIT, 4);
            if (tokens == null || tokens.length != 4)
                throw new IllegalArgumentException(
                    "Invalid serialized string \"" + s + "\"");

            // Skip first field since it is version
            this.systemId = tokens[1];
            this.colorHex = tokens[2];
            this.url = tokens[3];
            
            if (StringUtils.isEmpty(this.url)) {
                this.url = Medias.generateIconUrl(this.systemId);
            }
        }
        // Other version goes here

        return this;
    }


    @Override
    public String toStr()
    {
        return String.format(
            "%d|%s|%s|%s",
            VERSION,
            StringUtils.defaultString(systemId),
            StringUtils.defaultString(colorHex),
            StringUtils.defaultString(url));
    }


    //
    // Getters & Setters
    //

    public String getSystemId()
    {
        return systemId;
    }


    public void setSystemId(
        String systemId)
    {
        this.systemId = systemId;
    }


    public String getColorHex()
    {
        return colorHex;
    }


    public void setColorHex(
        String colorHex)
    {
        this.colorHex = colorHex;
    }


    public String getUrl()
    {
        return url;
    }


    public void setUrl(
        String url)
    {
        this.url = url;
    }
}
