
package gd.impl.media;


import gd.api.v1.post.MediaType;


public class TextMedia
    extends
        Media
{
    /**
     * The text will contains MOST kinds of printable character. To
     * serialization, we need non-printable control char. Also, we
     * need different than the SPLIT used by post media list. Check
     * {@link Post} for details.
     */
    private static final String SPLIT = "\u0002";

    private static final int VERSION = 1;



    /**
     * Supported text format
     */
    public static enum Format
    {
        PLAIN, ;
    }


    private Format format;

    private String text;

    private String textBackgroundColor;

    private String textColor;


    public TextMedia()
    {
    }


    /**
     * Copy constructor
     */
    public TextMedia(
        TextMedia tm)
    {
        this.format = tm.format;
        this.text = tm.text;
        this.textBackgroundColor = tm.textBackgroundColor;
        this.textColor = tm.textColor;
    }


    public MediaType getType()
    {
        return MediaType.TEXT;
    }


    //
    // Stringifiable
    //


    @Override
    public Media fromStr(
        String s)
    {
        // Proceed according to the VERSION
        if (s.startsWith("1\u0002"))
        {
            // Version 1
            String[] tokens = s.split(SPLIT, 5);
            if (tokens == null || tokens.length != 5)
                throw new IllegalArgumentException(
                    "Invalid serialized string \"" + s + "\"");

            // Skip first field since it is version
            this.format = Format.valueOf(tokens[1]);
            this.text = tokens[2];
            this.textBackgroundColor = tokens[3];
            this.textColor = tokens[4];
        }
        // Other version goes here
        return this;
    }


    @Override
    public String toStr()
    {
        return String.format(
            "%d%s%s%s%s%s%s%s%s",
            VERSION,
            SPLIT,
            format.name(),
            SPLIT,
            text,
            SPLIT,
            textBackgroundColor,
            SPLIT,
            textColor);
    }


    //
    // Getters & Setters
    //


    public Format getFormat()
    {
        return format;
    }


    public void setFormat(
        Format format)
    {
        this.format = format;
    }


    public String getText()
    {
        return text;
    }


    public void setText(
        String text)
    {
        this.text = text;
    }


    public String getTextBackgroundColor()
    {
        return textBackgroundColor;
    }


    public void setTextBackgroundColor(
        String textBackgroundColor)
    {
        this.textBackgroundColor = textBackgroundColor;
    }


    public String getTextColor()
    {
        return textColor;
    }


    public void setTextColor(
        String textColor)
    {
        this.textColor = textColor;
    }
}
