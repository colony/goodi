
package gd.impl.media;


import static java.nio.charset.StandardCharsets.UTF_8;
import gd.api.v1.post.MediaType;
import gd.impl.ComponentConfig;
import gd.impl.config.Configs;
import gd.impl.config.GlobalProperties;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.util.GDUtils;
import gd.impl.util.InitializationError;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;
import com.google.common.io.LineProcessor;
import com.google.common.util.concurrent.Striped;


public final class Medias
{
    public static final String[] AUDIO_BACK_COLOR_HEXES = {
            "2dcb71", "ef4836", "22a7f0", "22313f", "f7ca18", "f89406", "6c7a89", "8e44ad", "f62459"
    };

    public static final String[] AUDIO_BACK_SYSTEM_IDS = {
            "audio_back_duck", "audio_back_monkey", "audio_back_ninja", "audio_back_star", "audio_back_temple"
    };

    public static final String[] COMMENTER_SYSTEM_IDS = {
            "AngryNinja", "Duck", "HappyNinja", "MonkeyFace", "Ninja", "NinjaStar", "Bee", "Cat", "Dragon",
            "FishBones", "Lotus", "Seahorse", "Sushi"
    };

    public static final String[] COMMENTER_COLOR_HEXES = {
            "ff1829", "ff8518", "5cbb77", "9618ff", "ff4bde", "919191", "f3c51e", "1088fe", "363636"
    };


    private static final String ICON_FILE = "icon";

    private static final Logger LOG = LoggerFactory.getLogger(Medias.class);

    /**
     * This is the delimiter used to serialize array of Media to a
     * string form. It is an non-printable control character. All
     * Media classes who implement @{link Stringfiable} interface
     * must choose a different delimiter
     */
    private static final String MEDIA_LIST_SPLIT = "\u0001";

    private static final String STATIC_MEDIA_UPDATENOW_PICTURE = "static/updatenow3.png";

    private static String _baseMediaUrl;

    private static LLRelation _hasCommenterRel;

    private static List<IconPoolSet> _poolSetList;

    private static Striped<Lock> _stripedLocks;

    private static volatile PhotoMedia _updateNowPictureMedia;


    public static Media deserializeMedia(
        String str)
    {
        if (StringUtils.isEmpty(str))
            return null;
        
        // The first two letter is MediaType initial
        String initial = str.substring(0, 2);
        MediaType type = MediaType.fromInitial(initial);
        Media media = null;
        switch (type)
        {
        case AUDIO:
            media = new AudioMedia();
            break;
        case ICON:
            media = new IconMedia();
            break;
        case LINK:
            media = new LinkMedia();
            break;
        case PHOTO:
            media = new PhotoMedia();
            break;
        case TEXT:
            media = new TextMedia();
            break;
        case VIDEO:
            media = new VideoMedia();
            break;
        }
        media.fromStr(str.substring(2));
        return media;
    }


    public static List<Media> deserializeMediaList(
        long postId,
        String str)
    {
        if (StringUtils.isEmpty(str))
            return null;

        List<Media> medias = new ArrayList<>();
        String[] tokens = str.split(MEDIA_LIST_SPLIT);
        if (tokens.length == 1) {
            // There is only ONE media
            Media m = deserializeMedia(tokens[0]);
            medias.add(m);
            return medias;
        }

        // The first one is number
        int num = Integer.parseInt(tokens[0]);
        if (num != tokens.length - 1)
            throw new IllegalArgumentException("The media str \"" + str + "\" is incorrect for postID " + postId);

        for (int i = 1; i < tokens.length; i++) {
            medias.add(deserializeMedia(tokens[i]));
        }
        return medias;
    }


    public static String getRandomAudioBackColorHex()
    {
        int i = RandomUtils.nextInt(0, AUDIO_BACK_COLOR_HEXES.length);
        return AUDIO_BACK_COLOR_HEXES[i];
    }


    public static IconMedia generateCommenterIconMedia(
        long postId,
        long commenterId)
    {
        LEntry node = _hasCommenterRel.getEdge(postId, commenterId);
        if (node == null)
        {
            boolean wrapAround = false;
            // Round-robin get a lock from the stripe
            Lock lock = _stripedLocks.get(postId);
            lock.lock();
            try
            {
                IconPoolSet poolSet = null;
                // This is the (n+1)th commenter
                int n = (int) _hasCommenterRel.getNumEdges(postId);
                
                // 
                for (int i = _poolSetList.size() - 1; i >= 0; i--) {
                    poolSet = _poolSetList.get(i);
                    if (poolSet.canSupportPostId(postId)) {
                        break;
                    }
                }

                wrapAround = n >= poolSet.size();

                // Returned string in format of "x|y", where x is index in SYSTEM_IDs and y is
                // index in SYSTEM_COLOR_HEXs
                String iconInfo = poolSet.getCandidate(postId, n + 1);
                int x = Integer.parseInt(iconInfo.split("\\|")[0]);
                int y = Integer.parseInt(iconInfo.split("\\|")[1]);

                IconMedia im = new IconMedia();
                im.setSystemId(COMMENTER_SYSTEM_IDS[x]);
                im.setColorHex(COMMENTER_COLOR_HEXES[y]);

                LEntry newNode =
                    new LEntry(commenterId).with(n + 1).with(im.toStr());
                _hasCommenterRel.addEdge(postId, newNode);
                node = newNode;
            }
            finally
            {
                lock.unlock();
            }

            if (wrapAround) {
                LOG.warn(
                    "Commenter icon wrap around for postId {} commenterId {}",
                    postId,
                    commenterId);
            }
        }
        return (IconMedia) new IconMedia().fromStr(node.s1);
    }
    
    
    public static String generateIconUrl(String iconSystemId)
    {
        //TODO: configurable
        return "https://s3.amazonaws.com/media.stealthie.io/static/icon/" + iconSystemId;
    }


    public static void init()
    {
        ComponentConfig cfg = Configs.getComponentConfig();
        _baseMediaUrl = cfg.getMedia().getMediaBaseUrl();

        RelationManager relMgr = RelationManager.instance();
        _hasCommenterRel =
            relMgr.get(RelationType.HAS_COMMENTER, LLRelation.class);

        _stripedLocks = Striped.lazyWeakLock(10);

        // Initialize icon pool from file
        Path iconFilePath =
            GlobalProperties.instance()
                .resourceDir()
                .resolve(ICON_FILE);
        if (!java.nio.file.Files.exists(iconFilePath))
            throw new InitializationError(
                "Cannot find icon file in resources folder");

        _poolSetList = new ArrayList<>();
        try
        {
            Files.readLines(
                iconFilePath.toFile(),
                UTF_8,
                new LineProcessor<Void>() {
                    @Override
                    public boolean processLine(
                        String line)
                        throws IOException
                    {
                        // Each line could be
                        // 1. comment line, start with '#', skip
                        // 2. empty/blank line, skip
                        // 3. an array of x|y separated by comma, x is idx in IconMedia.SYSTEM_IDS and y is idx in IconMedia.SYSTEM_COLOR_HEXS

                        if (StringUtils.isNotEmpty(line))
                        {
                            if (line.startsWith("#"))
                                return true; // comment, skip to next line

                            if (line.startsWith("Pool=")) {
                                Pattern pattern = Pattern.compile("^Pool=([0-9]+),([0-9]+)");
                                Matcher m = pattern.matcher(line);
                                if (m.matches()) {
                                    int version = Integer.parseInt(m.group(1));
                                    int minPostId = Integer.parseInt(m.group(2));
                                    IconPoolSet poolSet = new IconPoolSet(version, minPostId);
                                    _poolSetList.add(poolSet);
                                }
                                else {
                                    throw new InitializationError("Illegal syntax in icon file");
                                }
                                return true;
                            }

                            // Apparently, this is a permutation, insert into last seen poolSet
                            _poolSetList.get(_poolSetList.size() - 1).addPool(line.split(","));
                        }
                        return true;
                    }


                    @Override
                    public Void getResult()
                    {
                        return null;
                    }
                });
        }
        catch (IOException e)
        {
            throw new InitializationError(
                "Fail to read icon file",
                e);
        }
    }


    public static PhotoMedia makeUpdateNowPicture()
    {
        if (_updateNowPictureMedia == null)
        {
            PhotoMedia pm = new PhotoMedia();
            pm.setFormat(PhotoMedia.Format.PNG);
            pm.setLarge(GDUtils.joinPath(_baseMediaUrl, STATIC_MEDIA_UPDATENOW_PICTURE));
            pm.setLargeHeight(320);
            pm.setLargeWidth(320);
            _updateNowPictureMedia = pm;
        }
        return _updateNowPictureMedia;
    }


    public static PhotoMedia makeFixNowPicture()
    {
        PhotoMedia pm = new PhotoMedia();
        pm.setFormat(PhotoMedia.Format.PNG);
        pm.setLarge(GDUtils.joinPath(_baseMediaUrl, "static/fixnow.png"));
        pm.setLargeHeight(320);
        pm.setLargeWidth(320);
        return pm;
    }


    public static String resolveHostFromUrl(
        String url)
    {
        try {
            URL aUrl = new URL(url);
            String host = aUrl.getHost();
            if (host.startsWith("www."))
                host = host.substring(4);
            return host;
        }
        catch (MalformedURLException e) {
            LOG.error("Malformed URL {}", url);

            // URL is invalid, let's try out best guess the host from it, otherwise return itself
            if (url.contains("/")) {
                url = url.split("/")[0];
            }
            if (url.startsWith("www.")) {
                url = url.substring(4);
            }
            return url;
        }
    }


    public static String resolveMediaUrl(
        String mediaUrl)
    {
        String mediaUrlLower = mediaUrl.toLowerCase();
        if (mediaUrlLower.startsWith("http:")
            || mediaUrlLower.startsWith("https:"))
        {
            return mediaUrl;
        }
        return GDUtils.joinPath(_baseMediaUrl, mediaUrl);
    }
    
    
    public static String serializeMedia(Media media)
    {
        if (media == null)
            return null;
        
        // The format
        // <type2LetterInitial><mediaStr>
        StringBuilder sb = new StringBuilder();
        sb.append(media.getType().getTwoLetterInitial());
        sb.append(media.toStr());
        return sb.toString();
    }


    /**
     * Serialize a list of media to string form
     */
    public static String serializeMediaList(
        List<Media> medias)
    {
        if (medias == null || medias.isEmpty())
            return null;

        int n = medias.size();

        // The format, <SP> stands for one split(delimiter)
        // number<SP>type2LetterInitialmediaStr1<SP>type2LetterInitialmediaStr2
        // if there is only ONE media, we don't prepend "number<SP>"
        StringBuilder sb = new StringBuilder();
        if (n == 1) {
            sb.append(medias.get(0).getType().getTwoLetterInitial());
            sb.append(medias.get(0).toStr());
        }
        else {
            sb.append(n);
            for (Media m : medias) {
                sb.append(MEDIA_LIST_SPLIT);
                sb.append(m.getType().getTwoLetterInitial());
                sb.append(m.toStr());
            }
        }
        return sb.toString();
    }



    //
    // INTERNAL CLASS
    //

    /**
     * A set of pool, which each pool contains a fixed number of
     * combination of icons and colors
     */
    private static class IconPoolSet
    {
        long minPostId;

        List<String[]> pools;


        public IconPoolSet(
            int version,
            long minPostId)
        {
            this.minPostId = minPostId;
            this.pools = new ArrayList<>();
        }


        public void addPool(
            String[] pool)
        {
            this.pools.add(pool);
        }


        /**
         * Check if the icon pool's minimum supported post ID is
         * less or equal to the specified one
         */
        public boolean canSupportPostId(
            long postId)
        {
            return this.minPostId <= postId;
        }


        /**
         * Randomize and return a candidate (color and icon) for
         * postId and nth commenter in that post, nth starts with 1
         */
        public String getCandidate(
            long postId,
            int nth)
        {
            String[] pool = this.pools.get((int) (postId % this.pools.size()));
            // NOTE, if nth is too large, we have to modulo, that means the icons
            // generated is wrapped around, and duplicated
            return pool[(nth - 1) % pool.length];
        }


        /**
         * Returns the total number of combination in this pool
         */
        public int size()
        {
            return pools.get(0).length;
        }
    }
}
