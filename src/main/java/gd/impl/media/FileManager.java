
package gd.impl.media;


import gd.api.v1.post.MediaType;
import gd.impl.ComponentConfig.MediaConfig;
import gd.impl.ComponentConfig.MediaMode;
import gd.impl.config.Configs;
import gd.impl.config.GlobalProperties;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


public class FileManager
{
    protected Path baseDir;

    protected Random random;


    FileManager()
    {
        GlobalProperties gp = GlobalProperties.instance();
        this.baseDir = gp.mediaDir();
        this.random = new Random();
    }
    
    
    public String computeRandomKey()
    {
        long num = this.random.nextLong();
        return String.format(
            "%02x/%02x/%02x/%010x",
            (num >> 56) & 0xFF,
            (num >> 48) & 0xFF,
            (num >> 40) & 0xFF,
            num & 0xFFFFFFFFFFL);
    }
   

    public boolean delete(
        String key)

        throws IOException
    {
        if (Strings.isNullOrEmpty(key))
            return false;

        Path filePath = this.baseDir.resolve(key);
        return Files.deleteIfExists(filePath);
    }


    public boolean deleteQuietly(
        String key)
    {
        if (Strings.isNullOrEmpty(key))
            return false;

        try
        {
            return delete(key);
        }
        catch (IOException e)
        {
            LOG.warn("Fail to delete file with key=" + key
                + ", suppress the exception to make it quiet", e);
            return false;
        }
    }


    /**
     * Returns the full URL for the given key, note the URL may
     * include different protocol per impl (i.e. Local fileManager
     * uses file://, while s3 uses https://)
     */
    public String getFullUrl(
        String key)
    {
        return "file://" + this.baseDir.resolve(key).toString();
    }


    /**
     * 
     * @param stream
     *            InputStream providing the data
     * @param type
     *            MediaType enum, used to calculate the path/folder
     * @param contentType
     *            Mime type
     * @param contentSize
     *            Size in bytes
     * @param appendedKey
     *            Arbitrary string attached at end of generated key
     *            (path). It is useful to provide additional info on
     *            path. (i.e. path/to/file/aabbcc_WxH)
     * @param extension
     *            File extension
     * @return
     * @throws IOException
     */
    public String store(
        InputStream stream,
        MediaType type,
        String contentType,
        long contentSize,
        String appendedKey,
        String extension)

        throws IOException
    {
        Path filePath;
        String key;
        synchronized (this)
        {
            // Generating a non-duplicate file key (path)
            do
            {
                long num = this.random.nextLong();
                key = String.format(
                    "%s/%02x/%02x/%02x/%010x",
                    type.name().toLowerCase(),
                    (num >> 56) & 0xFF,
                    (num >> 48) & 0xFF,
                    (num >> 40) & 0xFF,
                    num & 0xFFFFFFFFFFL);

                if (!Strings.isNullOrEmpty(appendedKey))
                {
                    key = key + appendedKey;
                }

                if (!Strings.isNullOrEmpty(extension))
                {
                    key = key + "." + extension;
                }

                filePath = this.baseDir.resolve(key);
            } while (Files.exists(filePath));

            // Create parent directories if necessary
            Path parent = filePath.getParent();
            if (Files.notExists(parent))
            {
                Files.createDirectories(parent);
            }

            // Create an empty file to reserve the key
            Files.createFile(filePath);
        }

        long size =
            Files.copy(
                stream,
                filePath,
                StandardCopyOption.REPLACE_EXISTING);
        LOG.info("Copied " + size + " bytes into file " + filePath);

        // Return URI
        return key;
    }


    /**
     * Use with caution. If a file with the same key exists, the one
     * will be override.
     * 
     * @param stream
     * @param type
     * @param key
     */
    public void storeWithKey(
        InputStream stream,
        String contentType,
        String key)

        throws IOException
    {
        Path filePath = this.baseDir.resolve(key);

        // Create parent directories if necessary
        Path parent = filePath.getParent();
        if (Files.notExists(parent))
        {
            Files.createDirectories(parent);
        }

        long size =
            Files.copy(
                stream,
                filePath,
                StandardCopyOption.REPLACE_EXISTING);
        LOG.info("Copied " + size + " bytes into file " + filePath);
    }



    //
    // SINGLETON MANAGEMENT
    //


    private static class SingletonHolder
    {
        private static final FileManager INSTANCE;

        static
        {
            MediaConfig mc = Configs.getComponentConfig().getMedia();
            MediaMode mode = mc.getMode();
            if (mode == MediaMode.LOCAL)
            {
                INSTANCE = new FileManager();
            }
            else
            {
                INSTANCE = new S3FileManager();
            }
        }
    }


    public static FileManager instance()
    {
        return SingletonHolder.INSTANCE;
    }


    // A logger to use for debugging
    private static final Logger LOG =
        LoggerFactory.getLogger(FileManager.class);
}
