
package gd.impl.media;


import gd.api.v1.post.MediaType;
import gd.impl.ComponentConfig;
import gd.impl.ComponentConfig.AWSConfig;
import gd.impl.config.Configs;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.common.base.Strings;


public class S3FileManager
    extends
        FileManager
{
    private static final Logger LOG =
        LoggerFactory.getLogger(S3FileManager.class);

    private AmazonS3Client client = null;

    private String bucket;


    S3FileManager()
    {
        LOG.info("S3FileManager starting...");
        ComponentConfig cc = Configs.getComponentConfig();

        AWSConfig awsCfg = cc.getAWS();
        BasicAWSCredentials creds =
            new BasicAWSCredentials(
                awsCfg.getAccessKey(),
                awsCfg.getSecretKey());

        this.client = new AmazonS3Client(creds);
        this.client.setEndpoint(cc.getMedia().getS3Endpoint());
        this.bucket = cc.getMedia().getS3Bucket();

        LOG.info("S3FileManager started");
    }
    
    
    @Override
    public String computeRandomKey()
    {
        return UUID.randomUUID().toString();
    }


    @Override
    public boolean delete(
        String key)
        throws IOException
    {
        if (Strings.isNullOrEmpty(key))
            return false;
        
        this.client.deleteObject(new DeleteObjectRequest(
            this.bucket,
            key));
        LOG.debug("S3 delete key {}", key);
        return true;
    }


    @Override
    public boolean deleteQuietly(
        String key)
    {
        try
        {
            return delete(key);
        }
        catch (IOException e)
        {
            LOG.warn(
                "Fail to delete S3 key {}, but suppress the exception to make it quiet",
                key,
                e);
            return false;
        }
    }


    @Override
    public String getFullUrl(
        String key)
    {
        return this.client.getResourceUrl(this.bucket, key);
    }


    @Override
    public String store(
        InputStream stream,
        MediaType type,
        String contentType,
        long contentSize,
        String appendedKey,
        String extension)
        throws IOException
    {
        String key =
            type.name().toLowerCase() + "/" + UUID.randomUUID().toString();
        if (StringUtils.isNotEmpty(appendedKey))
        {
            key += appendedKey;
        }
        if (StringUtils.isNotEmpty(extension))
        {
            key += "." + extension;
        }

        ObjectMetadata om = new ObjectMetadata();
        if (contentSize > 0) {
            om.setContentLength(contentSize);
        }
        if (StringUtils.isNotEmpty(contentType)) {
            om.setContentType(contentType);
        }

        this.client.putObject(new PutObjectRequest(
            this.bucket,
            key,
            stream,
            om).withCannedAcl(CannedAccessControlList.PublicRead));

        LOG.debug("S3 put key {} in bucket {}", key, bucket);
        return key;
    }


    @Override
    public void storeWithKey(
        InputStream stream,
        String contentType,
        String key)
        throws IOException
    {
        ObjectMetadata om = new ObjectMetadata();
        if (StringUtils.isNotEmpty(contentType)) {
            om.setContentType(contentType);
        }
        
        this.client.putObject(new PutObjectRequest(
            this.bucket, 
            key, 
            stream, 
            om).withCannedAcl(CannedAccessControlList.PublicRead));
        
        LOG.debug("S3 put key {} in bucket {}", key, bucket);
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final S3FileManager INSTANCE =
            new S3FileManager();
    }


    public static S3FileManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
