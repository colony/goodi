
package gd.impl.media;


import gd.api.v1.post.MediaType;


public class AudioMedia
    extends
        Media
{
    private static final String SPLIT = "\\|";

    private static final int VERSION = 1;



    /**
     * Supported audio
     */
    public static enum Format
    {
        M4A("audio/m4a", "m4a"),
        MP3("audio/mp3", "mp3"),
        WAV("audio/wav", "wav");

        private final String ctype;
        private final String ext;


        Format(
            String ctype,
            String ext)
        {
            this.ctype = ctype;
            this.ext = ext;
        }


        /**
         * Convert to Content-Type HTTP header value
         */
        public String getContentType()
        {
            return ctype;
        }
        
        
        public String getExtension()
        {
            return ext;
        }
    }


    private Format format;

    /** milliseconds */
    private long duration;

    private String url;


    public AudioMedia()
    {
    }
    
    
    public AudioMedia(AudioMedia am)
    {
        this.format = am.format;
        this.duration = am.duration;
        this.url = am.url;
    }


    public MediaType getType()
    {
        return MediaType.AUDIO;
    }
    
    
    //
    // Stringifiable
    //
    
    @Override
    public Media fromStr(String s)
    {
        // Proceed according to the VERSION
        if (s.startsWith("1|"))
        {
            // Version 1
            String[] tokens = s.split(SPLIT, 4);
            if (tokens == null || tokens.length != 4)
                throw new IllegalArgumentException(
                    "Invalid serialized string \"" + s + "\"");
            
            // Skip first field since it is version
            this.format = Format.valueOf(tokens[1]);
            this.duration = Long.parseLong(tokens[2]);
            this.url = tokens[3];
        }
        // Other version goes here
        
        return this;
    }
    
    
    @Override
    public String toStr()
    {
        return String.format(
            "%d|%s|%d|%s",
            VERSION,
            format.name(),
            duration,
            url);
    }


    //
    // Getters & Setters
    //
    
    
    public Format getFormat()
    {
        return format;
    }


    public void setFormat(
        Format format)
    {
        this.format = format;
    }


    public long getDuration()
    {
        return duration;
    }


    public void setDuration(
        long duration)
    {
        this.duration = duration;
    }


    public String getUrl()
    {
        return url;
    }


    public void setUrl(
        String url)
    {
        this.url = url;
    }
}
