
package gd.impl.config;


import gd.impl.ComponentConfig;
import gd.impl.config.GlobalProperties.ENV;
import gd.impl.repository.RepositoriesConfig;
import gd.impl.repository.RepositoryConfig;
import gd.impl.util.GDUtils;
import gd.impl.util.InitializationError;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;

import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;


public class ConfigManager
{
    private static final Logger LOG = LoggerFactory.getLogger(ConfigManager.class);
    
    private Path baseDir;

    private Map<Class<?>,Object> configurations;


    private ConfigManager()
    {
        // Recursively look for all @Configured class
        ClassPath cp;
        try
        {
            cp =
                ClassPath.from(Thread.currentThread()
                    .getContextClassLoader());
        }
        catch (IOException e)
        {
            throw new InitializationError(e);
        }

        GlobalProperties gp = GlobalProperties.instance();
        ObjectMapper mapper = new ObjectMapper();
        YAMLFactory yaml = new YAMLFactory();

        /**
         * If a program is running on a web application server such
         * as JBoss and Tomcat, the ClassPool object may not be able
         * to find user classes since such a web application server
         * uses multiple class loaders as well as the system class
         * loader. In that case, an additional class path must be
         * registered to the ClassPool
         */
        ClassPool pool = ClassPool.getDefault();
        pool.insertClassPath(new ClassClassPath(this.getClass()));

        this.configurations = Maps.newHashMap();
        this.baseDir = gp.configDir();
        Set<ClassInfo> clazz =
            cp.getTopLevelClassesRecursive("gd.impl");
        for (ClassInfo ci : clazz)
        {
            try
            {
                // Use javassit to check Annotation w/o loading the class
                CtClass cc = pool.get(ci.getName());
                if (cc.getAnnotation(Configured.class) != null)
                {
                    cc.detach();

                    Class<?> configClass = ci.load();
                    Object cfg =
                        parse(configClass, mapper, yaml, gp.env());
                    this.configurations.put(configClass, cfg);
                }
            }
            catch (Exception e)
            {
                throw new InitializationError(e);
            }
        }

        pool = null; // Release the pool
    }


    public <T> T get(
        Class<T> clazz)
    {
        Object cfg = this.configurations.get(clazz);
        if (cfg == null)
            return null;

        return GDUtils.cast(cfg);
    }


    public <T> void reload(
        Class<T> clazz)
    {
        T cfg = get(clazz);
        if (cfg == null)
        {
            LOG.warn("Try to reload nonexistent config {}", clazz.getName());
            return;
        }
        if (!(cfg instanceof Reloadable))
        {
            return;
        }

        try
        {
            GlobalProperties gp = GlobalProperties.instance();
            T newCfg =
                parse(
                    clazz,
                    new ObjectMapper(),
                    new YAMLFactory(),
                    gp.env());
            Reloadable<T> oldCfg = GDUtils.cast(cfg);
            oldCfg.reload(newCfg);
        }
        catch (Exception e)
        {
            LOG.error("Fail to reload config {}", clazz.getName(), e);
        }
    }


    /**
     * Validate important non-default configuration that can't be
     * missed at boot time of lifecycle
     * 
     * @throw RuntimException If validation not pass
     */
    public void validate()
    {
        ComponentConfig cc = get(ComponentConfig.class);
        if (!cc.validate())
            throw new RuntimeException("Fail to validate component config");
        
        RepositoriesConfig rsCfg = get(RepositoriesConfig.class);
        for (RepositoryConfig rcfg : rsCfg.getRepositories()) {
            if (!rcfg.validate())
                throw new RuntimeException("Fail to validate repository config " + rcfg.getId());
        }
    }


    //
    // INTERNAL METHODS
    //

    private <T> T parse(
        Class<T> clazz,
        ObjectMapper mapper,
        YAMLFactory yaml,
        ENV env)

        throws Exception
    {
        Configured ann =
            clazz.getAnnotation(Configured.class);

        String fname = ann.fname();
        String ext = Files.getFileExtension(fname);

        // For now, only yml is supported. Otherwise, return a default instance
        if (!"yml".equals(ext))
        {
            // log warn
            return clazz.newInstance();
        }
        else
        {
            // Based on the host environment, we will first lookup
            // config.yml.ENV then config.yml
            boolean found = false;
            Path filePath = null;
            String[] lookups =
            {
                    fname + "." + env.name(),
                    fname + "." + env.name().toLowerCase(),
                    fname
            };

            for (int i = 0; !found && i < lookups.length; ++i)
            {
                filePath = this.baseDir.resolve(lookups[i]);
                found = java.nio.file.Files.exists(filePath);
            }

            if (!found)
            {
                // log warn
                return clazz.newInstance();
            }

            // If we make here, file is located, parse YAML now
            try (InputStream input =
                java.nio.file.Files.newInputStream(filePath))
            {
                JsonNode node =
                    mapper.readTree(yaml.createParser(input));
                return mapper.readValue(
                    new TreeTraversingParser(node),
                    clazz);
            }
        }
    }



    /**
     * SingletonHolder is loaded on the first execution of
     * ConfigManager.instance() or the first access to
     * SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder
    {
        private static final ConfigManager INSTANCE =
            new ConfigManager();
    }


    public static ConfigManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
