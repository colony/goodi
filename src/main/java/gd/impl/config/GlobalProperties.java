
package gd.impl.config;


import gd.impl.util.InitializationError;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.common.base.Strings;


public final class GlobalProperties
{
    private ENV env;

    private Path home;


    private GlobalProperties()
    {
        this.setupEnv();
        this.setupHome();
    }


    public Path archiveDir()
    {
        return homeDir().resolve("archive");
    }
    
    
    public Path cacheDir()
    {
        return homeDir().resolve("cache");
    }


    public Path configDir()
    {
        return homeDir().resolve("config");
    }


    public Path dataDir()
    {
        return homeDir().resolve("data");
    }


    public ENV env()
    {
        return env;
    }


    public Path indexDir()
    {
        return homeDir().resolve("index");
    }


    public Path mediaDir()
    {
        return homeDir().resolve("media");
    }


    public Path homeDir()
    {
        return home;
    }


    public Path relationDir()
    {
        return dataDir().resolve("relation");
    }


    public Path repositoryDir()
    {
        return dataDir().resolve("repo");
    }


    public Path resourceDir()
    {
        return homeDir().resolve("resources");
    }


    public Path templateDir()
    {
        return homeDir().resolve("template");
    }


    //
    // INTERNAL METHODS
    //

    private void setupEnv()
    {
        // Look for the Goodi environment
        //  1. System properties
        String envVariable = System.getProperty(GD_ENV_SYSPROP);

        // 2. Fall back to the env variable
        if (Strings.isNullOrEmpty(envVariable))
        {
            envVariable = System.getenv(GD_ENV_ENVVAR);
        }

        if (Strings.isNullOrEmpty(envVariable))
        {
            envVariable = DFLT_GD_ENV;
        }

        this.env = ENV.valueOf(envVariable.toUpperCase());
    }


    private void setupHome()
    {
        // Look for the home directory.
        // 1. Custom properties
        String homePath = System.getProperty(GD_HOME_SYSPROP);

        // 2. Fall back to the env variable
        if (Strings.isNullOrEmpty(homePath))
        {
            homePath = System.getenv(GD_HOME_ENVVAR);
        }

        // 3. Fall back to built-in default
        if (Strings.isNullOrEmpty(homePath))
        {
            homePath = DFLT_GD_HOME;
        }

        this.home = Paths.get(homePath);
        if (Files.notExists(this.home))
        {
            throw new InitializationError("Goodi home " + this.home
                + " does not exist");
        }

        // 4. Create each directory in home
        try
        {
            if (Files.notExists(this.archiveDir()))
                Files.createDirectory(this.archiveDir());
            if (Files.notExists(this.cacheDir()))
                Files.createDirectory(this.cacheDir());
            if (Files.notExists(this.configDir()))
                Files.createDirectory(this.configDir());
            if (Files.notExists(this.dataDir()))
                Files.createDirectory(this.dataDir());
            if (Files.notExists(this.indexDir()))
                Files.createDirectory(this.indexDir());
            if (Files.notExists(this.mediaDir()))
                Files.createDirectory(this.mediaDir());
            if (Files.notExists(this.relationDir()))
                Files.createDirectory(this.relationDir());
            if (Files.notExists(this.repositoryDir()))
                Files.createDirectory(this.repositoryDir());
            if (Files.notExists(this.resourceDir()))
                Files.createDirectory(this.resourceDir());
        }
        catch (IOException e)
        {
            throw new InitializationError(e);
        }
    }



    //
    // INNER CLASSES
    //


    public static enum ENV
    {
        LOCAL,
        DEV,
        PROD, ;
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final GlobalProperties INSTANCE =
            new GlobalProperties();
    }


    public static GlobalProperties instance()
    {
        return SingletonHolder.INSTANCE;
    }


    //
    // Constants
    //

    public static final String DFLT_GD_ENV = "LOCAL";

    public static final String DFLT_GD_HOME = "/usr/local/goodi";

    public static final String GD_ENV_ENVVAR = "GOODI_ENV";

    public static final String GD_ENV_SYSPROP = "goodi.env";

    public static final String GD_HOME_ENVVAR = "GOODI_HOME";

    public static final String GD_HOME_SYSPROP = "goodi.home";

    public static final String GLOBAL_PROP_FILE =
        "global.properties";
}
