
package gd.impl.config;


import static java.nio.charset.StandardCharsets.UTF_8;
import gd.api.v1.ClientType;
import gd.impl.ComponentConfig;
import gd.impl.util.InitializationError;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;


public class Configs
{
    private static final Logger LOG =
        LoggerFactory.getLogger(Configs.class);

    private static final String SYSTEM_SETTING = "sys_setting";

    private static LoadingCache<String,String> _clientConfigCache;

    private static ConfigManager _configMgr;

    private static LoadingCache<String,String> _sysSettingCache;


    /**
     * Alias getSystemConfig
     */
    public static <T> T get(
        Class<T> clazz)
    {
        return _configMgr.get(clazz);
    }


    public static String getClientConfig(
        ClientType type,
        int ver)
    {
        try
        {
            return _clientConfigCache.get(makeCacheKey(type, ver));
        }
        catch (ExecutionException e)
        {
            throw new RuntimeException(
                "Fail to fetch client config for key \""
                    + makeCacheKey(type, ver)
                    + "\"",
                e);
        }
    }


    public static ComponentConfig getComponentConfig()
    {
        return get(ComponentConfig.class);
    }


    public static <T> T getSystemConfig(
        Class<T> clazz)
    {
        return get(clazz);
    }


    //
    // SystemSetting Related
    //

    public static boolean getBool(
        SystemSetting key,
        boolean dflt)
    {
        String val = getSetting(key, null);
        return Strings.isNullOrEmpty(val) ? dflt : Boolean.parseBoolean(val);
    }


    public static boolean getBool(
        SystemSetting key,
        String variant,
        boolean dflt)
    {
        String val = getSetting(key, variant);
        return Strings.isNullOrEmpty(val) ? dflt : Boolean.parseBoolean(val);
    }


    public static double getDouble(
        SystemSetting key,
        double dflt)
    {
        String val = getSetting(key, null);
        return Strings.isNullOrEmpty(val) ? dflt : Double.parseDouble(val);
    }


    public static double getDouble(
        SystemSetting key,
        String variant,
        double dflt)
    {
        String val = getSetting(key, variant);
        return Strings.isNullOrEmpty(val) ? dflt : Double.parseDouble(val);
    }


    public static int getInt(
        SystemSetting key,
        int dflt)
    {
        String val = getSetting(key, null);
        return Strings.isNullOrEmpty(val) ? dflt : Integer.parseInt(val);
    }


    public static int getInt(
        SystemSetting key,
        String variant,
        int dflt)
    {
        String val = getSetting(key, variant);
        return Strings.isNullOrEmpty(val) ? dflt : Integer.parseInt(val);
    }


    public static long getLong(
        SystemSetting key,
        long dflt)
    {
        String val = getSetting(key, null);
        return Strings.isNullOrEmpty(val) ? dflt : Long.parseLong(val);
    }


    public static long getLong(
        SystemSetting key,
        String variant,
        long dflt)
    {
        String val = getSetting(key, variant);
        return Strings.isNullOrEmpty(val) ? dflt : Long.parseLong(val);
    }


    public static String getStr(
        SystemSetting key,
        String dflt)
    {
        String val = getSetting(key, null);
        return Strings.isNullOrEmpty(val) ? dflt : val;
    }


    public static String getStr(
        SystemSetting key,
        String variant,
        String dflt)
    {
        String val = getSetting(key, variant);
        return Strings.isNullOrEmpty(val) ? dflt : val;
    }


    public static String getSetting(
        SystemSetting key,
        String variant)
    {
        String k = key.key();
        if (StringUtils.isNotEmpty(variant))
            k += "." + variant;

        String v = null;
        v = _sysSettingCache.getIfPresent(k);

        int i;
        while (v == null && (i = k.lastIndexOf('.')) > 0)
        {
            k = k.substring(0, i);
            v = _sysSettingCache.getIfPresent(k);
        }
        return v;
    }


    public static void init()
    {
        _configMgr = ConfigManager.instance();
        _clientConfigCache = CacheBuilder.newBuilder().build(
            new CacheLoader<String,String>() {
                @Override
                public String load(
                    String key)
                    throws Exception
                {
                    // cache key is same as file name
                    GlobalProperties gp =
                        GlobalProperties.instance();
                    Path path = gp.resourceDir().resolve(key);
                    if (!java.nio.file.Files.exists(path))
                    {
                        LOG.warn(
                            "Can't find client config with key {}",
                            key);
                        return "";
                    }
                    else
                    {
                        return Files.toString(
                            path.toFile(),
                            UTF_8);
                    }
                }
            });

        _sysSettingCache = CacheBuilder.newBuilder().build(
            new CacheLoader<String,String>() {
                @Override
                public String load(
                    String key)
                    throws Exception
                {
                    // Do not load any value for the cache key
                    // We put all value (not expirable) at init time
                    throw new Exception();
                }
            });
        GlobalProperties gp = GlobalProperties.instance();
        Path sysPath = gp.configDir().resolve(SYSTEM_SETTING);
        if (java.nio.file.Files.exists(sysPath))
        {
            try
            {
                Files.readLines(
                    sysPath.toFile(),
                    UTF_8,
                    new LineProcessor<Void>() {
                        @Override
                        public boolean processLine(
                            String line)
                            throws IOException
                        {
                            // Each line should be in a format of "<key><4 spaces><value>"
                            // Four spaces right after the key, space not allowed in SystemConfig 
                            // key but allowed in value, value must be not BLANK
                            if (StringUtils.isNotEmpty(line))
                            {
                                String[] tokens =
                                    line.split("\\s\\s\\s\\s", 2);
                                if (tokens.length != 2)
                                {
                                    LOG.warn(
                                        "Don't understand the line '{}' in setting file",
                                        line);
                                }
                                else
                                {
                                    _sysSettingCache.put(
                                        tokens[0],
                                        tokens[1]);
                                }
                            }
                            return true;
                        }


                        @Override
                        public Void getResult()
                        {
                            return null;
                        }
                    });
            }
            catch (IOException e)
            {
                throw new InitializationError(
                    "Fail to initialize system configuration",
                    e);
            }
        }
    }


    public static void reloadClientConfig()
    {
        _clientConfigCache.invalidateAll();
    }


    public static void reloadSystemConfig(
        Class<?> clazz)
    {
        _configMgr.reload(clazz);
    }


    public static void setSetting(
        SystemSetting key,
        Object value)
    {
        setSetting(key, null, value);
    }


    /**
     * Persist the toString() of the specified object as setting
     * value for the specified key and variant
     * 
     * @throws RuntimeException
     *             each setSetting call will store the new setting
     *             to cache as well as dump/persist everything to a
     *             file. The exception is thrown if I/O errors
     */
    public static void setSetting(
        SystemSetting key,
        String variant,
        Object value)
    {
        String k = key.key();
        if (StringUtils.isNotEmpty(variant))
            k += "." + variant;

        if (value == null || value.toString().isEmpty())
        {
            // Empty or null value means to delete the setting
            _sysSettingCache.invalidate(k);
            return;
        }

        String str = value.toString();
        if (!validate(key, str))
        {
            LOG.warn(
                "Skip setting for key '{}', variant '{}', value '{}' because it is not validated",
                key.key(),
                variant,
                str);
            return;
        }

        _sysSettingCache.put(k, str);

        // Dump setting cache to file
        GlobalProperties gp = GlobalProperties.instance();
        Path sysPath = gp.configDir().resolve(SYSTEM_SETTING);
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String,String> e : _sysSettingCache.asMap()
            .entrySet())
        {
            // <key><4 spaces><value>
            sb.append(String.format(
                "%s    %s%n",
                e.getKey(),
                e.getValue()));
        }
        // Delete last newline
        sb.deleteCharAt(sb.length() - 1);
        try
        {
            Files.write(sb.toString(), sysPath.toFile(), UTF_8);
        }
        catch (IOException ex)
        {
            throw new RuntimeException(
                "Fail to dump setting to file",
                ex);
        }
    }


    //
    // INTERNAL METHODS
    //


    private static String makeCacheKey(
        ClientType type,
        int version)
    {
        return String.format(
            "%s-%d",
            type.name(),
            version);
    }


    private static boolean validate(
        SystemSetting key,
        String val)
    {
        return SystemSetting.validate(key, val);
    }
}
