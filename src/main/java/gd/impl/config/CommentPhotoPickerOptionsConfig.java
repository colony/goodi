
package gd.impl.config;


import gd.impl.util.Page;
import gd.impl.util.Sortable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;


@Configured(fname = "comment-photo-picker.yml")
public class CommentPhotoPickerOptionsConfig
    implements
        Reloadable<CommentPhotoPickerOptionsConfig>
{
    @JsonIgnore
    private volatile List<OptionConfig> optionConfigs;

    @JsonIgnore
    private volatile Map<Integer,ItemConfig> itemConfigs;

    // ItemConfig is sorted by ID from low to high
    @JsonIgnore
    private volatile Map<Integer,List<ItemConfig>> optionToItemsMap;


    public CommentPhotoPickerOptionsConfig()
    {
    }


    @JsonIgnore
    public ItemConfig getItemConfig(
        int itemId)
    {
        return itemConfigs.get(itemId);
    }


    @JsonIgnore
    public List<ItemConfig> getItemConfigs(
        int optionId,
        Page page)
    {
        List<ItemConfig> items = this.optionToItemsMap.get(optionId);
        if (items == null || items.isEmpty())
            return ImmutableList.<ItemConfig> of();

        Integer boundary = Strings.isNullOrEmpty(page.boundary) ? null : Integer.valueOf(page.boundary);
        int boundaryIndex = -1;
        if (boundary != null) {
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).id == boundary) {
                    boundaryIndex = i;
                    break;
                }
            }
        }

        List<ItemConfig> subList = null;
        int fromIndex, toIndex;
        if (page.isDescending() ^ page.isAfter()) {
            fromIndex = (boundaryIndex >= 0) ? (page.exclusive ? boundaryIndex + 1 : boundaryIndex) : 0;
            toIndex = Math.min(fromIndex + page.size, items.size());
            subList = new ArrayList<ItemConfig>(items.subList(fromIndex, toIndex));
        }
        else {
            fromIndex = (boundaryIndex >= 0) ? (page.exclusive ? boundaryIndex : boundaryIndex + 1) : items.size();
            toIndex = Math.max(0, fromIndex - page.size);
            subList = new ArrayList<ItemConfig>(items.subList(toIndex, fromIndex));
        }
        if (page.isDescending())
            Collections.reverse(subList);
        return subList;
    }


    @JsonIgnore
    public List<OptionConfig> getOptionConfigs()
    {
        return optionConfigs;
    }


    @Override
    public void reload(
        CommentPhotoPickerOptionsConfig t)
    {
        // Hot swap in memory
        this.optionConfigs = t.optionConfigs;
        this.itemConfigs = t.itemConfigs;
        this.optionToItemsMap = t.optionToItemsMap;
    }


    @SuppressWarnings({ "unused", "rawtypes", "unchecked" })
    private void setOptions(
        List<Map> options)
    {
        this.optionConfigs = new ArrayList<>(options.size());
        this.itemConfigs = new HashMap<Integer,ItemConfig>();
        this.optionToItemsMap = new HashMap<>();

        for (Map m : options) {
            OptionConfig oc = new OptionConfig();
            oc.id = (Integer) m.get("id");
            oc.name = (String) m.get("name");
            oc.coverImageUrl = (String) m.get("coverImageUrl");
            this.optionConfigs.add(oc);

            List<Map> items = (List<Map>) m.get("items");
            List<ItemConfig> itemList = new ArrayList<>();
            for (Map im : items) {
                ItemConfig ic = new ItemConfig();
                ic.id = (Integer) im.get("id");
                ic.url = (String) im.get("url");
                ic.width = Integer.parseInt((((String) im.get("dimension")).split("x")[0]));
                ic.height = Integer.parseInt((((String) im.get("dimension")).split("x")[1]));
                ic.thumbnailUrl = (String) im.get("thumbnailUrl");
                this.itemConfigs.put(ic.id, ic);
                itemList.add(ic);
            }
            this.optionToItemsMap.put(oc.id, itemList);
        }
    }



    public static class OptionConfig
    {
        public String coverImageUrl;

        public int id;

        public String name;
    }



    public static class ItemConfig
        implements
            Sortable
    {
        private int id;

        private String thumbnailUrl;

        private String url;

        private int width;

        private int height;


        public int getId()
        {
            return id;
        }


        public String getThumbnailUrl()
        {
            return thumbnailUrl;
        }


        public String getUrl()
        {
            return url;
        }


        public int getWidth()
        {
            return width;
        }


        public int getHeight()
        {
            return height;
        }


        @Override
        public String getSortKey()
        {
            return "ID";
        }


        @Override
        public String getSortVal()
        {
            return String.valueOf(id);
        }
    }
}
