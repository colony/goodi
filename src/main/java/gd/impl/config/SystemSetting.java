
package gd.impl.config;


import java.util.regex.Pattern;

import com.google.common.base.Strings;


/**
 * The setting key could be any arbitrary string except:
 * <p>
 * 1. Space is not allowed <br>
 * 2. The dot '.' is not allowed <br>
 * A setting value is uniquely mapped to a setting key plus variant,
 * a variant is expressed as arbitrary string separated by dots, For
 * example: <br>
 * k.v1.v2, the key is "k" followed by two variants "v1" and "v2",
 * when locating a setting, the search order is k.v1.v2, k.v1, k, if
 * none of them exist, the setting is considered as either missing
 * or null (NOTE, since '.' is using to distinguish variants, it is
 * not allowed in key string)
 */
public enum SystemSetting
{
    //
    // act.*: Settings related to activity (etc)
    //

    /**
     * The maximum number of activities to kept when
     * ActivityCleanerJob runs
     */
    ACT_CLEAN_CAP("act.clean_cap"),

    /**
     * Frequency cap means the min interval (ms) between two
     * consecutive activity of same type that happens on same user
     * and content (i.e. LIKE_POST of a post is allowed to be logged
     * only once per 30 minutes). The variant key is the name() of
     * ActivityType
     */
    ACT_FREQUENCY_CAP("act.freq.cap"),

    /**
     * Boolean. True if the frequency cap for a particular
     * ActivityType (the variant key) is enabled. False otherwise
     */
    ACT_FREQUENCY_CAP_ENABLED("act.freq.cap_enabled"),

    /**
     * Integer, whenever every X new voters vote on a post, send out
     * activity (notification)
     */
    ACT_NUM_VOTER_DIVISOR("act.num_voter_divisor"),


    //
    // alg.*: Settings related to different algorithm
    // (For detail, please check each algorithm implementation)
    //
    // NB_RP1 - NearbyRecentPopular1
    // RP1 - RecentPopular1
    // PIG_RP1 - RecentPopular1 for index POSTS_IN_GROUP_BY_POPULARITY
    //
    // Common variant factor values:
    //   comment_weight
    //   flag_weight
    //   share_weight
    //   vote_weight
    //   create_boost_scale
    //   time_denominator

    /**
     * The setting prefix for algorithm NP_RP1, the accepted variant
     * is in format of "factor.state", NOTE,if state is empty, all
     * setting value should default to variant "factor"
     */
    ALG_NB_RP1("alg.nbrp1"),

    /**
     * The setting prefix for algorithm RP1, no variant
     */
    ALG_RP1("alg.rp1"),

    /**
     * The setting prefix for algorithm PIG_RP1, the accepted
     * variant is in format of "factor.groupId"
     */
    ALG_PIG_RP1("alg.pigrp1"),



    //
    // idx.*: Settings related to index
    //

    /**
     * Enabled flag per index. The variant key is abbreviation of
     * IndexType
     */
    IDX_ENABLED("idx.enabled"),


    //
    // geo.*: Settings related to geo (nearby, etc)
    //

    /**
     * A comma-separated list of string, if a group is private, all
     * posts and comments within this group should reveal the real
     * geo information, to achieve that, the client will only
     * display geo's name if set, hence, we will populate the name
     * by randomly picking a word from this wordlist
     * <p>
     * The variant is group ID, that each group maintains a
     * different word list, if there's no specified word list for a
     * particular group, default to no-variant-setting key
     */
    GEO_COVERT_WORDLIST("geo.covert_wordlist"),

    /**
     * A enum string [ANON,COARSE,FINE], if ANON, any geo without
     * name will set "Anonymous" as name, if COARSE, only
     * state,country will be sent, if FINE (by default), send the
     * geo info as accurate as possible
     */
    GEO_DISPLAY_TYPE("geo.display_type"),

    /**
     * Number of miles used in searching nearby posts (feeds)
     */
    GEO_NEARBY_MILES("geo.nearby.post"),

    /**
     * For SQL repository only. R-tree index only stores rectangle.
     * A TINY square will be created for each new post, where the
     * post's geo is center and this setting defines the
     * width/height of the square, NOTE, the value is double
     */
    GEO_POST_BOUNDRECT_SIZE("geo.boundrect.post"),



    //
    // grp.*: Settings related to group
    //

    /**
     * A comma-separated list of group IDs, in this list,
     * lower-indexed (zero-indexed array) group will be displayed at
     * higher location. Also, if group IDs is not included in the
     * order list, the larger group ID will displayed at higher
     * location
     */
    GROUP_DISPLAY_ORDERS("grp.display_orders"),

    /**
     * The ID of the group containing featured post list
     */
    GROUP_FEATURED("grp.featured"),


    //
    // pts.*: Settings related to points rewarding system
    //

    /**
     * Points rewarded per PointEvent. The variant key is the name()
     * of PointEvent
     */
    POINTS("pts"),


    //
    // Other Settings, no pre-defined prefix
    //

    /**
     * If enabled, when user flags a post with specific complaint
     * message, server will trigger a special logic (i.e. nudity to
     * block)
     */
    AUTO_FLAG_LOGIC_ENABLED("auto_flag_logic.enabled"),

    /**
     * A comma-separated list of users who are blocked to post
     */
    BLOCKED_USER_IDS("blocked_user_ids"),

    /**
     * Return the configured number of cards when client draw cards,
     * the variant is either "REQ" or "REP"
     */
    CARD_COUNT("card_count"),

    /**
     * If downvotes - upvotes >= configured delta, set
     * CommentRep.isBadReviewed true. If the configured one is
     * non-positive value, the badreview feature is disabled, in
     * other words, when return response, none of comments are bad
     * reviewed
     */
    COMMENT_BADREVIEW_DELTA("comment_badreview_delta"),

    /**
     * Total number of featured post to return
     */
    FEATURED_LIST_COUNT("featured_list_count"),

    /**
     * If enabled, "General" group (ID=1) is eligible as default
     * selection when user creates post in general feed (or
     * somewhere outside of group view)
     */
    GENERAL_GROUP_ENALBED("general_group_enabled"),

    /**
     * For feed GLBOAL_POPULAR, GLOBAL_RECENT, we need filter out
     * posts that belong to the configured group Ids. The value is a
     * comma-separated list of IDs
     */
    GLOBAL_FEED_FILTER_GROUP_IDS("global_feed_filter_group_ids"),

    /**
     * Maximum badge count to send to devices. Badge counts higher
     * than this will be capped at the maximum value before they are
     * sent to the device.
     */
    MAX_DEVICE_BADGE_COUNT("max_device_badge_cnt"),

    /**
     * For feed NEARBY_POPULAR, NEARBY_RECENT, we need filter out
     * posts that belong to the configured group Ids. The value is a
     * comma-separated list of IDs
     */
    NEARBY_FEED_FILTER_GROUP_IDS("nearby_feed_filter_group_ids"),

    /**
     * If downvotes - upvotes >= configured delta, set
     * PostRep.isBadReviewed true. If the configured one is
     * non-positive value, the badreview feature is disabled, in
     * other words, when return response, none of posts are bad
     * reviewed
     */
    POST_BADREVIEW_DELTA("post_badreview_delta"),

    /**
     * The max number of events cached before computing post scores,
     * check PostScoreUpdater for detail
     */
    POST_SCORE_UPDATER_MAX_CACHE("postscoreupdater_maxcache"),

    THROTTLE_HACK_FILTER_ENABLED("throttle.hack.enabled"),

    ;

    private final String key;

    private static Pattern _commaSepNumberPattern;


    SystemSetting(
        String key)
    {
        this.key = key;
    }


    public String key()
    {
        return this.key;
    }


    public static SystemSetting fromKey(
        String key)
    {
        for (SystemSetting sc : SystemSetting.values())
        {
            if (sc.key.equals(key))
                return sc;
        }

        throw new IllegalArgumentException(
            "SystemConfig contains no key with \"" + key + "\"");
    }


    public static boolean validate(
        SystemSetting key,
        String val)
    {
        if (Strings.isNullOrEmpty(val))
            return false;

        switch (key)
        {
        // For int value
        case ACT_CLEAN_CAP:
        case ACT_NUM_VOTER_DIVISOR:
        case ALG_NB_RP1:
        case ALG_RP1:
        case ALG_PIG_RP1:
        case CARD_COUNT:
        case COMMENT_BADREVIEW_DELTA:
        case FEATURED_LIST_COUNT:
        case GEO_NEARBY_MILES:
        case MAX_DEVICE_BADGE_COUNT:
        case POINTS:
        case POST_BADREVIEW_DELTA:
        case POST_SCORE_UPDATER_MAX_CACHE:
            try
            {
                Integer.parseInt(val);
            }
            catch (NumberFormatException e)
            {
                return false;
            }
            break;

        // For double value
        case GEO_POST_BOUNDRECT_SIZE:
            try
            {
                Double.parseDouble(val);
            }
            catch (NumberFormatException e)
            {
                return false;
            }
            break;

        // For long value
        case ACT_FREQUENCY_CAP:
        case GROUP_FEATURED:
            try
            {
                Long.parseLong(val);
            }
            catch (NumberFormatException e)
            {
                return false;
            }
            break;

        // For boolean value
        case ACT_FREQUENCY_CAP_ENABLED:
        case AUTO_FLAG_LOGIC_ENABLED:
        case GENERAL_GROUP_ENALBED:
        case IDX_ENABLED:
        case THROTTLE_HACK_FILTER_ENABLED:
            String v = val.toLowerCase();
            if ("0".equals(v) || "false".equals(v))
            {
                break;
            }
            if ("1".equals(v) || "true".equals(v))
            {
                break;
            }
            return false;

            // For comma-separated list of number value
        case BLOCKED_USER_IDS:
        case GLOBAL_FEED_FILTER_GROUP_IDS:
        case GROUP_DISPLAY_ORDERS:
        case NEARBY_FEED_FILTER_GROUP_IDS:
            {
                if (_commaSepNumberPattern == null) {
                    _commaSepNumberPattern = Pattern.compile("^\\d+(,\\d+)*$");
                }
                if (_commaSepNumberPattern.matcher(val).find()) {
                    break;
                }
                return false;
            }

        // For string value
        default:
            return true;
        }

        return true;
    }
}
