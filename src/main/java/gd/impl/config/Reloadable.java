package gd.impl.config;

public interface Reloadable<T>
{
    public void reload(T t);
}
