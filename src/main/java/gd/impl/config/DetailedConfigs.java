
package gd.impl.config;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.ImmutableList;


/**
 * A configuration utility provides specific logic to process the
 * raw value from settings and return
 */
public final class DetailedConfigs
{
    public static List<Long> getBlockedUserIds()
    {
        return getListLongFromSetting(SystemSetting.BLOCKED_USER_IDS);
    }
    
    
    /**
     * Randomly pick a word from SystemSetting GEO_COVERT_WORDLIST
     * 
     * @param groupId
     *          Get the covert word list for the specified group
     */
    public static String getRandomGeoCovertWord(long groupId)
    {
        String settingVal = Configs.getStr(SystemSetting.GEO_COVERT_WORDLIST, String.valueOf(groupId), null);
        if (settingVal == null)
            settingVal = Configs.getStr(SystemSetting.GEO_COVERT_WORDLIST, null);
        if (StringUtils.isNotEmpty(settingVal)) {
            String[] tokens = settingVal.split(",");
            return tokens[RandomUtils.nextInt(0, tokens.length)];
        }
        return "Hidden";
    }
    
    
    /**
     * Fetch and pre-process SystemSetting
     * GLOBAL_FEED_FILTER_GROUP_IDS
     * 
     * @return a list of group IDs, or empty list
     */
    public static List<Long> getGlobalFeedFilterGroupIds()
    {
        return getListLongFromSetting(SystemSetting.GLOBAL_FEED_FILTER_GROUP_IDS);
    }
    
    
    public static List<Long> getGroupDisplayOrders()
    {
        return getListLongFromSetting(SystemSetting.GROUP_DISPLAY_ORDERS);
    }
    
    
    public static List<Long> getNearbyFeedFilterGroupIds()
    {
        return getListLongFromSetting(SystemSetting.NEARBY_FEED_FILTER_GROUP_IDS);
    }


    public static void init()
    {
    }
    
    
    //
    // INTERNAL METHODS
    //
    
    private static List<Long> getListLongFromSetting(SystemSetting settingKey)
    {
        String settingVal = Configs.getStr(settingKey, null);
        if (StringUtils.isNotEmpty(settingVal)) {
            List<Long> result = new ArrayList<>();
            String[] tokens = settingVal.split(",");
            for (int i = 0; i < tokens.length; i++) {
                result.add(Long.parseLong(tokens[i]));
            }
            return result;
        }
        return ImmutableList.<Long> of();
    }
}
