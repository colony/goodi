
package gd.impl.activity;


import gd.api.v1.ClientContext;
import gd.api.v1.ClientType;
import gd.api.v1.PaginationRep;
import gd.api.v1.activity.ActivityRepList;
import gd.api.v1.activity.ActivityResource;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.repository.Repositories;
import gd.impl.repository.RepositoryType;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/activities")
@Singleton
public class ActivityResourceImpl
    implements
        ActivityResource
{
    protected ActivityFeedManager activityFeedMgr;

    protected ActivityRepository activityRepo;


    public ActivityResourceImpl()
    {
        this.activityFeedMgr = ActivityFeedManager.instance();
        this.activityRepo =
            Repositories.get(
                RepositoryType.ACTIVITY,
                ActivityRepository.class);
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getAndAck(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam)
    {
        long callerId = Rests.beginOperation(headers, "act:getAndAck");
        Page p = pageParam.get();

        List<Activity> actList = this.activityFeedMgr.find(callerId, p);

        ActivityRepList repList = new ActivityRepList();
        Reps.populateActivityList(repList, actList, callerId);

        // We need mark all activities as READ if the request not come from CLI and
        // it asks for first page (p.isDefault()), NOTE, this no longer works for app
        // version 6 and above, which only acknowledge individual activity
        ClientContext cc = Rests.retrieveClientContext(headers);
        if (cc.getType() != ClientType.CLI && cc.getVersion() < 5)
        {
            if (p.isDefault() && !actList.isEmpty())
            {
                this.activityFeedMgr.markRead(callerId);
            }
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(actList),
                GDUtils.getLast(actList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }


    @Override
    @Path("/{id}/ack")
    @POST
    @Produces("application/json")
    public Response ackActivity(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam)
    {
        long callerId = Rests.beginOperation(headers, "act:ack");
        this.activityFeedMgr.markRead(callerId, idParam.get());
        return Response.ok().build();
    }
}
