
package gd.impl.activity;


import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.util.Page;
import gd.impl.util.UpdateConflictException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;




public class ActivityFeedManager
{
    private static final Logger LOG = LoggerFactory.getLogger(ActivityFeedManager.class);

    /**
     * This is the delimiter used to serialize array of Activity to
     * a string form. It is an non-printable control character. The
     * Activity class that implement @{link Stringfiable} interface
     * must choose a different delimiter
     */
    public static final String ACTIVITY_FEED_SPLIT = "\u0001";

    public static final int ACTIVITY_FEED_MAX_SIZE = 50;

    // Sort activities by ID
    public static final String SK_ID = "ID";

    private ActivityAggregator aggregator;

    private LSRelation hasUserMetaRel;


    private ActivityFeedManager()
    {
        RelationManager relMgr = RelationManager.instance();
        this.hasUserMetaRel = relMgr.get(RelationType.HAS_USER_METADATA, LSRelation.class);
        this.aggregator = new ActivityAggregator();
    }


    /**
     * Add the specified activity to the specified user's feed,
     * depending on the impl of ActivityAggregator, the added
     * activity might be merged with (or replace) another existing
     * activity to become the most RECENT one in feed
     */
    public void add(
        long userId,
        Activity activity)
    {
        long id = this.createActivityId(activity);
        activity.setId(id);

        SEntry entry = this.hasUserMetaRel.getEdge(userId, Relations.HUM_ACTIVITY_FEED);
        if (entry == null) {
            this.hasUserMetaRel.addEdge(
                userId,
                new SEntry(Relations.HUM_ACTIVITY_FEED)
                    .with(1L)
                    .with(activity.toStr()));
            this.updateUnreadActivityIds(userId, id, 0L);
        }
        else {
            String[] tokens = StringUtils.split(entry.s1, ACTIVITY_FEED_SPLIT);
            int aggregatedIdx = -1;
            Activity aggregatedActivity = null;
            StringBuilder sb = new StringBuilder();
            boolean iefa = this.aggregator.isEligibleForAggregation(activity);
            long numActivities = entry.l1;
            long removedActId = 0L;

            // Try to merge from the latest to oldest, once we fine the right activity to merge, skip the rest
            for (int i = 0; i < tokens.length && iefa; i++) {
                Activity existAct = new Activity().fromStr(tokens[i]);
                aggregatedActivity = this.aggregator.aggregate(activity, existAct);
                if (aggregatedActivity != null) {
                    aggregatedIdx = i;
                    break;
                }
            }

            if (aggregatedIdx < 0) {
                // Aggregation not happened, this is brandly-new activity
                if (numActivities + 1 <= ACTIVITY_FEED_MAX_SIZE) {
                    sb.append(activity.toStr());
                    sb.append(ACTIVITY_FEED_SPLIT);
                    sb.append(entry.s1);
                    numActivities += 1;
                }
                else {
                    sb.append(activity.toStr());
                    sb.append(ACTIVITY_FEED_SPLIT);
                    sb.append(StringUtils.substringBeforeLast(entry.s1, ACTIVITY_FEED_SPLIT));
                    removedActId = Activity.getIdFromSerializedStr(tokens[tokens.length - 1]);
                }
            }
            else {
                // Aggregation happened
                sb.append(aggregatedActivity.toStr());
                for (int j = 0; j < tokens.length; j++) {
                    if (j == aggregatedIdx)
                        continue;
                    sb.append(ACTIVITY_FEED_SPLIT);
                    sb.append(tokens[j]);
                }
                removedActId = Activity.getIdFromSerializedStr(tokens[aggregatedIdx]);
            }
            // Since this is lock-free, we don't impose optimistic lock neither
            this.hasUserMetaRel.putEdge(
                userId,
                new SEntry(Relations.HUM_ACTIVITY_FEED)
                    .with(numActivities)
                    .with(sb.toString()));
            this.updateUnreadActivityIds(userId, id, removedActId);
        }
    }


    /**
     * A simple finder method that retrieve activities for the
     * specified user. For simplicity, only DESCENDING and AFTER
     * pagination param is allowed, otherwise empty list is returned
     */
    public List<Activity> find(
        long userId,
        Page page)
    {
        if (!page.isDescending() || !page.isAfter())
            return ImmutableList.<Activity> of();

        SEntry entry = this.hasUserMetaRel.getEdge(userId, Relations.HUM_ACTIVITY_FEED);
        if (entry == null || StringUtils.isEmpty(entry.s1)) {
            return ImmutableList.<Activity> of();
        }
        else {
            List<Activity> result = new ArrayList<>();
            String[] tokens = entry.s1.split(ACTIVITY_FEED_SPLIT);
            Long boundaryActId = StringUtils.isNotEmpty(page.boundary) ? Long.parseLong(page.boundary) : null;
            if (boundaryActId == null) {
                for (int i = 0; i < Math.min(page.size, tokens.length); i++) {
                    Activity a = new Activity().fromStr(tokens[i]);
                    a.setSortKey(SK_ID);
                    a.setSortVal(String.valueOf(a.getId()));
                    result.add(a);
                }
            }
            else {
                for (int i = 0; i < tokens.length; i++) {
                    long id = Activity.getIdFromSerializedStr(tokens[i]);
                    if (id < boundaryActId) {
                        for (int j = i; j < Math.min(j + page.size, tokens.length); j++) {
                            Activity a = new Activity().fromStr(tokens[j]);
                            a.setSortKey(SK_ID);
                            a.setSortVal(String.valueOf(a.getId()));
                            result.add(a);
                        }
                        break;
                    }
                }
            }
            return result;
        }
    }


    public long getUnreadCount(
        long userId)
    {
        SEntry entry = this.hasUserMetaRel.getEdge(userId, Relations.HUM_ACTIVITY_UNREAD_IDS);
        return (entry == null) ? 0L : entry.l1;
    }


    /**
     * Retrieve list of unread activity Ids of the specified user
     */
    public Set<Long> getUnreadIds(
        long userId)
    {
        SEntry entry = this.hasUserMetaRel.getEdge(userId, Relations.HUM_ACTIVITY_UNREAD_IDS);
        if (entry == null || StringUtils.isEmpty(entry.s1)) {
            return ImmutableSet.<Long> of();
        }
        else {
            String[] tokens = entry.s1.split(",");
            Set<Long> result = new HashSet<>();
            for (int i = 0; i < tokens.length; i++) {
                result.add(Long.valueOf(tokens[i]));
            }
            return result;
        }

    }


    /**
     * Mark all activities of the specified user as read
     */
    public void markRead(
        long userId)
    {
        this.clearUnreadActivityIds(userId);
    }


    /**
     * Only mark the specified activity Id of the specified user as
     * read
     */
    public void markRead(
        long userId,
        long activityId)
    {
        this.updateUnreadActivityIds(userId, 0L, activityId);
    }



    //
    // INTERNAL METHODS
    //

    /**
     * For now, we maintain a separated list of activities per user,
     * and we don't necessarily generate a UNIQUE ID
     * <p>
     * Currently, the generated ID is a 64-bit value with following
     * bit fields:
     * <ul>
     * <li>bits 63-59: ActivityType ordinal
     * <li>bits 58-0: Milliseconds since the epoch (note 63-59 bits
     * aren't used until 100 years later)
     * </ul>
     */
    private long createActivityId(
        Activity a)
    {
        return (a.getType().ordinal() << 59) + System.currentTimeMillis();
    }


    private synchronized void clearUnreadActivityIds(
        long userId)
    {
        this.hasUserMetaRel.putEdge(
            userId,
            new SEntry(Relations.HUM_ACTIVITY_UNREAD_IDS)
                .with("")
                .with(0L));
    }


    /**
     * Update the unread activity info for the specified user
     * 
     * @param addActId
     *            Newly added activity ID
     * @param removedActId
     *            The activity be removed, either be truncated
     *            because of max feed limit or replaced because of
     *            aggregation. If non-positive,
     */
    private synchronized void updateUnreadActivityIds(
        long userId,
        long addActId,
        long removedActId)
    {
        for (int i = 0; i < 3; i++) {
            SEntry entry = this.hasUserMetaRel.getEdge(userId, Relations.HUM_ACTIVITY_UNREAD_IDS);
            if ((entry == null) && addActId > 0L) {
                this.hasUserMetaRel.addEdge(
                    userId,
                    new SEntry(Relations.HUM_ACTIVITY_UNREAD_IDS)
                        .with(1L)
                        .with(String.valueOf(addActId)));
                return;
            }
            else {
                boolean changed = false;
                List<String> tokens;
                if (StringUtils.isEmpty(entry.s1)) {
                    tokens = new ArrayList<>(1);
                }
                else {
                    tokens = Lists.newArrayList(entry.s1.split(","));
                }
                if (removedActId > 0L)
                    changed = tokens.remove(String.valueOf(removedActId));
                if (addActId > 0L)
                    changed = tokens.add(String.valueOf(addActId));

                if (changed) {
                    entry.l1 = tokens.size();
                    entry.s1 = StringUtils.join(tokens, ",");
                    try {
                        this.hasUserMetaRel.updateEdge(userId, entry);
                        return;
                    }
                    catch (UpdateConflictException e) {
                        // Other thread beats us, retry
                        continue;
                    }
                }
            }
        }

        LOG.info("Too much conflict to update unread activity ids for user {}", userId);
    }



    //
    // INTERNAL CLASSES
    //

    class ActivityAggregator
    {
        public Activity aggregate(
            Activity newAct,
            Activity oldAct)
        {
            if (StringUtils.equals(
                this.convertAggregateId(newAct),
                this.convertAggregateId(oldAct))) {
                // The aggregate logic is as simple as replace
                return newAct;
            }
            return null;
        }


        public boolean isEligibleForAggregation(
            Activity a)
        {
            switch (a.getType())
            {
            case DECLARE_WINNER:
                return false;
            default:
                return true;
            }
        }


        private String convertAggregateId(
            Activity a)
        {
            switch (a.getType())
            {
            case COMMENT_POST:
            case OTHER_COMMENT_POST:
                return "C" + a.getTargetId(); // targetId = postId

            case PARTICIPATE_THREAD:
            case OTHER_PARTICIPATE_THREAD:
                return "P" + a.getTargetId(); // targetId = threadId

            case REPLY_COMMENT:
                return "R" + a.getTargetId(); // targetId = repliedCmtId

            case VOTE_POST:
                return "V" + a.getTargetId();

            default:
                return null;
            }
        }
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final ActivityFeedManager INSTANCE = new ActivityFeedManager();
    }


    public static ActivityFeedManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
