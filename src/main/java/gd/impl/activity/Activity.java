
package gd.impl.activity;


import gd.api.v1.activity.ActivityType;
import gd.impl.util.Sortable;
import gd.impl.util.Stringifiable;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


/**
 * The class is designed by referring to the general concept of
 * Activity Stream Spec http://activitystrea.ms/specs/atom/1.0/
 */
public class Activity
    implements
        Sortable,
        Stringifiable<Activity>
{
    private static final int VERSION = 1;

    private static final String SPLIT1 = "|";
    
    private static final String SPLIT1_REGEX = "\\|";

    /**
     * The non-printable control char is used to SPLIT extra content
     * because the extra may contain any text
     */
    private static final String SPLIT2 = "\u0002";

    // Actor
    private long actorId;

    // Time
    private Date createTime;

    // Extra content if any, mainly used to build up the context from template
    private List<String> extra;

    // Id
    private long id;

    // Object
    private long objectId;

    // Target
    private long targetId;

    // Verb
    private ActivityType type;

    /**
     * sortKey and sortVal are not persisted. Both fields are
     * normally null unless the activity is part of collection (i.e.
     * feed). The fields indicate how the activity is sorted.
     */
    private String sortKey;

    private String sortVal;


    public Activity()
    {
    }


    /**
     * For certain type of activity, objectId or targetId might not
     * be needed, in this case, leave them to 0. Also, the extra
     * parameters must conform to per activity type, check
     * {@ActivityType} for details
     */
    public Activity(
        long actorId,
        ActivityType type,
        long objectId,
        long targetId,
        Date createTime,
        String... extra)
    {
        this.actorId = actorId;
        this.type = type;
        this.objectId = objectId;
        this.targetId = targetId;
        this.createTime = (createTime == null) ? new Date() : createTime;
        if (extra != null) {
            this.extra = Arrays.asList(extra);
        }
    }


    /**
     * Copy constructor
     */
    public Activity(
        Activity a)
    {
        this.actorId = a.actorId;
        this.createTime = a.createTime;
        this.extra = a.extra;
        this.id = a.id;
        this.objectId = a.objectId;
        this.targetId = a.targetId;
        this.type = a.type;
    }


    /**
     * This is an optimized way to extract ONLY id field from
     * serialized string instead of deserialize it fully
     */
    public static long getIdFromSerializedStr(
        String s)
    {
        if (s.startsWith("1|")) {
            String[] tokens = s.split(SPLIT1_REGEX, 3);
            return Long.parseLong(tokens[1]);
        }
        throw new IllegalArgumentException("Unrecognized string \"" + s + "\"");
    }


    //
    // Stringfiable
    //

    @Override
    public Activity fromStr(
        String s)
    {
        // Proceed according to the VERSION
        if (s.startsWith("1|")) {
            // Version 1
            // 1|id|type.ordinal|actorId|objectId|targetId|createTime|extra\2extra\2..
            String[] tokens = s.split(SPLIT1_REGEX, 7);
            if (tokens == null || tokens.length != 7)
                throw new IllegalArgumentException("Invalid serialized string \"" + s + "\"");

            // Skip first field since it is version
            this.id = Long.parseLong(tokens[1]);
            this.type = ActivityType.fromAbbreviation(tokens[2]);
            this.actorId = Long.parseLong(tokens[3]);
            this.objectId = Long.parseLong(tokens[4]);
            this.targetId = Long.parseLong(tokens[5]);
            String lastToken = tokens[6];
            if (lastToken.contains(SPLIT1)) {
                tokens = lastToken.split(SPLIT1_REGEX, 2);
                this.createTime = new Date(Long.parseLong(tokens[0]));
                this.extra = Arrays.asList(tokens[1].split(SPLIT2));
            }
            else {
                this.createTime = new Date(Long.parseLong(lastToken));
            }
        }
        
        // Other version goes here
        return this;
    }


    @Override
    public String toStr()
    {
        // 1|id|type|actorId|objectId|targetId|createTime(|extra)
        StringBuilder sb = new StringBuilder();
        sb.append(VERSION);
        sb.append(SPLIT1);
        sb.append(id);
        sb.append(SPLIT1);
        sb.append(type.abbreviation());
        sb.append(SPLIT1);
        sb.append(actorId);
        sb.append(SPLIT1);
        sb.append(objectId);
        sb.append(SPLIT1);
        sb.append(targetId);
        sb.append(SPLIT1);
        sb.append(createTime.getTime());
        if (extra != null) {
            sb.append(SPLIT1);
            sb.append(StringUtils.join(extra, SPLIT2));
        }
        return sb.toString();
    }



    //
    // Sortable
    //

    public String getSortKey()
    {
        return sortKey;
    }


    public void setSortKey(
        String sk)
    {
        this.sortKey = sk;
    }


    public String getSortVal()
    {
        return sortVal;
    }


    public void setSortVal(
        String sv)
    {
        this.sortVal = sv;
    }


    //
    // Getters & Setters
    //

    public long getActorId()
    {
        return actorId;
    }


    public void setActorId(
        long actorId)
    {
        this.actorId = actorId;
    }


    public Date getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        Date createTime)
    {
        this.createTime = createTime;
    }


    public List<String> getExtra()
    {
        return extra;
    }


    public String getExtra(
        int i)
    {
        if (this.extra == null || this.extra.size() <= i)
            return null;
        return this.extra.get(i);
    }


    public void setExtra(
        List<String> extra)
    {
        this.extra = extra;
    }


    public long getId()
    {
        return id;
    }


    public void setId(
        long id)
    {
        this.id = id;
    }


    public long getObjectId()
    {
        return objectId;
    }


    public void setObjectId(
        long objectId)
    {
        this.objectId = objectId;
    }


    public long getTargetId()
    {
        return targetId;
    }


    public void setTargetId(
        long targetId)
    {
        this.targetId = targetId;
    }


    public ActivityType getType()
    {
        return type;
    }


    public void setType(
        ActivityType type)
    {
        this.type = type;
    }
}
