
package gd.impl.activity;


import gd.impl.queue.Event;
import gd.impl.queue.EventClass;
import gd.impl.queue.EventConsumer;
import gd.impl.queue.EventType;


public class ActivityConsumer
    implements
        EventConsumer
{

    @Override
    public boolean accept(
        EventType type)
    {
        return type.getEventClass() == EventClass.ACTIVITY;
    }


    @Override
    public void consume(
        Event event)
    {
        EventType type = event.type;
        switch (type)
        {
        case ACT_COMMENT_POST:
            Activities.logCommentPost(
                event.lng1,
                event.lng2,
                event.lng3);
            break;
            
        case ACT_DECLARE_WINNER:
            Activities.logDeclareWinner(
                event.lng1,
                event.lng2);
            break;

        case ACT_PARTICIPATE_THREAD:
            Activities.logParticipateThread(
                event.lng1,
                event.lng2,
                event.lng3);
            break;

        case ACT_VOTE_POST:
            Activities.logVotePost(
                event.lng1,
                event.lng2);
            break;

        default:
            break; // Do nothing
        }
    }

}
