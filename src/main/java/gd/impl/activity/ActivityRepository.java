
package gd.impl.activity;


import gd.impl.repository.Repository;
import gd.impl.repository.RepositoryAdmin;
import gd.impl.util.Filter;
import gd.impl.util.Page;

import java.util.List;


public interface ActivityRepository
    extends
        Repository<Long,Activity>,
        RepositoryAdmin
{
    // Sort activities by ID
    public static final String SK_ID = "ID";
    
    /**
     * Count number of activities of the specified ownerId
     */
    public long count(long ownerId);


    public List<Activity> findByOwner(
        Long ownerId,
        Page page,
        Filter<Activity> filter);
}
