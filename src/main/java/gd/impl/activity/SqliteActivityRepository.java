
package gd.impl.activity;


import gd.impl.SQLiteManager;
import gd.impl.repository.RepositoryConfig;
import gd.impl.repository.SqliteRepository;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableLong;
import org.apache.commons.lang3.mutable.MutableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SqliteActivityRepository
    implements
        ActivityRepository,
        SqliteRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SqliteActivityRepository.class);

    private int batchSize = 100;


    public SqliteActivityRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    //
    // ActivityRepository
    //

    @Override
    public long count(
        long ownerId)
    {
        final MutableLong ml = new MutableLong();
        SqliteUtils.executeSelectQuery(
            "SELECT COUNT(*) FROM activity WHERE owner_id = " + ownerId,
            null,
            "",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    ml.setValue(rs.getLong(1));
                }
            });
        return ml.getValue();
    }
    

    @Override
    public Activity add(
        Activity a)
    {
        String sql =
            "insert into activity (body, create_time, data, owner_id, type) values (?,?,?,?,?)";

        Activity newAct = new Activity(a);
        newAct.setCreateTime(new Date());

        // SQL statement parameters construction
        Object param[] = new Object[5];
//        param[0] = newAct.getBody();
//        param[1] = newAct.getCreateTime().getTime();
//        if (null != newAct.getData())
//        {
//            param[2] = newAct.getData().toString();
//        }
//        param[3] = newAct.getOwnerId();
//        param[4] = newAct.getType().name();

        // Execute SQL
        long newId =
            SqliteUtils.executeInsertQuery(
                sql,
                param,
                "addActivity",
                true);
        newAct.setId(newId);
        return newAct;
    }


    @Override
    public boolean delete(
        Long id)
    {
        String sql = "delete from activity where id = ?";
        return SqliteUtils.executeQuery(
            sql,
            new Object[] {
            id
            },
            "deleteActivity");
    }


    @Override
    public boolean exists(
        Long id)
    {
        final String sql = "select 1 from activity where id=?";
        Object[] param = {
                id
        };
        final MutableBoolean mb = new MutableBoolean(false);

        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "isActivityExist",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }

            });
        return mb.booleanValue();
    }


    @Override
    public List<Activity> findByOwner(
        Long ownerId,
        Page page,
        Filter<Activity> filter)
    {
        final List<Activity> acts = new ArrayList<>(page.size);
        StringBuilder sb = new StringBuilder();
        sb.append("select * from activity where owner_id=? ");

        boolean hasBoundary = StringUtils.isNotEmpty(page.boundary);
        if (hasBoundary)
        {
            String op = page.toMathOperator();
            sb.append("AND id" + op + "? ");
        }

        sb.append("order by id ");
        /**
         * if order is desc before or asc before, we change it desc
         * before -> asc after, acs before -> desc after then we
         * reverse it .
         */
        String order =
            page.isDescending() == page.isAfter() ? "desc" : "asc";
        sb.append(order);
        sb.append(" limit ");
        sb.append(page.size);
        Object[] param = null;
        if (hasBoundary)
            param = new Object[] { ownerId, page.boundary };
        else
            param = new Object[] { ownerId };
        SqliteUtils.executeSelectQuery(
            sb.toString(),
            param,
            "findActivityByOwner",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Activity act = new Activity();
                        parseResultSet(act, rs);
                        acts.add(act);
                    }
                }

            });
        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */

        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(acts);
        }
        return acts;
    }


    @Override
    public Activity get(
        Long id)
    {
        final String sql = "select * from activity where id=?";
        Object[] param = {
                id
        };
        final Activity act = new Activity();

        final MutableObject<Activity> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "getActivity",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        Activity act = new Activity();
                        parseResultSet(act, rs);
                        mo.setValue(act);
                    }
                }
            });
        return act;
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        createActivityTable(config);
        SqliteUtils.createCompositeIndex(
            config.getTableName(),
            "activity_ownerId_id",
            "owner_id",
            "id",
            true);
        this.batchSize = config.getBatchSize();
    }


    @Override
    public Activity put(
        Activity a)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public Activity update(
        Activity t)
        throws UpdateConflictException
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void visitAll(
        Visitor<Activity> visitor)
    {
        if (!visitor.before())
            return;

        SQLiteManager sqliteManager = SQLiteManager.instance();
        Connection connection = null;
        PreparedStatement statement = null;

        boolean hasMore = true;
        boolean aborted = false;
        long id = 0;

        try
        {
            connection = sqliteManager.getConnection();
            final String sql =
                "select * from activity where id>? limit "
                    + this.batchSize;

            statement =
                connection.prepareStatement(sql);
            while (hasMore && !aborted)
            {
                statement.setLong(1, id);
                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next())
                {
                    count++;
                    id = rs.getLong("id");

                    Activity act = new Activity();
                    parseResultSet(act, rs);
                    if (!visitor.visit(act))
                    {
                        aborted = true;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Fail to execute query \"visitAll\"",
                e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for visitAll",
                    e);
            }
        }

        visitor.after();
    }


    //
    // RepositoryAdmin
    //

    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }



    //
    // INTERNAL METHODS
    //

    private void createActivityTable(
        RepositoryConfig config)
    {
        final String createActivityTableSql =
            "create table if not exists "
                + config.getTableName()
                + "(" +
                "id integer primary key autoincrement not null," +
                "body text," +
                "create_time integer," +
                "data text," +
                "owner_id integer not null," +
                "type text" +
                ")";
        SqliteUtils.executeQuery(
            createActivityTableSql,
            null,
            "CreateActivityTable");
    }


    private void parseResultSet(
        Activity act,
        ResultSet rs)
        throws SQLException
    {
//        act.setBody(rs.getString("body"));
//        act.setCreateTime(new Date(rs.getLong("create_time")));
//        String dataStr = rs.getString("data");
//        if (StringUtils.isNotEmpty(dataStr))
//        {
//            act.setData(new JSONObject(dataStr));
//        }
//        act.setId(rs.getLong("id"));
//        act.setOwnerId(rs.getLong("owner_id"));
//        act.setType(ActivityType.valueOf(rs.getString("type")));
    }
}
