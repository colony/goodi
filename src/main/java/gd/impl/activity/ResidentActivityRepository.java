
package gd.impl.activity;


import gd.impl.repository.RepositoryConfig;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;


public class ResidentActivityRepository
    implements
        ActivityRepository
{
    protected List<Activity> activities;


    public ResidentActivityRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public synchronized long count(
        long ownerId)
    {
        //TODO
        return 0L;
    }
    

    @Override
    public synchronized Activity add(
        Activity a)
    {
        // Assign a new ID to the activity
        int id = this.activities.size();
        a.setId(id);

        // Append to the end of list
        this.activities.add(a);

        return new Activity(a);
    }


    @Override
    public synchronized boolean delete(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        Activity act = this.activities.get(intId);
        if (act == null)
            return false;

        // Delete the entry by setting to null so as to avoid array shifting
        this.activities.set(intId, null);
        return true;
    }


    @Override
    public synchronized boolean exists(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        return this.activities.get(intId) != null;
    }


    @Override
    public synchronized Activity get(
        Long id)
    {
        if (!checkBoundary(id))
            return null;

        int intId = id.intValue();
        return this.activities.get(intId);
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        this.activities = new ArrayList<>();
        this.activities.add(null); // ID 0 is not used
    }


    @Override
    public synchronized Activity update(
        Activity newAct)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void visitAll(
        Visitor<Activity> visitor)
    {
        // This is not synchronized, use with cautions

        if (!visitor.before())
            return;

        // We don't need to worry about outOfBoundary because
        // delete will only nullify items instead of remove
        // Also, we don't use iterator to avoid ConcurrentModificationException
        int num = this.activities.size();
        for (int i = 1; i < num; ++i)
        {
            // Maybe stale in concurrent system
            Activity act = this.activities.get(i);

            // Ignore null
            if (act == null)
            {
                continue;
            }

            if (!visitor.visit(act))
            {
                // Visitor aborts
                break;
            }
        }

        // Clean up
        visitor.after();
    }


    //
    // RepositoryAdmin
    //

    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        // TODO Auto-generated method stub
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        // TODO Auto-generated method stub
    }


    //
    // ActivityRepository
    //

    public List<Activity> findByOwner(
        Long userId,
        Page page,
        Filter<Activity> filter)
    {
        String key =
            StringUtils.defaultIfEmpty(
                page.key,
                SK_ID);
        switch (key)
        {
        case SK_ID:
            Long boundary =
                Strings.isNullOrEmpty(page.boundary) ? null : Long.valueOf(page.boundary);
            boolean inBound = checkBoundary(boundary);
            int size = this.activities.size();

            // 1. determine startId;
            int startId;
            boolean decreasing; // order of traversal
            if (page.isDescending())
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
            }
            else
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
            }

            // 2. do traversal
            List<Activity> result = new ArrayList<>();
            int num = 0;
            for (int id = startId; (decreasing ? id > 0 : id < size);)
            {
                Activity c = this.activities.get(id);
                id = decreasing ? id - 1 : id + 1;

                if (c == null)
                    continue;

                // Since, we store every comments as a flat array, we need to
                // consult the postId to check it is the child
//                if (c.getOwnerId() == userId)
                {
                    if (filter != null && !filter.accept(c))
                        continue;

                    // Copy over, since this is resident (in-memory) data structure
                    Activity act = new Activity(c);
                    act.setSortKey(key);
                    act.setSortVal(String.valueOf(act.getId()));
                    result.add(act);
                    if (++num >= page.size)
                        break;
                }
            }

            // 3. in BEFORE, we traverse in a reverse direction of page.order
            if (!page.isAfter())
                Collections.reverse(result);
            return result;

        default:
            throw new UnsupportedOperationException(
                "Unsupported sort key " + key);
        }
    }


    //
    // INTERNAL METHODS
    //

    protected boolean checkBoundary(
        Long id)
    {
        // Internally, we store data by using an array with integer
        // position as index. The id can't go beyond boundary
        return (id != null &&
            id > 0 && id < this.activities.size());
    }


    @Override
    public Activity put(
        Activity t)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
