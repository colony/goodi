
package gd.impl.activity;


import gd.api.v1.activity.ActivityType;
import gd.api.v1.post.ThreadType;
import gd.impl.cache.C.LLCache;
import gd.impl.cache.CacheManager;
import gd.impl.cache.CacheType;
import gd.impl.comment.Comment;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.media.LinkMedia;
import gd.impl.media.TextMedia;
import gd.impl.notification.NotificationManager;
import gd.impl.post.Post;
import gd.impl.post.PostStatus;
import gd.impl.post.Posts;
import gd.impl.queue.Event;
import gd.impl.queue.EventQueue;
import gd.impl.queue.EventType;
import gd.impl.queue.QueueManager;
import gd.impl.queue.QueueType;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.user.Users;
import gd.impl.util.GDUtils;
import gd.impl.util.Visitor;


public final class Activities
{
    private static EventQueue _actQueue;

    private static ActivityFeedManager _actFeedMgr;

    private static LLRelation _hasCommenterRel;

    private static LLRelation _hasThreadChildrenRel;

    private static LLRelation _hasThreadParticipantRel;

    private static NotificationManager _notifyMgr;

    private static LLCache _postVoterCountCache;


    public static long getUnreadCount(
        long userId)
    {
        return _actFeedMgr.getUnreadCount(userId);
    }


    public static void init()
    {
        QueueManager queueMgr = QueueManager.instance();
        _actQueue =
            queueMgr.get(QueueType.ACTIVITY, EventQueue.class);

        RelationManager relMgr = RelationManager.instance();
        _hasCommenterRel =
            relMgr.get(RelationType.HAS_COMMENTER, LLRelation.class);
        _hasThreadChildrenRel =
            relMgr.get(RelationType.HAS_THREAD_CHILDREN, LLRelation.class);
        _hasThreadParticipantRel =
            relMgr.get(RelationType.HAS_THREAD_PARTICIPANT, LLRelation.class);

        CacheManager cacheMgr = CacheManager.instance();
        _postVoterCountCache = cacheMgr.get(CacheType.POST_VOTER_COUNT, LLCache.class);

        _notifyMgr = NotificationManager.instance();
        _actFeedMgr = ActivityFeedManager.instance();
    }


    //
    // Log Activity via Queue (async)
    //

    public static void asyncLogCommentPost(
        long commenterId,
        long commentId,
        long postId)
    {
        Event evt = new Event(EventType.ACT_COMMENT_POST);
        evt.lng1 = commenterId;
        evt.lng2 = commentId;
        evt.lng3 = postId;
        _actQueue.enq(evt);
    }


    public static void asyncLogDeclareWinner(
        long postId,
        long threadId)
    {
        Event evt = new Event(EventType.ACT_DECLARE_WINNER);
        evt.lng1 = postId;
        evt.lng2 = threadId;
        _actQueue.enq(evt);
    }


    /**
     * Participate a thread by submitting a post to it.
     * 
     * @param participantId
     *            The creator of the participating post
     */
    public static void asyncLogParticipateThread(
        long participantId,
        long postId,
        long threadId)
    {
        Event evt = new Event(EventType.ACT_PARTICIPATE_THREAD);
        evt.lng1 = participantId;
        evt.lng2 = postId;
        evt.lng3 = threadId;
        _actQueue.enq(evt);
    }


    public static void asyncLogVotePost(
        long voterId,
        long postId)
    {
        Event evt = new Event(EventType.ACT_VOTE_POST);
        evt.lng1 = voterId;
        evt.lng2 = postId;
        _actQueue.enq(evt);
    }


    public static void asyncLogVoteComment()
    {
        //TODO
    }


    //
    // The REAL Activity Recording
    //

    public static void logCommentPost(
        final long commenterId,
        final long commentId,
        final long postId)
    {
        final Post post = Posts.get(postId);
        if (post == null)
            return; // Ignore and bail out

        final Comment cmt = Posts.getComment(commentId);
        if (cmt == null)
            return; // Ignore and bail out

        // Prepare the template data
        final String postAbbrev = GDUtils.abbreviate(extractPostTextualData(post), 15 + 3);
        final String cmtAbbrev = GDUtils.abbreviate(cmt.getBody(), 15 + 3);
        final long ownerId = post.getOwnerId();

        //1. Send activity to owner of post
        if (ownerId != commenterId)
        {
            if (Users.isAlertable(ownerId))
            {
                _actFeedMgr.add(ownerId, new Activity(
                    commenterId,
                    ActivityType.COMMENT_POST,
                    commentId,
                    postId,
                    null,
                    cmtAbbrev,
                    postAbbrev));
                _notifyMgr.refreshBadgeCountOnAppIcon(ownerId);
            }
        }

        //2. Send activity to user who is replied by the comment (if it is reply)
        long repliedCmtOwnerId = 0L;
        if (cmt.getParentId() != null) {
            Comment repliedComment = Posts.getComment(cmt.getParentId());
            if (repliedComment != null &&
                repliedComment.getOwnerId() != ownerId &&
                repliedComment.getOwnerId() != commenterId) {
                repliedCmtOwnerId = repliedComment.getOwnerId();
                if (Users.isAlertable(repliedCmtOwnerId)) {
                    _actFeedMgr.add(repliedCmtOwnerId, new Activity(
                        commenterId,
                        ActivityType.REPLY_COMMENT,
                        commentId,
                        repliedComment.getId(),
                        null,
                        String.valueOf(postId),
                        cmtAbbrev,
                        GDUtils.abbreviate(repliedComment.getBody(), 15 + 3)));
                    _notifyMgr.refreshBadgeCountOnAppIcon(repliedCmtOwnerId);
                }
            }
        }
        final long repliedCommentOwnerId = repliedCmtOwnerId;

        //3. Send activity to rest other commenters of the post, excluding already-notified ones
        _hasCommenterRel.visitAllEdges(postId, new Visitor<LEntry>() {
            @Override
            public boolean visit(
                LEntry item)
            {
                long otherCommenterId = item.getId();
                if (otherCommenterId == ownerId ||
                    otherCommenterId == commenterId ||
                    otherCommenterId == repliedCommentOwnerId) {
                    return true;
                }

                if (Users.isAlertable(otherCommenterId)) {
                    _actFeedMgr.add(otherCommenterId, new Activity(
                        commenterId,
                        ActivityType.OTHER_COMMENT_POST,
                        commentId,
                        postId,
                        null,
                        cmtAbbrev,
                        postAbbrev));
                    _notifyMgr.refreshBadgeCountOnAppIcon(otherCommenterId);
                }

                return true;
            }
        });
    }

    
    public static void logDeclareWinner(
        final long winPostId,
        final long threadId)
    {
        LEntry entry = _hasThreadChildrenRel.getSource(threadId);
        if (entry == null)
            return;
        final long threadOwnerId = entry.l2;
        
        Post p = Posts.get(winPostId);
        if (p == null)
            return;
        final long winPostOwnerId = p.getOwnerId();
        
        _hasThreadParticipantRel.visitAllEdges(threadId, new Visitor<LEntry>() {

            @Override
            public boolean visit(
                LEntry item)
            {
                long userId = item.getId();
                if (userId == threadOwnerId)
                    return true;
                
                if (Users.isAlertable(userId)) {
                    if (userId == winPostOwnerId) {
                        _actFeedMgr.add(userId, new Activity(
                            threadOwnerId,
                            ActivityType.DECLARE_WINNER,
                            threadOwnerId,
                            threadId,
                            null,
                            "true"));
                    } else {
                        _actFeedMgr.add(userId, new Activity(
                            threadOwnerId,
                            ActivityType.DECLARE_WINNER,
                            threadOwnerId,
                            threadId,
                            null,
                            "false"));
                    }
                    _notifyMgr.refreshBadgeCountOnAppIcon(userId);
                }
                return true;
            }
        });
    }
    

    public static void logParticipateThread(
        final long participantId,
        final long postId,
        final long threadId)
    {
        LEntry entry = _hasThreadChildrenRel.getSource(threadId);
        if (entry == null)
            return;

        final long ownerId = entry.l2;
        final ThreadType type = ThreadType.valueOf(entry.s1);

        // 1. Send activity to owner of the thread
        if (ownerId != participantId) {
            if (Users.isAlertable(ownerId))
            {
                _actFeedMgr.add(ownerId, new Activity(
                    participantId,
                    ActivityType.PARTICIPATE_THREAD,
                    postId,
                    threadId,
                    null,
                    type.name()));
                _notifyMgr.refreshBadgeCountOnAppIcon(ownerId);
            }
        }

        // 2. Send activity to other participants
        _hasThreadParticipantRel.visitAllEdges(threadId, new Visitor<LEntry>() {

            @Override
            public boolean visit(
                LEntry item)
            {
                long otherUserId = item.getId();
                if (otherUserId == ownerId ||
                    otherUserId == participantId) {
                    return true;
                }

                if (Users.isAlertable(otherUserId)) {
                    _actFeedMgr.add(otherUserId, new Activity(
                        participantId,
                        ActivityType.OTHER_PARTICIPATE_THREAD,
                        postId,
                        threadId,
                        null,
                        type.name()));
                    _notifyMgr.refreshBadgeCountOnAppIcon(otherUserId);
                }

                return true;
            }
        });
    }


    public static void logVotePost(
        long voterId,
        long postId)
    {
        Post post = Posts.get(postId);
        if (post == null)
            return;

        if (post.getStatus() != null && post.getStatus().isSet(PostStatus.Status.BLOCKED))
            return;

        long val = _postVoterCountCache.incr(postId, 1L);
        int numVoterDivisor = Configs.getInt(SystemSetting.ACT_NUM_VOTER_DIVISOR, 5);
        if (val > 0 && val % numVoterDivisor == 0) {
            if (Users.isAlertable(post.getOwnerId())) {
                String postAbbrev = GDUtils.abbreviate(extractPostTextualData(post), 15 + 3);
                _actFeedMgr.add(post.getOwnerId(), new Activity(
                    0L, // this is not a particular actor, so we set it 0
                    ActivityType.VOTE_POST,
                    0L, // no object
                    postId,
                    null,
                    postAbbrev));
            }
        }
    }



    //
    // INTERNAL METHODS
    //


    /**
     * Extract textual info from the post. For photo or audio post,
     * we take caption (body). For text post, we take the text
     * directly. The method might return NULL
     */
    private static String extractPostTextualData(
        Post post)
    {
        switch (post.getType())
        {
        case AUDIO:
        case PHOTO:
        case VIDEO:
            return post.getBody();

        case LINK:
            {
                LinkMedia linkMedia = post.getMedia(0, LinkMedia.class);
                return linkMedia.getTitle();
            }

        case TEXT:
            {
                TextMedia textMedia = post.getMedia(0, TextMedia.class);
                return textMedia.getText();
            }

        default:
            return null;
        }
    }
}
