
package gd.impl.algorithm;


import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.index.IDX.LDEntry;
import gd.impl.index.IDX.LDIndex;
import gd.impl.post.Post;
import gd.impl.post.PostClass;
import gd.impl.post.PostCount;
import gd.impl.post.PostRepository;
import gd.impl.post.PostStatus;
import gd.impl.post.PostStatus.Status;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.CancelledException;
import gd.impl.util.Visitor;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The algorithm (VERSION 1) is designated to populate index
 * POSTS_BY_RECENT_POPULARITY
 * 
 * The recent-popular score formulas:
 * 
 * <pre>
 *      RPS = N / M
 *      B  = CmtWeight * MIN(NumOfCmt,100) + 
 *           FlagWeight * NumOfFlag + 
 *           ShareWeight * NumOfShare +
 *           VoteWeight * (NumOfUpvotes - NumOfDownvotes)
 *      M  = Maximum of all N
 *      N  = B + T1/T2
 *      T1 = the difference between creation time and 2015/1/1 in seconds
 *      T2 = time denominator in seconds
 * </pre>
 */
public class PostsByRecentPopularityAlgorithm1
{
    private static final Logger LOG = LoggerFactory.getLogger(PostsByRecentPopularityAlgorithm1.class);

    protected volatile boolean cancelled;


    public PostsByRecentPopularityAlgorithm1()
    {
        this.cancelled = false;
    }


    public void cancel()
    {
        this.cancelled = true;
    }


    public void populate(
        LDIndex index)
        throws CancelledException
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        PostRepository repo = repoMgr.get(RepositoryType.POST, PostRepository.class);

        InternalAlgorithmVisitor visitor = new InternalAlgorithmVisitor();
        try {
            repo.visitAll(visitor);
        }
        catch (Throwable th) {
            throw new RuntimeException("Unable to populate POSTS_BY_RECENT_POPULARITY", th);
        }

        if (this.cancelled) {
            throw new CancelledException(
                "POSTS_BY_RECENT_POPULARITY index building has been cancelled");
        }

        double maxScore = visitor.getMaxRawScore();
        for (PostData data : visitor.getItems()) {
            if (this.cancelled) {
                throw new CancelledException(
                    "POSTS_BY_RECENT_POPULARITY index building has been cancelled");
            }

            data.rawScore /= maxScore;

            double normalizedScore = data.rawScore / maxScore;

            LDEntry entry = new LDEntry(data.id, normalizedScore);
            entry.s1 = String.valueOf(data.rawScore);
            entry.s2 = String.valueOf(data.groupId);
            index.add(entry);
        }
    }



    /**
     * Internal data structure holding data about a post for
     * computation
     */
    protected class PostData
    {
        public long createTime;

        public long groupId;

        public long id;

        public double rawScore;
    }



    private class InternalAlgorithmVisitor
        extends
            Visitor<Post>
    {
        private List<PostData> items;

        private double maxRawScore;

        private long numPosts;

        private PostRepository repo;

        private int cmtWeight;

        private int flagWeight;

        private int shareWeight;

        private int voteWeight;

        private long timeDenominator;


        @Override
        public boolean before()
        {
            this.items = new ArrayList<>();
            this.maxRawScore = 0.0;
            this.numPosts = 0L;
            this.repo = RepositoryManager.instance().get(RepositoryType.POST, PostRepository.class);

            this.cmtWeight =
                Configs.getInt(SystemSetting.ALG_RP1, "comment_weight", 2);
            this.flagWeight =
                Configs.getInt(SystemSetting.ALG_RP1, "flag_weight", -5);
            this.shareWeight =
                Configs.getInt(SystemSetting.ALG_RP1, "share_weight", 10);
            this.voteWeight =
                Configs.getInt(SystemSetting.ALG_RP1, "vote_weight", 1);
            this.timeDenominator =
                Configs.getLong(SystemSetting.ALG_RP1, "time_denominator", 3600);
            return true;
        }


        @Override
        public void after()
        {
            LOG.info("PostsByRecentPopularAlgorithm1 processed {} posts", this.numPosts);
        }


        @Override
        public boolean visit(
            Post post)
        {
            // Bail out if we have been cancelled
            if (PostsByRecentPopularityAlgorithm1.this.cancelled) {
                return false;
            }

            // Keep track of how many posts have seen
            ++this.numPosts;

            // Filter out some not-needed posts
            PostStatus status = post.getStatus();
            if (status != null
                && status.isSetAny(
                    Status.HIDDEN,
                    Status.ANNOUNCEMENT)) {
                return true;
            }
            PostClass clazz = post.getClazz();
            if (clazz == PostClass.ANNOUNCEMENT ||
                clazz == PostClass.THREAD) {
                return true;
            }

            // Filter out unwanted group
            Long groupId = post.getGroupId();
            if (groupId == null || groupId == 0L)
                return true;

            long postId = post.getId();

            // Gather together some interesting facts about the post for algorithm
            PostData data = new PostData();

            data.createTime = post.getCreateTime().getTime();
            data.groupId = groupId;
            data.id = postId;

            // Compute score
            PostCount pc = this.repo.getCount(postId);
            data.rawScore = 0.0;
            data.rawScore += cmtWeight * Math.min(pc.getNumComments(), 100);
            data.rawScore += flagWeight * pc.getNumFlags();
            data.rawScore += shareWeight * pc.getNumShares();
            data.rawScore += voteWeight * (pc.getNumUpvotes() - pc.getNumDownvotes());

            long diffMSec = data.createTime - 1420070400000L;
            long diffSec = diffMSec / 1000;
            data.rawScore += (diffSec / this.timeDenominator);

            this.items.add(data);

            if (data.rawScore > this.maxRawScore)
                this.maxRawScore = data.rawScore;

            return true;
        }


        public List<PostData> getItems()
        {
            return items;
        }


        public double getMaxRawScore()
        {
            return maxRawScore;
        }
    }
}
