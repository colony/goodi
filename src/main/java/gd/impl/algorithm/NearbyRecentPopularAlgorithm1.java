
package gd.impl.algorithm;


import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.post.Post;
import gd.impl.post.PostCount;
import gd.impl.post.Posts;

import org.apache.commons.lang3.StringUtils;


/**
 * <pre>
 * CW = weight of comment 
 * FW = weight of flag 
 * LW = weight of like 
 * SW = weight of share
 * N(C,F,L,S) = number of corresponding actions
 * </pre>
 * 
 * NOTE: since we summarize them together, negative weight will work
 * same way as substraction (i.e. FW) FORMULA:
 * <p>
 * CW * NC + FW * NF + LW * NL + SW * NS
 */
public class NearbyRecentPopularAlgorithm1
{
    public NearbyRecentPopularAlgorithm1()
    {

    }


    /**
     * @return The computed score based on algorithm. If
     *         non-positive, the post doesn't exist anymore
     */
    public double computeScore(
        long postId)
    {
        Post post = Posts.get(postId);
        if (post == null)
            return -1.0;

        String state = "";
        if (post.getGeo() != null) {
            if (StringUtils.isNotEmpty(post.getGeo().getState())) {
                state = "." + post.getGeo().getState();
            }
        }

        int cmtWeight =
            Configs.getInt(SystemSetting.ALG_NB_RP1, "comment_weight" + state, 2);
        int flagWeight =
            Configs.getInt(SystemSetting.ALG_NB_RP1, "flag_weight" + state, -5);
        int shareWeight =
            Configs.getInt(SystemSetting.ALG_NB_RP1, "share_weight" + state, 10);
        int voteWeight =
            Configs.getInt(SystemSetting.ALG_NB_RP1, "vote_weight" + state, 1);
        int createBoostScale =
            Configs.getInt(SystemSetting.ALG_NB_RP1, "create_boost_scale" + state, 1);
        int timeDenominator =
            Configs.getInt(SystemSetting.ALG_NB_RP1, "time_denominator" + state, 3600);

        PostCount pc = Posts.getCount(postId);
        double score = 0.0;
        score += cmtWeight * Math.min(pc.getNumComments(), 100);
        score += flagWeight * pc.getNumFlags();
        score += shareWeight * pc.getNumShares();
        score += voteWeight * (pc.getNumUpvotes() - pc.getNumDownvotes());

        if (createBoostScale > 0) {
            long diffMSec = post.getCreateTime().getTime() - 1420070400000L;
            long diffSec = diffMSec / 1000;
            score += createBoostScale * (diffSec / timeDenominator);
        }

        // The score is used to rank post, when pagination, same score maybe cause
        // miss post to return, since the chance is rare for now, we don't handle it
        return score;
    }
}
