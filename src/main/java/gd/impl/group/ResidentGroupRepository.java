
package gd.impl.group;


import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.impl.group.GroupCount.Field;
import gd.impl.group.GroupExporter.GroupExporterEntry;
import gd.impl.repository.RepositoryConfig;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.AbstractIterator;


public class ResidentGroupRepository
    implements
        GroupRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ResidentGroupRepository.class);

    protected List<Group> groups;

    protected List<GroupCount> groupCounts;


    public ResidentGroupRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public synchronized Group add(
        Group t)
    {
        // Assign a new ID to the group
        int id = this.groups.size();
        t.setId(id);
        t.setCreateTime(new Date());
        t.setUpdateTime(new Date());

        this.groups.add(t);
        this.groupCounts.add(new GroupCount());
        return new Group(t);
    }


    @Override
    public synchronized boolean delete(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        Group group = this.groups.get(intId);
        if (group == null)
            return false;

        // Delete the entry by setting to null so as to avoid array shifting
        this.groups.set(intId, null);
        this.groupCounts.set(intId, null);

        return true;
    }


    @Override
    public synchronized boolean exists(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        return this.groups.get(intId) != null;
    }


    @Override
    public synchronized Group get(
        Long id)
    {
        if (!checkBoundary(id))
            return null;

        int intId = id.intValue();
        return this.groups.get(intId);
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        this.groups = new ArrayList<>();
        this.groups.add(null); // group ID 0 is not used
        this.groupCounts = new ArrayList<>();
        this.groupCounts.add(null);
    }


    @Override
    public synchronized Group put(
        Group newGrp)
    {
        if (newGrp.getId() == 0L) {
            return this.add(newGrp);
        }

        if (!checkBoundary(newGrp.getId()))
            return null;

        int intId = (int) newGrp.getId();
        this.groups.set(intId, newGrp); // Replace over

        return new Group(newGrp);
    }


    @Override
    public synchronized Group update(
        Group newGrp)
        throws UpdateConflictException
    {
        if (!checkBoundary(newGrp.getId()))
            return null;

        int intId = (int) newGrp.getId();
        Group oldGrp = this.groups.get(intId);
        if (oldGrp == null)
        {
            return null;
        }

        if (oldGrp.getUpdateTime().compareTo(newGrp.getUpdateTime()) != 0) {
            throw new UpdateConflictException(
                "Update conflict - specified update time: "
                    + newGrp.getUpdateTime()
                    + "  current update time: "
                    + oldGrp.getUpdateTime());
        }

        Group upGrp = new Group(newGrp);
        upGrp.setUpdateTime(new Date());
        this.groups.set(intId, upGrp);

        // Return a clone, so that the stored one won't be modifiable
        return new Group(upGrp);
    }


    @Override
    public void visitAll(
        Visitor<Group> visitor)
    {
        if (!visitor.before())
            return;

        // We don't need to worry about outOfBoundary because
        // delete will only nullify items instead of remove
        // Also, we don't use iterator to avoid ConcurrentModificationException
        int num = this.groups.size();
        for (int i = 1; i < num; ++i) {
            // Maybe stale in concurrent system
            Group grp = this.groups.get(i);
            
            if (grp == null)
                continue;
            
            if (!visitor.visit(grp))
                break; // Visitor aborts
        }
        
        // Cleanup
        visitor.after();
    }


    @Override
    public synchronized void exportData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.exists(path))
        {
            LOG.warn("Export file \"{}\" already exists", fname);
        }

        LOG.info("Exporting groups to file {}", fname);
        try (
             OutputStream os =
                 Files.newOutputStream(path, CREATE, WRITE, TRUNCATE_EXISTING);
             BufferedOutputStream bos =
                 new BufferedOutputStream(os);)
        {
            final Iterator<Group> it = this.groups.iterator();

            GroupExporter exporter = new GroupExporter();
            exporter.write(bos, new AbstractIterator<GroupExporterEntry>() {

                @Override
                protected GroupExporterEntry computeNext()
                {
                    if (!it.hasNext())
                        return endOfData();

                    Group g = it.next();
                    if (g == null)
                        return null;

                    GroupCount gc = getCount(g.getId());

                    GroupExporterEntry e = new GroupExporterEntry();
                    e.coverImageUrl = g.getCoverImageUrl();
                    e.createTime = g.getCreateTime();
                    e.id = g.getId();
                    e.name = g.getName();
                    e.numDownvotes = gc.getNumDownvotes();
                    e.numPosts = gc.getNumPosts();
                    e.numUpvotes = gc.getNumUpvotes();
                    e.ruleDescriptionImageUrl = g.getRuleDescriptionImageUrl();
                    e.ruleDescriptionText = g.getRuleDescriptionText();
                    if (g.getRuleJson() != null)
                        e.ruleJson = g.getRuleJson().toString();
                    e.updateTime = g.getUpdateTime();
                    return e;
                }
            });
        }
        LOG.info("Done exporting groups");
    }


    @Override
    public synchronized void importData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.notExists(path)) {
            throw new IllegalArgumentException("Import file " + fname + " not exist");
        }

        LOG.info("Imporing groups from file {}", fname);
        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            GroupExporter exporter = new GroupExporter();
            Iterator<GroupExporterEntry> it = exporter.read(bis);

            // Wipe out current data
            this.groups.clear();
            this.groupCounts.clear();
            this.groups.add(null); // ID 0 is always null
            this.groupCounts.add(null);

            int i = 1;
            while (it.hasNext())
            {
                GroupExporterEntry e = it.next();

                // The in-memory list adds nulls for deleted/missing posts
                // Here, we need fill up the gap during import
                for (int j = i; j < e.id; ++j)
                {
                    this.groups.add(null);
                    this.groupCounts.add(null);
                }

                Group g = new Group();
                g.setCoverImageUrl(e.coverImageUrl);
                g.setCreateTime(e.createTime);
                g.setId(e.id);
                g.setName(e.name);
                g.setRuleDescriptionImageUrl(e.ruleDescriptionImageUrl);
                g.setRuleDescriptionText(e.ruleDescriptionText);
                if (StringUtils.isNotEmpty(e.ruleJson))
                    g.setRuleJson(new JSONObject(e.ruleJson));
                g.setUpdateTime(e.updateTime);
                this.groups.add(g);

                GroupCount gc = new GroupCount();
                gc.setNumDownvotes(e.numDownvotes);
                gc.setNumPosts(e.numPosts);
                gc.setNumUpvotes(e.numUpvotes);
                this.groupCounts.add(gc);
            }
        }
        LOG.info("Done importing groups");
    }


    @Override
    public synchronized GroupCount getCount(
        Long id)
    {
        if (!checkBoundary(id))
            return null;

        int intId = id.intValue();
        GroupCount count = this.groupCounts.get(intId);
        if (count == null) {
            return new GroupCount();
        }
        return count;
    }


    @Override
    public synchronized void incrCount(
        Long groupId,
        Field count,
        long incr)
    {
        GroupCount c = getCount(groupId);
        if (c == null)
            return;

        switch (count)
        {
        case DOWNVOTE:
            c.setNumDownvotes(c.getNumDownvotes() + incr);
            break;
        case POST:
            c.setNumPosts(c.getNumPosts() + incr);
            break;
        case UPVOTE:
            c.setNumUpvotes(c.getNumUpvotes() + incr);
            break;
        }
    }


    @Override
    public List<Group> find(
        Page page,
        Filter<Group> filter)
    {
        final String key =
            StringUtils.defaultIfEmpty(
                page.key,
                SK_ID);
        switch (key)
        {
        case SK_ID:
            Long boundary =
                Strings.isNullOrEmpty(page.boundary) ? null : Long.valueOf(page.boundary);
            boolean inBound = checkBoundary(boundary);
            int size = this.groups.size();

            // 1. determine startId;
            int startId;
            boolean decreasing; // order of traversal
            if (page.isDescending())
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
            }
            else
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
            }

            // 2. do traversal
            List<Group> result = new ArrayList<>();
            int num = 0;
            for (int id = startId; (decreasing ? id > 0 : id < size);)
            {
                Group p = this.groups.get(id);
                id = decreasing ? id - 1 : id + 1;

                if (p == null)
                    continue;

                if (filter != null && !filter.accept(p))
                    continue;

                // Copy over, since this is resident (in-memory) data structure
                Group c = new Group(p);
                c.setSortKey(key);
                c.setSortVal(String.valueOf(c.getId()));
                result.add(c);
                if (++num >= page.size)
                    break;
            }

            // 3. in BEFORE, we traverse in a reverse direction of page.order
            if (!page.isAfter())
                Collections.reverse(result);
            return result;

        default:
            throw new UnsupportedOperationException("Unsupported sort key " + key);
        }
    }


    //
    // INTERNAL METHODS
    //

    protected boolean checkBoundary(
        Long id)
    {
        // Internally, we store data by using an array with integer
        // position as index. The id can't go beyond boundary
        return (id != null &&
            id > 0 && id < this.groups.size());
    }
}
