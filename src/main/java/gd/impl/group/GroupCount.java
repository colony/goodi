package gd.impl.group;

public class GroupCount
{
    private long numDownvotes;
    
    private long numPosts;
    
    private long numUpvotes;
    
    
    public static enum Field
    {
        DOWNVOTE,
        POST,
        UPVOTE
    }
    
    
    public GroupCount()
    {
    }


    public long getNumDownvotes()
    {
        return numDownvotes;
    }


    public void setNumDownvotes(
        long numDownvotes)
    {
        this.numDownvotes = numDownvotes;
    }


    public long getNumPosts()
    {
        return numPosts;
    }


    public void setNumPosts(
        long numPosts)
    {
        this.numPosts = numPosts;
    }


    public long getNumUpvotes()
    {
        return numUpvotes;
    }


    public void setNumUpvotes(
        long numUpvotes)
    {
        this.numUpvotes = numUpvotes;
    }
}
