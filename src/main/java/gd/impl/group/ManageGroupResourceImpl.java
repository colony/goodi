
package gd.impl.group;


import gd.api.v1.admin.ManageGroupForm;
import gd.api.v1.admin.ManageGroupResource;
import gd.api.v1.group.GroupRep;
import gd.api.v1.group.GroupRepList;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.group.Group.Rule;
import gd.impl.media.FileManager;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.Page;
import gd.impl.util.UpdateConflictException;
import gd.support.jersey.LongParam;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringEscapeUtils;


@Path("/adm/groups")
@Singleton
public class ManageGroupResourceImpl
    implements
        ManageGroupResource
{
    protected FileManager fileMgr;

    protected GroupRepository grpRepo;


    public ManageGroupResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.grpRepo = repoMgr.get(RepositoryType.GROUP, GroupRepository.class);
        this.fileMgr = FileManager.instance();
    }


    @Override
    @POST
    @Path("/assign")
    @Consumes("application/x-www-form-urlencoded")
    public Response assignGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @FormParam("post_id") LongParam postIdParam,
        @FormParam("group_id") LongParam groupIdParam)
    {
        Rests.beginAdminOperation(headers, "adm:mgroup:assign:" + postIdParam.get() + ":" + groupIdParam.get());
        Groups.assignGroup(postIdParam.get(), groupIdParam.get());
        return Response.ok().build();
    }


    @Override
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createOrModifyGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam ManageGroupForm groupForm)
    {
        String desc = "";
        if (null != groupForm.getId())
            desc = "modify:" + groupForm.getId();
        else
            desc = "create";
        Rests.beginAdminOperation(headers, "adm:mgroup:" + desc);

        Group returnGroup = null;
        Long id = groupForm.getId();
        boolean isUpdate = (id != null);
        if (isUpdate) {
            // Update
            Group group = this.grpRepo.get(id);
            if (group == null)
                return Response.status(Status.NOT_FOUND)
                    .entity("Group not found id=" + id)
                    .build();

            // Only update not-null fields of the form
            String oldCoverImageKey = null;
            String newCoverImageKey = null;
            if (null != groupForm.getCoverImageUrl()) {
                oldCoverImageKey = group.getCoverImageUrl();
                newCoverImageKey = groupForm.getCoverImageUrl();
            }
            else if (null != groupForm.getCoverImageStream()) {
                oldCoverImageKey = group.getCoverImageUrl();
                newCoverImageKey = "static/group/" + this.fileMgr.computeRandomKey();
                try {
                    this.fileMgr.storeWithKey(
                        groupForm.getCoverImageStream(),
                        null,
                        newCoverImageKey);
                }
                catch (IOException e) {
                    throw new WebApplicationException("Fail to update file for group cover image", e);
                }
            }

            if (null != newCoverImageKey)
                group.setCoverImageUrl(newCoverImageKey);

            if (null != oldCoverImageKey)
                this.fileMgr.deleteQuietly(oldCoverImageKey);

            if (null != groupForm.getCoverImageStream()) {
                // replace image
                String oldCoverImageUrl = group.getCoverImageUrl();
                String coverImageKey = "static/" + this.fileMgr.computeRandomKey();
                try {
                    this.fileMgr.storeWithKey(
                        groupForm.getCoverImageStream(),
                        null,
                        coverImageKey);
                    this.fileMgr.deleteQuietly(oldCoverImageUrl);
                    group.setCoverImageUrl(coverImageKey);
                }
                catch (IOException e) {
                    throw new WebApplicationException("Fail to update file for group cover image", e);
                }
            }
            if (null != groupForm.getName())
                group.setName(groupForm.getName());
            if (null != groupForm.getRuleImageUrl())
                group.setRuleDescriptionImageUrl(groupForm.getRuleImageUrl());
            if (null != groupForm.getRuleText()) {
                String text = StringEscapeUtils.unescapeJava(groupForm.getRuleText());
                group.setRuleDescriptionText(text);
            }
            if (null != groupForm.getAllowedPostTypes()) {
                group.setRule(Rule.POST_TYPE_RULE, Arrays.asList(groupForm.getAllowedPostTypes().split(",")));
            }
            if (null != groupForm.getIsReversed()) {
                group.setRule(Rule.SORT_RULE, groupForm.getIsReversed().get() ? "ID:ASC" : "ID:DESC");
            }
            if (null != groupForm.getIsPrivate()) {
                group.setRule(Rule.PRIVACY_RULE, groupForm.getIsPrivate().get() ? "1" : "0");
            }
            if (null != groupForm.getThreadType()) {
                group.setRule(Rule.THREAD_TYPE_RULE, groupForm.getThreadType());
            }
            try {
                returnGroup = this.grpRepo.update(group);
            }
            catch (UpdateConflictException e) {
                throw new WebApplicationException("Fail to update group ID=" + id, e);
            }
        }
        else {
            // Create
            Group group = new Group();

            String coverImageKey = null;
            if (null != groupForm.getCoverImageUrl()) {
                coverImageKey = groupForm.getCoverImageUrl();
            }
            else if (null != groupForm.getCoverImageStream()) {
                coverImageKey = "static/group/" + this.fileMgr.computeRandomKey();
                try {
                    this.fileMgr.storeWithKey(
                        groupForm.getCoverImageStream(),
                        null,
                        coverImageKey);
                }
                catch (IOException e) {
                    throw new WebApplicationException("Fail to upload file for group cover image", e);
                }
            }
            else {
                return Response.status(Status.BAD_REQUEST).entity("Can't create group without cover image").build();
            }
            group.setCoverImageUrl(coverImageKey);
            group.setName(groupForm.getName());
            group.setRuleDescriptionImageUrl(groupForm.getRuleImageUrl());
            if (null != groupForm.getRuleText()) {
                String text = StringEscapeUtils.unescapeJava(groupForm.getRuleText());
                group.setRuleDescriptionText(text);
            }
            if (null != groupForm.getAllowedPostTypes()) {
                group.setRule(Rule.POST_TYPE_RULE, Arrays.asList(groupForm.getAllowedPostTypes().split(",")));
            }
            if (null != groupForm.getIsReversed()) {
                group.setRule(Rule.SORT_RULE, groupForm.getIsReversed().get() ? "ID:ASC" : "ID:DESC");
            }
            if (null != groupForm.getIsPrivate()) {
                group.setRule(Rule.PRIVACY_RULE, groupForm.getIsPrivate().get() ? "1" : "0");
            }
            if (null != groupForm.getThreadType()) {
                group.setRule(Rule.THREAD_TYPE_RULE, groupForm.getThreadType());
            }
            returnGroup = this.grpRepo.add(group);
        }

        // We made modification here, clear the groupPickList cache, so that
        // next time, client needs it, it will recompute the newest list from
        // database and update the cache accordingly
        Groups.clearGroupPickListCache();

        GroupRep rep = new GroupRep();
        GroupCount groupCount = isUpdate ? this.grpRepo.getCount(id) : new GroupCount();
        Reps.populateGroupAdmin(rep, returnGroup, groupCount);
        return Response.ok(rep).build();
    }


    @Override
    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deleteGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginAdminOperation(headers, "adm:mgroup:del:" + idParam.get());
        this.grpRepo.delete(idParam.get());
        //TODO: delete posts of the group??
        return Response.ok().build();
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getGroupList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo)
    {
        Page p = new Page(GroupRepository.SK_ID);
        p.size = 100; // Get all groups
        List<Group> groupList = this.grpRepo.find(p, null);
        GroupRepList repList = new GroupRepList();
        Reps.populateGroupListBasic(repList, groupList, true);
        return Response.ok(repList).build();
    }
}
