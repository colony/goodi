
package gd.impl.group;


import gd.support.protobuf.GroupProtos.GroupProto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.Conversion;

import com.google.common.collect.AbstractIterator;


public class GroupExporter
{
    public static final int HEADER_MAGIC = 0x19890828;

    public static final int HEADER_VERSION = 1;

    private GroupProto.Builder builder;

    private int version;


    public GroupExporter()
    {
        this.builder = GroupProto.newBuilder();
        this.version = HEADER_VERSION;
    }


    /**
     * Construct an exporter with specified version
     */
    public GroupExporter(
        int version)
    {
        this.builder = GroupProto.newBuilder();
        this.version = version;
    }


    public Iterator<GroupExporterEntry> read(
        InputStream in)
        throws IOException
    {
        readHeader(in);
        switch (this.version)
        {
        case 1:
            return new V1Iterator(in);

        default:
            throw new IllegalStateException(
                "Unsupported version number: " + this.version);
        }
    }


    private int readHeader(
        InputStream in)

        throws IOException
    {
        byte[] b = new byte[4];
        in.read(b);
        int magic = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (magic != HEADER_MAGIC)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid magic number 0x%08x, expected 0x%08x",
                    magic,
                    HEADER_MAGIC));
        }

        b = new byte[4];
        in.read(b);
        int version = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (version <= 0 || version > HEADER_VERSION)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid version number %d, expected %d",
                    version,
                    HEADER_VERSION));
        }

        this.version = version;
        return version;
    }



    public void write(
        OutputStream out,
        Iterator<GroupExporterEntry> it)
        throws IOException
    {
        writeHeader(out);

        while (it.hasNext()) {
            GroupExporterEntry c = it.next();
            if (c == null)
                continue;

            this.writeGroup(out, c);
        }
    }
    
    
    private void writeGroup(
        OutputStream out,
        GroupExporterEntry g)
        throws IOException
    {
        GroupProto.Builder b = this.builder;

        // Clear inner state before reuse
        b.clear();

        // Some fields has default value and required, we don't do NULL sanity check
        b.setCreateTime(g.createTime.getTime());
        if (null != g.coverImageUrl)
            b.setCoverImageUrl(g.coverImageUrl);
        b.setId(g.id);
        b.setName(g.name);
        b.setNumDownvotes(g.numDownvotes);
        b.setNumPosts(g.numPosts);
        b.setNumUpvotes(g.numUpvotes);
        if (null != g.ruleDescriptionImageUrl)
            b.setRuleDescriptionImageUrl(g.ruleDescriptionImageUrl);
        if (null != g.ruleDescriptionText)
            b.setRuleDescriptionText(g.ruleDescriptionText);
        if (null != g.ruleJson)
            b.setRuleJson(g.ruleJson);
        if (null != g.updateTime)
            b.setUpdateTime(g.updateTime.getTime());

        b.build().writeDelimitedTo(out);
    }


    private void writeHeader(
        OutputStream out)

        throws IOException
    {
        out.write(Conversion.intToByteArray(
            HEADER_MAGIC,
            0,
            new byte[4],
            0,
            4));
        out.write(Conversion.intToByteArray(
            this.version,
            0,
            new byte[4],
            0,
            4));
    }



    class V1Iterator
        extends
            AbstractIterator<GroupExporterEntry>
    {
        InputStream in;


        public V1Iterator(
            InputStream in)
        {
            this.in = in;
            builder.clear();
        }


        @Override
        protected GroupExporterEntry computeNext()
        {
            GroupProto.Builder b = builder;
            boolean success;
            try
            {
                success = b.mergeDelimitedFrom(in);

                if (!success)
                {
                    // EOF or some other reason
                    return endOfData();
                }

                // Deserialize using Google protobuf
                GroupProto cp = b.build();
                GroupExporterEntry e = new GroupExporterEntry();
                
                e.createTime = new Date(cp.getCreateTime());
                e.coverImageUrl = cp.getCoverImageUrl();
                e.id = cp.getId();
                e.name = cp.getName();
                e.numDownvotes = cp.getNumDownvotes();
                e.numPosts = cp.getNumPosts();
                e.numUpvotes = cp.getNumUpvotes();
                e.ruleDescriptionImageUrl = cp.getRuleDescriptionImageUrl();
                e.ruleDescriptionText = cp.getRuleDescriptionText();
                e.ruleJson = cp.getRuleJson();
                if (cp.hasUpdateTime())
                    e.updateTime = new Date(cp.getUpdateTime());

                // Clean inner state of builder before next mergeFrom
                b.clear();
                return e;
            }
            catch (IOException e)
            {
                throw new RuntimeException(
                    "Error occurs when importing the data: "
                        + e.getMessage(),
                    e);
            }
        }
    }
    
    
    static class GroupExporterEntry
    {
        public Date createTime;
        public String coverImageUrl;
        public long id;
        public String name;
        public long numDownvotes;
        public long numPosts;
        public long numUpvotes;
        public String ruleDescriptionImageUrl;
        public String ruleDescriptionText;
        public String ruleJson;
        public Date updateTime;
    }
}
