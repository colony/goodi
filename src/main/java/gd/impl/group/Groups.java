
package gd.impl.group;


import gd.api.v1.user.InitializationRep.InitGroupRep;
import gd.impl.cache.C.SSCache;
import gd.impl.cache.CacheManager;
import gd.impl.cache.CacheType;
import gd.impl.cache.Caches;
import gd.impl.config.Configs;
import gd.impl.config.DetailedConfigs;
import gd.impl.config.SystemSetting;
import gd.impl.index.IDX.LDEntry;
import gd.impl.index.IDX.LDIndex;
import gd.impl.index.IndexType;
import gd.impl.index.Indexes;
import gd.impl.post.Post;
import gd.impl.post.PostCount;
import gd.impl.post.PostRepository;
import gd.impl.post.PostStatus;
import gd.impl.post.Posts;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SSRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class Groups
{
    private static final Logger LOG = LoggerFactory.getLogger(Groups.class);

    private static SSCache _anythingCache;

    private static SSRelation _anythingRel;

    private static GroupRepository _groupRepo;

    private static PostRepository _postRepo;


    public static void assignGroup(
        long postId,
        long groupId)
    {
        Post post = Posts.get(postId);
        if (post == null)
            return;

        Long oldGroupId = post.getGroupId();
        if (oldGroupId == groupId)
            return; // No group change, bail out

        Group newGroup = get(groupId);
        if (newGroup == null)
            return;

        if (newGroup.isPrivate()) {
            throw new RuntimeException("Not allowed to assign post " + postId + " to private group " + groupId);
        }

        // Update groupId first
        post.setGroupId(groupId);
        try {
            _postRepo.update(post);
        }
        catch (UpdateConflictException e) {
            LOG.error("Fail to assign groupId {} on postId {}", groupId, postId, e);
            return;
        }

        // Change counters, We need be careful with special group ID such 0 and 1
        PostCount pc = Posts.getCount(postId);
        if (pc.getNumDownvotes() != 0L) {
            if (oldGroupId > 1L)
                _groupRepo.incrCount(oldGroupId, GroupCount.Field.DOWNVOTE, -1 * pc.getNumDownvotes());
            if (groupId > 1L)
                _groupRepo.incrCount(groupId, GroupCount.Field.DOWNVOTE, pc.getNumDownvotes());
        }
        if (pc.getNumUpvotes() != 0L) {
            if (oldGroupId > 1L)
                _groupRepo.incrCount(oldGroupId, GroupCount.Field.UPVOTE, -1 * pc.getNumUpvotes());
            if (groupId > 1L)
                _groupRepo.incrCount(groupId, GroupCount.Field.UPVOTE, pc.getNumUpvotes());
        }
        if (oldGroupId > 1L)
            _groupRepo.incrCount(oldGroupId, GroupCount.Field.POST, -1L);
        if (groupId > 1L)
            _groupRepo.incrCount(groupId, GroupCount.Field.POST, 1L);

        // Change index entry if any
        LDIndex pbrp = Indexes.get(IndexType.POSTS_BY_RECENT_POPULARITY, LDIndex.class);
        LDEntry entry = pbrp.getEntry(postId);
        if (entry != null) {
            entry.s2 = String.valueOf(groupId);
            pbrp.put(entry);
        }
        LDIndex pigp = Indexes.get(IndexType.POSTS_IN_GROUP_BY_POPULARITY, LDIndex.class);
        if (groupId <= 1L) {
            pigp.delete(postId);
        }
        else {
            entry = pigp.getEntry(postId);
            if (entry != null) {
                entry.s2 = String.valueOf(groupId);
                pigp.put(entry);
            }
            else {
                // We are moving from oldGroupId (1L) to newGroupId (>1L), unfortunately, for now
                // pigp index doesn't have it yet, wait until indexRebuilder kicks off, hence, we
                // don't do anything here
            }
        }
    }


    /**
     * There is a dedicated group for BLOCKED post, use this method
     * to assign or unassign BLOCKED or UNBLOCKED from the special
     * group. The method will first check post's status, make sure
     * caller update post's status before calling this method
     */
    public static void assignBlockGroup(
        long postId,
        boolean assigned)
    {
        final long groupIdForBlock = 2L; //TODO, make it configurable

        Post post = Posts.get(postId);
        if (post == null)
            return;

        if (assigned ^ post.hasStatus(PostStatus.Status.BLOCKED))
            return;

        if (assigned) {
            // We need store previous groupId (if not 1L) to store it later
            // because, we might unblock it back to original group
            long oldGroupId = post.getGroupId();
            if (oldGroupId == groupIdForBlock)
                return;

            if (oldGroupId != 1L) {
                SEntry node = new SEntry(String.valueOf(postId)).with(oldGroupId);
                _anythingRel.addEdge(Relations.ANYTHING_BLOCKED_PREVIOUS_GROUPID, node);
            }
            assignGroup(postId, groupIdForBlock);
        }
        else {
            // Recover from previous stored groupId
            SEntry node = _anythingRel.getEdge(Relations.ANYTHING_BLOCKED_PREVIOUS_GROUPID, String.valueOf(postId));
            if (node != null) {
                assignGroup(postId, node.l1);
                _anythingRel.deleteEdge(Relations.ANYTHING_BLOCKED_PREVIOUS_GROUPID, String.valueOf(postId));
            }
            else {
                assignGroup(postId, 1L);
            }
        }
    }


    public static void clearGroupPickListCache()
    {
        _anythingCache.remove(Caches.ANYTHING_GROUP_PICK_LIST);
    }


    public static Group get(
        Long id)
    {
        if (id <= 0L)
            return null;
        return _groupRepo.get(id);
    }


    public static Group getByPost(
        long postId)
    {
        Post p = Posts.get(postId);
        if (p != null) {
            return get(p.getGroupId());
        }
        return null;
    }


    public static List<InitGroupRep> getGroupPickList()
    {
        List<InitGroupRep> result = new ArrayList<>();

        String value = _anythingCache.get(Caches.ANYTHING_GROUP_PICK_LIST);
        if (value == null) {
            final List<Group> list = new ArrayList<>();
            final List<Long> orderGroupIds = DetailedConfigs.getGroupDisplayOrders();
            final boolean generalGroupEnabled = Configs.getBool(SystemSetting.GENERAL_GROUP_ENALBED, true);
            _groupRepo.visitAll(new Visitor<Group>() {

                @Override
                public boolean visit(
                    Group item)
                {
                    if (item.getId() == 1L && !generalGroupEnabled)
                        return true;
                    
                    //TODO: plz not hardcode T.T
                    // group 2 is moderation group, we can't put it into pick list
                    if (item.getId() == 2L)
                        return true;
                    
                    if (!orderGroupIds.contains(item.getId())) {
                        // If the group is not displayed, don't bother show it in pickList
                        return true;
                    }
                    list.add(item);
                    return true;
                }
            });

            Collections.sort(list, new Comparator<Group>() {
                @Override
                public int compare(
                    Group o1,
                    Group o2)
                {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });

            
            StringBuilder sb = new StringBuilder();
            for (Group g : list) {
                sb.append(g.getId());
                sb.append(",");
                sb.append(g.getName());
                sb.append(",");
                sb.append(g.getAllowedPostTypesCode());
                sb.append(",");

                InitGroupRep igr = new InitGroupRep();
                igr.id = g.getId();
                igr.name = g.getName();
                igr.allowedPostTypesCode = g.getAllowedPostTypesCode();
                result.add(igr);
            }
            if (sb.length() > 0)
                sb.deleteCharAt(sb.length() - 1); // Remove last camma

            value = sb.toString();
            _anythingCache.put(Caches.ANYTHING_GROUP_PICK_LIST, value);
        }
        else {
            // Decode the cached value
            String[] tokens = value.split(",");
            for (int i = 0; i < tokens.length; i += 3) {
                InitGroupRep igr = new InitGroupRep();
                igr.id = Long.valueOf(tokens[i]);
                igr.name = tokens[i + 1];
                igr.allowedPostTypesCode = Integer.parseInt(tokens[i + 2]);
                result.add(igr);
            }
        }
        return result;
    }


    public static void init()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        _groupRepo = repoMgr.get(RepositoryType.GROUP, GroupRepository.class);
        _postRepo = repoMgr.get(RepositoryType.POST, PostRepository.class);

        CacheManager cacheMgr = CacheManager.instance();
        _anythingCache = cacheMgr.get(CacheType.ANYTHING, SSCache.class);

        RelationManager relMgr = RelationManager.instance();
        _anythingRel = relMgr.get(RelationType.ANYTHING, SSRelation.class);
    }
}
