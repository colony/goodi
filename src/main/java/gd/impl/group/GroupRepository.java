
package gd.impl.group;


import gd.impl.repository.Repository;
import gd.impl.repository.RepositoryAdmin;
import gd.impl.util.Filter;
import gd.impl.util.Page;

import java.util.List;


public interface GroupRepository
    extends
        Repository<Long,Group>,
        RepositoryAdmin
{
    // Sort groups by ID
    public static final String SK_ID = "ID";


    public GroupCount getCount(
        Long groupId);


    public void incrCount(
        Long groupId,
        GroupCount.Field count,
        long incr);


    public List<Group> find(
        Page page,
        Filter<Group> filter);
}
