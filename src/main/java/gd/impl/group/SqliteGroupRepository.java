
package gd.impl.group;


import gd.impl.SQLiteManager;
import gd.impl.group.GroupCount.Field;
import gd.impl.repository.RepositoryConfig;
import gd.impl.repository.SqliteRepository;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableObject;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


/**
 * Schema (group):
 * <ul>
 * <li>cover_image_url
 * <li>create_time
 * <li>id
 * <li>name
 * <li>rule_description_image_url
 * <li>rule_description_text
 * <li>rule_json
 * <li>update_time
 * </ul>
 * 
 * Schema (group_counter):
 * <ul>
 * <li>group_id
 * <li>num_downvotes
 * <li>num_posts
 * <li>num_upvotes
 * </ul>
 */
public class SqliteGroupRepository
    implements
        GroupRepository,
        SqliteRepository
{
    private static final Logger LOG = LoggerFactory.getLogger(SqliteGroupRepository.class);

    private int batchSize = 100;

    private String tableName;


    public SqliteGroupRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    //
    // Repository
    //

    @Override
    public Group add(
        Group t)
    {
        String sql =
            "INSERT INTO "
                + this.tableName
                + " (cover_image_url, create_time, name, rule_description_image_url, rule_description_text, rule_json, update_time)"
                + " VALUES "
                + "(?, ?, ?, ?, ?, ?, ?)";

        Group newGrp = new Group(t);

        // It is allowed to specify createTime and updateTime from elsewhere (i.e. admin console)
        if (newGrp.getCreateTime() == null)
            newGrp.setCreateTime(new Date());
        if (newGrp.getUpdateTime() == null)
            newGrp.setUpdateTime(new Date());

        // SQL statement parameters construction
        Object param[] = new Object[7];
        param[0] = newGrp.getCoverImageUrl();
        param[1] = newGrp.getCreateTime().getTime();
        param[2] = newGrp.getName();
        param[3] = newGrp.getRuleDescriptionImageUrl();
        param[4] = newGrp.getRuleDescriptionText();
        if (newGrp.getRuleJson() != null)
            param[5] = newGrp.getRuleJson().toString();
        param[6] = newGrp.getUpdateTime().getTime();

        // Execute SQL
        long newId = SqliteUtils.executeInsertQuery(sql, param, "addGroup", true);

        newGrp.setId(newId);

        // Execute rest queries
        // 1. By default (SQL schema), all counters are ZEROS
        String sql1 =
            "INSERT INTO group_counter (group_id) VALUES (?)";
        SqliteUtils.executeInsertQuery(sql1, new Object[] {
            newId
        }, "addGroupCounter", false);

        return newGrp;
    }


    @Override
    public boolean delete(
        Long id)
    {
        String sql =
            "DELETE FROM "
                + this.tableName
                + " WHERE id = "
                + id;
        boolean success = SqliteUtils.executeQuery(sql, null, "DeleteGroup");
        if (success) {
            // Delete counter
            String sql2 = "DELETE FROM group_counter WHERE group_id = " + id;
            SqliteUtils.executeQuery(sql2, null, "DeleteGroupCounter");
        }

        return success;
    }


    @Override
    public boolean exists(
        Long id)
    {
        final String sql = "SELECT 1 FROM " + this.tableName + " WHERE id=?";
        Object[] param = {
                id
        };
        final MutableBoolean mb = new MutableBoolean(false);

        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "isGroupExist", new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }

            });
        return mb.booleanValue();
    }


    @Override
    public Group get(
        Long id)
    {
        final String sql = "SELECT * FROM " + this.tableName + " WHERE id=?";
        final MutableObject<Group> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            new Object[] {
                id
            },
            "getPost",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        Group group = new Group();
                        parseResultSet(group, rs);
                        mo.setValue(group);
                    }
                }
            });
        return mo.getValue();
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        createGroupTable(config);
        createGroupCounterTable(config);
        this.batchSize = config.getBatchSize();
        this.tableName = config.getTableName();
    }


    @Override
    public Group put(
        Group t)
    {
        Group upGroup = new Group(t);

        // For replace into, any not specified column will be nullified if the row exists
        String sql =
            "INSERT OR REPLACE INTO "
                + this.tableName
                + " (cover_image_url, create_time, id, name, rule_description_image_url, rule_description_text, rule_json, update_time)"
                + " VALUES"
                + " (?,?,?,?,?,?,?,?)";

        Object param[] = new Object[8];
        param[0] = upGroup.getCoverImageUrl();
        if (null == upGroup.getCreateTime())
            throw new IllegalArgumentException("Try to put a group with no create_time");
        param[1] = upGroup.getCreateTime().getTime();
        if (0L == upGroup.getId())
            throw new IllegalArgumentException("Try to put a group with ID 0");
        param[2] = upGroup.getId();
        param[3] = upGroup.getName();
        param[4] = upGroup.getRuleDescriptionImageUrl();
        param[5] = upGroup.getRuleDescriptionText();
        if (null != upGroup.getRuleJson())
            param[6] = upGroup.getRuleJson().toString();
        if (null != upGroup.getUpdateTime())
            param[7] = upGroup.getUpdateTime().getTime();

        SqliteUtils.executeQuery(sql, param, "putGroup");
        return upGroup;
    }


    @Override
    public Group update(
        Group t)
        throws UpdateConflictException
    {
        // MODIFIABLE fields:
        // cover_image_url, name, rule_description_image_url, rule_description_text, rule_json

        // NON-MODIFIABLE fields:
        // create_time, id

        // update_time is modifiable, but we also use the old data as optimistic version lock

        Group upGroup = new Group(t);

        Date oldUpdateTime = t.getUpdateTime();
        Date newUpdateTime = new Date();
        upGroup.setUpdateTime(newUpdateTime);
        Object[] param = new Object[9];

        // Prepare a query that updates mutable fields
        String updateSql =
            "UPDATE "
                + this.tableName
                + " SET cover_image_url=?, name=?, rule_description_image_url=?, rule_description_text=?, rule_json=?, update_time=? ";
        param[0] = upGroup.getCoverImageUrl();
        param[1] = upGroup.getName();
        param[2] = upGroup.getRuleDescriptionImageUrl();
        param[3] = upGroup.getRuleDescriptionText();
        if (null != upGroup.getRuleJson())
            param[4] = upGroup.getRuleJson().toString();
        param[5] = newUpdateTime.getTime();

        // Prepare a query that matches an immutable fields
        String querySql = "WHERE id=? AND create_time=? AND update_time=?";
        param[6] = upGroup.getId();
        param[7] = upGroup.getCreateTime().getTime();
        param[8] = oldUpdateTime.getTime();

        String sql = updateSql + querySql;
        boolean success =
            SqliteUtils.executeQuery(sql, param, "updateGroup");
        if (!success)
        {
            // Find out if the entity exists
            Group group = this.get(upGroup.getId());
            if (group == null)
            {
                return null;
            }

            if (group.getUpdateTime().compareTo(oldUpdateTime) != 0)
                throw new UpdateConflictException(
                    "Update conflict - specified update time: "
                        + oldUpdateTime
                        + "  current update time: "
                        + group.getUpdateTime());

            // If we make it here, it must be the case that an immutable field was changed
            throw new IllegalArgumentException(
                "Attempt to modify an immutable field");
        }
        return upGroup;
    }


    @Override
    public void visitAll(
        Visitor<Group> visitor)
    {
        if (!visitor.before())
            return;

        SQLiteManager sqliteManager = SQLiteManager.instance();
        Connection connection = null;
        PreparedStatement statement = null;

        boolean hasMore = true;
        boolean aborted = false;
        long id = 0;

        try {
            connection = sqliteManager.getConnection();
            final String sql = "SELECT * FROM " + this.tableName + " WHERE id>? LIMIT " + this.batchSize;
            statement = connection.prepareStatement(sql);
            while (hasMore && !aborted) {
                statement.setLong(1, id);
                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next()) {
                    count++;
                    id = rs.getLong("id");

                    Group group = new Group();
                    parseResultSet(group, rs);
                    if (!visitor.visit(group))
                    {
                        aborted = true;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();
            }
        }
        catch (Exception e) {
            LOG.error("Fail to visitAll groups", e);
        }
        finally {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for visitAll",
                    e);
            }
        }

        visitor.after();
    }


    //
    // GroupRepository
    //


    @Override
    public GroupCount getCount(
        Long groupId)
    {
        final GroupCount gc = new GroupCount();
        String sql = "SELECT * FROM group_counter WHERE group_id = " + groupId;
        SqliteUtils.executeSelectQuery(sql, null, "getGroupCount", new ResultSetParser() {

            @Override
            public void parse(
                ResultSet rs)
                throws SQLException
            {
                if (rs.next()) {
                    gc.setNumDownvotes(rs.getLong("num_downvotes"));
                    gc.setNumPosts(rs.getLong("num_posts"));
                    gc.setNumUpvotes(rs.getLong("num_upvotes"));
                }
            }
        });
        return gc;
    }


    @Override
    public void incrCount(
        Long groupId,
        Field count,
        long incr)
    {
        if (incr == 0L)
            return;
        
        String sql = "";
        switch (count)
        {
        case DOWNVOTE:
            sql = "UPDATE group_counter SET num_downvotes = num_downvotes + ? WHERE group_id = ?";
            break;
        case POST:
            sql = "UPDATE group_counter SET num_posts = num_posts + ? WHERE group_id = ?";
            break;
        case UPVOTE:
            sql = "UPDATE group_counter SET num_upvotes = num_upvotes + ? WHERE group_id = ?";
            break;
        }
        SqliteUtils.executeQuery(sql, new Object[] {
                incr, groupId
        }, "incrGroupCount" + count);
    }


    @Override
    public List<Group> find(
        Page page,
        Filter<Group> filter)
    {
        String key = StringUtils.defaultIfEmpty(page.key, SK_ID).toUpperCase();
        switch (key)
        {
        case SK_ID:
            return this.findById(page, filter);
        default:
            throw new IllegalArgumentException("Unsupported sort key " + key);
        }
    }


    private List<Group> findById(
        Page page,
        Filter<Group> filter)
    {
        // SELECT * 
        // FROM group
        // WHERE id <=> ?
        // ORDER BY id desc|asc
        // LIMIT ?

        final List<Group> groups = new ArrayList<>(page.size);
        String sql;
        if (Strings.isNullOrEmpty(page.boundary)) {
            sql = "SELECT *"
                + " FROM " + this.tableName
                + " ORDER BY id "
                + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                + " LIMIT "
                + page.size;
        }
        else {
            sql = "SELECT *"
                + " FROM " + this.tableName
                + " WHERE id "
                + (page.isDescending() == page.isAfter() ? (page.exclusive ? "<?" : "<=?") : (page.exclusive ? ">?" : ">=?"))
                + " ORDER BY id "
                + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                + " LIMIT "
                + page.size;
        }
        SqliteUtils.executeSelectQuery(
            sql, 
            Strings.isNullOrEmpty(page.boundary) ? null : new Object[] {page.boundary},
            "groupFindById",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next()) {
                        Group g = new Group();
                        parseResultSet(g, rs);
                        g.setSortKey(SK_ID);
                        g.setSortVal(String.valueOf(g.getId()));
                        groups.add(g);
                    }
                }
            });
        
        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */
        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(groups);
        }
        
        return groups;
    }


    //
    // RepositoryAdmin
    //

    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }



    //
    // Table Creation
    //

    private void createGroupTable(
        RepositoryConfig config)
    {
        final String createGroupTableSql =
            "CREATE TABLE IF NOT EXISTS "
                + config.getTableName()
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "cover_image_url TEXT,"
                + "create_time INTEGER,"
                + "name TEXT NOT NULL,"
                + "rule_description_image_url TEXT,"
                + "rule_description_text TEXT,"
                + "rule_json TEXT,"
                + "update_time INTEGER"
                + ")";
        
        SqliteUtils.executeQuery(createGroupTableSql, null, "createGroupTable");
    }


    private void createGroupCounterTable(
        RepositoryConfig config)
    {
        final String createGroupCounterTableSql =
            "CREATE TABLE IF NOT EXISTS group_counter "
                + "(group_id INTEGER PRIMARY KEY NOT NULL,"
                + "num_downvotes INTEGER DEFAULT 0,"
                + "num_posts INTEGER DEFAULT 0,"
                + "num_upvotes INTEGER DEFAULT 0"
                + ")";
        SqliteUtils.executeQuery(createGroupCounterTableSql, null, "createGroupCounterTable");
    }


    //
    // INTERNAL METHODS
    //

    private void parseResultSet(
        Group group,
        ResultSet rs)
        throws SQLException
    {
        group.setCoverImageUrl(rs.getString("cover_image_url"));
        group.setCreateTime(new Date(rs.getLong("create_time")));
        group.setId(rs.getLong("id"));
        group.setName(rs.getString("name"));
        group.setRuleDescriptionImageUrl(rs.getString("rule_description_image_url"));
        group.setRuleDescriptionText(rs.getString("rule_description_text"));
        String ruleJsonStr = rs.getString("rule_json");
        if (StringUtils.isNotEmpty(ruleJsonStr)) {
            JSONObject ruleJson = new JSONObject(ruleJsonStr);
            group.setRuleJson(ruleJson);
        }
        group.setUpdateTime(new Date(rs.getLong("update_time")));
    }
}
