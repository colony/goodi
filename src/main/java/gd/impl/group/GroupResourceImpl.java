
package gd.impl.group;


import gd.api.v1.PaginationRep;
import gd.api.v1.group.GroupRep;
import gd.api.v1.group.GroupRepList;
import gd.api.v1.group.GroupResource;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;


@Path("/groups")
@Singleton
public class GroupResourceImpl
    implements
        GroupResource
{
    private GroupRepository grpRepo;


    public GroupResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.grpRepo = repoMgr.get(RepositoryType.GROUP, GroupRepository.class);
    }


    @Override
    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getGroup(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginOperation(headers, "g:get" + idParam.get());
        long id = idParam.get();
        Group group = this.grpRepo.get(id);

        GDUtils.check(group != null, Status.NOT_FOUND, "No group with ID " + id);

        GroupRep rep = new GroupRep();
        GroupCount count = this.grpRepo.getCount(id);
        Reps.populateGroupBasic(rep, group, count);
        return Response.ok(rep).build();
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getGroupList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam)
    {
        Rests.beginOperation(headers, "g:getList");
        
        Page p = pageParam.get();
        
        // 9/23/15, we temporarily disable pagination function for displaying a group list, instead
        // we load all of current groups at once, the reason is, pagination won't work with our
        // configured displaying order SystemSetting.GROUP_DISPLAY_ORDERS 
        if (p.isDefault()) {
            p.size = 50;
        } else {
            return Response.ok(new GroupRepList()).build();
        }
        
        List<Group> groupList = this.grpRepo.find(p, null);
        GroupRepList repList = new GroupRepList();
        Reps.populateGroupListBasic(repList, groupList, false);

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                pageParam.get(),
                GDUtils.getFirst(groupList),
                GDUtils.getLast(groupList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }
}
