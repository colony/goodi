
package gd.impl.group;


import gd.api.v1.post.PostType;
import gd.api.v1.post.ThreadType;
import gd.impl.util.Sortable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.collect.ImmutableList;


public class Group
    implements
        Sortable
{
    /**
     * Enum describing different rules applied on each group
     */
    public static enum Rule
    {
        /**
         * Rule describing allowed post types. The value:
         * 
         * <pre>
         * A List of string, each is name of PostType enum, except, there's special
         * string "ALL" meaning all PostType is allowed. Note, case-sensitive. The
         * default value is null or empty meaning none of PostType is acceptable in
         * this group
         * </pre>
         */
        POST_TYPE_RULE,

        /**
         * Rule describing settings that is related with privacy
         * issue. If enabled, any poster or commenter's geo
         * information won't be revealed. The value:
         * 
         * <pre>
         * "0" or not set means disabled, "1" means enabled
         * </pre>
         */
        PRIVACY_RULE,

        /**
         * Rule describing the sorting of post, the value will
         * affect how user paginate the posts of this group. The
         * value:
         * 
         * <pre>
         * A string, in format of, SORT_KEY:SORT_ORDER, where SORT_KEY must be one of
         * the predefined SK recognized by GroupRepository and SORT_ORDER is "ASC" or
         * "DESC". The default value is ID:DESC
         * </pre>
         */
        SORT_RULE,

        /**
         * Rule describing type of thread opened when a new post is
         * created in the group, If not set, no thread is opened.
         * The value is {@link ThreadType#name()}
         */
        THREAD_TYPE_RULE,
    }


    private String coverImageUrl;

    private Date createTime;

    private long id;

    private String name;

    private String ruleDescriptionImageUrl;

    private String ruleDescriptionText;

    /**
     * A json object containing rule details where each json key
     * corresponds to enum Rule
     */
    private JSONObject ruleJson;

    private Date updateTime;



    //
    // Sortable
    //

    /**
     * sortKey and sortVal are not persisted. Both fields are
     * normally null unless the post is part of collection (i.e.
     * feed). The fields indicate how the post is sorted.
     */
    private String sortKey;

    private String sortVal;


    /**
     * Default constructor
     */
    public Group()
    {
    }


    /**
     * Copy constructor
     */
    public Group(
        Group g)
    {
        this.coverImageUrl = g.coverImageUrl;
        this.createTime = g.createTime;
        this.id = g.id;
        this.name = g.name;
        this.ruleDescriptionImageUrl = g.ruleDescriptionImageUrl;
        this.ruleDescriptionText = g.ruleDescriptionText;
        this.ruleJson = g.ruleJson;
        this.updateTime = g.updateTime;
    }


    //
    // RULE related
    //

    /**
     * Check if the specified rule exists on the group
     */
    public boolean hasRule(
        Rule r)
    {
        if (this.ruleJson == null)
            return false;
        return this.ruleJson.has(r.name());
    }


    @SuppressWarnings("unchecked")
    public List<String> getAllowedPostTypes()
    {
        return (List<String>) this.getRule(Group.Rule.POST_TYPE_RULE);
    }


    /**
     * The returned int is computed by turn on the bit whose
     * position corresponds to allowed PostType ordinal
     */
    public int getAllowedPostTypesCode()
    {
        int i = 0;
        for (String s : this.getAllowedPostTypes()) {
            if (s.equals("ALL")) {
                i = (int) Math.pow(2, PostType.values().length) - 1;
                break;
            }
            
            if (s.isEmpty()) {
                continue;
            }
            
            PostType pt = PostType.valueOf(s);
            i |= 1 << pt.ordinal();
        }
        return i;
    }


    /**
     * Return details about the specified rule if any. If the rule
     * doesn't exist, it will return default value. Check
     * {@link Rule}.
     */
    public Object getRule(
        Rule r)
    {
        JSONObject json = this.ruleJson;
        if (json == null)
            json = new JSONObject();

        String key = r.name();
        Object val = null;
        switch (r)
        {
        case POST_TYPE_RULE:
            val = json.optJSONArray(key);
            if (val == null) {
                val = ImmutableList.<String> of();
            }
            else {
                JSONArray jsonAry = (JSONArray) val;
                List<String> result = new ArrayList<>();
                for (int i = 0; i < jsonAry.length(); i++) {
                    result.add(jsonAry.getString(i));
                }
                val = result;
            }
            break;

        case PRIVACY_RULE:
            val = json.optString(key, "0");
            break;

        case SORT_RULE:
            val = json.optString(key, "ID:DESC");
            break;

        default:
            val = json.opt(key);
        }
        return val;
    }


    /**
     * Set the rule data. Per rule, has a different object type,
     * check {@link Rule} for details
     */
    @SuppressWarnings("unchecked")
    public void setRule(
        Rule r,
        Object o)
    {
        if (this.ruleJson == null)
            this.ruleJson = new JSONObject();

        switch (r)
        {
        case PRIVACY_RULE: // Expect String
        case SORT_RULE:
        case THREAD_TYPE_RULE:
            this.ruleJson.put(r.name(), o);
            break;

        case POST_TYPE_RULE: // Expect list of string
            JSONArray jsonAry = new JSONArray((List<String>) o);
            this.ruleJson.put(r.name(), jsonAry);
            break;
        }
    }


    //
    // RULE convenient helper
    //

    public boolean isPrivate()
    {
        return this.getRule(Rule.PRIVACY_RULE).equals("1");
    }


    //
    // Getters & Setters
    //

    public String getCoverImageUrl()
    {
        return coverImageUrl;
    }


    public void setCoverImageUrl(
        String coverImageUrl)
    {
        this.coverImageUrl = coverImageUrl;
    }


    public Date getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        Date createTime)
    {
        this.createTime = createTime;
    }


    public long getId()
    {
        return id;
    }


    public void setId(
        long id)
    {
        this.id = id;
    }


    public String getName()
    {
        return name;
    }


    public void setName(
        String name)
    {
        this.name = name;
    }


    public String getRuleDescriptionImageUrl()
    {
        return ruleDescriptionImageUrl;
    }


    public void setRuleDescriptionImageUrl(
        String ruleDescriptionImageUrl)
    {
        this.ruleDescriptionImageUrl = ruleDescriptionImageUrl;
    }


    public String getRuleDescriptionText()
    {
        return ruleDescriptionText;
    }


    public void setRuleDescriptionText(
        String ruleDescriptionText)
    {
        this.ruleDescriptionText = ruleDescriptionText;
    }


    public JSONObject getRuleJson()
    {
        return ruleJson;
    }


    public void setRuleJson(
        JSONObject ruleJson)
    {
        this.ruleJson = ruleJson;
    }


    public Date getUpdateTime()
    {
        return updateTime;
    }


    public void setUpdateTime(
        Date updateTime)
    {
        this.updateTime = updateTime;
    }


    //
    // Sortable
    //


    @Override
    public String getSortKey()
    {
        return sortKey;
    }


    public void setSortKey(
        String sk)
    {
        this.sortKey = sk;
    }



    @Override
    public String getSortVal()
    {
        return sortVal;
    }


    public void setSortVal(
        String sv)
    {
        this.sortVal = sv;
    }
}
