
package gd.impl.identity;


import gd.api.v1.user.CredentialsType;
import gd.impl.repository.Repository;
import gd.impl.repository.RepositoryAdmin;

import org.apache.commons.lang3.tuple.Pair;


/**
 * Primary ID is a composite key of type and
 * {@link Credentials#getId()}
 */
public interface CredentialsRepository
    extends
        Repository<Pair<CredentialsType,String>,Credentials>,
        RepositoryAdmin
{

}
