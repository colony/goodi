
package gd.impl.identity;


import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;


public class Rights
{
    private static final String STAR = "*";
    
    /**
     * Rights: Capable of any actions
     */
    public static final Rights ANY = new Rights(STAR);
    
    /**
     * If the last char is "*", this is prefix match. Otherwise,
     * this is exact match.
     */
    private String statement;


    public Rights()
    {
        // An empty rights means NO PRVILEDGE!
        this.statement = "";
    }


    public Rights(
        String statement)
    {
        this.statement = statement;
    }


    /**
     * Current rights satisfies (surpass or equal) the required one
     * 
     * @param required
     *          The rights to be testified
     * @return true if current rights satisfies, false otherwise
     */
    public boolean eval(
        Rights required)
    {
        // Save energy from typing ;)
        String st = this.statement;
        String rst = required.getStatement();
        
        if (Strings.isNullOrEmpty(rst))
            return true;
        
        if (Strings.isNullOrEmpty(st))
            return false;
        
        if (StringUtils.endsWith(st, STAR))
        {
            return rst.startsWith(StringUtils.chop(st));
        }
        else
        {
            return st.equals(rst);
        }
    }

    
    public String getStatement()
    {
        return statement;
    }
    
    
    public void setStatement(String statement)
    {
        this.statement = statement;
    }
    
    
    @Override
    public String toString()
    {
        return this.statement;
    }
}
