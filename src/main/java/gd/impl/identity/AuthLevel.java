
package gd.impl.identity;


/**
 * Modes describing an operation's authentication requirements
 */
public enum AuthLevel
{
    /**
     * The operation does not use an authentication token at all
     */
    UNAUTH,

    /**
     * The operation accepts an authentication token but does not
     * require it
     */
    AUTH_OPTIONAL,

    /**
     * The operation requires full authentication (i.e. not guest)
     */
    NORMAL,

    /**
     * The operation requires admin status
     */
    ADMIN,
    
    /**
     * The operation only accepts an authentication token from web client
     */
    WEB,

    ;
}
