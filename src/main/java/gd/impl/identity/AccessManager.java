
package gd.impl.identity;


import gd.impl.post.Post;
import gd.impl.post.PostStatus;
import gd.impl.relation.RL.SLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.user.UserStatus;
import gd.impl.user.Users;



public class AccessManager
{
    private SLRelation includesSpecialUserRel;


    private AccessManager()
    {
        RelationManager relMgr = RelationManager.instance();
        this.includesSpecialUserRel =
            relMgr.get(
                RelationType.INCLUDES_SPECIAL_USER,
                SLRelation.class);
    }


    public boolean isAdmin(
        long userId)
    {
        return userId == Users.ADMIN_ID
            ||
            this.includesSpecialUserRel.hasEdge(
                UserStatus.Status.ADMIN.name(),
                userId);
    }


    public boolean mayFlagPost(
        long userId,
        Post post)
    {
        if (userId == Users.ADMIN_ID)
            return true;

        // Don't allow flag on special post
        PostStatus ps = post.getStatus();
        if (ps == null || ps.isEmpty())
            return true;
        
        return false;
    }


    public boolean mayManagePost(
        long userId,
        Post post)
    {
        // Admin is okay
        if (userId == Users.ADMIN_ID)
            return true;

        // Owner is okay
        if (post.getOwnerId() == userId)
            return true;

        return false;
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final AccessManager INSTANCE =
            new AccessManager();
    }


    public static AccessManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
