
package gd.impl.identity;


import gd.api.v1.user.CredentialsType;


public class Credentials
{
    private long createTime;

    private String data;

    private String id;

    private CredentialsType type;

    private long updateTime;

    private long userId;


    public Credentials(){}
    
    /**
     * Copy constructor 
     */
    public Credentials(Credentials c)
    {
        this.createTime = c.createTime;
        this.data = c.data;
        this.id = c.id;
        this.type = c.type;
        this.updateTime = c.updateTime;
        this.userId = c.userId;
    }


    @Override
    public String toString()
    {
        return  "[data=" + data + ", id=" + id
            + ", type=" + type + ", userId=" + userId + "]";
    }

    
    //
    // GETTERS & SETTERS
    //


    public long getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        long createTime)
    {
        this.createTime = createTime;
    }


    public String getData()
    {
        return data;
    }


    public void setData(
        String data)
    {
        this.data = data;
    }


    public String getId()
    {
        return id;
    }


    public void setId(
        String id)
    {
        this.id = id;
    }


    public CredentialsType getType()
    {
        return type;
    }


    public void setType(
        CredentialsType type)
    {
        this.type = type;
    }


    public long getUpdateTime()
    {
        return updateTime;
    }


    public void setUpdateTime(
        long updateTime)
    {
        this.updateTime = updateTime;
    }


    public long getUserId()
    {
        return userId;
    }


    public void setUserId(
        long userId)
    {
        this.userId = userId;
    }
}
