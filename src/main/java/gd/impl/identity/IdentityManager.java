
package gd.impl.identity;


import gd.impl.api.ApiException;
import gd.impl.api.FacebookApi;
import gd.impl.api.FacebookApi.GetUserRequest;
import gd.impl.api.FacebookApi.GetUserResponse;
import gd.impl.config.GlobalProperties;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SSRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.util.InitializationError;
import gd.impl.util.Managed;
import gd.impl.util.Stringifiable;
import gd.impl.util.UpdateConflictException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


public class IdentityManager
    implements
        Managed
{
    private static final Logger LOG =
        LoggerFactory.getLogger(IdentityManager.class);

    // A file holding unique ID that used for screen_name generation
    // For detail, check generateScreenName method
    private static final String ID_FILE = "id";

    public static final long ADMIN_ID = 1L;

    // Default email verification code lifetime, one week in ms
    public static final long DFLT_EMAIL_VERFICATION_LIFETIME = 7
        * 24 * 60 * 60 * 1000;

    private SSRelation anythingRel;

    private CredentialsRepository credsRepo;

    private FacebookApi fbApi;

    private AtomicInteger idGen;

    //  private MailManager mailMgr;

    private Random random;


//    private SLRelation specialUserFlagRel;


    private IdentityManager()
    {
//        RepositoryManager repoMgr = RepositoryManager.instance();
//        this.credsRepo =
//            repoMgr.get(
//                RepositoryType.CREDS,
//                CredentialsRepository.class);
        this.fbApi = null;
//            ApiManager.instance().get(
//                ApiType.FACEBOOK,
//                FacebookApi.class);
        this.random = new Random();

        RelationManager relMgr = RelationManager.instance();
        this.anythingRel =
            relMgr.get(RelationType.ANYTHING, SSRelation.class);
//        this.specialUserFlagRel =
//            relMgr.get(
//                RelationType.SPECIAL_USER_STATUS,
//                SLRelation.class);

        // this.mailMgr = MailManager.instance();
        GlobalProperties gp = GlobalProperties.instance();
        Path path = gp.resourceDir().resolve(ID_FILE);
        if (Files.notExists(path))
        {
            this.idGen = new AtomicInteger(0);
            try
            {
                com.google.common.io.Files.write(
                    String.valueOf(this.idGen.get()),
                    path.toFile(),
                    StandardCharsets.UTF_8);
            }
            catch (Exception e)
            {
                throw new InitializationError(
                    "Fail to write id file " + path,
                    e);
            }
        }
        else
        {
            try
            {
                String str = com.google.common.io.Files.toString(
                    path.toFile(),
                    StandardCharsets.UTF_8);
                this.idGen =
                    new AtomicInteger(Integer.valueOf(str));
            }
            catch (Exception e)
            {
                throw new InitializationError(
                    "Fail to read token file " + path,
                    e);
            }
        }
    }


    public long authenticate(
        Credentials authCreds)
    {
        if (Strings.isNullOrEmpty(authCreds.getId()) ||
            Strings.isNullOrEmpty(authCreds.getData()))
        {
            // log
            return -1L;
        }

        switch (authCreds.getType())
        {
        case FACEBOOK:
            String accessToken = authCreds.getData();
            String fbid = authCreds.getId();
            // Validate Facebook token first
            try
            {
                GetUserResponse r =
                    (GetUserResponse) this.fbApi.execute(new GetUserRequest().accessToken(accessToken));

                if (!fbid.equals(r.getId()))
                {
                    // Weird case, the access token mismatch with FB ID
                    LOG.warn(
                        "Fail to authenticate, Facebbok token {} mismatch with ID {}",
                        accessToken,
                        fbid);
                    return -1L;
                }

                Credentials creds =
                    this.credsRepo.get(Pair.of(
                        authCreds.getType(),
                        authCreds.getId()));
                if (creds == null)
                    return -1L;

                // If the passed-in token is different than the stored one
                // we trust that (might expire) and update access token HERE
                if (!StringUtils.equals(
                    creds.getData(),
                    accessToken))
                {
                    creds.setData(accessToken);
                    this.credsRepo.update(creds);
                }

                return creds.getUserId();
            }
            catch (UpdateConflictException e)
            {
                LOG.error("", e);
                return -1L;
            }
            catch (ApiException e)
            {
                LOG.error(
                    "Fail to verify facebook data for token {} and ID {}",
                    authCreds.getData(),
                    authCreds.getId(),
                    e);
                return -1L;
            }

        case PASSWORD:
            Credentials creds =
                this.credsRepo.get(Pair.of(
                    authCreds.getType(),
                    authCreds.getId()));
            if (creds == null)
                return -1L;

            // Compare password
            String authmd5 =
                DigestUtils.md5Hex(authCreds.getData());
            String md5 = DigestUtils.md5Hex(creds.getData());
            if (StringUtils.equals(authmd5, md5))
                return creds.getUserId();
            else
            {
                // log
                return -1L;
            }
        default:
            throw new UnsupportedOperationException(
                "Unrecognized credentials type: "
                    + authCreds.getType());
        }
    }


    public boolean create(
        Credentials creds)
    {
        return this.credsRepo.add(creds) != null;
    }


    public boolean exists(
        Credentials creds)
    {
        return this.credsRepo.exists(Pair.of(
            creds.getType(),
            creds.getId()));
    }


    /**
     * Obtain the user ID based on the credentials
     * 
     * @return -1 if not found.
     */
    public long get(
        Credentials creds)
    {
        Credentials c =
            this.credsRepo.get(Pair.of(
                creds.getType(),
                creds.getId()));
        if (c == null)
            return -1L;
        return c.getUserId();
    }


    /**
     * Generate a unique screen name when user first signs up
     */
    public String generateScreenName()
    {
        return "Bee" + this.idGen.incrementAndGet();
    }


    /**
     * 
     * @param verifyCode
     * @param isExpired
     *            OUT PARAM. This mutable param will be true after
     *            return if the verification fail because of
     *            expiration, in this case, we will resend a new one
     * @return False if the verification failed, true otherwise
     */
    public boolean performEmailVerification(
        String verifyCode,
        MutableBoolean isExpired)
    {
        isExpired.setFalse();
        boolean succeed = true;
        // Find the verification data
        SEntry sval =
            this.anythingRel.getEdge(
                Relations.ANYTHING_EV,
                verifyCode);
        if (Strings.isNullOrEmpty(sval.s1))
        {
            LOG.info("Unrecognized email verification coe \""
                + verifyCode + "\"");
            succeed = false;
        }

        EmailVerificationData evd = new EmailVerificationData();
        try
        {
            evd.fromStr(sval.s1);
        }
        catch (IllegalArgumentException e)
        {
            LOG.warn(e.getMessage());
            succeed = false;
        }

        if (evd.expired < System.currentTimeMillis())
        {
            LOG.info("Email verification code \"" + verifyCode
                + "\" has expired");
            isExpired.setTrue();
            // TODO: resend one and cleanup previous data
            succeed = false;
        }

        if (succeed)
        {
            // Cleanup the data
            this.anythingRel.deleteEdge(
                Relations.ANYTHING_EV,
                verifyCode);
//            this.specialUserFlagRel.deleteEdge(
//                SpecialUserStatus.PENDING_EMAIL_VERIFICATION.abbreviation(),
//                evd.userId);
        }

        return succeed;
    }


    public void sendEmailVerification(
        String email,
        long userId)
    {
        EmailVerificationData evd =
            new EmailVerificationData(email, userId);

        // Generate a random verification code
        byte[] randBytes = new byte[64];
        this.random.nextBytes(randBytes);
        String verifyCode =
            Base64.encodeBase64URLSafeString(randBytes);

        this.anythingRel.addEdge(
            Relations.ANYTHING_EV,
            new SEntry(verifyCode).with(evd.toStr()));
//        this.specialUserFlagRel.addEdge(
//            SpecialUserStatus.PENDING_EMAIL_VERIFICATION.abbreviation(),
//            userId);

        // TODO
        // this.mailMgr.sendMail(sender, recipient, subject, textBody, htmlBody);
    }


    @Override
    public void start()
    {
    }


    @Override
    public void stop()
    {
        LOG.info("IdentityManager stopping...");
        // Flush the id to file
        GlobalProperties gp = GlobalProperties.instance();
        Path path = gp.resourceDir().resolve(ID_FILE);
        try
        {
            com.google.common.io.Files.write(
                String.valueOf(this.idGen.get()),
                path.toFile(),
                StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            LOG.warn("IdentityManager failed to stop", e);
        }
        LOG.info("IdentityManager stopped");
    }



    //
    // INTERNAL CLASSES
    //


    protected class EmailVerificationData
        implements
            Stringifiable<EmailVerificationData>
    {
        final int VERSION = 1;

        final int NUM = 4;

        // Control char, no visual display
        final String DELIM = "\u0001";

        String email;

        // Expiration time in ms
        long expired;

        long userId;


        EmailVerificationData()
        {
        }


        EmailVerificationData(
            String email,
            long userId)
        {
            this.email = email;
            this.expired =
                System.currentTimeMillis()
                    + DFLT_EMAIL_VERFICATION_LIFETIME;
            this.userId = userId;
        }


        @Override
        public EmailVerificationData fromStr(
            String s)
        {
            // Proceed according to version
            if (s.startsWith("1\u0001"))
            {
                String[] tokens = s.split(DELIM, NUM);
                try
                {
                    // first field is the version
                    this.email = tokens[1];
                    this.expired = Long.valueOf(tokens[2]);
                    this.userId = Long.valueOf(tokens[3]);
                }
                catch (Exception e)
                {
                    throw new IllegalArgumentException(
                        "Fail to deserialize the str \"" + s + "\"",
                        e);
                }
            }
            // Other versions go here

            throw new IllegalArgumentException(
                "Invalid version in serialized str \"" + s + "\"");
        }


        @Override
        public String toStr()
        {
            return StringUtils.join(new Object[] {
                    VERSION,
                    email,
                    expired,
                    userId
            }, DELIM);
        }
    }



    //
    // SINGLETON MANAGEMENT
    //


    private static class SingletonHolder
    {
        private static final IdentityManager INSTANCE =
            new IdentityManager();
    }


    public static IdentityManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
