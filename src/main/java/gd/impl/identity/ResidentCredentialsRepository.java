
package gd.impl.identity;


import gd.api.v1.user.CredentialsType;
import gd.impl.repository.RepositoryConfig;
import gd.impl.util.Visitor;

import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Maps;


public class ResidentCredentialsRepository
    implements
        CredentialsRepository
{

    /**
     * Indexed by type+id
     */
    protected Map<String,Credentials> entries;


    public ResidentCredentialsRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }



    @Override
    public synchronized Credentials add(
        Credentials c)
    {
        String k = makeKey(c.getType(), c.getId());
        if (this.entries.containsKey(c))
        {
            // Already exists
            return null;
        }

        c.setUpdateTime(System.currentTimeMillis());
        this.entries.put(k, c);

        return new Credentials(c);
    }


    @Override
    public synchronized boolean delete(
        Pair<CredentialsType,String> id)
    {
        String k = makeKey(id.getLeft(), id.getRight());
        if (this.entries.remove(k) == null)
        {
            // No entry found
            return false;
        }

        return true;
    }


    @Override
    public synchronized boolean exists(
        Pair<CredentialsType,String> id)
    {
        return this.entries.containsKey(makeKey(
            id.getLeft(),
            id.getRight()));
    }


    @Override
    public synchronized Credentials get(
        Pair<CredentialsType,String> id)
    {
        return this.entries.get(makeKey(
            id.getLeft(),
            id.getRight()));
    }


    @Override
    public synchronized Credentials update(
        Credentials newCreds)
    {
        String k = makeKey(newCreds.getType(), newCreds.getId());
        Credentials oldCreds = this.entries.get(k);
        if (oldCreds == null)
        {
            return null;
        }

        //TODO: update conflict handle??

        Credentials upCreds = new Credentials(newCreds);
        upCreds.setUpdateTime(System.currentTimeMillis());
        this.entries.put(k, upCreds);

        return new Credentials(upCreds);
    }


    @Override
    public void visitAll(
        Visitor<Credentials> visitor)
    {
        // TODO Auto-generated method stub

    }


    @Override
    public void exportData(
        String fname)
    {
        // TODO Auto-generated method stub

    }


    @Override
    public void importData(
        String fname)
    {
        // TODO Auto-generated method stub

    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        this.entries = Maps.newHashMap();
    }


    //
    // INTERNAL METHOD
    //

    protected String makeKey(
        CredentialsType type,
        String id)
    {
        return String.format(
            "%s:%s",
            type.abbreviation(),
            id);
    }



    @Override
    public Credentials put(
        Credentials t)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
