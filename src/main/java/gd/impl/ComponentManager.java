
package gd.impl;


import gd.impl.config.ConfigManager;
import gd.impl.util.InitializationError;
import gd.impl.util.Managed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ComponentManager implements Managed
{
    private HttpComponent httpComp;
    
    private ComponentManager()
    {
        LOG.info("Start initialize components");

        ComponentConfig compCfg =
            ConfigManager.instance().get(ComponentConfig.class);
        if (compCfg == null)
        {
            throw new InitializationError(
                "Cannot find component configuration");
        }
        
        LOG.info("-- Initializing HTTP component");
        this.httpComp = new HttpComponent(compCfg.getHttp());

        LOG.info("Complete initialize all components");
    }
    
    
    public HttpComponent http()
    {
        return httpComp;
    }

    
    @Override
    public void start()
    {
        
    }
    
    
    @Override
    public void stop()
    {
        LOG.info("ComponentManager stopping...");
        this.httpComp.stop();
        LOG.info("ComponentManager stopped");
    }


    //
    // SINGLETON MANAGEMENT
    //


    private static class SingletonHolder
    {
        private static final ComponentManager INSTANCE =
            new ComponentManager();
    }


    public static ComponentManager instance()
    {
        return SingletonHolder.INSTANCE;
    }


    private static final Logger LOG =
        LoggerFactory.getLogger(ComponentManager.class);
}
