
package gd.impl.queue;


import gd.impl.config.ConfigManager;
import gd.impl.util.InitializationError;
import gd.impl.util.Managed;

import java.util.EnumMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class QueueManager
    implements
        Managed
{
    private static final Logger LOG =
        LoggerFactory.getLogger(QueueManager.class);

    protected boolean active = false;

    protected EnumMap<QueueType,Queue<?>> queues;


    private QueueManager()
    {
        LOG.info("Start initialize queues");

        this.queues = new EnumMap<>(QueueType.class);
        QueuesConfig qsCfg =
            ConfigManager.instance().get(QueuesConfig.class);

        int numLoaded = 0;
        for (QueueType id : QueueType.values())
        {
            Queue<?> q = initQueue(qsCfg.get(id));
            this.queues.put(id, q);
            ++numLoaded;
        }

        LOG.info("Complete initialize {} queues", numLoaded);
    }

    
    public <T> T get(QueueType id, Class<T> clazz)
    {
        T result;
        
        Queue<?> obj = this.queues.get(id);
        if (obj == null)
        {
            throw new RuntimeException("Cannot find Queue: " + id);
        }
        else
        {
            result = clazz.cast(obj);
        }
        
        return result;
    }
    
    
    public String generateSummary()
    {
        if (!this.active)
        {
            return "Inactive: QueueManager hasn't started yet";
        }else
        {
            StringBuilder sb = new StringBuilder();
            for (Queue<?> q : this.queues.values())
            {
                sb.append(q.generateReport());
                sb.append("\n");
            }
            return sb.toString();
        }
    }
    

    @Override
    public void start()
    {
        LOG.info("QueueManager starting...");
        
        if (this.active)
            return;
        
        for (Queue<?> q : this.queues.values())
            q.start();

        this.active = true;
        LOG.info("QueueManager activated and started");
    }


    @Override
    public void stop()
    {
        if (!this.active)
            return;

        LOG.info("QueueManager stopping...");

        for (Queue<?> q : this.queues.values())
            q.stop();
        
        this.active = false;
        LOG.info("QueueManager deactivated and stopped");
    }
    
    
    //
    // INTERNAL METHODS
    //

    private Queue<?> initQueue(
        QueueConfig cfg)
    {
        Class<? extends Queue<?>> implClass;
        try
        {
            implClass = cfg.implClass();
        }
        catch (ClassNotFoundException e)
        {
            throw new InitializationError(
                "Fail to get the impl class for Queue "
                    + cfg.getId(),
                e);
        }

        Queue<?> implObj;
        try
        {
            implObj = implClass.newInstance();
        }
        catch (Exception e)
        {
            throw new InitializationError(
                "Fail to create instance for Queue " + cfg.getId(),
                e);
        }

        implObj.init(cfg);
        return implObj;
    }


    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final QueueManager INSTANCE =
            new QueueManager();
    }


    public static QueueManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
