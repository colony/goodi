
package gd.impl.queue;


/**
 * An enum groups different types of event into a class. Ideally,
 * different class of events should be fan out on different queue
 */
public enum EventClass
{
    /**
     * Event that related to activity, push notification
     */
    ACTIVITY,

    /**
     * Any other non-specific event belongs to this NORMAL class
     */
    NORMAL,

    ;
}
