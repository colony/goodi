
package gd.impl.queue;


import gd.impl.util.Handler;
import gd.impl.util.InitializationError;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.reflect.TypeToken;


/**
 * A TaskQueue has very similar concept as Guava's EventBus
 * 
 * <p>
 * During initialization, a list of consumer class names are
 * configured via QueueConfig. Each class will be investigated to
 * find methods that meet the following criteria:
 * <ol>
 * <li>A declared method (regardless of visibility, but no inherited
 * one), which accept a single argument of (sub)type {@link Task}</li>
 * <li>Marked with a {@link Consumer} annotation</li>
 * </ol>
 * The found methods are REAL task consumer logic. A class might
 * handle various tasks by declaring different consumer methods
 * 
 * <p>
 * Tasks are routed based on their type; a task will be delivered to
 * any consumer methods whose first argument (MUST implement
 * {@link Task}) is same type or supertype of the task, includes
 * interfaces, superclasses, and interfaces implemented by
 * superclasses
 */
public class ResidentTaskQueue
    extends
        AbstractResidentQueue<Task>
    implements
        TaskQueue
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ResidentTaskQueue.class);

    private SetMultimap<Class<? extends Task>,TaskConsumer> consumersByTask;


    @Override
    protected Handler<Task> initHandler()
    {
        this.consumersByTask = HashMultimap.create();

        for (String consumerClass : this.config.getConsumers())
        {
            try
            {
                Class<?> clazz = Class.forName(consumerClass);
                Object inst = clazz.newInstance();
                for (Method method : this.findAnnotatedMethods(clazz))
                {
                    Class<?>[] parameterTypes =
                        method.getParameterTypes();
                    Class<? extends Task> taskType =
                        parameterTypes[0].asSubclass(Task.class);
                    this.consumersByTask.put(
                        taskType,
                        new TaskConsumer(inst, method));
                }
            }
            catch (Exception e)
            {
                throw new InitializationError(
                    "Fail to init handler for TaskQueue for consumer class: "
                        + consumerClass,
                    e);
            }
        }


        return new InternalHandler();
    }


    //
    // INTERNAL METHOD
    //

    private List<Method> findAnnotatedMethods(
        Class<?> clazz)
    {
        List<Method> methods = new ArrayList<>();
        // For now, inherited annotated methods are not looked up
        for (Method m : clazz.getDeclaredMethods())
        {
            if (m.isAnnotationPresent(Consumer.class))
            {
                Class<?>[] parameterTypes = m.getParameterTypes();
                if (parameterTypes.length != 1)
                {
                    throw new RuntimeException(
                        "Method "
                            + m
                            + " has @Consumer annotation, but requires "
                            + parameterTypes.length
                            + " arguments. Must be a single argument.");
                }

                if (!Task.class.isAssignableFrom(parameterTypes[0]))
                {
                    throw new RuntimeException(
                        "Method "
                            + m
                            + " argument is not an instance of Task");
                }

                methods.add(m);
            }

        }
        return methods;
    }



    //
    // INTERNAL CLASS
    //

    private class InternalHandler
        implements
            Handler<Task>
    {
        /**
         * Since we only have one handler on one worker thread, it
         * is safe to have a cache without synchronization block.
         * Here, we employ a cache to remember all interfaces and
         * superclasses (assigned from {@link Task}) for all seen
         * Task type
         */
        Map<Class<? extends Task>,List<Class<? extends Task>>> taskTypeHierarchy =
            new HashMap<>();


        @SuppressWarnings({
                "unchecked", "rawtypes"
        })
        @Override
        public void handle(
            Task t)
        {
            Class<?> clazz = t.getClass();
            List<Class<? extends Task>> list =
                taskTypeHierarchy.get(clazz);
            if (list == null)
            {
                list = new ArrayList<>();
                Set<Class<?>> set =
                    (Set) TypeToken.of(clazz).getTypes().rawTypes();
                for (Class<?> c : set)
                {
                    if (Task.class.isAssignableFrom(c))
                        list.add((Class<? extends Task>)c);
                }
                taskTypeHierarchy.put((Class<? extends Task>)clazz, list);
            }

            for (Class<? extends Task> taskType : list)
            {
                Set<TaskConsumer> consumers =
                    consumersByTask.get(taskType);
                for (TaskConsumer c : consumers)
                {
                    c.consume(t);
                }
            }
        }
    }



    private class TaskConsumer
    {
        private final Method invoc;

        private final Object target;


        TaskConsumer(
            Object target,
            Method invocation)
        {
            this.target = target;
            this.invoc = invocation;
            this.invoc.setAccessible(true);
        }


        void consume(
            Object task)
        {
            try
            {
                this.invoc.invoke(this.target, new Object[] {
                    task
                });
            }
            catch (IllegalAccessException
                | IllegalArgumentException
                | InvocationTargetException e)
            {
                LOG.error(
                    "Fail to consume a task by consumer {} and method {}",
                    this.target.getClass().getName(),
                    this.invoc.getName(),
                    e);
            }
        }


        @Override
        public int hashCode()
        {
            final int PRIME = 31;
            return (PRIME + invoc.hashCode()) * PRIME
                + System.identityHashCode(target);
        }


        @Override
        public boolean equals(
            Object obj)
        {
            if (obj instanceof TaskConsumer)
            {
                TaskConsumer that = (TaskConsumer) obj;
                // Use == so that different equal instances will still receive events.
                // We only guard against the case that the same object is registered
                // multiple times
                return target == that.target
                    && invoc.equals(that.invoc);
            }
            return false;
        }
    }
}
