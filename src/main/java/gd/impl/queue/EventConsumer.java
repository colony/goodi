
package gd.impl.queue;


public interface EventConsumer
{
    public boolean accept(
        EventType type);

    public void consume(
        Event event);
}
