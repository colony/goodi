
package gd.impl.queue;




public enum EventType
{
    /**
     * EventClass.ACTIVITY (prefixed by 'ACT_')
     * <p>
     * These types of events are designated to transfer data for
     * activity related logic (push notification, badge count
     * update, activity creation, etc)
     */

    
    /**
     * <pre>
         lng1 - actorId
         lng2 - commentId
         lng3 - postId
     * </pre>
     */
    ACT_COMMENT_POST(EventClass.ACTIVITY),
    
    /**
     * <pre>
     *   lng1 - win post Id
     *   lng2 - threadId
     * </pre>
     */
    ACT_DECLARE_WINNER(EventClass.ACTIVITY),
    
    /**
     * <pre>
     *   lng1 - actorId
     *   lng2 - postId
     *   lng3 - threadId
     * </pre>
     */
    ACT_PARTICIPATE_THREAD(EventClass.ACTIVITY),

    /**
     * <pre>
     *  lng1 - actorId
     *  lng2 - postId
     * </pre>
     */
    ACT_VOTE_POST(EventClass.ACTIVITY),


    //
    // EventClass.NORMAL
    // (no prefix)
    //

    /**
     * A post has been updated by administrative operation
     * 
     * <pre>
     * lng1  - post ID
     * bool1 - count has been updated
     * </pre>
     */
    ADMIN_UPDATE_POST(EventClass.NORMAL),

    /**
     * A user comments a post
     * 
     * <pre>
       lng1 - ID of the commenter
       lng2 - ID of the created comment
       lng3 - ID of the post
       lng4 - ID of the post owner
     </pre>
     */
    CREATE_COMMENT(EventClass.NORMAL),

    /**
     * A user creates a post
     * 
     * <pre>
       lng1 - user ID
       lng2 - post ID
       lng3 - thread ID if any
       str1 - PostType name
       bool1 - true if created from admin console
     </pre>
     */
    CREATE_POST(EventClass.NORMAL),

    /**
     * A user flags a post
     * 
     * <pre>
       lng1 - ID of the flagger
       lng2 - ID of the post
       lng3 - ID of the post owner
     </pre>
     */
    FLAG_POST(EventClass.NORMAL),

    /**
     * A user shares a post
     * 
     * <pre>
         lng1 - user ID (sharer)
         lng2 - post ID
         str1 - type (share target, i.e. email, facebook, etc)
     </pre>
     */
    SHARE_POST(EventClass.NORMAL),

    /**
     * A user views a post
     * 
     * <pre>
       lng1 - user ID (viewer)
       lng2 - post ID
     </pre>
     */
//    VIEW_POST(EventClass.NORMAL),
    
    /**
     * A user upvotes or downvotes on a comment
     * 
     * <pre>
     * lng1 - ID of the voter
     * lng2 - ID of the comment
     * lng3 - ID of comment owner
     * int1 - -2/-1/1/2 delta vote, -2 means switch from upvote to downvote
     * bool1 - true if unvote
     * </pre>
     */
    VOTE_COMMENT(EventClass.NORMAL),
    
    
    /**
     * A user upvotes or downvotes on a post
     * 
     * <pre>
     * lng1 - ID of the voter
     * lng2 - ID of the post
     * lng3 - ID of post owner
     * int1 - -2/-1/1/2 delta vote, -2 means switch from upvote to downvote
     * bool1 - true if unvote
     * </pre>
     */
    VOTE_POST(EventClass.NORMAL),

    ;


    private final EventClass clazz;


    EventType(
        EventClass clazz)
    {
        this.clazz = clazz;
    }


    public EventClass getEventClass()
    {
        return clazz;
    }
}
