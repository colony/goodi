package gd.impl.queue;

import gd.impl.config.Configured;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;

// Not reloadable
@Configured(fname = "queue.yml")
public class QueuesConfig
{
    /**
     * N.B. The setters are primarily used by Jackson
     * deserialization. Make them private so that the configuration
     * must be immutable.
     */
    
    private EnumMap<QueueType, QueueConfig> queues;
    
    
    /**
     * Default constructor
     */
    public QueuesConfig()
    {
    }
    
    
    public QueueConfig get(QueueType id)
    {
        if (this.queues == null || !this.queues.containsKey(id))
        {
            return new QueueConfig(id);
        }
        return this.queues.get(id);
    }
    
    
    public List<QueueConfig> getQueues()
    {
        if (this.queues == null)
        {
            return Collections.emptyList();
        }
        
        return new ArrayList<>(this.queues.values());
    }
    
    
    @SuppressWarnings("unused")
    private void setQueues(List<QueueConfig> queues)
    {
        this.queues = new EnumMap<>(QueueType.class);
        for (QueueConfig cfg : queues)
        {
            this.queues.put(cfg.getId(), cfg);
        }
        
        // Create default configuration for missing one
        for (QueueType id : QueueType.values())
        {
            if (!this.queues.containsKey(id))
            {
                this.queues.put(id, new QueueConfig(id));
            }
        }
    }
}
