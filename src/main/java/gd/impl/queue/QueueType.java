
package gd.impl.queue;


public enum QueueType
{
    ACTIVITY(
        "a",
        ResidentEventQueue.class, 
        EventQueue.class),

    EVENT(
        "e",
        ResidentEventQueue.class,
        EventQueue.class),

    TASK(
        "t",
        ResidentTaskQueue.class,
        TaskQueue.class),

    ;

    private final String abbrev;

    private final Class<? extends Queue<?>> dfltImplClass;

    private final Class<? extends Queue<?>> specializedInterface;


    private QueueType(
        String abbrev,
        Class<? extends Queue<?>> dfltImplClass,
        Class<? extends Queue<?>> spInterface)
    {
        this.abbrev = abbrev;
        this.dfltImplClass = dfltImplClass;
        this.specializedInterface = spInterface;
    }


    public String abbreviation()
    {
        return abbrev;
    }


    public Class<? extends Queue<?>> defaultImplClass()
    {
        return dfltImplClass;
    }


    public Class<? extends Queue<?>> specializedInterface()
    {
        return specializedInterface;
    }


    public static QueueType fromAbbreviation(
        String abbrev)
    {
        for (QueueType q : QueueType.values())
        {
            if (q.abbrev.equals(abbrev))
            {
                return q;
            }
        }

        throw new IllegalArgumentException(
            "No queue found for abbrev \"" + abbrev + "\"");
    }
}
