
package gd.impl.queue;


import gd.impl.util.GDUtils;
import gd.impl.util.Handler;
import gd.impl.util.Managed;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractResidentQueue<T>
    implements
        Queue<T>
{
    public static final int DFLT_CAPACITY = 1000;

    private static final Logger LOG =
        LoggerFactory.getLogger(AbstractResidentQueue.class);

    protected volatile boolean active = false;

    protected QueueConfig config;

    protected Handler<T> handler;

    protected QueueInfo info;

    protected BlockingQueue<T> queue;

    protected Worker worker;


    public AbstractResidentQueue()
    {
        // Real initialization happens at init(cfg)
    }


    @Override
    public void flush()
    {
        if (this.active)
        {
            LOG.warn("Ignore flush, Queue {} is not active",
                this.config.getName());
            return;
        }
        this.queue.clear();

        ++this.info.numFlush;
        this.info.size = 0;
    }


    @Override
    public String generateReport()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(
            "Current: %s%n",
            this.active ? "active" : "inactive"));
        sb.append(String.format(
            "Active: %s%n",
            GDUtils.printDuration(
                info.lastStartTime,
                System.currentTimeMillis())));
        sb.append(String.format("Name: %s%n", this.config.getName()));
        sb.append(String.format("Capacity: %s%n", info.capacity));
        sb.append(String.format(
            "LastStartTime: %d%n",
            info.lastStartTime));
        sb.append(String.format(
            "LastStopTime: %d%n",
            info.lastStopTime));
        sb.append(String.format("NumDeq: %d%n", info.numDeq));
        sb.append(String.format("NumEnq: %d%n", info.numEnq));
        sb.append(String.format("NumFlush: %d%n", info.numFlush));
        sb.append(String.format("NumSpill: %d%n", info.numSpill));
        sb.append(String.format("Size: %d%n", info.size));
        return sb.toString();
    }


    @Override
    public synchronized void init(
        QueueConfig config)
    {
        if (this.active)
        {
            LOG.error("Ignore init, Queue {} is already active",
                this.config.getName());
            return;
        }

        this.config = config;
        this.queue =
            new ArrayBlockingQueue<>(config.getCapacity(), true);
        this.info = new QueueInfo();
        this.info.capacity = config.getCapacity();

        // Initialize the handler
        this.handler = this.initHandler();

        LOG.info(
            "Complete initialize message queue \"{}\"",
            this.config.getName());
    }


    @Override
    public boolean enq(
        T item)
    {
        if (!this.active)
        {
            LOG.warn("Ignore enq, Queue {} is not active",
                this.config.getName());
            return false;
        }

        if (!this.queue.offer(item))
        {
            // Seems like, our queue is full
            ++this.info.numSpill;
            return false;
        }

        ++this.info.numEnq;
        ++this.info.size;
        return true;
    }


    @Override
    public T deq()
        throws InterruptedException
    {
        if (!this.active)
        {
            LOG.warn("Ignore deq, Queue {} is not active",
                this.config.getName());
            return null;
        }

        // Caller thread is blocked on waiting
        T item = this.queue.take();

        ++this.info.numDeq;
        --this.info.size;
        return item;
    }


    @Override
    public void register(
        Handler<T> handler)
    {
        // The impl doesn't allow register external handler, instead
        // the subclass should override initHandler method
        throw new UnsupportedOperationException();
    }


    @Override
    public synchronized void start()
    {
        if (this.active)
        {
            LOG.warn("Ignore start, Queue {} is already active",
                this.config.getName());
            return;
        }

        if (this.handler == null)
        {
            LOG.warn(
                "There is no handler registered with queue {}",
                this.config.getName());
        }

        this.info.lastStartTime = System.currentTimeMillis();
        this.info.numDeq = 0L;
        this.info.numEnq = 0L;
        this.info.numFlush = 0L;
        this.info.numSpill = 0L;

        this.active = true;
        this.worker = new Worker();
        this.worker.setName("MessageQueue-" + config.getName());
        this.worker.start();
        
        LOG.info("Queue {} is now active", this.config.getName());
    }


    @Override
    public synchronized void stop()
    {
        if (!this.active)
        {
            LOG.warn("Ignore stop, Queue {} is already stop");
            return;
        }
        
        // Prevent accepting any new events
        this.active = false;

        // Tell the worker thread to shutdown
        this.worker.shutdown();

        // Wait for the worker thread to terminate
        while (this.worker.isAlive())
        {
            LOG.debug(
                "Waiting for worker thread of {} queue to terminate...",
                this.config.getName());

            try
            {
                this.worker.join(3000);
            }
            catch (InterruptedException e)
            {
                LOG.debug(
                    "Interrupted while waitting for worker thread of queue {} to terminate",
                    this.config.getName());
            }
        }

        this.info.lastStopTime = System.currentTimeMillis();
        LOG.info("Queue {} is now stopped", this.config.getName());
    }


    //
    // ABSTRACT INTERFACE
    //


    protected abstract Handler<T> initHandler();



    //
    // INTERNAL CLASSES
    //

    private class QueueInfo
    {
        int capacity = 0;

        long lastStartTime;

        /**
         * Last stop time if the queue has ever been stopped when
         * server is up
         */
        long lastStopTime;

        long numDeq = 0L;

        long numEnq = 0L;

        long numFlush = 0L;

        /**
         * Number of time enq() rejected because the capacity is
         * full
         */
        long numSpill = 0L;

        /**
         * Current number of items in the queue
         */
        int size = 0;
    }



    private class Worker
        extends
            Thread
    {
        volatile boolean stopped;


        Worker()
        {
            this.stopped = false;
        }


        @Override
        public void run()
        {
            while (!this.stopped)
            {
                // Wait for dequeue
                T item = null;
                try
                {
                    item = deq();
                }
                catch (InterruptedException e)
                {
                    // We were interrupted. Possibly by shutdown.
                    // Return to top of loop see if ask for stopped
                    continue;
                }

                if (handler != null)
                {
                    try
                    {
                        if (item != null)
                            handler.handle(item);
                    }
                    catch (Throwable t)
                    {
                        LOG.error(
                            "Error occurs when handling queue item {}",
                            item.getClass().getName(),
                            t);
                    }
                }
            }
        }


        public void shutdown()
        {
            this.stopped = true;
            if (handler != null)
            {
                if (handler instanceof Managed)
                {
                    ((Managed) handler).stop();
                }
            }

            // Interrupt the worker thread. If the worker is blocked,
            // it will be waked up by InterruptedException. If the worker
            // is interrupted before it blocks ), this is equivalent to 
            // it being interrupted immediately upon blocking on that method
            this.interrupt();
        }
    }
}
