
package gd.impl.queue;


import gd.api.v1.admin.ManageQueueResource;
import gd.impl.Rests;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;


@Path("/adm/queue")
@Singleton
public class ManageQueueResourceImpl
    implements
        ManageQueueResource
{
    protected QueueManager qMgr;


    public ManageQueueResourceImpl()
    {
        this.qMgr = QueueManager.instance();
    }


    @Override
    @GET
    @Produces("text/plain")
    public Response getQueueSummary(
        @Context HttpHeaders headers)
    {
        Rests.beginAdminOperation(headers, "adm:queue:summary");
        return Response.ok(this.qMgr.generateSummary())
            .build();
    }


    @Override
    @Path("/{id}/start")
    @POST
    @Produces("text/plain")
    public Response startQueue(
        @Context HttpHeaders headers,
        @PathParam("id") QueueType id)
    {
        Rests.beginAdminOperation(headers, "adm:queue:start:" + id.name());
        Queue<?> q = this.qMgr.get(id, Queue.class);
        q.start();
        return Response.ok().build();
    }


    @Override
    @Path("/{id}/stop")
    @POST
    @Produces("text/plain")
    public Response stopQueue(
        @Context HttpHeaders headers,
        @PathParam("id") QueueType id)
    {
        Rests.beginAdminOperation(headers, "adm:queue:stop:" + id.name());
        Queue<?> q = this.qMgr.get(id, Queue.class);
        q.stop();
        return Response.ok().build();
    }
}
