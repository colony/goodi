
package gd.impl.queue;


import gd.impl.util.GDUtils;
import gd.impl.util.Handler;
import gd.impl.util.InitializationError;

import java.util.ArrayList;
import java.util.List;



public class ResidentEventQueue
    extends
        AbstractResidentQueue<Event>
    implements
        EventQueue
{
    private List<EventConsumer> consumers;


    @Override
    protected Handler<Event> initHandler()
    {
        this.consumers = new ArrayList<>();
        for (String consumerClass : this.config.getConsumers())
        {
            try
            {
                this.consumers.add(GDUtils.getInstance(
                    consumerClass,
                    EventConsumer.class));
            }
            catch (Exception e)
            {
                throw new InitializationError(
                    "Fail to init handler for EventQueue for consumer class: "
                        + consumerClass,
                    e);
            }
        }

        return new InternalHandler();
    }


    private class InternalHandler
        implements
            Handler<Event>
    {
        @Override
        public void handle(
            Event e)
        {
            if (consumers.isEmpty())
                return;

            // Pick consumer based on the list ordering
            for (EventConsumer ec : consumers)
            {
                if (ec.accept(e.type))
                {
                    ec.consume(e);
                }
            }
        }
    }
}
