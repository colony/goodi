package gd.impl.queue;

public class ATasker
{
    @Consumer
    public void doTask(ATask t)
    {
        System.out.println("A");
    }
    
    @Consumer
    public void doTask(BTask t)
    {
        System.out.println("B");
    }
    
    @Consumer
    public void doTask(CTask t)
    {
        System.out.println("C");
    }
    
    @Consumer
    public void doTask(DTask t)
    {
        System.out.println("D");
    }
    
    
    public static class ATask implements Task
    {
    }
    
    
    public static class BTask extends ATask
    {
        
    }
    
    
    public static class CTask extends DTask implements Task
    {
        
    }
    
    public static interface TaskEnhance extends Task{}
    
    public static class DTask implements TaskEnhance
    {
        
    }
}
