
package gd.impl.queue;


public class Event
{
    /**
     * Unique ID of this event or 0 if this event did not come from
     * a persistent store.
     */
    public long id = 0L;

    /**
     * The specific kind
     */
    public final EventType type;

    /**
     * Time the event was created, in milliseconds since the epoch
     */
    public long time;


    //
    // SCALA FIELDS
    //
    
    public boolean bool1 = false;
    
    public int int1;
    
    public int int2;

    public long lng1;

    public long lng2;
    
    public long lng3;
    
    public long lng4;

    public String str1;

    public String str2;
    
    public String str3;


    public Event(
        EventType type)
    {
        this.type = type;
        this.time = System.currentTimeMillis();
    }
}
