
package gd.impl.queue;


import gd.impl.util.GDUtils;

import java.util.List;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;


public class QueueConfig
{
    public static final int DFLT_CAPACITY = 100000;
    
    /**
     * A list of consumer class names
     */
    private List<String> consumers = ImmutableList.of();

    private QueueType id;

    private String implClassName;

    private String name;


    //
    // Config applicable on ResidentQueue
    //

    private int capacity = DFLT_CAPACITY;


    /**
     * Default constructor
     */
    public QueueConfig()
    {
    }
    
    
    public QueueConfig(QueueType id)
    {
        this.id = id;
    }


    public int getCapacity()
    {
        return capacity;
    }
    
    
    public List<String> getConsumers()
    {
        return consumers;
    }


    public QueueType getId()
    {
        return id;
    }


    public String getImplClassName()
    {
        if (Strings.isNullOrEmpty(this.implClassName))
        {
            return id.defaultImplClass().getName();
        }

        return this.implClassName;
    }


    public String getName()
    {
        if (Strings.isNullOrEmpty(name))
            return id.name();
        return name;
    }


    public Class<? extends Queue<?>> implClass()
        
        throws ClassNotFoundException
    {
        Class<?> rawClass = Class.forName(this.getImplClassName());
        Class<? extends Queue<?>> implClass =
            GDUtils.cast(rawClass.asSubclass(Queue.class));
        return implClass;
    }
}
