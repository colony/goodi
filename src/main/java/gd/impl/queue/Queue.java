
package gd.impl.queue;


import gd.impl.util.Handler;


public interface Queue<T>
{
    /**
     * Pop a work item from top of queue
     * 
     * @return
     * @throws InterruptedException
     */
    public T deq()
        throws InterruptedException;


    /**
     * Push a work item to tail of queue
     * 
     * @param item
     *            Work item
     * @return true if success, false otherwise (possibly
     *         over-capacity)
     */
    public boolean enq(
        T item);


    /**
     * Delete and clear out entire queue
     */
    public void flush();


    /**
     * Generate a live summary about the queue
     * 
     * @return A queue summary and statistics
     */
    public String generateReport();


    /**
     * Initialize by configuration
     * 
     * @param cfg
     *            The configuration
     */
    public void init(
        QueueConfig cfg);


    /**
     * Register a handler to handle the item pop off the queue.
     * Note1: This method should be called before {{@link #start()}
     * Note2: It leaves the underlying implementation to allow
     * single or multiple handler
     */
    public void register(
        Handler<T> handler);


    /**
     * Start the queue
     */
    public void start();


    /**
     * Stop the queue
     */
    public void stop();
}
