package gd.impl.session;

import gd.impl.notification.NotificationToken;
import gd.impl.repository.Repository;
import gd.impl.repository.RepositoryAdmin;

import java.util.List;

public interface SessionRepository
    extends
        Repository<String,Session>,
        RepositoryAdmin
{
    public boolean deleteByNotification(NotificationToken notiToken);
    
    
    public List<Session> findByNotification(NotificationToken notiToken);
    
    
    public Session findByOwner(long ownerId);
}
