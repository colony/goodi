
package gd.impl.session;


import gd.impl.config.GlobalProperties;
import gd.impl.identity.Rights;
import gd.impl.notification.NotificationToken;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.InitializationError;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.base.Strings;


/**
 * A manager that is responsible for managing the session tokens and
 * device tokens for authenticated users.
 */
public class SessionManager
{
    private static final String TOKEN_FILE = "token";

    private String federationToken;

    private SessionRepository sessRepo;

    private String webToken;


    private SessionManager()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.sessRepo =
            repoMgr.get(
                RepositoryType.SESSION,
                SessionRepository.class);

        // Load up the federation token from file
        // If not exist, create/save one on-the-fly
        GlobalProperties gp = GlobalProperties.instance();
        Path path = gp.resourceDir().resolve(TOKEN_FILE);
        if (Files.notExists(path))
        {
            this.federationToken =
                DigestUtils.md5Hex(String.format("GDF:%d", System.currentTimeMillis()));
            this.webToken =
                DigestUtils.md5Hex(String.format("GDW:%d", System.currentTimeMillis()));
            try
            {
                String str = this.federationToken + "\n" + this.webToken;
                Files.write(
                    path,
                    str.getBytes(StandardCharsets.UTF_8));
            }
            catch (IOException e)
            {
                throw new InitializationError(
                    "Fail to write token file " + path,
                    e);
            }
        }
        else
        {
            try
            {
                List<String> tokens =
                    Files.readAllLines(path, StandardCharsets.UTF_8);
                this.federationToken = tokens.get(0);
                this.webToken = tokens.get(1);
            }
            catch (IOException e)
            {
                throw new InitializationError(
                    "Fail to read token file " + path,
                    e);
            }
        }
    }


    public boolean closeSession(
        String token)
    {
        Session sess = this.sessRepo.get(token);
        if (sess == null)
        {
            // log
            return false;
        }

//        sess.setEndTime(System.currentTimeMillis());
//        this.sessRepo.update(sess);
        return true;
    }


    public String openSession(
        long userId)
    {
        Date now = new Date();
        String token =
            DigestUtils.md5Hex(String.format(
                "%s%d:%d",
                RandomStringUtils.randomAlphanumeric(5),
                userId,
                now.getTime()));

        Session sess = new Session();
        sess.setCreateTime(now);
        sess.setOwnerId(userId);
        sess.setToken(token);
        sess.setUpdateTime(now);

        this.sessRepo.add(sess);
        return token;
    }


    public String openSessionWithNotificationToken(
        long userId,
        NotificationToken notiToken)
    {
        Date now = new Date();
        String token =
            DigestUtils.md5Hex(String.format(
                "%s%d:%d",
                RandomStringUtils.randomAlphanumeric(5),
                userId, 
                now.getTime()));

        Session sess = new Session();
        sess.setCreateTime(now);
        sess.setOwnerId(userId);
        sess.setToken(token);
        sess.setUpdateTime(now);

        this.sessRepo.add(sess);
        
        // Update the device token and remove any duplicated one
        Sessions.setNotificationToken(userId, notiToken);
        return token;
    }


    public long validateSession(
        String token)

        throws InvalidSessionException
    {
        return this.validateSession(token, Rights.ANY);
    }


    /**
     * Validate a session against certain rights. If a session isn't
     * associated with any restriction, that means it is valid to be
     * served by any resource (note, another layer of access control
     * is forced on user level). Otherwise, we will evaluate the
     * restriction against the required rights
     * 
     * @param token
     * @param rights
     *            The required rights to be evaluated
     * @return The session owner ID
     * @throws InvalidSessionException
     */
    public long validateSession(
        String token,
        Rights rights)

        throws InvalidSessionException
    {
        if (Strings.isNullOrEmpty(token))
        {
            throw new InvalidSessionException("Empty session token");
        }

        if (token.startsWith(this.federationToken))
        {
            // Special token, the federated user ID is appended
            String federatedId =
                token.substring(this.federationToken.length());
            if (Strings.isNullOrEmpty(federatedId))
            {
                throw new InvalidSessionException(
                    "Federation token \"" + token + "\" invalid");
            }
            return Long.valueOf(federatedId);
        }
        
        if (token.equals(this.webToken)) {
            // Special token, the web token is ONLY used by web client
            // For now, 08/07/15, we don't specify a user ID within the web request
            return 0L;
        }

        Session sess = this.sessRepo.get(token);
        if (sess == null)
        {
            throw new InvalidSessionException("Session token \""
                + token + "\" not found");
        }

//        if (sess.getEndTime() != 0L ||
//            System.currentTimeMillis() > sess.getExpireTime())
//        {
//            throw new InvalidSessionException("Session token \""
//                + token + "\" either expires or closes");
//        }

        long userId = sess.getOwnerId();

        // If there are no concerns about rights, then done
//        if (rights == null)
//        {
        return userId;
//        }

        // If there are no restrictions on the session, then done
//        if (sess.getRestrictions() == null)
//        {
//            return userId;
//        }
//
//        if (!sess.getRestrictions().eval(rights))
//        {
//            throw new InvalidSessionException(
//                "Session token \""
//                    + token
//                    + "\" don't have the rights \""
//                    + rights
//                    + "\"");
//        }
//
//        return sess.getUserId();
    }



    public static class InvalidSessionException
        extends
            Exception
    {
        private static final long serialVersionUID =
            8988070372815404021L;


        public InvalidSessionException(
            String msg)
        {
            super(msg);
        }
    }



    //
    // SINGLETON MANAGEMENT
    //


    private static class SingletonHolder
    {
        private static final SessionManager INSTANCE =
            new SessionManager();
    }


    public static SessionManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
