
package gd.impl.session;


import gd.impl.notification.NotificationToken;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.UpdateConflictException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class Sessions
{
    private static final Logger LOG =
        LoggerFactory.getLogger(Sessions.class);

    private static SessionRepository _sessRepo;


    public static void init()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        _sessRepo = repoMgr.get(RepositoryType.SESSION, SessionRepository.class);
    }


    /**
     * Set the notification token on caller's session. If there's
     * any other session containing same notification token (because
     * of re-installation issue), they will be nullified.
     * 
     * @param callerId
     * @param newToken
     * @return True if notification token is updated, false if fail
     *         to update or the session already have the same token
     */
    public static boolean setNotificationToken(
        long callerId,
        NotificationToken newToken)
    {
        // If a user has deleted and re-install the app, the same and unique deviceToken
        // will exist in other sessions, we need clean them up
        StringBuilder otherIds = new StringBuilder();
        List<Session> otherSesses = _sessRepo.findByNotification(newToken);
        for (Session otherSess : otherSesses)
        {
            if (otherSess.getOwnerId() != callerId)
            {
                otherIds.append(otherSess.getOwnerId());
                otherIds.append(",");

                otherSess.setNotificationToken(null);
                try
                {
                    _sessRepo.update(otherSess);
                }
                catch (UpdateConflictException e)
                {
                    LOG.error(
                        "Fail to clear notification token on session token={}",
                        otherSess.getToken(),
                        e);
                }
            }
        }

        if (otherIds.length() > 0) {
            otherIds.append(callerId);
            LOG.warn("User IDs with same device token: " + otherIds.toString());
        }

        Session sess = _sessRepo.findByOwner(callerId);
        NotificationToken nt = sess.getNotificationToken();
        if (nt == null || !nt.equals(newToken))
        {
            // Update the token now
            sess.setNotificationToken(newToken);
            try
            {
                _sessRepo.update(sess);
                return true;
            }
            catch (UpdateConflictException e)
            {
                LOG.error(
                    "Fail to update session notification (provider={}, token={}) for user ID={}",
                    newToken.getProvider(),
                    newToken.getId(),
                    callerId,
                    e);
            }
        }
        return false;
    }
}
