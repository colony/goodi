
package gd.impl.session;


import gd.impl.SQLiteManager;
import gd.impl.notification.NotificationProvider;
import gd.impl.notification.NotificationToken;
import gd.impl.repository.RepositoryConfig;
import gd.impl.repository.SqliteRepository;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Schema:
 * <ul>
 * <li>create_time
 * <li>notification_provider
 * <li>notification_token
 * <li>owner_id
 * <li>token
 * <li>update_time
 * </ul>
 */
public class SqliteSessionRepository
    implements
        SessionRepository,
        SqliteRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SqliteSessionRepository.class);

    private int batchSize = 100;


    public SqliteSessionRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public Session add(
        Session s)
    {
        String sql =
            "insert into session (create_time, notification_provider, notification_token, owner_id, token, update_time) values "
                + "(?,?,?,?,?,?)";

        Session newSess = new Session(s);
        newSess.setCreateTime(new Date());
        newSess.setUpdateTime(new Date());

        // SQL statement parameters construction
        Object param[] = new Object[6];
        param[0] = newSess.getCreateTime().getTime();
        if (newSess.getNotificationToken() != null)
        {
            param[1] =
                newSess.getNotificationToken().getProvider().name();
            param[2] = newSess.getNotificationToken().getId();
        }
        param[3] = newSess.getOwnerId();
        param[4] = newSess.getToken();
        param[5] = newSess.getUpdateTime().getTime();

        // Execute SQL
        SqliteUtils.executeInsertQuery(
            sql,
            param,
            "addSession",
            false);
        return newSess;
    }


    @Override
    public boolean delete(
        String token)
    {
        String sql = "delete from session where token=?";
        Object[] param = {
                token
        };
        return SqliteUtils.executeQuery(sql, param, "deleteSession");
    }


    @Override
    public boolean deleteByNotification(
        NotificationToken notiToken)
    {
        String sql =
            "delete from session where notification_provider=? and notification_token=?";
        Object[] param = {
                notiToken.getProvider().name(), notiToken.getId()
        };
        return SqliteUtils.executeQuery(
            sql,
            param,
            "deleteSessionByNoti");
    }


    @Override
    public boolean exists(
        String token)
    {
        String sql = "select 1 from session where token=?";
        Object[] param = {
                token
        };
        final MutableBoolean mb = new MutableBoolean(false);

        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "checkSessionExist", new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }

            });
        return mb.booleanValue();
    }


    @Override
    public List<Session> findByNotification(
        NotificationToken notiToken)
    {
        String sql = "SELECT * FROM session WHERE notification_provider=? AND notification_token=?";
        final List<Session> sessions = new ArrayList<>();
        SqliteUtils.executeSelectQuery(
            sql,
            new Object[] {
                    notiToken.getProvider().name(), notiToken.getId()
            },
            "findSessByNoti",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next()) {
                        Session sess = new Session();
                        parseResultSet(sess, rs);
                        sessions.add(sess);
                    }
                }
            });
        return sessions;
    }


    @Override
    public Session findByOwner(
        long ownerId)
    {
        String sql = "select * from session where owner_id=?";
        Object[] param = { ownerId };

        final MutableObject<Session> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "findSessionByOwner",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        Session sess = new Session();
                        parseResultSet(sess, rs);
                        mo.setValue(sess);
                    }
                }
            });
        return mo.getValue();
    }


    @Override
    public Session get(
        String token)
    {
        String sql = "select * from session where token=?";
        Object[] param = {
                token
        };

        final MutableObject<Session> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "getSession",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        Session sess = new Session();
                        parseResultSet(sess, rs);
                        mo.setValue(sess);
                    }
                }
            });
        return mo.getValue();
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        createSessionTable(config);
        SqliteUtils.createSingleIndex(
            config.getTableName(),
            "session_ownerId",
            "owner_id",
            false);
        SqliteUtils.createCompositeIndex(
            config.getTableName(),
            "notificationProvider_token",
            "notification_provider",
            "notification_token",
            false);
        this.batchSize = config.getBatchSize();
    }


    @Override
    public Session put(
        Session s)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public Session update(
        Session s)
        throws UpdateConflictException
    {
        // MODIFIABLE fields:
        // notification_provider, notification_token, owner_id

        // NON-MODIFIABLE fields:
        // create_time, token

        // update_time is modifiable, but we also use the old data as optimistic version lock

        Session upSess = new Session(s);

        Date oldUpdateTime = s.getUpdateTime();
        Date newUpdateTime = new Date();
        upSess.setUpdateTime(newUpdateTime);
        Object param[] = new Object[7];

        // Prepare a query that updates mutable fields
        String updateSql =
            "UPDATE session SET notification_provider=?, notification_token=?, owner_id=?, update_time=? ";
        if (null != upSess.getNotificationToken())
        {
            param[0] =
                upSess.getNotificationToken().getProvider().name();
            param[1] = upSess.getNotificationToken().getId();
        }
        param[2] = upSess.getOwnerId();
        param[3] = newUpdateTime.getTime();

        // Prepare a query that matches on immutable fields
        String querySql =
            "WHERE token=? AND create_time=? AND update_time=?";
        param[4] = upSess.getToken();
        param[5] = upSess.getCreateTime().getTime();
        param[6] = oldUpdateTime.getTime();

        String sql = updateSql + querySql;
        boolean success =
            SqliteUtils.executeQuery(sql, param, "updateSession");
        if (!success)
        {
            // Find out if the entity exists
            Session sess = this.get(upSess.getToken());
            if (sess == null)
            {
                return null;
            }

            if (sess.getUpdateTime().compareTo(oldUpdateTime) != 0)
                throw new UpdateConflictException(
                    "Update conflict - specified update time: "
                        + oldUpdateTime
                        + "  current update time: "
                        + sess.getUpdateTime());

            // If we make it here, it must be the case that immutable fields were changed
            throw new IllegalArgumentException(
                "Attempt to modify an immutable field");
        }

        return upSess;
    }


    //TODO: junit test
    @Override
    public void visitAll(
        Visitor<Session> visitor)
    {
        if (!visitor.before())
            return;

        SQLiteManager sqliteManager = SQLiteManager.instance();
        Connection connection = null;
        PreparedStatement statement = null;

        boolean hasMore = true;
        boolean aborted = false;
        String token = "";

        try
        {
            connection = sqliteManager.getConnection();
            final String sql =
                "SELECT * FROM session WHERE token>? ORDER BY token ASC LIMIT "
                    + this.batchSize;

            statement =
                connection.prepareStatement(sql);
            while (hasMore && !aborted)
            {
                statement.setString(1, token);
                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next())
                {
                    count++;
                    token = rs.getString("token");

                    Session sess = new Session();
                    parseResultSet(sess, rs);
                    if (!visitor.visit(sess))
                    {
                        aborted = true;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Fail to execute query \"visitAll\"",
                e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for visitAll",
                    e);
            }
        }

        visitor.after();
    }


    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }


    //
    // INTERNAL METHODS
    //

    private void createSessionTable(
        RepositoryConfig config)
    {
        final String createSessionTableSql =
            "create table if not exists "
                + config.getTableName()
                + "(" +
                "create_time integer," +
                "notification_provider text," +
                "notification_token text," +
                "owner_id integer not null," +
                "token text primary key not null," +
                "update_time integer" +
                ")";
        SqliteUtils.executeQuery(
            createSessionTableSql,
            null,
            "CreateSessionTable");
    }


    private void parseResultSet(
        Session sess,
        ResultSet rs)
        throws SQLException
    {
        sess.setCreateTime(new Date(rs.getLong("create_time")));

        String notiId = rs.getString("notification_token");
        if (StringUtils.isNotEmpty(notiId))
        {
            NotificationProvider notiProvider =
                NotificationProvider.valueOf(rs.getString("notification_provider"));
            sess.setNotificationToken(new NotificationToken(
                notiProvider,
                notiId));
        }

        sess.setOwnerId(rs.getLong("owner_id"));
        sess.setToken(rs.getString("token"));
        sess.setUpdateTime(new Date(rs.getLong("update_time")));
    }
}
