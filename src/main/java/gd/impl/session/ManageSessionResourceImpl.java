
package gd.impl.session;


import gd.api.v1.admin.ManageSessionResource;
import gd.api.v1.admin.SessionRep;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.repository.RepositoryType;
import gd.impl.repository.RepositoryManager;
import gd.impl.util.GDUtils;
import gd.impl.util.Visitor;
import gd.support.jersey.BoolParam;
import gd.support.jersey.LongParam;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;


@Path("/adm/sessions")
@Singleton
public class ManageSessionResourceImpl
    implements
        ManageSessionResource
{
    protected SessionManager sessMgr;

    protected SessionRepository sessRepo;


    public ManageSessionResourceImpl()
    {
        this.sessMgr = SessionManager.instance();

        RepositoryManager repoMgr = RepositoryManager.instance();
        this.sessRepo =
            repoMgr.get(
                RepositoryType.SESSION,
                SessionRepository.class);
    }


    @Override
    @Path("/{token}")
    @DELETE
    public Response close(
        @Context HttpHeaders headers,
        @PathParam("token") String token)
    {
        Rests.beginAdminOperation(headers, "adm:msess:close:" + token);
        
        GDUtils.check(
            (!Strings.isNullOrEmpty(token)),
            Status.BAD_REQUEST,
            "Close empty session token");
        
        if (!this.sessMgr.closeSession(token))
        {
            return (Response.status(Status.INTERNAL_SERVER_ERROR)
                .entity("Fail to close Session " + token)
                .build());
        }
        return Response.ok().build();
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getAllSessions(
        @Context HttpHeaders headers,
        @QueryParam("user_id") LongParam userIdParam,
        @QueryParam("only_open") @DefaultValue("0") final BoolParam onlyOpenParam)
    {
        Rests.beginAdminOperation(headers, "adm:msess:getAll");
        
        GDUtils.check(
            (userIdParam != null),
            Status.BAD_REQUEST,
            "Missing user_id");
        
        final List<SessionRep> spl = Lists.newArrayList();

        this.sessRepo.visitAll(new Visitor<Session>() {
            @Override
            public boolean visit(
                Session item)
            {
                if (onlyOpenParam.get())
                {
//                    if (item.isClosed() || item.isExpired())
//                        return true;
                }

                SessionRep rep = new SessionRep();
                Reps.populateSession(rep, item);
                spl.add(rep);
                return true;
            }
        });

        return Response.ok(spl).build();
    }

}
