
package gd.impl.session;


import gd.impl.notification.NotificationToken;
import gd.impl.repository.RepositoryConfig;
import gd.impl.util.Visitor;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;


public class ResidentSessionRepository
    implements
        SessionRepository
{
    /**
     * Indexed by token
     */
    protected Map<String,Session> entries;


    public ResidentSessionRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public synchronized Session add(
        Session s)
    {
        String token = s.getToken();
        if (this.entries.containsKey(token))
        {
            // Already in use
            return null;
        }

        this.entries.put(token, s);
        return new Session(s);
    }


    @Override
    public synchronized boolean deleteByNotification(
        NotificationToken notiToken)
    {
        String foundToken = null;
        for (Map.Entry<String,Session> etry : entries.entrySet())
        {
            Session s = etry.getValue();
            if (notiToken.equals(s.getNotificationToken()))
            {
                foundToken = etry.getKey();
                break;
            }
        }

        if (foundToken != null)
        {
            return this.entries.remove(foundToken) != null;
        }
        return false;
    }


    @Override
    public synchronized boolean delete(
        String token)
    {
        return this.entries.remove(token) != null;
    }


    @Override
    public synchronized boolean exists(
        String token)
    {
        return this.entries.containsKey(token);
    }


    @Override
    public synchronized Session findByOwner(
        long ownerId)
    {
        for (Session sess : entries.values())
        {
            
        }
        return null;
    }


    @Override
    public synchronized Session get(
        String token)
    {
        return this.entries.get(token);
    }


    @Override
    public synchronized Session update(
        Session newSess)
    {
        String token = newSess.getToken();
        Session oldSess = this.entries.get(token);
        if (oldSess == null)
        {
            return null;
        }

        //TODO: update conflict handle??

        this.entries.put(token, newSess);
        return new Session(newSess);
    }


    @Override
    public void visitAll(
        Visitor<Session> visitor)
    {
        // TODO Auto-generated method stub

    }


    @Override
    public void exportData(
        String fname)
    {
        // TODO Auto-generated method stub

    }


    @Override
    public void importData(
        String fname)
    {
        // TODO Auto-generated method stub

    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        this.entries = Maps.newHashMap();
    }


    @Override
    public Session put(
        Session t)
    {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public List<Session> findByNotification(
        NotificationToken notiToken)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
