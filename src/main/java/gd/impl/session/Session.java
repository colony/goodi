
package gd.impl.session;


import gd.impl.notification.NotificationToken;

import java.util.Date;


public class Session
{
    private Date createTime;
    
//    private long endTime;

    /**
     * The targeted expiration time (ms since epoch). If less than
     * zero, it means NEVER EXPIRE
     */
//    private long expireTime;

    private NotificationToken notificationToken;
    
    private long ownerId;

//    private Rights restrictions;

    private String token;

    private Date updateTime;
    

    public Session()
    {
    }


    /**
     * Copy constructor
     */
    public Session(
        Session s)
    {
        this.createTime = s.createTime;
        this.notificationToken = s.notificationToken;
        this.ownerId = s.ownerId;
        this.token = s.token;
        this.updateTime = s.updateTime;
    }


//    public boolean isClosed()
//    {
//        return this.endTime != 0L;
//    }
//
//
//    public boolean isExpired()
//    {
//        return System.currentTimeMillis() >= this.expireTime;
//    }


    //
    // GETTERS & SETTERS
    //

    public Date getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        Date startTime)
    {
        this.createTime = startTime;
    }
    

    public NotificationToken getNotificationToken()
    {
        return notificationToken;
    }


    public void setNotificationToken(
        NotificationToken noToken)
    {
        this.notificationToken = noToken;
    }
    
    
    public long getOwnerId()
    {
        return ownerId;
    }
    
    
    public void setOwnerId(long ownerId)
    {
        this.ownerId = ownerId;
    }


//    public Rights getRestrictions()
//    {
//        return restrictions;
//    }
//
//
//    public void setRestrictions(
//        Rights res)
//    {
//        this.restrictions = res;
//    }

    public String getToken()
    {
        return token;
    }


    public void setToken(
        String token)
    {
        this.token = token;
    }
    
    
    public Date getUpdateTime()
    {
        return updateTime;
    }
    
    
    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }
}
