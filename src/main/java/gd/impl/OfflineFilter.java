
package gd.impl;


import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;


@Provider
@PreMatching
@Priority(2) // Request filer execute in ascending order
public class OfflineFilter
    implements
        ContainerRequestFilter
{
    @Override
    public void filter(
        ContainerRequestContext reqCtx)

        throws IOException
    {
        if (GDLifecycle.isOffline())
        {
            String relativeUrl = StringUtils.stripStart(reqCtx.getUriInfo().getPath(), "/");
            if (relativeUrl.startsWith("adm"))
            {
                // Administrative operation won't be affected by offline
                return;
            }
            
            reqCtx.abortWith(
                Response.status(Status.SERVICE_UNAVAILABLE)
                    .entity("Service offline")
                    .header("Retry-After", 120) // seconds
                    .build());
        }
    }
}
