
package gd.impl.relation;


import gd.impl.util.GDUtils;

import com.google.common.base.Strings;


public class RelationConfig
{
    private RelationType id;

    private String implClassName;

    private String tableName;

    private int batchSize = 100;  //visit 100 result at a time


    /**
     * Default constructor
     */
    public RelationConfig()
    {
    }


    public RelationConfig(
        RelationType id)
    {
        this.id = id;
    }


    public RelationType getId()
    {
        return id;
    }


    public String getTableName()
    {
        return tableName;
    }


    public int getBatchSize()
    {
        return batchSize;
    }


    public String getImplClassName()
    {
        if (Strings.isNullOrEmpty(this.implClassName))
        {
            return id.defaultImplClass().getName();
        }
        return this.implClassName;
    }


    public Class<? extends Relation<?,?,?,?>> implClass()
    {
        Class<?> rawClass;
        try
        {
            rawClass = Class.forName(this.getImplClassName());
            Class<? extends Relation<?,?,?,?>> implClass =
                GDUtils.cast(rawClass.asSubclass(Relation.class));
            return implClass;
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(
                "Fail to find the impl class for index " + id,
                e);
        }
    }
}
