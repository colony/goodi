
package gd.impl.relation;


public interface RelationExtension2<SID,SRC extends RelationEntry<SID>,DID,DST extends RelationEntry<DID>>
    extends
        Relation<SID,SRC,DID,DST>
{
    /**
     * Atomic modify the edge by specified sid and did with data.
     * Set null value in parameter (s1,s2,s3,l1,l2) to skip updating
     * that particular field. Also, for long (l1,l2) value,
     * additional boolean is provided to determine if the update is
     * either incremental or assignment. Note the operation should
     * refresh token
     * 
     * @param sid
     * @param did
     * @param s1
     * @param s2
     * @param s3
     * @param l1
     * @param isAssignL1
     * @param l2
     * @param isAssignL2
     * @return True if the update is performed, false if the edge
     *         specified by sid and did doesn't exist
     */
    public boolean modifyEdgeByData(
        SID sid,
        DID did,
        String s1,
        String s2,
        String s3,
        Long l1,
        boolean isAssignL1,
        Long l2,
        boolean isAssignL2);


    /**
     * Atmoic modify the source specified by sid with data. Check
     * {@link #modifyEdgeByData} for details
     * 
     * @param sid
     * @param s1
     * @param s2
     * @param s3
     * @param l1
     * @param isAssignL1
     * @param l2
     * @param isAssignL2
     * @return True if the update is performed, false if the source
     *         specified by sid doesn't exist
     */
    public boolean modifySourceByData(
        SID sid,
        String s1,
        String s2,
        String s3,
        Long l1,
        boolean isAssignL1,
        Long l2,
        boolean isAssignL2);
}
