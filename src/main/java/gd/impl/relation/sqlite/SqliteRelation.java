
package gd.impl.relation.sqlite;


import gd.impl.relation.Relation;
import gd.impl.relation.RelationAdmin;
import gd.impl.relation.RelationEntry;
import gd.impl.relation.RelationExtension2;


public interface SqliteRelation<SID,S extends RelationEntry<SID>,DID,D extends RelationEntry<DID>>
    extends
        Relation<SID,S,DID,D>,
        RelationAdmin,
        RelationExtension2<SID,S,DID,D>
{
    /**
     * The Sqlite implmentation of relation family contains a
     * specific metadata table which record information regard with
     * all relation tables. Such as numOfEdges of each source. Here
     * are convenient method to access and update the metadata.
     */


    public long getSqliteRelationMetadataEdgeCount(
        SID sid);


    public void incrSqliteRelationMetadataEdgeCount(
        SID sid,
        long incr);


    public void putSqliteRelationMetadataEdgeCount(
        SID sid,
        long count);
}
