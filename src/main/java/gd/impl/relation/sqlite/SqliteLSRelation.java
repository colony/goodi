
package gd.impl.relation.sqlite;


import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;

import java.sql.ResultSet;
import java.sql.SQLException;


public class SqliteLSRelation
    extends
        AbstractSqliteRelation<Long,LEntry,String,SEntry>
    implements
        LSRelation
{
    //
    // LSRelation METHODS
    //

    @Override
    public boolean addEdge(
        Long sid,
        String did)
    {
        return super.addEdge(sid, new SEntry(did));
    }
    
    
    //
    // Relation METHODS
    //
    
    @Override
    public String makeEdgeId(
        String didStr)
    {
        return didStr;
    }


    @Override
    public Long makeSourceId(
        String sidStr)
    {
        return Long.valueOf(sidStr);
    }


    //
    // OVERRIDE METHODS
    //

    @Override
    protected SEntry makeEdge(
        String did)
    {
        return new SEntry(did);
    }


    @Override
    protected SEntry makeEdge(
        ResultSet rs)
        throws SQLException
    {
        return new SEntry(rs.getString("des_id"));
    }


    @Override
    protected LEntry makeSource(
        Long sid)
    {
        return new LEntry(sid);
    }


    @Override
    protected LEntry makeSource(
        ResultSet rs)
        throws SQLException
    {
        return new LEntry(rs.getLong("src_id"));
    }
}
