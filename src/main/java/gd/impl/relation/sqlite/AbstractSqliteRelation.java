
package gd.impl.relation.sqlite;


import gd.impl.SQLiteManager;
import gd.impl.relation.AbstractRelationEntry;
import gd.impl.relation.RelationConfig;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.mutable.MutableLong;
import org.apache.commons.lang3.mutable.MutableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Each relation has a pair of tables: relation_source
 * 
 * relation_edge
 * 
 */
public abstract class AbstractSqliteRelation<SID extends Comparable<SID>,S extends AbstractRelationEntry<SID>,DID extends Comparable<DID>,D extends AbstractRelationEntry<DID>>
    implements
        SqliteRelation<SID,S,DID,D>
{
    protected static final Logger LOG = LoggerFactory
        .getLogger(AbstractSqliteRelation.class);

    private static final String METADATA_TABLE_NAME =
        "_relation_metadata";

    protected String sourceTableName;

    protected String edgeTableName;

    protected int batchSize;

    private SQLiteManager sqliteManager;

    protected RelationType type;


    protected AbstractSqliteRelation()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    //
    // ABSTRACT METHODS
    //

    protected abstract D makeEdge(
        DID did);


    protected abstract D makeEdge(
        ResultSet rs)
        throws SQLException;


    protected abstract S makeSource(
        SID sid);


    protected abstract S makeSource(
        ResultSet rs)
        throws SQLException;




    //
    // Relation METHODS
    //

    @Override
    public boolean addEdge(
        SID sid,
        D d)
    {
        final String sql =
            "INSERT INTO "
                + this.edgeTableName
                + " (src_id, des_id, ds1, ds2, ds3, dl1, dl2, token) VALUES"
                + " (?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] param =
        {
                sid, d.getId(), d.s1, d.s2, d.s3, d.l1, d.l2,
                d.getToken()
        };

        long l = SqliteUtils.executeInsertQuery(
            sql,
            param,
            "addEdge",
            false);
        if (l > 0)
        {
            this.incrSqliteRelationMetadataEdgeCount(sid, 1L);
        }
        return l > 0;
    }


    @Override
    public boolean addSource(
        S s)
    {

        final String sql =
            "INSERT INTO "
                + this.sourceTableName
                + " (src_id, ss1, ss2, ss3, sl1, sl2, token) VALUES"
                + " (?, ?, ?, ?, ?, ?, ?)";
        Object[] param =
        {
                s.getId(), s.s1, s.s2, s.s3, s.l1, s.l2,
                s.getToken()
        };

        return 0 < SqliteUtils.executeInsertQuery(
            sql,
            param,
            "addSource",
            false);
    }


//    @Override
//    public boolean deleteAllEdges(
//        SID sid)
//    {
//        final String sql = "DELETE FROM " + this.edgeTableName;
//        return SqliteUtils.executeQuery(sql, null, "deleteAllEdges");
//    }


//    @Override
//    public void deleteAllSources()
//    {
//        final String sql = "DELETE FROM " + this.sourceTableName;
//        SqliteUtils.executeQuery(sql, null, "deletAllSource");
//    }


    @Override
    public boolean deleteEdge(
        SID sid,
        DID did)
    {
        String sql =
            "DELETE FROM "
                + this.edgeTableName
                + " WHERE src_id=? AND des_id=?";
        Object[] param = {
                sid, did
        };

        boolean success =
            SqliteUtils.executeQuery(sql, param, "DeleteEdge");
        if (success)
        {
            this.incrSqliteRelationMetadataEdgeCount(sid, -1L);
        }
        return success;
    }


    @Override
    public boolean deleteSource(
        SID sid)
    {
        // 1. Remove all edges
        String sql1 =
            "DELETE FROM "
                + this.edgeTableName
                + " WHERE src_id=?";
        Object[] param1 = {
                sid
        };

        // 2. Remove the metadata count
        String sql2 =
            "DELETE FROM "
                + METADATA_TABLE_NAME
                + " WHERE relation=? AND src_id=?";
        Object[] param2 = {
                this.type.abbreviation(), sid
        };

        SqliteUtils.executeTransactionalQueries(
            "DeleteSourceRelated",
            sql1,
            param1,
            sql2,
            param2);

        // 3. Delete the source node if any
        String sql3 =
            "DELETE FROM "
                + this.sourceTableName
                + " WHERE src_id=?";
        Object[] param3 = {
                sid
        };
        return SqliteUtils.executeQuery(
            sql3,
            param3,
            "DeleteSource");
    }


    @Override
    public D getEdge(
        SID sid,
        final DID did)
    {
        final String sql = "select * from " + this.edgeTableName
            + " where src_id=? and des_id=?";
        Object[] param = {
                sid, did
        };

        final MutableObject<D> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "getEdge",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        D d = makeEdge(did);
                        d.s1 = rs.getString("ds1");
                        d.s2 = rs.getString("ds2");
                        d.s3 = rs.getString("ds3");
                        d.l1 = rs.getLong("dl1");
                        d.l2 = rs.getLong("dl2");
                        d.setToken(rs.getLong("token"));
                        mo.setValue(d);
                    }
                }
            });
        return mo.getValue();
    }


    @Override
    public S getSource(
        final SID sid)
    {
        final String sql = "select * from " + this.sourceTableName
            + " where src_id=?";
        Object[] param = {
                sid
        };

        final MutableObject<S> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "getSource",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        S s = makeSource(sid);
                        s.s1 = rs.getString("ss1");
                        s.s2 = rs.getString("ss2");
                        s.s3 = rs.getString("ss3");
                        s.l1 = rs.getLong("sl1");
                        s.l2 = rs.getLong("sl2");
                        s.setToken(rs.getLong("token"));
                        mo.setValue(s);
                    }
                }
            });
        return mo.getValue();
    }


    @Override
    public long getNumEdges(
        SID sid)
    {
        return this.getSqliteRelationMetadataEdgeCount(sid);
    }


    @Override
    public long getNumSources()
    {
        final MutableInt count = new MutableInt();
        final String sql =
            "select count(*) from " + this.sourceTableName;
        SqliteUtils.executeSelectQuery(
            sql,
            null,
            "getNumSources",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    count.setValue(rs.getInt(1));
                }
            });
        return count.getValue();
    }



    @Override
    public boolean hasEdge(
        SID sid,
        DID did)
    {
        final String sql =
            "select 1 from " + this.edgeTableName
                + " where src_id=? and des_id=?";
        Object[] param = {
                sid, did
        };

        final MutableBoolean mb = new MutableBoolean(false);
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "hasEdge",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }
            });
        return mb.booleanValue();
    }


    @Override
    public boolean hasSource(
        SID sid)
    {
        final String sql = "select 1 from " + this.sourceTableName
            + " where src_id=?";
        Object[] param = {
                sid
        };
        final MutableBoolean mb = new MutableBoolean(false);
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "hasSource",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }
            });
        return mb.booleanValue();
    }


    @Override
    public void init(
        RelationConfig config)
    {
        this.sourceTableName = config.getTableName() + "_source";
        this.edgeTableName = config.getTableName() + "_edge";
        this.batchSize = config.getBatchSize();
        this.type = config.getId();
        createSourceTable(sourceTableName);
        createEdgeTable(edgeTableName);
        createMetaTable(METADATA_TABLE_NAME);
        SqliteUtils.createSingleIndex(
            sourceTableName,
            sourceTableName + "_index",
            "src_id",
            true);
        SqliteUtils.createCompositeIndex(
            edgeTableName,
            edgeTableName + "_index",
            "src_id",
            "des_id",
            true);
        SqliteUtils.createCompositeIndex(
            METADATA_TABLE_NAME,
            "relation_meta_index",
            "relation",
            "src_id",
            true);
        this.sqliteManager = SQLiteManager.instance();
    }


    @Override
    public void putEdge(
        SID sid,
        D d)
    {
        // We need to keep num_edges counter consistent, hence, we only incr the count
        // when an insert execute successfully

        String sql =
            "UPDATE "
                + this.edgeTableName
                + " SET ds1=?,ds2=?,ds3=?,dl1=?,dl2=?,token=? WHERE src_id=? AND des_id=?";
        Object[] param = {
                d.s1, d.s2, d.s3, d.l1, d.l2,
                d.getToken(), sid, d.getId()
        };

        boolean updateSuccess =
            SqliteUtils.executeQuery(sql, param, "putEdge");
        if (!updateSuccess)
        {
            // Seems like the row doesn't exist, perform the insert
            sql =
                "INSERT INTO "
                    + this.edgeTableName
                    + " (src_id, des_id, ds1, ds2, ds3, dl1, dl2, token) VALUES"
                    + " (?,?,?,?,?,?,?,?)";
            param = new Object[] {
                    sid, d.getId(), d.s1, d.s2, d.s3, d.l1, d.l2,
                    d.getToken()
            };

            long insertSuccess =
                SqliteUtils.executeInsertQuery(
                    sql,
                    param,
                    "putEdge",
                    false);
            if (insertSuccess <= 0)
            {
                // Oops, seems like another thread creates the row before us, update the row
                // with specified data
                sql =
                    "UPDATE "
                        + this.edgeTableName
                        + " SET ds1=?,ds2=?,ds3=?,dl1=?,dl2=?,token=? WHERE src_id=? AND des_id=?";
                param = new Object[] {
                        d.s1, d.s2, d.s3, d.l1, d.l2,
                        d.getToken(), sid, d.getId()
                };
                SqliteUtils.executeQuery(sql, param, "putEdge");
            }
            else
            {
                this.incrSqliteRelationMetadataEdgeCount(sid, 1L);
            }
        }
    }


    @Override
    public void putSource(
        S s)
    {
        final String sql =
            "insert or replace into "
                + this.sourceTableName
                + " (src_id, ss1, ss2, ss3, sl1, sl2, token) values"
                + " (?,?,?,?,?,?,?)";
        Object[] param =
        {
                s.getId(), s.s1, s.s2, s.s3, s.l1, s.l2,
                s.getToken()
        };
        SqliteUtils.executeQuery(sql, param, "putSource");
    }


    @Override
    public boolean updateEdge(
        SID sid,
        D d)
        throws UpdateConflictException
    {
        long oldToken = d.getToken();
        d.refreshToken();
        long newToken = d.getToken();

        final String sql =
            "UPDATE "
                + this.edgeTableName
                + " SET ds1=?, ds2=?, ds3=?, dl1=?, dl2=?, token=? WHERE src_id=? AND des_id=? AND token=?";
        Object[] param =
        {
                d.s1, d.s2, d.s3, d.l1, d.l2, newToken, sid, d.getId(), oldToken
        };

        boolean success = SqliteUtils.executeQuery(
            sql,
            param,
            "updateEdge");

        // If no update performed, find out what went wrong and respond accordingly
        if (!success)
        {
            // Try to retrieve the old edge
            D oldD = this.getEdge(sid, d.getId());
            if (oldD == null)
            {
                return false;
            }

            // Either the edge was just resurrected or else the update token did not match
            // Either way it is officially a conflict
            throw new UpdateConflictException(
                "Update conflict - specified token: " + oldToken
                    + " ,expected token: " + oldD.getToken());
        }
        return true;
    }


    @Override
    public boolean updateSource(
        S s)
        throws UpdateConflictException
    {
        long oldToken = s.getToken();
        s.refreshToken();
        long newToken = s.getToken();

        final String sql =
            "update "
                + this.sourceTableName
                + " set ss1=?,ss2=?,ss3=?,sl1=?,sl2=?,token=? where src_id=? and token=?";
        Object[] param =
        {
                s.s1, s.s2, s.s3, s.l1, s.l2, newToken,
                s.getId(), oldToken
        };

        boolean success = SqliteUtils.executeQuery(
            sql,
            param,
            "updateSource");

        // If no update performed, find out what went wrong and respond accordingly
        if (!success)
        {
            // Try to retrieve the old edge
            S oldS = this.getSource(s.getId());
            if (oldS == null)
                return false;

            // Either the source was just resurrected or else the update token did not match
            // Either way it is officially a conflict
            throw new UpdateConflictException(
                "Update conflict - specified token: " + oldToken
                    + " ,expected token: " + oldS.getToken());
        }
        return true;
    }


    @Override
    public void visitAllEdges(
        SID sid,
        Visitor<D> visitor)
    {
        this.visitEdges(
            sid,
            Relations.SK_DID,
            null,
            false,
            false,
            visitor);
    }


    @Override
    public void visitAllSources(
        Visitor<S> visitor)
    {
        this.visitSources(
            Relations.SK_SID,
            null,
            false,
            false,
            visitor);
    }


    @SuppressWarnings("unchecked")
    @Override
    public void visitEdges(
        SID sid,
        String sortKey,
        Object boundary,
        boolean descending,
        boolean exclusive,
        Visitor<D> visitor)
    {

        // Default token of pagination only supports ordering
        if (!StringUtils.equals(sortKey, Relations.SK_DID))
        {
            throw new IllegalArgumentException(
                "Unsupported page key \"" + sortKey + "\"");
        }

        if (!visitor.before())
            return;

        Connection connection = null;
        PreparedStatement statement = null;

        boolean hasMore = true;
        boolean aborted = false;

        DID did = (DID) boundary;

        String sql1 =
            "select * from " + this.edgeTableName
                + " where src_id=? order by des_id "
                + (descending ? "desc" : "asc")
                + " limit "
                + this.batchSize;

        String sql2 =
            "select * from "
                + this.edgeTableName
                + " where src_id=?"
                + " and des_id "
                + (descending ? (exclusive ? "<?" : "<=?") : (exclusive ? ">?" : ">=?"))
                + " order by des_id "
                + (descending ? "desc" : "asc")
                + " limit "
                + this.batchSize;

        String sql = boundary == null ? sql1 : sql2;
        boolean needSwitchSql = boundary == null;

        try
        {
            connection = sqliteManager.getConnection();
            statement =
                connection.prepareStatement(sql);
            while (hasMore && !aborted)
            {
                statement.setObject(1, sid);
                if (did != null)
                    statement.setObject(2, did);

                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next())
                {
                    count++;

                    D d = this.makeEdge(rs);
                    did = d.getId();
                    d.s1 = rs.getString("ds1");
                    d.s2 = rs.getString("ds2");
                    d.s3 = rs.getString("ds3");
                    d.l1 = rs.getLong("dl1");
                    d.l2 = rs.getLong("dl2");

                    d.setSortKey(Relations.SK_DID);
                    d.setSortValue(String.valueOf(did)); //TODO, create abstract method makeEdgeIdStr

                    if (!visitor.visit(d))
                    {
                        aborted = true;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();

                // Use a different query (sql2) for further pagination given that the initial boundary is NULL
                if (hasMore && !aborted && needSwitchSql
                    && boundary == null)
                {
                    statement.close();
                    statement = connection.prepareStatement(sql2);
                    needSwitchSql = false;
                }
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Error on visitEdges query ",
                e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                LOG.error("Error on closing connection/statement on visitAllEdges query "
                    + e);
            }

        }

        visitor.after();
    }


    @SuppressWarnings("unchecked")
    @Override
    public void visitSources(
        String sortKey,
        Object boundary,
        boolean descending,
        boolean exclusive,
        Visitor<S> visitor)
    {
        // Default token of pagination only supports ordering
        if (!StringUtils.equals(sortKey, Relations.SK_SID))
        {
            throw new IllegalArgumentException(
                "Unsupported page key \"" + sortKey + "\"");
        }

        if (!visitor.before())
            return;

        Connection connection = null;
        PreparedStatement statement = null;

        boolean hasMore = true;
        boolean aborted = false;
        boolean firstBatch = false;

        SID sid = (SID) boundary;

        String sql1 =
            "select * from " + this.sourceTableName
                + " order by src_id "
                + (descending ? "desc" : "asc")
                + " limit "
                + this.batchSize;

        String sql2 =
            "select * from "
                + this.sourceTableName
                + " where src_id "
                + (descending ? (exclusive ? "<?" : "<=?") : (exclusive ? ">?" : ">=?"))
                + " order by src_id "
                + (descending ? "desc" : "asc")
                + " limit "
                + this.batchSize;

        String sql = boundary == null ? sql1 : sql2;

        try
        {
            connection = sqliteManager.getConnection();
            statement =
                connection.prepareStatement(sql);
            while (hasMore && !aborted)
            {
                if (sid != null)
                    statement.setObject(1, sid);

                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next())
                {
                    count++;

                    S s = this.makeSource(rs);
                    sid = s.getId();
                    s.s1 = rs.getString("ss1");
                    s.s2 = rs.getString("ss2");
                    s.s3 = rs.getString("ss3");
                    s.l1 = rs.getLong("sl1");
                    s.l2 = rs.getLong("sl2");
                    if (!visitor.visit(s))
                    {
                        aborted = false;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();

                // Use a different query (sql2) for further pagination given that the initial boundary is NULL
                if (hasMore && !aborted && firstBatch
                    && boundary == null)
                {
                    statement.close();
                    statement = connection.prepareStatement(sql2);
                    firstBatch = true;
                }
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Error on visitAllEdges query ",
                e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                LOG.error("Error on closing connection/statement on visitAllEdges query "
                    + e);
            }

        }
        visitor.after();
    }


    //
    // Relation interface for string conversion
    //

    @Override
    public String makeEdgeStr(
        D d)
    {
        return d.toStr();
    }


    @Override
    public String makeEdgeIdStr(
        DID did)
    {
        return did.toString();
    }


    @Override
    public String makeSourceStr(
        S s)
    {
        return s.toStr();
    }


    @Override
    public String makeSourceIdStr(
        SID sid)
    {
        return sid.toString();
    }



    //
    // SqliteRelation METHODS
    //

    public long getSqliteRelationMetadataEdgeCount(
        SID sid)
    {
        final MutableLong ml = new MutableLong(0L);
        String sql = "SELECT num_edges FROM "
            + METADATA_TABLE_NAME
            + " WHERE relation = ?"
            + " AND src_id = ?";

        SqliteUtils.executeSelectQuery(
            sql,
            new Object[] {
                    this.type.abbreviation(), sid
            },
            "getEdgeCount",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        ml.setValue(rs.getLong("num_edges"));
                    }
                }
            });
        return ml.getValue();
    }


    public void incrSqliteRelationMetadataEdgeCount(
        SID sid,
        long incr)
    {
        // NOTE: the metadata row for specified relation and sid might not exist yet
        // The upsert in SQL is done by INSERT OR REPLACE query, which delete the
        // existing row prior to a new insert, this is not performant in most cases,
        // Instead, we perform an UPDATE first, then upsert if UPDATE fail, this will
        // only slow the first time

        String sql =
            "UPDATE "
                + METADATA_TABLE_NAME
                + " SET num_edges=MAX(0,num_edges+?)"
                + " WHERE relation=? AND src_id=?";
        boolean success =
            SqliteUtils.executeQuery(sql,
                new Object[] {
                        incr, this.type.abbreviation(), sid
                },
                "incrRelMetaEdge");
        if (!success)
        {
            sql =
                "INSERT OR REPLACE INTO "
                    + METADATA_TABLE_NAME
                    + " (relation, src_id, num_edges)"
                    + " VALUES"
                    + " (?,?,COALESCE((SELECT MAX(0,num_edges+?) FROM "
                    + METADATA_TABLE_NAME
                    + " WHERE relation=? AND src_id=?),?))";
            SqliteUtils.executeQuery(sql,
                new Object[] {
                        this.type.abbreviation(), sid, incr,
                        this.type.abbreviation(), sid,
                        (incr < 0 ? 0 : incr)
                },
                "incrRelMetaEdge");
        }
    }


    public void putSqliteRelationMetadataEdgeCount(
        SID sid,
        long count)
    {
        String sql =
            "INSERT OR REPLACE INTO "
                + METADATA_TABLE_NAME
                + " (relation, src_id, num_edges)"
                + " VALUES"
                + " (?,?,?)";
        SqliteUtils.executeQuery(
            sql,
            new Object[] {
                    this.type.abbreviation(), sid, count
            },
            "putRelMetaEdge");
    }



    //
    // RelationAdmin METHODS
    //

    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        // throw new UnsupportedOperationException();
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
//        throw new UnsupportedOperationException();
    }



    //
    // RelationExtension2 METHODS
    //


    public boolean modifyEdgeByData(
        SID sid,
        DID did,
        String s1,
        String s2,
        String s3,
        Long l1,
        boolean isAssignL1,
        Long l2,
        boolean isAssignL2)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(this.edgeTableName);
        sb.append(" SET ");

        List<Object> paramList = new ArrayList<>();

        if (StringUtils.isNotEmpty(s1))
        {
            sb.append("ds1=?,");
            paramList.add(s1);
        }
        if (StringUtils.isNotEmpty(s2))
        {
            sb.append("ds2=?,");
            paramList.add(s2);
        }
        if (StringUtils.isNotEmpty(s3))
        {
            sb.append("ds3=?,");
            paramList.add(s3);
        }
        if (l1 != null)
        {
            if (isAssignL1)
                sb.append("dl1=?,");
            else
                sb.append("dl1=dl1+?,");
            paramList.add(l1);
        }
        if (l2 != null)
        {
            if (isAssignL1)
                sb.append("dl2=?,");
            else
                sb.append("dl2=dl2+?,");
            paramList.add(l2);
        }

        if (paramList.isEmpty())
        {
            // All param is NULL, no modification will happen in this case
            LOG.warn(
                "Fail to modifyEdgeByData sid={} did={} with all NULL values",
                sid,
                did);
            return false;
        }

        sb.append("token=?");
        paramList.add(System.currentTimeMillis());
        sb.append(" WHERE src_id=? AND des_id=?");
        paramList.add(sid);
        paramList.add(did);

        return SqliteUtils.executeQuery(
            sb.toString(),
            paramList.toArray(),
            "modifyEdgeByData");
    }


    public boolean modifySourceByData(
        SID sid,
        String s1,
        String s2,
        String s3,
        Long l1,
        boolean isAssignL1,
        Long l2,
        boolean isAssignL2)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(this.sourceTableName);
        sb.append(" SET ");

        List<Object> paramList = new ArrayList<>();

        if (StringUtils.isNotEmpty(s1))
        {
            sb.append("ss1=?,");
            paramList.add(s1);
        }
        if (StringUtils.isNotEmpty(s2))
        {
            sb.append("ss2=?,");
            paramList.add(s2);
        }
        if (StringUtils.isNotEmpty(s3))
        {
            sb.append("ss3=?,");
            paramList.add(s3);
        }
        if (l1 != null)
        {
            if (isAssignL1)
                sb.append("sl1=?,");
            else
                sb.append("sl1=sl1+?,");
            paramList.add(l1);
        }
        if (l2 != null)
        {
            if (isAssignL1)
                sb.append("sl2=?,");
            else
                sb.append("sl2=sl2+?,");
            paramList.add(l2);
        }

        if (paramList.isEmpty())
        {
            // All param is NULL, no modification will happen in this case
            LOG.warn(
                "Fail to modifySourceByData sid={} with all NULL values",
                sid);
            return false;
        }

        sb.append("token=?");
        paramList.add(System.currentTimeMillis());
        sb.append(" WHERE src_id=?");
        paramList.add(sid);

        return SqliteUtils.executeQuery(
            sb.toString(),
            paramList.toArray(),
            "modifySourceByData");
    }



    //
    // INTERNAL METHODS
    //


    private void createEdgeTable(
        String edgeTableName)
    {
        /**
         * Use type affinity to help Sqlite decide which storage
         * class to store. Here, since different relation has
         * different Java types of ID (long, string, etc), we use
         * NUMERIC affinity for src_id and des_id for max
         * flexibility. Sqlite will be smart enough to choose right
         * storage class based on the inserted data
         */
        final String createEdgeTableSQL =
            "create table if not exists "
                + this.edgeTableName
                + "(src_id numeric,"
                + "des_id numeric,"
                + "ds1 text," + "ds2 text," + "ds3 text,"
                + "dl1 integer,"
                + "dl2 integer,"
                + "token integer"
                + " )";

        SqliteUtils.executeQuery(
            createEdgeTableSQL,
            null,
            "CreateEdgeTable"
                + this.edgeTableName);
    }


    private void createMetaTable(
        String metaTableName)
    {
        final String createMetaTableSQL =
            "CREATE TABLE IF NOT EXISTS "
                + metaTableName
                + "("
                + "relation TEXT,"
                + "src_id NUMERIC,"
                + "num_edges INTEGER"
                + ")";
        SqliteUtils.executeQuery(
            createMetaTableSQL,
            null,
            "CreateMetaTable" + metaTableName);
    }


    private void createSourceTable(
        String sourceTableName)
    {
        final String createSourceTableSQL =
            "create table if not exists "
                + this.sourceTableName
                + "(src_id numeric,"
                + "ss1 text," + "ss2 text," + "ss3 text,"
                + "sl1 integer,"
                + "sl2 integer,"
                + "token integer"
                + " )";
        SqliteUtils.executeQuery(
            createSourceTableSQL,
            null,
            "CreateSourceTable"
                + this.sourceTableName);
    }
}
