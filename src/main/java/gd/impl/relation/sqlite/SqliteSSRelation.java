
package gd.impl.relation.sqlite;


import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SSRelation;

import java.sql.ResultSet;
import java.sql.SQLException;


public class SqliteSSRelation
    extends
        AbstractSqliteRelation<String,SEntry,String,SEntry>
    implements
        SSRelation
{
    //
    // SSRelation METHODS
    //

    @Override
    public boolean addEdge(
        String sid,
        String did)
    {
        return super.addEdge(sid, new SEntry(did));
    }


    //
    // Relation METHODS
    //

    @Override
    public String makeEdgeId(
        String didStr)
    {
        return didStr;
    }


    @Override
    public String makeSourceId(
        String sidStr)
    {
        return sidStr;
    }



    //
    // OVERRIDE METHODS
    //



    @Override
    protected SEntry makeEdge(
        String did)
    {
        return new SEntry(did);
    }


    @Override
    protected SEntry makeEdge(
        ResultSet rs)
        throws SQLException
    {
        return new SEntry(rs.getString("des_id"));
    }


    @Override
    protected SEntry makeSource(
        String sid)
    {
        return new SEntry(sid);
    }


    @Override
    protected SEntry makeSource(
        ResultSet rs)
        throws SQLException
    {
        return new SEntry(rs.getString("src_id"));
    }
}
