
package gd.impl.relation.sqlite;


import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;

import java.sql.ResultSet;
import java.sql.SQLException;


public class SqliteLLRelation
    extends
        AbstractSqliteRelation<Long,LEntry,Long,LEntry>
    implements
        LLRelation
{
    //
    // LLRelation METHODS
    //

    @Override
    public boolean addEdge(
        Long sid,
        Long did)
    {
        return super.addEdge(sid, new LEntry(did));
    }


    //
    // Relation METHODS
    //

    @Override
    public Long makeEdgeId(
        String didStr)
    {
        return Long.valueOf(didStr);
    }


    @Override
    public Long makeSourceId(
        String sidStr)
    {
        return Long.valueOf(sidStr);
    }


    //
    // OVERRIDE METHODS
    //

    @Override
    protected LEntry makeEdge(
        Long did)
    {
        return new LEntry(did);
    }


    @Override
    protected LEntry makeEdge(
        ResultSet rs)
        throws SQLException
    {
        return new LEntry(rs.getLong("des_id"));
    }



    @Override
    protected LEntry makeSource(
        Long sid)
    {
        return new LEntry(sid);
    }


    @Override
    protected LEntry makeSource(
        ResultSet rs)
        throws SQLException
    {
        return new LEntry(rs.getLong("src_id"));
    }
}
