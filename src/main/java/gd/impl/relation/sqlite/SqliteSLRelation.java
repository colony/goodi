
package gd.impl.relation.sqlite;


import java.sql.ResultSet;
import java.sql.SQLException;

import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SLRelation;


public class SqliteSLRelation
    extends
        AbstractSqliteRelation<String,SEntry,Long,LEntry>
    implements
        SLRelation
{
    //
    // SLRelation METHODS
    //

    @Override
    public boolean addEdge(
        String sid,
        Long did)
    {
        return super.addEdge(sid, new LEntry(did));
    }
    
    
    //
    // Relation METHODS
    //
    
    @Override
    public Long makeEdgeId(
        String didStr)
    {
        return Long.valueOf(didStr);
    }


    @Override
    public String makeSourceId(
        String sidStr)
    {
        return sidStr;
    }


    //
    // OVERRIDE METHODS
    //

    @Override
    protected LEntry makeEdge(
        Long did)
    {
        return new LEntry(did);
    }


    @Override
    protected LEntry makeEdge(
        ResultSet rs)
        throws SQLException
    {
        return new LEntry(rs.getLong("des_id"));
    }


    @Override
    protected SEntry makeSource(
        String sid)
    {
        return new SEntry(sid);
    }


    @Override
    protected SEntry makeSource(
        ResultSet rs)
        throws SQLException
    {
        return new SEntry(rs.getString("src_id"));
    }
}
