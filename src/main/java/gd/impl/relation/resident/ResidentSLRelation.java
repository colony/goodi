
package gd.impl.relation.resident;


import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SLRelation;


public class ResidentSLRelation
    extends
        AbstractResidentRelation<String,SEntry,Long,LEntry>
    implements
        SLRelation
{
    //
    // SLRelation METHODS
    //

    @Override
    public boolean addEdge(
        String sid,
        Long did)
    {
        return super.addEdge(sid, new LEntry(did));
    }


    //
    // OVERRIDE METHODS
    //

    @Override
    protected LEntry makeEdge(
        Long did)
    {
        return new LEntry(did);
    }


    @Override
    protected SEntry makeSource(
        String sid)
    {
        return new SEntry(sid);
    }


    //
    // Relation METHODS
    //


    @Override
    public Long makeEdgeId(
        String didStr)
    {
        return Long.valueOf(didStr);
    }


    @Override
    public String makeSourceId(
        String sidStr)
    {
        return sidStr;
    }
}
