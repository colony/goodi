
package gd.impl.relation.resident;


import gd.impl.relation.Relation;
import gd.impl.relation.RelationAdmin;
import gd.impl.relation.RelationEntry;
import gd.impl.relation.RelationExtension2;


public interface ResidentRelation<SID,S extends RelationEntry<SID>,DID,D extends RelationEntry<DID>>
    extends
        Relation<SID,S,DID,D>,
        RelationAdmin,
//        RelationExtension<SID,S,DID,D>
        RelationExtension2<SID,S,DID,D>
{
    public boolean isDirty();
}
