
package gd.impl.relation.resident;


import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SSRelation;


public class ResidentSSRelation
    extends
        AbstractResidentRelation<String,SEntry,String,SEntry>
    implements
        SSRelation
{
    //
    // SSRelation METHODS
    //

    @Override
    public boolean addEdge(
        String sid,
        String did)
    {
        return super.addEdge(sid, new SEntry(did));
    }


    //
    // OVERRIDE METHODS
    //

    @Override
    protected SEntry makeEdge(
        String did)
    {
        return new SEntry(did);
    }

    @Override
    protected SEntry makeSource(
        String sid)
    {
        return new SEntry(sid);
    }


    //
    // Relation METHODS
    //


    @Override
    public String makeEdgeId(
        String didStr)
    {
        return didStr;
    }


    @Override
    public String makeSourceId(
        String sidStr)
    {
        return sidStr;
    }
}
