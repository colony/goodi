
package gd.impl.relation.resident;


import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.impl.relation.AbstractRelationEntry;
import gd.impl.relation.RelationConfig;
import gd.impl.relation.RelationExporter;
import gd.impl.relation.RelationExporter.RelationExporterEdgeEntry;
import gd.impl.relation.RelationExporter.RelationExporterSourceEntry;
import gd.impl.relation.Relations;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;


public abstract class AbstractResidentRelation<SID extends Comparable<SID>,S extends AbstractRelationEntry<SID>,DID extends Comparable<DID>,D extends AbstractRelationEntry<DID>>
    implements
        ResidentRelation<SID,S,DID,D>
{
    protected static final Logger LOG =
        LoggerFactory.getLogger(AbstractResidentRelation.class);

    protected TreeTable data;

    /**
     * true if a modification has been made since the last call to
     * exportData, false if the current data does not need to be
     * written to persistent storage. Note that this field is NOT
     * persisted.
     */
    protected transient boolean dirty;

    protected Lock readLock;

    protected ReentrantReadWriteLock rwParentLock;

    protected Map<SID,S> sources;

    protected Lock writeLock;


    protected AbstractResidentRelation()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    //
    // ABSTRACT OR OVERRIDEABLE METHODS
    //

    protected abstract D makeEdge(
        DID did);


    protected abstract S makeSource(
        SID sid);



    //
    // ResidentRelation METHODS
    //

    public boolean isDirty()
    {
        return dirty;
    }


    //
    // Relation METHODS for string conversion
    //

    @Override
    public String makeEdgeStr(
        D d)
    {
        return d.toStr();
    }


    @Override
    public String makeEdgeIdStr(
        DID did)
    {
        return String.valueOf(did);
    }


    @Override
    public String makeSourceStr(
        S s)
    {
        return s.toStr();
    }


    @Override
    public String makeSourceIdStr(
        SID sid)
    {
        return String.valueOf(sid);
    }



    //
    // Relation METHODS
    //


    @Override
    public boolean addEdge(
        SID sid,
        D d)
    {
        this.writeLock.lock();
        try
        {
            if (this.data.containsColumn(sid, d.getId()))
            {
                return false;
            }

            this.data.put(sid, d.getId(), d);
            if (!this.sources.containsKey(sid))
            {
                this.sources.put(sid, makeSource(sid));
            }
            this.dirty = true;
            return true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean addSource(
        S s)
    {
        this.writeLock.lock();
        try
        {
            if (this.sources.containsKey(s.getId()))
                return false;
            this.sources.put(s.getId(), s);
            this.dirty = true;
            return true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean deleteEdge(
        SID sid,
        DID did)
    {
        this.writeLock.lock();
        try
        {
            boolean deleted = (null != this.data.remove(sid, did));
            if (deleted)
                this.dirty = true;
            return deleted;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean deleteSource(
        SID sid)
    {
        this.writeLock.lock();
        try
        {
            boolean notNull = (null != this.data.remove(sid));
            boolean notNull2 = (null != this.sources.remove(sid));
            if (notNull || notNull2)
                this.dirty = true;
            return notNull2;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public D getEdge(
        SID sid,
        DID did)
    {
        this.readLock.lock();
        try
        {
            return this.data.get(sid, did);
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @Override
    public long getNumEdges(
        SID sid)
    {
        this.readLock.lock();
        try
        {
            Map<DID,D> rowMap = this.data.row(sid);
            if (rowMap == null)
            {
                return 0;
            }
            return rowMap.size();
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @Override
    public long getNumSources()
    {
        this.readLock.lock();
        try
        {
            return this.data.size();
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @Override
    public S getSource(
        SID sid)
    {
        this.readLock.lock();
        try
        {
            return this.sources.get(sid);
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @Override
    public boolean hasEdge(
        SID sid,
        DID did)
    {
        this.readLock.lock();
        try
        {
            return this.data.containsColumn(sid, did);
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @Override
    public boolean hasSource(
        SID sid)
    {
        this.readLock.lock();
        try
        {
            return this.data.containsRow(sid);
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @Override
    public void putEdge(
        SID sid,
        D d)
    {
        this.writeLock.lock();
        try
        {
            this.data.put(sid, d.getId(), d);
            if (!this.sources.containsKey(sid))
            {
                this.sources.put(sid, makeSource(sid));
            }
            this.dirty = true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public void putSource(
        S s)
    {
        this.writeLock.lock();
        try
        {
            this.sources.put(s.getId(), s);
            this.dirty = true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean updateEdge(
        SID sid,
        D newD)
        throws UpdateConflictException
    {
        if (newD == null)
        {
            throw new IllegalArgumentException("Update null edge");
        }

        this.writeLock.lock();
        try
        {
            D oldD = this.data.get(sid, newD.getId());
            if (oldD == null)
            {
                // Bail out if the edge doesn't even exist
                return false;
            }

            long newToken = newD.getToken();
            long oldToken = oldD.getToken();
            if (newToken != oldToken)
            {
                throw new UpdateConflictException(
                    "Edge Update conflict - new token:"
                        + newToken
                        + " current token:"
                        + oldToken);
            }

            // Now we can safely update the token
            newD.refreshToken();

            this.data.put(sid, newD.getId(), newD);
            this.dirty = true;
            return true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean updateSource(
        S newS)
        throws UpdateConflictException
    {
        if (newS == null)
        {
            throw new IllegalArgumentException("Update null source");
        }

        this.writeLock.lock();
        try
        {
            S oldS = this.sources.get(newS.getId());
            if (oldS == null)
            {
                // Bail out if the source doesn't even exist
                return false;
            }

            long newToken = newS.getToken();
            long oldToken = oldS.getToken();
            if (newToken != oldToken)
            {
                throw new UpdateConflictException(
                    "Source Update conflict - new token:"
                        + newToken
                        + " current token:"
                        + oldToken);
            }

            // Now we can safely update the token
            newS.refreshToken();
            this.sources.put(newS.getId(), newS);
            this.dirty = true;
            return true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public void visitAllEdges(
        SID sid,
        Visitor<D> visitor)
    {
        this.readLock.lock();
        try
        {
            if (!visitor.before())
            {
                return;
            }

            // Visit order is indeterministic
            Map<DID,D> rowMap = this.data.row(sid);
            if (rowMap != null)
            {
                for (D e : rowMap.values())
                {
                    if (!visitor.visit(e))
                        break;
                }
            }

            visitor.after();
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @Override
    public void visitAllSources(
        Visitor<S> visitor)
    {
        this.readLock.lock();
        try
        {
            if (!visitor.before())
            {
                return;
            }

            // Visit order is indeterministic
            if (this.sources != null)
            {
                for (S s : this.sources.values())
                {
                    if (!visitor.visit(s))
                        break;
                }
            }

            visitor.after();
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public void visitEdges(
        SID sid,
        String sortKey,
        Object boundary,
        boolean descending,
        boolean exclusive,
        Visitor<D> visitor)
    {
        // Resident version of pagination only supports ordering
        // on destination ID
        if (!StringUtils.equals(sortKey, Relations.SK_DID))
        {
            throw new IllegalArgumentException(
                "Unsupported page key \"" + sortKey + "\"");
        }

        this.readLock.lock();
        try
        {
            if (!visitor.before())
            {
                return;
            }

            Map<DID,D> rowMap = this.data.row(sid);
            if (rowMap != null)
            {
                NavigableMap<DID,D> m =
                    (NavigableMap<DID,D>) rowMap;
                Map<DID,D> sub;

                if (boundary == null)
                {
                    sub = descending ? m.descendingMap() : m;
                }
                else
                {
                    DID k = (DID) boundary;
                    sub =
                        descending ?
                            //  m.h
                            m.headMap(k, !exclusive)
                                .descendingMap() :
                            m.tailMap(k, !exclusive);
                }

                // Visit each entry
                for (D e : sub.values())
                {
                    // Populate sort info
                    e.setSortKey(Relations.SK_DID);
                    e.setSortValue(String.valueOf(e.getId()));
                    if (!visitor.visit(e))
                        break;
                }
            }

            visitor.after();
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public void visitSources(
        String sortKey,
        Object boundary,
        boolean descending,
        boolean exclusive,
        Visitor<S> visitor)
    {
        // Default version of pagination only supports ordering
        // on destination ID
        if (!StringUtils.equals(sortKey, Relations.SK_SID))
        {
            throw new IllegalArgumentException(
                "Unsupported page key \"" + sortKey + "\"");
        }

        this.readLock.lock();
        try
        {
            if (!visitor.before())
            {
                return;
            }

            if (this.sources != null)
            {
                NavigableMap<SID,S> m =
                    (NavigableMap<SID,S>) this.sources;
                Map<SID,S> sub;

                if (boundary == null)
                {
                    sub = descending ? m.descendingMap() : m;
                }
                else
                {
                    SID k = (SID) boundary;
                    sub =
                        descending ?
                            //  m.h
                            m.headMap(k, !exclusive)
                                .descendingMap() :
                            m.tailMap(k, !exclusive);
                }

                // Visit each entry
                for (S e : sub.values())
                {
                    if (!visitor.visit(e))
                        break;
                }
            }

            visitor.after();
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    //
    // RelationAdmin METHODS
    //


    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        // This is a read operation since the relation is not modified
        this.readLock.lock();

        LOG.info("Exporting relation to file {}", fname);

        Path path = Paths.get(fname);
        Files.createDirectories(path.getParent());

        RelationExporter exporter = new RelationExporter();
        try (
             OutputStream os =
                 Files.newOutputStream(path, CREATE, WRITE, TRUNCATE_EXISTING);
             BufferedOutputStream bos =
                 new BufferedOutputStream(os);)
        {
            exporter.writeHeader(bos);

            for (SID sid : this.data.keySet()) {
                // Write source node
                RelationExporterSourceEntry sentry = new RelationExporterSourceEntry();
                sentry.id = this.makeSourceIdStr(sid);
                sentry.numEdges = this.getNumEdges(sid);
                S src = this.sources.get(sid);
                if (src != null) {
                    sentry.token = src.getToken();
                    sentry.s1 = src.s1;
                    sentry.s2 = src.s2;
                    sentry.s3 = src.s3;
                    sentry.l1 = src.l1;
                    sentry.l1 = src.l2;
                }
                exporter.writeSource(bos, sentry);

                // Write each edges per source
                Map<DID,D> rowMap = this.data.row(sid);
                for (Map.Entry<DID,D> e : rowMap.entrySet()) {
                    RelationExporterEdgeEntry eentry = new RelationExporterEdgeEntry();
                    eentry.id = this.makeEdgeIdStr(e.getKey());
                    eentry.sourceId = this.makeSourceIdStr(sid);
                    D dst = e.getValue();
                    if (dst != null) {
                        eentry.token = src.getToken();
                        eentry.s1 = src.s1;
                        eentry.s2 = src.s2;
                        eentry.s3 = src.s3;
                        eentry.l1 = src.l1;
                        eentry.l1 = src.l2;
                    }
                    exporter.writeEdge(bos, eentry);
                }
            }

            this.dirty = false;
        }
        finally
        {
            this.readLock.unlock();
            LOG.info("Complete export relation to file {}", fname);
        }
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        // It is assumed that this will normally be called on an inactive instance
        // of the relation, so the fact that we acquire an exclusive write lock
        // should not interfere with ordinary run-time requests
        this.writeLock.lock();

        Path path = Paths.get(fname);
        if (Files.notExists(path))
        {
            throw new IllegalArgumentException(String.format(
                "Import file %s doesn't exist",
                fname));
        }

        LOG.info("Importing relation from file {}", fname);

        RelationExporter exporter = new RelationExporter();
        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            // Initialize the exporter with header version internally
            exporter.readHeader(bis);

            // Cleanup
            this.data = new TreeTable();
            this.sources = Maps.newHashMap();

            RelationExporterSourceEntry sentry = exporter.readSource(bis);
            while (sentry != null) {
                SID sid = this.makeSourceId(sentry.id);
                S s = this.makeSource(sid);
                s.setToken(sentry.token);
                s.s1 = sentry.s1;
                s.s2 = sentry.s2;
                s.s3 = sentry.s3;
                s.l1 = sentry.l1;
                s.l2 = sentry.l2;
                this.sources.put(sid, s);

                for (int i = 0; i < sentry.numEdges; i++) {
                    RelationExporterEdgeEntry eentry = exporter.readEdge(bis);
                    if (eentry.sourceId == null) {
                        LOG.warn("Ignore edge data - no source id found");
                    }
                    DID did = this.makeEdgeId(eentry.id);
                    D d = this.makeEdge(did);
                    d.setToken(eentry.token);
                    d.s1 = eentry.s1;
                    d.s2 = eentry.s2;
                    d.s3 = eentry.s3;
                    d.l1 = eentry.l1;
                    d.l2 = eentry.l2;
                    this.data.put(sid, did, d);
                }

                // Read next data
                sentry = exporter.readSource(bis);
            }

            this.dirty = false;
        }
        finally {
            this.writeLock.unlock();
            LOG.info("Done import relation from file {}", fname);
        }
    }


    @Override
    public synchronized void init(
        RelationConfig config)
    {
        this.data = new TreeTable();
        this.sources = Maps.newHashMap();
        this.rwParentLock = new ReentrantReadWriteLock(true); //fair
        this.readLock = this.rwParentLock.readLock();
        this.writeLock = this.rwParentLock.writeLock();
        this.dirty = false;
    }


    //
    // RelationExtension2 METHODS
    //

    public boolean modifyEdgeByData(
        SID sid,
        DID did,
        String s1,
        String s2,
        String s3,
        Long l1,
        boolean isAssignL1,
        Long l2,
        boolean isAssignL2)
    {
        this.writeLock.lock();
        try {
            D d = this.getEdge(sid, did);
            if (d == null)
                return false;

            if (s1 != null)
                d.s1 = s1;
            if (s2 != null)
                d.s2 = s2;
            if (s3 != null)
                d.s3 = s3;
            if (l1 != null)
                d.l1 = isAssignL1 ? l1 : (d.l1 + l1);
            if (l2 != null)
                d.l2 = isAssignL2 ? l2 : (d.l2 + l2);

            this.dirty = true;
            return true;
        }
        finally {
            this.writeLock.unlock();
        }
    }


    public boolean modifySourceByData(
        SID sid,
        String s1,
        String s2,
        String s3,
        Long l1,
        boolean isAssignL1,
        Long l2,
        boolean isAssignL2)
    {
        this.writeLock.lock();
        try {
            S s = this.getSource(sid);
            if (s == null)
                return false;

            if (s1 != null)
                s.s1 = s1;
            if (s2 != null)
                s.s2 = s2;
            if (s3 != null)
                s.s3 = s3;
            if (l1 != null)
                s.l1 = isAssignL1 ? l1 : (s.l1 + l1);
            if (l2 != null)
                s.l2 = isAssignL2 ? l2 : (s.l2 + l2);

            this.dirty = true;
            return true;
        }
        finally {
            this.writeLock.unlock();
        }
    }



    //
    // INTERNAL CLASSES
    //

    protected class TreeTable
        extends
            TreeMap<SID,TreeMap<DID,D>>
    {
        private static final long serialVersionUID =
            81227531317961313L;


        boolean containsColumn(
            SID sid,
            DID did)
        {
            return containsKey(sid) && get(sid).containsKey(did);
        }


        boolean containsRow(
            SID sid)
        {
            return containsKey(sid);
        }


        D get(
            SID sid,
            DID did)
        {
            Map<DID,D> rowMap = row(sid);
            if (rowMap != null)
            {
                return rowMap.get(did);
            }
            return null;
        }


        D put(
            SID sid,
            DID did,
            D e)
        {
            D previous = null;
            TreeMap<DID,D> m;
            if (containsKey(sid))
            {
                m = get(sid);
                previous = m.put(did, e);
            }
            else
            {
                m = Maps.newTreeMap();
                m.put(did, e);
                put(sid, m);
            }

            // might be null
            return previous;
        }


        D remove(
            SID sid,
            DID did)
        {
            if (!containsKey(sid))
            {
                return null;
            }

            return get(sid).remove(did);
        }


        Map<DID,D> row(
            SID sid)
        {
            return get(sid);
        }
    }
}
