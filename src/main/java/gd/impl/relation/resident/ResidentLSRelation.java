
package gd.impl.relation.resident;


import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;


public class ResidentLSRelation
    extends
        AbstractResidentRelation<Long,LEntry,String,SEntry>
    implements
        LSRelation
{
    //
    // LSRelation METHODS
    //

    @Override
    public boolean addEdge(
        Long sid,
        String did)
    {
        return super.addEdge(sid, new SEntry(did));
    }

    
    //
    // OVERRIDE METHODS
    //
    
    @Override
    protected SEntry makeEdge(
        String did)
    {
        return new SEntry(did);
    }
    
    @Override
    protected LEntry makeSource(
        Long sid)
    {
        return new LEntry(sid);
    }


    //
    // Relation METHODS
    //


    @Override
    public String makeEdgeId(
        String didStr)
    {
        return didStr;
    }


    @Override
    public Long makeSourceId(
        String sidStr)
    {
        return Long.valueOf(sidStr);
    }
}
