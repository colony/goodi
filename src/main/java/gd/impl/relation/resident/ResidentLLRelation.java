
package gd.impl.relation.resident;


import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;


public class ResidentLLRelation
    extends
        AbstractResidentRelation<Long,LEntry,Long,LEntry>
    implements
        LLRelation
{
    //
    // LLRelation METHODS
    //

    @Override
    public boolean addEdge(
        Long sid,
        Long did)
    {
        return super.addEdge(sid, new LEntry(did));
    }


    //
    // OVERRIDE METHODS
    //

    @Override
    protected LEntry makeEdge(
        Long did)
    {
        return new LEntry(did);
    }


    @Override
    protected LEntry makeSource(
        Long sid)
    {
        return new LEntry(sid);
    }
    
    
    //
    // Relation METHODS
    //


    @Override
    public Long makeEdgeId(
        String didStr)
    {
        return Long.valueOf(didStr);
    }


    @Override
    public Long makeSourceId(
        String sidStr)
    {
        return Long.valueOf(sidStr);
    }
}
