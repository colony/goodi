
package gd.impl.relation;




public final class Relations
{
    //
    // Relation SORT KEY
    //

    public static final String SK_DID = "did";

    public static final String SK_SID = "sid";


    //
    // Relation ANYTHING SID COLLECTION
    //

    /**
     * When a post is blocked, it will be assigned to group ID=2, we
     * need remember its previous group ID if case later on we need
     * unblock it and so we could assign its group back, also note,
     * it won't be saved into relation if its old group is 1L
     */
    public static final String ANYTHING_BLOCKED_PREVIOUS_GROUPID = "bpgid";

    /**
     * Relation ANYTHING is designed to provide auxiliary/ephemeral
     * data for any use case. As convention, the source ID should be
     * unique, hence, we collect all defined SID in one place.
     */

    /**
     * <ul>
     * <li>D.id - email verification code
     * <li>D.s1 - String form of EmailVerificationData (string)
     * </ul>
     */
    public static final String ANYTHING_EV = "ev";
    
    /**
     *  A featured post has a featured title associated with it. The
     * title might be a format string, for example, if the post is a
     * thread root, the title might contain number of thread
     * children (i.e. [x photos replied])
     * 
     * <ul>
     * <li>D.id - featured post ID 
     * <li>D.s1 - featured title
     * <li>D.l1 - 1 if D.s1 is a format string, using String.format
     * to resolve
     * </ul>
     */
    public static final String ANYTHING_FEATURED_FEED = "ff";

    /**
     * <ul>
     * <li>D.id - IndexType abbreviation
     * <li>D.s1 - last rebuild time (long)
     * </ul>
     */
    public static final String ANYTHING_IDX = "idx";


    //
    // Relation HAS_POST_METADATA DID COLLECTION
    //

    /**
     * Relation HAS_POST_METADATA is designed to store different
     * metadata for a post. The source ID is post ID and DID is a
     * string key for metadata item.
     */

    /**
     * Info related to activities on/about a post, the DID should be
     * HPM_ACTIVITY_INFO + ActivityType, for now, the metadata is
     * mainly used for suppression when generating activities for a
     * particular post
     */
    public static final String HPM_ACTIVITY_INFO = "ai.";

    /**
     * If a post is created in a private group, the post's geo info
     * won't be displayed, to achieve that, we override post's geo's
     * name with this field, and client will only display the covert
     * name
     * <ul>
     * <li>D.s1 - covert name
     * </ul>
     */
    public static final String HPM_COVERT_GEO = "cg";


    //
    // Relation HAS_USER_METADATA DID COLLECTION
    //

    /**
     * Relation HAS_USER_METADATA is designed to store different
     * metadata for a user. The source ID is user ID and DID is a
     * string key for metadata item
     */

    /**
     * User's activity feed
     * <ul>
     * <li>D.s1 - a serialized string form of entire feed for this
     * user, check {@ActivityFeeds} for detail
     * <li>D.l1 - number of activities in feed
     * </ul>
     */
    public static final String HUM_ACTIVITY_FEED = "af";

    /**
     * A list of unread activity IDs
     * <ul>
     * <li>D.l1 - num of IDs stored in D.s1
     * <li>D.s1 - A comma-separated list of activity IDs
     * </ul>
     */
    public static final String HUM_ACTIVITY_UNREAD_IDS = "aui";

    /**
     * Info related to user's announcements
     * <ul>
     * <li>D.l1 - the ID of announcement post that was most recently
     * read
     * </ul>
     */
    public static final String HUM_ANNOUNCEMENT_INFO = "ani";

    /**
     * Info related to user's points
     * <ul>
     * <li>D.l1 - current num of points
     * </ul>
     */
    public static final String HUM_POINT_INFO = "pi";



    private static RelationManager _relationMgr;


    public static <T> T get(
        RelationType id,
        Class<T> clazz)
    {
        return _relationMgr.get(id, clazz);
    }


    public static void init()
    {
        _relationMgr = RelationManager.instance();
    }
}
