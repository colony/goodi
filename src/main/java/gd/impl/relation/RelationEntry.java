
package gd.impl.relation;


public interface RelationEntry<ID>
{
    public ID getId();


    /**
     * Convert ID to string. This allows the entry be serialized on
     * network as well as storage (export/import) file
     */
    public String getIdStr();


    public void setIdStr(String s);


    /**
     * Indicate the kind of sort value and algorithm of sorting
     */
    public String getSortKey();


    /**
     * Sort value. The value is transferred to/from client to enable
     * sorting and pagination
     */
    public String getSortValue();
}
