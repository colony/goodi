
package gd.impl.relation;


import gd.api.v1.admin.ManageRelationResource;
import gd.impl.Rests;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SuppressWarnings({ "rawtypes", "unchecked" })
@Path("/adm/relations")
@Singleton
public class ManageRelationResourceImpl
    implements
        ManageRelationResource
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ManageRelationResourceImpl.class);

    protected RelationManager relationMgr;


    public ManageRelationResourceImpl()
    {
        this.relationMgr = RelationManager.instance();
    }


    @Override
    @Path("/{relation}/s/{sid}")
    @DELETE
    public Response deleteSource(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType type,
        @PathParam("sid") String sidStr)
    {
        Rests.beginAdminOperation(headers, "adm:rel:delS:" + type + ":" + sidStr);

        Relation rel = this.relationMgr.get(type, Relation.class);
        Object sid = rel.makeSourceId(sidStr);
        rel.deleteSource(sid);
        return Response.ok().build();
    }


    @Override
    @Path("/{relation}/s/{sid}/e/{did}")
    @DELETE
    public Response deleteEdge(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType type,
        @PathParam("sid") String sidStr,
        @PathParam("did") String didStr)
    {
        Rests.beginAdminOperation(headers, "adm:rel:delE:" + type + ":" + sidStr + ":" + didStr);

        Relation rel = this.relationMgr.get(type, Relation.class);
        Object sid = rel.makeSourceId(sidStr);
        Object did = rel.makeEdgeId(didStr);
        rel.deleteEdge(sid, did);
        return Response.ok().build();
    }


    @Override
    @Path("/export")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response exportRelation(
        @Context HttpHeaders headers,
        @FormParam("relation") RelationType r,
        @FormParam("fname") String fileName)
    {
        Rests.beginAdminOperation(headers, "adm:rel:export:" + r.name() + ":" + fileName);

        GDUtils.check(
            StringUtils.isNotEmpty(fileName),
            Status.BAD_REQUEST,
            "Empty not allowed for file_name");

        // Not all relation conforms to RelationAdmin interface
        RelationAdmin rel =
            this.relationMgr.get(r, RelationAdmin.class);
        try
        {
            rel.exportData(fileName);
        }
        catch (IOException e)
        {
            LOG.error(
                "Fail to export relation {} to file {}",
                r,
                fileName,
                e);
            throw new WebApplicationException(
                (Response.status(Status.INTERNAL_SERVER_ERROR)
                    .entity("Something bad happens")
                    .build()));
        }

        return Response.ok().build();
    }


    @Override
    @Path("/{relation}/s/{sid}/e/{did}")
    @GET
    @Produces("text/plain")
    public Response getEdge(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @PathParam("sid") String sidStr,
        @PathParam("did") String didStr)
    {
        Rests.beginAdminOperation(headers, "adm:rel:getEdge:" + r.name() + ":" + sidStr + ":" + didStr);

        Relation rel = this.relationMgr.get(r, Relation.class);
        Object sid = rel.makeSourceId(sidStr);
        Object did = rel.makeEdgeId(didStr);
        RelationEntry entry = rel.getEdge(sid, did);

        if (entry != null) {
            return Response.ok(rel.makeEdgeStr(entry)).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }


    @Override
    @Path("/{relation}/s/{sid}/e")
    @GET
    @Produces("text/plain")
    public Response getEdgeList(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @PathParam("sid") String sidStr,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:rel:getEdgeList:" + r.name() + ":" + sidStr);
        final Page p = pageParam.get();
        final Relation rel = this.relationMgr.get(r, Relation.class);
        Object sid = rel.makeSourceId(sidStr);
        Object boundary = p.boundary;
        final List<String> result = new ArrayList<>();
        if (boundary != null)
            boundary = rel.makeEdgeId(p.boundary);
        rel.visitEdges(
            sid, 
            StringUtils.defaultIfEmpty(p.key, Relations.SK_DID),
            boundary,
            p.isDescending(),
            p.exclusive,
            new Visitor<RelationEntry<?>>(){
                @Override
                public boolean visit(
                    RelationEntry<?> item)
                {
                    result.add(rel.makeEdgeStr(item));
                    
                    if (result.size() >= p.size)
                        return false;
                    
                    return true;
                }
            });
        
        if (result.isEmpty()) {
            return Response.status(Status.NOT_FOUND).build();
        }
        
        return Response.ok(StringUtils.join(result, "\n")).build();
    }


    @Override
    @Path("/{relation}/s/{sid}")
    @GET
    @Produces("text/plain")
    public Response getSource(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @PathParam("sid") String sidStr)
    {
        Rests.beginAdminOperation(headers, "adm:rel:getSrc:" + r.name() + ":" + sidStr);

        Relation rel = this.relationMgr.get(r, Relation.class);
        Object sid = rel.makeSourceId(sidStr);
        RelationEntry entry = rel.getSource(sid);
        
        if (entry != null) {
            return Response.ok(rel.makeSourceStr(entry)).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }


    @Override
    @Path("/{relation}/s")
    @GET
    @Produces("text/plain")
    public Response getSourceList(
        @Context HttpHeaders headers,
        @PathParam("relation") RelationType r,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:rel:getSrcList:" + r.name());
        final Page p = pageParam.get();
        final Relation rel = this.relationMgr.get(r, Relation.class);
        Object boundary = p.boundary;
        final List<String> result = new ArrayList<>();
        if (boundary != null)
            boundary = rel.makeEdgeId(p.boundary);
        rel.visitSources(
            StringUtils.defaultIfEmpty(p.key, Relations.SK_SID),
            boundary,
            p.isDescending(),
            p.exclusive,
            new Visitor<RelationEntry<?>>(){
                @Override
                public boolean visit(
                    RelationEntry<?> item)
                {
                    result.add(rel.makeEdgeStr(item));
                    
                    if (result.size() >= p.size)
                        return false;
                    
                    return true;
                }
            });
        
        if (result.isEmpty()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok(StringUtils.join(result, "\n")).build();
    }


    @Override
    @Path("/import")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response importRelation(
        @Context HttpHeaders headers,
        @FormParam("relation") RelationType r,
        @FormParam("fname") String fileName)
    {
        Rests.beginAdminOperation(headers, "adm:rel:import:" + r.name() + ":" + fileName);

        GDUtils.check(
            StringUtils.isNotEmpty(fileName),
            Status.BAD_REQUEST,
            "Empty not allowed for file_name");

        RelationAdmin rel =
            this.relationMgr.get(r, RelationAdmin.class);
        try
        {
            rel.importData(fileName);
        }
        catch (IOException e)
        {
            LOG.error(
                "Fail to import relation {} to file {}",
                r,
                fileName,
                e);
            throw new WebApplicationException(
                (Response.status(Status.INTERNAL_SERVER_ERROR)
                    .entity("Something bad happens")
                    .build()));
        }

        return Response.ok().build();
    }
}
