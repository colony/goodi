
package gd.impl.relation;



import gd.support.protobuf.RelationProtos.RelationEdgeEntryProto;
import gd.support.protobuf.RelationProtos.RelationSourceEntryProto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.lang3.Conversion;


public class RelationExporter
{
    //
    // CONSTANTS
    //

    public static final int HEADER_MAGIC = 0x19890828;

    public static final int HEADER_VERSION = 1;


    protected int version;


    public RelationExporter()
    {
        this.version = HEADER_VERSION;
    }


    /**
     * Reads the standard header from input stream. Internally, it
     * will set the current version to the read one.
     */
    public int readHeader(
        InputStream in)
        throws IOException
    {
        byte[] b = new byte[4];
        in.read(b);
        int magic = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (magic != HEADER_MAGIC)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid magic number 0x%08x, expected 0x%08x",
                    magic,
                    HEADER_MAGIC));
        }

        b = new byte[4];
        in.read(b);
        int version = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (version <= 0 || version > HEADER_VERSION)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid version number %d, expected %d",
                    version,
                    HEADER_VERSION));
        }

        // Set the current version
        this.version = version;
        return version;
    }


    public RelationExporterEdgeEntry readEdge(
        InputStream in)
        throws IOException
    {
        // Proceed according to our version
        switch (this.version)
        {
        case 1:
            return readEdgeV1(in);

        default:
            throw new IllegalStateException("Unrecognized version " + this.version);
        }
    }


    public RelationExporterSourceEntry readSource(
        InputStream in)
        throws IOException
    {
        // Proceed according to our version
        switch (this.version)
        {
        case 1:
            return readSourceV1(in);

        default:
            throw new IllegalStateException("Unrecognized version " + this.version);
        }
    }


    /**
     * Write the current version specified by HEADER_VERSION
     */
    public void writeHeader(
        OutputStream out)
        throws IOException
    {
        out.write(Conversion.intToByteArray(
            HEADER_MAGIC,
            0,
            new byte[4],
            0,
            4));
        out.write(Conversion.intToByteArray(
            HEADER_VERSION,
            0,
            new byte[4],
            0,
            4));
    }


    public void writeEdge(
        OutputStream out,
        RelationExporterEdgeEntry entry)
        throws IOException
    {
        RelationEdgeEntryProto.Builder b = RelationEdgeEntryProto.newBuilder();
        b.setId(entry.id);
        b.setToken(entry.token);
        b.setSourceId(entry.sourceId);
        if (null != entry.s1)
            b.setS1(entry.s1);
        if (null != entry.s2)
            b.setS2(entry.s2);
        if (null != entry.s3)
            b.setS3(entry.s3);
        b.setL1(entry.l1);
        b.setL2(entry.l2);

        b.build().writeDelimitedTo(out);
    }


    public void writeSource(
        OutputStream out,
        RelationExporterSourceEntry entry)
        throws IOException
    {
        RelationSourceEntryProto.Builder b = RelationSourceEntryProto.newBuilder();
        b.setId(entry.id);
        b.setToken(entry.token);
        b.setNumEdges(entry.numEdges);
        if (null != entry.s1)
            b.setS1(entry.s1);
        if (null != entry.s2)
            b.setS2(entry.s2);
        if (null != entry.s3)
            b.setS3(entry.s3);
        b.setL1(entry.l1);
        b.setL2(entry.l2);

        b.build().writeDelimitedTo(out);
    }


    //
    // INTERNAL METHODS
    //

    private RelationExporterEdgeEntry readEdgeV1(
        InputStream in)
        throws IOException
    {
        RelationEdgeEntryProto.Builder b = RelationEdgeEntryProto.newBuilder();
        boolean success = b.mergeDelimitedFrom(in);
        if (success) {
            RelationEdgeEntryProto p = b.build();
            RelationExporterEdgeEntry e = new RelationExporterEdgeEntry();
            e.id = p.getId();
            e.token = p.getToken();
            e.sourceId = p.getSourceId();
            e.s1 = p.getS1();
            e.s2 = p.getS2();
            e.s3 = p.getS3();
            e.l1 = p.getL1();
            e.l2 = p.getL2();
            return e;
        }
        return null;
    }


    private RelationExporterSourceEntry readSourceV1(
        InputStream in)
        throws IOException
    {
        RelationSourceEntryProto.Builder b = RelationSourceEntryProto.newBuilder();
        boolean success = b.mergeDelimitedFrom(in);
        if (success) {
            RelationSourceEntryProto p = b.build();
            RelationExporterSourceEntry e = new RelationExporterSourceEntry();
            e.id = p.getId();
            e.token = p.getToken();
            e.numEdges = p.getNumEdges();
            e.s1 = p.getS1();
            e.s2 = p.getS2();
            e.s3 = p.getS3();
            e.l1 = p.getL1();
            e.l2 = p.getL2();
            return e;
        }
        return null;
    }



    //
    // INTERNAL CLASSES
    //


    public static class RelationExporterEdgeEntry
    {
        public String id;

        public long token;

        public String sourceId;

        public String s1;

        public String s2;

        public String s3;

        public long l1;

        public long l2;
    }



    public static class RelationExporterSourceEntry
    {
        public String id;

        public long token;

        public long numEdges;

        public String s1;

        public String s2;

        public String s3;

        public long l1;

        public long l2;
    }
}
