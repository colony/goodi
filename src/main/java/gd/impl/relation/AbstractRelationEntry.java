
package gd.impl.relation;


import org.json.JSONObject;

import gd.impl.util.Stringifiable;

import com.google.common.base.Strings;


public abstract class AbstractRelationEntry<ID>
    implements
        RelationEntry<ID>,
        Stringifiable<AbstractRelationEntry<ID>>
{
    protected ID id;

    /**
     * SortKey and sortVal are NOT persisted. They are normally null
     * unless the relationValue is retrieved as a collection (i.e.
     * visitEdge), then both fields are set to provide the sorting
     * info
     */
    protected String sortKey;

    protected String sortVal;


    /**
     * The update token that stores the time of last update, in
     * milliseconds since the epoch. Also, in practical situation,
     * the field is an optimistic locking version field that used to
     * detect update conflict (locking contention) issue
     */
    protected long token;


    /**
     * A fixed number of public strings are used as the basic
     * storage unit to contain any additional data associated with
     * the entry. The underlying storage might NOT persist the
     * string if it is null or empty
     */
    public String s1;

    public String s2;

    public String s3;


    /**
     * A fixed number of public longs are used as the basic storage
     * unit to contain any additional NUMERIC data associated with
     * the entry
     */
    public long l1;

    public long l2;


    public AbstractRelationEntry(
        ID id)
    {
        this.id = id;
        this.refreshToken();
    }


    public void refreshToken()
    {
        this.token = System.currentTimeMillis();
    }



    //
    // Stringifiable INTERFACE
    //
    
    @Override
    public AbstractRelationEntry<ID> fromStr(
        String s)
    {
        // For now, i only use toStr() to convert the entry to string
        // for frontend API (i.e. admin console), there's no need to
        // convert it back from string
        return null;
    }

    /**
     * Conform to Stringifiable interface. Convert the relation
     * entry to a string representation. NOTE, the implementation
     * will use toString() method to convert ID to string, children
     * class should override this method if need special handling
     */
    @Override
    public String toStr()
    {
        JSONObject json = new JSONObject();
        json.putOpt("id", id.toString());
        json.putOpt("token", token);
        json.putOpt("s1", s1);
        json.putOpt("s2", s2);
        json.putOpt("s3", s3);
        json.putOpt("l1", l1);
        json.putOpt("l2", l2);
        return json.toString();
    }


    //
    // RelationEntry INTERFACE
    //

    public ID getId()
    {
        return id;
    }


    public String getSortKey()
    {
        return sortKey;
    }


    public String getSortValue()
    {
        return sortVal;
    }


    //
    // CONVENIENT METHODS TO ACCESS STORAGE UNIT PER SCALAR TYPES
    //

    public String s(
        int i)
    {
        switch (i)
        {
        case 1:
            return s1;
        case 2:
            return s2;
        case 3:
            return s3;
        default:
            return null;
        }
    }


    public boolean sb(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return false;
        return Boolean.parseBoolean(s(i));
    }


    public int si(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return 0;
        return Integer.parseInt(s(i));
    }


    public double sd(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return 0;
        return Double.parseDouble(s(i));
    }


    public long sl(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return 0L;
        return Long.parseLong(s(i));
    }


    //
    // GETTERS & SETTERS
    //


    public long getToken()
    {
        return token;
    }


    /**
     * CAUTION, set token might disrupt the optimistic lock
     * mechanism
     */
    public void setToken(
        long token)
    {
        this.token = token;
    }


    public void setSortKey(
        String sk)
    {
        this.sortKey = sk;
    }


    public void setSortValue(
        String sv)
    {
        this.sortVal = sv;
    }
}
