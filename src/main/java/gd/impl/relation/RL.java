
package gd.impl.relation;


/**
 * Group of Relation interface and related class
 */
public final class RL
{
    public static interface LLRelation
        extends
            Relation<Long,LEntry,Long,LEntry>,
            RelationExtension2<Long,LEntry,Long,LEntry>
    {
        // Simple interface
        public boolean addEdge(
            Long sid,
            Long did);
    }



    public static interface LSRelation
        extends
            Relation<Long,LEntry,String,SEntry>,
            RelationExtension2<Long,LEntry,String,SEntry>
    {
        // Simple interface
        public boolean addEdge(
            Long sid,
            String did);
    }



    public static interface SLRelation
        extends
            Relation<String,SEntry,Long,LEntry>,
            RelationExtension2<String,SEntry,Long,LEntry>
    {
        // Simple interface
        public boolean addEdge(
            String sid,
            Long did);
    }



    public static interface SSRelation
        extends
            Relation<String,SEntry,String,SEntry>,
            RelationExtension2<String,SEntry,String,SEntry>
    {
        // Simple interface
        public boolean addEdge(
            String sid,
            String did);
    }



    public static class LEntry
        extends
            AbstractRelationEntry<Long>
    {
        public LEntry(
            Long id)
        {
            super(id);
        }


        /**
         * Copy constructor
         */
        public LEntry(
            LEntry e)
        {
            super(e.id);
            this.s1 = e.s1;
            this.s2 = e.s2;
            this.s3 = e.s3;
            this.l1 = e.l1;
            this.l2 = e.l2;
            this.sortKey = e.sortKey;
            this.sortVal = e.sortVal;
            this.token = e.token;
        }


        //
        // BUILDER
        //

        public LEntry with(
            String... stringStorageUnits)
        {
            int len = stringStorageUnits.length;
            if (len > 0)
                this.s1 = stringStorageUnits[0];
            if (len > 1)
                this.s2 = stringStorageUnits[1];
            if (len > 2)
                this.s3 = stringStorageUnits[2];
            return this;
        }


        public LEntry with(
            long... longStorageUnits)
        {
            int len = longStorageUnits.length;
            if (len > 0)
                this.l1 = longStorageUnits[0];
            if (len > 1)
                this.l2 = longStorageUnits[1];
            return this;
        }


        //
        // OVERRIDE METHODS
        //

        @Override
        public String getIdStr()
        {
            /**
             * format string to 19 digit string in order to support
             * sqlite order by
             */
            return String.format("%019d", (id));
        }


        @Override
        public void setIdStr(
            String s)
        {
            this.id = Long.valueOf(s);
        }
    }



    public static class SEntry
        extends
            AbstractRelationEntry<String>
    {
        public SEntry(
            String id)
        {
            super(id);
        }


        /**
         * Copy constructor
         */
        public SEntry(
            SEntry e)
        {
            super(e.id);
            this.s1 = e.s1;
            this.s2 = e.s2;
            this.s3 = e.s3;
            this.l1 = e.l1;
            this.l2 = e.l2;
            this.sortKey = e.sortKey;
            this.sortVal = e.sortVal;
            this.token = e.token;
        }


        //
        // BUILDER
        //

        public SEntry with(
            String... stringStorageUnits)
        {
            int len = stringStorageUnits.length;
            if (len > 0)
                this.s1 = stringStorageUnits[0];
            if (len > 1)
                this.s2 = stringStorageUnits[1];
            if (len > 2)
                this.s3 = stringStorageUnits[2];
            return this;
        }


        public SEntry with(
            long... longStorageUnits)
        {
            int len = longStorageUnits.length;
            if (len > 0)
                this.l1 = longStorageUnits[0];
            if (len > 1)
                this.l2 = longStorageUnits[1];
            return this;
        }


        //
        // OVERRIDE METHODS
        //

        @Override
        public String getIdStr()
        {
            return id;
        }


        @Override
        public void setIdStr(
            String s)
        {
            this.id = s;
        }
    }
}
