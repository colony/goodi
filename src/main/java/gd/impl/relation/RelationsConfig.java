
package gd.impl.relation;


import gd.impl.config.Configured;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import com.google.common.collect.Lists;


//Not reloadable
@Configured(fname = "relation.yml")
public class RelationsConfig
{

    private EnumMap<RelationType,RelationConfig> relations;


    /**
     * Default constructor
     */
    public RelationsConfig()
    {
    }


    public RelationConfig get(
        RelationType id)
    {
        if (this.relations == null
            || !this.relations.containsKey(id))
        {
            // Return a default one when miss
            return new RelationConfig(id);
        }
        return this.relations.get(id);
    }


    public List<RelationConfig> getRelations()
    {
        if (this.relations == null)
        {
            return Lists.newArrayList();
        }

        return new ArrayList<>(this.relations.values());
    }


    @SuppressWarnings("unused")
    private void setRelations(
        List<RelationConfig> relations)
    {
        this.relations = new EnumMap<>(RelationType.class);
        for (RelationConfig cfg : relations)
        {
            this.relations.put(cfg.getId(), cfg);
        }

        // Create default configuration for missing one
        for (RelationType id : RelationType.values())
        {
            if (!this.relations.containsKey(id))
            {
                this.relations.put(id, new RelationConfig(id));
            }
        }
    }
}
