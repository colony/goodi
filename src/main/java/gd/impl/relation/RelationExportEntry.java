package gd.impl.relation;

import java.util.Collection;

public class RelationExportEntry
{
    public AbstractRelationEntry<?> source;
    
    public Collection<AbstractRelationEntry<?>> edges;
    
    public RelationExportEntry()
    {
    }
    
    public boolean hasEdge()
    {
        return edges != null && !edges.isEmpty();
    }
}
