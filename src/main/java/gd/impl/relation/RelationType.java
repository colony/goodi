
package gd.impl.relation;


import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SLRelation;
import gd.impl.relation.RL.SSRelation;
import gd.impl.relation.resident.ResidentLLRelation;
import gd.impl.relation.resident.ResidentLSRelation;
import gd.impl.relation.resident.ResidentSLRelation;
import gd.impl.relation.resident.ResidentSSRelation;



public enum RelationType
{
    ANYTHING(
        "a",
        ResidentSSRelation.class,
        SSRelation.class),

    /**
     * A user (S) creates a post (D)
     */
//    CREATES_POST(
//        "cp",
//        ResidentLLRelation.class,
//        LLRelation.class),

    /**
     * A user (S) flags a post (D)
     */
    FLAGS(
        "f",
        ResidentLLRelation.class,
        LLRelation.class),


    /**
     * A post (S)'s thread has commenter (D)
     * 
     * <pre>
     * S.id: ID of post 
     * D.id: ID of commenter
     * D.l1: position of the commenter in thread, start with 1 
     * D.s1: serialized string of IconMedia
     * </pre>
     */
    HAS_COMMENTER(
        "hc",
        ResidentLLRelation.class,
        LLRelation.class),

    /**
     * A post (S) has metadata (D). The ID of each dest node is a
     * key for metadata item, please check Relations for detail
     */
    HAS_POST_METADATA(
        "hpm",
        ResidentLSRelation.class,
        LSRelation.class),

    /**
     * A thread (S) has children posts (D)
     * 
     * <pre>
     * S.id: ID of thread
     * S.l1: ID of root post of the thread
     * S.l2: Owner ID of root post
     * S.s1: thread type name
     * S.s2: thread metadata
     * D.id: ID of post
     * </pre>
     */
    HAS_THREAD_CHILDREN(
        "htc",
        ResidentLLRelation.class,
        LLRelation.class),

    /**
     * A thread (S) has participants (D), a participant is who
     * create post in the thread
     * 
     * <pre>
     * S.id: ID of thread
     * D.id: ID of participant
     * </pre>
     */
    HAS_THREAD_PARTICIPANT(
        "htp",
        ResidentLLRelation.class,
        LLRelation.class),

    /**
     * A user (S) has metadata (D). The ID of each dest node is a
     * key for metadata item, please check Relations for detail
     */
    HAS_USER_METADATA(
        "hum",
        ResidentLSRelation.class,
        LSRelation.class),

    /**
     * A hashtag (S) has been attached on a post (D)
     */
    HASHTAG(
        "ht",
        ResidentSLRelation.class,
        SLRelation.class),

    /**
     * For each special PostStatus.Status (S), post (D) has that
     * status. NOT all status are included.
     * 
     * <pre>
     * BLOCKED          Y
     * ONE_O_ONE        N, deprecate
     * FAKE             N, too much fake content, doesn't gain much to track them here
     * HIDDEN           Y
     * GEO_RESTRICTED   Y
     * ANNOUNCEMENT     Y
     * </pre>
     */
    INCLUDES_SPECIAL_POST(
        "isp",
        ResidentSLRelation.class,
        SLRelation.class),

    /**
     * For each special UserStatus.Status (S), user (D) has that
     * status
     */
    INCLUDES_SPECIAL_USER(
        "isu",
        ResidentSLRelation.class,
        SLRelation.class),

    /**
     * A comment is voted by a user
     */
    IS_COMMENT_VOTED_BY(
        "icvb",
        ResidentLLRelation.class,
        LLRelation.class),

    /**
     * A post is voted by a user
     */
    IS_VOTED_BY(
        "ivb",
        ResidentLLRelation.class,
        LLRelation.class),

    /**
     * A user votes a post
     * 
     * <pre>
     * S.id: ID of user 
     * D.id: ID of post
     * D.l1: VoteStatus.ordinal
     * </pre>
     */
    VOTES(
        "v",
        ResidentLLRelation.class,
        LLRelation.class),

    /**
     * A user votes a comment
     * 
     * <pre>
     * S.id: ID of user
     * D.id: ID of comment
     * D.l1: VoteStatus.ordinal
     * </pre>
     */
    VOTES_COMMENT(
        "vc",
        ResidentLLRelation.class,
        LLRelation.class),

    ;



    private String abbrev;

    private Class<? extends Relation<?,?,?,?>> dfltImplClass;

    /**
     * The specialized and REQUIRED interface (i.e. some extension
     * of {@link Relation} used to implement this relation
     */
    private Class<? extends Relation<?,?,?,?>> specializedInterface;


    private RelationType(
        String abbrev,
        Class<? extends Relation<?,?,?,?>> dlftImplClass,
        Class<? extends Relation<?,?,?,?>> spInterface)
    {
        this.abbrev = abbrev;
        this.dfltImplClass = dlftImplClass;
        this.specializedInterface = spInterface;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public Class<? extends Relation<?,?,?,?>> defaultImplClass()
    {
        return this.dfltImplClass;
    }


    public Class<? extends Relation<?,?,?,?>> specializedInterface()
    {
        return this.specializedInterface;
    }


    public static RelationType fromAbbreviation(
        String abbrev)
    {
        for (RelationType r : RelationType.values())
        {
            if (r.abbrev.equals(abbrev))
            {
                return r;
            }
        }

        throw new IllegalArgumentException(
            "Unrecognized Relations abbreviation \"" + abbrev
                + "\"");
    }
}
