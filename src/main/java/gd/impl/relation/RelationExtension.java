
package gd.impl.relation;


import gd.impl.util.Visitor;


public interface RelationExtension<SID,SRC extends RelationEntry<SID>,DID,DST extends RelationEntry<DID>>
    extends
        Relation<SID,SRC,DID,DST>
{
    //TODO: Define query and update statement format
    
    
    public boolean hasEdgeByQuery(
        String query,
        Object... params);


    public boolean hasSourceByQuery(
        String query,
        Object... params);


    public boolean modifyEdgeByUpdate(
        SID sid,
        DID did,
        String update,
        Object... params);


    public boolean modifySourceByUpdate(
        SID sid,
        String update,
        Object... params);


    public void visitEdgesByQuery(
        String query,
        Object[] params,
        boolean descending,
        Visitor<DST> visitor);


    public void visitSourcesByQuery(
        String query,
        Object[] params,
        boolean descending,
        Visitor<SRC> visitor);
}
