
package gd.impl.relation;


import java.io.IOException;


public interface RelationAdmin
{
    public void exportData(
        String fname)
        throws IOException;


    public void importData(
        String fname)
        throws IOException;
}
