
package gd.impl.relation;


import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;


/**
 * A Relation represents an unidirectional relationship between two
 * entities, source node and destination node. Each source node
 * could point to one or many destination nodes. Each source node
 * contains id (SID) and data (SRC), while each destination node
 * contains same (DID, DST).
 */
public interface Relation<SID,SRC extends RelationEntry<SID>,DID,DST extends RelationEntry<DID>>
{
    /**
     * Add a new edge. If the source of specified SID doesn't exist,
     * a simple source (only SID) is added also
     * 
     * @return true if the edge is added, false if duplicate
     *         destination with same DID already exist
     */
    public boolean addEdge(
        SID sid,
        DST d);


    /**
     * Add a new source
     * 
     * @return true if the source is added, false if duplicate
     *         source with same SID already exist
     */
    public boolean addSource(
        SRC s);


    /**
     * Delete an edge with specified SID and DID
     * 
     * @return The destination node that was deleted, or null if the
     *         edge with SID and DID doesn't exist
     */
    public boolean deleteEdge(
        SID sid,
        DID did);


    /**
     * Delete the source with specified SID and all edges connect to
     * the SID
     * 
     * @return True if the source node that was deleted, or false if
     *         the source doesn't exist (NOTE, edge can still
     *         connect to the SID without source NODE exist)
     */
    public boolean deleteSource(
        SID sid);


    /**
     * Return the destination node in the edge of specified SID and
     * DID
     * 
     * @return The destination node, or null if the edge doesn't
     *         exist
     */
    public DST getEdge(
        SID sid,
        DID did);


    /**
     * Return number of edges for the specified SID
     * 
     * @return The number, or -1 if the source of specified SID
     *         doesn't exist
     */
    public long getNumEdges(
        SID sid);


    /**
     * Return number of sources
     */
    public long getNumSources();


    /**
     * Return the source node of specified SID
     * 
     * @return The source node, or null if it doesn't exist
     */
    public SRC getSource(
        SID sid);


    /**
     * Checks if the edge of specified SID and DID exist
     */
    public boolean hasEdge(
        SID sid,
        DID did);


    /**
     * Checks if the source of specified SID exist
     */
    public boolean hasSource(
        SID sid);


    public void init(
        RelationConfig config);


    /**
     * Unconditionally add an edge. If the edge already exists, it
     * will be replaced/updated. If the entry doesn't exist, it will
     * be added.
     * <p>
     * Note that the method bypasses the update conflict mechanism.
     * The result maybe inconsistent under concurrent environment
     */
    public void putEdge(
        SID sid,
        DST d);


    /**
     * Unconditionally add a source. If the source already exists,
     * it will replaced/updated. If the source doesn't exist, it
     * will be added.
     * <p>
     * Note that the method bypasses the update conflict mechanism.
     * The result maybe inconsistent under concurrent environment
     */
    public void putSource(
        SRC s);


    /**
     * Updates an edge with the specified source ID and destination
     * node. The method will throw update conflict if the existing
     * token of dest mismatches the one passed in
     * 
     * @return true if update operation succeed, false if the source
     *         or destination node doesn't exist
     * @throws UpdateConflictException
     */
    public boolean updateEdge(
        SID sid,
        DST d)
        throws UpdateConflictException;


    /**
     * Updates a source. The method will throw update conflict if
     * the existing token of source mismatches the one passed in
     * 
     * @return true if update operation succeed, false if the source
     *         doesn't exist
     * @throws UpdateConflictException
     */
    public boolean updateSource(
        SRC s)
        throws UpdateConflictException;


    /**
     * Visit all edges of specified SID
     * 
     * @param sid
     *            ID of the source whose edges are being visited
     * @param visitor
     */
    public void visitAllEdges(
        SID sid,
        Visitor<DST> visitor);


    /**
     * Visit all sources of the relation
     * 
     * @param visitor
     */
    public void visitAllSources(
        Visitor<SRC> visitor);


    /**
     * Paginate and visit the edges of specified SID with a
     * particular sorting order specified by sortKey
     * 
     * @param sid
     *            ID of the source whose edges are being visited
     * @param sortKey
     *            The name of the sorting algorithm
     * @param boundary
     *            The boundary, or sort value according to sortKey,
     *            if not null, the pagination starts with the
     *            boundary
     * @param descending
     * @param exclusive
     * @param visitor
     */
    public void visitEdges(
        SID sid,
        String sortKey,
        Object boundary,
        boolean descending,
        boolean exclusive,
        Visitor<DST> visitor);


    /**
     * Paginate and visit sources with a particular sorting order
     * specified by sortKey
     * 
     * @param sortKey
     *            The name of the sorting algorithm
     * @param boundary
     *            The boundary, or sort value according to sortKey,
     *            if not null, the pagination starts with the
     *            boundary
     * @param descending
     * @param exclusive
     * @param visitor
     */
    public void visitSources(
        String sortKey,
        Object boundary,
        boolean descending,
        boolean exclusive,
        Visitor<SRC> visitor);


    //
    // Easy interface to convert from/to string representation
    //

    /**
     * Convert the ID of a destination node from a string
     */
    public DID makeEdgeId(
        String didStr);


    /**
     * Convert the ID of a destination node to string
     */
    public String makeEdgeIdStr(
        DID did);


    /**
     * Convert a destination node to string representation
     */
    public String makeEdgeStr(
        DST dst);


    /**
     * Convert the ID of a source node from a string
     */
    public SID makeSourceId(
        String sidStr);


    /**
     * Convert the ID of a source node to string
     */
    public String makeSourceIdStr(
        SID sid);


    /**
     * Convert a source node to string representation
     */
    public String makeSourceStr(
        SRC src);

}
