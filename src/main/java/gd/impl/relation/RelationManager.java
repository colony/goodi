
package gd.impl.relation;


import gd.impl.config.ConfigManager;
import gd.impl.config.GlobalProperties;
import gd.impl.relation.resident.ResidentRelation;
import gd.impl.relation.sqlite.SqliteRelation;
import gd.impl.util.InitializationError;
import gd.impl.util.Managed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RelationManager
    implements
        Managed
{
    private static final long DFLT_FLUSHER_INTERVAL = 300 * 1000; //ms, 5 minute

    private static final long DFLT_INITIAL_FLUSHER_DELAY = 120 * 1000; //ms, 2 minute

    private static final Logger LOG =
        LoggerFactory.getLogger(RelationManager.class);

    /**
     * Each relation local directory consists of a store file, which
     * holds the data flushed out
     */
    private static final String STORE_FILE = "_store";

    private ScheduledExecutorService flushThread;

    protected EnumMap<RelationType,Relation<?,?,?,?>> relations;

    /**
     * True if stop has been called, false otherwise
     */
    protected volatile boolean stopping = false;


    private RelationManager()
    {
        LOG.info("Start initialize relations");

        // Start with empty map
        this.relations = new EnumMap<>(RelationType.class);

        RelationsConfig rlCfg =
            ConfigManager.instance().get(RelationsConfig.class);
        if (rlCfg == null)
        {
            throw new InitializationError(
                "Cannot find relation configuration");
        }

        int numLoaded = 0;
        for (RelationType id : RelationType.values())
        {
            Relation<?,?,?,?> rel =
                initRelation(rlCfg.get(id));
            this.relations.put(id, rel);
            ++numLoaded;
        }

        LOG.info("Complete initialize {} relations", numLoaded);
    }


    public void flush()
        throws IOException
    {
        if (this.stopping) {
            LOG.warn("Abort flush() due to stopping");
            return;
        }

        int numFlushed = 0;
        for (RelationType id : RelationType.values()) {
            if (this.flushRelation(id))
                ++numFlushed;
        }

        LOG.info("Flushed {} relations to file", numFlushed);
    }


    /**
     * Flush the cache by the specified type
     * 
     * @return true if the relation was flushed, false otherwise
     */
    public boolean flushRelation(
        RelationType id)
        throws IOException
    {
        if (this.stopping) {
            LOG.warn("Abort flushRelation {} due to stopping", id);
            return false;
        }

        return this.flushRelationInternal(id);
    }


    public <T> T get(
        RelationType id,
        Class<T> clazz)
    {
        T result;

        Relation<?,?,?,?> obj = this.relations.get(id);
        if (obj == null)
        {
            throw new RuntimeException(
                "Relation \"" + id + "\" doesn't exist");
        }
        else
        {
            result = clazz.cast(obj);
        }

        return result;
    }


    //
    // INTERNAL METHODS
    //

    @SuppressWarnings("rawtypes")
    private boolean flushRelationInternal(
        RelationType id)
        throws IOException
    {
        Relation rel = this.get(id, Relation.class);
        // Relation must conform to RelationAdmin interface to support serialization
        // And ONLY ResidentRelation is flushed
        if (rel instanceof RelationAdmin) {
            RelationAdmin ra = (RelationAdmin) rel;
            if (ra instanceof ResidentRelation) {
                ResidentRelation rr = (ResidentRelation) ra;
                if (rr.isDirty()) {
                    ra.exportData(this.getExportFile(id));
                    return true;
                }
            }
        }
        return false;
    }


    private void flushInternal()
        throws IOException
    {
        int numFlushed = 0;
        for (RelationType id : RelationType.values()) {
            if (flushRelationInternal(id))
                ++numFlushed;
        }
        LOG.info("Flushed {} relations to file", numFlushed);
    }


    private String getExportFile(
        RelationType id)
    {
        GlobalProperties gp = GlobalProperties.instance();
        return gp.relationDir()
            .resolve(id.abbreviation())
            .resolve(STORE_FILE)
            .toString();
    }


    private Relation<?,?,?,?> initRelation(
        RelationConfig cfg)
    {
        Relation<?,?,?,?> relation = null;

        // Initialize based on relation type
        if (ResidentRelation.class.isAssignableFrom(cfg.implClass()))
        {
            try
            {
                Relation<?,?,?,?> implObj =
                    cfg.implClass().newInstance();
                implObj.init(cfg);

                if (implObj instanceof RelationAdmin) {
                    // Resident relation initialized from local store file if any
                    String file = this.getExportFile(cfg.getId());
                    Path path = Paths.get(file);
                    if (!Files.notExists(path)) {
                        LOG.info("Importing relation {}", cfg.getId());
                        ((RelationAdmin) implObj).importData(file);
                    }
                }

                relation = implObj;
            }
            catch (Exception e)
            {
                throw new InitializationError(
                    "Fail to create instance for relation "
                        + cfg.getId(),
                    e);
            }
        }
        else if (SqliteRelation.class.isAssignableFrom(cfg.implClass()))
        {
            try
            {
                Relation<?,?,?,?> implObj =
                    cfg.implClass().newInstance();
                implObj.init(cfg);
                relation = implObj;
            }
            catch (Exception e)
            {
                throw new InitializationError(
                    "Fail to create instance for relation "
                        + cfg.getId(),
                    e);
            }
        }
        // Other relation type goes here

        return relation;
    }



    //
    // Managed
    //

    @Override
    public void start()
    {
        LOG.info("RelationManager starting...");
        
        // Spin up flusher
        this.flushThread = Executors.newScheduledThreadPool(1);
        this.flushThread.scheduleWithFixedDelay(
            new Flusher(),
            DFLT_INITIAL_FLUSHER_DELAY,
            DFLT_FLUSHER_INTERVAL,
            TimeUnit.MILLISECONDS);
        this.stopping = false;
        
        LOG.info("RelationManager started");
    }


    @Override
    public void stop()
    {
        LOG.info("RelationManager stopping...");

        // A gate to prevent anything else from happening while in the middle of stopping
        this.stopping = true;

        // Shutdown flusher
        LOG.info("Waiting for Relation Flusher to terminate");
        this.flushThread.shutdown();
        try {
            // Wait until flusher done
            while (!this.flushThread.awaitTermination(10, TimeUnit.SECONDS))
                ;
        }
        catch (InterruptedException e) {
            LOG.warn("Interrupted while waiting for flusher to finish");
        }
        LOG.info("Relation Flusher has terminated");

        // One last flush for good
        try {
            this.flushInternal();
        }
        catch (IOException e) {
            LOG.warn("Fail to do final flush", e);
        }

        LOG.info("RelationManager stopped");
    }



    //
    // SINGLETON MANAGEMENT
    //


    private static class SingletonHolder
    {
        private static final RelationManager INSTANCE =
            new RelationManager();
    }


    public static RelationManager instance()
    {
        return SingletonHolder.INSTANCE;
    }



    @SuppressWarnings("rawtypes")
    private class Flusher
        implements
            Runnable
    {
        @Override
        public void run()
        {
            int numFlushed = 0;
            for (RelationType type : RelationType.values()) {
                Relation rel = get(type, Relation.class);

                // The flusher will only export ResidentRelation periodically
                if (rel instanceof ResidentRelation) {
                    try {
                        if (flushRelation(type))
                            ++numFlushed;
                    }
                    catch (Throwable e) {
                        LOG.error("Fail to flush relation {}", type.name(), e);
                    }
                }
            }
            LOG.info("Flusher flushed {} relations to file", numFlushed);
        }
    }

}
