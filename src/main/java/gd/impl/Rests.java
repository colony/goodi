
package gd.impl;


import gd.api.v1.ClientContext;
import gd.api.v1.ClientType;
import gd.api.v1.LatLng;
import gd.api.v1.post.Geo;
import gd.impl.identity.AccessManager;
import gd.impl.identity.AuthLevel;
import gd.impl.session.SessionManager;
import gd.impl.session.SessionManager.InvalidSessionException;
import gd.impl.util.Page;
import gd.support.jersey.AppException;

import java.util.List;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;


public final class Rests
{
    private static final ThreadLocal<ClientContext> CONTEXT = new ThreadLocal<>();

    /**
     * A logger designated to log unrecognized locale
     */
    private static final Logger LOCALE_LOG = LoggerFactory.getLogger("unknown_locale");

    private static final Logger REQUEST_LOG = LoggerFactory.getLogger("request");

    public static final String OK_JSON = "{\"result\":\"ok\"}";

    private static AccessManager _accessMgr;

    private static SessionManager _sessMgr;



    public static Response appError(
        String message)
    {
        return Response.status(499).encoding(message).build();
    }


    public static long beginAdminOperation(
        HttpHeaders headers,
        String desc)

        throws AppException
    {
        return beginOperation(headers, AuthLevel.ADMIN, desc);
    }


    public static long beginOperation(
        HttpHeaders headers,
        String desc)

        throws AppException
    {
        return beginOperation(headers, AuthLevel.NORMAL, desc);
    }


    public static long beginUnauthOperation(
        HttpHeaders headers,
        String desc)

        throws AppException
    {
        return beginOperation(null, headers, AuthLevel.UNAUTH, desc);
    }


    /**
     * Validate if the http header has web session token
     * 
     * @return 0 or positive ID, this is not the real user ID, for
     *         now, the web session token doesn't contain any user
     *         ID, just used as verification
     */
    public static long beginWebOperation(
        HttpHeaders headers,
        String desc)

        throws AppException
    {
        return beginOperation(headers, AuthLevel.WEB, desc);
    }


    public static long beginOperation(
        HttpHeaders headers,
        AuthLevel authLevel,
        String desc)

        throws AppException
    {
        String token = null;
        if (authLevel == AuthLevel.AUTH_OPTIONAL)
        {
            String authHdr = retrieveAuthHeader(headers);
            if (StringUtils.isNotEmpty(authHdr))
            {
                token = retrieveSessionTokenFromAuthHeader(authHdr);
            }
        }
        else if (authLevel != AuthLevel.UNAUTH)
        {
            token = retrieveSessionToken(headers);
        }

        return beginOperation(token, headers, authLevel, desc);
    }


    public static long beginOperation(
        String token,
        HttpHeaders headers,
        AuthLevel authLevel,
        String desc)

        throws AppException
    {
        // If there is no token, make sure the authentication level permits it
        if (token == null)
        {
            if (authLevel != AuthLevel.UNAUTH &&
                authLevel != AuthLevel.AUTH_OPTIONAL)
            {
                throw new AppException(
                    Status.UNAUTHORIZED,
                    "Expected session token missing");
            }
        }

        // Make sure we are not offline unless this is an admin operation
        if (authLevel != AuthLevel.ADMIN)
        {
            if (GDLifecycle.isOffline())
            {
                throw new AppException(
                    Status.SERVICE_UNAVAILABLE,
                    "Service offline");
            }
        }

        // Validate the resulting session token and extract the related user ID
        long userId;
        if (token != null)
        {
            try
            {
                userId = _sessMgr.validateSession(token);
            }
            catch (InvalidSessionException e)
            {
                throw new AppException(
                    Status.UNAUTHORIZED,
                    "Invalid session token \"" + token + "\" :" + e.getMessage());
            }
        }
        else
        {
            // No session, must be AUTH_OPTIONAL or UNAUTH
            userId = -1;
        }

        if (authLevel == AuthLevel.ADMIN && !_accessMgr.isAdmin(userId))
        {
            throw new AppException(
                Status.FORBIDDEN,
                "Not authorized for admin operation");
        }

        // Obtain additional client details from the headers
        ClientContext cc = retrieveClientContext(headers);
        REQUEST_LOG.info(String.format("%s%d %8d %s", cc.getType().name(), cc.getVersion(), userId, desc));

        CONTEXT.remove();
        CONTEXT.set(cc);

        return userId;
    }


    public static String computePageUrl(
        Page page,
        UriInfo uriInfo)
    {
        // MUST be consistent with PageParam implementation
        StringBuilder sb =
            new StringBuilder(uriInfo.getAbsolutePath().toString());
        sb.append("?");
        if (StringUtils.isNotEmpty(page.boundary))
        {
            sb.append(page.isAfter() ? "_a" : "_b");
            sb.append("=");
            sb.append(page.boundary);
            sb.append("&");
        }

        if (StringUtils.isNotEmpty(page.key))
        {
            sb.append("_k=");
            sb.append(page.key);
            sb.append("&");
        }

        // Rest parameters do have default value
        sb.append(String.format(
            "_e=%d&_n=%d&_o=%d",
            page.exclusive ? 1 : 0,
            page.size,
            page.isDescending() ? -1 : 1));

        // If there is any param in uri other than pagination, append them
        MultivaluedMap<String,String> qp =
            uriInfo.getQueryParameters();
        if (!qp.isEmpty())
        {
            for (Map.Entry<String,List<String>> e : qp.entrySet())
            {
                if ("_a".equals(e.getKey()) ||
                    "_b".equals(e.getKey()) ||
                    "_e".equals(e.getKey()) ||
                    "_k".equals(e.getKey()) ||
                    "_n".equals(e.getKey()) ||
                    "_o".equals(e.getKey()))
                    continue;
                sb.append(String.format(
                    "&%s=%s",
                    e.getKey(),
                    e.getValue().get(0)));
            }
        }
        return sb.toString();
    }


    public static int getValue(
        MultivaluedMap<String,String> map,
        String key,
        int dflt)
    {
        String value = getValue(map, key, null);
        if (value == null)
        {
            return dflt;
        }

        try
        {
            return Integer.valueOf(value);
        }
        catch (IllegalArgumentException e)
        {
//            throw new WebApplicationException(
//                (Response.status(Status.BAD_REQUEST)
//                    .entity(
//                        "Invalid \""
//                            + key
//                            + "\" value: \""
//                            + value
//                            + "\"")
//                    .build()));

            throw new AppException(
                Status.BAD_REQUEST,
                "Invalid \""
                    + key
                    + "\" value: \""
                    + value
                    + "\"");
        }
    }


    public static long getValue(
        MultivaluedMap<String,String> map,
        String key,
        long dflt)
    {
        String value = getValue(map, key, null);
        if (value == null)
        {
            return dflt;
        }

        try
        {
            return Long.valueOf(value);
        }
        catch (IllegalArgumentException e)
        {
//            throw new WebApplicationException(
//                (Response.status(Status.BAD_REQUEST)
//                    .entity(
//                        "Invalid \""
//                            + key
//                            + "\" value: \""
//                            + value
//                            + "\"")
//                    .build()));

            throw new AppException(
                Status.BAD_REQUEST,
                "Invalid \""
                    + key
                    + "\" value: \""
                    + value
                    + "\"");
        }
    }


    public static String getValue(
        MultivaluedMap<String,String> map,
        String key,
        String dflt)
    {
        List<String> values = map.get(key);
        if (values == null || values.isEmpty())
        {
            return dflt;
        }

        // Only interested at FIRST value
        String value = values.get(0);
        if (Strings.isNullOrEmpty(value))
        {
            return dflt;
        }

        return value;
    }


    public static void init()
    {
        _accessMgr = AccessManager.instance();
        _sessMgr = SessionManager.instance();
    }


//    public static void logAuthenticationError(
//        String msg)
//    {
//        REQUEST_LOG.info(msg);
//    }


    public static Response ok()
    {
        return Response.ok(OK_JSON).build();
    }


    public static <K,V> Response ok(
        K k,
        V v)
    {
        return Response.ok(ImmutableMap.of(k, v)).build();
    }


    public static <K,V> Response ok(
        K k1,
        V v1,
        K k2,
        V v2)
    {
        return Response.ok(ImmutableMap.of(k1, v1, k2, v2)).build();
    }


    public static ClientContext retrieveClientContext(
        HttpHeaders headers)

        throws AppException
    {
        // Prepare default
        ClientContext cc = new ClientContext();

        // Fetch locale info
        String localeStr = retrieveLocaleHeader(headers);
        if (!Strings.isNullOrEmpty(localeStr))
        {
            try
            {
                cc.setLocale(LocaleUtils.toLocale(localeStr));
            }
            catch (IllegalArgumentException e)
            {
                // 
                LOCALE_LOG.info(localeStr);

//                throw new WebApplicationException(
//                    (Response.status(Status.BAD_REQUEST)
//                        .entity(
//                            "Invalid X-GD-LOCALE \""
//                                + localeStr
//                                + "\"")
//                        .build()));
            }
        }

        // Fetch client info
        String clientStr = retrieveClientHeader(headers);
        if (!Strings.isNullOrEmpty(clientStr))
        {
            // Expect format: ClientType:version
            String[] fields = clientStr.split(":");

            if (fields == null || fields.length != 2)
            {
                throw new AppException(
                    Status.BAD_REQUEST,
                    "Invalid X-GD-CLIENT \"" + clientStr + "\"");
            }

            try
            {
                cc.setType(ClientType.valueOf(fields[0]));
                cc.setVersion(Integer.valueOf(fields[1]));
            }
            catch (IllegalArgumentException e)
            {
                throw new AppException(
                    Status.BAD_REQUEST,
                    "Invalid X-GD-CLIENT \"" + clientStr + "\"");
            }
        }

        // Fetch geo info
        String geoStr = retrieveGeoHeader(headers);
        if (!Strings.isNullOrEmpty(geoStr))
        {
            try
            {
                cc.setLatLng(new LatLng(geoStr));
            }
            catch (IllegalArgumentException e)
            {
                throw new AppException(
                    Status.BAD_REQUEST,
                    "Invalid X-GD-GEO \"" + geoStr + "\"");
            }
        }

        return cc;
    }


    /**
     * Returns the client context from thread local storage. NOTE,
     * the client context will only be set after calling
     * beginOperation
     */
    public static ClientContext retrieveClientContextThreadLocal()
    {
        return CONTEXT.get();
    }


    /**
     * Deprecated. Since ios 1.2.1, we no longer inject all
     * geographic info into X-GD-GEO. Instead, we only inject a
     * simple format of "lat,lon". However, old app still rely on
     * this magic header to pass geo information while creating
     * posts and comments. Hence, we will call this method to parse
     * and return old geo information. Caller must make sure the
     * header value is old X-GD-GEO before calling this method
     */
    public static Geo retrieveGeo(
        HttpHeaders headers)
    {
        String geoStr = retrieveGeoHeader(headers);
        if (!Strings.isNullOrEmpty(geoStr))
        {
            try
            {
                // Caller must make sure the header value is old X-GD-GEO
                // before calling this method, otherwise, it might throw
                // illegal exception
                return new Geo(geoStr);
            }
            catch (IllegalArgumentException e)
            {
                throw new AppException(
                    Status.BAD_REQUEST,
                    "Expect old X-GD-GEO but invalid \"" + geoStr + "\"");
            }
        }
        return null;
    }


    public static LatLng retrieveLatLng(
        HttpHeaders headers)
    {
        ClientContext cc = retrieveClientContext(headers);
        return cc.getLatLng();
    }


    public static String retrieveSessionToken(
        HttpHeaders headers)

        throws AppException
    {
        // Go fetch the Authorization header
        String authHdr = retrieveAuthHeader(headers);
        if (authHdr == null)
        {
            throw new AppException(
                Status.UNAUTHORIZED,
                "Missing Authentication header");

//            throw new WebApplicationException(
//                (Response.status(Status.UNAUTHORIZED)
//                    .header(
//                        HttpHeaders.WWW_AUTHENTICATE,
//                        makeAuthenticateHeader(null, null))
//                    .entity("Authentication required")
//                    .build()));
        }
        return retrieveSessionTokenFromAuthHeader(authHdr);
    }


    public static String retrieveSessionTokenFromAuthHeader(
        String header)

        throws AppException
    {
        // Go parse the session token out of the header
        String[] fields = header.split("\\s+");

        // Make sure we have the correct authentication scheme
        if (fields == null ||
            fields.length < 1)
        {
//            throw new WebApplicationException(
//                (Response.status(Status.UNAUTHORIZED)
//                    .header(
//                        HttpHeaders.WWW_AUTHENTICATE,
//                        makeAuthenticateHeader(null, null))
//                    .entity("Authentication required")
//                    .build()));

            throw new AppException(
                Status.UNAUTHORIZED,
                "Missing Authentication header");
        }

        if (!fields[0].equals(AUTH_SCHEME))
        {
//            logAuthenticationError("Invalid authentication scheme \"" + header + "\"");

//            throw new WebApplicationException(
//                (Response.status(Status.UNAUTHORIZED)
//                    .header(
//                        HttpHeaders.WWW_AUTHENTICATE,
//                        makeAuthenticateHeader(null, null))
//                    .entity("Authentication required")
//                    .build()));

            throw new AppException(
                Status.UNAUTHORIZED,
                "Invalid auth header \"" + header + "\"");
        }

        // Make sure we have the right number of tokens for our
        // scheme
        if (fields.length != 2)
        {
//            logAuthenticationError("Malformed auth header \"" + header + "\"");

//            throw new WebApplicationException(
//                (Response.status(Status.BAD_REQUEST)
//                    .header(
//                        HttpHeaders.WWW_AUTHENTICATE,
//                        makeAuthenticateHeader(
//                            AUTH_ERROR_REQUEST,
//                            ("expected 2 tokens, found "
//                            + fields.length)))
//                    .entity("Invalid Authorization header")
//                    .build()));

            throw new AppException(
                Status.UNAUTHORIZED,
                "Malformed auth header \"" + header + "\"");
        }

        return fields[1];
    }


    //
    // INTERNAL METHODS
    //


    private static String makeAuthenticateHeader(
        String error,
        String desc)
    {
        StringBuilder sb = new StringBuilder();

        sb.append(AUTH_SCHEME);
        sb.append(' ');
        sb.append(AUTH_PARAM_REALM);
        sb.append("=\"");
        sb.append(AUTH_REALM);
        sb.append('"');

        if (error != null)
        {
            sb.append(", ");
            sb.append(AUTH_PARAM_ERROR);
            sb.append("=\"");
            sb.append(error);
            sb.append('"');
        }

        if (desc != null)
        {
            sb.append(", ");
            sb.append(AUTH_PARAM_ERROR_DESC);
            sb.append("=\"");
            sb.append(desc);
            sb.append('"');
        }

        return sb.toString();
    }


    private static String retrieveAuthHeader(
        HttpHeaders headers)
    {
        // Retrieve the complete set of Authorization headers
        List<String> authHdrs =
            headers.getRequestHeader(HttpHeaders.AUTHORIZATION);
        if (authHdrs == null ||
            authHdrs.isEmpty())
        {
            // No Authorization header present
            return null;
        }

        // Return the first header in the collection
        return authHdrs.get(0);
    }


    private static String retrieveClientHeader(
        HttpHeaders headers)
    {
        // Retrieve the complete set of Client headers
        List<String> clientHdrs =
            headers.getRequestHeader(GD_CLIENT_HEADER);
        if (clientHdrs == null ||
            clientHdrs.isEmpty())
        {
            // No Client header present
            return null;
        }

        // Return the first header in the collection
        return clientHdrs.get(0);
    }


    private static String retrieveGeoHeader(
        HttpHeaders headers)
    {
        // Retrieve the complete set of Geo headers
        List<String> geoHdrs =
            headers.getRequestHeader(GD_GEO_HEADER);
        if (geoHdrs == null || geoHdrs.isEmpty())
        {
            // No geo header present
            return null;
        }

        // Return the first header in the collection
        return geoHdrs.get(0);
    }


    private static String retrieveLocaleHeader(
        HttpHeaders headers)
    {
        // Retrieve the complete set of Locale headers
        List<String> localeHdrs =
            headers.getRequestHeader(GD_LOCALE_HEADER);
        if (localeHdrs == null ||
            localeHdrs.isEmpty())
        {
            // No Locale header present
            return null;
        }

        // Return the first header in the collection
        return localeHdrs.get(0);
    }



    //
    // CONSTANTS
    //

    /**
     * WWW-Authenticate error code for an invalid request (e.g.
     * missing or invalid parameters). This is associated with
     * response code 400.
     */
    public static final String AUTH_ERROR_REQUEST =
        "invalid_request";

    /**
     * WWW-Authenticate error code for an invalid token (e.g.
     * expired, revoked, malformed). This is associated with
     * response code 401.
     */
    public static final String AUTH_ERROR_TOKEN = "invalid_token";

    /**
     * Auth-Param key for the realm name
     */
    public static final String AUTH_PARAM_REALM = "realm";

    /**
     * Auth-Param key for the error code
     */
    public static final String AUTH_PARAM_ERROR = "error";

    /**
     * Auth-Param key for an error description string
     */
    public static final String AUTH_PARAM_ERROR_DESC =
        "error_description";

    /**
     * The HTTP authentication realm for Goodi
     */
    public static final String AUTH_REALM = "Goodi";

    /**
     * Authentication scheme for Authorization and WWW-Authenticate
     * headers
     */
    public static final String AUTH_SCHEME = "Bearer";

    /**
     * The custom HTTP header for GD client in format of
     * ClientType:version
     */
    public static final String GD_CLIENT_HEADER = "X-GD-CLIENT";

    /**
     * The custom HTTP header for geo. Check {@link Geo} for details
     * on format
     */
    public static final String GD_GEO_HEADER = "X-GD-GEO";

    /**
     * The custom HTTP header for locale in format of
     * <language>_<COUNTRY uppercase>, i.e. en_US
     */
    public static final String GD_LOCALE_HEADER = "X-GD-LOCALE";
}
