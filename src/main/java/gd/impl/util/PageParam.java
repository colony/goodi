
package gd.impl.util;


import gd.impl.Rests;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;


public class PageParam
{
    private Page page;


    /**
     * <h3>QUERY PARAMETERS</h3>
     * 
     * <pre>
         _a      Only include items that come after the item
         _b      Only include items that come before the item
         _e      Excludes[0|1] the item specified by "_a" or "_b"
         _k      Name of the sort key
         _n      Do not include more than the specified number of items
         _o      "1" to sort in ascending order, "-1" for descending order
       </pre>
     */
    public PageParam(
        @Context UriInfo uriInfo)
    {
        MultivaluedMap<String,String> qp =
            uriInfo.getQueryParameters();

        int size = Rests.getValue(qp, "_n", Page.DFLT_PAGE_SIZE);

        // Default descending
        short order;
        int orderVal = Rests.getValue(qp, "_o", -1);
        if (orderVal == -1)
        {
            order = Page.DESC;
        }
        else if (orderVal == 1)
        {
            order = Page.ASC;
        }
        else
        {
            throw new WebApplicationException(
                (Response.status(Status.BAD_REQUEST)
                    .entity(
                        "Invalid sort order \"" + orderVal + "\"\n")
                    .build()));
        }

        // Default exclusive
        boolean exclusive;
        int excVal = Rests.getValue(qp, "_e", 1);
        if (excVal == 1)
        {
            exclusive = true;
        }
        else if (excVal == 0)
        {
            exclusive = false;
        }
        else
        {
            throw new WebApplicationException(
                (Response.status(Status.BAD_REQUEST)
                    .entity(
                        "Invalid exclusive \"" + excVal + "\"\n")
                    .build()));
        }

        String key = Rests.getValue(qp, "_k", null);

        short position = Page.AFTER;
        String boundary = null;
        int numSet = 0;
        if (qp.containsKey("_a"))
        {
            position = Page.AFTER;
            boundary = Rests.getValue(qp, "_a", null);
            ++numSet;
        }

        if (qp.containsKey("_b"))
        {
            position = Page.BEFORE;
            boundary = Rests.getValue(qp, "_b", null);
            ++numSet;
        }

        if (numSet == 0)
        {
            // Default to AFTER and NULL boundary
            position = Page.AFTER;
            boundary = null;
        }
        else if (numSet > 1)
        {
            throw new WebApplicationException(
                (Response.status(400)
                    .entity(
                        "Cannot specify zero or more than one boundary parameter")
                    .build()));
        }

        // Finally, construct the Page
        this.page =
            new Page(
                boundary,
                order,
                exclusive,
                key,
                position,
                size);
    }


    public Page get()
    {
        return page;
    }
}
