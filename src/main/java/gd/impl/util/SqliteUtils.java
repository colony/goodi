
package gd.impl.util;


import gd.impl.SQLiteManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SqliteUtils
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SqliteUtils.class);


    /**
     * sqlite persistance wrapper for Use for SELECT Clause
     * 
     * @param sql
     * @param param
     * @param queryName
     * @return resultset
     */
    public static void executeSelectQuery(
        String sql,
        Object[] param,
        String queryName,
        ResultSetParser resultSetParser)
    {
        LOG.debug("execute SELECT \"{}\"", queryName);

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        try
        {
            connection = SQLiteManager.instance().getConnection();
            statement = connection.prepareStatement(sql);
            prepareStatement(statement, param);
            rs = statement.executeQuery();
            if (resultSetParser != null)
                resultSetParser.parse(rs);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Fail to execute query \""
                + queryName + "\"", e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for query \"{}\"",
                    queryName,
                    e);
            }
        }
    }


    /**
     * sqlite persistance wrapper for Use for UPDATE and DELETE.
     * NOTE the method is synchronized because we only allow one
     * writer connection
     * 
     * @param sql
     * @param param
     * @param queryName
     * 
     * @return A return value of 1 or greater would then indicate a
     *         successful insert, update, delete.
     */
    public synchronized static boolean executeQuery(
        String sql,
        Object[] param,
        String queryName)
    {
        LOG.debug("execute \"{}\"", queryName);

        Connection connection = null;
        PreparedStatement statement = null;
        try
        {
            connection = SQLiteManager.instance().getConnection();
            statement = connection.prepareStatement(sql);
            prepareStatement(statement, param);
            return statement.executeUpdate() >= 1;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Fail to execute query \""
                + queryName + "\"", e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for query \"{}\"",
                    queryName,
                    e);
            }
        }
    }


    /**
     * Sqlite wrapper for Use for INSERT Clause. NOTE the method
     * only works with table that has numeric primary key. NOTE the
     * method is synchronized because we only allow one writer
     * connection
     * 
     * @param sql
     * @param param
     * @param queryName
     * @param expectGeneratedKey
     *            True if the insertion will generate the numeric
     *            primary key (i.e. auto-increment) and expect
     *            return it. False if either the table's primary key
     *            is not generated (maybe non-numeric) or not expect
     *            return it (in this case -1 is returned)
     * 
     * @return 0 if insert fails for some reason (other than
     *         constraint violation), the generated unique primary
     *         key if expectGeneratedKey is true. Otherwise, 1 which
     *         means one row affected.
     * 
     * @throws SQLException
     *             Exception is thrown ONLY when constraint violated
     *             on duplicate index
     */
    public synchronized static long executeInsertQuery(
        String sql,
        Object[] params,
        String queryName,
        boolean expectGeneratedKey)
    {
        LOG.debug("execute INSERT \"{}\"", queryName);

        Connection connection = null;
        PreparedStatement statement = null;
        try
        {
            connection = SQLiteManager.instance().getConnection();

            statement =
                connection.prepareStatement(
                    sql,
                    expectGeneratedKey ?
                        PreparedStatement.RETURN_GENERATED_KEYS :
                        PreparedStatement.NO_GENERATED_KEYS
                    );
            prepareStatement(statement, params);

            int row = statement.executeUpdate();
            if (row >= 1 && expectGeneratedKey)
            {
                ResultSet rs = statement.getGeneratedKeys();
                long generateKey = 0L;
                if (rs != null && rs.next())
                {
                    generateKey = rs.getLong(1);
                }
                return generateKey;
            }
            else
            {
                return row;
            }
        }
        catch (SQLException e)
        {
            // 19 is vendor-specific error code
            // https://www.sqlite.org/rescode.html#constraint
//            if (e.getErrorCode() == 19)
//            {
//                return 0L;
//            }
//            else
            {
                throw new RuntimeException(
                    "Fail to execute query \""
                        + queryName + "\"",
                    e);
            }
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                    statement = null;
                }
                if (connection != null)
                {
                    connection.close();
                    connection = null;
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for query \"{}\"",
                    queryName,
                    e);
            }
        }
    }


    /**
     * Sqlite persistence wrapper to execute multiple queries within
     * one transaction. NOTE the method is synchronized because we
     * only allow one writer connection
     * 
     * @param summary
     *            Used for logging purpose ONLY
     * @param objects
     *            A variable-length of array containing
     *            query-related data. It must be the sql string
     *            followed by parameter array.
     * 
     */
    public synchronized static void executeTransactionalQueries(
        String summary,
        Object... objects)
    {
        LOG.debug("execute transation \"{}\"", summary);

        Connection conn = null;
        List<PreparedStatement> statements = new ArrayList<>(objects.length / 2);
        boolean oldAutoCommit = true;
        try
        {
            conn = SQLiteManager.instance().getConnection();
            oldAutoCommit = conn.getAutoCommit();
            conn.setAutoCommit(false);

            for (int i = 0; i < objects.length; i += 2)
            {
                String sql = (String) objects[i];
                Object[] param = (Object[]) objects[i + 1];
                PreparedStatement stat = conn.prepareStatement(sql);
                prepareStatement(stat, param);
                statements.add(stat);
                stat.executeUpdate();
            }

            conn.commit();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Fail to execute transaction " + summary, e);
        }
        finally
        {
            try
            {
                for (Statement stat : statements)
                {
                    if (stat != null)
                        stat.close();
                }
                if (conn != null) {
                    conn.setAutoCommit(oldAutoCommit);
                    conn.close();
                }
            }
            catch (Exception e)
            {
                LOG.error("Fail to close connection for transaction query " + summary, e);
            }
        }
    }


    /**
     * Sqlite persistence wrapper to execute multiple queries in
     * bulk.NOTE the method is synchronized because we only allow
     * one writer connection
     * 
     * @param summary
     *            Used for logging purpose ONLY
     * @param sql
     *            All executed queries have same sql but with
     *            different parameters
     * @param params
     *            A list of parameters (array) per query in the batch
     */
    public synchronized static void executeBatchQueries(
        String summary,
        String sql,
        List<Object[]> params)
    {
        LOG.debug("execute batch \"{}\"", summary);

        Connection conn = null;
        PreparedStatement statement = null;
        boolean oldAutoCommit = true;
        try
        {
            conn = SQLiteManager.instance().getConnection();
            oldAutoCommit = conn.getAutoCommit();
            conn.setAutoCommit(false);

            statement = conn.prepareStatement(sql);
            for (Object[] param : params) {
                prepareStatement(statement, param);
                statement.addBatch();
            }

            statement.executeBatch();
            conn.commit();
        }
        catch (Exception e)
        {
            throw new RuntimeException("Fail to execute batch " + summary, e);
        }
        finally
        {
            try
            {
                if (statement != null) {
                    statement.close();
                }
                if (conn != null) {
                    conn.setAutoCommit(oldAutoCommit);
                    conn.close();
                }
            }
            catch (Exception e)
            {
                LOG.error("Fail to close connection for transaction query " + summary, e);
            }
        }
    }


    public static void prepareStatement(
        PreparedStatement statement,
        Object[] params)
        throws SQLException
    {
//        statement.clearParameters();
        if (null != params)
        {
            for (int i = 0; i < params.length; i++)
            {
                if (params[i] == null)
                {
                    statement.setObject(i + 1, null);
                }
                else if (params[i] instanceof String)
                {
                    statement.setString(i + 1, String.valueOf(params[i]));
                }
                else if (params[i] instanceof Integer)
                {
                    statement.setInt(i + 1, (Integer) params[i]);
                }
                else if (params[i] instanceof Long)
                {
                    statement.setLong(i + 1, (Long) params[i]);
                }
                else if (params[i] instanceof Double)
                {
                    statement.setDouble(i + 1, (Double) params[i]);
                }
                else
                {
                    statement.setObject(i + 1, params[i]);
                }
            }
        }
    }


    public static void createCompositeIndex(
        String tableName,
        String index,
        String columnName1,
        String columnName2,
        boolean unique)
    {
        String sql =
            unique ?
                "create unique index if not exists " + index
                    + " on "
                    + tableName
                    + " ( " + columnName1 + ", "
                    + columnName2 + " )"
                :
                "create index if not exists " + index + " on "
                    + tableName
                    + " ( " + columnName1 + ", "
                    + columnName2 + " )";
        executeQuery(sql, null, "CreateCompositeIndex");
    }
    

    public static void createSingleIndex(
        String tableName,
        String index,
        String columnName,
        boolean unique)
    {
        String sql =
            unique ?
                "create unique index if not exists " + index
                    + " on "
                    + tableName
                    + " (" + columnName + " )"
                :
                "create index if not exists " + index + " on "
                    + tableName
                    + " (" + columnName + " )";
        executeQuery(sql, null, "CreateSingleIndex");
    }
}
