
package gd.impl.util;


public interface Filter<T>
{
    public boolean accept(
        T t);


    /**
     * Chain a filter
     * 
     * @param filter
     * @return The chained filter, in this way, it is easy to chain
     *         a list of filters together
     */
    public Filter<T> chainFilter(
        Filter<T> filter);


    public Filter<T> getNextFilter();
}
