package gd.impl.util;

public interface Converter<FROM,TO>
{
    public TO convert(FROM from);
}
