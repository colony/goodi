package gd.impl.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetParser
{
    public void parse(
        ResultSet rs)
        throws SQLException;
}