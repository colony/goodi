package gd.impl.util;

public interface Builder<T>
{
    public T build();
}
