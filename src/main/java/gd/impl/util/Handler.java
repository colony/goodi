package gd.impl.util;

public interface Handler<T>
{
    public void handle(T t);
}
