
package gd.impl.util;

import java.util.LinkedHashMap;
import java.util.Map.Entry;


public class LRUCache<K,V>
    extends
        LinkedHashMap<K,V>
{
    private static final long serialVersionUID =
        8331292080450254081L;
    
    private int limit;


    public LRUCache(
        int limit)
    {
        super(8, 0.75f, true);
        this.limit = limit;
    }


    @Override
    protected boolean removeEldestEntry(
        Entry<K,V> eldest)
    {
        return size() > limit;
    }
}
