package gd.impl.util;

public interface Sortable
{
    public String getSortKey();
    
    public String getSortVal();
}
