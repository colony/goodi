
package gd.impl.util;


/**
 * Exception indicating an update operation of some kind was aborted
 * because the new value appears to have been based on an
 * out-of-date version of the object being updated.
 */
public class UpdateConflictException
    extends
        Exception
{
    private static final long serialVersionUID =
        -3107979888548659221L;


    public UpdateConflictException()
    {
        super();
    }


    public UpdateConflictException(
        String msg)
    {
        super(msg);
    }


    public UpdateConflictException(
        Throwable cause)
    {
        super(cause);
    }


    public UpdateConflictException(
        String msg,
        Throwable cause)
    {
        super(msg, cause);
    }
}
