
package gd.impl.util;




/**
 * A pagination designated for REST and general usage. For now only
 * one sort key supported
 */
public class Page
{
    public String boundary;

    public boolean exclusive = false; // default

    /**
     * page key. More specifically, sort key by which the results
     * are paged and ordered
     */
    public String key;

    public short order;

    public short position;

    public int size;


    public Page()
    {
    }


    /**
     * Convenience constructor starting at NULL boundary with
     * default size, order, position
     */
    public Page(
        String key)
    {
        this(DFLT_ORDER, key, DFLT_POS);
    }


    /**
     * Convenience constructor starting at NULL boundary with
     * default size
     */
    public Page(
        short order,
        String key,
        short position)
    {
        this(order, key, position, DFLT_PAGE_SIZE);
    }


    /**
     * Convenience constructor starting at NULL boundary with
     * specified size
     */
    public Page(
        short order,
        String key,
        short position,
        int size)
    {
        // exclusive doesn't matter
        this(null, order, false, key, position, size);
    }


    public Page(
        String boundary,
        short order,
        boolean exclusive,
        String key,
        short position)
    {
        this(
            boundary,
            order,
            exclusive,
            key,
            position,
            DFLT_PAGE_SIZE);
    }


    public Page(
        String boundary,
        short order,
        boolean exclusive,
        String key,
        short position,
        int size)
    {
        this.boundary = boundary;
        this.order = order;
        this.exclusive = exclusive;
        this.key = key;
        this.position = position;
        this.size = size;
    }


    /**
     * Copy constructor
     */
    public Page(
        Page page)
    {
        this.boundary = page.boundary;
        this.exclusive = page.exclusive;
        this.key = page.key;
        this.order = page.order;
        this.position = page.position;
        this.size = page.size;
    }


    //
    // CONVENIENT METHODS
    //

    public boolean isAfter()
    {
        return this.position == AFTER;
    }


    /**
     * Convenient method to determine if this is the default page
     * scenario, which is AFTER, descending, NULL boundary, a.k.a
     * first page
     */
    public boolean isDefault()
    {
        return this.isAfter() && this.isDescending()
            && this.boundary == null;
    }


    public boolean isDescending()
    {
        return this.order == DESC;
    }


    /**
     * Return a math relational operator (<,>,<=,>=) based on this
     * page's order, position and exclusive. The method is useful
     * when preparing query, etc
     */
    public String toMathOperator()
    {
        // AFTER, ASC  - >
        // AFTER, DESC - <
        // BEFORE ASC  - <
        // BEFORE DESC - >
        String op = (this.isDescending() == this.isAfter()) ? "<" : ">";
        if (!this.exclusive)
            op += "=";
        return op;
    }


    //
    // BUILDER METHODS
    //


    public Page b(
        String boundary)
    {
        this.boundary = boundary;
        return this;
    }


    public Page o(
        short order)
    {
        this.order = order;
        return this;
    }


    public Page e(
        boolean exclusive)
    {
        this.exclusive = exclusive;
        return this;
    }


    public Page k(
        String key)
    {
        this.key = key;
        return this;
    }


    public Page p(
        short postion)
    {
        this.position = postion;
        return this;
    }


    public Page s(
        int size)
    {
        this.size = size;
        return this;
    }


    /**
     * The default page size
     */
    public static final int DFLT_PAGE_SIZE = 20;

    public static final short ASC = 0;

    public static final short DESC = 1;

    // order, default ascending
    public static final short DFLT_ORDER = DESC;

    public static final short AFTER = 0;

    public static final short BEFORE = 1;

    // position, default after
    public static final short DFLT_POS = AFTER;
}
