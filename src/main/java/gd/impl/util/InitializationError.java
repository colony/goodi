
package gd.impl.util;


public class InitializationError
    extends
        Error
{
    public InitializationError()
    {
        super();
    }


    public InitializationError(
        String msg)
    {
        super(msg);
    }


    public InitializationError(
        Throwable cause)
    {
        super(cause);
    }


    public InitializationError(
        String msg,
        Throwable cause)
    {
        super(msg, cause);
    }


    private static final long serialVersionUID =
        1309150876919408197L;

}
