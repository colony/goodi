package gd.impl.util;

public abstract class Visitor<T>
{
    public boolean before()
    {
        return true;
    }
    
    public abstract boolean visit(T item);
    
    public void after()
    {
        
    }
}
