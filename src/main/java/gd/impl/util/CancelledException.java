
package gd.impl.util;


/**
 * Exception thrown when a long-lived task, activity, etc. has been
 * administratively cancelled
 */
public class CancelledException
    extends
        Exception
{
    private static final long serialVersionUID =
        -4185896066067687072L;


    public CancelledException()
    {
        super();
    }


    public CancelledException(
        String msg)
    {
        super(msg);
    }


    public CancelledException(
        Throwable cause)
    {
        super(cause);
    }


    public CancelledException(
        String msg,
        Throwable cause)
    {
        super(msg, cause);
    }
}
