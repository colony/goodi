
package gd.impl.util;


public abstract class AbstractChainableFilter<T>
    implements
        Filter<T>
{
    protected Filter<T> nextFilter;


    public Filter<T> chainFilter(
        Filter<T> filter)
    {
        this.nextFilter = filter;
        return this.nextFilter;
    }


    public Filter<T> getNextFilter()
    {
        return this.nextFilter;
    }
}
