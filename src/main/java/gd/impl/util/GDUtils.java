
package gd.impl.util;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormat;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;


/**
 * Goodi common utilities
 */
public final class GDUtils
{
    private static final DateTimeFormatter DATE_FMT =
        DateTimeFormat.forPattern("HH:mm:ss MM/dd/yy ZZZ");

    private static final Pattern HASHTAG_PATTERN = Pattern.compile("(#[\\w\\d]+)");

    private static final String DOT = ".";

    private static final String SLASH = "/";


    /**
     * <p>
     * Abbreviates a String using ellipses. This will turn
     * "Now is the time for all good men" into
     * "Now is the time for..."
     * </p>
     * 
     * <p>
     * Specifically:
     * </p>
     * <ul>
     * <li>If str is less or equal to than maxWidth characters long,
     * return it</li>
     * <li>Else abbreviate it to
     * {@code (substring(str, 0, max-3) + "...")}.</li>
     * <li>If {@code maxWidth} is less than {@code 4}, throw an
     * {@code IllegalArgumentException}.</li>
     * <li>In no case will it return a String of length greater than
     * {@code maxWidth}.</li>
     * <li>IMPORTANT:the method will specially treat unicode within
     * range U+10000...0x10FFFF as one char when we deal with width
     * calculation</li>
     * </ul>
     */
    public static String abbreviate(
        final String str,
        final int maxWidth)
    {
        if (str == null)
            return null;
        
        if (maxWidth < 4)
            throw new IllegalArgumentException("Minimum abbreviation width is 4");
        
        int len = str.length();
        if (len <= maxWidth)
            return str;
        int cpc = str.codePointCount(0, len);
        if (cpc <= maxWidth)
            return str;
        
        final String abrevMarker = "...";
        
        // check if the string contains paird-surrogates codepoint, if not, we could
        // simply use substring method
        boolean hasSurrogatePair = (cpc != len);
        if (!hasSurrogatePair) {
            return str.substring(0, Math.min(len, maxWidth - 3)) + abrevMarker;
        } else {
            int offset = str.offsetByCodePoints(0, Math.min(cpc, maxWidth - 3));
            return str.substring(0, offset) + abrevMarker;
        }
    }


    @SuppressWarnings("unchecked")
    public static <T> T cast(
        Object obj)
    {
        return (T) obj;
    }


    public static void check(
        boolean expression,
        String errorMessage)
    {
        Preconditions.checkArgument(expression, errorMessage);
    }


    public static void check(
        boolean expression,
        String errorMessageFormat,
        Object... errorMessageArgs)
    {
        Preconditions.checkArgument(
            expression,
            String.format(errorMessageFormat, errorMessageArgs));
    }


    public static void check(
        boolean expression,
        Response.Status statusCode,
        String errorMessage)
    {
        if (!expression)
        {
            throw new WebApplicationException(
                Response.status(statusCode)
                    .entity(errorMessage)
                    .build());
        }
    }


    public static void check(
        boolean expression,
        Response.Status statusCode,
        String errorMessageFormat,
        Object... errorMessageArgs)
    {
        if (!expression)
        {
            throw new WebApplicationException(
                Response.status(statusCode)
                    .entity(
                        String.format(
                            errorMessageFormat,
                            errorMessageArgs))
                    .build());
        }
    }


    /**
     * Extract hashtags (i.e. #word) from the specified string. In
     * the returned list, all extracted tags are lowercased, removed
     * leading '#' sign and de-duped
     * 
     * @return Null if nothing found, otherwise, a list of hashtags
     */
    public static List<String> extractHashtags(
        String str)
    {
        Set<String> hashtags = null;
        if (StringUtils.isNotEmpty(str)) {
            Matcher matcher = HASHTAG_PATTERN.matcher(str);
            while (matcher.find())
            {
                if (hashtags == null)
                    hashtags = new HashSet<String>();
                hashtags.add(matcher.group().substring(1).toLowerCase()); // Remove leading #
            }
        }
        if (hashtags == null)
            return null;
        return new ArrayList<String>(hashtags); // Wrap in array and return
    }


    @SuppressWarnings("unchecked")
    public static <T> Class<? extends T> getClass(
        String name,
        Class<T> expected)
    {
        // Find the specified class
        Class<?> rawClass;
        try
        {
            rawClass = Class.forName(name);
        }
        catch (LinkageError | ClassNotFoundException e)
        {
            throw new RuntimeException("Fail to init class \""
                + name + "\"", e);
        }

        // Convert the class to one explicitly descended from the
        // expected type
        Class<? extends T> subClass;
        try
        {
            subClass = rawClass.asSubclass(expected);
        }
        catch (ClassCastException e)
        {
            throw new RuntimeException("Fail to cast class \""
                + name + "\" to \"" + expected.getName() + "\"", e);
        }

        // Declare victory
        return (Class<T>) subClass;
    }


    /**
     * Retrieve extension of file
     * 
     * @param path
     *            The filename to retrieve the extension of
     * @return the extension of the file or null if none exists.
     */
    public static String getFileExtension(
        String path)
    {
        if (Strings.isNullOrEmpty(path))
            return null;

        int i = path.lastIndexOf(DOT);
        if (i == -1)
            return null;
        return path.substring(i + 1);
    }


    public static <T> T getInstance(
        String name,
        Class<T> expected)
    {
        Class<? extends T> clazz = getClass(name, expected);
        try
        {
            return clazz.newInstance();
        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Fail to instantiate class \"" + clazz.getName()
                    + "\"",
                e);
        }
    }


    public static <T> T getFirst(
        List<T> coll)
    {
        if (coll == null || coll.isEmpty())
            return null;

        return coll.get(0);
    }


    public static <T> T getLast(
        List<T> coll)
    {
        if (coll == null || coll.isEmpty())
            return null;

        return coll.get(coll.size() - 1);
    }


    /**
     * Convenient method to join uri-like strings together by
     * resolving the leading and trailing "/" in between
     * 
     * <pre>
     *   GDUtils.joinPath("/aa/bb/","/cc/") = "/aa/bb/cc"
     * </pre>
     */
    public static String joinPath(
        String str1,
        String str2)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.stripEnd(str1, SLASH));
        sb.append(SLASH);
        sb.append(StringUtils.stripStart(str2, SLASH));
        return sb.toString();
    }


    /**
     * Format a date
     * 
     * @param timestamp
     *            An instant of time in milliseconds
     * @return
     */
    public static String printDateTime(
        long timestamp)
    {
        return DATE_FMT.print(timestamp);
    }


    /**
     * Print the duration (second - first) in a human readable way
     * 
     * @param first
     *            The start of duration in ms
     * @param second
     *            The end of duration in ms
     */
    public static String printDuration(
        long first,
        long second)
    {
        return PeriodFormat.getDefault().print(
            new Duration(second - first).toPeriod());
    }


    /**
     * Fisher-Yates shuffle
     * 
     * @param ar
     *            The array to be shuffled
     * @param seed
     *            The seed for random
     */
    public static void shuffle(
        int[] ar,
        long seed)
    {
        Random r = new Random(seed);
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = r.nextInt(i + 1);
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }


    /**
     * JodaTime duration could be parsed from ISO8061 duration
     * format including only seconds and milliseconds (PTa.bS). This
     * method extends the string to support hour and minute.
     * (PTaHbMc.dS)
     * 
     * @param str
     *            ISO8061 duration format including only time
     *            components
     * @return JodaTime Duration
     */
    public static Duration toDuration(
        String str)
    {
        if (!str.startsWith("PT"))
            throw new IllegalArgumentException(
                "Invalid duration string \"" + str + "\"");

        int hi = str.indexOf('H'), mi = str.indexOf('M'), si =
            str.indexOf('S');
        // The position order must be: H > M > S
        if (((mi > 0) && (hi > mi)) || ((si > 0) && (mi > si))
            || ((si > 0) && (hi > si)))
            throw new IllegalArgumentException(
                "Invalid duration string \"" + str + "\"");

        int h = 0, m = 0, s = 0, ms = 0;
        if (hi > 0)
        {
            h = Integer.parseInt(str.substring(2, hi));
        }
        if (mi > 0)
        {
            m = Integer.parseInt(str.substring(hi + 1, mi));
        }
        if (si > 0)
        {
            // Check the dot expression
            String sec = str.substring(mi + 1, si);
            String[] token = sec.split("\\.", 2);
            s = Integer.parseInt(token[0]);
            if (token.length == 2)
                ms = Integer.parseInt(token[1]);
        }

        // Sum everything to seconds and milliseconds
        s += 3600 * h + 60 * m;
        return new Duration(String.format("PT%d.%dS", s, ms));
    }
}
