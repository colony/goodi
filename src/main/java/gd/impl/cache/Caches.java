
package gd.impl.cache;


public final class Caches
{
    //
    // Cache ANYTHING Key
    //

    /**
     * Cache ANYTHING or ANYTHING_SET is designed to provide cache
     * function for any simple use case with least flexibility (key
     * and value must be stringfiable). The cache key should be
     * unique, hence, we collect all defined keys in one place.
     */

    /**
     * <ul>
     * <li>k - gpl
     * <li>v - a comma-separated string of
     * <group_id>,<group_name>,<group_id2>,<group_name2>,...
     * </ul>
     */
    public static final String ANYTHING_GROUP_PICK_LIST = "gpl";

    //
    // Cache USER_COUNT Key
    //

    /**
     * Cache USER_COUNT is designed to cache various counter for a
     * user. The cache key is a string of user ID plus an extra key
     * defined here
     */


    private static CacheManager _cacheMgr;


    public static <T> T get(
        CacheType id,
        Class<T> clazz)
    {
        return _cacheMgr.get(id, clazz);
    }


    public static void init()
    {
        _cacheMgr = CacheManager.instance();
    }
}
