
package gd.impl.cache;


import gd.impl.cache.C.LLCache;
import gd.impl.cache.C.SLCache;
import gd.impl.cache.C.SSCache;
import gd.impl.cache.C.SSSetCache;
import gd.impl.cache.resident.ResidentLLCache;
import gd.impl.cache.resident.ResidentSLCache;
import gd.impl.cache.resident.ResidentSSCache;
import gd.impl.cache.resident.ResidentSSSetCache;



public enum CacheType
{
    ANYTHING(
        "a",
        ResidentSSCache.class,
        SSCache.class),

    ANYTHING_SET(
        "as",
        ResidentSSSetCache.class,
        SSSetCache.class),

    /**
     * Number of voter per post
     * <ul>
     * <li>KEY: Long - ID of post
     * <li>VAL: Long - Number of voters
     * </ul>
     */
    POST_VOTER_COUNT(
        "pvc",
        ResidentLLCache.class,
        LLCache.class),

    /**
     * Cache for user different counters, for details on key, check
     * Caches
     */
    USER_COUNT(
        "uc",
        ResidentSLCache.class,
        SLCache.class),

    ;

    private final String abbrev;

    private final Class<? extends Cache<?,?>> dfltImplClass;

    private final Class<? extends Cache<?,?>> specializedInterface;


    private CacheType(
        String abbrev,
        Class<? extends Cache<?,?>> dfltImplClass,
        Class<? extends Cache<?,?>> spInterface)
    {
        this.abbrev = abbrev;
        this.dfltImplClass = dfltImplClass;
        this.specializedInterface = spInterface;
    }


    public String abbreviation()
    {
        return abbrev;
    }


    public Class<? extends Cache<?,?>> defaultImplClass()
    {
        return dfltImplClass;
    }


    public Class<? extends Cache<?,?>> specializedInterface()
    {
        return specializedInterface;
    }


    public static CacheType fromAbbreviation(
        String abbrev)
    {
        for (CacheType t : CacheType.values())
        {
            if (t.abbrev.equals(abbrev))
                return t;
        }

        throw new IllegalArgumentException("No cache found for abbrev \"" + abbrev + "\"");
    }
}
