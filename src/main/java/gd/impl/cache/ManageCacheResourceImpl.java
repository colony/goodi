
package gd.impl.cache;


import gd.api.v1.admin.ManageCacheResource;
import gd.impl.Rests;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


@SuppressWarnings({
        "rawtypes",
        "unchecked"
})
@Path("/adm/caches")
@Singleton
public class ManageCacheResourceImpl
    implements
        ManageCacheResource
{
    protected CacheManager cacheMgr;


    public ManageCacheResourceImpl()
    {
        this.cacheMgr = CacheManager.instance();
    }


    @Override
    @Path("/{cache}")
    @DELETE
    public Response deleteEntry(
        @Context HttpHeaders headers,
        @PathParam("cache") String typeStr,
        @QueryParam("key") String keyStr)
    {
        Rests.beginAdminOperation(headers, "adm:cache:delEntry:" + typeStr + ":" + keyStr);

        CacheType type = CacheType.valueOf(typeStr);
        
        Cache cache = this.cacheMgr.get(type, Cache.class);
        Object key = cache.makeKey(keyStr);
        cache.remove(key);

        return Response.ok().build();
    }


    @Override
    @Path("/{cache}")
    @GET
    @Produces("text/plain")
    public Response getEntry(
        @Context HttpHeaders headers,
        @PathParam("cache") String typeStr,
        @QueryParam("key") String keyStr)
    {
        Rests.beginAdminOperation(headers, "adm:cache:getEntry:" + typeStr + ":" + keyStr);

        CacheType type = CacheType.valueOf(typeStr);
        
        Cache cache = this.cacheMgr.get(type, Cache.class);
        Object key = cache.makeKey(keyStr);
        Object val = cache.get(key);
        if (val == null) {
            return Response.status(Status.NOT_FOUND)
                .entity("No value with key \"" + keyStr + "\" in cache " + type)
                .build();
        }
        return Response.ok(cache.makeValueStr(val)).build();
    }


    @Override
    @Path("/{cache}")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response putEntry(
        @Context HttpHeaders headers,
        @PathParam("cache") String typeStr,
        @FormParam("key") String keyStr,
        @FormParam("value") String valueStr)
    {
        Rests.beginAdminOperation(headers, "adm:cache:putEntry:" + typeStr + ":" + keyStr + ":" + valueStr);
        
        CacheType type = CacheType.valueOf(typeStr);
        
        Cache cache = this.cacheMgr.get(type, Cache.class);
        Object key = cache.makeKey(keyStr);
        Object val = cache.makeValue(valueStr);
        cache.put(key, val);
        
        return Response.ok().build();
    }
}
