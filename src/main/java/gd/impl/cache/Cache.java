
package gd.impl.cache;


public interface Cache<KEY,VALUE>
{
    /**
     * Stores the value in the cache using the specified key. If
     * there is already a value associated with the key, no changes
     * are made.
     * 
     * @param key
     * @param value
     * @return true if the value was added, false if not because
     *         there is already a value with the same key
     */
    public boolean add(
        KEY key,
        VALUE value);


    /**
     * Discards the entire contents of the cache
     */
    public void clear();


    /**
     * Returns the value associated with the specified key, if any
     * 
     * @param key
     * @return The value associated with the specified key, or null
     *         if none is present
     */
    public VALUE get(
        KEY key);


    public void init(
        CacheConfig config);


    /**
     * Generates a KEY object from the specified String
     * 
     * @param str
     *            The string to be converted
     * @return A KEY value
     */
    public KEY makeKey(
        String keyStr);


    /**
     * Generates a String representation of the specified KEY
     * 
     * @param key
     *            The KEY to be converted
     * @return A String representation of the KEY
     */
    public String makeKeyStr(
        KEY k);


    /**
     * Generates a VALUE object from the specified String
     * 
     * @param str
     *            The string to be converted
     * @return A VALUE value
     */
    public VALUE makeValue(
        String valStr);


    /**
     * Generates a String representation of the specified VALUE
     * 
     * @param value
     *            The VALUE to be converted
     * @return A String representation of the VALUE
     */
    public String makeValueStr(
        VALUE v);


    /**
     * Stores the specified value in the cache using the specified
     * key. If there is already a value assoicated with the key, it
     * will be replaced with the specified value.
     * 
     * @param key
     * @param value
     */
    public void put(
        KEY key,
        VALUE value);


    /**
     * Removes the value associated with the specified key, if any.
     * 
     * @param key
     * @return The value that was originally associated with the key
     *         or null if the key was not present to begin with
     */
    public VALUE remove(
        KEY key);
}
