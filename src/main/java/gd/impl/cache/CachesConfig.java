
package gd.impl.cache;


import gd.impl.config.Configured;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import com.google.common.collect.Lists;


// Not reloadable
@Configured(fname = "cache.yml")
public class CachesConfig
{
    private EnumMap<CacheType,CacheConfig> caches;


    /**
     * Default constructor
     */
    public CachesConfig()
    {
    }


    public CacheConfig get(
        CacheType id)
    {
        if (this.caches == null || !this.caches.containsKey(id))
        {
            // Return a default one when miss
            return new CacheConfig(id);
        }
        return this.caches.get(id);
    }


    public List<CacheConfig> getCaches()
    {
        if (this.caches == null)
        {
            return Lists.newArrayList();
        }
        return new ArrayList<>(this.caches.values());
    }


    @SuppressWarnings("unused")
    private void setCaches(
        List<CacheConfig> caches)
    {
        this.caches = new EnumMap<>(CacheType.class);
        for (CacheConfig cfg : caches)
        {
            this.caches.put(cfg.getId(), cfg);
        }

        // Create default configuration for missing one
        for (CacheType id : CacheType.values())
        {
            if (!this.caches.containsKey(id))
            {
                this.caches.put(id, new CacheConfig(id));
            }
        }
    }
}
