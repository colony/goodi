
package gd.impl.cache;


import java.util.Set;


/**
 * Extension of {@link Cache} that is specialized for the case where
 * the value are sets,
 */
public interface SetCache<KEY,ITEM,VALUE extends Set<ITEM>>
    extends
        Cache<KEY,VALUE>
{
    /**
     * Adds the specified item into the set value associated with
     * the key, if the set value doesn't exist, the set will be
     * created first. If the set value already exist and the item to
     * be added already exists, the set won't allow duplicate value,
     * the method won't do anything and return false.
     * 
     * @return true if the item was added and there wasn't same item
     *         in set before, false if the item already exist and
     *         the set value doesn't allow duplicate
     */
    public boolean addItem(
        KEY key,
        ITEM item);


    /**
     * Check if the specified item is a member of the set value
     * associated with the specified key.
     * 
     * @return true if the item exists in the set value, false
     *         otherwise
     */
    public boolean existItem(
        KEY key,
        ITEM item);


    /**
     * Removes the specified item from the set value associated with
     * the specified key.
     * 
     * @return True if the item exists in the set value and be
     *         removed, false otherwise
     */
    public boolean removeItem(
        KEY key,
        ITEM item);
}
