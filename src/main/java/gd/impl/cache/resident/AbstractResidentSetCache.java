
package gd.impl.cache.resident;


import gd.impl.cache.SetCache;

import java.util.HashSet;
import java.util.Set;


public abstract class AbstractResidentSetCache<KEY,ITEM>
    extends
        AbstractResidentCache<KEY,Set<ITEM>>
    implements
        SetCache<KEY,ITEM,Set<ITEM>>
{
    protected static final String SPLIT = ",";


    public AbstractResidentSetCache()
    {
    }


    //
    // ABSTRACT METHODS
    //

    protected abstract ITEM makeItem(
        String itemStr);


    protected abstract String makeItemStr(
        ITEM item);



    //
    // Cache
    // (NOTE, we need synchronized all super interface since we are going
    //  to modify items for value set)
    //


    @Override
    public synchronized boolean add(
        KEY k,
        Set<ITEM> v)
    {
        return super.add(k, v);
    }


    @Override
    public synchronized void clear()
    {
        super.clear();
    }


    @Override
    public synchronized Set<ITEM> get(
        KEY key)
    {
        Set<ITEM> value = super.get(key);
        if (value == null)
            return null;
        
        // We need return a copy of set, also the copy-constructor
        // is not safe, we need synchronized
        return new HashSet<>(value);
    }


    @Override
    public synchronized void put(
        KEY key,
        Set<ITEM> value)
    {
        super.put(key, value);
    }


    @Override
    public synchronized Set<ITEM> remove(
        KEY key)
    {
        return super.remove(key);
    }


    @Override
    public Set<ITEM> makeValue(
        String valStr)
    {
        Set<ITEM> set = new HashSet<>();

        String[] itemStrs = valStr.split(SPLIT);
        if (itemStrs != null) {
            for (String itemStr : itemStrs) {
                set.add(this.makeItem(itemStr));
            }
        }
        return set;
    }


    @Override
    public String makeValueStr(
        Set<ITEM> v)
    {
        StringBuilder sb = new StringBuilder();
        for (ITEM item : v) {
            sb.append(this.makeItemStr(item));
            sb.append(SPLIT);
        }
        if (sb.length() > 0)
            sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }


    //
    // SetCache
    //

    @Override
    public synchronized boolean addItem(
        KEY key,
        ITEM item)
    {
        Set<ITEM> value = this.get(key);
        if (value == null) {
            value = new HashSet<>();
        }

        boolean result = value.add(item);
        if (result) {
            this.put(key, value);
        }
        return result;
    }


    @Override
    public synchronized boolean existItem(
        KEY key,
        ITEM item)
    {
        Set<ITEM> value = this.get(key);
        if (value == null) {
            return false;
        }
        else {
            return value.contains(item);
        }
    }


    @Override
    public synchronized boolean removeItem(
        KEY key,
        ITEM item)
    {
        Set<ITEM> value = this.get(key);
        if (value == null) {
            return false;
        }
        else {
            boolean result = value.remove(item);
            if (result) {
                this.put(key, value);
            }
            return result;
        }
    }

}
