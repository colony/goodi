
package gd.impl.cache.resident;


import gd.impl.cache.Cache;


public interface ResidentCache<KEY,VALUE>
    extends
        Cache<KEY,VALUE>
{
    /**
     * Determines if any modifications have been made to this index
     * since the last time is was written.
     */
    public boolean isDirty();
}
