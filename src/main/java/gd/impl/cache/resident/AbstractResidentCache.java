
package gd.impl.cache.resident;


import gd.impl.cache.CacheConfig;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;


public abstract class AbstractResidentCache<KEY,VALUE>
    implements
        ResidentCache<KEY,VALUE>
{
    private static final Logger LOG = LoggerFactory.getLogger(AbstractResidentCache.class);

    protected com.google.common.cache.Cache<KEY,VALUE> cache;

    protected volatile boolean dirty;


    public AbstractResidentCache()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    //
    // ResidentCache
    //

    @Override
    public boolean isDirty()
    {
        return dirty;
    }


    //
    // Cache METHODS
    //

    @Override
    public boolean add(
        KEY k,
        final VALUE v)
    {
        try
        {
            final MutableBoolean mb = new MutableBoolean(false);
            // The conventional "if cached, return; otherwise create, cache and return" pattern.
            this.cache.get(k, new Callable<VALUE>() {
                @Override
                public VALUE call()
                    throws Exception
                {
                    mb.setTrue();
                    return v;
                }
            });

            if (mb.booleanValue()) {
                dirty = true;
            }
            return mb.booleanValue();
        }
        catch (ExecutionException e)
        {
            LOG.error("Fail to add key {} val {}", k, v, e);
            return false;
        }
    }


    @Override
    public void clear()
    {
        this.cache.invalidateAll();
        this.dirty = true;
    }


    @Override
    public VALUE get(
        KEY key)
    {
        return this.cache.getIfPresent(key);
    }


    @Override
    public void init(
        CacheConfig cfg)
    {
        CacheBuilder<Object,Object> b = CacheBuilder.newBuilder();
        if (cfg.getMaxSize() > 0)
            b = b.maximumSize(cfg.getMaxSize());
        this.cache = b.recordStats().build();
        this.dirty = false;
    }


    @Override
    public void put(
        KEY key,
        VALUE value)
    {
        this.cache.put(key, value);
        this.dirty = true;
    }


    @Override
    public VALUE remove(
        KEY key)
    {
        // This might be out-of-sync, the value might be stale before we call invalidate
        // However, since the memory is blazing fast, we ignore the rare case
        VALUE v = this.cache.getIfPresent(key);
        this.cache.invalidate(key);
        if (v != null)
            this.dirty = true;
        return v;
    }
}
