
package gd.impl.cache.resident;


import gd.impl.cache.C.SSCache;


public class ResidentSSCache
    extends
        AbstractResidentCache<String,String>
    implements
        SSCache
{
    @Override
    public String makeKeyStr(
        String k)
    {
        return k;
    }


    @Override
    public String makeValueStr(
        String v)
    {
        return v;
    }


    @Override
    public String makeKey(
        String keyStr)
    {
        return keyStr;
    }


    @Override
    public String makeValue(
        String valStr)
    {
        return valStr;
    }
}
