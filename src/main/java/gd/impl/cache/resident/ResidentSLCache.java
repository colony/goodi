
package gd.impl.cache.resident;


import gd.impl.cache.C.SLCache;


public class ResidentSLCache
    extends
        AbstractResidentCache<String,Long>
    implements
        SLCache
{
    @Override
    public String makeKey(
        String keyStr)
    {
        return keyStr;
    }


    @Override
    public String makeKeyStr(
        String k)
    {
        return k;
    }


    @Override
    public Long makeValue(
        String valStr)
    {
        return Long.valueOf(valStr);
    }


    @Override
    public String makeValueStr(
        Long v)
    {
        return String.valueOf(v);
    }


    @Override
    public synchronized long incr(
        String key,
        Long incr)
    {
        Long val = this.cache.getIfPresent(key);
        if (val == null)
            val = 0L;
        val = val + incr;
        this.cache.put(key, val);
        return val;
    }
}
