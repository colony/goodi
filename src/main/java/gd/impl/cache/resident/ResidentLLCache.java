
package gd.impl.cache.resident;


import gd.impl.cache.C.LLCache;


public class ResidentLLCache
    extends
        AbstractResidentCache<Long,Long>
    implements
        LLCache
{
    @Override
    public Long makeKey(
        String keyStr)
    {
        return Long.valueOf(keyStr);
    }


    @Override
    public String makeKeyStr(
        Long k)
    {
        return String.valueOf(k);
    }


    @Override
    public Long makeValue(
        String valStr)
    {
        return Long.valueOf(valStr);
    }


    @Override
    public String makeValueStr(
        Long v)
    {
        return String.valueOf(v);
    }


    @Override
    public synchronized long incr(
        Long key,
        Long incr)
    {
        Long val = this.cache.getIfPresent(key);
        if (val == null) 
            val = 0L;
        val = val + incr;
        this.cache.put(key, val);
        return val;
    }
}
