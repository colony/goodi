
package gd.impl.cache.resident;


import gd.impl.cache.C.SSSetCache;


public class ResidentSSSetCache
    extends
        AbstractResidentSetCache<String,String>
    implements
        SSSetCache
{

    @Override
    public String makeKey(
        String keyStr)
    {
        return keyStr;
    }


    @Override
    public String makeKeyStr(
        String k)
    {
        return k;
    }


    @Override
    protected String makeItem(
        String itemStr)
    {
        return itemStr;
    }


    @Override
    protected String makeItemStr(
        String item)
    {
        return item;
    }

}
