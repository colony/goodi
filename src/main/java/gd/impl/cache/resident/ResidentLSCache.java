
package gd.impl.cache.resident;


import gd.impl.cache.C.LSCache;


public class ResidentLSCache
    extends
        AbstractResidentCache<Long,String>
    implements
        LSCache
{
    @Override
    public String makeKeyStr(
        Long k)
    {
        return String.valueOf(k);
    }


    @Override
    public String makeValueStr(
        String v)
    {
        return v;
    }


    @Override
    public Long makeKey(
        String keyStr)
    {
        return Long.valueOf(keyStr);
    }


    @Override
    public String makeValue(
        String valStr)
    {
        return valStr;
    }
}
