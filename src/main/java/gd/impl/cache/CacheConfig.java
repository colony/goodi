
package gd.impl.cache;


import gd.impl.util.GDUtils;

import com.google.common.base.Strings;


public class CacheConfig
{
    private CacheType id;

    private String implClassName;


    // Config applicable for ResidentCache

    /**
     * Max number of items allowed in cache. Size-based eviction. If
     * the cache grows beyond the size, The cache will try to evict
     * entries that haven't been used recently or very often. If the
     * value is zero or negative, that means NO eviction at all.
     */
    private int maxSize = 0;
    

    /**
     * Default constructor
     */
    public CacheConfig()
    {
    }


    public CacheConfig(
        CacheType id)
    {
        this.id = id;
    }


    public CacheType getId()
    {
        return id;
    }


    public String getImplClassName()
    {
        if (Strings.isNullOrEmpty(this.implClassName))
        {
            return id.defaultImplClass().getName();
        }
        return this.implClassName;
    }
    
    
    public int getMaxSize()
    {
        return maxSize;
    }


    public Class<? extends Cache<?,?>> implClass()
    {
        Class<?> rawClass;
        try
        {
            rawClass = Class.forName(this.getImplClassName());
            Class<? extends Cache<?,?>> implClass =
                GDUtils.cast(rawClass.asSubclass(Cache.class));
            return implClass;
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(
                "Fail to find the impl class for index " + id,
                e);
        }
    }
}
