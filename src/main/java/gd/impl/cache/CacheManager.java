
package gd.impl.cache;


import gd.impl.cache.resident.ResidentCache;
import gd.impl.config.ConfigManager;
import gd.impl.config.GlobalProperties;
import gd.impl.util.InitializationError;

import java.nio.file.Path;
import java.util.EnumMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CacheManager
{
    private static final Logger LOG = LoggerFactory.getLogger(CacheManager.class);

    protected EnumMap<CacheType,Cache<?,?>> caches;


    private CacheManager()
    {
        LOG.info("Start initialize caches");

        // Start with empty map
        this.caches = new EnumMap<>(CacheType.class);

        CachesConfig cCfg = ConfigManager.instance().get(CachesConfig.class);
        if (cCfg == null)
            throw new InitializationError("Cannot find cache configuration");

        int numLoaded = 0;
        for (CacheType id : CacheType.values())
        {
            Cache<?,?> cache = initCache(cCfg.get(id));
            this.caches.put(id, cache);
            ++numLoaded;
        }

        LOG.info("Complete initialize {} caches", numLoaded);
    }


    public <T> T get(
        CacheType id,
        Class<T> clazz)
    {
        T result;

        Cache<?,?> obj = this.caches.get(id);
        if (obj == null)
        {
            throw new RuntimeException("Cache \"" + id + "\" doesn't exist");
        }
        else
        {
            result = clazz.cast(obj);
        }

        return result;
    }


    private Cache<?,?> initCache(
        CacheConfig cfg)
    {
        Cache<?,?> cache = null;

        // Initialize based on cache type
        if (ResidentCache.class.isAssignableFrom(cfg.implClass()))
        {
            try
            {
                Cache<?,?> implObj = cfg.implClass().newInstance();
                implObj.init(cfg);
                cache = implObj;
            }
            catch (Exception e)
            {
                throw new InitializationError("Fail to create instance of cache " + cfg.getId(), e);
            }
        }
        // Other type goes here

        return cache;
    }


    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final CacheManager INSTANCE = new CacheManager();
    }


    public static CacheManager instance()
    {
        return SingletonHolder.INSTANCE;
    }


    //
    // PUBLIC STATIC INTERFACE
    //

    public static Path getDir(
        CacheType id)
    {
        GlobalProperties gp = GlobalProperties.instance();
        return gp.cacheDir().resolve(id.abbreviation());
    }
}
