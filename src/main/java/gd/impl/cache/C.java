
package gd.impl.cache;


import java.util.Set;


/**
 * Group of Cache interface and related class
 */
public final class C
{
    public static interface LLCache
        extends
            Cache<Long,Long>
    {
        /**
         * Increment the value of the specified key by the specified
         * amount. If the key doesn't exist,it is set to 0 before
         * performing the operation
         * 
         * @return The incremented value
         */
        public long incr(
            Long key,
            Long incr);
    }



    public static interface LSCache
        extends
            Cache<Long,String>
    {
    }



    public static interface SLCache
        extends
            Cache<String,Long>
    {
        /**
         * Increment the value of the specified key by the specified
         * amount. If the key doesn't exist,it is set to 0 before
         * performing the operation
         * 
         * @return The incremented value
         */
        public long incr(
            String key,
            Long incr);
    }



    public static interface SSCache
        extends
            Cache<String,String>
    {
    }



    public static interface SSSetCache
        extends
            SetCache<String,String,Set<String>>
    {
    }
}
