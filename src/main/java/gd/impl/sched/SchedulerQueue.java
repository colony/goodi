
package gd.impl.sched;


import java.util.List;


public interface SchedulerQueue
{
    /**
     * Add a new job info into queue. If a job with same ID already
     * in queue, a RuntimeException will be thrown
     * 
     * @param info
     *            The job to be added
     */
    public void addJob(
        ScheduledJobInfo info);


    /**
     * Cancel the current running job if any
     */
    public void cancelCurrentJob();


    /**
     * Generate a live summary about the queue
     * 
     * @return A job queue summary
     */
    public String generateReport();


    /**
     * Retrieve all job infos that have been assigned in the queue
     * 
     * @return A collection of job infos
     */
    public List<ScheduledJobInfo> getAllJobs();


    /**
     * Retrieve the info regarding with current running job
     * 
     * @return The job info to be returned
     */
    public ScheduledJobInfo getCurrentJob();


    /**
     * Return the last time (ms since epoch) when inner state of
     * the queue has been modified
     */
    public long getLastModTime();


    /**
     * Return the name of queue
     * 
     * @return The name of queue
     */
    public String getName();


    /**
     * Remove a job from queue by the specified ID
     * 
     * @param jobId
     *            ID of job
     * @return The removed job info. Null if the queue doesn't have
     *         it
     * @throws RuntimException
     *             If the removed job is currently running
     */
    public ScheduledJobInfo removeJob(
        String jobId);


    /**
     * Star the queue
     */
    public void start();


    /**
     * Stop the queue
     */
    public void stop();


    /**
     * Update a job with new schedule. If the queue doesn't have the
     * job, it takes no effect
     * 
     * @param jobId
     *            ID of job
     * @param sched
     *            The scheduled to be updated with
     */
    public void updateJobSchedule(
        String jobId,
        Schedule sched);
}
