
package gd.impl.sched;


import gd.api.v1.admin.ManageSchedulerResource;
import gd.impl.Rests;
import gd.impl.util.GDUtils;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;


@Path("/adm/sched")
@Singleton
public class ManageSchedulerResourceImpl
    implements
        ManageSchedulerResource
{
    protected Scheduler scheduler;


    public ManageSchedulerResourceImpl()
    {
        this.scheduler = Scheduler.instance();
    }


    @Override
    @Path("/queues/cancel")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response cancelRunJob(
        @Context HttpHeaders headers,
        @FormParam("q") String queueName)
    {
        Rests.beginAdminOperation(headers, "adm:msched:cancel:" + queueName);
        
        GDUtils.check(
            (StringUtils.isNotBlank(queueName)),
            Status.BAD_REQUEST,
            "Blank queue name not allowed");
        
        SchedulerQueue q = this.scheduler.getQueue(queueName);
        GDUtils.check(
            q != null,
            Status.BAD_REQUEST,
            "Cannot find queue with name %s",
            queueName);

        q.cancelCurrentJob();
        return Response.ok().build();
    }


    @Override
    @Path("/queues")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response createQueue(
        @Context HttpHeaders headers,
        @FormParam("q") String queueName)
    {
        Rests.beginAdminOperation(headers, "adm:msched:createQ:" + queueName);
        
        GDUtils.check(
            (StringUtils.isNotBlank(queueName)),
            Status.BAD_REQUEST,
            "Blank queue name not allowed");
        
        this.scheduler.addJobQueue(queueName);
        return Response.ok().build();
    }


    @Override
    @Path("/queues/{q}")
    @DELETE
    @Produces("text/plain")
    public Response deleteQueue(
        @Context HttpHeaders headers,
        @PathParam("q") String queueName)
    {
        Rests.beginAdminOperation(headers, "adm:msched:deleteQ:" + queueName);
        
        GDUtils.check(
            (StringUtils.isNotBlank(queueName)),
            Status.BAD_REQUEST,
            "Blank queue name not allowed");
        
        this.scheduler.deleteJobQueue(queueName);
        return Response.ok().build();
    }


    @Override
    @Path("/queues/{q}")
    @GET
    @Produces("text/plain")
    public Response getQueueReport(
        @Context HttpHeaders headers,
        @PathParam("q") String queueName)
    {
        Rests.beginAdminOperation(headers, "adm:msched:getQReport:" + queueName);
        SchedulerQueue q = this.scheduler.getQueue(queueName);
        GDUtils.check(
            q != null,
            Status.NOT_FOUND,
            "Queue name %s doesn't exist",
            queueName);

        return Response.ok(q.generateReport()).build();
    }


    @Override
    @GET
    @Produces("text/plain")
    public Response getSchedulerSummary(
        @Context HttpHeaders headers)
    {
        Rests.beginAdminOperation(headers, "adm:msched:getSummary");
        return Response.ok(this.scheduler.generateSummary())
            .build();
    }


    @Override
    @Path("/jobs")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response updateJobSchedule(
        @Context HttpHeaders headers,
        @FormParam("id") String jobId,
        @FormParam("q") String queueName,
        @FormParam("sched") String schedStr)
    {
        Rests.beginAdminOperation(headers, "adm:msched:updateSched");
        GDUtils.check(
            StringUtils.isNotBlank(jobId) && StringUtils.isNotBlank(queueName),
            Status.BAD_REQUEST,
            "Blank job id or queueName is not allowed");

        if (StringUtils.isBlank(schedStr))
        {
            // A blank schedule string means to UNSCHEDULE the job
            this.scheduler.unscheduleJob(jobId, queueName);
        }
        else
        {
            this.scheduler.scheduleJob(
                jobId,
                queueName,
                new Schedule().fromStr(schedStr));
        }

        return Response.ok().build();
    }
}
