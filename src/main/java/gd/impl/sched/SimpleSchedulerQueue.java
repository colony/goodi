
package gd.impl.sched;


import gd.impl.sched.ScheduledJobInfo.Status;
import gd.impl.util.CancelledException;
import gd.impl.util.GDUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;


public class SimpleSchedulerQueue
    implements
        SchedulerQueue
{

    /**
     * Amount of time to wait each time we cancel a job
     */
    public static final long CANCEL_WAIT = 3 * 1000;

    private static final Logger LOG =
        LoggerFactory.getLogger(SimpleSchedulerQueue.class);

    /**
     * The maximum amount of time the worker wait (in ms) before
     * looking for work to do
     */
    public static final long MAX_WAIT = 30 * 1000;

    public static final char NL = '\n';

    public static final String TAB = "    ";


    private boolean active;

    private long activeTime;

    private JobEntry current;
    
    private Map<String,JobEntry> idToEntry;
    
    private long modTime;

    private String name;

    private PriorityQueue<JobEntry> queue;

    private Worker worker;

    
    public SimpleSchedulerQueue(String name)
    {
        this(name, Collections.<ScheduledJobInfo>emptyList());
    }
    

    public SimpleSchedulerQueue(
        String name,
        List<ScheduledJobInfo> infos)
    {
        this.name = name;
        
        this.idToEntry = Maps.newHashMap();
        this.queue = new PriorityQueue<>();
        for (ScheduledJobInfo info : infos) {
            this.enqueueJob(info);
        }
        this.modTime = 0L; // Initially, modTime is zero
        
        // Spin up worker thread
        this.worker = new Worker();
        this.worker.setName("JobQueue-" + name);

        // Inactive. Either stopped or not started yet
        this.active = false;
    }


    @Override
    public synchronized void addJob(
        ScheduledJobInfo info)
    {
        if (this.idToEntry.containsKey(info.getId()))
        {
            throw new RuntimeException(
                String.format(
                    "Fail to add job \"%s\" to queue \"%s\", a job with same ID already exists",
                    info.getId(),
                    this.name));
        }

        info.setQueueName(this.name);
        enqueueJob(info);
    }


    @Override
    public synchronized void cancelCurrentJob()
    {
        if (this.current == null)
        {
            // Nothing to do
            LOG.warn(
                "Nothing current job to cancel in job queue {}",
                this.name);
            return;
        }

        synchronized (this.current)
        {
            ScheduledJob cur = this.current.job;
            cur.cancel();
            if (cur.isRunning())
            {
                try
                {
                    this.current.wait(CANCEL_WAIT);
                }
                catch (InterruptedException e)
                {
                    // Something bad happens, abort the waiting and log it
                    LOG.error(
                        "Interrupted while we're waiting for job {} to complete in queue {}",
                        cur.getId(),
                        this.name,
                        e);
                }
            }
        }
    }


    @Override
    public synchronized String generateReport()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(
            "Active: %s%n",
            GDUtils.printDuration(
                this.activeTime,
                System.currentTimeMillis())));
        sb.append(String.format("Name: %s%n", this.name));
        sb.append(String.format(
            "Num of jobs: %d%n",
            this.idToEntry.size()));

        sb.append(NL);
        if (this.idToEntry.isEmpty())
        {
            sb.append(TAB + "<empty>");
        }
        else
        {
            if (this.current != null)
            {
                ScheduledJobInfo info = this.current.info;
                sb.append(String.format(
                    "*%s [%s]%n",
                    info.getId(),
                    info.getClassName()));
                sb.append(String.format(
                    "%sSched: %s%n",
                    TAB,
                    info.getSched().toStr()));
                sb.append(String.format(
                    "%sLast start: %s%n",
                    TAB,
                    info.printLastStartTime()));
                sb.append(String.format(
                    "%sLast end: %s%n",
                    TAB,
                    info.printLastEndTime()));
                sb.append(String.format(
                    "%sLast duration: %s%n",
                    TAB,
                    info.printLastDuration()));
                sb.append(String.format(
                    "%sLast status: %s%n",
                    TAB,
                    info.printLastStatus()));
                sb.append(NL);
            }

            for (JobEntry e : this.queue)
            {
                ScheduledJobInfo info = e.info;
                sb.append(String.format(
                    "%s [%s]%n",
                    info.getId(),
                    info.getClassName()));
                sb.append(String.format(
                    "%sSched: %s%n",
                    TAB,
                    info.getSched().toStr()));
                sb.append(String.format(
                    "%sLast start: %s%n",
                    TAB,
                    info.printLastStartTime()));
                sb.append(String.format(
                    "%sLast end: %s%n",
                    TAB,
                    info.printLastEndTime()));
                sb.append(String.format(
                    "%sLast duration: %s%n",
                    TAB,
                    info.printLastDuration()));
                sb.append(String.format(
                    "%sLast status: %s%n",
                    TAB,
                    info.printLastStatus()));
                sb.append(String.format(
                    "%sNext start: %s%n",
                    TAB,
                    GDUtils.printDateTime(e.nextFireTime)));
                sb.append(NL);
            }
        }

        return sb.toString();
    }


    @Override
    public synchronized List<ScheduledJobInfo> getAllJobs()
    {
        List<ScheduledJobInfo> result = new ArrayList<>();
        if (this.current != null)
            result.add(new ScheduledJobInfo(this.current.info));

        // Follow the exact order in queue
        for (JobEntry e : queue)
        {
            // Copy instance for safe modification outside
            result.add(new ScheduledJobInfo(e.info));
        }
        return result;
    }


    @Override
    public synchronized ScheduledJobInfo getCurrentJob()
    {
        if (this.current == null)
            return null;

        // Copy instance for safe modification outside
        return new ScheduledJobInfo(this.current.info);
    }

    
    @Override
    public long getLastModTime()
    {
        return modTime;
    }
    

    @Override
    public String getName()
    {
        return name;
    }
    

    @Override
    public synchronized ScheduledJobInfo removeJob(
        String jobId)
    {
        if (this.current != null
            && StringUtils.equals(this.current.info.getId(), jobId))
        {
            // Current running job is unremovable
            throw new RuntimeException("Fail to remove job ID \""
                + jobId
                + "\" from queue \""
                + this.name
                + "\" that is current running");
        }

        JobEntry e = this.idToEntry.remove(jobId);
        if (e != null)
        {
            this.queue.remove(e);
            this.modTime = System.currentTimeMillis();
            return e.info;
        }
        return null;
    }


    @Override
    public synchronized void start()
    {
        if (this.active)
        {
            LOG.error(
                "Ignoring start, job queue {} is already active",
                this.name);
            return;
        }

        this.active = true;
        this.activeTime = System.currentTimeMillis();
        this.worker.start();
        LOG.info("Job queue {} is now active", this.name);
    }


    @Override
    public void stop()
    {
        synchronized (this)
        {

            if (!this.active)
            {
                LOG.error(
                    "Ignoring stop, job queue {} is already inactive",
                    this.name);
                return;
            }

            LOG.info("Job queue {} is stopping...");
            this.active = false;

            // Tell the worker thread to shut down. Note that this will
            // NOT affect the current task if one is actually running,
            // but it will prevent a new task from starting.
            this.worker.shutdown();

            // Carefully cancel the current task (if any), but don't
            // wait for it to complete just yet
            if (this.current != null)
            {
                LOG.info(
                    "Cancelling current task on {} job queue..."
                    , this.name);

                this.current.job.cancel();
            }
        }

        // Wait for the worker thread to terminate
        while (this.worker.isAlive())
        {
            LOG.debug(
                "Waiting for worker thread of {} job qeue to terminate...",
                this.name);

            try
            {
                this.worker.join(3000);
            }
            catch (InterruptedException e)
            {
                LOG.debug(
                    "Interrupted while waiting for worker thread of {} job queue to terminate",
                    this.name);
            }
        }

        LOG.info("Job queue {} has stopped", this.name);
    }


    @Override
    public synchronized void updateJobSchedule(
        String jobId,
        Schedule sched)
    {
        JobEntry e = this.idToEntry.get(jobId);
        if (e == null)
        {
            // The job is not found, do nothing
            return;
        }

        if (this.current == e)
        {
            // The task is now running, so we simply update the task
            // itself and let the worker thread requeue when it is
            // finished running
            e.info.setSched(sched);
            
            // Refresh the modTime here
            // No worry about else case, enqueueJob will update modTime too
            this.modTime = System.currentTimeMillis();
        }
        else
        {
            // The task is pending. Drop it and requeue it with
            // updated schedule
            this.queue.remove(e);
            e.info.setSched(sched);

            // addJob will also persist the update
            this.enqueueJob(e.info);
        }
    }


    //
    // INTERNAL METHODS 
    //


    private void enqueueJob(
        ScheduledJobInfo info)
    {
        JobEntry e = new JobEntry(info);
        this.queue.add(e);
        this.idToEntry.put(info.getId(), e);
        this.modTime = System.currentTimeMillis();
    }


    //
    // INTERNAL CLASSES
    //

    private class JobEntry
        implements
            Comparable<JobEntry>
    {
        ScheduledJobInfo info;

        ScheduledJob job;

        long nextFireTime;


        JobEntry(
            ScheduledJobInfo info)
        {
            this.info = info;
            // Create a job instance on-the-fly everytime enqueue
            this.job = GDUtils.getInstance(info.getClassName(), ScheduledJob.class);
            this.nextFireTime = info.getNextFireTime();
        }


        @Override
        public int compareTo(
            JobEntry o)
        {
            // Compare next fire time
            if (this.nextFireTime != o.nextFireTime)
            {
                return Long.compare(
                    this.nextFireTime,
                    o.nextFireTime);
            }

            // Then, compare job ID
            return this.info.getId().compareTo(o.info.getId());
        }


        @Override
        public boolean equals(
            Object obj)
        {
            JobEntry other = (JobEntry) obj;
            return StringUtils.equals(
                info.getId(),
                other.info.getId());
        }


        @Override
        public int hashCode()
        {
            return info.getId().hashCode();
        }
    }



    private class Worker
        extends
            Thread
    {
        boolean stopped;

        boolean waiting;


        Worker()
        {
            this.stopped = false;
            this.waiting = false;
        }


        public long elect()
        {
            // Hold an monitor on queue since we might modify
            // the internal state of it
            synchronized (SimpleSchedulerQueue.this)
            {
                SimpleSchedulerQueue q = SimpleSchedulerQueue.this;
                if (q.queue.isEmpty())
                {
                    // No job in queue, wait maximum
                    return MAX_WAIT;
                }

                JobEntry head = q.queue.peek();
                long now = System.currentTimeMillis();
                if (now < head.nextFireTime)
                {
                    return Math.min(
                        (head.nextFireTime - now),
                        MAX_WAIT);
                }

                q.current = q.queue.poll();
                return 0L;
            }
        }


        public void finish()
        {
            // Hold an monitor on queue since we might modify
            // the internal state of it
            synchronized (SimpleSchedulerQueue.this)
            {
                SimpleSchedulerQueue q = SimpleSchedulerQueue.this;
                JobEntry e = q.current;

                // Re-enqueue the job, this essentially re-compute
                // the next fire time
                q.enqueueJob(e.info);

                // Nullify current because it has done
                q.current = null;

                // Poke threads waiting on current task
                synchronized (e)
                {
                    e.notifyAll();
                }
            }
        }


        @Override
        public void run()
        {
            while (true)
            {
                synchronized (this)
                {
                    if (this.stopped)
                    {
                        break;
                    }

                    long waitTime = this.elect();
                    if (waitTime > 0L)
                    {
                        this.waiting = true;
                        try
                        {
                            this.wait(waitTime);
                        }
                        catch (InterruptedException e)
                        {
                            LOG.info(
                                "Worker for job queue {} interrupted",
                                SimpleSchedulerQueue.this.name);
                        }
                        finally
                        {
                            this.waiting = false;
                        }

                        // Back to the top of loop
                        continue;
                    }
                }

                // If we make here, we have a job to run
                JobEntry cur = SimpleSchedulerQueue.this.current;

                // Record start time
                cur.info.setLastStartTime(System.currentTimeMillis());
                try
                {
                    cur.job.execute();
                    cur.info.setLastStatus(Status.SUCCESS);
                }
                catch (CancelledException e)
                {
                    LOG.error(
                        "Current running job {} is cancelled",
                        cur.job.getId(),
                        e);
                    cur.info.setLastStatus(Status.CANCEL);
                }
                catch (Throwable t)
                {
                    LOG.error(
                        "Error occurs running job {}",
                        cur.job.getId(),
                        t);
                    cur.info.setLastStatus(Status.FAIL);
                }
                finally
                {
                    // Record end time
                    long t = System.currentTimeMillis();
                    cur.info.setLastDuration(t
                        - cur.info.getLastStartTime());
                    cur.info.setLastEndTime(t);

                    finish();
                }
            }
        }


        public synchronized void shutdown()
        {
            // Change status to STOPPED
            this.stopped = true;

            // Only wakeup the woker thread while it is waiting for
            // next job to run. If it is in the middle of running,
            // the running job can be cancelled by JobQueue
            if (this.waiting)
            {
                notifyAll();
            }
        }
    }
}
