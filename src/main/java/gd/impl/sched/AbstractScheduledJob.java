
package gd.impl.sched;


import gd.impl.util.CancelledException;


public abstract class AbstractScheduledJob
    implements
        ScheduledJob
{
    protected volatile boolean cancelled = false;

    protected volatile boolean running = false;


    @Override
    public void cancel()
    {
        this.cancelled = true;
    }


    @Override
    public final void execute()
        throws CancelledException
    {
        this.running = true;
        try
        {
            this.executeJob();
        }
        finally
        {
            this.running = false;
        }
    }


    @Override
    public final boolean isRunning()
    {
        return running;
    }


    protected abstract void executeJob()
        throws CancelledException;
}
