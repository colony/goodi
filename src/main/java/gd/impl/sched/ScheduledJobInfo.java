
package gd.impl.sched;


import gd.impl.util.GDUtils;

import java.util.Date;

import org.joda.time.Duration;
import org.joda.time.format.PeriodFormat;


/**
 * A data structure holds all information (i.e. queue, schedule, job
 * implementation, etc) regarding with a job instance.
 */
public class ScheduledJobInfo
{
    private String className;

    private String id;

    private Long lastDuration;

    private Long lastEndTime;

    private Long lastStartTime;

    private Status lastStatus;

    private String queueName;

    private Schedule sched;


    public ScheduledJobInfo()
    {
    }


    /**
     * Copy constructor
     */
    public ScheduledJobInfo(
        ScheduledJobInfo ji)
    {
        this.className = ji.className;
        this.id = ji.id;
        this.lastDuration = ji.lastDuration;
        this.lastEndTime = ji.lastEndTime;
        this.lastStartTime = ji.lastStartTime;
        this.lastStatus = ji.lastStatus;
        this.queueName = ji.queueName;
        this.sched = ji.sched;
    }


    public long getNextFireTime()
    {
        if (this.sched == null)
            throw new IllegalArgumentException(
                "Fail to compute next fire time because schedule is null");

        Date d;
        if (this.lastEndTime == null)
        {
            d = new Date();
        }
        else
        {
            d = new Date(this.lastEndTime);
        }

        Date nextDate = this.sched.getNextValidTimeAfter(d);
        return nextDate.getTime();
    }


    public String printLastEndTime()
    {
        if (this.lastEndTime == null)
            return "";
        return GDUtils.printDateTime(this.lastEndTime);
    }


    public String printLastStartTime()
    {
        if (this.lastStartTime == null)
            return "";
        return GDUtils.printDateTime(this.lastStartTime);
    }


    public String printLastDuration()
    {
        if (this.lastDuration == null)
            return "";
        return PeriodFormat.getDefault().print(
            new Duration(this.lastDuration).toPeriod());
    }


    public String printLastStatus()
    {
        if (this.lastStatus == null)
            return "";

        return this.lastStatus.name();
    }


    //
    // Getters & Setters
    //


    public String getId()
    {
        return id;
    }


    public void setId(
        String id)
    {
        this.id = id;
    }


    public String getClassName()
    {
        return className;
    }


    public void setClassName(
        String className)
    {
        this.className = className;
    }


    public Long getLastDuration()
    {
        return lastDuration;
    }


    public void setLastDuration(
        Long lastDuration)
    {
        this.lastDuration = lastDuration;
    }


    public Long getLastEndTime()
    {
        return lastEndTime;
    }


    public void setLastEndTime(
        Long lastEndTime)
    {
        this.lastEndTime = lastEndTime;
    }


    public Long getLastStartTime()
    {
        return lastStartTime;
    }


    public void setLastStartTime(
        Long lastStartTime)
    {
        this.lastStartTime = lastStartTime;
    }


    public Status getLastStatus()
    {
        return lastStatus;
    }


    public void setLastStatus(
        Status lastStatus)
    {
        this.lastStatus = lastStatus;
    }


    public String getQueueName()
    {
        return queueName;
    }


    public void setQueueName(
        String queueName)
    {
        this.queueName = queueName;
    }


    public Schedule getSched()
    {
        return sched;
    }


    public void setSched(
        Schedule sched)
    {
        this.sched = sched;
    }



    public static enum Status
    {
        CANCEL,
        FAIL,
        SUCCESS,
    }
}
