package gd.impl.sched;

import gd.impl.util.CancelledException;

public class Job2 extends AbstractScheduledJob
{

    @Override
    public String getId()
    {
        return "JOB2";
    }

    @Override
    protected void executeJob() throws CancelledException
    {
        System.out.println("Start executing JOB2");
        
        long now = System.currentTimeMillis();
        long interval = 60 * 2 * 1000;
        
        long end = now + interval;
        while (System.currentTimeMillis() < end)
        {
            if (this.cancelled)
            {
                throw new CancelledException("JOB2 cancelled");
            }
            
            try
            {
                Thread.sleep(10 * 1000);
                System.out.println("JOB2 progress: " + (end - System.currentTimeMillis()));
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

}
