
package gd.impl.sched;


import gd.impl.util.CancelledException;


public interface ScheduledJob
{
    public void cancel();


    public void execute()
        throws CancelledException;


    public String getId();


    public boolean isRunning();
}
