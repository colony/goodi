
package gd.impl.sched;


import gd.impl.util.CancelledException;


public class Job1
    extends
        AbstractScheduledJob
{
    @Override
    public String getId()
    {
        return "JOB1";
    }


    @Override
    protected void executeJob()
        throws CancelledException
    {
        System.out.println("Start executing JOB1");

        long now = System.currentTimeMillis();
        long interval = 60 * 1 * 1000;

        long end = now + interval;
        while (System.currentTimeMillis() < end)
        {
            if (this.cancelled)
            {
                throw new CancelledException("JOB1 cancelled");
            }

            try
            {
                Thread.sleep(10 * 1000);
                System.out.println("JOB1 progress: " + (end - System.currentTimeMillis()));
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
