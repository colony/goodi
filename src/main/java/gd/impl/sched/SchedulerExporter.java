
package gd.impl.sched;


import gd.impl.sched.ScheduledJobInfo.Status;
import gd.support.protobuf.MiscProtos.JobInfoProto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import com.google.common.collect.AbstractIterator;


public class SchedulerExporter
{
    public static Iterator<ScheduledJobInfo> read(
        InputStream in)

        throws IOException
    {
        return new InternalIterator(in);
    }


    public static void write(
        OutputStream out,
        Iterator<ScheduledJobInfo> it)

        throws IOException
    {
        JobInfoProto.Builder b = JobInfoProto.newBuilder();
        while (it.hasNext())
        {
            ScheduledJobInfo info = it.next();

            b.setClassName(info.getClassName())
                .setId(info.getId())
                .setQueueName(info.getQueueName())
                .setSched(info.getSched().toStr());
            if (info.getLastDuration() != null)
                b.setLastDuration(info.getLastDuration());
            if (info.getLastEndTime() != null)
                b.setLastEndTime(info.getLastEndTime());
            if (info.getLastStartTime() != null)
                b.setLastStartTime(info.getLastStartTime());
            if (info.getLastStatus() != null)
                b.setLastStatus(info.getLastStatus().name());

            b.build().writeDelimitedTo(out);
            // Clear inner state of builder before next loop
            b.clear();
        }
    }



    static class InternalIterator
        extends
            AbstractIterator<ScheduledJobInfo>
    {
        JobInfoProto.Builder b;

        InputStream in;


        public InternalIterator(
            InputStream in)
        {
            this.in = in;
            this.b = JobInfoProto.newBuilder();
        }


        @Override
        protected ScheduledJobInfo computeNext()
        {
            boolean success;
            try
            {
                success = b.mergeDelimitedFrom(in);
                if (!success)
                {
                    // EOF or some other reason
                    return endOfData();
                }

                // Deserialize using Google protobuf
                JobInfoProto jip = b.build();
                ScheduledJobInfo info = new ScheduledJobInfo();
                info.setClassName(jip.getClassName());
                info.setId(jip.getId());
                if (jip.hasLastDuration())
                    info.setLastDuration(jip.getLastDuration());
                if (jip.hasLastEndTime())
                    info.setLastEndTime(jip.getLastEndTime());
                if (jip.hasLastStartTime())
                    info.setLastStartTime(jip.getLastStartTime());
                if (jip.hasLastStatus())
                    info.setLastStatus(Status.valueOf(jip.getLastStatus()));
                info.setQueueName(jip.getQueueName());
                info.setSched(new Schedule().fromStr(jip.getSched()));

                // Clean inner state of builder before next mergeFrom
                b.clear();
                return info;
            }
            catch (IOException e)
            {
                throw new RuntimeException("Error occurs when importing the data: " + e.getMessage(), e);
            }
        }
    }

}
