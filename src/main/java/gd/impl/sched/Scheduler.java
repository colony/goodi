
package gd.impl.sched;


import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.impl.ComponentConfig;
import gd.impl.ComponentConfig.SchedulerConfig;
import gd.impl.config.ConfigManager;
import gd.impl.util.GDUtils;
import gd.impl.util.InitializationError;
import gd.impl.util.Managed;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;


public class Scheduler
    implements
        Managed
{
    private static final Logger LOG =
        LoggerFactory.getLogger(Scheduler.class);

    public static final String TAB = "    ";

    
    private transient boolean dirty;
    
    private long flushInterval;
    
    private ScheduledExecutorService flushThread;

    /**
     * A map of registered jobId to jobClassName in scheduler
     * config.
     */
    private BiMap<String,String> idToClass;

    /**
     * A map of jobId to a queue that the job is assigned on
     */
    private Map<String,String> idToQueue;

    /**
     * The persist path of all infos
     */
    private Path path;

    /**
     * A map of name to queues
     */
    private Map<String,SchedulerQueue> queues;
    
    /**
     * A map of queue name to last modification time of it
     */
    private Map<String,Long> queueModTime;

    private TimeZone schedTimeZone;


    /**
     * A set of unscheduled jobs, a job is unscheduled if it is not
     * assigned on any queue
     */
    private Set<String> unscheduledJobIds;


    private Scheduler()
    {
        LOG.info("Start initialize scheduler");

        // Grab the configuration
        ComponentConfig compConfig =
            ConfigManager.instance().get(ComponentConfig.class);
        SchedulerConfig schedConfig = compConfig.getScheduler();

        this.dirty = false;
        this.schedTimeZone = TimeZone.getTimeZone(schedConfig.getTimeZoneId());
        this.path = schedConfig.getPersistPath();
        this.flushInterval = schedConfig.getFlushInterval();
        
        this.idToClass = HashBiMap.create();
        this.idToQueue = new HashMap<>();
        this.queues = new HashMap<>();
        this.queueModTime = new HashMap<>();
        this.unscheduledJobIds = new HashSet<>();

        // Register job class with job id
        Set<String> registeredJobClasses =
            new HashSet<String>(schedConfig.getRegisterJobClasses());
        for (String jobClassName : registeredJobClasses)
        {
            Class<? extends ScheduledJob> rawClass = GDUtils.getClass(jobClassName, ScheduledJob.class);
            try
            {
                ScheduledJob j = rawClass.newInstance();
                this.idToClass.put(j.getId(), jobClassName);
            }
            catch (Exception e)
            {
                throw new InitializationError(
                    "Fail to register job class \"" + jobClassName
                        + "\"",
                    e);
            }
        }

        // Import data
        List<ScheduledJobInfo> infos;
        try {
            infos = this.importData();
        }
        catch (IOException e) {
            throw new InitializationError("Fail to import scheduler data", e);
        }
        
        Multimap<String, ScheduledJobInfo> queueToInfo = HashMultimap.create();
        Set<String> unseen = new HashSet<>(registeredJobClasses);
        // We need to check against our configuration to see if we no longer
        // registers a specific job and we remove it
        for (Iterator<ScheduledJobInfo> it = infos.iterator(); it.hasNext();) {
            ScheduledJobInfo info = it.next();
            String className = info.getClassName();
            if (!registeredJobClasses.contains(className)) {
                LOG.info("Unregister job {}", info.getClassName());
                it.remove();
            } else {
                queueToInfo.put(info.getQueueName(), info);
                this.idToQueue.put(info.getId(), info.getQueueName());
            }
            unseen.remove(className);
        }
        
        for (String name : queueToInfo.keys()) {
            SchedulerQueue queue = new SimpleSchedulerQueue(name, new ArrayList<>(queueToInfo.get(name)));
            this.queues.put(name, queue);
            this.queueModTime.put(name, 0L);
        }

        // Left over are unscheduled jobs
        for (String unseenJobClass : unseen)
        {
            this.unscheduledJobIds.add(this.idToClass.inverse().get(unseenJobClass));
        }

        LOG.info("Complete initialize scheduler");
    }


    /**
     * Create an empty job queue. Note that, the queue info won't be
     * persist by JobStore until there's at least one assigned job
     */
    public synchronized void addJobQueue(
        String queueName)
    {
        if (this.queues.containsKey(queueName))
        {
            throw new RuntimeException("Job queue \"" + queueName
                + "\" already exist");
        }

        SchedulerQueue q = new SimpleSchedulerQueue(queueName); // Empty list
        this.queues.put(queueName, q);
        this.queueModTime.put(queueName, 0L);

        // Spin up the queue, the underlying worker thread will wake up
        q.start();
        this.dirty = true;
    }


    public synchronized void deleteJobQueue(
        String queueName)
    {
        SchedulerQueue q = this.queues.get(queueName);
        if (q != null)
        {
            LOG.info("Start delete job queue {}", queueName);

            // Stop the queue and cancel its current running job if any
            q.stop();

            // Remove from catalog
            List<ScheduledJobInfo> infos = q.getAllJobs();
            for (ScheduledJobInfo info : infos) {
                this.idToQueue.remove(info.getId());
                this.unscheduledJobIds.add(info.getId());
            }

            // Remove all data associated with the queue
            this.queues.remove(queueName);
            this.queueModTime.remove(queueName);
            this.dirty = true;
            
            LOG.info("Complete delete job queue {}", queueName);
        }
    }


    /**
     * Generate a summary about all registered jobs. The summary
     * describes a list of unscheduled jobs as well as the scheduled
     * ones with corresponding queues
     */
    public String generateSummary()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Queues:\n");
        for (SchedulerQueue q : this.queues.values())
        {
            List<ScheduledJobInfo> jobs = q.getAllJobs();
            sb.append(String.format(
                "%s%s (%d jobs):%n",
                TAB,
                q.getName(),
                jobs.size()));

            if (!jobs.isEmpty())
            {
                List<String> ids = new ArrayList<>();
                for (ScheduledJobInfo ji : jobs)
                    ids.add(ji.getId());
                sb.append(String.format(
                    "%s%sScheduled jobs: %s%n",
                    TAB,
                    TAB,
                    StringUtils.join(ids, ',')));
            }
        }

        sb.append("\nScheduled jobs:\n");
        // Sort from lowest ID to highest
        List<String> ids = new ArrayList<>(this.idToQueue.keySet());
        Collections.sort(ids);
        for (String id : ids)
        {
            sb.append(String.format(
                "%s%s [%s]%n",
                TAB,
                id,
                this.idToClass.get(id)));
            sb.append(String.format(
                "%s%sOn queue: %s%n",
                TAB,
                TAB,
                this.idToQueue.get(id)));
        }

        sb.append("\nUnscheduled jobs:\n");
        ids = new ArrayList<>(this.unscheduledJobIds);
        Collections.sort(ids);
        for (String id : ids)
        {
            sb.append(String.format(
                "%s%s [%s]%n",
                TAB,
                id,
                this.idToClass.get(id)));
        }

        return sb.toString();
    }


    public SchedulerQueue getQueue(
        String queueName)
    {
        return queues.get(queueName);
    }


    public Collection<SchedulerQueue> getQueues()
    {
        return queues.values();
    }


    public synchronized void scheduleJob(
        String jobId,
        String queueName,
        Schedule sched)
    {
        if (!this.idToClass.containsKey(jobId))
        {
            throw new RuntimeException("Unregistered job ID \"" + jobId + "\"");
        }

        if (StringUtils.isEmpty(queueName))
        {
            throw new RuntimeException("Cannot find job \"" + jobId
                + "\" on queue \"" + queueName + "\"");
        }

        // Set the timezone for schedule
        sched.setTimeZone(this.schedTimeZone);
        
        if (this.idToQueue.containsKey(jobId)) {
            String existQueueName = this.idToQueue.get(jobId);
            if (queueName.equals(existQueueName)) {
                // Apparently, we update the schedule on jobId
                SchedulerQueue oldQ = this.queues.get(queueName);
                oldQ.updateJobSchedule(jobId, sched);
            } else {
                // Apparently, we move the job from one queue to another
                SchedulerQueue oldQ = this.queues.get(queueName);
                oldQ.removeJob(jobId);
                
                if (!this.queues.containsKey(queueName))
                    addJobQueue(queueName);
                SchedulerQueue newQ = this.queues.get(queueName);
                ScheduledJobInfo info = new ScheduledJobInfo();
                info.setClassName(this.idToClass.get(jobId));
                info.setId(jobId);
                info.setQueueName(queueName);
                info.setSched(sched);
                newQ.addJob(info);

                this.unscheduledJobIds.remove(jobId);
                this.idToQueue.put(jobId, queueName);
            }
        } else {
            // Apparently, the jobId has not been scheduled
            if (!this.queues.containsKey(queueName))
                addJobQueue(queueName);
            SchedulerQueue newQ = this.queues.get(queueName);
            ScheduledJobInfo info = new ScheduledJobInfo();
            info.setClassName(this.idToClass.get(jobId));
            info.setId(jobId);
            info.setQueueName(queueName);
            info.setSched(sched);
            newQ.addJob(info);

            this.unscheduledJobIds.remove(jobId);
            this.idToQueue.put(jobId, queueName);
        }
        
        this.dirty = true;
    }


    @Override
    public void start()
    {
        // Start queue
        for (SchedulerQueue q : this.queues.values())
            q.start();
        
        // Flusher
        this.flushThread = Executors.newScheduledThreadPool(1);
        this.flushThread.scheduleWithFixedDelay(
            new Flusher(),
            0L,
            this.flushInterval,
            TimeUnit.MILLISECONDS);
        
        LOG.info("Scheduler started");
    }


    @Override
    public synchronized void stop()
    {
        LOG.info("Scheduler stopping...");
        
        // Stop each queue
        for (SchedulerQueue q : this.queues.values())
        {
            q.stop();
        }

        // Stop Flusher
        try
        {
            this.flushThread.shutdown();
            this.flushThread.awaitTermination(10, TimeUnit.SECONDS);

            // One last flush for good
            this.flush(true);
        }
        catch (InterruptedException e)
        {
            LOG.warn(
                "Interrupted while waiting for flusher finishes",
                e);
        }

        LOG.info("Scheduler stopped");
    }


    public synchronized void unscheduleJob(
        String jobId,
        String queueName)
    {
        if (!this.idToClass.containsKey(jobId))
        {
            throw new RuntimeException("Unregistered job ID \"" + jobId + "\"");
        }

        if (!this.queues.containsKey(queueName))
        {
            throw new RuntimeException("Unknown queue name \"" + queueName + "\"");
        }

        SchedulerQueue q = this.queues.get(queueName);
        q.removeJob(jobId);
        this.idToQueue.remove(jobId);
        this.unscheduledJobIds.add(jobId);
        
        dirty = true;
    }


    //
    // INTERNAL METHODS
    //
    
    private synchronized void flush(boolean force)
    {
        boolean shouldExport = force;
        
        if (!shouldExport) {
            shouldExport = this.dirty;
        }
        
        if (!shouldExport) {
            // Check each queue, see if modTime is different
            for (String qName : this.queues.keySet()) {
                SchedulerQueue q = this.queues.get(qName);
                if (q.getLastModTime() != this.queueModTime.get(qName)) {
                    shouldExport = true;
                    break;
                }
            }
        }
        
        if (shouldExport) {
            List<ScheduledJobInfo> list = new ArrayList<>();
            
            Map<String, Long> tmpQueueModTime = new HashMap<>();
            
            for (SchedulerQueue q : this.queues.values()) {
                // Temporarily save all queue's modTime
                tmpQueueModTime.put(q.getName(), q.getLastModTime());
                list.addAll(q.getAllJobs());
            }

            try {
                this.exportData(list);
            }
            catch (Throwable e) {
                LOG.error("Fail to flush scheduler job: {}", e.getMessage(), e);
            }
            
            this.dirty = false;
            this.queueModTime = tmpQueueModTime; // hot swap
        }
    }

    private List<ScheduledJobInfo> importData()
        throws IOException
    {
        LOG.info("Start importing scheduler jobs");
        List<ScheduledJobInfo> result = new ArrayList<>();
        if (Files.exists(this.path)) {
            try (
                 InputStream is = Files.newInputStream(this.path, READ);
                 BufferedInputStream bis = new BufferedInputStream(is);)
            {
                Iterator<ScheduledJobInfo> it = SchedulerExporter.read(bis);
                while (it.hasNext())
                    result.add(it.next());
            }
        }
        LOG.info("Complete importing {} scheduler jobs", result.size());
        return result;
    }


    private void exportData(
        List<ScheduledJobInfo> list)
        throws IOException
    {
        LOG.info("Start exporting scheduled jobs");

        try (
             OutputStream os = Files.newOutputStream(this.path, new OpenOption[] {
                     CREATE, WRITE, TRUNCATE_EXISTING
             });
             BufferedOutputStream bos = new BufferedOutputStream(os);)
        {
            SchedulerExporter.write(bos, list.iterator());
        }

        LOG.info("Complete exporting {} scheduler jobs", list.size());
    }



    private class Flusher
        implements
            Runnable
    {
        @Override
        public void run()
        {
            Scheduler.this.flush(false);
        }
    }



    //
    // SINGLETON MANAGEMENT
    //


    private static class SingeltonHolder
    {
        private static final Scheduler INSTANCE = new Scheduler();
    }


    public static Scheduler instance()
    {
        return SingeltonHolder.INSTANCE;
    }
}
