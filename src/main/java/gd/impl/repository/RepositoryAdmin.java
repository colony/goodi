
package gd.impl.repository;


import java.io.IOException;


public interface RepositoryAdmin
{
    public void exportData(
        String fname)
    
        throws IOException;


    public void importData(
        String fname)
    
        throws IOException;
}
