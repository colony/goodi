
package gd.impl.repository;


public final class Repositories {
    private static RepositoryManager _repoMgr;


    public static <T> T get(
        RepositoryType id,
        Class<T> clazz) {
        return _repoMgr.get(id, clazz);
    }


    public static void init() {
        _repoMgr = RepositoryManager.instance();
    }
}
