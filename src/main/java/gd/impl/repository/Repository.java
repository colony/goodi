
package gd.impl.repository;


import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;


// Basic CRUD
public interface Repository<ID,T>
{
    /**
     * 
     * @param t
     * @return
     * @throws InsertConflictException
     *             An entity with same ID has already existed. And
     *             the operation violate the unique constraint on ID
     */
    public T add(
        T t);


    public boolean delete(
        ID i);


    public boolean exists(
        ID i);


    public T get(
        ID i);


    public void init(
        RepositoryConfig config);


    /**
     * USE WITH CAUTION. the method will UPSERT all the passed-in
     * data WITHOUT considering the consistency in concurrent
     * environment (normally, updateTime is used as optimistic
     * version lock). Additionally, NON-MODIFIABLE fields might also
     * be changed/replaced, also null fields is valid and will
     * nullify existing data if any
     * 
     * @param t
     *            Then entity containing updatable data
     * @return The updated entity
     */
    public T put(
        T t);


    /**
     * Update with the specified entity. Per implementation, not all
     * fields of entity are updatable, only a portion of data from
     * specified t will be updated and persisted. Also, the method
     * will throw update conflict if the exiting NON-MODIFIABLE
     * fields (i.e. ID) and updateTime mismatch the data passed in.
     * 
     * @param t
     *            The entity containing updatable data
     * @return The updated copy of entity, or null if the store did
     *         not contain a message with the same ID as the
     *         specified message (in which case no change will
     *         occur)
     */
    public T update(
        T t)
        throws UpdateConflictException;


    public void visitAll(
        Visitor<T> visitor);
}
