
package gd.impl.repository;


import gd.impl.config.Configs;
import gd.impl.config.GlobalProperties;
import gd.impl.util.InitializationError;
import gd.impl.util.Managed;

import java.nio.file.Path;
import java.util.EnumMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RepositoryManager
    implements
        Managed
{
    private static final Logger LOG =
        LoggerFactory.getLogger(RepositoryManager.class);

    protected EnumMap<RepositoryType,Repository<?,?>> repositories;


    private RepositoryManager()
    {
        LOG.info("Start initialize repositories");

        // Start with empty map
        this.repositories = new EnumMap<>(RepositoryType.class);

        RepositoriesConfig rsCfg =
            Configs.get(RepositoriesConfig.class);

        int numLoaded = 0;
        for (RepositoryType id : RepositoryType.values())
        {
            Repository<?,?> repo =
                initRepository(rsCfg.get(id));
            this.repositories.put(id, repo);
            ++numLoaded;
        }

        LOG.info("Complete initialize {} repositories", numLoaded);
    }


    public <T> T get(
        RepositoryType id,
        Class<T> clazz)
    {
        T result;

        Repository<?,?> obj = this.repositories.get(id);
        if (obj == null)
        {
            // The repository was not initialized?
            throw new RuntimeException("Cannot find repository: "
                + id);
        }
        else
        {
            result = clazz.cast(obj);
        }

        return result;
    }


    @Override
    public void start()
    {
        // Nothing to do
        LOG.info("RepositoryManager starting...");
        for (Repository<?,?> repo : this.repositories.values())
        {
            if (repo instanceof Managed)
            {
                ((Managed) repo).start();
            }
        }
        LOG.info("RepositoryManager started");
    }


    @Override
    public void stop()
    {
        LOG.info("RepositoryManager stopping...");
        try
        {
            for (Repository<?,?> repo : this.repositories.values())
            {
                if (repo instanceof Managed)
                {
                    ((Managed) repo).stop();
                }
            }
        }
        catch (Throwable t)
        {
            LOG.error("RepositoryManager failed to stop", t);
        }
        LOG.info("RepositoryManager stopped");
    }


    private Repository<?,?> initRepository(
        RepositoryConfig cfg)
    {
        Class<? extends Repository<?,?>> implClass;
        try
        {
            implClass = cfg.implClass();
        }
        catch (ClassNotFoundException e)
        {
            throw new InitializationError(
                "Fail to get the impl class for Repository "
                    + cfg.getId(), e);
        }

        // Validate against the metadata
//        RepositoryMetadata meta =
//            RepositoryManager.readMetadata(cfg.getId());
//        if (meta != null)
//        {
//            // If the repository exists, sanity check on impl class
//            if (!StringUtils.equals(
//                meta.getImplClass().getName(),
//                cfg.getImplClassName()))
//            {
//                throw new InitializationError(
//                    "Inconsistent impl class, meta:"
//                        + meta.getImplClass().getName()
//                        + ", while config:"
//                        + cfg.getImplClassName());
//            }
//        }
        // Any update on configuration writes to metadata here
        Repository<?,?> repo = null;
        try
        {
            repo = implClass.newInstance();
            repo.init(cfg);
            return repo;
        }
        catch (Throwable t)
        {
            throw new InitializationError(
                "Fail to create instance for Repository "
                    + cfg.getId(), t);
        }
    }


    //
    // STATIC INTERFACE
    //


    public static Path getDir(
        RepositoryType id)
    {
        GlobalProperties gp = GlobalProperties.instance();
        return gp.repositoryDir()
            .resolve(id.abbreviation());
    }



    private static class SingletonHolder
    {
        private static final RepositoryManager INSTANCE =
            new RepositoryManager();
    }


    public static RepositoryManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
