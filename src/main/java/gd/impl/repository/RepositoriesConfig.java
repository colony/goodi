
package gd.impl.repository;


import gd.impl.config.Configured;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;


// Not reloadable
@Configured(fname = "repository.yml")
public class RepositoriesConfig
{
    /**
     * N.B. The setters are primarily used by Jackson
     * deserialization. Make them private so that the configuration
     * must be immutable.
     */

    private EnumMap<RepositoryType,RepositoryConfig> repositories;


    /**
     * Default constructor
     */
    public RepositoriesConfig()
    {
    }


    public RepositoryConfig get(
        RepositoryType id)
    {
        if (this.repositories == null
            || !this.repositories.containsKey(id))
        {
            return new RepositoryConfig(id);
        }
        return this.repositories.get(id);
    }


    public List<RepositoryConfig> getRepositories()
    {
        if (this.repositories == null)
        {
            return Collections.emptyList();
        }
        return new ArrayList<>(this.repositories.values());
    }


    @SuppressWarnings("unused")
    private void setRepositories(
        List<RepositoryConfig> repositories)
    {
        this.repositories = new EnumMap<>(RepositoryType.class);
        for (RepositoryConfig cfg : repositories)
        {
            this.repositories.put(cfg.getId(), cfg);
        }

        // Create default configuration for missing one
        for (RepositoryType id : RepositoryType.values())
        {
            if (!this.repositories.containsKey(id))
            {
                this.repositories.put(id, new RepositoryConfig(id));
            }
        }
    }
}
