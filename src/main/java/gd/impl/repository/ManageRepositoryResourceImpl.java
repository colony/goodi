
package gd.impl.repository;


import gd.api.v1.admin.ManageRepositoryResource;
import gd.impl.Rests;
import gd.impl.util.GDUtils;

import java.io.IOException;

import javax.inject.Singleton;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/adm/repositories")
@Singleton
public class ManageRepositoryResourceImpl
    implements
        ManageRepositoryResource
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ManageRepositoryResourceImpl.class);

    protected RepositoryManager repoMgr;


    public ManageRepositoryResourceImpl()
    {
        this.repoMgr = RepositoryManager.instance();
    }


    @Override
    @Path("/export")
    @POST
    @Produces("text/plain")
    public Response exportRepository(
        @Context HttpHeaders headers,
        @FormParam("repo_type") RepositoryType type,
        @FormParam("file_name") String fileName)
    {
        Rests.beginAdminOperation(headers, "adm:repo:export:" + type.name() + ":" + fileName);
        
        GDUtils.check(
            StringUtils.isNotEmpty(fileName),
            Status.BAD_REQUEST,
            "Empty not allowed for file_name");
        
        RepositoryAdmin repo =
            this.repoMgr.get(type, RepositoryAdmin.class);
        try
        {
            repo.exportData(fileName);
        }
        catch (IOException e)
        {
            LOG.error(
                "Fail to export repository {} to file {}",
                type,
                fileName,
                e);
            throw new WebApplicationException(
                (Response.status(Status.INTERNAL_SERVER_ERROR)
                    .entity("Something bad happens")
                    .build()));
        }

        return Response.ok().build();
    }


    @Override
    @Path("/import")
    @POST
    @Produces("text/plain")
    public Response importRepository(
        @Context HttpHeaders headers,
        @FormParam("repo_type") RepositoryType type,
        @FormParam("file_name") String fileName)
    {
        Rests.beginAdminOperation(headers, "adm:repo:import:" + type.name() + ":" + fileName);
        
        GDUtils.check(
            StringUtils.isNotEmpty(fileName),
            Status.BAD_REQUEST,
            "Empty not allowed for file_name");
        
        RepositoryAdmin repo =
            this.repoMgr.get(type, RepositoryAdmin.class);
        try
        {
            repo.importData(fileName);
        }
        catch (IOException e)
        {
            LOG.error(
                "Fail to import repository {} to file {}",
                type,
                fileName,
                e);
            throw new WebApplicationException(
                (Response.status(Status.INTERNAL_SERVER_ERROR)
                    .entity("Something bad happens")
                    .build()));
        }

        return Response.ok().build();
    }

}
