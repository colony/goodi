
package gd.impl.repository;


import gd.impl.util.GDUtils;

import com.google.common.base.Strings;



public class RepositoryConfig
{
    private RepositoryType id;

    private String implClassName;

    //
    // Config applicable for ResidentRepository
    //

    private int capacity = -1;


    //
    // Config applicable for SQLiteRepository
    //

    private String tableName;

    /**
     * When visitAll, items are fetched in batch then be visited.
     * The setting defines how many items to be fetched per batch
     */
    private int batchSize = 100;



    /**
     * Default constructor
     */
    public RepositoryConfig()
    {
    }


    public RepositoryConfig(
        RepositoryType id)
    {
        this.id = id;
    }


    //
    // CONVENIENT METHODS
    //


    public Class<? extends Repository<?,?>> implClass()
        throws ClassNotFoundException
    {
        Class<?> rawClass = Class.forName(this.getImplClassName());
        Class<? extends Repository<?,?>> implClass =
            GDUtils.cast(rawClass.asSubclass(Repository.class));
        return implClass;
    }


    /**
     * Validate critical non-default configuration that can't be
     * missed during system lifecycle. The method should be called
     * at boot time, and fail if the validation doesn't pass
     */
    public boolean validate()
    {
        try
        {
            if (this.implClass().isAssignableFrom(SqliteRepository.class)) {
                if (Strings.isNullOrEmpty(this.tableName))
                    return false;
            }
        }
        catch (ClassNotFoundException e)
        {
            return false;
        }
        
        return true;
    }


    //
    // Getters
    //


    public int getCapacity()
    {
        return capacity;
    }


    public RepositoryType getId()
    {
        return id;
    }


    public String getImplClassName()
    {
        if (Strings.isNullOrEmpty(this.implClassName))
        {
            return id.defaultImplClass().getName();
        }

        return this.implClassName;
    }


    public String getTableName()
    {
        return tableName;
    }


    public int getBatchSize()
    {
        return batchSize;
    }
}
