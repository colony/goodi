
package gd.impl.repository;


import gd.impl.activity.ActivityRepository;
import gd.impl.activity.ResidentActivityRepository;
import gd.impl.comment.CommentRepository;
import gd.impl.comment.ResidentCommentRepository;
import gd.impl.group.GroupRepository;
import gd.impl.group.ResidentGroupRepository;
import gd.impl.post.PostRepository;
import gd.impl.post.ResidentPostRepository;
import gd.impl.session.ResidentSessionRepository;
import gd.impl.session.SessionRepository;
import gd.impl.user.ResidentUserRepository;
import gd.impl.user.UserRepository;


public enum RepositoryType
{
    ACTIVITY(
        "a",
        ResidentActivityRepository.class,
        ActivityRepository.class),

    COMMENT(
        "cm",
        ResidentCommentRepository.class,
        CommentRepository.class),

//    CREDS(
//        "c",
//        ResidentCredentialsRepository.class,
//        CredentialsRepository.class),
        
    GROUP(
        "g",
        ResidentGroupRepository.class,
        GroupRepository.class),

    POST(
        "p",
        ResidentPostRepository.class,
        PostRepository.class),

    SESSION(
        "s",
        ResidentSessionRepository.class,
        SessionRepository.class),

    USER(
        "u",
        ResidentUserRepository.class,
        UserRepository.class),

    ;


    private final String abbrev;

    private final Class<? extends Repository<?,?>> dfltImplClass;

    /**
     * The specialized and REQUIRED interface (i.e. some extension
     * of {@link Repository} used to implement this repository
     */
    private final Class<? extends Repository<?,?>> specializedInterface;


    private RepositoryType(
        String abbrev,
        Class<? extends Repository<?,?>> dfltImplClass,
        Class<? extends Repository<?,?>> spInterface)
    {
        this.abbrev = abbrev;
        this.dfltImplClass = dfltImplClass;
        this.specializedInterface = spInterface;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public Class<? extends Repository<?,?>> defaultImplClass()
    {
        return this.dfltImplClass;
    }


    public Class<? extends Repository<?,?>> specializedInterface()
    {
        return this.specializedInterface;
    }


    public static RepositoryType fromAbbreviation(
        String abbrev)
    {
        for (RepositoryType r : RepositoryType.values())
        {
            if (r.abbrev.equals(abbrev))
            {
                return r;
            }
        }

        throw new IllegalArgumentException(
            "No repository found for abbrev \"" + abbrev + "\"");
    }
}
