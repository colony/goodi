
package gd.impl.template;


import java.util.Map;


public interface Template
{
    /**
     * Pass the input parameter to the underlying template and
     * generate the final message
     * 
     * @return The parsed message
     */
    public String parse(
        Map<String,Object> data);


    /**
     * A shorter version but help most cases that require only one pair of data
     */
    public String parse(
        String key,
        Object val);
}
