
package gd.impl.template;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.collect.ImmutableMap;


public class MustacheTemplate
    implements
        Template
{
    private Mustache mustache;


    public MustacheTemplate(
        Path p)
    {
        MustacheFactory mf = new DefaultMustacheFactory();
        String name = p.toString();

        try
        {
            // Seems like the name is only logging usage
            this.mustache =
                mf.compile(new FileReader(p.toFile()), name);
        }
        catch (FileNotFoundException e)
        {
            throw new RuntimeException(
                "Fail to compile mustache template data for path: "
                    + name,
                e);
        }
    }


    @Override
    public String parse(
        Map<String,Object> data)
    {
        try
        {
            StringWriter sw = new StringWriter();
            this.mustache.execute(sw, data).flush();
            return StringUtils.trim(sw.toString());
        }
        catch (IOException e)
        {
            throw new RuntimeException(
                "Fail to execute mustache template data for path: "
                    + this.mustache.getName(),
                e);
        }
    }


    @Override
    public String parse(
        String key,
        Object val)
    {
        return this.parse(val == null ? null : ImmutableMap.of(key, val));
    }
}
