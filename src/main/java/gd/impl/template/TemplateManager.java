
package gd.impl.template;


import gd.impl.config.GlobalProperties;
import gd.impl.util.GDUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;


public class TemplateManager
{
    private static final Logger LOG =
        LoggerFactory.getLogger(TemplateManager.class);

    private static final String[] SUPPORT_FORMAT = {
        "mustache", "txt"
    };

    protected Path baseDir;

    protected LoadingCache<TemplateKey,Template> templates;


    private TemplateManager()
    {
        this.baseDir = GlobalProperties.instance().templateDir();

        this.templates =
            CacheBuilder.newBuilder().build(
                new CacheLoader<TemplateKey,Template>() {
                    @Override
                    public Template load(
                        final TemplateKey key)
                        throws Exception
                    {
                        // Template path corresponds to key
                        File templatePath = baseDir
                            .resolve(key.locale.toString())
                            .resolve(key.id.name().toLowerCase())
                            .toFile();

                        File[] templates =
                            templatePath.listFiles(new FilenameFilter() {

                                @Override
                                public boolean accept(
                                    File dir,
                                    String name)
                                {
                                    // remove the extension
                                    int i = name.lastIndexOf(".");
                                    String ext = null;
                                    if (i != -1)
                                    {
                                        ext = name.substring(i + 1);
                                        name = name.substring(0, i);
                                    }

                                    return name.equalsIgnoreCase(key.type.name())
                                        &&
                                        ext != null
                                        &&
                                        ArrayUtils.contains(
                                            SUPPORT_FORMAT,
                                            ext);
                                }
                            });

                        if (templates.length == 0)
                            return null;

                        // If there is multiple templates for same key,
                        // possibly different format (mustach, java mf)
                        // We only pick the first one (BE CAUTION)
                        Path p = templates[0].toPath();
                        String ext =
                            GDUtils.getFileExtension(p.getFileName()
                                .toString());
                        switch (ext)
                        {
                        case "mustache":
                            return new MustacheTemplate(p);

                        case "txt":
                            return new TextTemplate(p);

                        default:
                            LOG.warn(
                                "Unrecognized template format extension for path {}",
                                p);
                            return null;
                        }
                    }
                });
    }


    public Template get(
        TemplateType id,
        Locale locale,
        TemplateClass type)
    {
        if (locale == null)
            locale = Locale.US;

        TemplateKey key = new TemplateKey(
            id,
            locale,
            type);
        try
        {
            Template t = this.templates.get(key);
            if (t == null)
                LOG.warn("There is no template for key: " + key);

            return t;
        }
        catch (ExecutionException e)
        {
            LOG.error("Fail to fetch template for key: " + key, e);
            return null;
        }
    }


    public void reload()
    {
        this.templates.invalidateAll();
    }
    

    private class TemplateKey
    {
        TemplateType id;

        Locale locale;

        TemplateClass type;

        int hash;


        TemplateKey(
            TemplateType id,
            Locale locale,
            TemplateClass type)
        {
            this.id = id;
            this.locale = locale;
            this.type = type;

            this.hash =
                Objects.hash(this.id, this.locale, this.type);
        }


        @Override
        public boolean equals(
            Object o)
        {
            if (o == null ||
                !(o instanceof TemplateKey))
            {
                return false;
            }
            TemplateKey other = (TemplateKey) o;

            return this.id == other.id &&
                this.type == other.type &&
                this.locale.equals(other.locale);
        }


        @Override
        public int hashCode()
        {
            return this.hash;
        }


        @Override
        public String toString()
        {
            return String.format(
                "%s:%s:%s",
                this.locale.toString(),
                this.id.name(),
                this.type.name());
        }
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final TemplateManager INSTANCE =
            new TemplateManager();
    }


    public static TemplateManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
