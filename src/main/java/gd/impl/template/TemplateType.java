
package gd.impl.template;


import gd.api.v1.activity.ActivityType;


public enum TemplateType
{
    /**
     * Template for COMMENT_POST activity
     */
    ACTIVITY_COMMENT_POST,
    
    /**
     * Template for DECLARE_WINNER activity
     */
    ACTIVITY_DECLARE_WINNER,

    /**
     * Template for OTHER_COMMENT_POST activity
     */
    ACTIVITY_OTHER_COMMENT_POST,

    /**
     * Template for OTHER_PARTICIPATE_THREAD activity
     */
    ACTIVITY_OTHER_PARTICIPATE_THREAD,

    /**
     * Template for PARTICIPATE_THREAD activity
     */
    ACTIVITY_PARTICIPATE_THREAD,

    /**
     * Template for REPLY_COMMENT activity
     */
    ACTIVITY_REPLY_COMMENT,

    /**
     * Template for VOTE_POST activity
     */
    ACTIVITY_VOTE_POST,

    NEW_USER_EMAIL_VERIFICATION,

    ;


    public static TemplateType fromActivityType(
        ActivityType type)
    {
        switch (type)
        {
        case COMMENT_POST:
            return TemplateType.ACTIVITY_COMMENT_POST;
        case DECLARE_WINNER:
            return TemplateType.ACTIVITY_DECLARE_WINNER;
        case OTHER_COMMENT_POST:
            return TemplateType.ACTIVITY_OTHER_COMMENT_POST;
        case OTHER_PARTICIPATE_THREAD:
            return TemplateType.ACTIVITY_OTHER_PARTICIPATE_THREAD;
        case PARTICIPATE_THREAD:
            return TemplateType.ACTIVITY_PARTICIPATE_THREAD;
        case REPLY_COMMENT:
            return TemplateType.ACTIVITY_REPLY_COMMENT;
        case VOTE_POST:
            return TemplateType.ACTIVITY_VOTE_POST;
        default:
            throw new IllegalArgumentException("Unsupported activity type " + type);
        }
    }
}
