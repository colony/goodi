package gd.impl.template;

public enum TemplateClass
{   
    APNS,
    
    /**
     * Content of Activity that displayed in app (i.e. notification page)
     */
    BODY,
    
    MAIL_BODY_HTML,
    
    MAIL_BODY_TEXT,
    
    MAIL_SUBJECT,
    
    ;
}
