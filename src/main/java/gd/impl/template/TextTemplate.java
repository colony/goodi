
package gd.impl.template;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;


public class TextTemplate
    implements
        Template
{
    private String text;
    
    public TextTemplate(
        Path p)
    {
        // Read template data
        List<String> lines;
        try
        {
            lines = Files.readAllLines(p, StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            throw new RuntimeException(
                "Fail to read txt template data for path: " + p,
                e);
        }
        
        // The above method will strip off any newline and merge them back
        // The only exception here is, we discard any line beginning with '#"
        StringBuilder sb = new StringBuilder();
        char nl = '\n';
        for (String ln : lines)
        {
            // Skip comment lines
            if (ln.startsWith("#"))
                continue;
            
            sb.append(ln);
            sb.append(nl);
        }
        // Remove last newline
        sb.deleteCharAt(sb.length() - 1);
        
        this.text = sb.toString();
    }

    
    @Override
    public String parse(Map<String,Object> data)
    {
        // Do nothing with data
        return text;
    }


    @Override
    public String parse(
        String key,
        Object val)
    {
        return text;
    }
}
