
package gd.impl.api;


import gd.impl.config.Configured;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import com.google.common.collect.Lists;


// Not reloadable
@Configured(fname = "api.yml")
public class ApisConfig
{
    private EnumMap<ApiType,ApiConfig> apis;


    /**
     * Default constructor
     */
    public ApisConfig()
    {
    }


    public ApiConfig get(
        ApiType id)
    {
        if (this.apis == null || !this.apis.containsKey(id))
        {
            // Return a default one when miss
            return new ApiConfig(id);
        }
        return this.apis.get(id);
    }


    public List<ApiConfig> getApis()
    {
        if (this.apis == null)
        {
            return Lists.newArrayList();
        }
        return new ArrayList<>(this.apis.values());
    }


    @SuppressWarnings("unused")
    private void setApis(
        List<ApiConfig> apis)
    {
        this.apis = new EnumMap<>(ApiType.class);
        for (ApiConfig cfg : apis)
        {
            this.apis.put(cfg.getId(), cfg);
        }

        // Create default configuration for missing one
        for (ApiType id : ApiType.values())
        {
            if (!this.apis.containsKey(id))
            {
                this.apis.put(id, new ApiConfig(id));
            }
        }
    }
}
