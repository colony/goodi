
package gd.impl.api;


import gd.impl.api.http.HttpGoogleApi;


public enum ApiType
{
//    FACEBOOK(
//        "fb",
//        HttpFacebookApi.class,
//        FacebookApi.class),
        
    GOOGLE(
        "g",
        HttpGoogleApi.class,
        GoogleApi.class),

    ;

    private final String abbrev;

    private final Class<? extends Api> dfltImplClass;

    private final Class<? extends Api> specializedInterface;


    private ApiType(
        String abbrev,
        Class<? extends Api> dfltImplClass,
        Class<? extends Api> spInterface)
    {
        this.abbrev = abbrev;
        this.dfltImplClass = dfltImplClass;
        this.specializedInterface = spInterface;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public Class<? extends Api> defaultImplClass()
    {
        return dfltImplClass;
    }


    public Class<? extends Api> specializedInterface()
    {
        return specializedInterface;
    }


    public static ApiType fromAbbreviation(
        String abbrev)
    {
        for (ApiType a : ApiType.values())
        {
            if (a.abbrev.equals(abbrev))
            {
                return a;
            }
        }

        throw new IllegalArgumentException(
            "Unrecognized Apis abbreviation \"" + abbrev + "\"");
    }
}
