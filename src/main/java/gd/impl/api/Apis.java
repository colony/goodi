
package gd.impl.api;


public final class Apis
{
    private static ApiManager _apiMgr;


    public static <T extends Api> T get(
        ApiType id,
        Class<T> clazz)
    {
        return _apiMgr.get(id, clazz);
    }


    public static void init()
    {
        _apiMgr = ApiManager.instance();
    }
}
