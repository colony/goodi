
package gd.impl.api;


import com.google.common.base.Strings;



public interface GoogleApi
    extends
        Api
{
    //
    // API DATA
    //


    public static class GeocodeRequest
    {
        public double lat;

        public double lon;


        public GeocodeRequest(
            double lat,
            double lon)
        {
            this.lat = lat;
            this.lon = lon;
        }
    }



    public static class GeocodeResponse
    {
        // Type = neighborhood, long_name
        public String neighborhoodLong;

        // Type = sublocality, long_name
        public String sublocalityLong;

        // Type = locality, long_name
        public String localityLong;

        // Type = administrative_area_level_1, long_name
        public String administrativeAreaLevel1Long;

        // Type = country, long_name
        public String countryLong;

        // Type = country, short_name
        public String countryShort;

        // Not null, if status is not OK
        public String errorStatus;


        public boolean isOK()
        {
            return Strings.isNullOrEmpty(errorStatus);
        }
    }
}
