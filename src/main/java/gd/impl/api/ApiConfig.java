
package gd.impl.api;


import gd.impl.util.GDUtils;

import com.google.common.base.Strings;


public class ApiConfig
{
    private String endpoint;
    
    /**
     * Auxiliary data
     */
    private String extra;

    private ApiType id;

    private String implClassName;

    /**
     * Associated with {@link #publicKey}. Similarly,
     * consumerSecret, appSecret. The key is used to calculate the
     * digital signature that is sent along with each request. The
     * API provider, which also has a copy of this key on their
     * side, validate the caller by recalculating&matching the
     * signature.
     */
    private String privateKey;

    /**
     * Multiple uses. Similarly, consumerKey, appId. For some APIs,
     * which employ token based authentication, the field stores the
     * public token and is send along with each request. For some
     * APIs, the field stores a public access key, paired with a
     * secret.
     */
    private String publicKey;


    /**
     * Default constructor
     */
    public ApiConfig()
    {
    }


    public ApiConfig(
        ApiType id)
    {
        this.id = id;
    }


    public String getEndpoint()
    {
        return endpoint;
    }
    
    
    public String getExtra()
    {
        return extra;
    }


    public ApiType getId()
    {
        return id;
    }


    public String getImplClassName()
    {
        if (Strings.isNullOrEmpty(this.implClassName))
        {
            return id.defaultImplClass().getName();
        }
        return this.implClassName;
    }
    
    
    public String getPrivateKey()
    {
        return privateKey;
    }
    
    
    public String getPublicKey()
    {
        return publicKey;
    }


    public Class<? extends Api> implClass()

        throws ClassNotFoundException
    {
        Class<?> rawClass = Class.forName(this.getImplClassName());
        Class<? extends Api> implClass =
            GDUtils.cast(rawClass.asSubclass(Api.class));

        return implClass;
    }
}
