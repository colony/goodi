package gd.impl.api;

public interface Api
{
    Object execute(Object request) throws ApiException;
    
    void init(ApiConfig cfg);
}

