
package gd.impl.api;


import gd.impl.config.ConfigManager;
import gd.impl.util.InitializationError;

import java.util.EnumMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ApiManager
{
    protected EnumMap<ApiType,Api> apis;


    private ApiManager()
    {
        LOG.info("Start initialize apis");

        // Start with empty map
        this.apis = new EnumMap<>(ApiType.class);

        ApisConfig apisCfg =
            ConfigManager.instance().get(ApisConfig.class);
        if (apisCfg == null)
        {
            throw new InitializationError(
                "Cannot find api configuration");
        }

        int numLoaded = 0;
        for (ApiType id : ApiType.values())
        {
            Api api = initApi(apisCfg.get(id));
            this.apis.put(id, api);
            ++numLoaded;
        }
        LOG.info("Complete initialize {} apis", numLoaded);
    }


    public <T extends Api> T get(
        ApiType id,
        Class<T> clazz)
    {
        T result;

        Api obj = this.apis.get(id);
        if (obj == null)
        {
            throw new RuntimeException(
                "Api \"" + id + "\" doesn't exist");
        }
        else
        {
            result = clazz.cast(obj);
        }

        return result;
    }


    private Api initApi(
        ApiConfig cfg)
    {
        Class<? extends Api> implClass;
        try
        {
            implClass = cfg.implClass();
        }
        catch (ClassNotFoundException e)
        {
            throw new InitializationError("");
        }

        Api implObj;
        try
        {
            implObj = implClass.newInstance();
        }
        catch (Exception e)
        {
            throw new InitializationError("");
        }

        implObj.init(cfg);
        return implObj;
    }


    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final ApiManager INSTANCE = new ApiManager();
    }


    public static ApiManager instance()
    {
        return SingletonHolder.INSTANCE;
    }


    //
    // INTERNAL CONSTANTS
    //

    private static final Logger LOG =
        LoggerFactory.getLogger(ApiManager.class);
}
