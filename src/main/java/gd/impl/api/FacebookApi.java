
package gd.impl.api;


import java.util.ArrayList;
import java.util.List;


public interface FacebookApi
    extends
        Api
{
//    public GetUserResponse getUser(
//        GetUserRequest req)
//
//        throws ApiException;
//
//
//    public GetUserFriendsResponse getUserFriends(
//        GetUserFriendsRequest req)
//
//        throws ApiException;
    

    //
    // API DATA
    //
    

    public static class GetUserRequest
    {
        private String accessToken;

        private String userId;

        
        public GetUserRequest accessToken(String at)
        {
            this.accessToken = at;
            return this;
        }
        
        
        public GetUserRequest userId(String userId)
        {
            this.userId = userId;
            return this;
        }


        public String getAccessToken()
        {
            return accessToken;
        }

        
        public String getUserId()
        {
            return userId;
        }
    }



    public static class GetUserResponse
    {
        private String email;

        private String id;

        private String gender;

        private String name;


        public String getEmail()
        {
            return email;
        }


        public void setEmail(
            String email)
        {
            this.email = email;
        }


        public String getId()
        {
            return id;
        }


        public void setId(
            String id)
        {
            this.id = id;
        }


        public String getGender()
        {
            return gender;
        }


        public void setGender(
            String gender)
        {
            this.gender = gender;
        }


        public String getName()
        {
            return name;
        }


        public void setName(
            String name)
        {
            this.name = name;
        }
    }



    public static class GetUserFriendsRequest
    {
        private String accessToken;

        private String userId;

        private int limit = 50; // default
        
        
        public GetUserFriendsRequest accessToken(String at)
        {
            this.accessToken = at;
            return this;
        }
        
        
        public GetUserFriendsRequest limit(int l)
        {
            this.limit = l;
            return this;
        }
        
        
        public GetUserFriendsRequest userId(String ui)
        {
            this.userId = ui;
            return this;
        }


        public String getAccessToken()
        {
            return accessToken;
        }


        public String getUserId()
        {
            return userId;
        }


        public int getLimit()
        {
            return limit;
        }
    }



    public static class GetUserFriendsResponse
    {
        private List<Data> data;

        private int totalCount;


        public GetUserFriendsResponse()
        {
        }


        public void addData(
            Data d)
        {
            if (data == null)
                data = new ArrayList<>();
            data.add(d);
        }


        public List<Data> getData()
        {
            return data;
        }


        public void setData(
            List<Data> d)
        {
            this.data = d;
        }


        public int getTotalCount()
        {
            return totalCount;
        }


        public void setTotalCount(
            int totalCount)
        {
            this.totalCount = totalCount;
        }



        public class Data
        {
            private String id;

            private String name;


            public Data(
                String id,
                String name)
            {
                this.id = id;
                this.name = name;
            }


            public String getId()
            {
                return id;
            }


            public String getName()
            {
                return name;
            }
        }
    }
}
