package gd.impl.api;

public class ApiException
    extends
        Exception
{
    public ApiException(int statusCode, String err)
    {
        super(statusCode + ": " + err);
    }
    
    public ApiException(String msg)
    {
        super(msg);
    }
    
    public ApiException(String msg, Throwable th)
    {
        super(msg, th);
    }
    
    private static final long serialVersionUID =
        -8938406567831520403L;
}
