
package gd.impl.api.http;


import gd.impl.ComponentManager;
import gd.impl.api.Api;
import gd.impl.api.ApiConfig;
import gd.impl.api.ApiException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;

import com.fasterxml.jackson.databind.ObjectMapper;


public abstract class AbstractHttpApi
    implements
        Api
{
    protected HttpClient client;

    protected ApiConfig config;

    protected ObjectMapper mapper;


    @Override
    public Object execute(
        Object req)

        throws ApiException
    {
        HttpUriRequest httpRequest = null;
        HttpResponse httpResponse = null;
        Object rep = null;
        try
        {
            httpRequest = makeHttpRequest(req);
            httpResponse = this.client.execute(httpRequest);
            rep = makeResponse(req, httpResponse);
        }
        catch (Exception e)
        {
            throw new ApiException(makeErrorMsg(req), e);
        }
        finally
        {
            if (httpResponse != null)
                HttpClientUtils.closeQuietly(httpResponse);
        }

        return rep;
    }


    public void init(
        ApiConfig cfg)
    {
        this.config = cfg;
        this.client =
            ComponentManager.instance().http().getClient();
        this.mapper = new ObjectMapper();
    }


    //
    // ABSTRACT PROTECTED METHODS - subclasses must implement these
    //


    protected abstract String makeErrorMsg(
        Object req);


    protected abstract HttpUriRequest makeHttpRequest(
        Object req)
        throws Exception;


    protected abstract Object makeResponse(
        Object req,
        HttpResponse resp)
        throws Exception;
}
