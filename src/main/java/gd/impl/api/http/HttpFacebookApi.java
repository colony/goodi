
package gd.impl.api.http;


import gd.impl.api.ApiException;
import gd.impl.api.FacebookApi;
import gd.impl.util.GDUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Strings;


public class HttpFacebookApi
    extends
        AbstractHttpApi
    implements
        FacebookApi
{
    private static final String ACCESS_TOKEN = "access_token";

    private static final String APP_SECRET_PROOF_PARAM_NAME =
        "appsecret_proof";


    @Override
    protected String makeErrorMsg(
        Object req)
    {
        if (req instanceof GetUserRequest)
        {
            GetUserRequest request = (GetUserRequest) req;
            return String.format(
                "Facebook:GetUser userId=%s accessToken=%s",
                request.getUserId(),
                request.getAccessToken());
        }
        else if (req instanceof GetUserFriendsRequest)
        {
            GetUserFriendsRequest request =
                (GetUserFriendsRequest) req;
            return String.format(
                "Facebook:GetUserFriends userId=%s accessToken=%s limit=%d",
                request.getUserId(),
                request.getAccessToken(),
                request.getLimit());
        }
        // Other request goes here

        return "";
    }


    @Override
    protected HttpUriRequest makeHttpRequest(
        Object req)

        throws Exception
    {
        if (req instanceof GetUserRequest)
        {
            GetUserRequest request = (GetUserRequest) req;
            String accessToken = request.getAccessToken();
            String userId = request.getUserId();
            if (Strings.isNullOrEmpty(userId))
                userId = "me";

            URIBuilder builder = new URIBuilder();
            builder.setPath(
                GDUtils.joinPath(this.config.getEndpoint(), userId))
                .setParameter("fields", "email,name,gender")
                .setParameter(ACCESS_TOKEN, accessToken)
                .setParameter(
                    APP_SECRET_PROOF_PARAM_NAME,
                    obtainAppSecretProof(
                        accessToken,
                        this.config.getPrivateKey()));

            return new HttpGet(builder.build());
        }
        else if (req instanceof GetUserFriendsRequest)
        {
            GetUserFriendsRequest request =
                (GetUserFriendsRequest) req;
            String accessToken = request.getAccessToken();
            String userId = request.getUserId();
            int limit = request.getLimit();
            if (Strings.isNullOrEmpty(userId))
                userId = "me";

            URIBuilder builder = new URIBuilder();
            builder.setPath(
                GDUtils.joinPath(this.config.getEndpoint(), "fql"))
                .setParameter(
                    "q",
                    "SELECT uid,name FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1=me()) LIMIT 0,100")
                .setParameter(ACCESS_TOKEN, accessToken);

//            builder.setPath(
//                GDUtils.joinPath(
//                    this.config.getEndpoint(),
//                    userId,
//                    "friends"))
//                .setParameter("fields", "id,name")
//                .setParameter("limit", String.valueOf(limit))
//                .setParameter(ACCESS_TOKEN, accessToken)
//                .setParameter(
//                    APP_SECRET_PROOF_PARAM_NAME,
//                    obtainAppSecretProof(
//                        accessToken,
//                        this.config.getPrivateKey()));

            return new HttpGet(builder.build());
        }
        // Other request goes here

        throw new IllegalArgumentException(
            "Unrecognized request type " + req.getClass().getName());
    }


    @Override
    protected Object makeResponse(
        Object req,
        HttpResponse rep)

        throws Exception
    {
        if (req instanceof GetUserRequest)
        {
            JsonNode root =
                this.mapper.readTree(rep.getEntity().getContent());
            if (root.has("error"))
            {
                throw new ApiException(rep.getStatusLine()
                    .getStatusCode(), root.toString());
            }

            GetUserResponse result = new GetUserResponse();
            result.setEmail(root.path("email").textValue());
            result.setId(root.path("id").textValue());
            result.setGender(root.path("gender").textValue());
            result.setName(root.path("name").textValue());
            return result;
        }
        else if (req instanceof GetUserFriendsRequest)
        {
            JsonNode root =
                this.mapper.readTree(rep.getEntity().getContent());
            if (root.has("error"))
            {
                throw new ApiException(rep.getStatusLine()
                    .getStatusCode(), root.asText());
            }

            GetUserFriendsResponse result =
                new GetUserFriendsResponse();
            result.setTotalCount(root.path("summary")
                .path("total_count")
                .intValue());

            JsonNode data = root.path("data");
            if (data.isArray())
            {
                int i = 0;
                JsonNode d = data.path(i);
                while (d != null && !d.isMissingNode())
                {
                    result.addData(result.new Data(
                        d.path("id").textValue(),
                        d.path("name").textValue()));
                    d = data.path(++i);
                }
            }
            return result;
        }
        // Other request goes here

        throw new IllegalArgumentException(
            "Unrecognized request type " + req.getClass().getName());
    }


    private String obtainAppSecretProof(
        String accessToken,
        String appSecret)
    {
        try
        {
            SecretKeySpec signingKey =
                new SecretKeySpec(
                    appSecret.getBytes(),
                    "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);
            byte[] raw = mac.doFinal(accessToken.getBytes());
            return Hex.encodeHexString(raw);
        }
        catch (Exception e)
        {
            throw new IllegalStateException(
                "Creation of appsecret_proof has failed",
                e);
        }
    }
}
