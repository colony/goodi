
package gd.impl.api.http;


import gd.impl.api.GoogleApi;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;

import com.fasterxml.jackson.databind.JsonNode;


public class HttpGoogleApi
    extends
        AbstractHttpApi
    implements
        GoogleApi
{

    @Override
    protected String makeErrorMsg(
        Object req)
    {
        if (req instanceof GeocodeRequest)
        {
            GeocodeRequest request = (GeocodeRequest) req;
            return "Google:Geocode lat=" + request.lat + ", lon=" + request.lon;
        }

        return "";
    }


    @Override
    protected HttpUriRequest makeHttpRequest(
        Object req)
        throws Exception
    {
        if (req instanceof GeocodeRequest)
        {
            GeocodeRequest request = (GeocodeRequest) req;
            URIBuilder builder = new URIBuilder();
            builder.setPath(this.config.getEndpoint())
                .setParameter("latlng", request.lat + "," + request.lon)
                .setParameter("key", this.config.getPublicKey());
            return new HttpGet(builder.build());
        }

        throw new IllegalArgumentException("Unrecognized request type " + req.getClass().getName());
    }


    @Override
    protected Object makeResponse(
        Object req,
        HttpResponse rep)
        throws Exception
    {
        if (req instanceof GeocodeRequest)
        {
            JsonNode root =
                this.mapper.readTree(rep.getEntity().getContent());

            GeocodeResponse response = new GeocodeResponse();
            String status = root.path("status").textValue();
            if ("OK".equals(status))
            {
                JsonNode results = root.path("results");
                for (JsonNode resultNode : results)
                {
                    JsonNode addrComponents = resultNode.path("address_components");
                    for (JsonNode comp : addrComponents)
                    {
                        String types = comp.path("types").toString();
                        if (response.neighborhoodLong == null && types.contains("\"neighborhood\""))
                        {
                            response.neighborhoodLong = comp.path("long_name").textValue();
                        }
                        else if (response.sublocalityLong == null && types.contains("\"sublocality\""))
                        {
                            response.sublocalityLong = comp.path("long_name").textValue();
                        }
                        else if (response.localityLong == null && types.contains("\"locality\""))
                        {
                            response.localityLong = comp.path("long_name").textValue();
                        }
                        else if (response.administrativeAreaLevel1Long == null
                            && types.contains("\"administrative_area_level_1\""))
                        {
                            response.administrativeAreaLevel1Long = comp.path("long_name").textValue();
                        }
                        else if (response.countryLong == null && types.contains("\"country\""))
                        {
                            response.countryLong = comp.path("long_name").textValue();
                            response.countryShort = comp.path("short_name").textValue();
                        }
                    }
                }
            }
            else
            {
                response.errorStatus = status;
            }
            return response;
        }
        // Other request goes here

        throw new IllegalArgumentException(
            "Unrecognized request type " + req.getClass().getName());
    }
}
