package gd.impl;

import gd.impl.ComponentConfig.HttpConfig;
import gd.impl.util.Managed;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

public class HttpComponent
    implements Managed
{
    private CloseableHttpClient httpClient;
    
    
    /**
     * Package-level constructor 
     */
    HttpComponent(HttpConfig cfg)
    {
        // Connection manager setting
        PoolingHttpClientConnectionManager cm =
            new PoolingHttpClientConnectionManager();
        cm.setDefaultMaxPerRoute(cfg.getMaxConnectionsPerRoute());
        cm.setMaxTotal(cfg.getMaxTotalConnections());

        // Client level request configuration
        RequestConfig defaultRCfg = RequestConfig.custom()
            .setConnectTimeout(cfg.getConnectTimeout())
            .setConnectionRequestTimeout(
                cfg.getConnectionRequestTimeout())
            .setSocketTimeout(cfg.getSoTimeout())
            .setStaleConnectionCheckEnabled(false)
            .build();

        // Socket configuration
        SocketConfig defaultSCfg = SocketConfig.custom()
            .setTcpNoDelay(cfg.getTcpNoDelay())
            .setSoKeepAlive(cfg.getSoKeepAlive())
            .build();

        this.httpClient =
            HttpClients
                .custom()
                .setConnectionManager(cm)
                .setDefaultRequestConfig(defaultRCfg)
                .setDefaultSocketConfig(defaultSCfg)
                .build();
    }
    
    
    public HttpClient getClient()
    {
        return httpClient;
    }

    
    @Override
    public void start()
    {
        
    }
    
    
    @Override
    public void stop()
    {
        HttpClientUtils.closeQuietly(this.httpClient);
    }
}
