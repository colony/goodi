
package gd.impl;


import gd.api.v1.ClientContext;
import gd.api.v1.PaginationRep;
import gd.api.v1.activity.ActivityRep;
import gd.api.v1.activity.ActivityRepList;
import gd.api.v1.activity.ActivityType;
import gd.api.v1.admin.SessionRep;
import gd.api.v1.comment.CommentRep;
import gd.api.v1.comment.CommentRepList;
import gd.api.v1.group.GroupRep;
import gd.api.v1.group.GroupRepList;
import gd.api.v1.post.MediaRep;
import gd.api.v1.post.PostRep;
import gd.api.v1.post.PostType;
import gd.api.v1.post.ThreadRep;
import gd.api.v1.post.ThreadType;
import gd.api.v1.post.VoteStatus;
import gd.api.v1.user.UserRep;
import gd.impl.activity.Activity;
import gd.impl.activity.ActivityFeedManager;
import gd.impl.comment.Comment;
import gd.impl.comment.CommentCount;
import gd.impl.config.Configs;
import gd.impl.config.DetailedConfigs;
import gd.impl.config.SystemSetting;
import gd.impl.group.Group;
import gd.impl.group.GroupCount;
import gd.impl.group.GroupRepository;
import gd.impl.group.Groups;
import gd.impl.media.AudioMedia;
import gd.impl.media.IconMedia;
import gd.impl.media.LinkMedia;
import gd.impl.media.Media;
import gd.impl.media.Medias;
import gd.impl.media.PhotoMedia;
import gd.impl.media.TextMedia;
import gd.impl.media.VideoMedia;
import gd.impl.post.Post;
import gd.impl.post.PostClass;
import gd.impl.post.PostCount;
import gd.impl.post.PostStatus;
import gd.impl.post.Posts;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.session.Session;
import gd.impl.template.TemplateClass;
import gd.impl.template.TemplateManager;
import gd.impl.template.TemplateType;
import gd.impl.user.User;
import gd.impl.user.UserStatus;
import gd.impl.user.UserStatus.Status;
import gd.impl.user.Users;
import gd.impl.util.Page;
import gd.impl.util.Sortable;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;


public final class Reps
{
    private static ActivityFeedManager _actFeedMgr;

    private static GroupRepository _groupRepo;

    private static LLRelation _hasThreadChildrenRel;

    private static TemplateManager _templateMgr;

    private static LLRelation _votesRel;

    private static LLRelation _votesCommentRel;


    public static void init()
    {
        RelationManager relMgr = RelationManager.instance();
        _hasThreadChildrenRel = relMgr.get(RelationType.HAS_THREAD_CHILDREN, LLRelation.class);
        _votesRel = relMgr.get(RelationType.VOTES, LLRelation.class);
        _votesCommentRel = relMgr.get(RelationType.VOTES_COMMENT, LLRelation.class);

        RepositoryManager repoMgr = RepositoryManager.instance();
        _groupRepo = repoMgr.get(RepositoryType.GROUP, GroupRepository.class);

        _actFeedMgr = ActivityFeedManager.instance();
        _templateMgr = TemplateManager.instance();
    }


    /**
     * 
     * @param actRep
     * @param act
     * @param lastReadId
     *            ID of last read activity, the passed in activity
     *            whose ID larger than lastReadId will be marked
     *            unread. Also note, if lastReadId is -1, marked as
     *            READ always
     */
    public static void populateActivityList(
        ActivityRepList repList,
        List<Activity> actList,
        long viewerId)
    {
        Map<String,Object> tmplData = new HashMap<>();
        Set<Long> unreadIds = _actFeedMgr.getUnreadIds(viewerId);

        for (Activity act : actList) {
            // Construct the body from activity
            ActivityType at = act.getType();
            TemplateType tt = TemplateType.fromActivityType(at);
            switch (at)
            {
            case COMMENT_POST:
            case OTHER_COMMENT_POST:
                tmplData.put("1", act.getExtra(1));
                tmplData.put("2", act.getExtra(0));
                break;

            case DECLARE_WINNER:
                tmplData.put("1", Boolean.valueOf(act.getExtra(0)));
                break;

            case PARTICIPATE_THREAD:
            case OTHER_PARTICIPATE_THREAD:
                break; // Do nothing

            case REPLY_COMMENT:
                tmplData.put("1", act.getExtra(1));
                tmplData.put("2", act.getExtra(2));
                break;

            case VOTE_POST:
                tmplData.put("1", act.getExtra(0));
                break;
            }
            String body = _templateMgr.get(tt, null, TemplateClass.BODY).parse(tmplData);
            tmplData.clear();

            ActivityRep actRep = new ActivityRep();

            // Common fields
            actRep.body = body;
            actRep.createTime = act.getCreateTime();
            actRep.id = act.getId();
            actRep.type = act.getType();

            // Version-specific fields
            ClientContext ctx = Rests.retrieveClientContextThreadLocal();
            if (ctx.getVersion() < 5) { // Version 1
                actRep.data = new JSONObject();
                if (at == ActivityType.REPLY_COMMENT)
                    actRep.data.put("pi", act.getExtra(0));
                else
                    actRep.data.put("pi", act.getTargetId());
            }
            else { // Version 2
                actRep.actorId = act.getActorId();
                switch (at)
                {
                case COMMENT_POST:
                case OTHER_COMMENT_POST:
                    actRep.commentId = act.getObjectId();
                    actRep.postId = act.getTargetId();
                    break;

                case DECLARE_WINNER:
                    actRep.postId = act.getObjectId();
                    actRep.threadId = act.getTargetId();
                    break;

                case PARTICIPATE_THREAD:
                case OTHER_PARTICIPATE_THREAD:
                    actRep.postId = act.getObjectId();
                    actRep.threadId = act.getTargetId();
                    break;

                case REPLY_COMMENT:
                    actRep.commentId = act.getObjectId();
                    actRep.postId = Long.valueOf(act.getExtra(0));
                    break;

                case VOTE_POST:
                    actRep.postId = act.getTargetId();
                    break;
                }
            }

            // FAKE
//            actRep.threadId = 1441612628651L;

            // Unread
            actRep.read = !unreadIds.contains(act.getId());
            repList.add(actRep);
        }
    }


    public static void populateCommentAdmin(
        CommentRep cmtRep,
        Comment cmt)
    {
        cmtRep.body = cmt.getBody();
        cmtRep.createTime = cmt.getCreateTime();
        cmtRep.geo = cmt.getGeo();
        cmtRep.hashtags = cmt.getHashtags();
        cmtRep.id = cmt.getId();
        cmtRep.postId = cmt.getPostId();
        cmtRep.type = cmt.getType();

        // Only populate ownerId for each comment
        UserRep owner = new UserRep();
        owner.id = cmt.getOwnerId();
        cmtRep.owner = owner;

        if (null != cmt.getParentId()) {
            Comment parentComment = Posts.getComment(cmt.getParentId());
            if (parentComment != null) {
                CommentRep parentCmtRep = new CommentRep();
                parentCmtRep.body = parentComment.getBody();
                parentCmtRep.createTime = parentComment.getCreateTime();
                parentCmtRep.geo = parentComment.getGeo();
                parentCmtRep.id = parentComment.getId();
                parentCmtRep.postId = parentComment.getPostId();
                UserRep parentCmtOwner = new UserRep();
                parentCmtOwner.id = parentComment.getOwnerId();
                parentCmtRep.owner = parentCmtOwner;
                cmtRep.parent = parentCmtRep;
            }
        }

        CommentCount count = cmt.getCount();
        if (count == null)
            count = new CommentCount();
        cmtRep.numDownvotes = count.getNumDownvotes();
        cmtRep.numReplies = count.getNumReplies();
        cmtRep.numUpvotes = count.getNumUpvotes();
    }


    /**
     * 
     * @param cmtRep
     * @param cmt
     * @param viewerId
     *            The viewer, if non-positive number and skipVote is
     *            false, the logic will be same as justCreated
     * @param justCreated
     *            Flag indicates that we are returning a rep for
     *            newly created comment (i.e. CommentResource
     *            createComment returns the json of created
     *            comment), the method will optimize a little bit if
     *            that's the case
     * @param skipParent
     *            True if not populate the parent comment (if any),
     *            just set the parentId
     * @param skipVote
     *            True if not populate vote related info (i.e.
     *            num_upvotes, etc)
     */
    public static void populateCommentBasic(
        CommentRep cmtRep,
        Comment cmt,
        long viewerId,
        boolean justCreated,
        boolean skipParent,
        boolean skipVote)
    {
        cmtRep.body = cmt.getBody();
        cmtRep.createTime = cmt.getCreateTime();
        cmtRep.geo = cmt.getGeo();
        cmtRep.hashtags = cmt.getHashtags();
        cmtRep.id = cmt.getId();
        cmtRep.postId = cmt.getPostId();
        cmtRep.type = cmt.getType();

        // Only populate ownerId for each comment
        UserRep owner = new UserRep();
        owner.id = cmt.getOwnerId();
        cmtRep.owner = owner;
        cmtRep.ownerIcon = toMediaRep(cmt.getOwnerIcon());

        // Media
        if (cmt.hasMedia()) {
            switch (cmt.getType())
            {
            case PHOTO:
                cmtRep.photo = toMediaRep(cmt.getMedia());
                break;
            default:
            }
        }

        if (!skipParent && null != cmt.getParentId()) {
            Comment parentComment = Posts.getComment(cmt.getParentId());
            if (parentComment != null) {
                CommentRep parentCmtRep = new CommentRep();
                populateCommentBasic(parentCmtRep, parentComment, viewerId, false, true, true);
                cmtRep.parent = parentCmtRep;
            }
        }

        if (!skipVote) {
            if (viewerId <= 0L || justCreated)
            {
                cmtRep.numDownvotes = 0L;
                cmtRep.numUpvotes = 0L;
                cmtRep.voteStatus = VoteStatus.NOVOTE;
                cmtRep.isBadReviewed = false;
            }
            else
            {
                CommentCount count = cmt.getCount();
                if (count == null)
                    count = new CommentCount();
                cmtRep.numDownvotes = count.getNumDownvotes();
                cmtRep.numUpvotes = count.getNumUpvotes();

                cmtRep.voteStatus = VoteStatus.NOVOTE;
                LEntry edge = _votesCommentRel.getEdge(viewerId, cmt.getId());
                if (edge != null) {
                    cmtRep.voteStatus = VoteStatus.values()[(int) edge.l1];
                }

                // Bad review
                int delta = Configs.getInt(SystemSetting.COMMENT_BADREVIEW_DELTA, 5);
                cmtRep.isBadReviewed = (delta > 0) && (cmtRep.numDownvotes - cmtRep.numUpvotes >= delta);
            }
        }
    }


    public static void populateCommentListBasic(
        CommentRepList repList,
        List<Comment> cmtList,
        long viewerId)
    {
        // We need a temporary cache to lookup parent comment (optimization)
        Map<Long,CommentRep> map = new HashMap<>();
        for (Comment cmt : cmtList) {
            CommentRep rep = new CommentRep();
            populateCommentBasic(rep, cmt, viewerId, false, true, false);
            repList.add(rep);

            // Avoid problem that the parent in commentRep is recursively populated
            // We do a shallow copy here but just NULLify the parent
            CommentRep copyRep = rep.shallowCopy();
            copyRep.parent = null;
            map.put(rep.id, copyRep);
        }

        for (int i = 0; i < cmtList.size(); i++) {
            Comment cmt = cmtList.get(i);
            if (cmt.hasParent()) {
                CommentRep cmtRep = repList.get(i);
                Long parentId = cmt.getParentId();
                if (map.containsKey(parentId)) {
                    cmtRep.parent = map.get(parentId);
                }
                else {
                    // Note parent comment might be null (deleted, or etc)
                    Comment parentCmt = Posts.getComment(parentId);
                    if (parentCmt == null) {
                        map.put(parentId, null); // Cache it
                        cmtRep.parent = null;
                    }
                    else {
                        CommentRep parentCmtRep = new CommentRep();
                        populateCommentBasic(parentCmtRep, parentCmt, viewerId, false, true, true);
                        cmtRep.parent = parentCmtRep;
                        map.put(parentId, parentCmtRep); // Cache it
                    }
                }
            }
        }
    }


    public static void populateCommentListWeb(
        CommentRepList repList,
        List<Comment> cmtList)
    {
        populateCommentListBasic(repList, cmtList, -1L);
    }


    public static void populateGroupAdmin(
        GroupRep groupRep,
        Group group,
        GroupCount groupCount)
    {
        populateGroupBasic(groupRep, group, groupCount);
    }


    @SuppressWarnings("unchecked")
    public static void populateGroupBasic(
        GroupRep groupRep,
        Group group,
        GroupCount groupCount)
    {
        groupRep.coverImageUrl = Medias.resolveMediaUrl(group.getCoverImageUrl());
        groupRep.id = group.getId();
        groupRep.name = group.getName();
        groupRep.ruleDescriptionImageUrl = group.getRuleDescriptionImageUrl();
        groupRep.ruleDescriptionText = group.getRuleDescriptionText();

        // Rule, check Group class for details
        groupRep.allowedPostTypes = (List<String>) group.getRule(Group.Rule.POST_TYPE_RULE);
        groupRep.threadType = (String) group.getRule(Group.Rule.THREAD_TYPE_RULE);
        groupRep.isPrivate = group.isPrivate();
        String sortRule = (String) group.getRule(Group.Rule.SORT_RULE);
        groupRep.isReversed = sortRule.contains(":ASC");

        // Count
        groupRep.numDownvotes = groupCount.getNumDownvotes();
        groupRep.numPosts = groupCount.getNumPosts();
        groupRep.numUpvotes = groupCount.getNumUpvotes();
    }


    /**
     * @param isAdmin
     *            True if we populate the groupList for admin
     *            console, in which case, we won't consult the
     *            configured group display orders, instead, groups
     *            are sorted based on id only
     */
    public static void populateGroupListBasic(
        GroupRepList repList,
        List<Group> grpList,
        boolean isAdmin)
    {
        final List<Long> orderGroupIds = DetailedConfigs.getGroupDisplayOrders();
        for (Group g : grpList) {
            if (g.getId() <= 1L)
                continue;
            if (!isAdmin && !orderGroupIds.contains(g.getId()))
                continue;

            GroupRep rep = new GroupRep();
            GroupCount gc = _groupRepo.getCount(g.getId());
            populateGroupBasic(rep, g, gc);
            repList.add(rep);
        }

        if (!isAdmin) {
            Collections.sort(repList.data, new Comparator<GroupRep>() {
                @Override
                public int compare(
                    GroupRep o1,
                    GroupRep o2)
                {
                    int i1 = orderGroupIds.indexOf(o1.id);
                    int i2 = orderGroupIds.indexOf(o2.id);
                    return Integer.compare(i1, i2);
                }
            });
        }
    }


    public static void populatePagination(
        PaginationRep pageRep,
        Page curPage,
        Sortable first,
        Sortable last,
        UriInfo uriInfo)
    {
        if (curPage == null || first == null || last == null)
            return;

        Page nextPage = new Page(curPage);
        nextPage.boundary = last.getSortVal();
        nextPage.position = Page.AFTER;
        nextPage.exclusive = true;
        pageRep.nextUrl = Rests.computePageUrl(nextPage, uriInfo);

        Page prevPage = new Page(curPage);
        prevPage.boundary = first.getSortVal();
        prevPage.position = Page.BEFORE;
        prevPage.exclusive = true;
        pageRep.prevUrl = Rests.computePageUrl(prevPage, uriInfo);
    }


    /**
     * Populate post representation
     * 
     * @param newlyCreated
     *            Boolean flag to hint the implementation that
     *            caller wants to populate the post just created by
     *            him, so that we could optimize accordingly
     */
    public static void populatePost(
        PostRep postRep,
        Post post,
        long viewerId,
        boolean newlyCreated)
    {
        if (post == null)
            return;

        if (post.hasMedias())
        {
            ClientContext ctx = Rests.retrieveClientContextThreadLocal();
            int ver = (ctx == null) ? 1 : ctx.getVersion();

            if (ver < 2 && (post.getType() == PostType.AUDIO || post.getType() == PostType.TEXT)) {
                postRep.clear();
                systemPopulateUpdateNowPost(postRep, post);
                return;
            }

            if (ver < 3 && post.getType() == PostType.LINK) {
                postRep.clear();
                systemPopulateUpdateNowPost(postRep, post);
                return;
            }

            if (ver < 4 && post.getType() == PostType.VIDEO) {
                postRep.clear();
                systemPopulateUpdateNowPost(postRep, post);
                return;
            }

            if (ver < 5 && post.getType() == PostType.CARD) {
                postRep.clear();
                systemPopulateUpdateNowPost(postRep, post);
                return;
            }
        }

        populatePostPart1(postRep, post);
        populatePostPart2(postRep, post);
        populatePostPart3(postRep, post);

        if (newlyCreated) {
            postRep.voteStatus = VoteStatus.NOVOTE;
        }
        else {
            LEntry edge = _votesRel.getEdge(viewerId, post.getId());
            if (edge == null)
                postRep.voteStatus = VoteStatus.NOVOTE;
            else
                postRep.voteStatus = VoteStatus.values()[(int) edge.l1]; //ordinal
        }
    }


    public static void populatePostAdmin(
        PostRep postRep,
        Post post)
    {
        populatePostPart1(postRep, post);

        postRep.threadId = post.getThreadId();
        postRep.groupId = post.getGroupId();
        postRep.sortKey = post.getSortKey();
        postRep.sortVal = post.getSortVal();

        postRep.isFake = false;
        postRep.isGeoRestricted = false;
        if (null != post.getStatus())
        {
            PostStatus ps = post.getStatus();
            postRep.isFake = ps.isSet(PostStatus.Status.FAKE);
            postRep.isGeoRestricted = ps.isSet(PostStatus.Status.GEO_RESTRICTED);
            postRep.isAnnouncement = ps.isSet(PostStatus.Status.ANNOUNCEMENT);
        }
    }


    private static void populatePostPart1(
        PostRep postRep,
        Post post)
    {
        // Common fields
        postRep.body = post.getBody();
        postRep.clazz = post.getClazz();
        postRep.createTime = post.getCreateTime();
        postRep.hashtags = post.getHashtags();
        postRep.id = post.getId();
        postRep.type = post.getType();
        postRep.updateTime = post.getUpdateTime();
        
        // Geo
        postRep.geo = post.getGeo();
        String geoDisplayType = Configs.getStr(SystemSetting.GEO_DISPLAY_TYPE, "FINE");
        if (geoDisplayType.equals("ANON")) {
            if (StringUtils.isEmpty(postRep.geo.getName()))
                postRep.geo.setName("Anonymous");
        } else if (geoDisplayType.equals("COARSE")) {
            postRep.geo.setCity(null);
            postRep.geo.setNeighborhood(null);
        } // FINE type does nothing, return the stored geo (as accurate as possible)

        // Only populate ownerId for each post
        UserRep owner = new UserRep();
        owner.id = post.getOwnerId();
        postRep.owner = owner;

        // Counters
        PostCount count = post.getCount();
        if (count == null)
            count = new PostCount();
        postRep.numComments = count.getNumComments();
        postRep.numDownvotes = count.getNumDownvotes();
        postRep.numFlags = count.getNumFlags();
        postRep.numShares = count.getNumShares();
        postRep.numUpvotes = count.getNumUpvotes();
        postRep.numViews = count.getNumViews();

        // Bad review
        int delta = Configs.getInt(SystemSetting.POST_BADREVIEW_DELTA, 10);
        postRep.isBadReviewed = (delta > 0) && (postRep.numDownvotes - postRep.numUpvotes >= delta);

        // Status
        if (null != post.getStatus())
        {
            PostStatus ps = post.getStatus();
            postRep.isBlocked = ps.isSet(PostStatus.Status.BLOCKED);
        }

        // Media
        if (post.hasMedias())
        {
            switch (post.getType())
            {
            case AUDIO:
                postRep.audio = toMediaRep(post.getMedia(0));
                postRep.audioBackground = toMediaRep(post.getMedia(1));
                break;

            case LINK:
                postRep.link = toMediaRep(post.getMedia(0));
                break;

            case PHOTO:
                postRep.photo = toMediaRep(post.getMedia(0));
                break;

            case CARD:
            case TEXT:
                postRep.text = toMediaRep(post.getMedia(0));
                break;

            case VIDEO:
                postRep.video = toMediaRep(post.getMedia(0));
                postRep.videoThumbnail = toMediaRep(post.getMedia(1));
                break;
            }
        }
    }


    /**
     * Populate group related info
     */
    private static void populatePostPart2(
        PostRep postRep,
        Post post)
    {
        // Group
        long groupId = post.getGroupId();
        if (post.getClazz() == PostClass.THREAD) {
            // For thread post, groupId is negative, check createPost implementation details
            groupId = Math.abs(groupId);
        }

        postRep.groupId = groupId;
        Group group = Groups.get(groupId);
        if (group != null) {
            postRep.groupName = group.getName();
        }
    }


    /**
     * Populate thread related info
     */
    private static void populatePostPart3(
        PostRep postRep,
        Post post)
    {
        Long threadId = post.getThreadId();
        if (threadId == null || threadId <= 0L)
            return;

        postRep.threadId = threadId;
        if (post.isThreadRoot()) {
            LEntry entry = _hasThreadChildrenRel.getSource(threadId);
            if (entry == null)
                return;

            ThreadType type = ThreadType.valueOf(entry.s1);
            switch (type)
            {
            case CARD_GAME:
                postRep.threadAnnotation = _hasThreadChildrenRel.getNumEdges(threadId) + " Cards Played";
                break;
            default: // Populate with a generic annotation
                postRep.threadAnnotation = _hasThreadChildrenRel.getNumEdges(threadId) + " Responses";
            }
        }
    }


    public static void populatePostWeb(
        PostRep postRep,
        Post post)
    {
        populatePostPart1(postRep, post);
        populatePostPart2(postRep, post);
        postRep.voteStatus = VoteStatus.NOVOTE;
    }


    public static void populateSession(
        SessionRep sessRep,
        Session sess)
    {
        if (sess == null)
            return;

        sessRep.createTime = sess.getCreateTime();
        sessRep.notificationToken = sess.getNotificationToken();
        sessRep.ownerId = sess.getOwnerId();
        sessRep.token = sess.getToken();
        sessRep.updateTime = sess.getUpdateTime();
    }


    public static void populateThread(
        ThreadRep thRep,
        long threadId,
        LEntry entry)
    {
        if (entry != null) {
            ThreadType type = ThreadType.valueOf(entry.s1);
            String metadata = entry.s2;
            thRep.id = threadId;
            thRep.type = type;
            thRep.data = metadata;
            thRep.actionTerm = "Reply";
            thRep.title = "The Thread";
            switch (type)
            {
            case CARD_GAME:
                {
                    thRep.actionTerm = "Play a Card";
                    thRep.title = "The Game";
                }
                break;

            case NORMAL:
                {
                    thRep.id = threadId;
                    if (StringUtils.isNotEmpty(metadata)) {
                        String[] tokens = metadata.split("\\|", 4);

                        // Skip first field since it is version
                        thRep.actionTerm = tokens[1];
                        thRep.title = tokens[2];
                        if (tokens.length > 3)
                            thRep.allowedPostTypes = Arrays.asList(tokens[3].split(","));
                    }
                }
                break;
            }
        }
    }


    public static void populateUserAdmin(
        UserRep userRep,
        User user)
    {
        if (user == null)
            return;

        userRep.createTime = user.getCreateTime();
        userRep.device = user.getDevice();
        userRep.id = user.getId();
        userRep.idfa = user.getIdentifierForAdvertising();
        if (null != user.getStatus())
        {
            UserStatus us = user.getStatus();
            userRep.isAdmin = us.isSet(Status.ADMIN);
            userRep.isFake = us.isSet(Status.FAKE);
        }
        else
        {
            userRep.isAdmin = false;
            userRep.isFake = false;
        }

        // Don't bother populate more if not NORMAL user
        if (!userRep.isAdmin && !userRep.isFake)
        {
            populateUserCounts(userRep, user.getId());
            userRep.points = Users.getPoints(user.getId());
        }
        userRep.signupLat = user.getLatitude();
        userRep.signupLon = user.getLongitude();
        userRep.updateTime = user.getUpdateTime();
    }


    /**
     * Populate "BASIC" part of user representation
     */
    public static void populateUserBasic(
        UserRep userRep,
        User user,
        boolean allowPrivacy)
    {
        if (user == null)
            return;

        userRep.createTime = user.getCreateTime();
        userRep.id = user.getId();
        userRep.updateTime = user.getUpdateTime();

        if (allowPrivacy)
        {
            // userRep.email = user.getEmail();
        }
    }


    public static void populateUserCounts(
        UserRep userRep,
        long userId)
    {
        userRep.numPosts = Users.getNumCreatedPosts(userId);
    }


    public static MediaRep toMediaRep(
        Media media)
    {
        if (media == null)
            return null;

        MediaRep rep = new MediaRep(media.getType());
        switch (media.getType())
        {
        case AUDIO:
            AudioMedia am = (AudioMedia) media;
            rep.audioUrl = Medias.resolveMediaUrl(am.getUrl());
            rep.audioDuration = am.getDuration();
            rep.audioFormat = am.getFormat();
            break;

        case PHOTO:
            PhotoMedia pm = (PhotoMedia) media;
            // For now, large picture is the main one
            rep.photoUrl = Medias.resolveMediaUrl(pm.getLarge());
            rep.photoH = pm.getLargeHeight();
            rep.photoW = pm.getLargeWidth();
            rep.photoFormat = pm.getFormat();
            rep.photoThumbnailUrl = pm.getThumbnail();
            rep.photoThumbnailH = pm.getThumbnailHeight();
            rep.photoThumbnailW = pm.getThumbnailWidth();
            rep.photoUploaded = pm.isUploaded();
            break;

        case TEXT:
            TextMedia tm = (TextMedia) media;
            rep.text = tm.getText();
            rep.textBackgroundColor = tm.getTextBackgroundColor();
            rep.textColor = tm.getTextColor();
            rep.textFormat = tm.getFormat();
            break;

        case ICON:
            IconMedia im = (IconMedia) media;
            rep.iconColorHex = im.getColorHex();
            rep.iconSystemId = im.getSystemId();
            rep.iconUrl = im.getUrl();
            break;

        case LINK:
            LinkMedia lm = (LinkMedia) media;
            rep.linkDescription = lm.getDescription();
            rep.linkHost = lm.getHost();
            rep.linkTitle = lm.getTitle();
            rep.linkUrl = lm.getUrl();
            rep.linkEmbedItemFormat = lm.getEmbedItemFormat();
            rep.linkEmbedItemH = lm.getEmbedItemHeight();
            rep.linkEmbedItemW = lm.getEmbedItemWidth();
            rep.linkEmbedItemUrl = lm.getEmbedItemUrl();
            break;

        case VIDEO:
            VideoMedia vm = (VideoMedia) media;
            rep.videoFormat = vm.getFormat();
            rep.videoUrl = Medias.resolveMediaUrl(vm.getUrl());
        }
        return rep;
    }


    //
    // INTERNAL METHODS
    //

    private static void systemPopulateUpdateNowPost(
        PostRep postRep,
        Post post)
    {
        postRep.createTime = post.getCreateTime();
        postRep.geo = post.getGeo();
        postRep.id = post.getId();
        postRep.type = PostType.PHOTO;
        postRep.updateTime = post.getUpdateTime();

        // We set the post owner as Admin, so any operation on this post
        // like Star, like won't contribute to anything
        UserRep owner = new UserRep();
        owner.id = Users.ADMIN_ID;
        postRep.owner = owner;

        postRep.photo = toMediaRep(Medias.makeUpdateNowPicture());
    }
}
