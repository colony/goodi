
package gd.impl;


import gd.impl.activity.Activities;
import gd.impl.activity.ActivityFeedManager;
import gd.impl.api.ApiManager;
import gd.impl.cache.CacheManager;
import gd.impl.cache.Caches;
import gd.impl.config.ConfigManager;
import gd.impl.config.Configs;
import gd.impl.config.DetailedConfigs;
import gd.impl.config.GlobalProperties;
import gd.impl.group.Groups;
import gd.impl.identity.AccessManager;
import gd.impl.index.IndexManager;
import gd.impl.index.Indexes;
import gd.impl.media.FileManager;
import gd.impl.media.Medias;
import gd.impl.notification.MailManager;
import gd.impl.notification.NotificationManager;
import gd.impl.post.CardManager;
import gd.impl.post.Posts;
import gd.impl.queue.QueueManager;
import gd.impl.relation.RelationManager;
import gd.impl.relation.Relations;
import gd.impl.repository.Repositories;
import gd.impl.repository.RepositoryManager;
import gd.impl.sched.Scheduler;
import gd.impl.session.SessionManager;
import gd.impl.session.Sessions;
import gd.impl.template.TemplateManager;
import gd.impl.user.Users;
import gd.impl.util.Managed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import jersey.repackaged.com.google.common.collect.Lists;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

import com.google.common.base.Charsets;


@SuppressWarnings("unused")
public final class GDLifecycle
{
    //
    // System Status
    //

    /**
     * true if the system is offline, i.e. available only for admin
     * requests, false if all requests are accepted. Note that the
     * system is initially offline.
     */
    protected static volatile boolean _offlineState = true;

    /**
     * true if the system has been shutdown or is in the midst of a
     * shutdown, false if it is active (at least for admin requests)
     */
    protected static volatile boolean _shutdownState = false;

    /**
     * true if the system is in the midst of coming up (shutdown is
     * false) or going down (shutdown is true), false if startup or
     * shutdown is complete
     */
    protected static volatile boolean _transitionState = true;



    // List of startable/stoppable object
    private static List<Managed> _school;

    // Manager school
    private static AccessManager _accessMgr;
    
    private static ApiManager _apiMgr;

    private static CacheManager _cacheMgr;
    
    private static ComponentManager _compMgr;

    private static ConfigManager _configMgr;

    private static IndexManager _idxMgr;

    private static MailManager _mailMgr;

    private static NotificationManager _notifyMgr;

    private static QueueManager _qMgr;

    private static RelationManager _relMgr;

    private static RepositoryManager _repoMgr;

    private static SessionManager _sessMgr;

    private static SQLiteManager _sqlMgr;

    // Others
    private static GlobalProperties _gp;

    private static Scheduler _scheduler;



    public static boolean isOffline()
    {
        return _offlineState || _shutdownState;
    }


    public static boolean isShutdown()
    {
        return _shutdownState;
    }


    public static boolean isTransition()
    {
        return _transitionState;
    }


    public static void setOfflineMode(
        boolean offline)
    {
        _offlineState = offline;
    }


    public static void start()
    {
        // Validate the global properties
        _gp = GlobalProperties.instance();

        // Configure Logback
        initLog(_gp.configDir().resolve("logback.xml").toString());

        // Start with our banner ;)
        printBanner("Starting Goodi...");

        LOG.info("Current environment is: " + _gp.env().name());

        _school = new ArrayList<>();

        // Initialize system's components, ORDER matters
        _configMgr = ConfigManager.instance();
        _configMgr.validate();

        // Initialize Configs facade as early as possible
        Configs.init();

        _compMgr = ComponentManager.instance();
        FileManager.instance();
        _sqlMgr = SQLiteManager.instance();
        _cacheMgr = CacheManager.instance();
        _relMgr = RelationManager.instance();
        _repoMgr = RepositoryManager.instance();
        _accessMgr = AccessManager.instance();
        _apiMgr = ApiManager.instance();
        _mailMgr = MailManager.instance();
        _notifyMgr = NotificationManager.instance();
        _sessMgr = SessionManager.instance();
        _qMgr = QueueManager.instance();
        TemplateManager.instance();
        ActivityFeedManager.instance();
        CardManager.instance();

        // Initialize facades, ORDER matters
        Rests.init();
        Reps.init();
        Caches.init();
        Relations.init();
        Repositories.init();
        Medias.init();
        Groups.init();
        Posts.init();
        Sessions.init();
        Users.init();
        Activities.init();
        DetailedConfigs.init();

        _idxMgr = IndexManager.instance();
        Indexes.init();

        // Initialize scheduler
        _scheduler = Scheduler.instance();

        _school.add(_compMgr);
        _school.add(_notifyMgr);
        _school.add(_relMgr);
        _school.add(_repoMgr);
        _school.add(_qMgr);
        _school.add(_idxMgr);
        _school.add(_scheduler);
        for (Managed m : _school)
        {
            // Start each managed component as insertion order
            m.start();
        }

        initData();
        
        LOG.info("Goodi start complete");

        // Declare victory
        setOfflineMode(false);
        _transitionState = false;
    }


    public static void stop()
    {
        synchronized (GDLifecycle.class)
        {
            if (_shutdownState)
            {
                if (_transitionState)
                {
                    LOG.info("Ignoring shutdown request - shut down already in progress");
                }
                else
                {
                    LOG.info("Ignoring shutdown request - shutdown already completed");
                }

                return;
            }

            _offlineState = true;
            _shutdownState = true;
            _transitionState = true;
        }


        // Stop with our banner ;)
        printBanner("Stopping Goodi...");

        for (Managed m : Lists.reverse(_school))
        {
            // Stop each managed component in a reverse order
            m.stop();
        }

        LOG.info("Goodi stop complete");
        _transitionState = false;
    }


    private static void initData()
    {
        Users.createAdminUserIfNeed();
    }


    private static void initLog(
        String logConfigFile)
    {
        // Warn! Logger will use default one
        if (Files.notExists(Paths.get(logConfigFile)))
            return;

        // assume SLF4J is bound to logback in the current environment
        LoggerContext context =
            (LoggerContext) LoggerFactory.getILoggerFactory();

        try
        {
            JoranConfigurator configurator =
                new JoranConfigurator();
            configurator.setContext(context);
            // Call context.reset() to clear any previous configuration, e.g. default 
            // configuration. For multi-step configuration, omit calling context.reset().
            context.reset();
            configurator.doConfigure(logConfigFile);
        }
        catch (JoranException je)
        {
            // StatusPrinter will handle this
        }
        StatusPrinter.printInCaseOfErrorsOrWarnings(context);
    }


    private static void printBanner(
        String headline)
    {
        Path bannerPath = _gp.resourceDir().resolve("banner.txt");
        if (Files.exists(bannerPath))
        {
            try
            {
                String banner = new String(Files.readAllBytes(bannerPath), Charsets.UTF_8);
                LOG.info("{}\n{}", banner, headline);
            }
            catch (IOException e)
            {
                // Fall back if we lose the banner
                LOG.info(
                    "====================\n{}\n====================",
                    headline);
            }
        }
        else
        {
            LOG.info(
                "====================\n{}\n====================",
                headline);
        }
    }


    //
    // INTERNAL CONSTANTS
    //

    // A logger to use for debugging
    private static final Logger LOG =
        LoggerFactory.getLogger(GDLifecycle.class);
}
