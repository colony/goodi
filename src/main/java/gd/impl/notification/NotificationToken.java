
package gd.impl.notification;


public class NotificationToken 
{
    // Device notification token
    private String id;

    private NotificationProvider provider;


    public NotificationToken() {
    }
    
    
    public NotificationToken(NotificationProvider p, String id)
    {
        this.provider = p;
        this.id = id;
    }


    public String getId()
    {
        return id;
    }


    public void setId(
        String id)
    {
        this.id = id;
    }


    public NotificationProvider getProvider()
    {
        return provider;
    }


    public void setProvider(
        NotificationProvider provider)
    {
        this.provider = provider;
    }


    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result =
            prime * result + ((id == null) ? 0 : id.hashCode());
        result =
            prime * result
                + ((provider == null) ? 0 : provider.hashCode());
        return result;
    }


    @Override
    public boolean equals(
        Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NotificationToken other = (NotificationToken) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (provider != other.provider)
            return false;
        return true;
    }
}
