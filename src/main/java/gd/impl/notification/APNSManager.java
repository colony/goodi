
package gd.impl.notification;


import gd.impl.ComponentConfig.APNSConfig;
import gd.impl.config.Configs;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.session.SessionRepository;
import gd.impl.util.Managed;

import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsDelegate;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;
import com.notnoop.apns.DeliveryError;
import com.notnoop.apns.ReconnectPolicy;


public class APNSManager
    implements
        Managed
{
    private static final Logger LOG =
        LoggerFactory.getLogger(APNSManager.class);

    private volatile boolean apnsEnabled = false;

    private ApnsService apnsService;


    private APNSManager()
    {
    }


    protected void checkAPNSFeedback()
    {
        if (!this.apnsEnabled)
            return;

        LOG.info("Checking APNS feedback for inactive devices...");
        Map<String,Date> result =
            this.apnsService.getInactiveDevices();
        if (result != null && !result.isEmpty())
        {
            LOG.info("Return {} inactive devices", result.size());

            RepositoryManager repoMgr =
                RepositoryManager.instance();
            SessionRepository sessRepo =
                repoMgr.get(
                    RepositoryType.SESSION,
                    SessionRepository.class);
            for (String inactiveDeviceToken : result.keySet())
            {
                LOG.info(
                    "Deleting inactive device token: {}",
                    inactiveDeviceToken);

                //TODO: unset device token instead of delete
//                sessRepo.deleteByNotification(new NotificationToken(
//                    NotificationProvider.IOS,
//                    inactiveDeviceToken));
            }
        }
        else
        {
            LOG.info("Return 0 inactive devices");
        }
    }


    public boolean isAPNSEnabled()
    {
        return apnsEnabled;
    }


    public void sendBadgeCount(
        String notificationId,
        BadgeCount badge)
    {
        if (!this.apnsEnabled)
            return;

        //TODO:
        
        String payload = APNS.newPayload().badge(badge.getBadgeCount()).build();
        this.sendPayload(notificationId, payload);
    }


    @Override
    public void start()
    {
        LOG.info("APNSManager starting...");

        // Initialize APNS service
        APNSConfig cfg = Configs.getComponentConfig().getAPNS();
        if (cfg.isEnabled())
        {
            ApnsServiceBuilder b =
                APNS.newService()
                    .withCert(
                        cfg.getCertificateFile(),
                        cfg.getCertificatePassword())
                    .asPool(cfg.getNumThreads())
                    .asQueued() // non-block
                    .withReconnectPolicy(
                        ReconnectPolicy.Provided.EVERY_HALF_HOUR)
                    .withDelegate(new APNSWatcher());

            if (cfg.isSandbox())
                b.withSandboxDestination();
            else
                b.withProductionDestination();

            this.apnsService = b.build();
            // Test connection, if failed, log it and turn the disabled flag to true
            // We won't interrupt server's boot because of APNS's failure
            try
            {
                this.apnsService.testConnection();
                this.apnsEnabled = true;
                LOG.info("Succesfully test APNS connection");
            }
            catch (Throwable t)
            {
                LOG.error(
                    "Fail to access APNS server, disable it",
                    t);
                this.apnsService.stop();
            }
        }
        LOG.info("APNSManager started");
    }


    @Override
    public void stop()
    {
        LOG.info("APNSManager stopping...");
        if (this.apnsService != null)
        {
            try
            {
                this.apnsService.stop();
            }
            catch (Throwable t)
            {
                LOG.error("APNSManager failed to stop", t);
            }
        }
        LOG.info("APNSManager stopped");
    }


    //
    // INTERNAL METHODS
    //

    private void sendPayload(
        String notificationId,
        String payload)
    {
        // Send the request off to Apple
        try
        {
            this.apnsService.push(notificationId, payload);
        }
        catch (Throwable e)
        {
            LOG.error(
                "Unable to send Apple PN to device {}: {}",
                notificationId,
                e.getMessage());
            return;
        }
    }



    //
    // INTERNAL CLASSES
    //

    protected class APNSWatcher
        implements
            ApnsDelegate
    {

        @Override
        public void messageSent(
            ApnsNotification message,
            boolean resent)
        {
            LOG.debug(
                "Sent PN message {} to device {}",
                message.getIdentifier(),
                Hex.encodeHexString(message.getDeviceToken()));
        }


        @Override
        public void messageSendFailed(
            ApnsNotification message,
            Throwable e)
        {
            LOG.warn(
                "Unable to send PN message {} to device {}: {}",
                message.getIdentifier(),
                Hex.encodeHexString(message.getDeviceToken()),
                e.getMessage(),
                e);
        }


        @Override
        public void connectionClosed(
            DeliveryError err,
            int messageIdentifier)
        {
            LOG.warn(
                "Error {} for PN message with identifier {}",
                err,
                messageIdentifier);
        }


        @Override
        public void cacheLengthExceeded(
            int newCacheLength)
        {
            LOG.debug("Cache length {} exceeded", newCacheLength);
        }


        @Override
        public void notificationsResent(
            int resendCount)
        {
            LOG.debug("PN reset {}", resendCount);
        }
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final APNSManager INSTANCE =
            new APNSManager();
    }


    public static APNSManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
