package gd.impl.notification;

import gd.impl.sched.AbstractScheduledJob;

public class APNSFeedbackJob extends AbstractScheduledJob
{
    private NotificationManager notifyMgr;
    
    public APNSFeedbackJob()
    {
        this.notifyMgr = NotificationManager.instance();
    }
    
    
    @Override
    public String getId()
    {
        return "APNS-feedback";
    }

    
    @Override
    protected void executeJob()
    {
        APNSManager apns = this.notifyMgr.getAPNS();
        if (apns != null) {
            apns.checkAPNSFeedback();
        }
    }
}
