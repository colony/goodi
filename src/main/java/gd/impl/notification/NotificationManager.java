
package gd.impl.notification;


import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.session.Session;
import gd.impl.session.SessionRepository;
import gd.impl.user.Users;
import gd.impl.util.Managed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class NotificationManager
    implements
        Managed
{
    private static final Logger LOG =
        LoggerFactory.getLogger(NotificationManager.class);

    private SessionRepository sessRepo;

    /**
     * Apple PUSH notification manager
     */
    private APNSManager apnsMgr;


    private NotificationManager()
    {
        apnsMgr = APNSManager.instance();

        RepositoryManager repoMgr = RepositoryManager.instance();
        sessRepo = repoMgr.get(RepositoryType.SESSION, SessionRepository.class);
    }


    public APNSManager getAPNS()
    {
        return APNSManager.instance();
    }


    public void refreshBadgeCountOnAppIcon(
        long userId)
    {
        Session sess = this.sessRepo.findByOwner(userId);
        if (sess != null && sess.getNotificationToken() != null)
        {
            NotificationToken noti = sess.getNotificationToken();

            // Compute badge count, capping at the official maximum if necessary
            BadgeCount bc = new BadgeCount();
            bc.setTimestamp(System.currentTimeMillis());
            bc.setCountOfNumUnreadActivites(Users.getNumUnreadActivites(userId));
            bc.setCountOfNumUnreadAnnouncements(Users.getNumUnreadAnnouncements(userId));

            LOG.debug("Sending badge count {} to user {} token:{}", bc.getBadgeCount(), userId, noti.getId());
            this.apnsMgr.sendBadgeCount(noti.getId(), bc);
        }
    }


    public void resetBadgeCountOnAppIcon(
        long userId)
    {
        Session sess = this.sessRepo.findByOwner(userId);
        if (sess != null && sess.getNotificationToken() != null)
        {
            NotificationToken noti = sess.getNotificationToken();

            BadgeCount bc = new BadgeCount();
            bc.setTimestamp(System.currentTimeMillis());
            bc.setCountOfNumUnreadActivites(0);
            bc.setCountOfNumUnreadAnnouncements(0);
            this.apnsMgr.sendBadgeCount(noti.getId(), bc);
        }
    }


    public void sendDeviceNotification(
        long userId)
    {
        //TODO
    }


    @Override
    public void start()
    {
        apnsMgr.start();
    }


    @Override
    public void stop()
    {
        apnsMgr.stop();
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final NotificationManager INSTANCE =
            new NotificationManager();
    }


    public static NotificationManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
