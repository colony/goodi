
package gd.impl.notification;

import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;


/**
 * Counts of data sent over to client for badge etc
 */
public class BadgeCount
{
    private int countOfNumUnreadActivites;

    private int countOfNumUnreadAnnouncements;

    /**
     * Due to push notification service is asynchronous, client will
     * maintain its local version of badge count by calling REST API
     * such as me/status or do some action (acknowledge all
     * activities). We need send along a timestamp to indicate the
     * freshness of these counts
     */
    private long timestamp;


    public BadgeCount()
    {
    }


    /**
     * @return Total number of badge counts
     */
    public int getBadgeCount()
    {
        int maxBadgeCount = Configs.getInt(SystemSetting.MAX_DEVICE_BADGE_COUNT, 999);
        int count = 0;
        count += this.getCountOfNumUnreadActivites();
        count += this.getCountOfNumUnreadAnnouncements();
        return Math.min(maxBadgeCount, count);
    }


    public int getCountOfNumUnreadActivites()
    {
        return countOfNumUnreadActivites;
    }


    public void setCountOfNumUnreadActivites(
        int countOfNumUnreadActivites)
    {
        this.countOfNumUnreadActivites = countOfNumUnreadActivites;
    }


    public int getCountOfNumUnreadAnnouncements()
    {
        return countOfNumUnreadAnnouncements;
    }


    public void setCountOfNumUnreadAnnouncements(
        int countOfNumUnreadAnnouncements)
    {
        this.countOfNumUnreadAnnouncements = countOfNumUnreadAnnouncements;
    }


    public long getTimestamp()
    {
        return timestamp;
    }


    public void setTimestamp(
        long ts)
    {
        this.timestamp = ts;
    }
}
