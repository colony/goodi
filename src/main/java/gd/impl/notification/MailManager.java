
package gd.impl.notification;


import gd.impl.ComponentConfig.AWSConfig;
import gd.impl.ComponentConfig.MailConfig;
import gd.impl.config.Configs;
import gd.impl.template.TemplateType;
import gd.impl.util.Managed;

import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.google.common.base.Strings;


public class MailManager
    implements
        Managed
{
    private static final String CHARSET = "UTF-8";

    private static final Logger LOG =
        LoggerFactory.getLogger(MailManager.class);

    private AmazonSimpleEmailServiceClient client = null;

    /**
     * In dummy mode, no real mail will be sent, instead, only
     * logging
     */
    private boolean isDummy;


    private MailManager()
    {
        LOG.info("MailManager starting...");
        MailConfig cfg = Configs.getComponentConfig().getMail();
        this.isDummy = cfg.isDummy();
        if (!this.isDummy)
        {
            AWSConfig awsCfg =
                Configs.getComponentConfig().getAWS();
            BasicAWSCredentials creds =
                new BasicAWSCredentials(
                    awsCfg.getAccessKey(),
                    awsCfg.getSecretKey());
            this.client = new AmazonSimpleEmailServiceClient(creds);
            this.client.setRegion(Region.getRegion(cfg.getRegion()));
        }
        LOG.info("MailManager started");
    }


    public void sendMail(
        String sender,
        String recipient,
        String subject,
        String textBody,
        String htmlBody)
    {
        if (this.isDummy)
        {
            this.logDummy();
            return;
        }

        // Validate the parameters, otherwise skip email
        if (Strings.isNullOrEmpty(sender))
        {
            LOG.warn(
                "Skipping email to {} with subject {} because no sender",
                recipient,
                subject);
            return;
        }
        if (Strings.isNullOrEmpty(recipient))
        {
            LOG.warn(
                "Skipping email with subject {} because no recipient",
                subject);
            return;
        }
        if (Strings.isNullOrEmpty(subject))
        {
            LOG.warn(
                "Skipping email to {} because no subject",
                recipient);
            return;
        }

        LOG.info(
            "Send email to {} with subject {}",
            recipient,
            subject);

        Destination destination =
            new Destination().withToAddresses(recipient);
        Content subj =
            new Content()
                .withData(subject)
                .withCharset(CHARSET);
        Body body = new Body();
        if (StringUtils.isNotEmpty(htmlBody))
        {
            body.setText(
                new Content().withData(htmlBody)
                    .withCharset(CHARSET));
        }
        if (StringUtils.isNotEmpty(textBody))
        {
            body.setText(
                new Content().withData(textBody)
                    .withCharset(CHARSET));
        }

        SendEmailRequest request =
            new SendEmailRequest()
                .withSource(sender)
                .withDestination(destination)
                .withMessage(
                    new Message()
                        .withSubject(subj)
                        .withBody(body));
        try
        {
            this.client.sendEmail(request);
        }
        catch (Exception e)
        {
            LOG.error(
                "Failed to send email to {} with subject {}",
                recipient,
                subject,
                e);
        }
    }


    public void sendMail(
        String sender,
        String recipient,
        TemplateType template,
        Locale locale,
        Map<String,String> data)
    {
        //TODO
    }
    
    
    @Override
    public void start()
    {
    }
    
    
    @Override
    public void stop()
    {
        LOG.info("MailManager stopping...");
        if (this.client != null)
        {
            this.client.shutdown();
        }
        LOG.info("MailManager stopped");
    }

    
    //
    // INTERNAL METHODS
    //

    private void logDummy()
    {
        //TODO
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final MailManager INSTANCE =
            new MailManager();
    }


    public static MailManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
