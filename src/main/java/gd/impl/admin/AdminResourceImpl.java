
package gd.impl.admin;


import gd.api.v1.admin.AdminResource;
import gd.api.v1.post.PostType;
import gd.impl.GDLifecycle;
import gd.impl.Rests;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.group.Groups;
import gd.impl.media.AudioMedia;
import gd.impl.media.FileManager;
import gd.impl.media.Media;
import gd.impl.media.Medias;
import gd.impl.media.PhotoMedia;
import gd.impl.media.TextMedia;
import gd.impl.media.VideoMedia;
import gd.impl.post.Post;
import gd.impl.post.PostRepository;
import gd.impl.post.PostStatus;
import gd.impl.post.Posts;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.template.TemplateManager;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.support.jersey.BoolParam;
import gd.support.jersey.IntParam;
import gd.support.jersey.LongParam;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/adm")
@Singleton
public class AdminResourceImpl
    implements
        AdminResource
{
    private static final Logger LOG =
        LoggerFactory.getLogger(AdminResourceImpl.class);


    @Override
    @Path("/destroy")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response destroyOldPost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @FormParam("post_id_low") @DefaultValue("0") LongParam postIdLowParam,
        @FormParam("post_id_high") @DefaultValue("0") LongParam postIdHighParam,
        @FormParam("num_comments_high") @DefaultValue("0") IntParam numCommentsHighParam,
        @FormParam("num_upvotes_high") @DefaultValue("0") IntParam numUpvotesHighParam,
        @FormParam("only_fake") @DefaultValue("0") BoolParam onlyFakeParam,
        @FormParam("group_id") LongParam groupIdParam)
    {
        Rests.beginAdminOperation(headers, "adm:destroyOldPost");

        long idLow = postIdLowParam.get();
        long idHigh = postIdHighParam.get();
        int numCmtsHigh = numCommentsHighParam.get();
        int numUpHigh = numUpvotesHighParam.get();
        final boolean onlyFake = onlyFakeParam.get();

        if (idHigh <= 0L || idHigh < idLow)
            return Response.status(Status.BAD_REQUEST).entity("Invalid post_id_high:" + idHigh).build();

        String sql =
            "SELECT p.id, p.status FROM post p JOIN post_counter pc on p.id=pc.post_id WHERE p.id > ? AND p.id < ?";
        if (numCmtsHigh > 0)
            sql = sql + " AND pc.num_comments < " + numCmtsHigh;
        if (numUpHigh > 0)
            sql = sql + " AND pc.num_upvotes < " + numUpHigh;
        if (groupIdParam != null)
            sql = sql + " AND p.group_id = " + groupIdParam.get();

        final MutableInt mi = new MutableInt();
        final MutableInt mi2 = new MutableInt();
        SqliteUtils.executeSelectQuery(
            sql,
            new Object[] { idLow, idHigh },
            "destroyOldPostSelect",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next()) {
                        long postId = rs.getLong("id");
                        String postStatus = rs.getString("status");
                        if (postId <= 0L)
                            continue;

                        boolean shouldDelete = true;
                        if (onlyFake) {
                            PostStatus ps = new PostStatus(postStatus);
                            if (!ps.isSet(PostStatus.Status.FAKE))
                                shouldDelete = false;
                        }

                        // It is time to delete
                        if (shouldDelete) {
                            Posts.deletePost(postId);
                            mi2.increment();
                        }

                        mi.increment();

                        if (mi.getValue() % 500 == 0)
                            LOG.info("{} old posts have been processed", mi.getValue());
                    }
                }
            });

        LOG.info("adm:destroyOldPost finished, total {} posts deleted", mi2.getValue());

        return Response.ok().build();
    }


    @Override
    @Path("/hc")
    @GET
    @Produces("text/plain")
    public Response healthCheck()
    {
        return Response.ok("Healthy").build();
    }


    @Override
    @Path("/setting")
    @GET
    @Produces("text/plain")
    public Response getSystemSetting(
        @Context HttpHeaders headers,
        @QueryParam("key") String settingKey,
        @QueryParam("variant") String variant)
    {
        Rests.beginAdminOperation(headers, "adm:getSetting:" + settingKey + ":" + (variant == null ? "" : variant));
        return Response.ok(
            Configs.getSetting(
                SystemSetting.fromKey(settingKey),
                variant)).build();
    }


    @Override
    @Path("/operate")
    @POST
    @Produces("text/plain")
    public Response operate(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        String arg)

        throws Exception
    {
        Rests.beginAdminOperation(headers, "adm:operate");

        // Operation description:
        // Covert the specified post (arg) from photo or video to text

        if (StringUtils.isEmpty(arg))
            return Response.ok().build();

        long postId = Long.parseLong(arg);
        Post post = Posts.get(postId);
        if (post == null)
            return Response.ok().build();

        String body = post.getBody();
        if (StringUtils.isEmpty(body)) {
            LOG.info("Post {} has empty body, nothing to covert", postId);
            return Response.ok().build();
        }

        LOG.info("Convert post {} to TEXT", postId);

        PostRepository postRepo = RepositoryManager.instance().get(RepositoryType.POST, PostRepository.class);
        FileManager fileMgr = FileManager.instance();

        TextMedia media = new TextMedia();
        media.setFormat(TextMedia.Format.PLAIN);
        media.setText(body);
        media.setTextBackgroundColor(Medias.getRandomAudioBackColorHex());
        media.setTextColor("ffffff");

        List<Media> replacedMedias = post.getMedias();
        post.setMedias(null);
        post.addMedia(media);

        post.setType(PostType.TEXT);
        post.setBody(null);
        postRepo.put(post);

        if (replacedMedias != null) {
            for (Media replacedMedia : replacedMedias) {
                if (replacedMedia instanceof AudioMedia) {
                    fileMgr.deleteQuietly(((AudioMedia) replacedMedia).getUrl());
                }
                else if (replacedMedia instanceof PhotoMedia) {
                    fileMgr.deleteQuietly(((PhotoMedia) replacedMedia).getLarge());
                }
                else if (replacedMedia instanceof VideoMedia) {
                    fileMgr.deleteQuietly(((VideoMedia) replacedMedia).getUrl());
                }
            }
        }
        
        
        // Zero all badge count for existing User
//        NotificationManager notiMgr = NotificationManager.instance();
//        for (long userId = 2L; userId < 5000L; userId++) {
//            notiMgr.resetBadgeCountOnAppIcon(userId);
//        }

        return Response.ok().build();
    }


    @Override
    @Path("/setting")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response putSystemSetting(
        @Context HttpHeaders headers,
        @FormParam("key") String settingKey,
        @FormParam("variant") String variant,
        @FormParam("value") String value)
    {
        Rests.beginAdminOperation(
            headers, "adm:putSetting:" + settingKey + ":" + (variant == null ? "" : variant) + ":" + value);
        Configs.setSetting(
            SystemSetting.fromKey(settingKey),
            variant,
            value);
        
        // For some specific setting key, if we change them, we need follow-ups
        SystemSetting ss = SystemSetting.fromKey(settingKey);
        if (ss == SystemSetting.GENERAL_GROUP_ENALBED) {
            Groups.clearGroupPickListCache();
        }
        
        return Response.ok().build();
    }


    @Override
    @Path("/config/client/reload")
    @POST
    @Produces("text/plain")
    public Response reloadClientConfig(
        @Context HttpHeaders headers)
    {
        Rests.beginAdminOperation(headers, "adm:reloadClientConfig");
        Configs.reloadClientConfig();
        return Response.ok().build();
    }


    @Override
    @Path("/config/reload")
    @POST
    @Produces("text/plain")
    public Response reloadSystemConfig(
        @Context HttpHeaders headers,
        @QueryParam("name") String name)
    {
        Rests.beginAdminOperation(headers, "adm:reloadSystemConfig");
        try
        {
            Configs.reloadSystemConfig(Class.forName(name));
            return Response.ok().build();
        }
        catch (ClassNotFoundException e)
        {
            LOG.error("Fail to reload config class {}", name, e);
            return Response.serverError()
                .entity("Fail to reload config class " + name)
                .build();
        }
    }


    @Override
    @Path("/template/reload")
    @POST
    @Produces("text/plain")
    public Response reloadTemplate(
        @Context HttpHeaders headers)
    {
        Rests.beginAdminOperation(headers, "adm:reloadTemplate");
        TemplateManager.instance().reload();
        return Response.ok().build();
    }


    @Override
    @Path("/mode")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response setMode(
        @Context HttpHeaders headers,
        @FormParam("offline") BoolParam offlineParam,
        @FormParam("shutdown") BoolParam shutdownParam)
    {
        String desc = "";
        if (offlineParam != null)
            desc = "offline:" + offlineParam.get();
        else if (shutdownParam != null)
            desc = "shutdown:" + shutdownParam.get();
        Rests.beginAdminOperation(headers, "adm:setMode:" + desc);

        // The setMode can set ONLY ONE mode at a time, we will only process the
        // first non-null param here
        if (offlineParam != null)
        {
            GDLifecycle.setOfflineMode(offlineParam.get());
            return Response.ok().build();
        }

        if (shutdownParam != null)
        {
            LOG.info("Administrative shutdown requested");
            GDLifecycle.stop();
            return Response.ok().build();
        }

        return Response.status(Status.BAD_REQUEST)
            .entity("There is no mode set")
            .build();
    }
}
