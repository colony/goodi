
package gd.impl.index;


import gd.api.v1.admin.ManageIndexResource;
import gd.impl.Rests;
import gd.impl.index.IndexManager.ConcurrentBuilderException;
import gd.impl.util.CancelledException;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.impl.util.Visitor;
import gd.support.jersey.BoolParam;

import java.io.IOException;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


@SuppressWarnings({
        "rawtypes",
        "unchecked"
})
@Path("/adm/indexes")
@Singleton
public class ManageIndexResourceImpl
    implements
        ManageIndexResource
{
    protected IndexManager indexMgr;


    public ManageIndexResourceImpl()
    {
        this.indexMgr = IndexManager.instance();
    }


    @Override
    @Path("/{index}")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response addEntry(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @FormParam("entry") String entryStr,
        @FormParam("allow_update") @DefaultValue("0") BoolParam allowUpdateIfExistParam)
    {
        Rests.beginAdminOperation(headers, "adm:idx:addEntry:" + indexName + ":" + entryStr);

        Index idx = this.getIndex(indexName);
        IndexEntry entry = idx.makeEntry(entryStr);
        IndexEntry existEntry = idx.getEntry(entry.getId());
        if (existEntry == null || allowUpdateIfExistParam.get()) {
            // Note, we use put instead of add, the later will return false if already exist
            idx.put(entry);
        }
        else {
            return Response.status(Status.BAD_REQUEST).entity("Index entry already exist").build();
        }

        return Response.ok().build();
    }


    @Override
    @Path("/{index}/{id}")
    @DELETE
    public Response deleteEntry(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @PathParam("id") String idStr)
    {
        Rests.beginAdminOperation(headers, "adm:idx:delEntry:" + indexName + ":" + idStr);

        Index idx = this.getIndex(indexName);
        Object id = idx.makeId(idStr);
        idx.delete(id);

        return Response.ok().build();
    }


    @Override
    @Path("/flush")
    @POST
    public Response flush(
        @Context HttpHeaders headers)
    {
        Rests.beginAdminOperation(headers, "adm:idx:flushAll");

        try
        {
            this.indexMgr.flush();
        }
        catch (IOException e)
        {
            throw new WebApplicationException(e);
        }
        return Response.ok().build();
    }


    @Override
    @Path("/flush/{index}")
    @POST
    public Response flush(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName)
    {
        Rests.beginAdminOperation(headers, "adm:idx:flush:" + indexName);
        IndexType id;
        try
        {
            id = IndexType.fromAbbreviation(indexName);
        }
        catch (IllegalArgumentException e)
        {
            return (Response.status(Status.NOT_FOUND)
                .entity(
                    "No index with name " + indexName)
                .build());
        }

        try
        {
            this.indexMgr.flushIndex(id);
        }
        catch (IOException e)
        {
            throw new WebApplicationException(e);
        }

        return Response.ok().build();
    }


    @Override
    @Path("/{index}/{id}")
    @GET
    @Produces("text/plain")
    public Response getEntry(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @PathParam("id") String idStr)
    {
        Rests.beginAdminOperation(headers, "adm:idx:getEntry");

        Index idx = this.getIndex(indexName);
        Object id = idx.makeId(idStr);
        IndexEntry entry = idx.getEntry(id);

        if (entry == null)
            return Response.status(Status.NOT_FOUND)
                .entity("ID " + idStr + " doesn't exist in index " + indexName)
                .build();

        return Response.ok(entry.toStr()).build();
    }


    @Override
    @Path("/{index}")
    @GET
    @Produces("text/plain")
    public Response getIndex(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:idx:getIndex");
        final Page p = pageParam.get();
        Index idx = this.getIndex(indexName);
        
        final StringBuilder sb = new StringBuilder();
        idx.visit(p.boundary, p.isDescending(), p.exclusive, new Visitor<IndexEntry>() {
            private int num = 0;
            
            @Override
            public boolean visit(
                IndexEntry item)
            {
                sb.append(item.toStr());
                sb.append("\n");
                
                ++num;
                if (num > p.size)
                    return false;
                
                return true;
            }
        });
        return Response.ok(sb.toString()).build();
    }


    @Override
    @Path("/rebuild")
    @POST
    public Response rebuild(
        @Context HttpHeaders headers)
    {
        Rests.beginAdminOperation(headers, "adm:idx:rebuildAll");

        try
        {
            this.indexMgr.rebuild();
        }
        catch (ConcurrentBuilderException e)
        {
            throw new WebApplicationException(
                "Concurrent builder in progress",
                e);
        }
        catch (CancelledException e)
        {
            throw new WebApplicationException(
                "Rebuild is cancelled",
                e);
        }
        return Response.ok().build();
    }


    @Override
    @Path("/rebuild/{index}")
    @POST
    public Response rebuild(
        @Context HttpHeaders headers,
        @PathParam("index") String indexName)
    {
        Rests.beginAdminOperation(headers, "adm:idx:rebuild:" + indexName);
        IndexType id;
        try
        {
            id = IndexType.fromAbbreviation(indexName);
        }
        catch (IllegalArgumentException e)
        {
            return (Response.status(Status.NOT_FOUND)
                .entity(
                    "No index with name " + indexName)
                .build());
        }

        try
        {
            this.indexMgr.rebuildIndex(id);
        }
        catch (ConcurrentBuilderException e)
        {
            throw new WebApplicationException(
                "Concurrent builder in progress",
                e);
        }
        catch (CancelledException e)
        {
            throw new WebApplicationException(
                "Rebuild is cancelled",
                e);
        }
        return Response.ok().build();
    }


    private Index getIndex(
        String indexName)
    {
        IndexType id;
        try {
            id = IndexType.fromAbbreviation(indexName);
        }
        catch (IllegalArgumentException e) {
            throw new WebApplicationException(
                Response.status(Status.BAD_REQUEST).entity("No index with name \"" + indexName + "\"").build());
        }

        return this.indexMgr.get(id, Index.class);
    }
}
