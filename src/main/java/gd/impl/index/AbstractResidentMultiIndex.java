
package gd.impl.index;


import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.impl.index.resident.ResidentIndex;
import gd.impl.util.GDUtils;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Abstract implementation of {@link IndexAdmin}, {@link MultiIndex}
 * that maintains two or more child indexes. The child indexes are
 * all separate instances of the same implementation class.
 * <p>
 * Please use the {@link #selectChildIndex} method to choose a
 * specific child index, then make any index specific operations.
 * Most time, add or get methods of {@link Index} are used directly,
 * it won't take any effect, but specific implementation may alter
 * that behavior
 * <p>
 * NOTE: The abstract class contains ONLY subclasses of
 * {@link ResidentIndex}, though we don't enforce it on generic type
 * in interface, and guarantee each method of MultiIndex returns the
 * resident instance
 * <p>
 * The generic parameters are hard to read in a single line. Here
 * they are more neatly:
 * 
 * <pre>
 *   ID
 *   SCORE
 *   ENTRY extends AbstractIndexEntry<ID,SCORE>
 *   INDEX extends Index<ID,SCORE,ENTRY>
 * </pre>
 */
public abstract class AbstractResidentMultiIndex<ID extends Comparable<ID>,SCORE extends Comparable<SCORE>,ENTRY extends AbstractIndexEntry<ID,SCORE>,INDEX extends Index<ID,SCORE,ENTRY>>
    implements
        ResidentIndex<ID,SCORE,ENTRY>,
        IndexAdmin,
        MultiIndex<ID,SCORE,ENTRY,INDEX>
{
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractResidentMultiIndex.class);

    /**
     * The list of child indexes
     */
    protected Map<Integer,INDEX> children;

    protected int numChildren;


    /**
     * Default constructor
     */
    protected AbstractResidentMultiIndex()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    //
    // Index METHODS
    //

    @Override
    public boolean add(
        ENTRY e)
    {
        return false;
    }


    @Override
    public boolean delete(
        ENTRY entry)
    {
        return false;
    }


    @Override
    public boolean delete(
        ID id)
    {
        return false;
    }


    @Override
    public ENTRY getEntry(
        ID id)
    {
        return null;
    }


    @Override
    public void init(
        IndexConfig config)
    {
        this.children = new HashMap<>();
        this.numChildren = 0;
    }


    @Override
    public synchronized void install(
        Index<?,?,?> other)
    {
        if (!(other instanceof AbstractResidentMultiIndex)) {
            throw new IllegalArgumentException(
                "Other index has type "
                    + other.getClass().getName()
                    + " which does not implement "
                    + "AbstractResidentMultiIndex");
        }

        AbstractResidentMultiIndex<ID,SCORE,ENTRY,INDEX> armi = GDUtils.cast(other);

        // Copy over the data from the other index. Note that all of
        // the other methods in AbstractResidentMultiIndex are synchronized
        // so it is safe to do this.
        this.children = armi.children;
        this.numChildren = armi.numChildren;
    }


    @Override
    public ENTRY makeEntry(
        String str)
    {
        return null;
    }


    @Override
    public ID makeId(
        String str)
    {
        return null;
    }


    @Override
    public SCORE makeScore(
        String str)
    {
        return null;
    }


    @Override
    public ENTRY put(
        ENTRY entry)
    {
        return null;
    }


    @Override
    public void visit(
        String boundary,
        boolean descending,
        boolean exclusive,
        Visitor<ENTRY> visitor)
    {
    }


    @Override
    public synchronized void exportData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        Files.createDirectories(path.getParent()); // Create any intermediate dir if not exist

        LOG.info("Exporting index to file {}", fname);

        try (
             OutputStream os = Files.newOutputStream(path, CREATE, WRITE, TRUNCATE_EXISTING);
             BufferedOutputStream bos = new BufferedOutputStream(os);)
        {
            this.exportData(bos);
            LOG.info("Complete export index to file {}", fname);
        }
    }


    @Override
    public synchronized void exportData(
        OutputStream out)
        throws IOException
    {
        IndexExporter exporter = new IndexExporter();
        exporter.writeHeader(out);
        exporter.writeInt(out, this.numChildren);

        List<Integer> selectorIds = new ArrayList<>(this.children.keySet());
        Collections.sort(selectorIds);
        for (Integer id : selectorIds) {
            INDEX child = this.children.get(id);
            if (!(child instanceof IndexAdmin)) {
                throw new IOException("The child index \""
                    + child.getClass().getName()
                    + "\" doesn't conform to IndexAdmin");
            }

            // Write out the selector
            exporter.writeInt(out, id);

            IndexAdmin childAdmin = GDUtils.cast(child);
            childAdmin.exportData(out);
        }
    }


    @Override
    public synchronized void importData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.notExists(path))
        {
            throw new IllegalArgumentException(String.format(
                "Import file %s doesn't exist",
                fname));
        }

        LOG.info("Importing index from file {}", fname);
        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            this.importData(bis);
            LOG.info("Done import index from file {}", fname);
        }
    }


    @Override
    public synchronized void importData(
        InputStream in)
        throws IOException
    {
        IndexExporter exporter = new IndexExporter();

        // Initialize the exporter with header version internally
        exporter.readHeader(in);

        // Read num of child indexes
        int numChildren = exporter.readInt(in);

        // Import each child index
        Map<Integer,INDEX> newChildren = new HashMap<>(numChildren);
        for (int i = 0; i < numChildren; i++) {
            INDEX newChild = makeChildIndex();
            if (!(newChild instanceof IndexAdmin)) {
                throw new IOException("The child index \""
                    + newChild.getClass().getName()
                    + "\" doesn't conform to IndexAdmin");
            }

            int selectorId = exporter.readInt(in);

            IndexAdmin newChildAdmin = GDUtils.cast(newChild);
            newChildAdmin.importData(in);
            newChildren.put(selectorId, newChild);
        }

        // Swap the internal data, synchronized no worry
        this.children = newChildren;
        this.numChildren = this.children.size();
    }


    //
    // ResidentIndex METHODS
    //

    @Override
    public synchronized boolean isDirty()
    {
        if (this.children == null)
            return false;

        // The impl returns true if any of the child indexes are dirty
        for (INDEX child : this.children.values()) {
            ResidentIndex<?,?,?> childResident = GDUtils.cast(child);
            if (childResident.isDirty())
                return true;
        }
        return false;
    }


    //
    // MultiIndex METHODS
    //

    @Override
    public synchronized void addChildIndex(
        INDEX index,
        int selector)
    {
        if (!(index instanceof ResidentIndex)) {
            throw new IllegalArgumentException("Add childIndex " + selector + " but it is not ResidentIndex");
        }
        
        boolean b = this.children.containsKey(selector);
        this.children.put(selector, index);
        if (b) {
            ++this.numChildren;
        }
    }


    @Override
    public synchronized INDEX selectChildIndex(
        int selector)
    {
        // The impl selects the child index with a simple mod operation

        if (this.numChildren < 1)
            return null;
        return this.children.get(selector);
    }
}
