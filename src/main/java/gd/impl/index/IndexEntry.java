
package gd.impl.index;

import gd.impl.util.Stringifiable;


public interface IndexEntry<ID,SCORE>
    extends
        Comparable<IndexEntry<ID,SCORE>>,
        Stringifiable<IndexEntry<ID,SCORE>>
{
    /**
     * The compareTo must behave consistently with the comparison of
     * toSortValue()
     */
    @Override
    public int compareTo(
        IndexEntry<ID,SCORE> other);


    public ID getId();


    /**
     * Convert ID to string. This allows the entry be serialized on
     * network as well as storage (flush/export/import) file
     */
    public String getIdStr();


    public SCORE getScore();


    /**
     * Convert SCORE to string. This allows the entry be serialized
     * on network as well as storage (flush/export/import to file)
     */
    public String getScoreStr();


    public void setIdStr(
        String idStr);


    public void setScoreStr(
        String scoreStr);


    /**
     * Indicate the kind of sort value and algorithm of sorting
     */
    public String toSortKey();


    /**
     * Sort value. The value is transferred to/from client to enable
     * sorting and pagination
     */
    public String toSortValue();
}
