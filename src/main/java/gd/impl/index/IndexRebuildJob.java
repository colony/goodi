
package gd.impl.index;


import gd.impl.config.Configs;
import gd.impl.index.IndexManager.ConcurrentBuilderException;
import gd.impl.sched.AbstractScheduledJob;
import gd.impl.util.CancelledException;

import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class IndexRebuildJob
    extends
        AbstractScheduledJob
{
    private static final Logger LOG =
        LoggerFactory.getLogger(IndexRebuildJob.class);

    /**
     * ID of the index currently being rebuilt (if any)
     */
    private volatile IndexType currentIndex;

    private IndexManager indexMgr;


    public IndexRebuildJob()
    {
        this.currentIndex = null;
        this.indexMgr = IndexManager.instance();
    }


    @Override
    public void cancel()
    {
        super.cancel();
        if (this.currentIndex != null)
        {
            this.indexMgr.cancelRebuildIndex(this.currentIndex);
        }
    }


    @Override
    public String getId()
    {
        return "IndexRebuild";
    }


    @Override
    protected void executeJob()
    {
        LOG.info("IndexRebuildJob starting");

        int numRebuilt = 0;
        IndexesConfig iscfg = Configs.get(IndexesConfig.class);
        for (IndexConfig icfg : iscfg.getIndexesBySeqAndType())
        {
            IndexType id = icfg.getId();
            if (this.cancelled)
            {
                LOG.info("IndexRebuildJob cancelled");
                break;
            }

            Duration rebuildInterval = icfg.rebuildInterval();
            if (rebuildInterval.equals(Duration.ZERO))
                continue;

            long lastRebuildTime =
                this.indexMgr.getLastRebuildTime(id);
            if (lastRebuildTime != 0L
                && (System.currentTimeMillis() < lastRebuildTime
                    + rebuildInterval.getMillis()))
                continue;

            try
            {
                this.currentIndex = id;
                if (this.indexMgr.rebuildIndex(id))
                {
                    ++numRebuilt;
                }
            }
            catch (ConcurrentBuilderException e)
            {
                LOG.warn(
                    "Fail to rebuild index {}, another rebuild in progress",
                    id,
                    e);
            }
            catch (CancelledException e)
            {
                LOG.warn(
                    "Fail to rebuild index {}, cancelled",
                    id,
                    e);
            }
            finally
            {
                this.currentIndex = null;
            }
        }

        if (numRebuilt > 0)
        {
            LOG.info(
                "IndexRebuildJob completed, succeed to rebuild {} indexes",
                numRebuilt);
        }
        else
        {
            LOG.info("IndexRebuildJob completed, no indexes rebuilt");
        }
    }
}
