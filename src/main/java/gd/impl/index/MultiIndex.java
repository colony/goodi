
package gd.impl.index;


/**
 * Interface provides for the ability to choose among one or more
 * associated child indexes. This might be used for cases like
 * multi-draw stochastic indexes, or multi-group popular indexes,
 * etc. The choise of a specific child index is based upon an
 * integer selector value; the manner in which the selector is
 * converted to a child index depends on the implementation
 */
public interface MultiIndex<ID,SCORE,ENTRY extends IndexEntry<ID,SCORE>,INDEX extends Index<ID,SCORE,ENTRY>>
{
    /**
     * Add the specified child index
     * 
     * @param index
     *            The child index to be added to this index
     * @param selector
     *            The selector associated with index
     */
    public void addChildIndex(
        INDEX index,
        int selector);


    /**
     * Creates an empty instance of a child index
     * 
     * @return An empty child index
     */
    public INDEX makeChildIndex();


    /**
     * Chooses a specific child of this index based upon the
     * specified selector value.
     * 
     * @param selector
     *            The integer value that will be used to select a
     *            child index
     * 
     * @return The selected child index or null if there are no
     *         children
     * @throws IllegalArgumentException
     *             If the selector value is invalid
     */
    public INDEX selectChildIndex(
        int selector);
}
