
package gd.impl.index;


import gd.impl.config.Configs;
import gd.impl.config.GlobalProperties;
import gd.impl.index.resident.ResidentIndex;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SSRelation;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.util.CancelledException;
import gd.impl.util.GDUtils;
import gd.impl.util.InitializationError;
import gd.impl.util.Managed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.EnumMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


public class IndexManager
    implements
        Managed
{
    private static final long DFLT_FLUSHER_INTERVAL = 300 * 1000; //ms, 5 minute

    private static final long DFLT_INITIAL_FLUSHER_DELAY = 120 * 1000; //ms, 2 minute

    private static final Logger LOG =
        LoggerFactory.getLogger(IndexManager.class);

    /**
     * Each index local directory consists of a store file, which
     * holds the data flushed out
     */
    private static final String STORE_FILE = "_store";

    /**
     * Relation to keep track of index related information, such as
     * lastRebuildTime check Relations.ANYTHING_IDX for details
     */
    protected SSRelation anythingRel;

    private ScheduledExecutorService flushThread;

    protected EnumMap<IndexType,IndexInfo> indexes;

    protected IndexesConfig indexesConfig;

    protected EnumMap<IndexType,IndexBuilder> runningBuilders;

    /**
     * True if stop has been called, false otherwise
     */
    protected volatile boolean stopping = false;


    private IndexManager()
    {
        LOG.info("Start initialize indexes");

        this.indexes = new EnumMap<>(IndexType.class);
        this.anythingRel =
            Relations.get(RelationType.ANYTHING, SSRelation.class);
        this.runningBuilders = new EnumMap<>(IndexType.class);

        this.indexesConfig = Configs.get(IndexesConfig.class);
        int numLoaded = 0;
        for (IndexType id : IndexType.values())
        {
            IndexInfo info = new IndexInfo(id);
            info.index = initIndex(this.indexesConfig.get(id));
            info.refreshNextFlushTime();

            this.indexes.put(id, info);
            ++numLoaded;
        }

        LOG.info("Complete initialize {} indexes", numLoaded);
    }


    /**
     * The method cancel the rebuild of a particular index LAZILY.
     * It only sends the cancel signal to IndexBuilder and the
     * rebuild process might NOT be terminated immediately
     * 
     * @return true if cancel signal sent, false if IndexBuilder for
     *         specified index is not currently running
     */
    public boolean cancelRebuildIndex(
        IndexType id)
    {
        synchronized (this.runningBuilders)
        {
            IndexBuilder ib = this.runningBuilders.get(id);
            if (ib == null)
            {
                // Builder not currently running
                return false;
            }

            ib.cancel();
        }
        return true;
    }


    public void flush()
        throws IOException
    {
        if (this.stopping)
        {
            LOG.warn("Abort flush() due to stopping");
            return;
        }

        int numFlushed = 0;
        for (IndexType id : IndexType.values())
        {
            IndexInfo info = this.indexes.get(id);
            if (info == null)
                continue;

            if (flushIndex(id))
                ++numFlushed;
        }

        LOG.info("Flushed {} indexes to file", numFlushed);
    }


    /**
     * Flush the index by the specified type
     * 
     * @return true if the index was flushed, false otherwise
     */
    public boolean flushIndex(
        IndexType id)
        throws IOException
    {
        if (this.stopping)
        {
            LOG.warn("Abort flushIndex {} due to stopping", id);
            return false;
        }

        return this.flushIndexInternal(id);
    }


    public <T> T get(
        IndexType id,
        Class<T> clazz)
    {
        IndexInfo info = this.indexes.get(id);
        if (info == null)
        {
            throw new IllegalArgumentException(
                "Index \""
                    + id
                    + "\" does not exist");
        }

        return clazz.cast(info.index);
    }


    public long getLastRebuildTime(
        IndexType id)
    {
        SEntry v =
            this.anythingRel.getEdge(
                Relations.ANYTHING_IDX,
                id.abbreviation());
        if (v != null)
        {
            return Strings.isNullOrEmpty(v.s1) ? 0L : Long.parseLong(v.s1);
        }
        return 0L;
    }


    public void rebuild()
        throws ConcurrentBuilderException,
            CancelledException
    {
        if (this.stopping)
        {
            LOG.warn("Abort rebuild() due to stopping");
            return;
        }

        int numRebuilt = 0;
        for (IndexType id : IndexType.values())
        {
            IndexInfo info = this.indexes.get(id);
            if (info == null)
                continue;

            if (rebuildIndex(id))
                ++numRebuilt;
        }

        if (numRebuilt > 0)
            LOG.info("Rebuilt {} indexes", numRebuilt);
    }


    public boolean rebuildIndex(
        IndexType id)
        throws ConcurrentBuilderException,
            CancelledException
    {
        if (this.stopping)
        {
            LOG.warn("Abort rebuildIndex {} due to stopping", id);
            return false;
        }

        IndexInfo info = this.indexes.get(id);
        if (info == null)
            return false; // Weird
        if (info.type.builderClass() == null)
        {
            LOG.warn("Skip rebuild index {}, no builder found", id);
            return false;
        }

        // Create an empty new index, then hot swap
        Index<?,?,?> tmpIndex =
            initEmptyIndex(this.indexesConfig.get(id));
        populateIndex(id, tmpIndex);

        info.index.install(tmpIndex);

        // Refresh the rebuild time
        this.anythingRel.putEdge(
            Relations.ANYTHING_IDX,
            new SEntry(id.abbreviation())
                .with(String.valueOf(System.currentTimeMillis())));
        return true;
    }


    @Override
    public void start()
    {
        LOG.info("IndexManager starting...");

        this.stopping = false;
        // Spin up flusher
        this.flushThread = Executors.newScheduledThreadPool(1);
        this.flushThread.scheduleWithFixedDelay(
            new Flusher(),
            DFLT_INITIAL_FLUSHER_DELAY,
            DFLT_FLUSHER_INTERVAL, //30s
            TimeUnit.MILLISECONDS);

        LOG.info("IndexManager started");
    }


    @Override
    public void stop()
    {
        LOG.info("IndexManager stopping...");

        // A gate to prevent anything else from happening while in the middle of stopping
        this.stopping = true;

        for (IndexBuilder ib : this.runningBuilders.values())
            ib.cancel();

        for (IndexBuilder ib : this.runningBuilders.values())
        {
            LOG.info(
                "Waiting for IndexBuilder {} to finish...",
                ib.getClass().getName());
            try
            {
                ib.waitForCompletion();
            }
            catch (InterruptedException e)
            {
                LOG.warn(
                    "Interrupted while waiting for IndexBuilder {} to finish",
                    ib.getClass().getName());
                continue;
            }
            LOG.info("IndexBuilder {} has finished", ib.getClass()
                .getName());
        }

        // Shut down flusher
        LOG.info("Waiting for Index Flusher to terminate");
        this.flushThread.shutdown();
        try
        {
            // Wait until flusher done
            while (!this.flushThread.awaitTermination(
                10,
                TimeUnit.SECONDS))
                ;
        }
        catch (InterruptedException e)
        {
            LOG.warn("Interrupted while waiting for flusher to finish");
        }
        LOG.info("Index Flusher has terminated");

        // One last flush for good
        try
        {
            this.flushInternal();
        }
        catch (IOException e)
        {
            LOG.warn("Fail to do final flush", e);
        }

        LOG.info("IndexManager stopped");
    }


    //
    // INTERNAL METHODS
    //

    /**
     * Internal method of flushIndex, this could be used by Flusher
     * thread without stopping guard
     */
    private boolean flushIndexInternal(
        IndexType id)
        throws IOException
    {
        IndexInfo info = this.indexes.get(id);
        if (info == null)
            return false;

        // Index must conform to IndexAdmin interface to support serialization
        if (info.index instanceof IndexAdmin)
        {
            IndexAdmin ia = (IndexAdmin) info.index;
            if (info.index instanceof ResidentIndex)
            {
                ResidentIndex<?,?,?> ri =
                    (ResidentIndex<?,?,?>) info.index;
                if (ri.isDirty())
                {
                    Path path = getDir(id).resolve(STORE_FILE);
                    ia.exportData(path.toString());
                    return true;
                }
            }
            // Other index goes here
        }
        return false;
    }


    /**
     * Internal method of flush all indexes, this could be used by
     * Flusher thread do final flush before terminate, it ignores
     * stopping guard
     */
    private void flushInternal()
        throws IOException
    {
        int numFlushed = 0;
        for (IndexType id : IndexType.values())
        {
            IndexInfo info = this.indexes.get(id);
            if (info == null)
                continue;

            if (flushIndexInternal(id))
                ++numFlushed;
        }

        LOG.info("Flushed {} indexes to file", numFlushed);
    }


    private Index<?,?,?> initEmptyIndex(
        IndexConfig cfg)
    {
        Class<? extends Index<?,?,?>> implClass = cfg.implClass();
        Index<?,?,?> implObj;
        try
        {
            implObj = implClass.newInstance();
        }
        catch (Exception e)
        {
            throw new InitializationError(
                "Fail to create instance for Index " + cfg.getId(),
                e);
        }

        implObj.init(cfg);
        return implObj;
    }


    private Index<?,?,?> initIndex(
        IndexConfig cfg)
    {
        Index<?,?,?> index = null;

        // Initialize based on index type
        if (ResidentIndex.class.isAssignableFrom(cfg.implClass()))
        {
            if (IndexAdmin.class.isAssignableFrom(cfg.implClass())) {
                // Resident index initialized from local store file if any
                Path path = getDir(cfg.getId()).resolve(STORE_FILE);
                if (!Files.notExists(path))
                {
                    LOG.info("Importing index {}", cfg.getId());
                    // Import the data from existing index store
                    try
                    {
                        IndexAdmin ia = (IndexAdmin) initEmptyIndex(cfg);
                        ia.importData(path.toString());
                        index = (Index<?,?,?>)ia;
                    }
                    catch (IOException e)
                    {
                        LOG.warn(
                            "Fail to import data from path {} to index {}, create an empty new one",
                            path,
                            cfg.getId());
                    }
                }
            }

            if (index == null)
            {
                LOG.info("Creating new index {} buildOnCreate={}", cfg.getId(), cfg.isBuildOnCreate());

                // We reach here either because fail to import or new index
                index = initEmptyIndex(cfg);

                if (cfg.isBuildOnCreate())
                {
                    try
                    {
                        populateIndex(cfg.getId(), index);
                    }
                    catch (ConcurrentBuilderException e)
                    {
                        LOG.error(
                            "Skip builder for new index {}, another one is running",
                            cfg.getId());
                    }
                    catch (CancelledException e)
                    {
                        // This is weird situation, because, IndexManager has not been fully instantiated yet
                        // It is impossible to cancel a running builder
                        LOG.error(
                            "Builder for new index {} has been cancelled, using empty one",
                            cfg.getId());

                        // It might be partially populated, create a new empty one
                        index = initEmptyIndex(cfg);
                    }

                    // Refresh the rebuild time
                    this.anythingRel.putEdge(
                        Relations.ANYTHING_IDX,
                        new SEntry(cfg.getId().abbreviation())
                            .with(String.valueOf(System.currentTimeMillis())));
                }
            }
        }
        // Other index type goes here

        return index;
    }


    private void populateIndex(
        IndexType id,
        Index<?,?,?> index)

        throws ConcurrentBuilderException,
            CancelledException
    {
        IndexBuilder b;
        synchronized (this.runningBuilders)
        {
            // Make sure there is not another builder running for the specified index
            if (this.runningBuilders.containsKey(id))
                throw new ConcurrentBuilderException();

            // Create the builder
            Class<? extends IndexBuilder> builderClass =
                id.builderClass();
            if (builderClass == null)
                return;

            // Be aware of thrown RuntimeException
            b =
                GDUtils.getInstance(
                    builderClass.getName(),
                    IndexBuilder.class);
            this.runningBuilders.put(id, b);
        }

        try
        {
            b.populate(index);
        }
        finally
        {
            synchronized (this.runningBuilders)
            {
                this.runningBuilders.remove(id);
            }
        }
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final IndexManager INSTANCE =
            new IndexManager();
    }


    public static IndexManager instance()
    {
        return SingletonHolder.INSTANCE;
    }


    //
    // PUBLIC STATIC INTERFACE
    //

    public static Path getDir(
        IndexType id)
    {
        GlobalProperties gp = GlobalProperties.instance();
        return gp.indexDir().resolve(id.abbreviation());
    }



    //
    // INTERNAL CLASSES
    //

    /**
     * Internal data structure containing information about a single
     * managed index
     */
    private static class IndexInfo
    {
        /**
         * The flush interval in ms
         */
        public long flushInterval;

        public Index<?,?,?> index;

        /**
         * Time that next flush should perform in ms since the epoch
         */
        public long nextFlushTime;

        public IndexType type;


        public IndexInfo(
            IndexType type)
        {
            this.type = type;
        }


        public void refreshNextFlushTime()
        {
            this.nextFlushTime =
                System.currentTimeMillis() + this.flushInterval;
        }
    }



    private class Flusher
        implements
            Runnable
    {
        @Override
        public void run()
        {
            int numFlushed = 0;
            for (IndexType type : IndexType.values())
            {
                IndexInfo info = indexes.get(type);
                if (info == null)
                    continue; // Weird, shouldn't happen

                // Ignore the index if it is not yet ready for a scheduled flush
                if (info.nextFlushTime > 0L &&
                    System.currentTimeMillis() < info.nextFlushTime)
                    continue;

                try
                {
                    if (flushIndex(type))
                        ++numFlushed;
                }
                catch (Throwable e)
                {
                    LOG.error(
                        "Fail to flush index {}",
                        type.name(),
                        e);
                }
                info.refreshNextFlushTime();
            }

            LOG.info("Flusher flushed {} indexes to file", numFlushed);
        }
    }



    public static class ConcurrentBuilderException
        extends
            Exception
    {
        private static final long serialVersionUID =
            7442635529418299740L;
    }
}
