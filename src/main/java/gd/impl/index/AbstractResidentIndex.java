
package gd.impl.index;


import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.impl.index.IndexExporter.IndexExporterEntry;
import gd.impl.index.resident.ResidentIndex;
import gd.impl.util.GDUtils;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractResidentIndex<ID extends Comparable<ID>,SCORE extends Comparable<SCORE>,ENTRY extends AbstractIndexEntry<ID,SCORE>>
    implements
        ResidentIndex<ID,SCORE,ENTRY>,
        IndexAdmin
{
    protected static final Logger LOG =
        LoggerFactory.getLogger(AbstractResidentIndex.class);

    protected transient boolean dirty;

    protected TreeSet<ENTRY> entries;

    /**
     * A reverse index from ID to KEY
     */
    protected HashMap<ID,ENTRY> entriesById;

    /**
     * The lock to use for read access to the index. Multiple
     * threads may acquire this lock at the same time provided there
     * are no writers.
     */
    protected Lock readLock;

    /**
     * The parent of the read and write locks
     */
    protected ReentrantReadWriteLock rwParentLock;

    /**
     * The lock to use for write access to the index. Only one
     * thread (and no readers) can acquire this lock at the same
     * time.
     */
    protected Lock writeLock;


    /**
     * Default constructor
     */
    protected AbstractResidentIndex()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    //
    // ABSTRACT METHODS
    //


    /**
     * Make an empty entry
     */
    protected abstract ENTRY makeEmptyEntry();



    //
    // Index METHODS
    //

    @Override
    public boolean add(
        ENTRY entry)
    {
        this.writeLock.lock();
        try
        {
            ID id = entry.getId();

            // Make sure we do not already have an entry for this ID
            if (this.entriesById.containsKey(id))
            {
                return false;
            }

            // Add the entry to our collections
            this.entries.add(entry);
            this.entriesById.put(id, entry);

            // Remember to flush our data
            this.dirty = true;

            // Declare victory
            return true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean delete(
        ID id)
    {
        this.writeLock.lock();
        try
        {
            ENTRY entry = this.entriesById.get(id);
            if (entry == null)
                return false;

            return delete(entry);
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean delete(
        ENTRY entry)
    {
        this.writeLock.lock();
        try
        {
            // Delete the entry if it is in fact present
            if (!this.entries.remove(entry))
            {
                // The entry was not present to begin with
                return false;
            }

            // Remove the ID entry as well
            this.entriesById.remove(entry.getId());

            // Remember to flush our data
            this.dirty = true;

            // Declare victory
            return true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public ENTRY getEntry(
        ID id)
    {
        this.readLock.lock();
        try
        {
            return this.entriesById.get(id);
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    /**
     * Should be only called once at initialization time
     */
    @Override
    public void init(
        IndexConfig config)
    {
        this.entries = new TreeSet<>();
        this.entriesById = new HashMap<>();
        this.rwParentLock = new ReentrantReadWriteLock(true);
        this.readLock = this.rwParentLock.readLock();
        this.writeLock = this.rwParentLock.writeLock();
        // Empty equals to no dirty
        this.dirty = false;
    }


    @Override
    public void install(
        Index<?,?,?> other)
    {
        this.writeLock.lock();
        try
        {
            // Make sure the other index is an AbstractResidentIndex, TOO
            if (!(other instanceof AbstractResidentIndex))
            {
                throw new IllegalArgumentException(
                    "Other index has type "
                        + other.getClass().getName()
                        + " which does not implement "
                        + "AbstractResidentIndex");
            }

            AbstractResidentIndex<ID,SCORE,ENTRY> ari =
                GDUtils.cast(other);

            // Copy over the data from the other index. Note that
            // all of the other methods in AbstractResidentIndex are
            // lock-protected, so it is safe to do this.
            this.entries = ari.entries;
            this.entriesById = ari.entriesById;
            this.dirty = true;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public ENTRY put(
        ENTRY entry)
    {
        this.writeLock.lock();
        try
        {
            ID id = entry.getId();
            ENTRY old = this.entriesById.get(id);
            if (old != null)
            {
                // N.B. we cannot simply update the old entry, because
                //      the new key may change its ordering, and only an
                //      add can put the entry in its new position

                this.entries.remove(entry);

                // Add the new entry at new position
                this.entries.add(entry);
                this.entriesById.put(id, entry);
                this.dirty = true;
            }
            else
            {
                this.add(entry);
            }

            return old;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public void visit(
        String boundary,
        boolean descending,
        boolean exclusive,
        Visitor<ENTRY> visitor)
    {
        this.readLock.lock();
        try
        {
            if (!visitor.before())
                return;

            // Prepare a boundary ENTRY that only contains ID and SCORE 
            // for comparison and sort purpose 
            ENTRY start = null;
            if (StringUtils.isNotEmpty(boundary))
            {
                start = makeEntry(boundary);
            }

            // Compute the subset that we will be visiting
            NavigableSet<ENTRY> subset;
            if (start == null)
            {
                if (descending)
                {
                    subset = this.entries.descendingSet();
                }
                else
                {
                    subset = this.entries;
                }
            }
            else
            {
                if (descending)
                {
                    subset =
                        (this.entries.headSet(start, !exclusive)
                            .descendingSet());
                }
                else
                {
                    subset =
                        this.entries.tailSet(start, !exclusive);
                }
            }

            // Visit each entry in the selected subset
            for (ENTRY entry : subset)
            {
                if (!visitor.visit(entry))
                    break;
            }

            // Clean up
            visitor.after();
        }
        finally
        {
            this.readLock.unlock();
        }
    }


    //
    // IndexAdmin METHODS
    //

    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        Files.createDirectories(path.getParent()); // Create any intermediate dir if not exist

        LOG.info("Exporting index to file {}", fname);

        try (
             OutputStream os = Files.newOutputStream(path, CREATE, WRITE, TRUNCATE_EXISTING);
             BufferedOutputStream bos = new BufferedOutputStream(os);)
        {
            this.exportData(bos);
            this.dirty = false;
            LOG.info("Complete export index to file {}", fname);
        }
    }


    @Override
    public void exportData(
        OutputStream out)
        throws IOException
    {
        // This is a read operation since the index is not modified
        this.readLock.lock();

        try {
            IndexExporter exporter = new IndexExporter();

            exporter.writeHeader(out);
            exporter.writeInt(out, this.entries.size());

            for (ENTRY e : this.entries) {
                IndexExporterEntry iee = new IndexExporterEntry();
                iee.idStr = e.getIdStr();
                iee.scoreStr = e.getScoreStr();
                iee.s1 = e.s1;
                iee.s2 = e.s2;
                iee.s3 = e.s3;
                iee.version = e.getVersion();

                exporter.writeEntry(out, iee);
            }
        }
        finally {
            this.readLock.unlock();
        }
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.notExists(path))
        {
            throw new IllegalArgumentException(String.format(
                "Import file %s doesn't exist",
                fname));
        }

        LOG.info("Importing index from file {}", fname);
        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            this.importData(bis);
            this.dirty = false;
            LOG.info("Done import index from file {}", fname);
        }
    }


    @Override
    public void importData(
        InputStream in)
        throws IOException
    {
        IndexExporter exporter = new IndexExporter();
        exporter.readHeader(in);
        int numEntries = exporter.readInt(in);

        // Temporarily hold the imported data into memory, then swap over
        TreeSet<ENTRY> newEntries = new TreeSet<>();
        HashMap<ID,ENTRY> newEntriesById = new HashMap<>();
        for (int i = 0; i < numEntries; i++) {
            IndexExporterEntry iee = exporter.readEntry(in);
            ENTRY e = this.makeEmptyEntry();
            e.setIdStr(iee.idStr);
            e.setScoreStr(iee.scoreStr);
            e.s1 = iee.s1;
            e.s2 = iee.s2;
            e.s3 = iee.s3;
            e.setVersion(iee.version);
            newEntries.add(e);
            newEntriesById.put(e.getId(), e);
        }

        // Hot swap
        this.writeLock.lock();
        try
        {
            this.entries = newEntries;
            this.entriesById = newEntriesById;
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public boolean isDirty()
    {
        this.readLock.lock();
        try
        {
            return this.dirty;
        }
        finally
        {
            this.readLock.unlock();
        }
    }

}
