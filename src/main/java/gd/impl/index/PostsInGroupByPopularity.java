
package gd.impl.index;


import gd.impl.algorithm.PostsInGroupByPopularityAlgorithm1;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.index.IDX.LDIndex;
import gd.impl.util.CancelledException;

import org.joda.time.Duration;


public class PostsInGroupByPopularity
    extends
        AbstractIndexBuilder
{
    private PostsInGroupByPopularityAlgorithm1 algorithm;

    
    public PostsInGroupByPopularity()
    {
        this.algorithm = new PostsInGroupByPopularityAlgorithm1();
    }
    

    @Override
    public void cancel()
    {
        super.cancel();
        this.algorithm.cancel();
    }


    @Override
    protected void build(
        Index<?,?,?> index)
        throws CancelledException
    {
        boolean enabled =
            Configs.getBool(
                SystemSetting.IDX_ENABLED,
                IndexType.POSTS_IN_GROUP_BY_POPULARITY.abbreviation(),
                false);
        if (!enabled) {
            LOG.info("Index {} is disabled", this.getIndexName());
        }

        // Let the algorithm object take care of things from here. Note it may
        // throw CancelledException, which we will allow to propagate up the stack
        long start = System.currentTimeMillis();
        LDIndex theIndex = (LDIndex) index;
        this.algorithm.populate(theIndex);
        long end = System.currentTimeMillis();

        Duration elapsed = Duration.millis(end - start);
        LOG.info("Elapsed time to initizliae index {}:{}", this.getIndexName(), elapsed);
    }


    protected String getIndexName()
    {
        return IndexType.POSTS_IN_GROUP_BY_POPULARITY.name();
    }
}
