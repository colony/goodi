
package gd.impl.index;


public final class Indexes
{
    private static IndexManager _indexMgr;


    public static <T> T get(
        IndexType id,
        Class<T> clazz)
    {
        return _indexMgr.get(id, clazz);
    }


    public static void init()
    {
        _indexMgr = IndexManager.instance();
    }
}
