
package gd.impl.index;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public interface IndexAdmin
{
    public void exportData(
        OutputStream out)
        throws IOException;


    public void exportData(
        String fname)
        throws IOException;


    public void importData(
        InputStream in)
        throws IOException;


    public void importData(
        String fname)
        throws IOException;
}
