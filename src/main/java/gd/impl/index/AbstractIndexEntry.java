
package gd.impl.index;


import com.google.common.base.Strings;


public abstract class AbstractIndexEntry<ID extends Comparable<ID>,SCORE extends Comparable<SCORE>>
    implements
        IndexEntry<ID,SCORE>
{
    /**
     * The sort key for this entry and descendants is always
     * SCORE+ID (score compares first)
     */
    public static final String SK = "SCORE+ID";

    protected ID id;

    protected SCORE score;

    /**
     * The version starts with zero that is also default value.
     * Hence, the underlying storage might NOT persist default value
     * to save space.
     */
    private int version;


    /**
     * A fixed number of public strings are used as the basic
     * storage unit to contain any additional data associated with
     * the entry. The underlying storage might NOT persist the
     * string if it is null or empty
     */
    public String s1;

    public String s2;

    public String s3;


    public AbstractIndexEntry()
    {
    }
    
    
    /**
     * Clone constructor
     */
    public AbstractIndexEntry(AbstractIndexEntry<ID,SCORE> other)
    {
        this.id = other.id;
        this.score = other.score;
        this.s1 = other.s1;
        this.s2 = other.s2;
        this.s3 = other.s3;
    }


    /**
     * A special constructor taking the string from toSortValue. The
     * entry is mainly used as boundary for sorting and pagination,
     * all other auxiliary data (s1,s2..) are omitted
     */
    public AbstractIndexEntry(
        String str)
    {
        this.fromStr(str);
    }


    public AbstractIndexEntry(
        ID id,
        SCORE s)
    {
        this.id = id;
        this.score = s;
    }


    @Override
    public int compareTo(
        IndexEntry<ID,SCORE> other)
    {
        int c = this.score.compareTo(other.getScore());
        if (c != 0)
            return c;
        return this.id.compareTo(other.getId());
    }


    @Override
    public String toSortKey()
    {
        return SK;
    }


    @Override
    public String toSortValue()
    {
        return this.toStr();
    }
    
    
    //
    // Stringifiable
    //
    
    @Override
    public IndexEntry<ID,SCORE> fromStr(
        String str)
    {
        String[] tokens = str.split(":", 2);
        if (tokens.length != 2)
        {
            throw new IllegalArgumentException(
                "Invalid toSortValue string \"" + str + "\"");
        }
        this.setScoreStr(tokens[0]);
        this.setIdStr(tokens[1]);
        return this;
    }


    @Override
    public String toStr()
    {
        return this.getScoreStr() + ":" + this.getIdStr();
    }
    
    
    @Override
    public String toString()
    {
        return this.toStr();
    }


    //
    // CONVENIENT METHODS TO ACCESS STORAGE UNIT PER SCALAR TYPES
    //

    public boolean sb(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return false;
        return Boolean.parseBoolean(s(i));
    }


    public int si(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return 0;
        return Integer.parseInt(s(i));
    }


    public double sd(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return 0;
        return Double.parseDouble(s(i));
    }


    public long sl(
        int i)
    {
        if (Strings.isNullOrEmpty(s(i)))
            return 0L;
        return Long.parseLong(s(i));
    }


    //
    // GETTER & SETTER
    //

    public ID getId()
    {
        return id;
    }


    public SCORE getScore()
    {
        return score;
    }


    public void setId(
        ID id)
    {
        this.id = id;
    }


    public void setScore(
        SCORE score)
    {
        this.score = score;
    }


    public int getVersion()
    {
        return version;
    }


    public void setVersion(
        int v)
    {
        this.version = v;
    }


    //
    // INTERNAL METHODS
    //

    private String s(
        int i)
    {
        switch (i)
        {
        case 1:
            return s1;
        case 2:
            return s2;
        case 3:
            return s3;
        default:
            return null;
        }
    }
}
