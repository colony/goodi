
package gd.impl.index;


import gd.impl.util.GDUtils;

import org.joda.time.Duration;

import com.google.common.base.Strings;


public class IndexConfig
{
    public static final long DFLT_FLUSH_INTERVAL_MS = 10 * 1000;

    /**
     * NOTE: 0 or negative value means NO rebuild
     */
    public static final long DFLT_REBUILD_INTERVAL_MS = 0L;

    /**
     * Boolean flag indicate whether to build the index while
     * creating a new empty one
     */
    private boolean buildOnCreate = false;

    private String flushInterval;

    private IndexType id;

    private String implClassName;

    private String rebuidInterval;

    /**
     * The order that each index (under schedule)'s rebuild process
     * takes. The LOWER the earlier. If seq is same, use IndexType
     * abbreviation alphabetic order.
     */
    private int sequence = 0;


    /**
     * Default constructor
     */
    public IndexConfig()
    {
    }


    public IndexConfig(
        IndexType id)
    {
        this.id = id;
    }


    public Class<? extends IndexBuilder> builderClass()
    {
        return this.id.builderClass();
    }


    public Duration flushInterval()
    {
        if (Strings.isNullOrEmpty(this.flushInterval))
        {
            return new Duration(DFLT_FLUSH_INTERVAL_MS);
        }
        return GDUtils.toDuration(this.flushInterval);
    }


    public Duration rebuildInterval()
    {
        if (Strings.isNullOrEmpty(this.rebuidInterval))
        {
            return new Duration(DFLT_REBUILD_INTERVAL_MS);
        }
        return GDUtils.toDuration(this.rebuidInterval);
    }
    
    
    public String getFlushInterval()
    {
        return flushInterval;
    }

    
    public String getRebuildInterval()
    {
        return rebuidInterval;
    }
    

    public IndexType getId()
    {
        return id;
    }


    public String getImplClassName()
    {
        if (Strings.isNullOrEmpty(this.implClassName))
        {
            return id.defaultImplClass().getName();
        }
        return this.implClassName;
    }


    public int getSequence()
    {
        return sequence;
    }


    public Class<? extends Index<?,?,?>> implClass()
    {
        Class<?> rawClass;
        try
        {
            rawClass = Class.forName(this.getImplClassName());
            Class<? extends Index<?,?,?>> implClass =
                GDUtils.cast(rawClass.asSubclass(Index.class));
            return implClass;
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(
                "Fail to find the impl class for index " + id,
                e);
        }
    }


    public boolean isBuildOnCreate()
    {
        return buildOnCreate;
    }


    //
    // SETTERS USED BY Jackson YAML and IndexesConfig reload
    // OTHERWISE, please don't call these setters directly
    //

    public void setFlushInterval(
        String flushIntervalStr)
    {
        this.flushInterval = flushIntervalStr;
    }


    public void setRebuildInterval(
        String rebuildIntervalStr)
    {
        this.rebuidInterval = rebuildIntervalStr;
    }


    public void setSequence(
        int seq)
    {
        this.sequence = seq;
    }
}
