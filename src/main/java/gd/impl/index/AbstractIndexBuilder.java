
package gd.impl.index;


import gd.impl.util.CancelledException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractIndexBuilder
    implements
        IndexBuilder
{
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractIndexBuilder.class);
    
    protected volatile boolean cancelled;

    protected volatile boolean running = false;
    

    protected abstract void build(
        Index<?,?,?> index)
        throws CancelledException;


    @Override
    public void cancel()
    {
        this.cancelled = true;
    }


    @Override
    public void populate(
        Index<?,?,?> index)
        throws CancelledException
    {
        this.running = true;
        try {
            this.build(index);
        }
        finally {
            synchronized (this) {
                this.running = false;
                notifyAll();
            }
        }
    }


    @Override
    public void waitForCompletion()
        throws InterruptedException
    {
        wait();
    }
}
