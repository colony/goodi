
package gd.impl.index;


import gd.impl.algorithm.PostsByRecentPopularityAlgorithm1;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.index.IDX.LDIndex;
import gd.impl.util.CancelledException;

import org.joda.time.Duration;


public class PostsByRecentPopularity
    extends
        AbstractIndexBuilder
{
    private PostsByRecentPopularityAlgorithm1 algorithm;


    public PostsByRecentPopularity()
    {
        this.algorithm = new PostsByRecentPopularityAlgorithm1();
    }
    
    
    @Override
    public void cancel()
    {
        super.cancel();
        this.algorithm.cancel();
    }


    @Override
    protected void build(
        Index<?,?,?> index)
        throws CancelledException
    {
        boolean enabled = Configs.getBool(
            SystemSetting.IDX_ENABLED,
            IndexType.POSTS_BY_RECENT_POPULARITY.abbreviation(),
            false);
        if (!enabled) {
            LOG.info("Index {} is disabled", this.getIndexName());
            return;
        }

        // Let the algorithm object take care of things from here. Note it may
        // throw CancelledException, which we will allow to propagate up the stack
        long start = System.currentTimeMillis();
        LDIndex theIndex = (LDIndex) index;
        this.algorithm.populate(theIndex);
        long end = System.currentTimeMillis();

        Duration elapsed = Duration.millis(end - start);
        LOG.info("Elapsed time to initizliae index {}:{}", this.getIndexName(), elapsed);
    }


    protected String getIndexName()
    {
        return IndexType.POSTS_BY_RECENT_POPULARITY.name();
    }
}
