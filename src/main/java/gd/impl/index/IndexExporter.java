
package gd.impl.index;


import gd.support.protobuf.IndexProtos.IndexEntryProto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.lang3.Conversion;


/**
 * Exporter specialized for AbstractIndexEntry
 */
public class IndexExporter
{
    //
    // CONSTANTS
    //

    public static final int HEADER_MAGIC = 0x19890828;

    public static final int HEADER_VERSION = 1;

    protected int version;


    public IndexExporter()
    {
        this.version = HEADER_VERSION;
    }


    public IndexExporter(
        int ver)
    {
        this.version = ver;
    }


    public IndexExporterEntry readEntry(
        InputStream in)
        throws IOException
    {
        switch (this.version)
        {
        case 1:
            return readEntryV1(in);

        default:
            throw new IllegalStateException("Unrecognized version " + this.version);
        }
    }


    /**
     * Reads the standard header from input stream. Internally, it
     * will set the current version to the read one.
     */
    public int readHeader(
        InputStream in)
        throws IOException
    {
        byte[] b = new byte[4];
        in.read(b);
        int magic = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (magic != HEADER_MAGIC)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid magic number 0x%08x, expected 0x%08x",
                    magic,
                    HEADER_MAGIC));
        }

        b = new byte[4];
        in.read(b);
        int version = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (version <= 0 || version > HEADER_VERSION)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid version number %d, expected %d",
                    version,
                    HEADER_VERSION));
        }

        // Set the current version
        this.version = version;
        return version;
    }


    /**
     * Reads the next integer from input stream
     */
    public int readInt(
        InputStream in)
        throws IOException
    {
        byte[] b = new byte[4];
        in.read(b);
        return Conversion.byteArrayToInt(b, 0, 0, 0, 4);
    }


    public void writeEntry(
        OutputStream out,
        IndexExporterEntry entry)
        throws IOException
    {
        switch (this.version)
        {
        case 1:
            this.writeEntryV1(out, entry);
            break;
        }
    }


    public void writeHeader(
        OutputStream out)
        throws IOException
    {
        out.write(Conversion.intToByteArray(
            HEADER_MAGIC,
            0,
            new byte[4],
            0,
            4));
        out.write(Conversion.intToByteArray(
            HEADER_VERSION,
            0,
            new byte[4],
            0,
            4));
    }


    public void writeInt(
        OutputStream out,
        int i)
        throws IOException
    {
        out.write(Conversion.intToByteArray(i, 0, new byte[4], 0, 4));
    }


    //
    // INTERNAL METHODS
    //

    private IndexExporterEntry readEntryV1(
        InputStream in)
        throws IOException
    {
        IndexEntryProto.Builder b = IndexEntryProto.newBuilder();
        boolean success = b.mergeDelimitedFrom(in);
        if (success) {
            IndexEntryProto iep = b.build();
            IndexExporterEntry e = new IndexExporterEntry();
            e.idStr = iep.getId();
            e.scoreStr = iep.getScore();
            e.s1 = iep.getS1();
            e.s2 = iep.getS2();
            e.s3 = iep.getS3();
            e.version = iep.getVersion();
            return e;
        }
        return null;
    }


    private void writeEntryV1(
        OutputStream out,
        IndexExporterEntry entry)
        throws IOException
    {
        IndexEntryProto.Builder b = IndexEntryProto.newBuilder();
        b.setId(entry.idStr);
        b.setScore(entry.scoreStr);
        b.setVersion(entry.version);
        if (entry.s1 != null)
            b.setS1(entry.s1);
        if (entry.s2 != null)
            b.setS2(entry.s2);
        if (entry.s3 != null)
            b.setS3(entry.s3);
        b.build().writeDelimitedTo(out);
    }


    public static class IndexExporterEntry
    {
        public String idStr;

        public String scoreStr;

        public String s1;

        public String s2;

        public String s3;

        public int version = 0;
    }
}
