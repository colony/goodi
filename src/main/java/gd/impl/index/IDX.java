
package gd.impl.index;




/**
 * Group of Index interface and related class
 */
public final class IDX
{
    public static interface LLIndex
        extends
            Index<Long,Long,LLEntry>
    {
        /**
         * Atomic increment an entry specified by id with incr
         * score, if the entry with specified id doesn't exist, a
         * new entry with the same incr score is added
         * 
         * @return The old entry, or null if the entry doesn't exist
         */
        public LLEntry increment(
            Long id,
            Long incr);
    }



    public static interface LDIndex
        extends
            Index<Long,Double,LDEntry>
    {
        public LDEntry increment(
            Long id,
            Double incr);
    }



    public static interface MultiLDIndex
        extends
            MultiIndex<Long,Double,LDEntry,LDIndex>
    {

    }



    public static class LLEntry
        extends
            AbstractIndexEntry<Long,Long>
    {
        public LLEntry()
        {
            super();
        }


        public LLEntry(
            LLEntry other)
        {
            super(other);
        }


        public LLEntry(
            String str)
        {
            super(str);
        }


        public LLEntry(
            Long id,
            Long s)
        {
            super(id, s);
        }


        @Override
        public String getIdStr()
        {
            return String.valueOf(id);
        }


        @Override
        public String getScoreStr()
        {
            return String.valueOf(score);
        }


        @Override
        public void setIdStr(
            String s)
        {
            this.id = Long.valueOf(s);
        }


        @Override
        public void setScoreStr(
            String s)
        {
            this.score = Long.valueOf(s);
        }
    }



    public static class LDEntry
        extends
            AbstractIndexEntry<Long,Double>
    {
        public LDEntry()
        {
            super();
        }


        public LDEntry(
            LDEntry other)
        {
            super(other);
        }


        public LDEntry(
            String str)
        {
            super(str);
        }


        public LDEntry(
            Long id,
            Double s)
        {
            super(id, s);
        }


        @Override
        public String getIdStr()
        {
            return String.valueOf(id);
        }


        @Override
        public String getScoreStr()
        {
            return String.valueOf(score);
        }


        @Override
        public void setIdStr(
            String s)
        {
            this.id = Long.valueOf(s);
        }


        @Override
        public void setScoreStr(
            String s)
        {
            this.score = Double.valueOf(s);
        }
    }
}
