
package gd.impl.index.resident;


import gd.impl.index.AbstractResidentIndex;
import gd.impl.index.IDX.LDEntry;
import gd.impl.index.IDX.LDIndex;


public class ResidentLDIndex
    extends
        AbstractResidentIndex<Long,Double,LDEntry>
    implements
        LDIndex
{
    public ResidentLDIndex()
    {
        super();
    }


    //
    // LDIndex METHOD
    //

    @Override
    public LDEntry increment(
        Long id,
        Double incr)
    {
        this.writeLock.lock();
        try
        {
            LDEntry entry = this.entriesById.get(id);
            if (entry == null)
            {
                this.add(new LDEntry(id, incr));
                return null;
            }
            else
            {
                LDEntry newEntry = new LDEntry(entry);
                newEntry.setScore(newEntry.getScore() + incr);
                this.put(newEntry);
                return entry; // Return the old one
            }
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public LDEntry makeEntry(
        String str)
    {
        return new LDEntry(str);
    }
    
    
    @Override
    public Long makeId(
        String str)
    {
        return Long.valueOf(str);
    }


    @Override
    public Double makeScore(
        String str)
    {
        return Double.valueOf(str);
    }


    //
    // OVERRIDE METHOD
    //
    

    @Override
    protected LDEntry makeEmptyEntry()
    {
        return new LDEntry();
    }
}
