package gd.impl.index.resident;

import gd.impl.index.Index;
import gd.impl.index.IndexEntry;

public interface ResidentIndex<ID,SCORE,ENTRY extends IndexEntry<ID,SCORE>>
    extends
        Index<ID,SCORE,ENTRY>
{
    /**
     * Determines if any modifications have been made to this index
     * since the last time it was written.
     */
    public boolean isDirty();
}
