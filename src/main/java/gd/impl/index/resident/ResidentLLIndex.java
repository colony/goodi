
package gd.impl.index.resident;


import gd.impl.index.AbstractResidentIndex;
import gd.impl.index.IDX.LLEntry;
import gd.impl.index.IDX.LLIndex;


public class ResidentLLIndex
    extends
        AbstractResidentIndex<Long,Long,LLEntry>
    implements
        LLIndex
{
    public ResidentLLIndex()
    {
        super();
    }


    //
    // LLIndex METHOD
    //


    @Override
    public LLEntry increment(
        Long id,
        Long incr)
    {
        this.writeLock.lock();
        try
        {
            LLEntry entry = this.entriesById.get(id);
            if (entry == null)
            {
                this.add(new LLEntry(id, incr));
                return null;
            }
            else
            {
                LLEntry newEntry = new LLEntry(entry);
                newEntry.setScore(newEntry.getScore() + incr);
                this.put(newEntry);
                return entry; // Return the old one
            }
        }
        finally
        {
            this.writeLock.unlock();
        }
    }


    @Override
    public LLEntry makeEntry(
        String str)
    {
        return new LLEntry(str);
    }


    @Override
    public Long makeId(
        String str)
    {
        return Long.valueOf(str);
    }


    @Override
    public Long makeScore(
        String str)
    {
        return Long.valueOf(str);
    }


    //
    // OVERRIDE METHOD
    //

    @Override
    protected LLEntry makeEmptyEntry()
    {
        return new LLEntry();
    }
}
