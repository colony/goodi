
package gd.impl.index;

import gd.impl.util.CancelledException;


public interface IndexBuilder
{
    public void cancel();


    public void populate(
        Index<?,?,?> index)
        throws CancelledException;


    public void waitForCompletion()
        throws InterruptedException;
}
