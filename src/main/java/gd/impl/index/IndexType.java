
package gd.impl.index;


import gd.impl.index.IDX.LDIndex;
import gd.impl.index.resident.ResidentLDIndex;



public enum IndexType
{
    /**
     * Posts (show up in GLOBAL HOT FEED) scored based on a
     * combination of recentness and popularity, also, in future, we
     * might trickle placeholder slot into the index for more
     * editorial management
     * 
     * IndexEntry data:
     * 
     * <pre>
     *   ID:    post id
     *   SCORE: normalized score
     *   s1:    raw score
     *   s2:    group id
     * </pre>
     */
    POSTS_BY_RECENT_POPULARITY(
        "pbrp",
        PostsByRecentPopularity.class,
        ResidentLDIndex.class,
        LDIndex.class
    ),

    /**
     * Posts (other than group 1) scored based on a combination of
     * popularity, recentness is not honored
     * 
     * IndexEntry data:
     * 
     * <pre>
     *   ID:    post id
     *   SCORE: normalized score
     *   s1:    raw score
     *   s2:    group id
     * </pre>
     */
    POSTS_IN_GROUP_BY_POPULARITY(
        "pigbp",
        PostsInGroupByPopularity.class,
        ResidentLDIndex.class,
        LDIndex.class
    ),

    ;

    private final String abbrev;

    private final Class<? extends IndexBuilder> builderClass;

    private final Class<? extends Index<?,?,?>> dfltImplClass;

    private final Class<? extends Index<?,?,?>> specializedInterface;


    private IndexType(
        String abbrev,
        Class<? extends IndexBuilder> builderClass,
        Class<? extends Index<?,?,?>> dfltImplClass,
        Class<? extends Index<?,?,?>> specializedInterface)
    {
        this.abbrev = abbrev;
        this.builderClass = builderClass;
        this.dfltImplClass = dfltImplClass;
        this.specializedInterface = specializedInterface;
    }


    public String abbreviation()
    {
        return this.abbrev;
    }


    public Class<? extends IndexBuilder> builderClass()
    {
        return this.builderClass;
    }


    public Class<? extends Index<?,?,?>> defaultImplClass()
    {
        return this.dfltImplClass;
    }


    public Class<? extends Index<?,?,?>> specializedInterface()
    {
        return this.specializedInterface;
    }


    public static IndexType fromAbbreviation(
        String abbrev)
    {
        for (IndexType r : IndexType.values())
        {
            if (r.abbrev.equals(abbrev))
            {
                return r;
            }
        }

        throw new IllegalArgumentException(
            "No index found for abbrev \"" + abbrev + "\"");
    }
}
