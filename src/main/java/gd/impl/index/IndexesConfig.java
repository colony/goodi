
package gd.impl.index;


import gd.impl.config.Configured;
import gd.impl.config.Reloadable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;


/**
 * The configuration of indexes are partially reloadable and
 * unreloadable. Things like buildOnCreate, id, implClassName are
 * unreloadable, they will only be considered when server boots.
 * Also, you can't dynamically add or remove an index by reloading
 * the configs. Things like flushingInterval, rebuildInterval and
 * sequence are reloadable but will only take effect for next
 * rebuild/flush cycle
 */
@Configured(fname = "index.yml")
public class IndexesConfig
    implements
        Reloadable<IndexesConfig>
{
    /**
     * N.B. The setters are primarily used by Jackson
     * deserialization. Make them private so that the configuration
     * must be immutable.
     */

    private volatile EnumMap<IndexType,IndexConfig> indexes;


    /**
     * Default constructor
     */
    public IndexesConfig()
    {
    }


    public IndexConfig get(
        IndexType id)
    {
        if (this.indexes == null || !this.indexes.containsKey(id))
        {
            return new IndexConfig(id);
        }
        return this.indexes.get(id);
    }


    public List<IndexConfig> getIndexes()
    {
        if (this.indexes == null)
        {
            return Collections.emptyList();
        }
        return new ArrayList<>(this.indexes.values());
    }


    /**
     * Return IndexConfig sorted by sequence and then IndexType
     * abbreviation's alphabetic. The LOWER comes first
     */
    public List<IndexConfig> getIndexesBySeqAndType()
    {
        List<IndexConfig> configs = getIndexes();
        Collections.sort(configs, new BySeqAndType());
        return configs;
    }


    @Override
    public void reload(
        IndexesConfig t)
    {
        for (IndexConfig icr : t.getIndexes())
        {
            // Only reload the existent configuration. 
            // Any newly added or removed config are ignored
            if (this.indexes.containsKey(icr.getId()))
            {
                // Only certain config items are reloadable
                IndexConfig ic = this.indexes.get(icr.getId());
                ic.setFlushInterval(icr.getFlushInterval());
                ic.setRebuildInterval(icr.getRebuildInterval());
                ic.setSequence(icr.getSequence());
            }
        }
    }


    @SuppressWarnings("unused")
    private void setIndexes(
        List<IndexConfig> indexes)
    {
        this.indexes = new EnumMap<>(IndexType.class);
        for (IndexConfig cfg : indexes)
        {
            this.indexes.put(cfg.getId(), cfg);
        }

        // Create default configuration for missing one
        for (IndexType id : IndexType.values())
        {
            if (!this.indexes.containsKey(id))
            {
                this.indexes.put(id, new IndexConfig(id));
            }
        }
    }


    private class BySeqAndType
        implements
            Comparator<IndexConfig>
    {
        @Override
        public int compare(
            IndexConfig o1,
            IndexConfig o2)
        {
            int c =
                Integer.compare(o1.getSequence(), o2.getSequence());
            if (c != 0)
                return c;

            return o1.getId()
                .abbreviation()
                .compareTo(o2.getId().abbreviation());
        }
    }
}
