package gd.impl;

import gd.impl.activity.ActivityResourceImpl;
import gd.impl.admin.AdminResourceImpl;
import gd.impl.cache.ManageCacheResourceImpl;
import gd.impl.comment.CommentResourceImpl;
import gd.impl.comment.ManageCommentResourceImpl;
import gd.impl.config.GlobalProperties;
import gd.impl.config.GlobalProperties.ENV;
import gd.impl.group.GroupResourceImpl;
import gd.impl.group.ManageGroupResourceImpl;
import gd.impl.index.ManageIndexResourceImpl;
import gd.impl.post.FeedResourceImpl;
import gd.impl.post.ManagePostResourceImpl;
import gd.impl.post.PostResourceImpl;
import gd.impl.post.ThreadResourceImpl;
import gd.impl.queue.ManageQueueResourceImpl;
import gd.impl.relation.ManageRelationResourceImpl;
import gd.impl.repository.ManageRepositoryResourceImpl;
import gd.impl.sched.ManageSchedulerResourceImpl;
import gd.impl.session.ManageSessionResourceImpl;
import gd.impl.user.AccountResourceImpl;
import gd.impl.user.ClientResourceImpl;
import gd.impl.user.ManageUserResourceImpl;
import gd.impl.user.MeResourceImpl;
import gd.support.jersey.CrossDomainFilter;
import gd.support.jersey.JacksonObjectMapperProvider;
import gd.support.jersey.SimpleExceptionMapper;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

public class GDApplication
    extends
        ResourceConfig
{
    
    public GDApplication()
    {
        GlobalProperties gp = GlobalProperties.instance();

        // Resources (admin)
        register(new AdminResourceImpl());
        register(new ManageCacheResourceImpl());
        register(new ManageCommentResourceImpl());
        register(new ManageGroupResourceImpl());
        register(new ManageIndexResourceImpl());
        register(new ManagePostResourceImpl());
        register(new ManageQueueResourceImpl());
        register(new ManageRelationResourceImpl());
        register(new ManageRepositoryResourceImpl());
        register(new ManageSchedulerResourceImpl());
        register(new ManageSessionResourceImpl());
        register(new ManageUserResourceImpl());
        
        // Resources
        register(new AccountResourceImpl());
        register(new ActivityResourceImpl());
        register(new ClientResourceImpl());
        register(new CommentResourceImpl());
        register(new FeedResourceImpl());
        register(new GroupResourceImpl());
        register(new MeResourceImpl());
//        register(new MediaResourceImpl());
        register(new PostResourceImpl());
        register(new ThreadResourceImpl());
//        register(new UserResourceImpl());

        // Providers, Features, etc
        register(SimpleExceptionMapper.class);
        register(MultiPartFeature.class);
        register(JacksonFeature.class);
        register(JacksonObjectMapperProvider.class);
        register(OfflineFilter.class);
//        register(ThrottleHackFilter.class);
        if (gp.env() != ENV.PROD)
            register(CrossDomainFilter.class);
        
        // Properties
        property(ServerProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true);
        property(ServerProperties.METAINF_SERVICES_LOOKUP_DISABLE, true);
        property(ServerProperties.WADL_FEATURE_DISABLE, true);
        
       // property(ServerProperties.TRACING, "ALL");
    }
    
}