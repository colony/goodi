
package gd.impl.user;


import gd.api.v1.PaginationRep;
import gd.api.v1.post.PostRep;
import gd.api.v1.post.PostRepList;
import gd.api.v1.user.MeResource;
import gd.api.v1.user.StatusRep;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.post.Post;
import gd.impl.post.PostRepository;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/me")
@Singleton
public class MeResourceImpl
    implements
        MeResource
{
    protected LSRelation hasUserMetaRel;

    protected PostRepository postRepo;

    protected UserRepository userRepo;


    public MeResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.postRepo =
            repoMgr.get(RepositoryType.POST, PostRepository.class);
        this.userRepo = repoMgr.get(RepositoryType.USER,
            UserRepository.class);

        RelationManager relMgr = RelationManager.instance();
        this.hasUserMetaRel =
            relMgr.get(
                RelationType.HAS_USER_METADATA,
                LSRelation.class);
    }


    @Override
    @Path("/posts/create")
    @GET
    @Produces("application/json")
    public Response getMyPosts(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam)
    {
        long callerId = Rests.beginOperation(headers, "me:myPosts");

        Page p = pageParam.get();
        PostRepList repList = new PostRepList();
        List<Post> postList = postRepo.findByOwner(callerId, p, null);
        
        for (Post post : postList) {
            PostRep rep = new PostRep();
            Reps.populatePost(rep, post, callerId, false);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(postList),
                GDUtils.getLast(postList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }
    
    
    @Override
    @Path("/status")
    @GET
    @Produces("application/json")
    public Response getMyStatus(
        @Context HttpHeaders headers)
    {
        long callerId = Rests.beginOperation(headers, "me:status");

        // Prepare the return representation
        StatusRep sr = new StatusRep();
        
        // Unread activity
        sr.numUnreadActivities = Users.getNumUnreadActivites(callerId);
        sr.numUnreadAnnouncements = Users.getNumUnreadAnnouncements(callerId);
        
        // Points
        sr.points = Users.getPoints(callerId);

        return Response.ok(sr).build();
    }
}
