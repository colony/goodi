
package gd.impl.user;


import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.impl.repository.RepositoryConfig;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ResidentUserRepository
    implements
        UserRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ResidentUserRepository.class);

    /**
     * Indexed by ID. Deleted users will be null.
     */
    protected List<User> users;


    public ResidentUserRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public synchronized User add(
        User u)
    {
        // Assign a new ID to the user
        int id = this.users.size();
        u.setId(id);
        Date d = new Date();
        u.setCreateTime(d);
        u.setUpdateTime(d);

        // Append to the end of list
        this.users.add(u);

        return new User(u);
    }


    @Override
    public synchronized boolean delete(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        User user = this.users.get(intId);
        if (user == null)
            return false;

        // Delete the entry by setting to null so as to avoid array shifting
        this.users.set(intId, null);

        return true;
    }


    @Override
    public synchronized boolean exists(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        return this.users.get(intId) != null;
    }


    @Override
    public synchronized void exportData(
        String fname)

        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.exists(path))
        {
            LOG.warn("Export file \"{}\" already exists", fname);
        }

        // Obtain num of NON-NULL objects
        int n = 0;
        for (User user : this.users)
        {
            if (user != null)
                ++n;
        }

        LOG.info("Exporting {} users to file {}", n, fname);
        try (
             OutputStream os =
                 Files.newOutputStream(path, WRITE);
             BufferedOutputStream bos =
                 new BufferedOutputStream(os);)
        {
            UserExporter.write(bos, this.users.iterator());
        }
    }


    @Override
    public synchronized User get(
        Long id)
    {
        if (!checkBoundary(id))
            return null;

        int intId = id.intValue();
        return this.users.get(intId);
    }


    @Override
    public synchronized void importData(
        String fname)

        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.notExists(path))
        {
            throw new IllegalArgumentException(String.format(
                "Import file %s doesn't exist",
                fname));
        }

        LOG.info("Importing users from file {}", fname);
        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            Iterator<User> it = UserExporter.read(bis);

            // Wipe out current data
            this.users.clear();
            this.users.add(null); // ID 0 is always null
            this.users.add(makeAdminUser());// User ID 1

            int i = 1;
            while (it.hasNext())
            {
                User u = it.next();

                // The in-memory list adds nulls for deleted/missing users
                // Here, we need fill up the gap during import
                for (int j = i; j < u.getId(); ++j)
                {
                    this.users.add(null);
                }
                this.users.add(u);
            }
        }
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        this.users = new ArrayList<>();
        this.users.add(null);           // User ID 0 is not used
        this.users.add(makeAdminUser());// User ID 1
    }



    @Override
    public synchronized User update(
        User newUser)
    {
        if (!checkBoundary(newUser.getId()))
            return null;

        int intId = (int) newUser.getId();
        User oldUser = this.users.get(intId);
        if (oldUser == null)
        {
            return null;
        }

        //TODO: update conflict handle??

        User upUser = new User(newUser);
        upUser.setUpdateTime(new Date());
        this.users.set(intId, upUser);

        // Return a clone, so that the stored one won't be modifiable
        return new User(upUser);
    }
    

    @Override
    public void visitAll(
        Visitor<User> visitor)
    {
        // This is not synchronized, use with cautions

        if (!visitor.before())
            return;

        // We don't need to worry about outOfBoundary because
        // delete will only nullify items instead of remove
        int num = this.users.size();
        for (int i = 1; i < num; ++i)
        {
            // Maybe stale in concurrent system
            User user = this.users.get(i);

            // Ignore null
            if (user == null)
            {
                continue;
            }

            if (!visitor.visit(user))
            {
                // Visitor aborts
                break;
            }
        }

        // Clean up
        visitor.after();
    }


    //
    // INTERNAL METHODS
    //

    protected boolean checkBoundary(
        Long id)
    {
        // Internally, we store data by using an array with integer
        // position as index. The id can't go beyond boundary
        return (id != null &&
            id > 0 && id < this.users.size());
    }


    protected User makeAdminUser()
    {
        User user = new User();

        user.setId(Users.ADMIN_ID);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        return user;
    }


    @Override
    public List<User> find(
        Page page,
        Filter<User> filter)
    {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public User put(
        User t)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
