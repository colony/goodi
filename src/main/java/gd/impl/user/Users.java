
package gd.impl.user;


import gd.impl.activity.Activities;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.post.Posts;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.session.SessionManager;
import gd.impl.user.UserStatus.Status;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class Users
{
    // A logger dedicated for reward points
    private static final Logger POINT_LOG =
        LoggerFactory.getLogger("point");

    private static LLRelation _flagsRel;

    private static LSRelation _hasUserMetaRel;

    private static SLRelation _includesSpecialUserRel;

    private static EnumMap<UserStatus.Status,Set<Long>> _statusCache;

    private static ReentrantReadWriteLock _statusCacheLock;

    private static UserRepository _userRepo;


    /**
     * Bootstrap the server with Admin user whose ID is 1
     */
    public static void createAdminUserIfNeed()
    {
        if (!_userRepo.exists(Users.ADMIN_ID))
        {
            // Since ADMIN_ID is 1, that means the repo just created, add the first user as admin
            User user = new User();
            UserStatus status = new UserStatus();
            status.set(Status.ADMIN, true);
            user.setStatus(status);

            User newUser = _userRepo.add(user);
            // Verify the created user do has the expected ADMIN_ID
            if (newUser.getId() != Users.ADMIN_ID)
            {
                throw new RuntimeException(
                    "Bootstrap the admin with wrong user ID: "
                        + newUser.getId());
            }

            SessionManager.instance().openSession(Users.ADMIN_ID);
            _includesSpecialUserRel.addEdge(
                Status.ADMIN.name(),
                Users.ADMIN_ID);
        }
    }


    public static User get(
        long id)
    {
        if (id < 0L)
            return null;

        return _userRepo.get(id);
    }


    public static Set<Long> getFlaggedPostIds(
        long userId)
    {
        final Set<Long> ids = new HashSet<>();
        _flagsRel.visitAllEdges(userId, new Visitor<LEntry>() {
            @Override
            public boolean visit(
                LEntry item)
            {
                ids.add(item.getId());
                return true;
            }
        });
        return ids;
    }


    public static long getNumCreatedPosts(
        long userId)
    {
        // TODO
        return 0L;
    }


    public static int getNumUnreadActivites(
        long userId)
    {
        return (int) Activities.getUnreadCount(userId);
    }


    public static int getNumUnreadAnnouncements(
        long userId)
    {
        SEntry node = _hasUserMetaRel.getEdge(userId, Relations.HUM_ANNOUNCEMENT_INFO);
        long lastReadId = 0L;
        if (node != null)
            lastReadId = node.l1;
        int numUnread = 0;
        for (Long id : Posts.getAnnouncementPostIds()) {
            if (id > lastReadId)
                numUnread++;
        }
        return numUnread;
    }


    public static long getPoints(
        long userId)
    {
        SEntry pointInfoEdge =
            _hasUserMetaRel.getEdge(
                userId,
                Relations.HUM_POINT_INFO);
        return pointInfoEdge != null ? pointInfoEdge.l1 : 0L;
    }


    public static boolean hasStatus(
        long userId,
        UserStatus.Status status)
    {
        if (status == Status.ADMIN ||
            status == Status.FAKE) {
            _statusCacheLock.readLock().lock();
            try {
                Set<Long> cache = _statusCache.get(status);
                if (cache != null)
                    return cache.contains(userId);
                else
                    return false;
            }
            finally {
                _statusCacheLock.readLock().unlock();
            }
        }
        // Otherwise, go to persistent source
        return _includesSpecialUserRel.hasEdge(status.name(), userId);
    }


    public static void init()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        _userRepo =
            repoMgr.get(RepositoryType.USER, UserRepository.class);

        RelationManager relMgr = RelationManager.instance();
        _flagsRel = relMgr.get(RelationType.FLAGS, LLRelation.class);
        _hasUserMetaRel =
            relMgr.get(
                RelationType.HAS_USER_METADATA,
                LSRelation.class);
        _includesSpecialUserRel =
            relMgr.get(
                RelationType.INCLUDES_SPECIAL_USER,
                SLRelation.class);

        _statusCacheLock = new ReentrantReadWriteLock(true);
        _statusCache = new EnumMap<>(UserStatus.Status.class);
        for (final UserStatus.Status status : UserStatus.Status.values()) {
            // We only cache certain status
            if (status == Status.ADMIN ||
                status == Status.FAKE)
            {
                _statusCache.put(status, new HashSet<Long>());
                _includesSpecialUserRel.visitAllEdges(
                    status.name(),
                    new Visitor<LEntry>() {
                        @Override
                        public boolean visit(
                            LEntry item)
                        {
                            _statusCache.get(status).add(item.getId());
                            return true;
                        }
                    });
            }
        }
    }


    /**
     * Determine if an user is eligible for activity, notification
     * or etc. Note that, some fake/admin users are excluded
     */
    public static boolean isAlertable(
        long userId)
    {
        if (hasStatus(userId, UserStatus.Status.ADMIN))
            return false;
        if (hasStatus(userId, UserStatus.Status.FAKE))
            return false;

        User user = get(userId);
        if (user == null)
            return false;
        return true;
    }


    /**
     * Log and update the PointEvent occurs on user specified by
     * userId, the details are varied and provided per PointEvent
     * 
     * @param userId
     * @param event
     * @param points
     *            A custom number of points earned. This override
     *            the one from system setting. Ignored if the param
     *            is zero.
     * @param details
     *            A variable-length object array providing auxiliary
     *            info per different PointEvent
     * 
     * @return The updated (maybe) points of user specified by
     *         userId. 0 if the user doesn't exist
     */
    public static void logPointEvent(
        long userId,
        PointEvent event,
        long points,
        Object... details)
    {
        // Fetch the points from configuration, if not set, fall back to defaults
        long pts = points;
        if (pts == 0) {
            pts = Configs.getInt(
                SystemSetting.POINTS,
                event.name(),
                event.points);
        }

        if (pts != 0)
        {
            if (event == PointEvent.ON_BOARDING) {
                _hasUserMetaRel.addEdge(
                    userId,
                    new SEntry(Relations.HUM_POINT_INFO).with(pts));
            }
            else {
                _hasUserMetaRel.modifyEdgeByData(
                    userId,
                    Relations.HUM_POINT_INFO,
                    null,
                    null,
                    null,
                    pts,
                    false,
                    null,
                    false);
            }
        }

        // LOG for internal journaling purpose
//        StringBuffer sb = new StringBuffer();
//        sb.append(event.name());
//        sb.append(",");
//        sb.append(pts);
//        sb.append(",");
//        sb.append(userId);
//        for (int i = 0; i < details.length; ++i)
//        {
//            sb.append(",");
//            sb.append(String.valueOf(details[i]));
//        }
//        POINT_LOG.info(sb.toString());
    }


    public static void setStatus(
        long userId,
        UserStatus.Status status,
        boolean unset)

        throws UpdateConflictException
    {
        User user = get(userId);
        if (user == null)
            return; // Do nothing

        UserStatus us = user.getStatus();
        if (us == null)
            us = new UserStatus();

        if (unset ^ us.isSet(status))
            return;

        us.set(status, !unset);
        user.setStatus(us);
        _userRepo.update(user);

        // For certain status, we persist and cache it
        switch (status)
        {
        case ADMIN:
        case FAKE:
            if (unset)
                _includesSpecialUserRel.deleteEdge(status.name(), userId);
            else
                _includesSpecialUserRel.addEdge(status.name(), userId);

            _statusCacheLock.writeLock().lock();
            try {
                Set<Long> cache = _statusCache.get(status);
                if (cache != null) {
                    if (unset)
                        cache.remove(userId);
                    else
                        cache.add(userId);
                }
            }
            finally {
                _statusCacheLock.writeLock().unlock();
            }
            break;

        default:
        }
    }


    /**
     * The Official user ID of the built-in admin user. Other users
     * may also be administrators by being grant admin rights.
     */
    public static final long ADMIN_ID = 1L;
}
