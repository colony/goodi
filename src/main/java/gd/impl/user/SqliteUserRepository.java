
package gd.impl.user;


import gd.api.v1.ClientDevice;
import gd.api.v1.user.UserSort;
import gd.impl.SQLiteManager;
import gd.impl.repository.RepositoryConfig;
import gd.impl.repository.SqliteRepository;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


/**
 * Schema (user):
 * <ul>
 * <li>create_time
 * <li>device
 * <li>id
 * <li>idfa
 * <li>lat
 * <li>lon
 * <li>status
 * <li>update_time
 * </ul>
 */
public class SqliteUserRepository
    implements
        UserRepository,
        SqliteRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SqliteUserRepository.class);

    private int batchSize = 100;


    public SqliteUserRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public User add(
        User u)
    {
        String sql =
            "INSERT INTO user (create_time,device,idfa,lat,lon,status,update_time) VALUES (?,?,?,?,?,?,?)";

        User newUser = new User(u);
        newUser.setCreateTime(new Date());
        newUser.setUpdateTime(new Date());

        // SQL statement parameters construction
        Object param[] = new Object[7];
        param[0] = newUser.getCreateTime().getTime();
        param[1] = newUser.getDevice().name();
        param[2] = newUser.getIdentifierForAdvertising();
        param[3] = newUser.getLatitude();
        param[4] = newUser.getLongitude();
        if (null != newUser.getStatus())
            param[5] = newUser.getStatus().toString();
        param[6] = newUser.getUpdateTime().getTime();

        // Execute SQL
        long newId =
            SqliteUtils.executeInsertQuery(
                sql,
                param,
                "addUser",
                true);
        newUser.setId(newId);
        return newUser;
    }


    @Override
    public boolean delete(
        Long id)
    {
        String sql = "DELETE FROM user WHERE id = ?";
        return SqliteUtils.executeQuery(
            sql,
            new Object[] {
                id
            },
            "deleteUser");
    }


    @Override
    public boolean exists(
        Long id)
    {
        if (id <= 0) return false;
        final String sql = "SELECT 1 FROM user WHERE id=?";
        Object[] param = {
                id
        };
        final MutableBoolean mb = new MutableBoolean(false);

        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "isUserExist",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }

            });
        return mb.booleanValue();
    }


    @Override
    public List<User> find(
        Page page,
        Filter<User> filter)
    {
        final String key =
            StringUtils.defaultIfEmpty(
                page.key,
                UserSort.ID.abbreviation());
        UserSort sort = UserSort.fromAbbreviation(key);
        switch (sort)
        {
        case ID:
            return this.findById(page, filter);

        default:
            throw new IllegalArgumentException(
                "Unsupported sort key " + key);
        }
    }


    private List<User> findById(
        Page page,
        Filter<User> filter)
    {
        // SELECT u.*
        // FROM user u
        // WHERE u.id <=> ?
        // ORDER BY u.id desc|asc
        // LIMIT ?

        final List<User> users = new ArrayList<>(page.size);
        String sql;
        if (Strings.isNullOrEmpty(page.boundary))
        {
            sql =
                "SELECT u.* "
                    + "FROM user u "
                    + "ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }
        else
        {
            sql =
                "SELECT u.* "
                    + "FROM user u "
                    + "WHERE id "
                    + (page.isDescending() == page.isAfter() ? (page.exclusive ? "<?" : "<=?") : (page.exclusive ? ">?" : ">=?"))
                    + " ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }

        final String sortKey = UserSort.ID.abbreviation();
        SqliteUtils.executeSelectQuery(
            sql,
            Strings.isNullOrEmpty(page.boundary) ? null : new Object[] {
                    page.boundary
            },
            "findById",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        User u = new User();
                        parseResultSet(u, rs);
                        u.setSortKey(sortKey);
                        u.setSortVal(String.valueOf(u.getId()));
                        users.add(u);
                    }
                }
            });

        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */
        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(users);
        }

        return users;
    }


    @Override
    public User get(
        Long id)
    {
        final String sql = "SELECT * FROM user WHERE id=?";
        Object[] param = {
                id
        };

        final MutableObject<User> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "getUser",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        User user = new User();
                        parseResultSet(user, rs);
                        mo.setValue(user);
                    }
                }
            });

        return mo.getValue();
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        createUserTable(config);
        this.batchSize = config.getBatchSize();
    }


    @Override
    public User put(
        User u)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public User update(
        User u)

        throws UpdateConflictException
    {
        // MODIFIABLE fields:
        // device, idfa, lat, lon, status

        // NON-MODIFIABLE fields:
        // create_time, id

        // update_time is modifiable, but we also use the old data as optimistic version lock
        User upUser = new User(u);

        Date oldUpdateTime = u.getUpdateTime();
        Date newUpdateTime = new Date();
        upUser.setUpdateTime(newUpdateTime);
        Object[] param = new Object[9];

        // Prepare a query that updates mutable fields
        String updateSql =
            "UPDATE user SET device=?, idfa=?, lat=?, lon=?, status=?, update_time=? ";
        param[0] = upUser.getDevice().name();
        param[1] = upUser.getIdentifierForAdvertising();
        param[2] = upUser.getLatitude();
        param[3] = upUser.getLongitude();
        if (upUser.getStatus() != null)
            param[4] = upUser.getStatus().toString();
        param[5] = newUpdateTime.getTime();

        // Prepare a query that matches on immutable field
        String querySql =
            "WHERE id=? AND create_time=? AND update_time=?";
        param[6] = upUser.getId();
        param[7] = upUser.getCreateTime().getTime();
        param[8] = oldUpdateTime.getTime();

        String sql = updateSql + querySql;
        boolean success =
            SqliteUtils.executeQuery(sql, param, "updateUser");
        if (!success)
        {
            // Find out if the entity exists
            User user = this.get(upUser.getId());
            if (user == null)
                return null;

            if (user.getUpdateTime().compareTo(oldUpdateTime) != 0)
                throw new UpdateConflictException(
                    "Update conflict - specified update time: "
                        + oldUpdateTime
                        + "  current update time: "
                        + user.getUpdateTime());

            // If we make it here, it must be the case that an immutable field was changed
            throw new IllegalArgumentException(
                "Attempt to modify an immutable field");
        }

        return upUser;
    }


    @Override
    public void visitAll(
        Visitor<User> visitor)
    {
        if (!visitor.before())
            return;

        SQLiteManager sqliteManager = SQLiteManager.instance();
        Connection connection = null;
        PreparedStatement statement = null;

        boolean aborted = false;
        boolean hasMore = true;
        long id = 0;

        try
        {
            connection = sqliteManager.getConnection();
            final String sql =
                "select * from user where id>? limit "
                    + this.batchSize;

            statement =
                connection.prepareStatement(sql);
            while (hasMore && !aborted)
            {
                statement.setLong(1, id);
                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next())
                {
                    count++;
                    id = rs.getLong("id");

                    User user = new User();
                    parseResultSet(user, rs);
                    if (!visitor.visit(user))
                    {
                        aborted = true;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Fail to execute query \"visitAll\"",
                e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for visitAll",
                    e);
            }
        }

        visitor.after();
    }


    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }


    //
    // INTERNAL METHODS
    //

    private void createUserTable(
        RepositoryConfig config)
    {
        final String createUserTableSql =
            "create table if not exists "
                + config.getTableName()
                + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                +
                "create_time INTEGER," +
                "device TEXT," +
                "idfa TEXT," +
                "lat REAL," +
                "lon REAL," +
                "status TEXT," +
                "update_time INTEGER" +
                ")";
        SqliteUtils.executeQuery(
            createUserTableSql,
            null,
            "CreateUserTable");
    }


    private void parseResultSet(
        User user,
        ResultSet rs)
        throws SQLException
    {
        user.setCreateTime(new Date(rs.getLong("create_time")));
        if (StringUtils.isNotEmpty(rs.getString("device")))
        {
            user.setDevice(ClientDevice.valueOf(rs.getString("device")));
        }
        else
        {
            user.setDevice(ClientDevice.UNIDENTIFIED);
        }
        user.setId(rs.getLong("id"));
        user.setIdentifierForAdvertising(rs.getString("idfa"));
        user.setLatitude(rs.getDouble("lat"));
        user.setLongitude(rs.getDouble("lon"));
        if (StringUtils.isNotEmpty(rs.getString("status")))
            user.setStatus(new UserStatus(rs.getString("status")));
        user.setUpdateTime(new Date(rs.getLong("update_time")));
    }
}
