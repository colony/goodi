
package gd.impl.user;


import gd.api.v1.PaginationRep;
import gd.api.v1.admin.ManageUserResource;
import gd.api.v1.comment.CommentRep;
import gd.api.v1.comment.CommentRepList;
import gd.api.v1.post.PostRep;
import gd.api.v1.post.PostRepList;
import gd.api.v1.user.UserRep;
import gd.api.v1.user.UserRepList;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.comment.Comment;
import gd.impl.comment.CommentRepository;
import gd.impl.post.Post;
import gd.impl.post.PostRepository;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.SLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.session.SessionManager;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;
import gd.support.jersey.LongParam;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/adm/users")
@Singleton
public class ManageUserResourceImpl
    implements
        ManageUserResource
{
    private static final Logger LOG = LoggerFactory.getLogger("admin");
    
    protected CommentRepository cmtRepo;
    
    protected SLRelation includesSpecialUserRel;

    protected PostRepository postRepo;
    
    protected UserRepository userRepo;


    public ManageUserResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.cmtRepo = repoMgr.get(RepositoryType.COMMENT, CommentRepository.class);
        this.postRepo = repoMgr.get(RepositoryType.POST, PostRepository.class);
        this.userRepo = repoMgr.get(RepositoryType.USER, UserRepository.class);

        RelationManager relMgr = RelationManager.instance();
        this.includesSpecialUserRel =
            relMgr.get(
                RelationType.INCLUDES_SPECIAL_USER,
                SLRelation.class);
    }
    
    
    @Override
    @POST
    @Path("/fake")
    @Produces("application/json")
    public Response createFakeUser(
        @Context HttpHeaders headers)
    {
        Rests.beginAdminOperation(headers, "adm:muser:createFake");
        
        User user = new User();
        User newUser = this.userRepo.add(user);
        try {
            Users.setStatus(newUser.getId(), UserStatus.Status.FAKE, false);
        }
        catch (UpdateConflictException e) {
            LOG.error("Fail to update FAKE to user ID={}", newUser.getId(), e);
            return Response.status(Status.CONFLICT).build();
        }
        
        SessionManager.instance().openSession(newUser.getId());
        
        UserRep rep = new UserRep();
        Reps.populateUserAdmin(rep, newUser);
        return Response.ok(rep).build();
    }

    
    @Override
    @GET
    @Path("/{id}/comments")
    @Produces("application/json")
    public Response getUserCommentList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:muser:getUserCmtList");
        long userId = idParam.get();
        GDUtils.check(
            this.userRepo.exists(userId),
            Status.NOT_FOUND,
            "No user with ID " + userId);
        
        Page p = pageParam.get();
        List<Comment> cmtList = this.cmtRepo.findByOwner(userId, p, null);
        CommentRepList repList = new CommentRepList();
        for (Comment cmt : cmtList)
        {
            CommentRep rep = new CommentRep();
            Reps.populateCommentAdmin(rep, cmt);
            repList.add(rep);
        }
        
        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(cmtList),
                GDUtils.getLast(cmtList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }
    

    @Override
    @GET
    @Produces("application/json")
    public Response getUserList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("status") UserStatus.Status statusFilter,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:muser:getUserList");

        // For simplicity, we don't do pagination on status filtered list, that means, we fetch all
        // Until it becomes a performance issue, we come back later
        if (statusFilter != null)
            return this.getUserListByStatus(statusFilter);

        Page p = pageParam.get();

        List<User> userList = this.userRepo.find(p, null);
        UserRepList repList = new UserRepList();
        for (User user : userList)
        {
            UserRep rep = new UserRep();
            Reps.populateUserAdmin(rep, user);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(userList),
                GDUtils.getLast(userList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }
    
    
    @Override
    @GET
    @Path("/{id}/posts")
    @Produces("application/json")
    public Response getUserPostList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:muser:getUserPostList");
        long userId = idParam.get();
        GDUtils.check(
            this.userRepo.exists(userId),
            Status.NOT_FOUND,
            "No user with ID " + userId);
        
        Page p = pageParam.get();
        PostRepList repList = new PostRepList();
        List<Post> postList = this.postRepo.findByOwner(userId, p, null);
        
        for (Post post : postList) {
            PostRep rep = new PostRep();
            Reps.populatePostAdmin(rep, post);
            repList.add(rep);
        }
        
        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(postList),
                GDUtils.getLast(postList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }


    private Response getUserListByStatus(
        UserStatus.Status status)
    {
        final UserRepList repList = new UserRepList();
        this.includesSpecialUserRel.visitAllEdges(
            status.name(),
            new Visitor<LEntry>() {

                @Override
                public boolean visit(
                    LEntry item)
                {
                    User user = Users.get(item.getId());
                    if (user == null)
                        return true;

                    UserRep rep = new UserRep();
                    Reps.populateUserAdmin(rep, user);
                    repList.add(rep);
                    return true;
                }
            });
        return Response.ok(repList).build();
    }
}
