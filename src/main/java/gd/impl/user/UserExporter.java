
package gd.impl.user;


import gd.support.protobuf.UserProtos.UserProto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import org.apache.commons.lang3.Conversion;

import com.google.common.collect.AbstractIterator;


public class UserExporter
{

    public static Iterator<User> read(
        InputStream in)

        throws IOException
    {
        int v = readHeader(in);
        switch (v)
        {
        case 1:
            return new V1Iterator(in);

        default:
            throw new IllegalStateException(
                "Unsupported version number: " + v);
        }
    }


    public static void write(
        OutputStream out,
        Iterator<User> it)

        throws IOException
    {
        writeHeader(out);

        UserProto.Builder b = UserProto.newBuilder();
        while (it.hasNext())
        {
            User u = it.next();
            if (u == null)
                continue;

//            if (u.getAvatar() != null)
//            {
//                AvatarMedia am = u.getAvatar();
//                // For detail, refer user.proto
//                b.setAvatar((am.isSystem() ? "1" : "0")
//                    + am.getAvatar());
//                b.setAvatarThumbnail((am.isSystem() ? "1" : "0")
//                    + am.getAvatarThumbnail());
//            }
//            b.setCreateTime(u.getCreateTime().getTime());
//            b.setEmail(u.getEmail());
//            b.setId(u.getId());
//            b.setScreenName(u.getScreenName());
//            b.setUpdateTime(u.getUpdateTime().getTime());
//            b.build().writeDelimitedTo(out);

            // Clear inner state of builder before next loop
            b.clear();
        }
    }


    //
    // INTERNAL METHODS
    //

    private static int readHeader(
        InputStream in)

        throws IOException
    {
        byte[] b = new byte[4];
        in.read(b);
        int magic = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (magic != HEADER_MAGIC)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid magic number 0x%08x, expected 0x%08x",
                    magic,
                    HEADER_MAGIC));
        }

        b = new byte[4];
        in.read(b);
        int version = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (version <= 0 || version > HEADER_VERSION)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid version number %d, expected %d",
                    version,
                    HEADER_VERSION));
        }

        return version;
    }


    private static void writeHeader(
        OutputStream out)

        throws IOException
    {
        out.write(Conversion.intToByteArray(
            HEADER_MAGIC,
            0,
            new byte[4],
            0,
            4));
        out.write(Conversion.intToByteArray(
            HEADER_VERSION,
            0,
            new byte[4],
            0,
            4));
    }



    //
    // INTERNAL CLASSES
    //


    static class V1Iterator
        extends
            AbstractIterator<User>
    {

        UserProto.Builder b;

        InputStream in;


        public V1Iterator(
            InputStream in)
        {
            this.in = in;
            this.b = UserProto.newBuilder();
        }


        @Override
        protected User computeNext()
        {
            try
            {
                boolean success = b.mergeDelimitedFrom(in);
                if (!success)
                {
                    // EOF or some other reason
                    return endOfData();
                }

                // Deserialize using Google protobuf
                UserProto up = b.build();
                User u = new User();

//                if (up.hasAvatar())
//                {
//                    // For detail, refer user.proto
//                    String av = up.getAvatar();
//                    String avt = up.getAvatarThumbnail();
//                    u.setAvatar(new AvatarMedia(
//                        av.substring(1),
//                        avt.substring(1),
//                        av.startsWith("1")));
//                }
//
//                u.setCreateTime(new Date(up.getCreateTime()));
//                u.setEmail(up.getEmail());
//                u.setId(up.getId());
//                u.setScreenName(up.getScreenName());
//                u.setUpdateTime(new Date(up.getUpdateTime()));

                // Clear inner state of builder before next mergeFrom
                b.clear();
                return u;
            }
            catch (IOException e)
            {
                throw new RuntimeException(
                    "Error occurs when importing the data: "
                        + e.getMessage(),
                    e);
            }
        }

    }


    //
    // CONSTANTS
    //

    public static final int HEADER_MAGIC = 0x19890828;

    public static final int HEADER_VERSION = 1;
}
