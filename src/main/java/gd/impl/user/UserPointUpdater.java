
package gd.impl.user;


import gd.impl.queue.Event;
import gd.impl.queue.EventConsumer;
import gd.impl.queue.EventType;


public class UserPointUpdater
    implements
        EventConsumer
{
    public UserPointUpdater()
    {
    }


    @Override
    public boolean accept(
        EventType type)
    {
        return type == EventType.CREATE_COMMENT ||
            type == EventType.CREATE_POST ||
            type == EventType.FLAG_POST ||
            type == EventType.VOTE_COMMENT ||
            type == EventType.VOTE_POST;
    }


    @Override
    public void consume(
        Event event)
    {
        EventType type = event.type;
        switch (type)
        {
        case CREATE_COMMENT:
            Users.logPointEvent(
                event.lng1,
                PointEvent.CREATE_COMMENT,
                0L);
            break;

        case CREATE_POST:
            Users.logPointEvent(
                event.lng1,
                PointEvent.CREATE_POST,
                0L);
            break;

        case FLAG_POST:
            Users.logPointEvent(
                event.lng3,
                PointEvent.POST_FLAGGED,
                0L);
            break;

        case VOTE_COMMENT:
            if (event.bool1) {
                Users.logPointEvent(
                    event.lng3, 
                    PointEvent.COMMENT_UNVOTED, 
                    (long)event.int1);
            } else {
                Users.logPointEvent(
                    event.lng3, 
                    event.int1 < 0 ? PointEvent.COMMENT_DOWNVOTED : PointEvent.COMMENT_UPVOTED, 
                    (long)event.int1);
            }
            break;
            
        case VOTE_POST:
            if (event.bool1) {
                Users.logPointEvent(
                    event.lng3, 
                    PointEvent.POST_UNVOTED, 
                    (long)event.int1);
            } else {
                Users.logPointEvent(
                    event.lng3, 
                    event.int1 < 0 ? PointEvent.POST_DOWNVOTED : PointEvent.POST_UPVOTED, 
                    (long)event.int1);
            }
            break;

        default:
            // Do nothing
        }
    }

}
