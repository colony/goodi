
package gd.impl.user;


import gd.api.v1.ClientDevice;
import gd.impl.util.Sortable;

import java.util.Date;


public class User
    implements
        Sortable
{
    private Date createTime;
    
    /**
     * The device that user register on
     */
    private ClientDevice device;

    private long id;

    /**
     * Apple's unique device identifier
     */
    private String identifierForAdvertising;

    /**
     * Latitude at which user register
     */
    private double latitude;

    /**
     * Longitude at which user register
     */
    private double longitude;


    /**
     * sortKey and sortVal are not persisted. Both fields are
     * normally null unless the post is part of collection (i.e.
     * feed). The fields indicate how the post is sorted.
     */
    private String sortKey;

    private String sortVal;

    private UserStatus status;

    private Date updateTime;


    public User()
    {
    }


    /**
     * Copy constructor
     */
    public User(
        User u)
    {
        this.createTime = u.createTime;
        this.device = u.device;
        this.id = u.id;
        this.identifierForAdvertising = u.identifierForAdvertising;
        this.latitude = u.latitude;
        this.longitude = u.longitude;
        this.status = u.status;
        this.updateTime = u.updateTime;
    }



    //
    // GETTERS & SETTERS
    //


    public Date getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        Date createTime)
    {
        this.createTime = createTime;
    }
    
    
    public ClientDevice getDevice()
    {
        if (device == null)
            return ClientDevice.UNIDENTIFIED;
        return device;
    }
    
    
    public void setDevice(ClientDevice device)
    {
        this.device = device;
    }


    public long getId()
    {
        return id;
    }


    public void setId(
        long id)
    {
        this.id = id;
    }


    public String getIdentifierForAdvertising()
    {
        return identifierForAdvertising;
    }


    public void setIdentifierForAdvertising(
        String idfa)
    {
        this.identifierForAdvertising = idfa;
    }


    public double getLatitude()
    {
        return latitude;
    }


    public void setLatitude(
        double lat)
    {
        this.latitude = lat;
    }


    public double getLongitude()
    {
        return longitude;
    }


    public void setLongitude(
        double lon)
    {
        this.longitude = lon;
    }


    public String getSortKey()
    {
        return sortKey;
    }


    public void setSortKey(
        String sk)
    {
        this.sortKey = sk;
    }


    public String getSortVal()
    {
        return sortVal;
    }


    public void setSortVal(
        String sv)
    {
        this.sortVal = sv;
    }


    public UserStatus getStatus()
    {
        return status;
    }


    public void setStatus(
        UserStatus status)
    {
        this.status = status;
    }


    public Date getUpdateTime()
    {
        return updateTime;
    }


    public void setUpdateTime(
        Date updateTime)
    {
        this.updateTime = updateTime;
    }
}
