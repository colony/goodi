
package gd.impl.user;


/**
 * An enum identifying the type of events that may be subject to
 * reward points
 */
public enum PointEvent
{
    COMMENT_DOWNVOTED(-1),
    
    /**
     * A comment is either undownvoted or unupvoted
     */
    COMMENT_UNVOTED(0),

    COMMENT_UPVOTED(1),

    /**
     * Details we save
     * <pre>
     * commentId, postId
     * </pre>
     */
    CREATE_COMMENT(2),

    CREATE_POST(3),

    /** A invites friends via any channel (i.e. Facebook) */
    INVITE(10),
    
    /**
     * Whenever a NEW user install app, upon first launch, the
     * system will generate an unique userId for him/her as well as
     * the ON_BOARDING reward
     */
    ON_BOARDING(101),

    POST_DOWNVOTED(-1),

    POST_FLAGGED(-5),
    
    /**
     * A post is either undownvoted or unupvoted
     */
    POST_UNVOTED(0),

    POST_UPVOTED(1),

    ;

    // Default points
    public final int points;


    PointEvent(
        int dfltPoints)
    {
        this.points = dfltPoints;
    }
}
