
package gd.impl.user;


import gd.api.v1.ClientContext;
import gd.api.v1.ClientDevice;
import gd.api.v1.ClientType;
import gd.api.v1.LatLng;
import gd.api.v1.user.AccountResource;
import gd.impl.Rests;
import gd.impl.notification.NotificationProvider;
import gd.impl.notification.NotificationToken;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.session.SessionManager;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


@Path("/accounts")
@Singleton
public class AccountResourceImpl
    implements
        AccountResource
{
    private static final Logger LOG = LoggerFactory.getLogger(AccountResourceImpl.class);
    
    protected SessionManager sessMgr;

    protected UserRepository userRepo;


    public AccountResourceImpl()
    {
        this.sessMgr = SessionManager.instance();

        RepositoryManager repoMgr = RepositoryManager.instance();
        this.userRepo =
            repoMgr.get(
                RepositoryType.USER,
                UserRepository.class);
    }


    @Override
    @Path("/bind")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response bind(
        @Context HttpHeaders headers,
        @FormParam("idfa") String identifierForAdvertising,
        @FormParam("device_token") String deviceToken,
        @FormParam("device_model") ClientDevice device)
    {
        Rests.beginUnauthOperation(headers, "acct:bind");

        ClientContext cc = Rests.retrieveClientContext(headers);
        
        // We don't need much user data now
        User user = new User();
        user.setIdentifierForAdvertising(identifierForAdvertising);
        user.setDevice(device == null ? ClientDevice.UNIDENTIFIED : device);
        LatLng latlng = cc.getLatLng();
        user.setLatitude(latlng.getLat());
        user.setLongitude(latlng.getLon());
        User newUser = this.userRepo.add(user);

        // Give user initial points
        Users.logPointEvent(newUser.getId(), PointEvent.ON_BOARDING, 0L);

        String token;
        if (Strings.isNullOrEmpty(deviceToken))
        {
            token = this.sessMgr.openSession(newUser.getId());
        }
        else
        {
            token =
                this.sessMgr.openSessionWithNotificationToken(
                    newUser.getId(),
                    new NotificationToken(
                        cc.getType() == ClientType.IPHONE ? NotificationProvider.IOS : NotificationProvider.ANDROID,
                        deviceToken));
        }
        
        LOG.info("Register user {} device {} session {}", newUser.getId(), device.name(), token);
        
        return Rests.ok("id", newUser.getId(), "token", token);
    }
}
