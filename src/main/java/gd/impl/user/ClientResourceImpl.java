
package gd.impl.user;


import gd.api.v1.ClientContext;
import gd.api.v1.post.Geo;
import gd.api.v1.user.ClientResource;
import gd.api.v1.user.InitializationRep;
import gd.impl.Rests;
import gd.impl.api.ApiException;
import gd.impl.api.ApiManager;
import gd.impl.api.ApiType;
import gd.impl.api.GoogleApi;
import gd.impl.api.GoogleApi.GeocodeRequest;
import gd.impl.api.GoogleApi.GeocodeResponse;
import gd.impl.config.Configs;
import gd.impl.config.GlobalProperties;
import gd.impl.config.SystemSetting;
import gd.impl.group.Groups;
import gd.impl.identity.AuthLevel;
import gd.impl.notification.NotificationProvider;
import gd.impl.notification.NotificationToken;
import gd.impl.post.CardManager;
import gd.impl.post.PostCount;
import gd.impl.post.PostRepository;
import gd.impl.queue.Event;
import gd.impl.queue.EventQueue;
import gd.impl.queue.EventType;
import gd.impl.queue.QueueManager;
import gd.impl.queue.QueueType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.session.SessionRepository;
import gd.impl.session.Sessions;
import gd.impl.util.UpdateConflictException;
import gd.support.jersey.BoolParam;
import gd.support.jersey.DoubleParam;
import gd.support.jersey.LongParam;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


@Path("/client")
@Singleton
public class ClientResourceImpl
    implements
        ClientResource
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ClientResourceImpl.class);

    /** The sync point used by reportFeedback API */
    private static final Object FEEDBACK_LOCK = new Object();
    
    protected CardManager cardMgr;

    protected EventQueue eventQ;

    protected GoogleApi googleApi;

    protected PostRepository postRepo;

    protected SessionRepository sessRepo;

    protected UserRepository userRepo;


    public ClientResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.postRepo =
            repoMgr.get(RepositoryType.POST, PostRepository.class);
        this.sessRepo =
            repoMgr.get(
                RepositoryType.SESSION,
                SessionRepository.class);
        this.userRepo =
            repoMgr.get(RepositoryType.USER,
                UserRepository.class);

        QueueManager qMgr = QueueManager.instance();
        this.eventQ = qMgr.get(QueueType.EVENT, EventQueue.class);

        ApiManager apiMgr = ApiManager.instance();
        this.googleApi = apiMgr.get(ApiType.GOOGLE, GoogleApi.class);
        
        this.cardMgr = CardManager.instance();
    }

    
    @Override
    @Path("/card")
    @GET
    @Produces("application/json")
    public Response drawCard(
        @Context HttpHeaders headers,
        @QueryParam("deck_id") String deckId,
        @QueryParam("is_request") @DefaultValue("0") BoolParam isRequestParam)
    {
        Rests.beginOperation(headers, AuthLevel.AUTH_OPTIONAL, "clnt:draw");
        String type;
        if (isRequestParam.get()) {
            deckId = this.cardMgr.nextDeckId();
            type = "REQ";
        } else {
            type = "REP";
        }
        int count = Configs.getInt(SystemSetting.CARD_COUNT, type, 5);
        List<String> cards = this.cardMgr.drawCards(deckId, type, count);
        
        // Prepare returning json
        JSONObject json = new JSONObject();
        json.put("deck_id", deckId);
        JSONArray array = new JSONArray(cards);
        json.put("deck", array);
        return Response.ok(json).build();
    }


    @Override
    @Path("/config")
    @GET
    @Produces("application/json")
    public Response getClientConfig(
        @Context HttpHeaders headers)
    {
        Rests.beginOperation(headers, AuthLevel.AUTH_OPTIONAL, "clnt:config");
        ClientContext cc = Rests.retrieveClientContext(headers);
        return Response.ok(
            Configs.getClientConfig(cc.getType(), cc.getVersion()))
            .build();
    }


    @Override
    @Path("/init")
    @GET
    @Produces("application/json")
    public Response getInitData(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo)
    {
        long userId = Rests.beginOperation(headers, AuthLevel.AUTH_OPTIONAL, "clnt:init");
        ClientContext cc = Rests.retrieveClientContext(headers);

        InitializationRep rep = new InitializationRep();
        rep.clientConfig = Configs.getClientConfig(cc.getType(), cc.getVersion());

        // Initialization data for registered user
        if (userId > 0) {
            rep.groupPickList = Groups.getGroupPickList();
        }

        return Response.ok(rep).build();
    }


    @Override
    @Path("/report/feedback")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportFeedback(
        @Context HttpHeaders headers,
        @FormParam("feedback") String feedback)
    {
        if (Strings.isNullOrEmpty(feedback))
            return Response.ok().build(); // Bail out

        long callerId = Rests.beginOperation(headers, "clnt:reportFeedback");
        StringBuilder sb = new StringBuilder();
        sb.append(new Date());
        sb.append(" ");
        sb.append(callerId);
        sb.append(" ");
        sb.append(feedback);

        synchronized (FEEDBACK_LOCK) {
            String fname = "feedback.txt";
            java.nio.file.Path path =
                GlobalProperties.instance()
                    .resourceDir()
                    .resolve(fname);
            try (BufferedWriter writer = Files.newBufferedWriter(
                path,
                StandardCharsets.UTF_8,
                StandardOpenOption.CREATE,
                StandardOpenOption.APPEND,
                StandardOpenOption.WRITE))
            {
                writer.write(sb.toString());
                writer.newLine();
            }
            catch (IOException e)
            {
                LOG.error("Fail to write feedback \"{}\" to file {}", feedback, fname, e);
            }
        }

        return Response.ok().build();
    }


    @Override
    @Path("/report/geo")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportGeo(
        @Context HttpHeaders headers,
        @FormParam("lat") DoubleParam latParam,
        @FormParam("lng") DoubleParam lngParam,
        @FormParam("is_first") @DefaultValue("0") BoolParam isFirst)
    {
        long callerId = Rests.beginOperation(headers, "clnt:reportGeo");
        if (isFirst.get())
        {
            // For now, we only track geo reporting at registration
            User user = this.userRepo.get(callerId);
            if (user == null)
            {
                return Response.status(Status.NOT_FOUND)
                    .entity("No user id = " + callerId)
                    .build();
            }

            // For now, it is possible for client to register server with no latlng
            // after client register, it will be present a location disable page then
            // user re-enable the location, then report geo here to update the signup geo info
            if (user.getLatitude() == 0 && user.getLongitude() == 0) {
                user.setLatitude(latParam.get());
                user.setLongitude(lngParam.get());
                try
                {
                    this.userRepo.update(user);
                }
                catch (UpdateConflictException e)
                {
                    LOG.error(
                        "Fail to update user's registration geo lat={}, lon={}",
                        latParam.get(),
                        lngParam.get(),
                        e);
                }
            }
        }

        return Response.ok().build();
    }


    @Override
    @Path("/report/invite")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportInvite(
        @Context HttpHeaders headers,
        @FormParam("type") String type)
    {
        long callerId = Rests.beginOperation(headers, "clnt:reportInvite:" + type);
        if (StringUtils.isNotEmpty(type))
        {
            Users.logPointEvent(
                callerId,
                PointEvent.INVITE,
                0L,
                type);
        }

        return Response.ok().build();
    }


    @Override
    @Path("/report/notification")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportNotificationToken(
        @Context HttpHeaders headers,
        @FormParam("token") String deviceToken)
    {
        long callerId = Rests.beginOperation(headers, "clnt:reportDN");
        ClientContext cc = Rests.retrieveClientContext(headers);
        NotificationProvider np = null;
        switch (cc.getType())
        {
        case ANDROID:
            np = NotificationProvider.ANDROID;
            break;
        case IPHONE:
            np = NotificationProvider.IOS;
            break;
        default:
        }

        if (np != null && StringUtils.isNotEmpty(deviceToken))
        {
            NotificationToken newToken =
                new NotificationToken(np, deviceToken);
            Sessions.setNotificationToken(callerId, newToken);
        }

        return Response.ok().build();
    }


    @Override
    @Path("/report/share")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response reportShare(
        @Context HttpHeaders headers,
        @FormParam("post_id") LongParam idParam,
        @FormParam("type") String type)
    {
        long callerId = Rests.beginOperation(headers, "clnt:reportShare:" + idParam.get() + ":" + type);
        if (idParam != null && StringUtils.isNotEmpty(type))
        {
            long id = idParam.get();
            this.postRepo.incrCount(id, PostCount.Field.SHARE, 1L);
            Event event = new Event(EventType.SHARE_POST);
            event.lng1 = callerId;
            event.lng2 = id;
            event.str1 = type;
            this.eventQ.enq(event);
        }

        return Response.ok().build();
    }


    @Override
    @Path("/util/reversegeo")
    @GET
    @Produces("application/json")
    public Response reverseGeo(
        @Context HttpHeaders headers,
        @QueryParam("lat") DoubleParam latParam,
        @QueryParam("lon") DoubleParam lonParam)
    {
        double lat = latParam.get();
        double lon = lonParam.get();
        Rests.beginUnauthOperation(headers, "clnt:reversegeo:" + lat + ":" + lon);

        GeocodeRequest req = new GeocodeRequest(lat, lon);
        try
        {
            GeocodeResponse rep = (GeocodeResponse) this.googleApi.execute(req);
            if (!rep.isOK())
            {
                LOG.warn("Google fail reversegeo ({},{}): {}", lat, lon, rep.errorStatus);
                return Response.status(Status.FORBIDDEN)
                    .entity("Fail to reverse geo: " + rep.errorStatus)
                    .build();
            }

            Geo geo = new Geo(
                lat,
                lon,
                null,
                StringUtils.defaultIfEmpty(rep.neighborhoodLong, rep.sublocalityLong),
                rep.localityLong,
                rep.administrativeAreaLevel1Long,
                rep.countryLong,
                rep.countryShort);
            return Response.ok(geo).build();
        }
        catch (ApiException e)
        {
            LOG.error("Fail reverse geo ({},{})", lat, lon, e);
            return Response.status(Status.INTERNAL_SERVER_ERROR)
                .entity("Fail to reverse geo").build();
        }
    }
}
