
package gd.impl.user;


import gd.impl.repository.Repository;
import gd.impl.repository.RepositoryAdmin;
import gd.impl.util.Filter;
import gd.impl.util.Page;

import java.util.List;


public interface UserRepository
    extends
        Repository<Long,User>,
        RepositoryAdmin
{
    /**
     * Retrieve a list of users with the specified pagination and
     * filter
     * 
     * @param page
     * @param filter
     * @return
     */
    public List<User> find(
        Page page,
        Filter<User> filter);
}
