
package gd.impl.user;


import gd.api.v1.user.UserRep;
import gd.api.v1.user.UserResource;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.PageParam;
import gd.impl.util.Visitor;
import gd.support.jersey.IntParam;
import gd.support.jersey.LongParam;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.server.model.Resource;

import com.google.common.collect.Lists;


@Path("/users")
@Singleton
public class UserResourceImpl
    implements
        UserResource
{
    protected UserRepository userRepo;


    public UserResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.userRepo =
            repoMgr.get(
                RepositoryType.USER,
                UserRepository.class);
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getAll(
        @Context HttpHeaders headers,
        @QueryParam("n") @DefaultValue("20") IntParam count)
    {
        final int n = count.get();
        final List<UserRep> upl = Lists.newArrayList();

        this.userRepo.visitAll(new Visitor<User>() {
            int i = 0;


            @Override
            public boolean visit(
                User u)
            {
                if (++i > n)
                {
                    return false;
                }

                UserRep rep = new UserRep();
                Reps.populateUserBasic(rep, u, false);
                upl.add(rep);
                return true;
            }
        });

        return Response.ok(upl).build();
    }


    @Override
    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getUser(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginOperation(headers, "u:get:" + idParam.get());

        long id = idParam.get();
        User user = this.userRepo.get(id);
        if (user == null)
        {
            return (Response.status(Status.NOT_FOUND)
                .entity("User ID " + id + " not found")
                .build());
        }

        // Build up the response
        UserRep rep = new UserRep();
        Reps.populateUserBasic(rep, user, false);

        return Response.ok(rep).build();
    }


    @Override
    @Path("/{id}/followed")
    @GET
    @Produces("application/json")
    public Response getFollowed(
        @Context HttpHeaders headers,
        @BeanParam PageParam pageParam,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginOperation(headers, "u:getFollowed:" + idParam.get());

//        Page p = pageParam.get();
//        UserRep.Lists reps = new UserRep.Lists();
//        this.isFollowedBy.visitEdges(
//            idParam.get(),
//            StringUtils.defaultIfEmpty(p.key, Relations.SK_DID),
//            p.boundary,
//            p.isDescending(),
//            p.exclusive,
//            new UserByIdVisitor(
//                reps,           // representation
//                p.size,         // max size
//                !p.isAfter(),   // reverse if BEFORE
//                null,           // dflt converter
//                null));         // no filter
//
//        return Response.ok(reps).build();
        return null;
    }


    @Override
    @Path("/{id}/following")
    @GET
    @Produces("application/json")
    public Response getFollowing(
        @Context HttpHeaders headers,
        @BeanParam PageParam pageParam,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginOperation(headers, "u:getFollowing:" + idParam.get());

//        Page p = pageParam.get();
//        UserRep.Lists reps = new UserRep.Lists();
//        this.follows.visitEdges(
//            idParam.get(),
//            StringUtils.defaultIfEmpty(p.key, Relations.SK_DID),
//            p.boundary,
//            p.isDescending(),
//            p.exclusive,
//            new UserByIdVisitor(
//                reps,           // representation
//                p.size,         // max size
//                !p.isAfter(),   // reverse if BEFORE
//                null,           // dflt converter
//                null));         // no filter
//
//        return Response.ok(reps).build();
        return null;
    }


    @Override
    @Path("/me")
    public Resource locateMeResource()
    {
        return Resource.from(MeResourceImpl.class);
    }

    //
    // INTERNAL CLASSES
    //
}
