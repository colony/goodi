
package gd.impl;


import gd.impl.config.Configs;
import gd.support.sqlite.MiniConnectionPoolManager;
import gd.support.sqlite.SQLiteConnectionPoolDataSource2;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConfig.JournalMode;
import org.sqlite.SQLiteConfig.SynchronousMode;
import org.sqlite.SQLiteConfig.TempStore;


/**
 * 
 * @author jinlin
 * 
 */
public class SQLiteManager
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SQLiteManager.class);

    private MiniConnectionPoolManager poolMgr;


    public SQLiteManager()
    {
        Path path =
            Configs.getComponentConfig().getSqlite().getPath();
        LOG.info("SQLiteManager started with path jdbc:sqlite:{}", path);

        SQLiteConnectionPoolDataSource2 dataSource =
            new SQLiteConnectionPoolDataSource2();
        dataSource.setUrl("jdbc:sqlite:" + path);

        SQLiteConfig cfg = new SQLiteConfig();
        cfg.setSynchronous(SynchronousMode.NORMAL);
        cfg.setTempStore(TempStore.MEMORY);
        cfg.setJournalMode(JournalMode.WAL);
        cfg.setCacheSize(131072); // 128Mb
        // Busy timeout?? default it zero
        dataSource.setConfig(cfg);

        poolMgr = new MiniConnectionPoolManager(dataSource, 20);
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final SQLiteManager INSTANCE =
            new SQLiteManager();
    }


    public static SQLiteManager instance()
    {
        return SingletonHolder.INSTANCE;
    }


    //
    //convenient method
    //

    public Connection getConnection()
        throws SQLException
    {
        return poolMgr.getConnection();
    }


    //
    //getter and setter
    //
    public MiniConnectionPoolManager getPoolMgr()
    {
        return poolMgr;
    }


    public void setPoolMgr(
        MiniConnectionPoolManager poolMgr)
    {
        this.poolMgr = poolMgr;
    }
}
