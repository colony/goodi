
package gd.impl.comment;


import gd.api.v1.PaginationRep;
import gd.api.v1.admin.ManageCommentForm;
import gd.api.v1.admin.ManageCommentResource;
import gd.api.v1.comment.CommentRep;
import gd.api.v1.comment.CommentRepList;
import gd.api.v1.comment.CommentType;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.activity.Activities;
import gd.impl.media.FileManager;
import gd.impl.media.IconMedia;
import gd.impl.media.Medias;
import gd.impl.post.Post;
import gd.impl.post.PostCount;
import gd.impl.post.PostRepository;
import gd.impl.post.Posts;
import gd.impl.queue.Event;
import gd.impl.queue.EventQueue;
import gd.impl.queue.EventType;
import gd.impl.queue.QueueManager;
import gd.impl.queue.QueueType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.user.User;
import gd.impl.user.UserStatus;
import gd.impl.user.Users;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.support.jersey.LongParam;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;


@Path("/adm/comments")
@Singleton
public class ManageCommentResourceImpl
    implements
        ManageCommentResource
{
    protected CommentRepository cmtRepo;

    protected EventQueue eventQ;

    protected FileManager fileMgr;

    protected PostRepository postRepo;


    public ManageCommentResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.cmtRepo =
            repoMgr.get(
                RepositoryType.COMMENT,
                CommentRepository.class);
        this.postRepo =
            repoMgr.get(RepositoryType.POST, PostRepository.class);

        this.fileMgr = FileManager.instance();

        QueueManager qMgr = QueueManager.instance();
        this.eventQ = qMgr.get(QueueType.EVENT, EventQueue.class);
    }


    @Override
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createOrModifyComment(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam ManageCommentForm commentForm)
    {
        String desc = "";
        if (commentForm.getId() != null)
            desc = "modify:" + commentForm.getId();
        else
            desc = "create:" + commentForm.getPostId() + ":" + commentForm.getOwnerId();
        Rests.beginAdminOperation(headers, "adm:mcmt:" + desc);

        Comment returnCmt;
        Long id = commentForm.getId();
        boolean isUpdate = (id != null);
        if (isUpdate)
        {
            // Update
            Comment cmt = this.cmtRepo.get(id);
            if (cmt == null)
                return Response.status(Status.NOT_FOUND)
                    .entity("Comment not found id=" + id)
                    .build();

            // Replace not-null values specified by the form
            // NOTE: ownerId, parentId, postId, type is not allowed to updated HERE
            if (StringUtils.isNotEmpty(commentForm.getBody()))
                cmt.setBody(commentForm.getBody());
            if (null != commentForm.getGeo())
                cmt.setGeo(commentForm.getGeo());
            
            //TODO: update media??

            // Replace 
            this.cmtRepo.put(cmt);

            returnCmt = cmt;
        }
        else
        {
            // Create
            if (null == commentForm.getOwnerId()
                || null == commentForm.getPostId())
                return Response.status(Status.BAD_REQUEST)
                    .entity(
                        "A fake user ID and post ID are needed to create a comment")
                    .build();

            long ownerId = commentForm.getOwnerId();
            User user = Users.get(ownerId);
            if (null == user || user.getStatus() == null
                || !user.getStatus().isSet(UserStatus.Status.FAKE))
            {
                return Response.status(Status.BAD_REQUEST)
                    .entity(
                        "The specified user ID \"" + ownerId
                            + "\" doesn't exist or is not FAKE")
                    .build();
            }

            Post post = Posts.get(commentForm.getPostId());

            // Prepare the comment
            IconMedia im = null;
            if (post.getOwnerId() != ownerId) {
                im = Medias.generateCommenterIconMedia(commentForm.getPostId(), ownerId);
            }

            Comment cmt = new Comment();
            cmt.setOwnerId(ownerId);
            cmt.setBody(commentForm.getBody());
            cmt.setGeo(commentForm.getGeo());
            cmt.setParentId(commentForm.getParentId());
            cmt.setPostId(commentForm.getPostId());
            cmt.setOwnerIcon(im);
            cmt.setType(CommentType.valueOf(StringUtils.defaultIfEmpty(commentForm.getType(), "SIMPLE")));

            // If the post was created in a private group, all of its comments' geo must cover by
            // a covert word, to avoid displaying real geo info
            String covertGeoWord = Posts.getCovertGeoWord(commentForm.getPostId());
            if (covertGeoWord != null)
                commentForm.getGeo().setName(covertGeoWord);

            Comment newCmt = this.cmtRepo.add(cmt);

            this.postRepo.incrCount(
                commentForm.getPostId(),
                PostCount.Field.COMMENT,
                1L);

            id = newCmt.getId();
            returnCmt = newCmt;
        }

        if (commentForm.hasNumDownvotesDelta()) {
            this.cmtRepo.incrCount(id, CommentCount.Field.DOWNVOTE, commentForm.getNumDownvotesDelta());
        }

        if (commentForm.hasNumUpvotesDelta()) {
            this.cmtRepo.incrCount(id, CommentCount.Field.UPVOTE, commentForm.getNumUpvotesDelta());
        }

        // Force post score re-computation if comment is created
        if (!isUpdate)
        {
            Event event = new Event(EventType.ADMIN_UPDATE_POST);
            event.bool1 = true; // Since, we add one comment, the num_comment is changed
            event.lng1 = returnCmt.getPostId();
            this.eventQ.enq(event);
        }

        // Send out activity/notification
        if (!isUpdate)
        {
            // Though it is fake, we still need poke REAL user with notification
            Activities.asyncLogCommentPost(
                returnCmt.getOwnerId(), // commenter
                returnCmt.getId(),
                returnCmt.getPostId());
        }

        CommentRep rep = new CommentRep();
        CommentCount count = this.cmtRepo.getCount(returnCmt.getId());
        returnCmt.setCount(count);
        Reps.populateCommentAdmin(rep, returnCmt);
        return Response.ok(rep).build();
    }


    @Override
    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deleteComment(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginAdminOperation(headers, "adm:mcmt:delete:" + idParam.get());
        Posts.deleteComment(idParam.get());
        return Response.ok().build();
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getCommentList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:mcmt:getCommentList");
        Page p = pageParam.get();

        List<Comment> cmtList = this.cmtRepo.find(p, null);

        CommentRepList repList = new CommentRepList();
        for (Comment cmt : cmtList)
        {
            CommentRep rep = new CommentRep();
            Reps.populateCommentAdmin(rep, cmt);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(cmtList),
                GDUtils.getLast(cmtList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }


    //
    // INTERNAL METHODS
    //


//    private PhotoMedia createMedia(
//        ManageCommentForm form)
//    {
//        if (null != form.getPhotoStream())
//        {
//            PhotoMedia.Format fmt =
//                PhotoMedia.Format.valueOf(form.getPhotoFormat());
//            long size = form.getPhotoSize();
//            try
//            {
//                String photoKey =
//                    this.fileMgr.store(
//                        form.getPhotoStream(),
//                        MediaType.PHOTO,
//                        fmt.getContentType(),
//                        size,
//                        null,
//                        null);
//                if (null != photoKey)
//                {
//                    PhotoMedia media = new PhotoMedia();
//                    media.setFormat(fmt);
//                    media.setLarge(photoKey);
//                    media.setLargeHeight(form.getPhotoHeight());
//                    media.setLargeWidth(form.getPhotoWidth());
//                    return media;
//                }
//            }
//            catch (IOException e)
//            {
//                throw new WebApplicationException(
//                    "Fail to upload file for mediaType=PHOTO and size="
//                        + size,
//                    e);
//            }
//        }
//        return null;
//    }
}
