
package gd.impl.comment;


import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.api.v1.comment.CommentType;
import gd.api.v1.post.Geo;
import gd.impl.SQLiteManager;
import gd.impl.comment.CommentCount.Field;
import gd.impl.media.IconMedia;
import gd.impl.media.Medias;
import gd.impl.repository.RepositoryConfig;
import gd.impl.repository.SqliteRepository;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


/**
 * Schema:
 * <ul>
 * <li>body
 * <li>create_time
 * <li>geo
 * <li>hashtags
 * <li>id
 * <li>media
 * <li>owner_icon
 * <li>owner_id
 * <li>parent_id
 * <li>post_id
 * <li>type
 * </ul>
 * 
 * Schema (comment_counter):
 * <ul>
 * <li>comment_id
 * <li>num_downvotes
 * <li>num_replies
 * <li>num_upvotes
 * </ul>
 */
public class SqliteCommentRepository
    implements
        CommentRepository,
        SqliteRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SqliteCommentRepository.class);

    private static final String SP = " ";

    private int batchSize = 100;


    public SqliteCommentRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public Comment add(
        Comment c)
    {
        String sql =
            "INSERT INTO comment (body, create_time, geo, hashtags, media, owner_icon, owner_id, parent_id, post_id, type) VALUES "
                + "(?,?,?,?,?,?,?,?,?,?)";

        Comment newCmt = new Comment(c);

        // It is allowed to specify createTime via AdminConsole
        if (c.getCreateTime() == null)
            newCmt.setCreateTime(new Date());

        // SQL statement parameters construction
        Object param[] = new Object[10];
        param[0] = newCmt.getBody();
        param[1] = newCmt.getCreateTime().getTime();
        if (null != newCmt.getGeo())
        {
            param[2] = newCmt.getGeo().toStr();
        }
        if (0 < c.getNumHashtags())
        {
            param[3] = StringUtils.join(c.getHashtags(), SP);
        }
        if (null != newCmt.getMedia())
            param[4] = Medias.serializeMedia(newCmt.getMedia());
        if (null != newCmt.getOwnerIcon())
            param[5] = newCmt.getOwnerIcon().toStr();
        param[6] = newCmt.getOwnerId();
        if (null != newCmt.getParentId())
            param[7] = newCmt.getParentId();
        param[8] = newCmt.getPostId();
        param[9] = newCmt.getType().name();

        // Execute SQL
        long newId =
            SqliteUtils.executeInsertQuery(
                sql,
                param,
                "addComment",
                true);
        newCmt.setId(newId);

        // Init comment_counter with all ZEROs
        sql = "INSERT INTO comment_counter (comment_id, num_downvotes, num_replies, num_upvotes) VALUES (?,0,0,0)";
        SqliteUtils.executeInsertQuery(sql, new Object[] {
            newId
        }, "addCommentCounter", false);

        return newCmt;
    }


    @Override
    public boolean delete(
        Long id)
    {
        String sql = "DELETE FROM comment WHERE id = " + id;
        boolean success =
            SqliteUtils.executeQuery(sql, null, "DeleteComment");
        if (success) {
            SqliteUtils.executeQuery(
                "DELETE FROM comment_counter WHERE comment_id = " + id,
                null,
                "DeleteCommentRelated");
        }

        return success;
    }


    @Override
    public boolean exists(
        Long id)
    {
        final String sql = "SELECT 1 FROM comment WHERE id=?";
        Object[] param = {
                id
        };
        final MutableBoolean mb = new MutableBoolean(false);

        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "isCommentExist", new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }
            });
        return mb.booleanValue();
    }


    @Override
    public List<Comment> find(
        Page page,
        Filter<Comment> filter)
    {
        String key =
            StringUtils.defaultIfEmpty(page.key, SK_ID);
        key = key.toUpperCase();
        switch (key)
        {
        case SK_ID:
            return this.findById(page, filter);

        default:
            throw new IllegalArgumentException(
                "Unsupported sort key " + key);
        }
    }


    private List<Comment> findById(
        Page page,
        Filter<Comment> filter)
    {
        // SELECT *
        // FROM comment
        // WHERE id <=> ?
        // ORDER BY id desc|asc
        // LIMIT ?

        final List<Comment> comments = new ArrayList<>(page.size);
        String sql;
        if (Strings.isNullOrEmpty(page.boundary))
        {
            sql =
                "SELECT c.*,cc.* "
                    + "FROM comment c, comment_counter cc "
                    + "WHERE c.id = cc.comment_id "
                    + "ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }
        else
        {
            sql =
                "SELECT c.*,cc.* "
                    + "FROM comment c, comment_counter cc "
                    + "WHERE c.id = cc.comment_id AND "
                    + "id "
                    + (page.isDescending() == page.isAfter() ? (page.exclusive ? "<?" : "<=?") : (page.exclusive ? ">?" : ">=?"))
                    + " ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }

        SqliteUtils.executeSelectQuery(
            sql,
            Strings.isNullOrEmpty(page.boundary) ? null : new Object[] { page.boundary },
            "findById",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Comment cmt = new Comment();
                        parseResultSet(cmt, rs);
                        cmt.setSortKey("ID");
                        cmt.setSortVal(String.valueOf(cmt.getId()));
                        comments.add(cmt);
                    }
                }
            });

        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */
        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(comments);
        }

        return comments;
    }


    @Override
    public List<Comment> findByOwner(
        Long ownerId,
        Page page,
        Filter<Comment> filter)
    {
        // SELECT * FROM comment
        // WHERE owner_id=? AND
        //       id <=> ?
        // ORDER BY id desc|asc
        // LIMIT ?

        final List<Comment> comments = new ArrayList<>(page.size);

        String sql;
        boolean hasBoundary = StringUtils.isNotEmpty(page.boundary);
        if (!hasBoundary)
        {
            sql =
                "SELECT c.*,cc.* "
                    + "FROM comment c, comment_counter cc "
                    + "WHERE c.id = cc.comment_id AND owner_id=? "
                    + "ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }
        else
        {
            sql =
                "SELECT c.*,cc.* "
                    + "FROM comment c, comment_counter cc "
                    + "WHERE c.id = cc.comment_id AND owner_id=? "
                    + "AND id "
                    + (page.isDescending() == page.isAfter() ? (page.exclusive ? "<?" : "<=?") : (page.exclusive ? ">?" : ">=?"))
                    + " ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }

        Object[] param = null;
        if (hasBoundary)
            param = new Object[] { ownerId, page.boundary };
        else
            param = new Object[] { ownerId };

        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "findCmtByOwnerId:" + ownerId,
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Comment cmt = new Comment();
                        parseResultSet(cmt, rs);
                        cmt.setSortKey("ID");
                        cmt.setSortVal(String.valueOf(cmt.getId()));
                        comments.add(cmt);
                    }
                }
            });

        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */
        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(comments);
        }

        return comments;
    }


    @Override
    public List<Comment> findByPost(
        Long postId,
        Page page,
        Filter<Comment> filter,
        boolean all)
    {
        final List<Comment> comments = new ArrayList<>(page.size);

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT c.*,cc.* FROM comment c, comment_counter cc WHERE c.id = cc.comment_id AND post_id=? ");

        boolean hasBoundary = StringUtils.isNotEmpty(page.boundary);
        if (hasBoundary)
        {
            String op = page.toMathOperator();
            sb.append("AND id" + op + "? ");
        }
        sb.append("order by id ");
        /**
         * if order is desc before or asc before, we change it desc
         * before -> asc after, acs before -> desc after then we
         * reverse it .
         */
        String order = page.isDescending() == page.isAfter() ? "desc" : "asc";
        sb.append(order);
        if (!all) {
            sb.append(" limit ");
            sb.append(page.size);
        }

        Object[] param = null;
        if (hasBoundary)
            param = new Object[] { postId, page.boundary };
        else
            param = new Object[] { postId };
        final String sortKey = "ID";
        SqliteUtils.executeSelectQuery(
            sb.toString(),
            param,
            "findCommentByPost",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Comment p = new Comment();
                        parseResultSet(p, rs);
                        p.setSortKey(sortKey);
                        p.setSortVal(String.valueOf(p.getId()));
                        comments.add(p);
                    }
                }

            });

        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */
        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(comments);
        }

        return comments;
    }


    @Override
    public Comment get(
        Long id)
    {
        final String sql = "SELECT * FROM comment WHERE id=?";
        Object[] param = {
                id
        };

        final MutableObject<Comment> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "getComment",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        Comment cmt = new Comment();
                        parseResultSetNoCount(cmt, rs);
                        mo.setValue(cmt);
                    }
                }
            });
        return mo.getValue();
    }


    @Override
    public CommentCount getCount(
        Long cmtId)
    {
        final CommentCount cc = new CommentCount();
        String sql =
            "SELECT * FROM comment_counter WHERE comment_id = ?";
        SqliteUtils.executeSelectQuery(sql, new Object[] {
            cmtId
        }, "getPostCount", new ResultSetParser() {
            @Override
            public void parse(
                ResultSet rs)
                throws SQLException
            {
                if (rs.next())
                {
                    cc.setNumDownvotes(rs.getLong("num_downvotes"));
                    cc.setNumReplies(rs.getLong("num_replies"));
                    cc.setNumUpvotes(rs.getLong("num_upvotes"));
                }
            }
        });
        return cc;
    }


    @Override
    public void incrCount(
        Long cmtId,
        Field count,
        long incr)
    {
        String sql = null;
        switch (count)
        {
        case DOWNVOTE:
            sql = "UPDATE comment_counter SET num_downvotes=num_downvotes+? WHERE comment_id=?";
            break;
        case REPLY:
            sql = "UPDATE comment_counter SET num_replies=num_replies+? WHERE comment_id=?";
            break;
        case UPVOTE:
            sql = "UPDATE comment_counter SET num_upvotes=num_upvotes+? WHERE comment_id=?";
            break;
        }

        if (sql != null) {
            SqliteUtils.executeQuery(
                sql,
                new Object[] {
                        incr, cmtId
                },
                "incrComment" + count.name());
        }
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        createCommentTable(config);
        createCommentCounterTable(config);
        SqliteUtils.createCompositeIndex(
            config.getTableName(),
            "comment_postId_id",
            "post_id",
            "id",
            true);
        SqliteUtils.createCompositeIndex(
            config.getTableName(),
            "comment_ownerId_id",
            "owner_id",
            "id",
            true);
        this.batchSize = config.getBatchSize();
    }


    @Override
    public Comment put(
        Comment c)
    {
        Comment upCmt = new Comment(c);

        // For replace into, any not specified column will be nullified if the row exists
        String sql =
            "INSERT OR REPLACE INTO comment (body, create_time, geo, hashtags, media, id, owner_icon, owner_id, parent_id, post_id, type) "
                + "VALUES "
                + "(?,?,?,?,?,?,?,?,?,?,?)";

        Object[] param = new Object[11];
        param[0] = upCmt.getBody();
        param[1] = upCmt.getCreateTime().getTime();
        if (null != upCmt.getGeo())
        {
            param[2] = upCmt.getGeo().toStr();
        }
        if (0 < c.getNumHashtags())
        {
            param[3] = StringUtils.join(c.getHashtags(), SP);
        }
        if (null != upCmt.getMedia())
            param[4] = Medias.serializeMedia(upCmt.getMedia());
        param[5] = upCmt.getId();
        if (null != upCmt.getOwnerIcon())
        {
            param[6] = upCmt.getOwnerIcon().toStr();
        }
        param[7] = upCmt.getOwnerId();
        if (null != upCmt.getParentId())
            param[8] = upCmt.getParentId();
        param[9] = upCmt.getPostId();
        param[10] = upCmt.getType().name();

        // Should we care about the return bool?
        SqliteUtils.executeQuery(sql, param, "putComment");

        return upCmt;
    }


    @Override
    public Comment update(
        Comment t)
        throws UpdateConflictException
    {
        // I don't see any good reason to make comment updatable
        throw new UnsupportedOperationException();
    }


    @Override
    public void visitAll(
        Visitor<Comment> visitor)
    {
        if (!visitor.before())
            return;

        SQLiteManager sqliteManager = SQLiteManager.instance();
        Connection connection = null;
        PreparedStatement statement = null;

        boolean hasMore = true;
        boolean aborted = false;
        long id = 0;

        try
        {
            connection = sqliteManager.getConnection();
            final String sql =
                "SELECT * FROM comment WHERE id>? LIMIT "
                    + this.batchSize;

            statement =
                connection.prepareStatement(sql);
            while (hasMore && !aborted)
            {
                statement.setLong(1, id);
                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next())
                {
                    count++;
                    id = rs.getLong("id");

                    Comment cmt = new Comment();
                    parseResultSetNoCount(cmt, rs);
                    if (!visitor.visit(cmt))
                    {
                        aborted = true;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Fail to execute query \"visitAll\"",
                e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for visitAll",
                    e);
            }
        }

        visitor.after();
    }



    //
    // RepositoryAdmin
    //


    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.exists(path))
        {
            LOG.warn("Export file \"{}\" already exists", fname);
        }

        LOG.info("Exporting comments to file {}", fname);
        try (
             OutputStream os =
                 Files.newOutputStream(path, CREATE, WRITE, TRUNCATE_EXISTING);
             BufferedOutputStream bos =
                 new BufferedOutputStream(os);)
        {
            final CommentExporter exporter = new CommentExporter();

            exporter.writeHeader(bos);

            this.visitAll(new Visitor<Comment>() {

                @Override
                public boolean visit(
                    Comment item)
                {
                    try {
                        exporter.writeComment(bos, item);
                    }
                    catch (IOException e) {
                        LOG.error("Fail to export comment data id={}", item.getId(), e);
                        return false;
                    }
                    return true;
                }

            });
        }
        LOG.info("Done exporting comments");
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.notExists(path)) {
            throw new IllegalArgumentException("Import file " + fname + " not exist");
        }

        LOG.info("Importing comments from file {}", fname);

        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            CommentExporter exporter = new CommentExporter();

            String sql =
                "INSERT INTO comment (body, create_time, geo, hashtags, id, owner_icon, owner_id, parent_id, post_id) VALUES (?,?,?,?,?,?,?,?,?)";
            List<Object> objects = new ArrayList<>();
            Iterator<Comment> it = exporter.read(bis);
            while (it.hasNext()) {
                Comment c = it.next();
                Object[] param = new Object[9];
                param[0] = c.getBody();
                param[1] = c.getCreateTime().getTime();
                if (null != c.getGeo())
                    param[2] = c.getGeo().toStr();
                if (c.hasHashtags())
                    param[3] = StringUtils.join(c.getHashtags(), SP);
                param[4] = c.getId();
                if (null != c.getOwnerIcon())
                    param[5] = c.getOwnerIcon().toStr();
                param[6] = c.getOwnerId();
                if (null != c.getParentId())
                    param[7] = c.getParentId();
                param[8] = c.getPostId();

                objects.add(sql);
                objects.add(param);

                // NOTE, we reuse the same batchSize (for visitAll) as INSERTION batchsize, might configure it differently later
                if (objects.size() >= 2 * this.batchSize) {
                    SqliteUtils.executeTransactionalQueries("ImportComment", objects);
                    objects.clear();
                }
            }

            if (!objects.isEmpty()) {
                SqliteUtils.executeTransactionalQueries("ImportComment", objects);
            }
        }
        LOG.info("Done importing comments");
        throw new UnsupportedOperationException();
    }



    //
    // INTERNAL METHODS
    //

    private void createCommentTable(
        RepositoryConfig config)
    {
        String createCommentTableSql =
            "CREATE TABLE IF NOT EXISTS "
                + config.getTableName()
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                +
                "body TEXT," +
                "create_time INTEGER," +
                "geo TEXT," +
                "hashtags TEXT," +
                "media TEXT," +
                "owner_icon TEXT," +
                "owner_id INTEGER NOT NULL," +
                "parent_id INTEGER," +
                "post_id INTEGER NOT NULL," +
                "type TEXT" +
                ") ";
        SqliteUtils.executeQuery(
            createCommentTableSql,
            null,
            "CreateCommentTable");
    }


    private void createCommentCounterTable(
        RepositoryConfig config)
    {
        String createCommentCounterTableSql =
            "CREATE TABLE IF NOT EXISTS comment_counter" +
                "(comment_id INTEGER PRIMARY KEY NOT NULL," +
                "num_downvotes INTEGER DEFAULT 0," +
                "num_replies INTEGER DEFAULT 0," +
                "num_upvotes INTEGER DEFAULT 0" +
                ")";
        SqliteUtils.executeQuery(createCommentCounterTableSql, null, "createCommentCounterTable");
    }


    private void parseResultSet(
        Comment cmt,
        ResultSet rs)
        throws SQLException
    {
        cmt.setBody(rs.getString("body"));
        cmt.setCreateTime(new Date(rs.getLong("create_time")));
        cmt.setId(rs.getLong("id"));

        if (StringUtils.isNotEmpty(rs.getString("hashtags")))
        {
            cmt.setHashtags(Arrays.asList(rs.getString("hashtags").split(SP)));
        }

        String geoStr = rs.getString("geo");
        if (StringUtils.isNotEmpty(geoStr))
        {
            cmt.setGeo(new Geo(geoStr));
        }

        if (StringUtils.isNotEmpty(rs.getString("owner_icon")))
        {
            cmt.setOwnerIcon((IconMedia) new IconMedia().fromStr(rs.getString("owner_icon")));
        }
        cmt.setOwnerId(rs.getInt("owner_id"));
        long parentId = rs.getLong("parent_id");
        cmt.setParentId(parentId > 0L ? parentId : null); // Default value (0L) is as same as NULL
        cmt.setPostId(rs.getLong("post_id"));
        // Backward compatibility, initially, we don't have type column, so old comments are all SIMPLE
        String typeStr = StringUtils.defaultIfEmpty(rs.getString("type"), "SIMPLE");
        cmt.setType(CommentType.valueOf(typeStr));

        // Media
        String mediaStr = rs.getString("media");
        cmt.setMedia(Medias.deserializeMedia(mediaStr));

        // Counters
        CommentCount cc = new CommentCount();
        cc.setNumDownvotes(rs.getLong("num_downvotes"));
        cc.setNumReplies(rs.getLong("num_replies"));
        cc.setNumUpvotes(rs.getLong("num_upvotes"));
        cmt.setCount(cc);
    }


    private void parseResultSetNoCount(
        Comment cmt,
        ResultSet rs)
        throws SQLException
    {
        cmt.setBody(rs.getString("body"));
        cmt.setCreateTime(new Date(rs.getLong("create_time")));
        cmt.setId(rs.getLong("id"));

        if (StringUtils.isNotEmpty(rs.getString("hashtags")))
        {
            cmt.setHashtags(Arrays.asList(rs.getString("hashtags").split(SP)));
        }

        String geoStr = rs.getString("geo");
        if (StringUtils.isNotEmpty(geoStr))
        {
            cmt.setGeo(new Geo(geoStr));
        }

        if (StringUtils.isNotEmpty(rs.getString("owner_icon")))
        {
            cmt.setOwnerIcon((IconMedia) new IconMedia().fromStr(rs.getString("owner_icon")));
        }
        cmt.setOwnerId(rs.getInt("owner_id"));
        long parentId = rs.getLong("parent_id");
        cmt.setParentId(parentId > 0L ? parentId : null); // Default value (0L) is as same as NULL
        cmt.setPostId(rs.getLong("post_id"));

        // Backward compatibility, initially, we don't have type column, so old comments are all SIMPLE
        String typeStr = StringUtils.defaultIfEmpty(rs.getString("type"), "SIMPLE");
        cmt.setType(CommentType.valueOf(typeStr));

        // Media
        String mediaStr = rs.getString("media");
        cmt.setMedia(Medias.deserializeMedia(mediaStr));
    }
}
