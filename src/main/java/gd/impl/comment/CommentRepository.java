
package gd.impl.comment;


import gd.impl.comment.CommentCount.Field;
import gd.impl.repository.Repository;
import gd.impl.repository.RepositoryAdmin;
import gd.impl.util.Filter;
import gd.impl.util.Page;

import java.util.List;


public interface CommentRepository
    extends
        Repository<Long,Comment>,
        RepositoryAdmin
{
    /**
     * A list of predefined page (sort) key for pagination and
     * different sorting
     */

    // Sort comments by ID
    public static final String SK_ID = "ID";


    public List<Comment> find(
        Page page,
        Filter<Comment> filter);


    public List<Comment> findByOwner(
        Long ownerId,
        Page page,
        Filter<Comment> filter);


    /**
     * Precondition, assume the validity of the post specified by
     * postId
     * 
     * @param all
     *            If true, ignore page size, fetching all comments
     */
    public List<Comment> findByPost(
        Long postId,
        Page page,
        Filter<Comment> filter,
        boolean all);


    public CommentCount getCount(
        Long id);


    public void incrCount(
        Long cmtId,
        Field count,
        long incr);
}
