
package gd.impl.comment;


import gd.api.v1.PaginationRep;
import gd.api.v1.comment.CommentForm;
import gd.api.v1.comment.CommentPhotoOptionItemRep;
import gd.api.v1.comment.CommentPhotoOptionItemRepList;
import gd.api.v1.comment.CommentPhotoOptionRep;
import gd.api.v1.comment.CommentRep;
import gd.api.v1.comment.CommentRepList;
import gd.api.v1.comment.CommentResource;
import gd.api.v1.comment.CommentType;
import gd.api.v1.post.Geo;
import gd.api.v1.post.MediaRep;
import gd.api.v1.post.MediaType;
import gd.api.v1.post.VoteStatus;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.activity.Activities;
import gd.impl.comment.CommentCount.Field;
import gd.impl.config.CommentPhotoPickerOptionsConfig;
import gd.impl.config.CommentPhotoPickerOptionsConfig.ItemConfig;
import gd.impl.config.CommentPhotoPickerOptionsConfig.OptionConfig;
import gd.impl.config.Configs;
import gd.impl.media.FileManager;
import gd.impl.media.IconMedia;
import gd.impl.media.Media;
import gd.impl.media.Medias;
import gd.impl.media.PhotoMedia;
import gd.impl.post.Post;
import gd.impl.post.PostCount;
import gd.impl.post.PostRepository;
import gd.impl.post.Posts;
import gd.impl.queue.Event;
import gd.impl.queue.EventQueue;
import gd.impl.queue.EventType;
import gd.impl.queue.QueueManager;
import gd.impl.queue.QueueType;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.support.jersey.BoolParam;
import gd.support.jersey.IntParam;
import gd.support.jersey.LongParam;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/comments")
@Singleton
public class CommentResourceImpl
    implements
        CommentResource
{
    private static final Logger LOG = LoggerFactory.getLogger(CommentResourceImpl.class);

    protected CommentRepository cmtRepo;

    protected EventQueue eventQ;

    protected FileManager fileMgr;

    protected LLRelation isCmtVotedByRel;

    protected PostRepository postRepo;

    protected LLRelation votesCommentRel;


    public CommentResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.cmtRepo =
            repoMgr.get(
                RepositoryType.COMMENT,
                CommentRepository.class);
        this.postRepo =
            repoMgr.get(RepositoryType.POST, PostRepository.class);

        RelationManager relMgr = RelationManager.instance();
        this.isCmtVotedByRel =
            relMgr.get(RelationType.IS_COMMENT_VOTED_BY, LLRelation.class);
        this.votesCommentRel =
            relMgr.get(RelationType.VOTES_COMMENT, LLRelation.class);

        QueueManager qMgr = QueueManager.instance();
        this.eventQ = qMgr.get(QueueType.EVENT, EventQueue.class);

        this.fileMgr = FileManager.instance();
    }


    @Override
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createComment(
        @Context HttpHeaders headers,
        @BeanParam CommentForm commentForm)
    {
        long callerId = Rests.beginOperation(headers, "cmt:create");
        Long postId = commentForm.getPostId();
        if (postId == null)
            return Response.status(Status.BAD_REQUEST)
                .entity("Empty post ID is not allowed")
                .build();

        Post post = this.postRepo.get(postId);
        GDUtils.check(
            post != null,
            Status.NOT_FOUND,
            "No post with ID " + postId);

        Geo geo = commentForm.getGeo();
        if (geo == null) {
            // This happens for old app that sends geo via header, new app will send geo via form
            geo = Rests.retrieveGeo(headers);
        }

        // If the post was created in a private group, all of its comments' geo must cover by
        // a covert word, to avoid displaying real geo info
        String covertGeoWord = Posts.getCovertGeoWord(postId);
        if (covertGeoWord != null)
            geo.setName(covertGeoWord);

        // Prepare the comment
        IconMedia im = null;
        if (post.getOwnerId() != callerId) {
            // For OP, it has a fixed displaying icon at front-end, skip generating
            im = Medias.generateCommenterIconMedia(postId, callerId);
        }

        Comment cmt = new Comment();
        cmt.setOwnerId(callerId);
        cmt.setBody(commentForm.getBody());
        cmt.setGeo(geo);
        cmt.setParentId(commentForm.getParentId());
        cmt.setPostId(postId);
        cmt.setOwnerIcon(im);
        cmt.setType(CommentType.valueOf(StringUtils.defaultIfEmpty(commentForm.getType(), "SIMPLE")));

        // Figure out additional media if any
        if (commentForm.hasPhotoOptionItemId()) {
            CommentPhotoPickerOptionsConfig cfg = Configs.get(CommentPhotoPickerOptionsConfig.class);
            ItemConfig itemConfig = cfg.getItemConfig(commentForm.getPhotoOptionItemId());

            if (itemConfig == null) // Oops, wrong id?
                cmt.setType(CommentType.SIMPLE);
            else
                cmt.setMedia(this.itemConfigToMedia(itemConfig));
        }
        else if (commentForm.getPhotoStream() != null) {
            // Upload/take a photo by camera
            //TODO
        }

        Comment newCmt = this.cmtRepo.add(cmt);
        this.postRepo.incrCount(postId, PostCount.Field.COMMENT, 1L);
        if (null != commentForm.getParentId()) {
            this.cmtRepo.incrCount(commentForm.getParentId(), Field.REPLY, 1L);
        }

        Activities.asyncLogCommentPost(
            callerId,
            newCmt.getId(),
            postId);

        Event event = new Event(EventType.CREATE_COMMENT);
        event.lng1 = callerId;
        event.lng2 = newCmt.getId();
        event.lng3 = postId;
        event.lng4 = post.getOwnerId();
        this.eventQ.enq(event);

        CommentRep cr = new CommentRep();
        Reps.populateCommentBasic(cr, newCmt, callerId, true, false, false);
        return Response.ok().entity(cr).build();
    }


    @Override
    @Path("/photopicker")
    @GET
    @Produces("application/json")
    public Response getCommentPhotoPickerOptionList(
        @Context HttpHeaders headers)
    {
        Rests.beginOperation(headers, "cmt:getPickerOptionList");

        CommentPhotoPickerOptionsConfig c = Configs.get(CommentPhotoPickerOptionsConfig.class);
        List<OptionConfig> configList = c.getOptionConfigs();
        List<CommentPhotoOptionRep> result = new ArrayList<>(configList.size());
        for (OptionConfig oc : configList) {
            CommentPhotoOptionRep rep = new CommentPhotoOptionRep();
            rep.id = oc.id;
            rep.name = oc.name;
            rep.coverImageUrl = oc.coverImageUrl;
            result.add(rep);
        }
        return Response.ok(result).build();
    }


    @Override
    @Path("/photopicker/{id}")
    @GET
    @Produces("application/json")
    public Response getCommentPhotoPickerOption(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") IntParam optionIdParam,
        @BeanParam PageParam pageParam)
    {
        Rests.beginOperation(headers, "cmt:getPickerOption:" + optionIdParam.get());
        Page page = pageParam.get();

        CommentPhotoOptionItemRepList repList = new CommentPhotoOptionItemRepList();
        CommentPhotoPickerOptionsConfig c = Configs.get(CommentPhotoPickerOptionsConfig.class);
        List<ItemConfig> configList = c.getItemConfigs(optionIdParam.get(), page);
        for (ItemConfig ic : configList) {
            CommentPhotoOptionItemRep rep = new CommentPhotoOptionItemRep();
            rep.id = ic.getId();

            MediaRep mr = new MediaRep();
            mr.type = MediaType.PHOTO;
            mr.photoFormat = PhotoMedia.Format.PNG; // Format doesn't matter here
            if (StringUtils.isNotEmpty(ic.getUrl())) {
                mr.photoUrl = ic.getUrl();
                mr.photoW = ic.getWidth();
                mr.photoH = ic.getHeight();
                PhotoMedia.Format fmt = PhotoMedia.guessFormatFromUrl(ic.getUrl());
                if (fmt != null)
                    mr.photoFormat = fmt;
            }
            if (StringUtils.isNotEmpty(ic.getThumbnailUrl())) {
                mr.photoThumbnailUrl = ic.getThumbnailUrl();
                mr.photoThumbnailW = ic.getWidth();
                mr.photoThumbnailH = ic.getHeight();
            }
            rep.media = mr;
            repList.add(rep);
        }

        if (repList.hasData()) {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                page,
                GDUtils.getFirst(configList),
                GDUtils.getLast(configList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getComments(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("post_id") LongParam idParam,
        @QueryParam("all") @DefaultValue("0") BoolParam allParam,
        @BeanParam PageParam pageParam)
    {
        long callerId = Rests.beginOperation(headers, "cmt:getComments:" + idParam.get());
        long id = idParam.get();
        Page p = pageParam.get();

        GDUtils.check(
            this.postRepo.exists(id),
            Status.NOT_FOUND,
            "No post with ID " + id);

        List<Comment> cmtList = this.cmtRepo.findByPost(id, p, null, allParam.get());
        CommentRepList repList = new CommentRepList();
        Reps.populateCommentListBasic(repList, cmtList, callerId);

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(cmtList),
                GDUtils.getLast(cmtList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }


    @Override
    @Path("/{id}/like")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response likeComment(
        @Context HttpHeaders headers,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @FormParam("status") BoolParam statusParam)
    {
        long callerId = Rests.beginOperation(headers, "cmt:like:" + idParam.get() + ":" + statusParam.get());
        LOG.warn("Deprecated likeCmt callerId={}", callerId);
        return Response.ok().build();
    }


    @Override
    @Path("/{id}/vote")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response voteComment(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("down") @DefaultValue("0") BoolParam isDownParam,
        @FormParam("up") @DefaultValue("0") BoolParam isUpParam,
        @FormParam("undown") @DefaultValue("0") BoolParam isUndownParam,
        @FormParam("unup") @DefaultValue("0") BoolParam isUnupParam)
    {
        boolean isDown = isDownParam.get();
        boolean isUp = isUpParam.get();
        boolean isUndown = isUndownParam.get();
        boolean isUnup = isUnupParam.get();

        long callerId = -1L;
        if (isDown || isUp) {
            callerId = Rests.beginOperation(headers, "cmt:vote:" + idParam.get() + ":" + (isDown ? "d" : "u"));
        }
        else if (isUndown || isUnup) {
            callerId = Rests.beginOperation(headers, "cmt:vote:" + idParam.get() + ":" + (isUndown ? "ud" : "uu"));
        }

        if (callerId < 0L)
            return Response.status(Status.BAD_REQUEST).entity("No vote is taken").build();

        long cmtId = idParam.get();
        Comment cmt = Posts.getComment(cmtId);
        GDUtils.check(cmt != null, Status.NOT_FOUND, "No comment with ID " + cmtId);

        if (isDown || isUp)
            this.cmtRepo.incrCount(cmtId, isDown ? CommentCount.Field.DOWNVOTE : CommentCount.Field.UPVOTE, 1L);
        if (isUndown || isUnup)
            this.cmtRepo.incrCount(cmtId, isUndown ? CommentCount.Field.DOWNVOTE : CommentCount.Field.UPVOTE, -1L);

        if (isDown || isUp) {
            VoteStatus status = isDown ? VoteStatus.DOWNVOTE : VoteStatus.UPVOTE;
            if (this.votesCommentRel.hasEdge(callerId, cmtId)) {
                this.votesCommentRel.modifyEdgeByData(
                    callerId,
                    cmtId,
                    null,
                    null,
                    null,
                    new Long(status.ordinal()), // l1
                    true, // assign l1
                    null,
                    false);
            }
            else {
                LEntry node = new LEntry(cmtId).with(status.ordinal());
                this.votesCommentRel.addEdge(callerId, node);
                this.isCmtVotedByRel.addEdge(cmtId, callerId);
            }
        }
        else if (isUndown || isUnup) {
            this.votesCommentRel.deleteEdge(callerId, cmtId);
            this.isCmtVotedByRel.deleteEdge(cmtId, callerId);
        }

        int delta = 0;
        if (isDown)
            delta -= 1;
        if (isUnup)
            delta -= 1;
        if (isUndown)
            delta += 1;
        if (isUp)
            delta += 1;
        Event event = new Event(EventType.VOTE_COMMENT);
        event.lng1 = callerId;
        event.lng2 = cmtId;
        event.lng3 = cmt.getOwnerId();
        event.int1 = delta;
        event.bool1 = (isUnup && !isDown) || (isUndown && !isUp);
        this.eventQ.enq(event);

        return Response.ok().build();
    }


//    private List<Media> fakeMedia()
//    {
//        String[] urls = {
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/1.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/2.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/3.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/4.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/5.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/6.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/7.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/8.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/9.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/10.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/11.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/12.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/13.png", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/14.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/15.gif",
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/16.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/17.png", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/18.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/19.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/20.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/21.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/22.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/23.gif",
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/24.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/25.gif", 
//            "https://s3-us-west-1.amazonaws.com/media-dev.colonyapp.com/static/cmtphotopicker/26.gif",
//        };
//        
//        String[] dimensions = {
//            "366x232",
//            "320x179",
//            "370x344",
//            "190x137",
//            "500x281",
//            "500x257",
//            "320x180",
//            "400x213",
//            "606x359",
//            "345x200",
//            "440x245",
//            "500x292",
//            "610x457",
//            "598x448",
//            "350x200",
//            "351x259",
//            "460x480",
//            "400x224",
//            "500x281",
//            "500x269",
//            "225x225",
//            "500x197",
//            "500x281",
//            "400x225",
//            "499x281",
//            "500x280"
//        };
//        
//        List<Media> list = new ArrayList<>();
//        list.add(null);
//        for (int i = 0; i < urls.length; i++) {
//            PhotoMedia pm = new PhotoMedia();
//            String url = urls[i];
//            PhotoMedia.Format fmt = PhotoMedia.Format.valueOf(StringUtils.substringAfterLast(url, ".").toUpperCase());
//            int w = Integer.parseInt(dimensions[i].split("x")[0]);
//            int h = Integer.parseInt(dimensions[i].split("x")[1]);
//            pm.setFormat(fmt);
//            pm.setLargeWidth(w);
//            pm.setLargeHeight(h);
//            pm.setLarge(url);
//            
//            if (fmt == PhotoMedia.Format.GIF) {
//                // The thumbnail stores the first frame of gif
//                pm.setThumbnail(url.replace(".gif",".jpg"));
//                pm.setThumbnailWidth(w);
//                pm.setThumbnailHeight(h);
//            }
//            
//            list.add(pm);
//        }
//        return list;
//    }


    private Media itemConfigToMedia(
        ItemConfig cfg)
    {
        PhotoMedia pm = new PhotoMedia();
        // Format doesn't really matter that much, we try to guess based on extension
        // If no ext found, just assume PNG
        pm.setFormat(PhotoMedia.Format.PNG);
        if (StringUtils.isNotEmpty(cfg.getUrl())) {
            pm.setLarge(cfg.getUrl());
            pm.setLargeWidth(cfg.getWidth());
            pm.setLargeHeight(cfg.getHeight());
            PhotoMedia.Format fmt = PhotoMedia.guessFormatFromUrl(cfg.getUrl());
            if (fmt != null)
                pm.setFormat(fmt);
        }
        if (StringUtils.isNotEmpty(cfg.getThumbnailUrl())) {
            pm.setThumbnail(cfg.getThumbnailUrl());
            pm.setThumbnailWidth(cfg.getWidth());
            pm.setThumbnailHeight(cfg.getHeight());
        }
        return pm;
    }
}
