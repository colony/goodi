package gd.impl.comment;

public class CommentCount
{
    private long numDownvotes;
    
    private long numReplies;
    
    private long numUpvotes;
    
    
    public static enum Field
    {
        DOWNVOTE,
        REPLY,
        UPVOTE,
    }
    
    
    public CommentCount()
    {
    }

    
    //
    // GETTERS & SETTERS
    //
    

    public long getNumDownvotes()
    {
        return numDownvotes;
    }


    public void setNumDownvotes(
        long numDownvotes)
    {
        this.numDownvotes = numDownvotes;
    }


    public long getNumReplies()
    {
        return numReplies;
    }


    public void setNumReplies(
        long numReplies)
    {
        this.numReplies = numReplies;
    }


    public long getNumUpvotes()
    {
        return numUpvotes;
    }


    public void setNumUpvotes(
        long numUpvotes)
    {
        this.numUpvotes = numUpvotes;
    }
}
