
package gd.impl.comment;


import gd.api.v1.post.Geo;
import gd.impl.media.IconMedia;
import gd.support.protobuf.CommentProtos.CommentProto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.Conversion;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.AbstractIterator;


public class CommentExporter
{
    public static final int HEADER_MAGIC = 0x19890828;

    public static final int HEADER_VERSION = 1;

    private CommentProto.Builder builder;

    private int version;


    public CommentExporter()
    {
        this.builder = CommentProto.newBuilder();
        this.version = HEADER_VERSION;
    }


    /**
     * Construct an exporter with specified version
     */
    public CommentExporter(
        int version)
    {
        this.builder = CommentProto.newBuilder();
        this.version = version;
    }


    public Iterator<Comment> read(
        InputStream in)
        throws IOException
    {
        readHeader(in);
        switch (this.version)
        {
        case 1:
            return new V1Iterator(in);

        default:
            throw new IllegalStateException(
                "Unsupported version number: " + this.version);
        }
    }
    

    private int readHeader(
        InputStream in)

        throws IOException
    {
        byte[] b = new byte[4];
        in.read(b);
        int magic = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (magic != HEADER_MAGIC)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid magic number 0x%08x, expected 0x%08x",
                    magic,
                    HEADER_MAGIC));
        }

        b = new byte[4];
        in.read(b);
        int version = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (version <= 0 || version > HEADER_VERSION)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid version number %d, expected %d",
                    version,
                    HEADER_VERSION));
        }

        this.version = version;
        return version;
    }


    public void write(
        OutputStream out,
        Iterator<Comment> it)
        throws IOException
    {
        writeHeader(out);

        while (it.hasNext()) {
            Comment c = it.next();
            if (c == null)
                continue;

            this.writeComment(out, c);
        }
    }


    public void writeComment(
        OutputStream out,
        Comment c)
        throws IOException
    {
        CommentProto.Builder b = this.builder;

        // Clear inner state before reuse
        b.clear();

        b.setBody(c.getBody());
        b.setCreateTime(c.getCreateTime().getTime());
        if (null != c.getGeo())
            b.setGeo(c.getGeo().toStr());
        if (c.hasHashtags())
            b.setHashtags(StringUtils.join(c.getHashtags(), " "));
        b.setId(c.getId());
        if (null != c.getOwnerIcon())
            b.setOwnerIcon(c.getOwnerIcon().toStr());
        b.setOwnerId(c.getOwnerId());
        if (null != c.getParentId())
            b.setParentId(c.getParentId());
        b.setPostId(c.getPostId());

        b.build().writeDelimitedTo(out);
    }


    public void writeHeader(
        OutputStream out)

        throws IOException
    {
        out.write(Conversion.intToByteArray(
            HEADER_MAGIC,
            0,
            new byte[4],
            0,
            4));
        out.write(Conversion.intToByteArray(
            this.version,
            0,
            new byte[4],
            0,
            4));
    }



    class V1Iterator
        extends
            AbstractIterator<Comment>
    {
        InputStream in;


        public V1Iterator(
            InputStream in)
        {
            this.in = in;
            builder.clear();
        }


        @Override
        protected Comment computeNext()
        {
            CommentProto.Builder b = builder;
            boolean success;
            try
            {
                success = b.mergeDelimitedFrom(in);

                if (!success)
                {
                    // EOF or some other reason
                    return endOfData();
                }

                // Deserialize using Google protobuf
                CommentProto cp = b.build();
                Comment c = new Comment();
                c.setBody(cp.getBody());
                c.setCreateTime(new Date(cp.getCreateTime()));
                if (cp.hasGeo())
                    c.setGeo(new Geo(cp.getGeo()));
                if (cp.hasHashtags()) {
                    List<String> hashtags = Arrays.asList(cp.getHashtags().split(" "));
                    c.setHashtags(hashtags);
                }
                c.setId(cp.getId());
                if (cp.hasOwnerIcon()) {
                    c.setOwnerIcon((IconMedia) new IconMedia().fromStr(cp.getOwnerIcon()));
                }
                c.setOwnerId(cp.getOwnerId());
                if (cp.hasParentId())
                    c.setParentId(cp.getParentId());
                c.setPostId(cp.getPostId());

                // Clean inner state of builder before next mergeFrom
                b.clear();
                return c;
            }
            catch (IOException e)
            {
                throw new RuntimeException(
                    "Error occurs when importing the data: "
                        + e.getMessage(),
                    e);
            }
        }
    }
}
