
package gd.impl.comment;


import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.impl.comment.CommentCount.Field;
import gd.impl.repository.RepositoryConfig;
import gd.impl.util.AbstractChainableFilter;
import gd.impl.util.Filter;
import gd.impl.util.Page;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


public class ResidentCommentRepository
    implements
        CommentRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ResidentCommentRepository.class);

    /**
     * Indexed by ID. Deleted comments will be null.
     */
    protected List<Comment> comments;


    public ResidentCommentRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public synchronized Comment add(
        Comment c)
    {
        // Assign a new ID to the post
        int id = this.comments.size();
        c.setId(id);

        // idk, sometimes, we already set the create time outside
        if (c.getCreateTime() == null)
            c.setCreateTime(new Date());

        // Append to the end of list
        this.comments.add(c);

        return new Comment(c);
    }


    @Override
    public synchronized boolean delete(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        Comment cmt = this.comments.get(intId);
        if (cmt == null)
            return false;

        // Delete the entry by setting to null so as to avoid array shifting
        this.comments.set(intId, null);
        return true;
    }


    @Override
    public synchronized boolean exists(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        return this.comments.get(intId) != null;
    }


    @Override
    public List<Comment> find(
        Page page,
        Filter<Comment> filter)
    {
        return this.findGeneric(page, filter, false);
    }


    @Override
    public List<Comment> findByOwner(
        Long ownerId,
        Page page,
        Filter<Comment> filter)
    {
        Filter<Comment> ownerFilter = new OwnerFilter(ownerId);
        if (filter != null)
            filter.chainFilter(ownerFilter);
        else
            filter = ownerFilter;
        return this.findGeneric(page, ownerFilter, false);
    }


    @Override
    public List<Comment> findByPost(
        Long postId,
        Page page,
        Filter<Comment> filter,
        boolean all)
    {
        Filter<Comment> postFilter = new PostFilter(postId);
        if (filter != null)
            filter.chainFilter(postFilter);
        else
            filter = postFilter;
        return this.findGeneric(page, filter, all);
    }


    @Override
    public synchronized Comment get(
        Long id)
    {
        if (!checkBoundary(id))
            return null;

        int intId = id.intValue();
        return this.comments.get(intId);
    }


    @Override
    public synchronized CommentCount getCount(
        Long id)
    {
        Comment c = get(id);
        if (c == null)
            return null;

        if (c.getCount() == null)
            return new CommentCount();

        return c.getCount();
    }


    @Override
    public synchronized void incrCount(
        Long cmtId,
        Field count,
        long incr)
    {
        Comment c = this.get(cmtId);
        if (c == null)
            return;
        
        CommentCount cc = c.getCount();
        if (cc == null) {
            cc = new CommentCount();
            c.setCount(cc);
        }
        
        switch (count)
        {
        case DOWNVOTE:
            cc.setNumDownvotes(cc.getNumDownvotes() + incr);
            break;
            
        case REPLY:
            cc.setNumReplies(cc.getNumReplies() + incr);
            break;
            
        case UPVOTE:
            cc.setNumUpvotes(cc.getNumUpvotes() + incr);
            break;
        }
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        this.comments = new ArrayList<>();
        this.comments.add(null);           // post ID 0 is not used
    }


    @Override
    public synchronized Comment put(
        Comment c)
    {
        // ID is default value, add this new comment
        if (c.getId() == 0L) {
            // Safe to return, since add() returns a copy one
            return this.add(c);
        }

        if (!this.exists(c.getId()))
            throw new IllegalArgumentException("Can't find comment id: " + c.getId());

        // PUT will override everything, even non-modifiable fields (i.e. create_time)
        this.comments.set((int) c.getId(), c);
        return new Comment(c);
    }


    @Override
    public synchronized Comment update(
        Comment newCmt)
    {
        throw new UnsupportedOperationException(this.getClass()
            .getName() + " doesn't support update operation");
    }


    @Override
    public void visitAll(
        Visitor<Comment> visitor)
    {
        // This is not synchronized, use with cautions

        if (!visitor.before())
            return;

        // We don't need to worry about outOfBoundary because
        // delete will only nullify items instead of remove
        // Also, we don't use iterator to avoid ConcurrentModificationException
        int num = this.comments.size();
        for (int i = 1; i < num; ++i)
        {
            // Maybe stale in concurrent system
            Comment cmt = this.comments.get(i);

            // Ignore null
            if (cmt == null)
            {
                continue;
            }

            if (!visitor.visit(cmt))
            {
                // Visitor aborts
                break;
            }
        }

        // Clean up
        visitor.after();
    }


    //
    // RepositoryAdmin
    //

    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.exists(path))
        {
            LOG.warn("Export file \"{}\" already exists", fname);
        }

        LOG.info("Exporting comments to file {}", fname);
        try (
             OutputStream os =
                 Files.newOutputStream(path, CREATE, WRITE, TRUNCATE_EXISTING);
             BufferedOutputStream bos =
                 new BufferedOutputStream(os);)
        {
            CommentExporter exporter = new CommentExporter();
            exporter.write(bos, this.comments.iterator());
        }
        LOG.info("Done exporting comments");
    }


    @Override
    public synchronized void importData(
        String fname)
        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.notExists(path)) {
            throw new IllegalArgumentException("Import file " + fname + " not exist");
        }

        LOG.info("Imporing comments from file {}", fname);
        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            CommentExporter exporter = new CommentExporter();
            Iterator<Comment> it = exporter.read(bis);

            // Wipe out current data
            this.comments.clear();
            this.comments.add(null); // ID 0 is always null

            int i = 1;
            while (it.hasNext())
            {
                Comment p = it.next();

                // The in-memory list adds nulls for deleted/missing posts
                // Here, we need fill up the gap during import
                for (int j = i; j < p.getId(); ++j)
                {
                    this.comments.add(null);
                }
                this.comments.add(p);
            }
        }
        LOG.info("Done importing comments");
    }


    //
    // INTERNAL METHODS
    //

    protected boolean checkBoundary(
        Long id)
    {
        // Internally, we store data by using an array with integer
        // position as index. The id can't go beyond boundary
        return (id != null &&
            id > 0 && id < this.comments.size());
    }


    protected List<Comment> findGeneric(
        Page page,
        Filter<Comment> filter,
        boolean all)
    {
        final String key =
            StringUtils.defaultIfEmpty(
                page.key,
                SK_ID);
        switch (key)
        {
        case SK_ID:
            Long boundary =
                Strings.isNullOrEmpty(page.boundary) ? null : Long.valueOf(page.boundary);
            boolean inBound = checkBoundary(boundary);
            int size = this.comments.size();

            // 1. determine startId;
            int startId;
            boolean decreasing; // order of traversal
            if (page.isDescending())
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
            }
            else
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
            }

            // 2. do traversal
            List<Comment> result = new ArrayList<>();
            int num = 0;
            for (int id = startId; (decreasing ? id > 0 : id < size);)
            {
                Comment p = this.comments.get(id);
                id = decreasing ? id - 1 : id + 1;

                if (p == null)
                    continue;

                if (filter != null && !filter.accept(p))
                    continue;

                // Copy over, since this is resident (in-memory) data structure
                Comment c = new Comment(p);
                c.setSortKey(key);
                c.setSortVal(String.valueOf(c.getId()));
                result.add(c);
                if (!all && ++num >= page.size)
                    break;
            }

            // 3. in BEFORE, we traverse in a reverse direction of page.order
            if (!page.isAfter())
                Collections.reverse(result);
            return result;

        default:
            throw new UnsupportedOperationException("Unsupported sort key " + key);
        }
    }



    //
    // INTERNAL CLASSES
    //

    private class OwnerFilter
        extends
            AbstractChainableFilter<Comment>
        implements
            Filter<Comment>
    {
        long ownerId;


        public OwnerFilter(
            long ownerId)
        {
            this.ownerId = ownerId;
        }


        @Override
        public boolean accept(
            Comment c)
        {
            return c.getOwnerId() == this.ownerId;
        }
    }



    private class PostFilter
        extends
            AbstractChainableFilter<Comment>
        implements
            Filter<Comment>
    {
        long postId;


        public PostFilter(
            long postId)
        {
            this.postId = postId;
        }


        @Override
        public boolean accept(
            Comment c)
        {
            return c.getPostId() == this.postId;
        }
    }
}
