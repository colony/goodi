
package gd.impl.comment;


import gd.api.v1.comment.CommentType;
import gd.api.v1.post.Geo;
import gd.impl.media.IconMedia;
import gd.impl.media.Media;
import gd.impl.util.Sortable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Comment
    implements
        Sortable
{
    private String body;

    /**
     * A struct containing different type of counts on comment
     */
    private CommentCount count;

    private Date createTime;

    private Geo geo;

    private List<String> hashtags;

    private long id;
    
    private Media media;

    private IconMedia ownerIcon;

    private long ownerId;

    /**
     * A comment may not have parent, we use object instead of
     * primitive type
     */
    private Long parentId;

    private long postId;

    /**
     * sortKey and sortVal are not persisted. Both fields are
     * normally null unless the post is part of collection (i.e.
     * feed). The fields indicate how the post is sorted.
     */
    private String sortKey;

    private String sortVal;
    
    private CommentType type;


    public Comment()
    {
    }


    /**
     * Copy constructor
     */
    public Comment(
        Comment c)
    {
        this.body = c.body;
        this.count = c.count;
        this.createTime = c.createTime;
        this.geo = c.geo;
        this.id = c.id;
        this.media = c.media;
        this.ownerIcon = c.ownerIcon;
        this.ownerId = c.ownerId;
        this.parentId = c.parentId;
        this.postId = c.postId;
        this.sortKey = c.sortKey;
        this.sortVal = c.sortVal;
        this.type = c.type;

        if (c.hashtags != null)
            this.hashtags = new ArrayList<>(c.hashtags);
    }


    public boolean hasHashtags()
    {
        return this.hashtags != null && !this.hashtags.isEmpty();
    }
    
    
    public boolean hasMedia()
    {
        return media != null;
    }


    public boolean hasParent()
    {
        return null != this.parentId;
    }


    //
    // Getters & Setters
    //

    public String getBody()
    {
        return body;
    }


    public void setBody(
        String body)
    {
        this.body = body;
    }


    public CommentCount getCount()
    {
        return count;
    }


    public void setCount(
        CommentCount count)
    {
        this.count = count;
    }


    public Date getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        Date createTime)
    {
        this.createTime = createTime;
    }


    public Geo getGeo()
    {
        return geo;
    }


    public void setGeo(
        Geo geo)
    {
        this.geo = geo;
    }


    public void addHashtag(
        String hashtag)
    {
        if (this.hashtags == null)
            this.hashtags = new ArrayList<>();
        this.hashtags.add(hashtag);
    }


    public int getNumHashtags()
    {
        return this.hashtags == null ? 0 : this.hashtags.size();
    }


    public List<String> getHashtags()
    {
        return hashtags;
    }


    public void setHashtags(
        List<String> ht)
    {
        this.hashtags = ht;
    }


    public long getId()
    {
        return id;
    }


    public void setId(
        long id)
    {
        this.id = id;
    }
    
    
    public Media getMedia()
    {
        return media;
    }
    
    
    public void setMedia(Media media)
    {
        this.media = media;
    }


    public IconMedia getOwnerIcon()
    {
        return ownerIcon;
    }


    public void setOwnerIcon(
        IconMedia ownerIcon)
    {
        this.ownerIcon = ownerIcon;
    }


    public long getOwnerId()
    {
        return ownerId;
    }


    public void setOwnerId(
        long ownerId)
    {
        this.ownerId = ownerId;
    }


    public Long getParentId()
    {
        return parentId;
    }


    public void setParentId(
        Long parentId)
    {
        this.parentId = parentId;
    }


    public long getPostId()
    {
        return postId;
    }


    public void setPostId(
        long postId)
    {
        this.postId = postId;
    }


    public String getSortKey()
    {
        return sortKey;
    }


    public void setSortKey(
        String sortKey)
    {
        this.sortKey = sortKey;
    }


    public String getSortVal()
    {
        return sortVal;
    }


    public void setSortVal(
        String sortVal)
    {
        this.sortVal = sortVal;
    }
    
    
    public CommentType getType()
    {
        return type;
    }
    
    
    public void setType(CommentType type)
    {
        this.type = type;
    }
}
