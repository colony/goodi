
package gd.impl;


import gd.impl.config.Configured;
import gd.impl.config.GlobalProperties;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.regions.Regions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;


// Not reloadable
@Configured(fname = "component.yml")
public class ComponentConfig
{
    // Default APNS configuration
    private APNSConfig apns = new APNSConfig();

    // Default AWS configuration
    private AWSConfig aws = new AWSConfig();

    // Default HTTP configuration
    private HttpConfig http = new HttpConfig();

    // Default Mail configuration
    private MailConfig mail = new MailConfig();

    // Default Media configuration
    private MediaConfig media = new MediaConfig();

    // Default Scheduler configuration
    private SchedulerConfig scheduler = new SchedulerConfig();

    // Default Sqlite configuration
    private SqliteConfig sqlite = new SqliteConfig();


    /**
     * Default constructor
     */
    public ComponentConfig()
    {
    }


    /**
     * Validate critical non-default configuration that can't be
     * missed during system lifecycle. The method should be called
     * at boot time, and fail if the validation doesn't pass
     */
    public boolean validate()
    {
        if (Strings.isNullOrEmpty(media.mediaBaseUrl))
            return false;

        if (apns.isEnabled()
            && StringUtils.isAnyEmpty(
                apns.getCertificateFile(),
                apns.getCertificatePassword()))
            return false;

        if (mail.mode == MailMode.SES
            && StringUtils.isAnyEmpty(
                aws.getAccessKey(),
                aws.getSecretKey()))
            return false;

        // Other check goes here

        return true;
    }


    // N.B. There is only getters but no setters
    //      The fields are populated by YAML marshal

    public APNSConfig getAPNS()
    {
        return apns;
    }


    public AWSConfig getAWS()
    {
        return aws;
    }


    public HttpConfig getHttp()
    {
        return http;
    }


    public MailConfig getMail()
    {
        return mail;
    }


    public MediaConfig getMedia()
    {
        return media;
    }


    public SchedulerConfig getScheduler()
    {
        return scheduler;
    }


    public SqliteConfig getSqlite()
    {
        return sqlite;
    }



    //
    // Per SUB-CONFIGURATION
    //

    public class APNSConfig
    {
        /**
         * The name of sound file to be played when push
         * notification is received. Null to play no sound
         */
        private String alertSound = null;

        private String certificateFile;

        private String certificatePassword;

        private APNSMode mode = APNSMode.DISABLED;

        /**
         * The number of threads spin up for APNS service
         */
        private int numThreads = 1;


        public APNSConfig()
        {
        }


        public String getAlertSound()
        {
            return alertSound;
        }


        public String getCertificateFile()
        {
            return certificateFile;
        }


        public String getCertificatePassword()
        {
            return certificatePassword;
        }


        public APNSMode getMode()
        {
            return mode;
        }


        public int getNumThreads()
        {
            return numThreads;
        }


        public boolean isEnabled()
        {
            return mode != APNSMode.DISABLED;
        }


        public boolean isProd()
        {
            return mode == APNSMode.PROD;
        }


        public boolean isSandbox()
        {
            return mode == APNSMode.SANDBOX;
        }
    }



    public enum APNSMode
    {
        DISABLED,
        PROD,
        SANDBOX,
    }



    public class AWSConfig
    {
        private String accessKey;

        private String secretKey;


        public AWSConfig()
        {
        }


        public String getAccessKey()
        {
            return accessKey;
        }


        public String getSecretKey()
        {
            return secretKey;
        }
    }



    public class HttpConfig
    {
        // Connection manager settings

        /**
         * Maximum concurrent connections per route
         */
        private int maxConnectionsPerRoute = 2;

        /**
         * Maximum concurrent connections in total
         */
        private int maxTotalConnections = 20;


        // Request settings

        /**
         * Client tries to connect to the server. This denotes the
         * time elapsed before the connection established or Server
         * responded to connection request in milliseconds.
         */
        private int connectTimeout = 5000;

        /**
         * Maximum period of inactivity waiting on a connection
         * lease request in milliseconds. (A lease request asks the
         * connection manager to return pooled connection)
         */
        private int connectionRequestTimeout = 5000;

        /**
         * In other words its maximum period inactivity between two
         * consecutive data packets arriving at client side after
         * connection is established in milliseconds
         */
        private int soTimeout = 5000;


        // Socket settings

        /**
         * Causes a packet (called a 'keepalive probe') to be sent
         * to the remote system if a long time (by default, more
         * than 2 hours) passes with no other data being sent or
         * received. This packet is designed to provoke an ACK
         * response from the peer. This enables detection of a peer
         * which has become unreachable
         */
        private Boolean soKeepAlive = false;

        /**
         * Disables a part of the Tcp stack which essentially
         * buffers Tcp packets until there are enough to basically
         * make the sending worthwhile.
         */
        private Boolean tcpNoDelay = false;


        HttpConfig()
        {
        }


        public int getMaxConnectionsPerRoute()
        {
            return maxConnectionsPerRoute;
        }


        public int getMaxTotalConnections()
        {
            return maxTotalConnections;
        }


        public int getConnectTimeout()
        {
            return connectTimeout;
        }


        public int getConnectionRequestTimeout()
        {
            return connectionRequestTimeout;
        }


        public int getSoTimeout()
        {
            return soTimeout;
        }


        public Boolean getSoKeepAlive()
        {
            return soKeepAlive;
        }


        public Boolean getTcpNoDelay()
        {
            return tcpNoDelay;
        }
    }



    public class MailConfig
    {
        private MailMode mode =
            MailMode.DUMMY;

        /**
         * AWS region when using SES. Note that production access
         * status, sending limits, and Amazon SES identity-related
         * settings are specific to a given AWS region
         */
        private Regions region = Regions.US_EAST_1;


        public MailConfig()
        {
        }


        public MailMode getMode()
        {
            return mode;
        }


        public Regions getRegion()
        {
            return region;
        }


        public boolean isDummy()
        {
            return mode == MailMode.DUMMY;
        }
    }



    public enum MailMode
    {
        DUMMY,
        SES;
    }



    public class MediaConfig
    {
        /**
         * Local media root directory, used for FileManager
         */
        private Path localDir = GlobalProperties.instance().mediaDir();

        private String mediaBaseUrl;

        private MediaMode mode = MediaMode.LOCAL;

        private String s3Bucket;

        /**
         * s3 endpoint address, used for S3FileManager
         */
        private String s3Endpoint;


        public MediaConfig()
        {
        }


        public Path getLocalDir()
        {
            return localDir;
        }


        public String getMediaBaseUrl()
        {
            return mediaBaseUrl;
        }


        public MediaMode getMode()
        {
            return mode;
        }


        public String getS3Bucket()
        {
            return s3Bucket;
        }


        public String getS3Endpoint()
        {
            return s3Endpoint;
        }


        @SuppressWarnings("unused")
        private void setLocalDir(
            String pathStr)
        {
            this.localDir = Paths.get(pathStr);
        }
    }



    public enum MediaMode
    {
        LOCAL,
        S3;
    }



    public class SchedulerConfig
    {
        private long flushInterval = 10 * 1000; //ms

        /**
         * Path of file that persist all scheduled job infos
         */
        private Path persistPath = GlobalProperties.instance().dataDir().resolve("scheduler");

        /**
         * A list of job class names
         */
        private List<String> registerJobClasses = ImmutableList.of();

        private String timeZoneId = "UTC";


        public SchedulerConfig()
        {
        }


        public long getFlushInterval()
        {
            return flushInterval;
        }


        public List<String> getRegisterJobClasses()
        {
            return registerJobClasses;
        }


        public Path getPersistPath()
        {
            return persistPath;
        }


        public String getTimeZoneId()
        {
            return timeZoneId;
        }


        @SuppressWarnings("unused")
        private void setPersistPath(
            String pathStr)
        {
            this.persistPath = Paths.get(pathStr);
        }
    }



    public class SqliteConfig
    {
        /**
         * Path of Sqlite db file (NOTE: only one database file
         * containing all tables)
         */
        private Path path = GlobalProperties.instance()
            .dataDir()
            .resolve("sql.db");


        public SqliteConfig()
        {
        }


        public Path getPath()
        {
            return path;
        }


        @SuppressWarnings("unused")
        private void setPath(
            String pathStr)
        {
            this.path = Paths.get(pathStr);
        }
    }
}
