
package gd.impl.post;


import gd.api.v1.post.MediaResource;
import gd.api.v1.post.MediaType;
import gd.impl.ComponentConfig;
import gd.impl.Rests;
import gd.impl.config.Configs;
import gd.impl.media.FileManager;
import gd.impl.util.GDUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


@Path("/medias")
@Singleton
public class MediaResourceImpl
    implements
        MediaResource
{
    protected String baseUrl;

    protected FileManager fileMgr;

    protected Map<String,MediaType> mimeToType;


    public MediaResourceImpl()
    {
        this.fileMgr = FileManager.instance();

        mimeToType = new HashMap<>();
        mimeToType.put("image/gif", MediaType.PHOTO);
        mimeToType.put("image/jpeg", MediaType.PHOTO);
        mimeToType.put("image/png", MediaType.PHOTO);
//        mimeToType.put("text/html", MediaType.TEXT);
//        mimeToType.put("text/plain", MediaType.TEXT);
        // For now, other MediaType doesn't have MIME

        ComponentConfig cfg = Configs.get(ComponentConfig.class);
        this.baseUrl = cfg.getMedia().getMediaBaseUrl();
    }


    @Override
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createMedia(
        @Context HttpHeaders headers,
        FormDataMultiPart multiPart)
    {
        Rests.beginOperation(headers, "media:create");

        if (multiPart == null)
        {
            return (Response.status(Status.BAD_REQUEST)
                .entity("Empty body in uploaded media")
                .build());
        }

        // Mapping from name to file key
        Map<String,String> result = new HashMap<>();
        Map<String,String> localIdToKey = new HashMap<>();
        boolean success = false;
        try
        {
            for (BodyPart p : multiPart.getBodyParts())
            {
                FormDataBodyPart part = (FormDataBodyPart) p;

                // Handle the upload based on per Content-Type
                FormDataContentDisposition contentDisp =
                    part.getFormDataContentDisposition();
                MediaType mediaType = parseMediaType(part);

                String name = contentDisp.getName();
                if (Strings.isNullOrEmpty(name))
                {
                    return (Response.status(Status.BAD_REQUEST)
                        .entity(
                            "Empty name in content-disposition is not allowed")
                        .build());
                }

                // The contentName is in format of localId[:partName]
                // 1. multipart/form-data payload could contain data from multiple files
                // 2. localId is used to differentiate them, same localId must be avoided
                // and the consequence is unpredictable.
                // 3. partName, if provided, is appended as _partName to the file key. A file
                // key uniquely corresponds to tuple (localId, partName), hence, same localId
                // will have same file prefix but appended with different partName. This is 
                // particular useful to upload image with different dimension
                String[] tokens = name.split(":", 2);
                String localId = tokens[0];
                String partName = null;
                String key = null;
                if (tokens.length == 2)
                {
                    partName = tokens[1];
                }

                if (partName == null)
                {
                    key =
                        this.fileMgr.store(
                            part.getValueAs(InputStream.class),
                            mediaType,
                            null,
                            0L,
                            null,
                            null);
                    localIdToKey.put(localId, key);
                }
                else
                {
                    if (localIdToKey.containsKey(localId))
                    {
                        key =
                            localIdToKey.get(localId) + "_"
                                + partName;
                        this.fileMgr.storeWithKey(
                            part.getValueAs(InputStream.class),
                            null,
                            key);
                    }
                    else
                    {
                        String appendKey = "_" + partName;
                        key =
                            this.fileMgr.store(
                                part.getValueAs(InputStream.class),
                                mediaType,
                                null,
                                0L,
                                appendKey,
                                null);
                        localIdToKey.put(
                            localId,
                            key.substring(0, key.length()
                                - appendKey.length()));
                    }
                }

                if (key != null)
                    result.put(name, key);
            }

            success = true;

            // Now, we are using local file system for storage
            // all the keys are relative path (i.e. /tmp/blahblah)
            // we need convert them to some public facing path so 
            // that file are accessible via HTTP
            for (Map.Entry<String,String> entry : result.entrySet())
            {
                result.put(
                    entry.getKey(),
                    GDUtils.joinPath(this.baseUrl, entry.getValue()));
            }

            return Response.ok(result, "application/json")
                .build();
        }
        catch (IOException e)
        {
            return (Response.status(Status.INTERNAL_SERVER_ERROR)
                .entity("Fail to persist all uploaded medias")
                .build());
        }
        finally
        {
            if (!success)
            {
                for (String savedKey : result.values())
                {
                    this.fileMgr.deleteQuietly(savedKey);
                }
            }
            multiPart.cleanup();
        }
    }


    @Override
    @DELETE
    public Response deleteMedia(
        @Context HttpHeaders headers,
        @QueryParam("uri") String uri)
    {
        GDUtils.check(
            !Strings.isNullOrEmpty(uri),
            Status.BAD_REQUEST,
            "Cannot delete media with empty uri");
        Rests.beginOperation(headers, "media:delete:" + uri);

        try
        {
            this.fileMgr.delete(uri);
        }
        catch (IOException e)
        {
            LOG.error(
                "Fail to delete uri \""
                    + uri
                    + "\": "
                    + e.getMessage(),
                e);
            return Response.status(Status.INTERNAL_SERVER_ERROR)
                .entity("Something bad happens")
                .build();
        }
        return Response.ok().build();
    }


    private MediaType parseMediaType(
        FormDataBodyPart part)
    {
        String contentType =
            part.getHeaders().getFirst("Content-Type");

        MediaType mediaType = this.mimeToType.get(contentType);
        if (mediaType == null)
        {
            throw new RuntimeException("Unrecognized mimeType \""
                + contentType + "\"");
        }

        return mediaType;
    }


    //
    // CONSTANTS
    //

    // A logger to use for debugging
    private static final Logger LOG =
        LoggerFactory.getLogger(MediaResourceImpl.class);
}
