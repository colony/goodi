
package gd.impl.post;


import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;
import gd.api.v1.LatLng;
import gd.api.v1.post.Geo;
import gd.api.v1.post.PostSort;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.repository.RepositoryConfig;
import gd.impl.util.AbstractChainableFilter;
import gd.impl.util.Filter;
import gd.impl.util.GeoLocation;
import gd.impl.util.Page;
import gd.impl.util.Visitor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;


public class ResidentPostRepository
    implements
        PostRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ResidentPostRepository.class);

    /**
     * Indexed by ID. Deleted posts will be null.
     */
    protected List<Post> posts;


    public ResidentPostRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public synchronized Post add(
        Post p)
    {
        // Assign a new ID to the post
        int id = this.posts.size();
        p.setId(id);
        p.setCreateTime(new Date());
        p.setUpdateTime(new Date());
        p.setCount(new PostCount());

        // Append to the end of list
        this.posts.add(p);

        return new Post(p);
    }
    
    
    @Override
    public synchronized Post addWithoutGeo(Post p)
    {
        return add(p);
    }


    @Override
    public Flag addFlag(
        Flag fp)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public synchronized boolean delete(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        Post post = this.posts.get(intId);
        if (post == null)
            return false;

        // Delete the entry by setting to null so as to avoid array shifting
        this.posts.set(intId, null);

        return true;
    }


    @Override
    public synchronized boolean exists(
        Long id)
    {
        if (!checkBoundary(id))
            return false;

        int intId = id.intValue();
        return this.posts.get(intId) != null;
    }


    @Override
    public synchronized void exportData(
        String fname)

        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.exists(path))
        {
            LOG.warn("Export file \"{}\" already exists", fname);
        }

        // Obtain num of NON-NULL objects
        int n = 0;
        for (Post post : this.posts)
        {
            if (post != null)
                ++n;
        }

        LOG.info("Exporting {} users to file {}", n, fname);
        try (
             OutputStream os =
                 Files.newOutputStream(path, WRITE);
             BufferedOutputStream bos =
                 new BufferedOutputStream(os);)
        {
            PostExporter.write(bos, this.posts.iterator());
        }
    }


    @Override
    public synchronized List<Post> find(
        Page page,
        Filter<Post> filter)
    {
        final String key =
            StringUtils.defaultIfEmpty(
                page.key,
                PostSort.ID.abbreviation());
        PostSort sort = PostSort.fromAbbreviation(key);
        switch (sort)
        {
        case ID:
            Long boundary =
                Strings.isNullOrEmpty(page.boundary) ? null : Long.valueOf(page.boundary);
            boolean inBound = checkBoundary(boundary);
            int size = this.posts.size();

            // 1. determine startId
            int startId;
            boolean decreasing; // order of traversal
            if (page.isDescending())
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
            }
            else
            {
                if (page.isAfter())
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() + 1 : boundary.intValue()) : 1;
                    decreasing = false;
                }
                else
                {
                    startId =
                        inBound ?
                            (page.exclusive ? boundary.intValue() - 1 : boundary.intValue()) : size - 1;
                    decreasing = true;
                }
            }

            // 2. do traversal
            List<Post> result = new ArrayList<>();
            int num = 0;
            for (int id = startId; (decreasing ? id > 0 : id < size);)
            {
                Post p = this.posts.get(id);
                id = decreasing ? id - 1 : id + 1;

                if (p == null)
                    continue;

                if (filter != null && !filter.accept(p))
                    continue;

                // Copy over, since this is resident (in-memory) data structure
                Post ap = new Post(p);
                ap.setSortKey(key);
                ap.setSortVal(String.valueOf(ap.getId()));
                result.add(ap);
                if (++num >= page.size)
                    break;
            }

            // 3. in BEFORE, we traverse in a reverse direction of page.order
            if (!page.isAfter())
                Collections.reverse(result);
            return result;
        default:
            throw new UnsupportedOperationException(
                "Unsupported sort key " + key);
        }
    }



    @Override
    public synchronized List<Post> findByLatLng(
        LatLng latlng,
        Page page,
        Filter<Post> filter)
    {
        String key =
            StringUtils.defaultIfEmpty(
                page.key,
                PostSort.NEARBY_RECENT.abbreviation());
        PostSort sort = PostSort.fromAbbreviation(key);
        switch (sort)
        {
        case NEARBY_POPULAR:
        case NEARBY_RECENT:
            // In resident, we treat both nearby algorithms same, and sorted by ID
            Filter<Post> geoFilter = new LatLngFilter(latlng);
            if (filter != null)
                filter.chainFilter(geoFilter);
            else
                filter = geoFilter;
            return this.find(page, filter);

        default:
            throw new UnsupportedOperationException(
                "Unsupported sort key " + key);
        }
    }


    @Override
    public synchronized List<Post> findByOwner(
        long ownerId,
        Page page,
        Filter<Post> filter)
    {
        // By default, get posts by owner is sorted by ID
        final String key =
            StringUtils.defaultIfEmpty(
                page.key,
                PostSort.ID.abbreviation());
        PostSort sort = PostSort.fromAbbreviation(key);
        switch (sort)
        {
        case ID:
            Filter<Post> ownerFilter = new OwnerFilter(ownerId);
            if (filter != null)
                filter.chainFilter(ownerFilter);
            else
                filter = ownerFilter;
            return this.find(page, filter);

        default:
            throw new UnsupportedOperationException(
                "Unsupported sort key " + key);
        }
    }


    @Override
    public synchronized Post get(
        Long id)
    {
        if (!checkBoundary(id))
            return null;

        int intId = id.intValue();
        return this.posts.get(intId);
    }


    @Override
    public PostCount getCount(
        Long id)
    {
        if (!checkBoundary(id))
            return null;

        int intId = id.intValue();
        Post post = this.posts.get(intId);
        if (post != null) {
            return post.getCount();
        }
        return new PostCount();
    }


    @Override
    public synchronized void importData(
        String fname)

        throws IOException
    {
        Path path = Paths.get(fname);
        if (Files.notExists(path))
        {
            throw new IllegalArgumentException(String.format(
                "Import file %s doesn't exist",
                fname));
        }

        LOG.info("Importing posts from file {}", fname);
        try (
             InputStream is = Files.newInputStream(path, READ);
             BufferedInputStream bis = new BufferedInputStream(is);)
        {
            Iterator<Post> it = PostExporter.read(bis);

            // Wipe out current data
            this.posts.clear();
            this.posts.add(null); // ID 0 is always null

            int i = 1;
            while (it.hasNext())
            {
                Post p = it.next();

                // The in-memory list adds nulls for deleted/missing posts
                // Here, we need fill up the gap during import
                for (int j = i; j < p.getId(); ++j)
                {
                    this.posts.add(null);
                }
                this.posts.add(p);
            }
        }
    }


    @Override
    public synchronized void incrCount(
        Long postId,
        PostCount.Field field,
        long incr)
    {
        if (!checkBoundary(postId) || !exists(postId))
            return;
        int intId = postId.intValue();
        Post post = this.posts.get(intId);
        if (post != null)
        {
            PostCount pc = post.getCount();
            if (pc == null)
                pc = new PostCount();
            pc.incr(field, incr);
        }
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        this.posts = new ArrayList<>();
        this.posts.add(null);           // post ID 0 is not used
    }


    @Override
    public synchronized Post update(
        Post newPost)
    {
        if (!checkBoundary(newPost.getId()))
            return null;

        int intId = (int) newPost.getId();
        Post oldPost = this.posts.get(intId);
        if (oldPost == null)
        {
            return null;
        }

        //TODO: update conflict handle??

        Post upPost = new Post(newPost);
        upPost.setUpdateTime(new Date());
        this.posts.set(intId, upPost);

        // Return a clone, so that the stored one won't be modifiable
        return new Post(upPost);
    }


    @Override
    public void visitAll(
        Visitor<Post> visitor)
    {
        // This is not synchronized, use with cautions

        if (!visitor.before())
            return;

        // We don't need to worry about outOfBoundary because
        // delete will only nullify items instead of remove
        // Also, we don't use iterator to avoid ConcurrentModificationException
        int num = this.posts.size();
        for (int i = 1; i < num; ++i)
        {
            // Maybe stale in concurrent system
            Post post = this.posts.get(i);

            // Ignore null
            if (post == null)
            {
                continue;
            }

            if (!visitor.visit(post))
            {
                // Visitor aborts
                break;
            }
        }

        // Clean up
        visitor.after();
    }


    //
    // INTERNAL METHODS
    //

    protected boolean checkBoundary(
        Long id)
    {
        // Internally, we store data by using an array with integer
        // position as index. The id can't go beyond boundary
        return (id != null &&
            id > 0 && id < this.posts.size());
    }



    //
    // INTERNAL CLASSES
    //

    private class LatLngFilter
        extends
            AbstractChainableFilter<Post>
        implements
            Filter<Post>
    {
        GeoLocation[] boundingCoords;


        public LatLngFilter(
            LatLng latlng)
        {
            int miles =
                Configs.getInt(SystemSetting.GEO_NEARBY_MILES, 2); //2 miles by default
            this.boundingCoords =
                GeoLocation.fromDegrees(latlng.getLat(), latlng.getLon())
                    .boundingCoordinates(miles);
        }


        @Override
        public boolean accept(
            Post p)
        {
            Geo g = p.getGeo();
            if (g == null)
                return false;

            if (g.getLat() < this.boundingCoords[0].getLatitudeInDegrees()
                ||
                g.getLat() > this.boundingCoords[1].getLatitudeInDegrees())
                return false;

            boolean meridian180WithinDistance =
                this.boundingCoords[0].getLongitudeInRadians() >
                this.boundingCoords[1].getLongitudeInRadians();
            if (meridian180WithinDistance)
            {
                if (g.getLon() < this.boundingCoords[0].getLongitudeInDegrees()
                    &&
                    g.getLon() > this.boundingCoords[1].getLongitudeInDegrees())
                    return false;
            }
            else
            {
                if (g.getLon() < this.boundingCoords[0].getLongitudeInDegrees()
                    ||
                    g.getLon() > this.boundingCoords[1].getLongitudeInDegrees())
                    return false;
            }

            return true;
        }
    }



    private class OwnerFilter
        extends
            AbstractChainableFilter<Post>
        implements
            Filter<Post>
    {
        long ownerId;


        public OwnerFilter(
            long ownerId)
        {
            this.ownerId = ownerId;
        }


        @Override
        public boolean accept(
            Post p)
        {
            return p.getOwnerId() == this.ownerId;
        }
    }


    @Override
    public void updateGeo(
        long postId,
        Geo geo)
    {

    }


    @Override
    public void updateScore(
        long postId,
        double score)
    {

    }


    @Override
    public List<Flag> findFlags(
        Page page,
        Filter<Flag> filter)
    {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public List<Flag> findFlagsByPost(
        long postId)
    {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public Post put(
        Post t)
    {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public List<Post> findByGroup(
        Long groupId,
        Page page,
        Filter<Post> filter)
    {
        return null;
    }
}
