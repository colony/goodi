
package gd.impl.post;


import gd.api.v1.LatLng;
import gd.api.v1.PaginationRep;
import gd.api.v1.post.FeedResource;
import gd.api.v1.post.Geo;
import gd.api.v1.post.PostRep;
import gd.api.v1.post.PostRepList;
import gd.api.v1.post.PostSort;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.config.Configs;
import gd.impl.config.DetailedConfigs;
import gd.impl.config.SystemSetting;
import gd.impl.group.Group;
import gd.impl.group.Groups;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SLRelation;
import gd.impl.relation.RL.SSRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.Filter;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.impl.util.Visitor;
import gd.support.jersey.LongParam;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;



@Path("/feed")
@Singleton
public class FeedResourceImpl
    implements
        FeedResource
{
    protected SSRelation anythingRel;

    protected SLRelation hashtagRel;

    protected LSRelation hasPostMetaRel;

    protected LLRelation hasThreadChildrenRel;

    protected LSRelation hasUserMetaRel;

    protected PostRepository postRepo;


    public FeedResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.postRepo =
            repoMgr.get(RepositoryType.POST, PostRepository.class);

        RelationManager relMgr = RelationManager.instance();
        this.anythingRel = relMgr.get(RelationType.ANYTHING, SSRelation.class);
        this.hashtagRel = relMgr.get(RelationType.HASHTAG, SLRelation.class);
        this.hasPostMetaRel = relMgr.get(RelationType.HAS_POST_METADATA, LSRelation.class);
        this.hasThreadChildrenRel = relMgr.get(RelationType.HAS_THREAD_CHILDREN, LLRelation.class);
        this.hasUserMetaRel = relMgr.get(RelationType.HAS_USER_METADATA, LSRelation.class);
    }


    @Override
    @GET
    @Path("/announcement")
    @Produces("application/json")
    public Response getAnnouncementFeed(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo)
    {
        long callerId = Rests.beginOperation(headers, "fd:getAnnounce");

        long maxId = 0L;
        List<Post> postList = Posts.getAnnouncementPosts();
        PostRepList repList = new PostRepList();
        for (Post post : postList)
        {
            PostRep rep = new PostRep();
            Reps.populatePost(rep, post, callerId, false);
            rep.isAnnouncement = true;
            repList.add(rep);
            maxId = Math.max(maxId, post.getId());
        }

        // We need mark the last read announcement ID
        SEntry node = this.hasUserMetaRel.getEdge(callerId, Relations.HUM_ANNOUNCEMENT_INFO);
        if (node == null) {
            // It's fine we fail to addEdge in multi-thread environment
            this.hasUserMetaRel.addEdge(callerId, new SEntry(Relations.HUM_ANNOUNCEMENT_INFO).with(maxId));
        }
        else {
            long lastReadId = node.l1;
            if (lastReadId != maxId) {
                this.hasUserMetaRel.modifyEdgeByData(
                    callerId,
                    Relations.HUM_ANNOUNCEMENT_INFO,
                    null,
                    null,
                    null,
                    maxId,
                    true,
                    null,
                    true);
            }
        }
        return Response.ok(repList).build();
    }


    @Override
    @GET
    @Path("/featured")
    @Produces("application/json")
    public Response getFeaturedFeed(
        @Context HttpHeaders headers,
        @Context UriInfo uriInf)
    {
        final long callerId = Rests.beginOperation(headers, "fd:getFeatured");
        final int limit = Configs.getInt(SystemSetting.FEATURED_LIST_COUNT, 5);
        final PostRepList repList = new PostRepList();
        this.anythingRel.visitEdges(
            Relations.ANYTHING_FEATURED_FEED,
            Relations.SK_DID,
            null,
            true,
            true,
            new Visitor<SEntry>() {
                @Override
                public boolean visit(
                    SEntry item)
                {
                    if (item == null)
                        return true;
                    
                    long id = Long.valueOf(item.getId());
                    if (id < 0L)
                        return true;
                    
                    Post post = Posts.get(id);
                    if (post == null)
                        return true;
                    
                    PostRep rep = new PostRep();
                    Reps.populatePost(rep, post, callerId, false);
                    if (item.l1 == 1 && post.getThreadId() != null) {
                        rep.featuredTitle = String.format(item.s1, hasThreadChildrenRel.getNumEdges(post.getThreadId()));
                    } else {
                        rep.featuredTitle = item.s1;
                    }
                    repList.add(rep);
                    
                    if (repList.getSize() > limit)
                        return false;
                    
                    return true;
                }
            });
        return Response.ok(repList).build();
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getFeed(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("geo") Geo geo,
        @BeanParam PageParam pageParam)
    {
        long callerId = Rests.beginOperation(headers, "fd:getFeed");
        Page p = pageParam.get();
        String sortKey =
            StringUtils.defaultIfBlank(p.key, PostSort.ID.abbreviation());
        PostSort sort =
            PostSort.fromAbbreviation(sortKey.toLowerCase());

        Filter<Post> filterHead = new FlagPostFilter(callerId);
        Filter<Post> filterTail = filterHead;
        filterTail = filterTail.chainFilter(new SpecialPostStatusFilter(PostStatus.Status.ANNOUNCEMENT));

        List<Long> filterGroupIds = null;
        List<Post> postList = null;
        switch (sort)
        {
        case NEARBY_POPULAR:
        case NEARBY_RECENT:
            LatLng latlng = null;
            if (geo != null) {
                // If we specify a geo in query, override the in header
                latlng = geo.toLatLng();
            }
            else {
                latlng = Rests.retrieveLatLng(headers);
            }
            filterGroupIds = DetailedConfigs.getNearbyFeedFilterGroupIds();
            if (!filterGroupIds.isEmpty()) {
                filterTail = filterTail.chainFilter(new PostGroupFilter(filterGroupIds));
            }
            postList = this.postRepo.findByLatLng(latlng, p, filterHead);
            break;

        case GLOBAL_POPULAR:
        case GLOBAL_RECENT:
            filterTail = filterTail.chainFilter(new SpecialPostStatusFilter(PostStatus.Status.GEO_RESTRICTED));
            filterGroupIds = DetailedConfigs.getGlobalFeedFilterGroupIds();
            if (!filterGroupIds.isEmpty()) {
                filterTail = filterTail.chainFilter(new PostGroupFilter(filterGroupIds));
            }
            postList = this.postRepo.find(p, filterHead);
            break;

        case ID:
            postList = this.postRepo.find(p, null);
            break;

        default:
            return Response.status(Status.BAD_REQUEST).entity("Unrecognized sortkey " + sort).build();
        }

        PostRepList repList = new PostRepList();
        for (Post post : postList)
        {
            PostRep rep = new PostRep();
            Reps.populatePost(rep, post, callerId, false);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(postList),
                GDUtils.getLast(postList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }


    @Override
    @GET
    @Path("/group/{id}")
    @Produces("application/json")
    public Response getFeedByGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam groupIdParam,
        @BeanParam PageParam pageParam)
    {
        long callerId = Rests.beginOperation(headers, "fd:getFeedByGP:" + groupIdParam.get());

        long groupId = groupIdParam.get();
        Page p = pageParam.get();

        Group group = Groups.get(groupId);
        GDUtils.check(group != null, Status.NOT_FOUND, "No group with ID=" + groupId);

        Filter<Post> filterHead = new FlagPostFilter(callerId);
        List<Post> postList = this.postRepo.findByGroup(groupId, p, filterHead);

        PostRepList repList = new PostRepList();
        for (Post post : postList) {
            PostRep rep = new PostRep();
            Reps.populatePost(rep, post, callerId, false);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(postList),
                GDUtils.getLast(postList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }


    @Override
    @Path("/hashtag/{tag}")
    @GET
    @Produces("application/json")
    public Response getFeedByHashtag(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("tag") String hashtag,
        @BeanParam PageParam pageParam)
    {
        long callerId = Rests.beginOperation(headers, "fd:getFeedByHT:" + hashtag);
        Page p = pageParam.get();

        if (Strings.isNullOrEmpty(hashtag))
            return Response.status(Status.BAD_REQUEST).entity("Hashtag can't be empty").build();

        String key = StringUtils.defaultIfEmpty(p.key, Relations.SK_DID);
        if (!key.equals(Relations.SK_DID))
            return Response.status(Status.BAD_REQUEST).entity("Sortkey " + key + " is not allowed").build();

        Object boundary = p.boundary;
        if (boundary != null)
            boundary = this.hashtagRel.makeEdgeId(p.boundary);
        List<Post> postList = new ArrayList<>();
        this.hashtagRel.visitEdges(
            hashtag.toLowerCase(), // The source id is in lower case
            key,
            boundary,
            p.isDescending(),
            p.exclusive,
            new PostByRelationLEntryVisitor(
                postList,        // representation
                p.size,         // max size
                !p.isAfter(),   // reverse if BEFORE
                null));         // filter

        PostRepList repList = new PostRepList();
        repList.hashtag = hashtag;
        for (Post post : postList)
        {
            PostRep rep = new PostRep();
            Reps.populatePost(rep, post, callerId, false);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(postList),
                GDUtils.getLast(postList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }
}
