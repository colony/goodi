
package gd.impl.post;


import gd.impl.util.AbstractChainableFilter;
import gd.impl.util.Filter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Filter posts that belongs to the specified group
 */
public class PostGroupFilter
    extends
        AbstractChainableFilter<Post>
    implements
        Filter<Post>
{
    private Set<Long> groupIdSet;


    public PostGroupFilter()
    {
    }


    public PostGroupFilter(
        List<Long> groupIds)
    {
        this.groupIdSet = new HashSet<>(groupIds);
    }


    @Override
    public boolean accept(
        Post p)
    {
        if (p == null)
            return false; // Never accept null

        if (p.getGroupId() == null || p.getGroupId() == 0L)
            return false; // Never accept INVALID group ID

        if (groupIdSet != null && groupIdSet.contains(p.getGroupId()))
            return false;

        if (this.nextFilter != null && !this.nextFilter.accept(p))
            return false; // Chained filter

        return true;
    }

    
    public List<Long> getGroupIds()
    {
        return new ArrayList<>(groupIdSet);
    }
    
    
    public boolean hasGroupIds()
    {
        return groupIdSet != null && !groupIdSet.isEmpty();
    }
}
