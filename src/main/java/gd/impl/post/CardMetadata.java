
package gd.impl.post;


/**
 * Parse the metadata stored in Post's body, the format is
 * VERSION|DECK_ID|[1,0] (0 means response card, 1 means request card)
 */
public class CardMetadata
{
    private String deckId;

    private boolean isRequest;



    public CardMetadata(
        String s)
    {
        String[] tokens = s.split("\\|", 3);
        this.deckId = tokens[1];
        this.isRequest = tokens[2].equals("1");
    }


    public String getDeckId()
    {
        return deckId;
    }


    public boolean isRequest()
    {
        return isRequest;
    }


    public boolean isResponse()
    {
        return !isRequest;
    }




    //
    //
    //
//
//    public static enum CardType
//    {
//        CARDS_AGAINST_HUMANITY("cah"),
//
//        ;
//
//        private final String abbrev;
//
//
//        CardType(
//            String abbrev)
//        {
//            this.abbrev = abbrev;
//        }
//
//
//        public String abbrev()
//        {
//            return abbrev;
//        }
//
//
//        public static CardType fromAbbreviation(
//            String abbrev)
//        {
//            for (CardType ct : CardType.values()) {
//                if (ct.abbrev.equalsIgnoreCase(abbrev))
//                    return ct;
//            }
//
//            throw new IllegalArgumentException("CardType contains no abbrev \"" + abbrev + "\"");
//        }
//    }
}
