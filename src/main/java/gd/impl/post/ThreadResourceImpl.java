
package gd.impl.post;


import gd.api.v1.PaginationRep;
import gd.api.v1.post.PostRep;
import gd.api.v1.post.PostRepList;
import gd.api.v1.post.ThreadRep;
import gd.api.v1.post.ThreadResource;
import gd.api.v1.post.ThreadType;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.activity.Activities;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.util.Filter;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.impl.util.UpdateConflictException;
import gd.support.jersey.LongParam;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/threads")
@Singleton
public class ThreadResourceImpl
    implements
        ThreadResource
{
    private static final Logger LOG =
        LoggerFactory.getLogger(ThreadResourceImpl.class);

    protected LLRelation hasThreadChildrenRel;


    public ThreadResourceImpl()
    {
        RelationManager relMgr = RelationManager.instance();
        this.hasThreadChildrenRel = relMgr.get(RelationType.HAS_THREAD_CHILDREN, LLRelation.class);
    }


    @Override
    @Path("/{id}/root")
    @GET
    @Produces("application/json")
    public Response getRootPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam)
    {
        long callerId = Rests.beginOperation(headers, "t:getRoot:" + idParam.get());

        long threadId = idParam.get();
        LEntry entry = this.hasThreadChildrenRel.getSource(threadId);
        GDUtils.check(entry != null, Status.NOT_FOUND, "No thread with id " + threadId);

        Post post = Posts.get(entry.l1);
        GDUtils.check(
            post != null,
            Status.NOT_FOUND,
            "No post with ID " + entry.l1);

        PostRep rep = new PostRep();
        Reps.populatePost(rep, post, callerId, false);

        return Response.ok(rep).build();
    }


    @Override
    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getThread(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam,
        @BeanParam PageParam pageParam)
    {
        long callerId = Rests.beginOperation(headers, "t:get:" + idParam.get());
        Page p = pageParam.get();

        long threadId = idParam.get();
        LEntry entry = this.hasThreadChildrenRel.getSource(threadId);
        GDUtils.check(entry != null, Status.NOT_FOUND, "Thread not found with ID " + threadId);

        ThreadType type = ThreadType.valueOf(entry.s1);
        String metadata = entry.s2;

        List<Post> postList = new ArrayList<>();
        String key = StringUtils.defaultIfEmpty(p.key, Relations.SK_DID);
        Object boundary = p.boundary;
        if (boundary != null)
            boundary = this.hasThreadChildrenRel.makeEdgeId(p.boundary);

        switch (type)
        {
        case CARD_GAME:
            {
                long winPostId = Long.parseLong(StringUtils.substringAfterLast(metadata, "|"));
                Filter<Post> filter = winPostId > 0L ? new PostIdFilter(winPostId) : null;
                this.hasThreadChildrenRel.visitEdges(
                    threadId,
                    key,
                    boundary,
                    p.isDescending(),
                    p.exclusive,
                    new PostByRelationLEntryVisitor(
                        postList,      // data
                        p.size,        // max size
                        !p.isAfter(),  // reverse if BEFORE
                        filter));      // filter TODO: flagPostFilter?
                if (winPostId > 0L && p.isDefault()) {
                    Post winPost = ((PostIdFilter) filter).getFilteredPost();
                    if (winPost == null)
                        winPost = Posts.get(winPostId);
                    postList.add(0, winPost);
                    if (postList.size() > p.size)
                        postList.remove(postList.size() - 1);
                }
                break;
            }

        default:
            {
                this.hasThreadChildrenRel.visitEdges(
                    threadId,
                    key,
                    boundary,
                    p.isDescending(),
                    p.exclusive,
                    new PostByRelationLEntryVisitor(
                        postList,      // data
                        p.size,        // max size
                        !p.isAfter(),  // reverse if BEFORE
                        null));        // filter TODO: flagPostFilter?
            }
        }

        PostRepList repList = new PostRepList();
        for (Post post : postList) {
            PostRep rep = new PostRep();
            Reps.populatePost(rep, post, callerId, false);
            repList.add(rep);
        }

        // The first (default) page, we return thread info
        if (p.isDefault()) {
            ThreadRep thRep = new ThreadRep();
            Reps.populateThread(thRep, threadId, entry);
            repList.thread = thRep;
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(postList),
                GDUtils.getLast(postList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }


    @Override
    @Path("/{id}/action")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response performAction(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("action_key") String actionKey,
        @FormParam("action_val") String actionValue)
    {
        Rests.beginOperation(headers, "t:act:" + idParam.get());
        long threadId = idParam.get();

        LEntry entry = this.hasThreadChildrenRel.getSource(threadId);
        GDUtils.check(entry != null, Status.NOT_FOUND, "No thread with id " + threadId);

        ThreadType type = ThreadType.valueOf(entry.s1);
        switch (type)
        {
        case CARD_GAME:
            // Key - DECLARE_WINNER
            // Val - won post ID
            if (actionKey.equalsIgnoreCase("DECLARE_WINNER")) {
                entry.s2 = StringUtils.substringBeforeLast(entry.s2, "|") + "|" + actionValue;
                try {
                    this.hasThreadChildrenRel.updateSource(entry);

                    Activities.asyncLogDeclareWinner(Long.parseLong(actionValue), threadId);
                }
                catch (UpdateConflictException e) {
                    LOG.error(
                        "Update conflict fail to declare winner of threadId {} as postId {}",
                        threadId,
                        actionValue);
                }
            }
            break;
        }

        return Response.ok().build();
    }
}
