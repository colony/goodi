
package gd.impl.post;


import gd.api.v1.post.ThreadType;
import gd.impl.comment.Comment;
import gd.impl.comment.CommentRepository;
import gd.impl.group.GroupCount;
import gd.impl.group.GroupRepository;
import gd.impl.index.IDX.LDIndex;
import gd.impl.index.IndexType;
import gd.impl.index.Indexes;
import gd.impl.media.AudioMedia;
import gd.impl.media.FileManager;
import gd.impl.media.Media;
import gd.impl.media.PhotoMedia;
import gd.impl.media.VideoMedia;
import gd.impl.post.PostCount.Field;
import gd.impl.post.PostStatus.Status;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SLRelation;
import gd.impl.relation.RL.SSRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.user.User;
import gd.impl.user.UserRepository;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.google.common.collect.ImmutableList;


public final class Posts
{
    private static SSRelation _anythingRel;

    private static CommentRepository _cmtRepo;

    private static FileManager _fileMgr;

    private static GroupRepository _groupRepo;

    private static LLRelation _hasCommenterRel;

    private static SLRelation _hashtagRel;

    private static LSRelation _hasPostMetaRel;

    private static LLRelation _hasThreadChildrenRel;

    private static LLRelation _hasThreadParticipantRel;

    private static SLRelation _includesSpecialPostRel;

    private static LLRelation _isCmtVotedByRel;

    private static LLRelation _isVotedByRel;

    private static PostRepository _postRepo;

    /**
     * A resident cache that backs INCLUDES_SPECIAL_POST TODO:
     * improve the cache impl if the relation grows bigger
     */
    private static EnumMap<PostStatus.Status,Set<Long>> _statusCache;

    private static ReentrantReadWriteLock _statusCacheLock;

    /**
     * Lock for protecting access to thread ID time
     */
    private static Object _threadIdLock;

    /**
     * Current ID time, in msecs, used to generate unique thread ID
     */
    private static long _threadIdTime;

    private static UserRepository _userRepo;

    private static LLRelation _votesRel;

    private static LLRelation _votesCmtRel;



    public static Post createPost(
        Post post,
        boolean hideGeo)
    {
        if (hideGeo)
            return _postRepo.addWithoutGeo(post);
        else
            return _postRepo.add(post);
    }


    public static long createThreadId(
        ThreadType type)
    {
        long nowSecs = System.currentTimeMillis() / 1000;
        synchronized (_threadIdLock)
        {
            if (nowSecs == _threadIdTime) {
                _threadIdTime++;
            }
            else {
                _threadIdTime = nowSecs;
            }
        }
        return _threadIdTime;
    }


    public static void deleteComment(
        final long cmtId)
    {
        // Retrieve the data to be deleted
        Comment cmt = _cmtRepo.get(cmtId);
        if (cmt == null)
            return;

        // Unlink comment from any relations, indexes, 
        // NOTE, it is extremely hard to delete entry from HAS_COMMENTER, now leave it intact
        _isCmtVotedByRel.visitAllEdges(
            cmtId,
            new Visitor<LEntry>() {
                @Override
                public boolean visit(
                    LEntry e)
                {
                    // Delete the comment from liker's relation
                    _votesCmtRel.deleteEdge(e.getId(), cmtId);
                    return true;
                }
            });
        _isCmtVotedByRel.deleteSource(cmtId);

        // Delete it from repo
        _cmtRepo.delete(cmtId);

        // Other related
        _postRepo.incrCount(cmt.getPostId(), Field.COMMENT, -1L);
    }


    public static void deletePost(
        final long postId)
    {
        Post post = _postRepo.get(postId);
        if (post == null)
            return;

        // 0. Prepare some data we need later
        PostCount postCount = _postRepo.getCount(postId);
        long ownerId = post.getOwnerId();

        // 1. Unlink post from any relations
        _hasCommenterRel.deleteSource(postId);
        _hasPostMetaRel.deleteSource(postId);
        if (post.hasHashtags())
        {
            for (String ht : post.getHashtags())
            {
                _hashtagRel.deleteEdge(ht, postId);
            }
        }
        if (post.getStatus() != null)
        {
            PostStatus ps = post.getStatus();
            for (Status status : Status.values())
            {
                if (!ps.isSet(status))
                    continue;

                switch (status)
                {
                case BLOCKED:
                    _includesSpecialPostRel.deleteEdge(status.name(), postId);
                    _anythingRel.deleteEdge(Relations.ANYTHING_BLOCKED_PREVIOUS_GROUPID, String.valueOf(postId));
                    break;

                case HIDDEN:
                case GEO_RESTRICTED:
                case ANNOUNCEMENT:
                    _includesSpecialPostRel.deleteEdge(status.name(), postId);
                    _statusCacheLock.writeLock().lock();
                    try {
                        Set<Long> cache = _statusCache.get(status);
                        if (cache != null)
                            cache.remove(postId);
                    }
                    finally {
                        _statusCacheLock.writeLock().unlock();
                    }
                    break;

                default:
                    break;
                }
            }
        }
        _isVotedByRel.visitAllEdges(postId, new Visitor<LEntry>() {
            @Override
            public boolean visit(
                LEntry item)
            {
                long voterId = item.getId();
                _votesRel.deleteEdge(voterId, postId);
                return true;
            }
        });
        _isVotedByRel.deleteSource(postId);

        if (post.getThreadId() != null) {
            if (post.getClazz() == PostClass.PUBLIC) {
                // Root post is deleted, thread is deleted too
                _hasThreadChildrenRel.deleteSource(post.getThreadId());
                _hasThreadParticipantRel.deleteSource(post.getThreadId());
            }
            else if (post.getClazz() == PostClass.THREAD) {
                _hasThreadChildrenRel.deleteEdge(post.getThreadId(), post.getId());
                // A user might participate multiple posts in a thread, however, for simplicity
                // it is okay to totally remove the owner's participation even if only one post deleted 
                _hasThreadParticipantRel.deleteEdge(post.getThreadId(), ownerId);
            }
        }

        // 2. Unlink it from any indexes
        LDIndex pbrp = Indexes.get(IndexType.POSTS_BY_RECENT_POPULARITY, LDIndex.class);
        pbrp.delete(post.getId());
        LDIndex pigp = Indexes.get(IndexType.POSTS_IN_GROUP_BY_POPULARITY, LDIndex.class);
        pigp.delete(post.getId());

        // 3. Delete it from repo
        if (!_postRepo.delete(postId)) {
            return;
        }

        // 4. Media assets
        if (post.hasMedias())
        {
            for (Media m : post.getMedias())
            {
                switch (m.getType())
                {
                case AUDIO:
                    _fileMgr.deleteQuietly(((AudioMedia) m).getUrl());
                    break;
                case PHOTO:
                    _fileMgr.deleteQuietly(((PhotoMedia) m).getLarge());
                    _fileMgr.deleteQuietly(((PhotoMedia) m).getMedium());
                    _fileMgr.deleteQuietly(((PhotoMedia) m).getThumbnail());
                    break;
                case VIDEO:
                    _fileMgr.deleteQuietly(((VideoMedia) m).getUrl());
                    break;
                default:
                    break; // Do nothing
                }
            }
        }

        // 5. Other
        long groupId = post.getGroupId();
        if (groupId > 1L) {
            _groupRepo.incrCount(groupId, GroupCount.Field.POST, -1L);
            _groupRepo.incrCount(groupId, GroupCount.Field.DOWNVOTE, -1 * postCount.getNumDownvotes());
            _groupRepo.incrCount(groupId, GroupCount.Field.UPVOTE, -1 * postCount.getNumUpvotes());
        }
    }


    public static boolean existThread(
        long threadId)
    {
        return _hasThreadChildrenRel.hasSource(threadId);
    }


    public static Post get(
        long id)
    {
        if (id <= 0L)
            return null;
        return _postRepo.get(id);
    }


    /**
     * Return announcement posts in chronological descending order,
     * recent first
     */
    public static List<Post> getAnnouncementPosts()
    {
        List<Long> ids = getAnnouncementPostIds();
        if (ids.isEmpty())
            return ImmutableList.<Post> of();
        else {
            List<Post> result = new ArrayList<>();
            Collections.sort(ids, Collections.reverseOrder());
            for (Long id : ids) {
                Post post = get(id);
                if (post == null)
                    continue;
                result.add(post);
            }
            return result;
        }
    }


    public static List<Long> getAnnouncementPostIds()
    {
        List<Long> ids = null;
        // Status.ANNOUNCEMENT is cached
        _statusCacheLock.readLock().lock();
        try {
            Set<Long> cache = _statusCache.get(Status.ANNOUNCEMENT);
            if (cache != null) {
                ids = new ArrayList<>(cache);
            }
        }
        finally {
            _statusCacheLock.readLock().unlock();
        }

        if (ids == null)
            return ImmutableList.<Long> of();
        return ids;
    }


    public static Comment getComment(
        long id)
    {
        if (id < 0L)
            return null;
        return _cmtRepo.get(id);
    }


    public static PostCount getCount(
        long id)
    {
        if (id <= 0L)
            return new PostCount();
        return _postRepo.getCount(id);
    }


    public static String getCovertGeoWord(
        long id)
    {
        SEntry entry = _hasPostMetaRel.getEdge(id, Relations.HPM_COVERT_GEO);
        if (entry == null)
            return null;
        return entry.s1;
    }


    public static User getOwner(
        long id)
    {
        Post post = get(id);
        if (post == null)
            return null;
        return _userRepo.get(post.getOwnerId());
    }


    public static boolean hasStatus(
        long postId,
        PostStatus.Status status)
    {
        // Cache is ready for certain status
        if (status == Status.HIDDEN ||
            status == Status.GEO_RESTRICTED ||
            status == Status.ANNOUNCEMENT)
        {
            _statusCacheLock.readLock().lock();
            try {
                Set<Long> cache = _statusCache.get(status);
                if (cache != null)
                    return cache.contains(postId);
                else
                    return false;
            }
            finally
            {
                _statusCacheLock.readLock().unlock();
            }
        }
        // Otherwise, go to persistent source
        return _includesSpecialPostRel.hasEdge(status.name(), postId);
    }


    public static void init()
    {
        _fileMgr = FileManager.instance();

        RepositoryManager repoMgr = RepositoryManager.instance();
        _cmtRepo =
            repoMgr.get(RepositoryType.COMMENT, CommentRepository.class);
        _groupRepo =
            repoMgr.get(RepositoryType.GROUP, GroupRepository.class);
        _postRepo =
            repoMgr.get(RepositoryType.POST, PostRepository.class);
        _userRepo =
            repoMgr.get(RepositoryType.USER, UserRepository.class);

        RelationManager relMgr = RelationManager.instance();
        _anythingRel =
            relMgr.get(RelationType.ANYTHING, SSRelation.class);
        _hasCommenterRel =
            relMgr.get(RelationType.HAS_COMMENTER, LLRelation.class);
        _hashtagRel =
            relMgr.get(RelationType.HASHTAG, SLRelation.class);
        _hasPostMetaRel =
            relMgr.get(RelationType.HAS_POST_METADATA, LSRelation.class);
        _hasThreadChildrenRel =
            relMgr.get(RelationType.HAS_THREAD_CHILDREN, LLRelation.class);
        _hasThreadParticipantRel =
            relMgr.get(RelationType.HAS_THREAD_PARTICIPANT, LLRelation.class);
        _includesSpecialPostRel =
            relMgr.get(RelationType.INCLUDES_SPECIAL_POST, SLRelation.class);
        _isCmtVotedByRel =
            relMgr.get(RelationType.IS_COMMENT_VOTED_BY, LLRelation.class);
        _isVotedByRel =
            relMgr.get(RelationType.IS_VOTED_BY, LLRelation.class);
        _votesRel =
            relMgr.get(RelationType.VOTES, LLRelation.class);
        _votesCmtRel =
            relMgr.get(RelationType.VOTES_COMMENT, LLRelation.class);

        _statusCacheLock = new ReentrantReadWriteLock(true);
        _statusCache = new EnumMap<>(PostStatus.Status.class);
        for (final PostStatus.Status status : PostStatus.Status.values())
        {
            // We only cache certain status
            if (status == Status.HIDDEN ||
                status == Status.GEO_RESTRICTED ||
                status == Status.ANNOUNCEMENT)
            {
                _statusCache.put(status, new HashSet<Long>());
                _includesSpecialPostRel.visitAllEdges(
                    status.name(),
                    new Visitor<LEntry>() {
                        @Override
                        public boolean visit(
                            LEntry item)
                        {
                            _statusCache.get(status).add(item.getId());
                            return true;
                        }
                    });
            }
        }

        _threadIdLock = new Object();
        _threadIdTime = System.currentTimeMillis();
    }


    /**
     * Set/unset the specified status on post, If post doesn't
     * exist, do nothing
     * 
     * @param unset
     *            If true, this is UNSET the specified status
     */
    public static void setStatus(
        long postId,
        PostStatus.Status status,
        boolean unset)

        throws UpdateConflictException
    {
        Post post = get(postId);
        if (post == null)
            return; // Do nothing

        PostStatus ps = post.getStatus();
        if (ps == null)
            ps = new PostStatus();

        if (unset ^ ps.isSet(status))
            return;

        ps.set(status, !unset);
        post.setStatus(ps);
        _postRepo.update(post);

        // Only certain status, we will track in the additional relation
        switch (status)
        {
        case BLOCKED:
        case HIDDEN:
        case GEO_RESTRICTED:
        case ANNOUNCEMENT:
            if (unset)
                _includesSpecialPostRel.deleteEdge(status.name(), postId);
            else
                _includesSpecialPostRel.addEdge(status.name(), postId);

            // Only certain status, we will cache it
            if (status == Status.HIDDEN ||
                status == Status.GEO_RESTRICTED ||
                status == Status.ANNOUNCEMENT)
            {
                _statusCacheLock.writeLock().lock();
                try {
                    Set<Long> cache = _statusCache.get(status);
                    if (cache != null)
                    {
                        if (unset)
                            cache.remove(postId);
                        else
                            cache.add(postId);
                    }
                }
                finally
                {
                    _statusCacheLock.writeLock().unlock();
                }
            }
            break;

        default:
            break;
        }
    }
}
