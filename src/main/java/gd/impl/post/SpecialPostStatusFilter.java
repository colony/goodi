
package gd.impl.post;


import org.apache.commons.lang3.ArrayUtils;

import gd.impl.util.AbstractChainableFilter;
import gd.impl.util.Filter;


/**
 * Filter posts that have the special PostStatus
 */
public class SpecialPostStatusFilter
    extends
        AbstractChainableFilter<Post>
    implements
        Filter<Post>
{
    /**
     * A list of statuses that
     */
    private PostStatus.Status[] statuses;


    public SpecialPostStatusFilter(
        PostStatus.Status... statuses)
    {
        this.statuses = statuses;
    }


    @Override
    public boolean accept(
        Post p)
    {
        if (p == null)
            return false; // Never accept null

        if (p.getStatus() != null && p.getStatus().isSetAny(this.statuses))
            return false;

        if (this.nextFilter != null && !this.nextFilter.accept(p))
            return false; // Chained filter

        return true;
    }


    public PostStatus.Status[] getStatuses()
    {
        return statuses;
    }


    /**
     * Check if the filter only on rare PostStaus such as HIDDEN,
     * ANNOUNCEMENT. Also, if no status is set, it returns true
     */
    public boolean isFilterRareStatus()
    {
        if (this.statuses == null || this.statuses.length == 0)
            return true;
        return ArrayUtils.contains(this.statuses, PostStatus.Status.HIDDEN) ||
            ArrayUtils.contains(this.statuses, PostStatus.Status.ANNOUNCEMENT);
    }
}
