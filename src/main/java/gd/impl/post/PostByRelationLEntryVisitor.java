
package gd.impl.post;
import gd.impl.relation.RL.LEntry;
import gd.impl.util.Filter;

import java.util.List;


public class PostByRelationLEntryVisitor
    extends
        AbstractPostVisitor<LEntry>
{

    public PostByRelationLEntryVisitor(
        List<Post> postList,
        int max,
        boolean reverse,
        Filter<Post> filter)
    {
        super(postList, max, reverse, filter);
    }


    @Override
    public long makePostId(
        LEntry edge)
    {
        return edge.getId();
    }
    
    
    @Override
    public void preprocess(LEntry item, Post post)
    {
        post.setSortKey(item.getSortKey());
        post.setSortVal(item.getSortValue());
    }
}
