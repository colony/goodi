
package gd.impl.post;


import gd.impl.algorithm.NearbyRecentPopularAlgorithm1;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.queue.Event;
import gd.impl.queue.EventConsumer;
import gd.impl.queue.EventType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The score updater for post, the score is used to rank in local
 * hot feed
 */
public class PostScoreUpdater
    implements
        EventConsumer
{
    private static final Logger LOG = LoggerFactory.getLogger(PostScoreUpdater.class);
    
    private NearbyRecentPopularAlgorithm1 alg;

    /**
     * Usually, event like vote or comment on a post happens in a
     * burst. This cache serves as a temporary buffer so that many
     * events on one post would only be considered (computed) once
     * for performance sake
     * <p>
     * For now, we use size-based cache, basically, we will flush
     * out the data in cache and do computation after the cache
     * reaches to certain size, this way would cause data loss if
     * server bounces. In future, we should introduce time-based
     * periodic cache flush
     */
    private Set<Long> cache;

    private int maxNumCachedEvents;
    
    private int numCachedEvents;

    private PostRepository postRepo;


    public PostScoreUpdater()
    {
        this.alg = new NearbyRecentPopularAlgorithm1();
        this.postRepo = RepositoryManager.instance().get(RepositoryType.POST, PostRepository.class);
        this.cache = new HashSet<>();
        this.maxNumCachedEvents = Configs.getInt(SystemSetting.POST_SCORE_UPDATER_MAX_CACHE, 20);
        this.numCachedEvents = 0;
    }


    @Override
    public boolean accept(
        EventType type)
    {
        return type == EventType.CREATE_COMMENT
            || type == EventType.FLAG_POST
            || type == EventType.SHARE_POST
            || type == EventType.VOTE_POST
            || type == EventType.ADMIN_UPDATE_POST;
    }


    @Override
    public void consume(
        Event event)
    {
        // Don't do anything for now, not tested yet, but i need deploy immediately
        long postId = 0L;
        if (event.type == EventType.ADMIN_UPDATE_POST)
        {
            // Check if count has been updated
            if (!event.bool1)
                return;

            postId = event.lng1;
        }
        else
        {
            postId = event.lng2;
        }
        
        this.cache.add(postId);
        ++this.numCachedEvents;
        
        if (this.numCachedEvents >= this.maxNumCachedEvents) {
            for (Long updatePostId : this.cache) {
                double score = alg.computeScore(updatePostId);
                if (score > 0.0)
                    this.postRepo.updateScore(updatePostId, score);
            }
            LOG.debug("Computed score for {} posts", this.cache.size());
            
            this.numCachedEvents = 0;
            this.cache.clear();
        }
    }
}
