
package gd.impl.post;


public class PostCount
{
    //
    // N.B. All counts are zero by default
    //

    private long numComments;

    private long numDownvotes;

    private long numFlags;

    private long numShares;

    private long numStars;

    private long numUpvotes;

    private long numViews;



    public static enum Field
    {
        COMMENT,
        DOWNVOTE,
        FLAG,
        SHARE,
        UPVOTE,
        VIEW
    }


    public PostCount()
    {
    }


    public long get(
        Field f)
    {
        switch (f)
        {
        case COMMENT:
            return getNumComments();
        case DOWNVOTE:
            return getNumDownvotes();
        case FLAG:
            return getNumFlags();
        case SHARE:
            return getNumShares();
        case UPVOTE:
            return getNumUpvotes();
        case VIEW:
            return getNumViews();
        }
        return 0L;
    }


    public void set(
        Field f,
        long v)
    {
        switch (f)
        {
        case COMMENT:
            setNumComments(v);
            break;
        case DOWNVOTE:
            setNumDownvotes(v);
            break;
        case FLAG:
            setNumFlags(v);
            break;
        case SHARE:
            setNumShares(v);
            break;
        case UPVOTE:
            setNumUpvotes(v);
            break;
        case VIEW:
            setNumViews(v);
            break;
        }
    }


    public long incr(
        Field f,
        long incr)
    {
        switch (f)
        {
        case COMMENT:
            numComments += incr;
            return numComments;
        case DOWNVOTE:
            numDownvotes += incr;
            return numDownvotes;
        case FLAG:
            numFlags += incr;
            return numFlags;
        case SHARE:
            numShares += incr;
            return numShares;
        case UPVOTE:
            numUpvotes += incr;
            return numUpvotes;
        case VIEW:
            numViews += incr;
            return numViews;
        }
        throw new IllegalArgumentException(
            "Unrecognized PostCount Field " + f);
    }


    //
    // GETTERS & SETTERS
    //

    public long getNumComments()
    {
        return numComments;
    }


    public void setNumComments(
        long numComments)
    {
        this.numComments = numComments;
    }


    public long getNumDownvotes()
    {
        return numDownvotes;
    }


    public void setNumDownvotes(
        long numDownvotes)
    {
        this.numDownvotes = numDownvotes;
    }


    public long getNumFlags()
    {
        return numFlags;
    }


    public void setNumFlags(
        long numFlags)
    {
        this.numFlags = numFlags;
    }


    public long getNumShares()
    {
        return numShares;
    }


    public void setNumShares(
        long numShares)
    {
        this.numShares = numShares;
    }


    public long getNumStars()
    {
        return numStars;
    }


    public void setNumStars(
        long numStars)
    {
        this.numStars = numStars;
    }


    public long getNumUpvotes()
    {
        return numUpvotes;
    }


    public void setNumUpvotes(
        long numUpvotes)
    {
        this.numUpvotes = numUpvotes;
    }


    public long getNumViews()
    {
        return numViews;
    }


    public void setNumViews(
        long numViews)
    {
        this.numViews = numViews;
    }
}
