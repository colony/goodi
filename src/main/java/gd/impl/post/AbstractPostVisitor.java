
package gd.impl.post;


import gd.impl.util.Filter;
import gd.impl.util.Visitor;

import java.util.Collections;
import java.util.List;


public abstract class AbstractPostVisitor<T>
    extends
        Visitor<T>
{
    private Filter<Post> filter;

    private List<Post> list;

    private int max;

    private boolean reverse;


    public AbstractPostVisitor(
        List<Post> postList,
        int max,
        boolean reverse,
        Filter<Post> filter)
    {
        this.filter = filter;
        this.list = postList;
        this.max = max;
        this.reverse = reverse;
    }


    //
    // ABSTRACT METHODS
    //


    public abstract long makePostId(
        T item);


    //
    // OVERRIDE METHODS
    //

    @Override
    public void after()
    {
        if (this.reverse)
        {
            Collections.reverse(this.list);
        }
    }


    /**
     * This method is called right before adding into the
     * collection. The child class could override it for additional
     * logic
     */
    public void preprocess(
        T item,
        Post post)
    {
        // Do nothing, child class could override
    }


    @Override
    public boolean visit(
        T item)
    {
        if (item == null)
            return true;

        long id = makePostId(item);

        // Sanity check on ID
        if (id < 0L)
            return true;

        // Retrieve the actual post by ID
        Post post = Posts.get(id);

        // Post no longer exists
        if (post == null)
            return true;

        // Make sure the post meets the filter criteria
        if (this.filter != null &&
            !this.filter.accept(post))
        {
            // Not accepted by filter parameters
            return true;
        }
        
        // Preprocess the item and post before adding into collection
        this.preprocess(item, post);

        // Convert to Post representation
        this.list.add(post);

        // If list if fully populated, bail out
        if (this.list.size() >= this.max)
        {
            return false;
        }

        // Continue to next
        return true;
    }

}
