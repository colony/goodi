
package gd.impl.post;


import gd.impl.index.IDX.LDEntry;
import gd.impl.util.Filter;
import gd.impl.util.Visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PostIndexVisitor
    extends
        Visitor<LDEntry>
{
    protected Filter<LDEntry> indexFilter;
    
    protected Filter<Post> postFilter;
    
    protected int max;
    
    protected int num;
    
    protected List<Post> posts;
    
    
    public PostIndexVisitor(int max)
    {
        this.max = max;
    }
    
    
    public List<Post> getList()
    {
        return posts;
    }
    
    
    @Override
    public boolean before()
    {
        this.posts = new ArrayList<Post>(this.max);
        this.num = 0;
        return true;
    }


    @Override
    public boolean visit(
        LDEntry entry)
    {
        Long postId = entry.getId();
        if (postId == null || postId <= 0L)
            return true;
        
        if (this.indexFilter != null && !indexFilter.accept(entry))
            return true;
        
        Post post = Posts.get(postId);
        if (post == null) {
            // Post is unavailable or no longer exists, move along
            return true;
        }
        
        if (this.postFilter != null && !postFilter.accept(post))
            return true;
        
        post.setSortKey(entry.toSortKey());
        post.setSortVal(entry.toSortValue());
        this.posts.add(post);
        
        ++num;
        if (num >= max)
            return false;
        
        return true;
    }


    public void reverse()
    {
        Collections.reverse(this.posts);
    }
    
    
    public void setIndexFilter(Filter<LDEntry> filter)
    {
        this.indexFilter = filter;
    }
    
    
    public void setPostFilter(Filter<Post> filter)
    {
        this.postFilter = filter;
    }
}
