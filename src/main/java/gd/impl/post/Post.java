
package gd.impl.post;


import gd.api.v1.post.Geo;
import gd.api.v1.post.PostType;
import gd.impl.media.Media;
import gd.impl.util.GDUtils;
import gd.impl.util.Sortable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Post
    implements
        Sortable
{
    private String body;

    private PostClass clazz;
    
    /**
     * A struct containing different type of counts on post
     */
    private PostCount count;

    private Date createTime;

    private Geo geo;
    
    private Long groupId;

    private List<String> hashtags;

    private long id;

    private List<Media> medias;

    private long ownerId;

    /**
     * sortKey and sortVal are not persisted. Both fields are
     * normally null unless the post is part of collection (i.e.
     * feed). The fields indicate how the post is sorted.
     */
    private String sortKey;

    private String sortVal;

    private PostStatus status;

    private Long threadId;
    
    private PostType type;

    private Date updateTime;


    /**
     * Default constructor
     */
    public Post()
    {
    }


    /**
     * Copy constructor
     */
    public Post(
        Post p)
    {
        this.body = p.body;
        this.clazz = p.clazz;
        this.count = p.count;
        this.createTime = p.createTime;
        this.geo = p.geo;
        this.groupId = p.groupId;
        this.id = p.id;
        if (p.hasMedias())
            this.medias = new ArrayList<>(p.medias);
        this.ownerId = p.ownerId;
        this.status = p.status;
        this.sortKey = p.sortKey;
        this.sortVal = p.sortVal;
        this.threadId = p.threadId;
        this.type = p.type;
        this.updateTime = p.updateTime;
        if (p.hashtags != null)
            this.hashtags = new ArrayList<>(p.hashtags);
    }


    public boolean hasHashtags()
    {
        return this.hashtags != null && !this.hashtags.isEmpty();
    }


    public boolean hasMedias()
    {
        return this.medias != null && !this.medias.isEmpty();
    }
    
    
    public boolean hasStatus(PostStatus.Status s)
    {
        if (this.status == null)
            return false;
        return this.status.isSet(s);
    }
    
    
    public boolean isThreadRoot()
    {
        return threadId != null && threadId > 0L && clazz == PostClass.PUBLIC;
    }
    


    //
    // Getters & Setters
    //

    public String getBody()
    {
        return body;
    }


    public void setBody(
        String body)
    {
        this.body = body;
    }
    
    
    public PostClass getClazz()
    {
        return clazz;
    }
    
    
    public void setClazz(PostClass clazz)
    {
        this.clazz = clazz;
    }


    public PostCount getCount()
    {
        return count;
    }


    public void setCount(
        PostCount count)
    {
        this.count = count;
    }


    public Date getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        Date createTime)
    {
        this.createTime = createTime;
    }


    public Geo getGeo()
    {
        return geo;
    }


    public void setGeo(
        Geo g)
    {
        this.geo = g;
    }
    
    
    public Long getGroupId()
    {
        return groupId;
    }
    
    
    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }


    public void addHashtag(
        String hashtag)
    {
        if (this.hashtags == null)
            this.hashtags = new ArrayList<>();
        this.hashtags.add(hashtag);
    }


    public int getNumHashtags()
    {
        return this.hashtags == null ? 0 : this.hashtags.size();
    }


    public List<String> getHashtags()
    {
        return hashtags;
    }


    public void setHashtags(
        List<String> ht)
    {
        this.hashtags = ht;
    }


    public long getId()
    {
        return id;
    }


    public void setId(
        long id)
    {
        this.id = id;
    }


    public void addMedia(
        Media m)
    {
        if (this.medias == null)
            this.medias = new ArrayList<>();
        this.medias.add(m);
    }


    public int getNumMedias()
    {
        return this.medias == null ? 0 : this.medias.size();
    }


    public Media getMedia(
        int index)
    {
        if (index >= this.getNumMedias())
            return null;
        return this.medias.get(index);
    }


    public <T extends Media> T getMedia(
        int index,
        Class<T> clazz)
    {
        if (index >= this.getNumMedias())
            return null;

        return GDUtils.cast(this.medias.get(index));
    }


    public List<Media> getMedias()
    {
        return medias;
    }


    public void setMedias(
        List<Media> m)
    {
        this.medias = m;
    }


    public long getOwnerId()
    {
        return ownerId;
    }


    public void setOwnerId(
        long ownerId)
    {
        this.ownerId = ownerId;
    }


    public String getSortKey()
    {
        return sortKey;
    }


    public void setSortKey(
        String sk)
    {
        this.sortKey = sk;
    }


    public String getSortVal()
    {
        return sortVal;
    }


    public void setSortVal(
        String sv)
    {
        this.sortVal = sv;
    }


    public PostStatus getStatus()
    {
        return status;
    }


    public void setStatus(
        PostStatus status)
    {
        this.status = status;
    }
    
    
    public Long getThreadId()
    {
        return threadId;
    }
    
    
    public void setThreadId(Long threadId)
    {
        this.threadId = threadId;
    }


    public PostType getType()
    {
        return type;
    }


    public void setType(
        PostType type)
    {
        this.type = type;
    }


    public Date getUpdateTime()
    {
        return updateTime;
    }


    public void setUpdateTime(
        Date updateTime)
    {
        this.updateTime = updateTime;
    }
}
