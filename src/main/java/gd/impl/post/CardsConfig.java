
package gd.impl.post;


import gd.impl.config.Configured;
import gd.impl.config.Reloadable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Configured(fname = "card.yml")
public class CardsConfig
    implements
        Reloadable<CardsConfig>
{
    @JsonIgnore
    private Map<String,CardConfig> cardConfigs;


    public CardsConfig()
    {
    }


    @JsonIgnore
    public CardConfig getCardConfig(
        String deckId)
    {
        return this.cardConfigs.get(deckId);
    }


    @JsonIgnore
    public List<String> getAllDeckIds()
    {
        return new ArrayList<String>(this.cardConfigs.keySet());
    }


    @Override
    public void reload(
        CardsConfig t)
    {
        // Hot swap in memory
        this.cardConfigs = t.cardConfigs;
    }


    @SuppressWarnings({ "unused", "rawtypes" })
    private void setCards(
        List<Map> cards)
    {
        this.cardConfigs = new HashMap<>();

        for (Map m : cards) {
            CardConfig cc = new CardConfig();
            cc.deckId = (String) m.get("deckId");
            cc.numRequests = (Integer) m.get("numRequests");
            cc.numResponses = (Integer) m.get("numResponses");
            this.cardConfigs.put(cc.deckId, cc);
        }
    }



    public static class CardConfig
    {
        private String deckId;

        private int numRequests;

        private int numResponses;


        public String getDeckId()
        {
            return deckId;
        }


        public int getNumRequests()
        {
            return numRequests;
        }


        public int getNumResponses()
        {
            return numResponses;
        }
    }
}
