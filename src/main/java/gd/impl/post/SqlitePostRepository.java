
package gd.impl.post;


import gd.api.v1.LatLng;
import gd.api.v1.post.Geo;
import gd.api.v1.post.PostSort;
import gd.api.v1.post.PostType;
import gd.impl.SQLiteManager;
import gd.impl.config.Configs;
import gd.impl.config.SystemSetting;
import gd.impl.index.IDX.LDEntry;
import gd.impl.index.IDX.LDIndex;
import gd.impl.index.IndexType;
import gd.impl.index.Indexes;
import gd.impl.media.Medias;
import gd.impl.post.PostCount.Field;
import gd.impl.repository.RepositoryConfig;
import gd.impl.repository.SqliteRepository;
import gd.impl.util.Filter;
import gd.impl.util.GDUtils;
import gd.impl.util.GeoLocation;
import gd.impl.util.Page;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.impl.util.Visitor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;



/**
 * Schema (post):
 * <ul>
 * <li>body
 * <li>clazz
 * <li>create_time
 * <li>geo
 * <li>group_id
 * <li>hashtags
 * <li>id
 * <li>medias
 * <li>owner_id
 * <li>status
 * <li>thread_id
 * <li>type
 * <li>update_time
 * </ul>
 * 
 * Schema (post_flag):
 * <ul>
 * <li>complaint_message
 * <li>create_time
 * <li>id
 * <li>post_id
 * <li>user_id
 * </ul>
 * 
 * Schema (post_counter):
 * <ul>
 * <li>post_id
 * <li>num_comments
 * <li>num_downvotes
 * <li>num_flags
 * <li>num_shares
 * <li>num_upvotes
 * <li>num_views
 * </ul>
 * 
 * Schema (post_geo) RTree:
 * <ul>
 * <li>id
 * <li>minX
 * <li>maxX
 * <li>minY
 * <li>maxY
 * </ul>
 * 
 * Schema (post_rank):
 * <ul>
 * <li>post_id
 * <li>score
 * </ul>
 */
public class SqlitePostRepository
    implements
        PostRepository,
        SqliteRepository
{
    private static final Logger LOG =
        LoggerFactory.getLogger(SqlitePostRepository.class);

    private static final String SP = " ";

    private int batchSize = 100;

    private String tableName;


    public SqlitePostRepository()
    {
        // Nothing to do here
        // Real initialization happens at init(cfg) method
    }


    @Override
    public Post add(
        Post p)
    {
        return this.addInternal(p, false);
    }


    @Override
    public Post addWithoutGeo(
        Post p)
    {
        return this.addInternal(p, true);
    }


    @Override
    public Flag addFlag(
        Flag fp)
    {
        Flag fp1 = new Flag(fp);
        final String sql =
            "INSERT INTO post_flag (post_id, user_id, complaint_message, create_time) VALUES (?, ?, ?, ?)";
        Object[] param =
        {
                fp.getPostId(),
                fp.getUserId(),
                fp.getComplainMessage(),
                fp.getCreateTime().getTime()
        };

        long newId = SqliteUtils.executeInsertQuery(
            sql,
            param,
            "addFlagPost",
            true);
        fp1.setId(newId);
        return fp1;
    }


    @Override
    public boolean delete(
        Long id)
    {
        String sql =
            "DELETE FROM post WHERE id = ?";
        Object[] param = {
                id
        };
        boolean success =
            SqliteUtils.executeQuery(sql, param, "DeletePost");
        // If the post doesn't exist, don't proceed
        if (success)
        {
            // 1. Delete all flags associated with
            String sql1 = "DELETE FROM post_flag WHERE post_id = ?";
            // 2. Delete counter, geo, rank
            String sql2 =
                "DELETE FROM post_counter WHERE post_id = ?";
            String sql3 =
                "DELETE FROM post_geo WHERE id = ?";
            String sql4 =
                "DELETE FROM post_rank WHERE post_id = ?";
            SqliteUtils.executeTransactionalQueries(
                "DeletePostRelated",
                sql1,
                param,
                sql2,
                param,
                sql3,
                param,
                sql4,
                param);
        }

        return success;
    }


    @Override
    public boolean exists(
        Long id)
    {
        final String sql = "SELECT 1 FROM post WHERE id=?";
        Object[] param = {
                id
        };
        final MutableBoolean mb = new MutableBoolean(false);

        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "isPostExist", new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                        mb.setTrue();
                }

            });
        return mb.booleanValue();
    }


    @Override
    public List<Post> find(
        Page page,
        Filter<Post> filter)
    {
        final String key =
            StringUtils.defaultIfEmpty(
                page.key,
                PostSort.ID.abbreviation());
        PostSort sort = PostSort.fromAbbreviation(key);
        switch (sort)
        {
        case ID:
            return this.findGeneric(FindType.BY_ID, page, filter);

        case GLOBAL_RECENT:
            return this.findGeneric(FindType.BY_GLOBAL_RECENT, page, filter);

        case GLOBAL_POPULAR:
            return this.findGeneric(FindType.BY_INDEX_RECENT_POPULARITY, page, filter);

        default:
            throw new IllegalArgumentException(
                "Unsupported sort key " + key);
        }
    }


    @Override
    public List<Post> findByGroup(
        Long groupId,
        Page page,
        Filter<Post> filter)
    {
        final String key =
            StringUtils.defaultIfEmpty(page.key, PostSort.GROUP_RECENT.abbreviation());
        PostSort sort = PostSort.fromAbbreviation(key);
        switch (sort)
        {
        case GROUP_POPULAR:
            return this.findGeneric(FindType.BY_GROUP_POPULAR, page, filter, groupId);

        case GROUP_RECENT:
            return this.findGeneric(FindType.BY_GROUP_RECENT, page, filter, groupId);

        default:
            throw new IllegalArgumentException("Unsupported sort key " + key);
        }
    }


    private List<Post> findByGroupRecent(
        Long groupId,
        Page page)
    {
        // SELECT p.*,pc.*
        // FROM post p LEFT JOIN post_counter pc ON p.id=pc.post_id
        // WHERE p.group_id=? AND
        //       p.id <=> ?
        // ORDER BY p.id desc|asc
        // LIMIT ?

        final List<Post> posts = new ArrayList<>(page.size);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p.*,pc.* FROM post p LEFT JOIN post_counter pc ON p.id=pc.post_id ");
        sb.append("WHERE p.group_id=? ");

        boolean hasBoundary = StringUtils.isNotEmpty(page.boundary);
        if (hasBoundary)
        {
            String op = page.toMathOperator();
            sb.append("AND p.id" + op + "? ");
        }

        /**
         * if order is desc before or asc before, we change it desc
         * before -> asc after, acs before -> desc after then we
         * reverse it .
         */
        String order =
            page.isDescending() == page.isAfter() ? "DESC " : "ASC ";
        sb.append("ORDER BY p.id " + order);
        sb.append("LIMIT " + page.size);

        Object[] param = hasBoundary ? new Object[] { groupId, page.boundary } : new Object[] { groupId };
        final String sortKey = PostSort.ID.abbreviation();
        SqliteUtils.executeSelectQuery(
            sb.toString(),
            param,
            "findPostByGroup",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Post p = new Post();
                        parseResultSet(p, rs);
                        p.setSortKey(sortKey);
                        p.setSortVal(String.valueOf(p.getId()));
                        posts.add(p);
                    }
                }

            });
        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */

        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(posts);
        }

        return posts;
    }


    private List<Post> findByLDIndex(
        LDIndex index,
        Page page,
        Filter<LDEntry> indexFilter)
    {
        PostIndexVisitor visitor = new PostIndexVisitor(page.size);
        if (indexFilter != null)
            visitor.setIndexFilter(indexFilter);

        boolean descending = page.isDescending();
        index.visit(
            page.boundary,
            (page.isAfter() ? descending : !descending),
            page.exclusive,
            visitor);

        if (!page.isAfter())
            visitor.reverse();
        return visitor.getList();
    }


    @Override
    public List<Post> findByLatLng(
        LatLng latlng,
        Page page,
        Filter<Post> filter)
    {
        String key =
            StringUtils.defaultIfEmpty(
                page.key,
                PostSort.ID.abbreviation());
        PostSort sort = PostSort.fromAbbreviation(key);
        switch (sort)
        {
        case ID:
        case NEARBY_RECENT:
            // Based on current algorithm, ID is same as RECENT
            return this.findGeneric(FindType.BY_GEO_RECENT, page, filter, latlng);

        case NEARBY_POPULAR:
            return this.findGeneric(FindType.BY_GEO_POPULAR, page, filter, latlng);

        default:
            throw new IllegalArgumentException(
                "Unsupported sort key " + key);
        }
    }


    private List<Post> findByLatLngRecent(
        LatLng latlng,
        Page page)
    {
        // SELECT p.*, c.*
        // FROM post p, post_counter pc
        // WHERE p.id = pc.post_id AND 
        //       p.id IN (
        //              SELECT id FROM post_geo g
        //                        WHERE g.minX>=? AND 
        //                              g.maxX<=? AND 
        //                              g.minY>=? AND
        //                              g.maxY<=?) AND
        //       p.id < ?
        // ORDER BY p.id desc|asc
        // LIMIT ?

        final List<Post> posts = new ArrayList<>(page.size);
        int miles =
            Configs.getInt(SystemSetting.GEO_NEARBY_MILES, 2); //2 miles by default
        GeoLocation geoLocation =
            GeoLocation.fromDegrees(latlng.getLat(), latlng.getLon());
        GeoLocation[] geos =
            geoLocation.boundingCoordinates(miles);
        Double minLat = geos[0].getLatitudeInDegrees();
        Double minLon = geos[0].getLongitudeInDegrees();
        Double maxLat = geos[1].getLatitudeInDegrees();
        Double maxLon = geos[1].getLongitudeInDegrees();

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p.*,c.* ");
        sb.append("FROM post p, post_counter c ");
        sb.append("WHERE p.id = c.post_id AND ");
        sb.append("p.id IN ");

        String rsql = " (SELECT id FROM post_geo g WHERE "
            + "g.minX>=? AND "
            + "g.maxX<=? AND "
            + "g.minY>=? AND "
            + "g.maxY<=?) ";

        // Case when meridian 180 within distance
        if (minLon > maxLon)
        {
            rsql = " (SELECT id FROM post_geo g WHERE "
                + "(g.minX>=? AND g.maxX<=?)"
                + " AND "
                + "(g.minY>=? OR g.maxY<=?)) ";
        }
        sb.append(rsql);

        boolean hasBoundary = StringUtils.isNotEmpty(page.boundary);
        if (hasBoundary)
        {
            String op = page.toMathOperator();
            sb.append("AND id" + op + "? ");
        }

        /**
         * if order is desc before or asc before, we change it desc
         * before -> asc after, acs before -> desc after then we
         * reverse it .
         */
        String order =
            page.isDescending() == page.isAfter() ? "DESC " : "ASC ";
        sb.append("ORDER BY id " + order);
        sb.append("LIMIT " + page.size);

        Object[] param = null;
        if (hasBoundary) {
            param = new Object[] { minLat, maxLat, minLon, maxLon, page.boundary };
        }
        else {
            param = new Object[] { minLat, maxLat, minLon, maxLon };
        }

        final String sortKey =
            PostSort.NEARBY_RECENT.abbreviation();
        SqliteUtils.executeSelectQuery(
            sb.toString(),
            param,
            "findByGeoRecent",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Post p = new Post();
                        parseResultSet(p, rs);
                        p.setSortKey(sortKey);
                        p.setSortVal(String.valueOf(p.getId()));
                        posts.add(p);
                    }
                }

            });
        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */

        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(posts);
        }

        return posts;
    }


    private List<Post> findByLatLngPopular(
        LatLng latlng,
        Page page)
    {
        // SELECT p.*, c.*, r.*
        // FROM post_rank r, post p, post_counter c
        // WHERE r.post_id = p.id AND 
        //       r.post_id = c.post_id AND
        //       r.post_id IN (
        //              SELECT id FROM post_geo pg
        //                        WHERE pg.minX>=? AND 
        //                              pg.maxX<=? AND 
        //                              pg.minY>=? AND
        //                              pg.maxY<=?) AND
        //       r.score < ?
        // ORDER BY r.score desc|asc
        // LIMIT ?

        final List<Post> posts = new ArrayList<>(page.size);

        int miles =
            Configs.getInt(SystemSetting.GEO_NEARBY_MILES, 2); //2 miles by default
        GeoLocation geoLocation =
            GeoLocation.fromDegrees(latlng.getLat(), latlng.getLon());
        GeoLocation[] geos =
            geoLocation.boundingCoordinates(miles);
        Double minLat = geos[0].getLatitudeInDegrees();
        Double minLon = geos[0].getLongitudeInDegrees();
        Double maxLat = geos[1].getLatitudeInDegrees();
        Double maxLon = geos[1].getLongitudeInDegrees();

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p.*,c.*,r.score ");
        sb.append("FROM post_rank r, post p, post_counter c, post_geo g ");
        sb.append("WHERE r.post_id = p.id AND ");
        sb.append("r.post_id = c.post_id AND ");
        sb.append("r.post_id = g.id AND ");

        String rsql =
            "g.minX>=? AND "
                + "g.maxX<=? AND "
                + "g.minY>=? AND "
                + "g.maxY<=? ";

        // Case when meridian 180 within distance
        if (minLon > maxLon)
        {
            rsql =
                "(g.minX>=? AND g.maxX<=?)"
                    + " AND "
                    + "(g.minY>=? OR g.maxY<=?) ";
        }
        sb.append(rsql);

        boolean hasBoundary = StringUtils.isNotEmpty(page.boundary);
        if (hasBoundary)
        {
            String op = page.toMathOperator();
            sb.append("AND r.score" + op + "? ");
        }

        String order =
            page.isDescending() == page.isAfter() ? "DESC " : "ASC ";
        sb.append("ORDER BY r.score " + order);
        sb.append("LIMIT " + page.size);

        Object[] param = null;
        if (hasBoundary) {
            param = new Object[] { minLat, maxLat, minLon, maxLon, page.boundary };
        }
        else {
            param = new Object[] { minLat, maxLat, minLon, maxLon };
        }

        final String sortKey =
            PostSort.NEARBY_POPULAR.abbreviation();
        SqliteUtils.executeSelectQuery(
            sb.toString(),
            param,
            "findByGeoPopular",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Post p = new Post();
                        parseResultSet(p, rs);
                        p.setSortKey(sortKey);
                        p.setSortVal(String.valueOf(rs.getDouble("score")));
                        posts.add(p);
                    }
                }
            });
        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */

        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(posts);
        }

        return posts;
    }


    /**
     * Fetch posts sorted by ID, if clazz is specified, fetch posts
     * of that clazz only
     */
    private List<Post> findById(
        Page page,
        PostClass clazz)
    {
        // SELECT p.*,pc.*
        // FROM post p JOIN post_counter pc ON p.id=pc.post_id
        // WHERE p.id <=> ?
        // ORDER BY p.id desc|asc
        // LIMIT ?

        final List<Post> posts = new ArrayList<>(page.size);
        Object[] param = null;
        StringBuilder sb = new StringBuilder();
        
        // select clause
        sb.append("SELECT p.*,pc.* FROM post p LEFT JOIN post_counter pc ON p.id = pc.post_id");
        
        // where clause
        if (clazz != null) {
            if (Strings.isNullOrEmpty(page.boundary)) {
                sb.append(" WHERE clazz=?");
                param = new Object[]{clazz.name()};
            } else {
                sb.append(" WHERE clazz=? AND id");
                sb.append(page.toMathOperator());
                sb.append("?");
                param = new Object[]{clazz.name(), page.boundary};
            }
        } else {
            if (Strings.isNullOrEmpty(page.boundary)) {
                // Do nothing here
            } else {
                sb.append(" WHERE id");
                sb.append(page.toMathOperator());
                sb.append("?");
                param = new Object[]{page.boundary};
            }
        }
        
        // order clause
        sb.append(" ORDER BY id ");
        sb.append((page.isDescending() == page.isAfter() ? "DESC" : "ASC"));
        sb.append(" LIMIT ");
        sb.append(page.size);
        
        String sql = sb.toString();

        final String sortKey = PostSort.ID.abbreviation();
        SqliteUtils.executeSelectQuery(
            sql,
            param,
            "findById",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Post p = new Post();
                        parseResultSet(p, rs);
                        p.setSortKey(sortKey);
                        p.setSortVal(String.valueOf(p.getId()));
                        posts.add(p);
                    }
                }
            });

        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */
        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(posts);
        }

        return posts;
    }


    @Override
    public List<Post> findByOwner(
        long ownerId,
        Page page,
        Filter<Post> filter)
    {
        final String key =
            StringUtils.defaultIfEmpty(
                page.key,
                PostSort.ID.abbreviation());
        PostSort sort = PostSort.fromAbbreviation(key);
        switch (sort)
        {
        case ID:
            return this.findGeneric(FindType.BY_OWNER_RECENT, page, filter, ownerId);

        default:
            throw new IllegalArgumentException("Unsupported sort key " + key);
        }
    }


    private List<Post> findByOwnerRecent(
        long ownerId,
        Page page)
    {
        // SELECT p.*,pc.*
        // FROM post p LEFT JOIN post_counter pc ON p.id=pc.post_id
        // WHERE p.owner_id=? AND
        //       p.id <=> ?
        // ORDER BY p.id desc|asc
        // LIMIT ?

        final List<Post> posts = new ArrayList<>(page.size);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p.*,pc.* FROM post p LEFT JOIN post_counter pc ON p.id=pc.post_id ");
        sb.append("WHERE p.owner_id=? ");

        boolean hasBoundary = StringUtils.isNotEmpty(page.boundary);
        if (hasBoundary)
        {
            String op = page.toMathOperator();
            sb.append("AND p.id" + op + "? ");
        }

        /**
         * if order is desc before or asc before, we change it desc
         * before -> asc after, acs before -> desc after then we
         * reverse it .
         */
        String order =
            page.isDescending() == page.isAfter() ? "DESC " : "ASC ";
        sb.append("ORDER BY p.id " + order);
        sb.append("LIMIT " + page.size);

        final String sortKey = PostSort.ID.abbreviation();
        Object[] param = null;
        if (hasBoundary)
            param = new Object[] { ownerId, page.boundary };
        else
            param = new Object[] { ownerId };
        SqliteUtils.executeSelectQuery(
            sb.toString(),
            param,
            "findPostByOwner",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Post p = new Post();
                        parseResultSet(p, rs);
                        p.setSortKey(sortKey);
                        p.setSortVal(String.valueOf(p.getId()));
                        posts.add(p);
                    }
                }

            });
        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */

        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(posts);
        }

        return posts;
    }


    @Override
    public List<Flag> findFlags(
        Page page,
        Filter<Flag> filter)
    {
        String key =
            StringUtils.defaultIfEmpty(
                page.key,
                FLAG_SK);
        key = key.toUpperCase();
        switch (key)
        {
        case FLAG_SK:
            return this.findFlagsById(page, filter);

        default:
            throw new IllegalArgumentException(
                "Unsupported sort key " + key);
        }
    }


    private List<Flag> findFlagsById(
        Page page,
        Filter<Flag> filter)
    {
        // SELECT *
        // FROM post_flag
        // WHERE id <=> ?
        // ORDER BY id desc|asc
        // LIMIT ?

        final List<Flag> flags = new ArrayList<>(page.size);
        String sql;
        if (Strings.isNullOrEmpty(page.boundary))
        {
            sql =
                "SELECT * "
                    + "FROM post_flag "
                    + "ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }
        else
        {
            sql =
                "SELECT * "
                    + "FROM post_flag "
                    + "WHERE id "
                    + (page.isDescending() == page.isAfter() ? (page.exclusive ? "<?" : "<=?") : (page.exclusive ? ">?" : ">=?"))
                    + " ORDER BY id "
                    + (page.isDescending() == page.isAfter() ? "DESC" : "ASC")
                    + " LIMIT "
                    + page.size;
        }


        final String sortKey = FLAG_SK;
        SqliteUtils.executeSelectQuery(
            sql,
            Strings.isNullOrEmpty(page.boundary) ? null : new Object[] { page.boundary },
            "findFlagsById",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Flag f = new Flag();
                        parseFlagResultSet(f, rs);
                        f.setSortKey(sortKey);
                        f.setSortVal(String.valueOf(f.getId()));
                        flags.add(f);
                    }
                }
            });

        /**
         * reverse when boundary=null position=before acs and desc,
         * also reverse all before since we changed desc before ->
         * asc after, acs before -> desc after .
         */
        if (StringUtils.isEmpty(page.boundary) && !page.isAfter()
            || !page.isAfter())
        {
            Collections.reverse(flags);
        }

        return flags;
    }


    @Override
    public List<Flag> findFlagsByPost(
        long postId)
    {
        // SELECT *
        // FROM post_flag
        // WHERE post_id = ?
        // ORDER BY id desc

        final List<Flag> flags = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * ");
        sb.append("FROM post_flag ");
        sb.append("WHERE post_id = ? ");
        sb.append("ORDER BY id DESC");

        SqliteUtils.executeSelectQuery(
            sb.toString(),
            new Object[] { postId },
            "findFlagsByPost",
            new ResultSetParser() {

                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next())
                    {
                        Flag f = new Flag();
                        parseFlagResultSet(f, rs);
                        flags.add(f);
                    }
                }
            });

        return flags;
    }


    @Override
    public Post get(
        Long id)
    {
        final String sql =
            "SELECT p.*,pc.* FROM post p, post_counter pc WHERE p.id=pc.post_id AND p.id=?";

        final MutableObject<Post> mo = new MutableObject<>();
        SqliteUtils.executeSelectQuery(
            sql,
            new Object[] {
            id
            },
            "getPost",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    if (rs.next())
                    {
                        Post post = new Post();
                        parseResultSet(post, rs);
                        mo.setValue(post);
                    }
                }
            });
        return mo.getValue();
    }


    @Override
    public void init(
        RepositoryConfig config)
    {
        createPostTable(config);
        createPostCounterTable(config);
        createPostFlagTable(config);
        createPostGeoRTreeTable(config);
        createPostRankTable(config);
        SqliteUtils.createCompositeIndex(
            "post",
            "post_ownerId_id",
            "owner_id",
            "id",
            true);
        SqliteUtils.createCompositeIndex(
            "post",
            "post_groupId_id",
            "group_id",
            "id",
            true);
        SqliteUtils.createCompositeIndex("post", "post_clazz_id", "clazz", "id", true);
        SqliteUtils.createCompositeIndex(
            "post_rank",
            "post_rank_postId_score",
            "post_id",
            "score",
            false);
        this.batchSize = config.getBatchSize();
        this.tableName = config.getTableName();
    }


    @Override
    public PostCount getCount(
        Long postId)
    {
        final PostCount pc = new PostCount();
        String sql =
            "SELECT * FROM post_counter WHERE post_id = ?";
        SqliteUtils.executeSelectQuery(sql, new Object[] {
            postId
        }, "getPostCount", new ResultSetParser() {
            @Override
            public void parse(
                ResultSet rs)
                throws SQLException
            {
                if (rs.next())
                {
                    pc.setNumComments(rs.getLong("num_comments"));
                    pc.setNumDownvotes(rs.getLong("num_downvotes"));
                    pc.setNumFlags(rs.getLong("num_flags"));
                    pc.setNumShares(rs.getLong("num_shares"));
                    pc.setNumUpvotes(rs.getLong("num_upvotes"));
                    pc.setNumViews(rs.getLong("num_views"));
                }
            }
        });
        return pc;
    }


    @Override
    public void incrCount(
        Long postId,
        Field count,
        long incr)
    {
        String sql = "";
        switch (count)
        {
        case COMMENT:
            sql =
                "UPDATE post_counter SET num_comments=MAX(0,num_comments+?) WHERE post_id=?";
            break;
        case DOWNVOTE:
            sql = "UPDATE post_counter SET num_downvotes=MAX(0,num_downvotes+?) WHERE post_id=?";
            break;
        case FLAG:
            sql =
                "UPDATE post_counter SET num_flags=MAX(0,num_flags+?) WHERE post_id=?";
            break;
        case SHARE:
            sql =
                "UPDATE post_counter SET num_shares=MAX(0,num_shares+?) WHERE post_id=?";
            break;
        case UPVOTE:
            sql = "UPDATE post_counter SET num_upvotes=MAX(0,num_upvotes+?) WHERE post_id=?";
            break;
        case VIEW:
            sql =
                "UPDATE post_counter SET num_views=MAX(0,num_views+?) WHERE post_id=?";
            break;
        }
        Object[] param = {
                incr, postId
        };
        SqliteUtils.executeQuery(
            sql,
            param,
            "incr" + count);
    }


    @Override
    public Post put(
        Post p)
    {
        if (p.getId() <= 0L)
            throw new IllegalArgumentException("Not allow put post with id " + p.getId());

        Post upPost = new Post(p);

        // For replace into, any not specified column will be nullified if the row exists
        String sql =
            "INSERT OR REPLACE INTO post (body, clazz, create_time, geo, group_id, hashtags, id, medias, owner_id, status, thread_id, type, update_time) "
                + "VALUES "
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?)";

        Object param[] = new Object[13];
        param[0] = upPost.getBody();
        param[1] = upPost.getClazz().name();
        param[2] = upPost.getCreateTime().getTime();
        if (null != upPost.getGeo()) {
            param[3] = upPost.getGeo().toStr();
        }
        param[4] = p.getGroupId();
        if (0 < p.getNumHashtags()) {
            param[5] = StringUtils.join(p.getHashtags(), SP);
        }
        param[6] = upPost.getId();
        if (upPost.hasMedias())
            param[7] = Medias.serializeMediaList(upPost.getMedias());
        param[8] = upPost.getOwnerId();
        if (null != upPost.getStatus())
            param[9] = upPost.getStatus().toString();
        param[10] = upPost.getThreadId();
        param[11] = upPost.getType().name();
        param[12] = upPost.getUpdateTime().getTime();

        // Should we care about the return bool?
        SqliteUtils.executeQuery(sql, param, "putPost");

        return upPost;
    }


    @Override
    public Post update(
        Post p)

        throws UpdateConflictException
    {
        // MODIFIABLE fields:
        // body, geo, group_id, hashtags, media, status, thread_id

        // NON-MODIFIABLE fields:
        // clazz, create_time, id, owner_id, type

        // update_time is modifiable, but we also use the old data as optimistic version lock

        Post upPost = new Post(p);

        Date oldUpdateTime = p.getUpdateTime();
        Date newUpdateTime = new Date();
        upPost.setUpdateTime(newUpdateTime);
        Object param[] = new Object[14];

        // Prepare a query that updates mutable fields
        String updateSql =
            "UPDATE post SET body=?, geo=?, group_id=?, hashtags=?, medias=?, status=?, thread_id=?, update_time=? ";
        param[0] = upPost.getBody();
        if (null != upPost.getGeo()) {
            param[1] = upPost.getGeo().toStr();
        }
        param[2] = upPost.getGroupId();
        if (0 < p.getNumHashtags()) {
            param[3] = StringUtils.join(p.getHashtags(), SP);
        }
        if (upPost.hasMedias())
            param[4] = Medias.serializeMediaList(upPost.getMedias());
        if (null != upPost.getStatus())
            param[5] = upPost.getStatus().toString();
        param[6] = upPost.getThreadId();
        param[7] = newUpdateTime.getTime();

        // Prepare a query that matches on immutable fields
        String querySql =
            "WHERE id=? AND clazz=? AND create_time=? AND owner_id=? AND type=? AND update_time=?";
        param[8] = upPost.getId();
        param[9] = upPost.getClazz().name();
        param[10] = upPost.getCreateTime().getTime();
        param[11] = upPost.getOwnerId();
        param[12] = upPost.getType().name();
        param[13] = oldUpdateTime.getTime();

        String sql = updateSql + querySql;
        boolean success =
            SqliteUtils.executeQuery(sql, param, "updatePost");
        if (!success)
        {
            // Find out if the entity exists
            Post post = this.get(upPost.getId());
            if (post == null)
            {
                return null;
            }

            if (post.getUpdateTime().compareTo(oldUpdateTime) != 0)
                throw new UpdateConflictException(
                    "Update conflict - specified update time: "
                        + oldUpdateTime
                        + "  current update time: "
                        + post.getUpdateTime());

            // If we make it here, it must be the case that an immutable field was changed
            throw new IllegalArgumentException(
                "Attempt to modify an immutable field");
        }

        return upPost;
    }



    @Override
    public void updateGeo(
        long postId,
        Geo geo)
    {
        GeoLocation geoLoc =
            GeoLocation.fromDegrees(geo.getLat(), geo.getLon());

        // R-tree only store rectangle value, we create a tiny square centered at the 
        // specified post's geo, with configuration size (by default, as close as 1m)
        GeoLocation[] geos =
            geoLoc.boundingCoordinates(Configs.getDouble(
                SystemSetting.GEO_POST_BOUNDRECT_SIZE,
                0.00001));
        Double minLat = geos[0].getLatitudeInDegrees();
        Double minLon = geos[0].getLongitudeInDegrees();
        Double maxLat = geos[1].getLatitudeInDegrees();
        Double maxLon = geos[1].getLongitudeInDegrees();

        // Meridian 180 within the tiny square
        if (minLon > maxLon)
        {
            maxLon = 180d;
        }

        String sql = "UPDATE post_geo SET minX=?, maxX=?, minY=?, maxY=? WHERE id=?";
        SqliteUtils.executeQuery(sql, new Object[] {
                minLat, maxLat, minLon, maxLon, postId
        }, "updatePostGeo");
    }


    @Override
    public void updateScore(
        long postId,
        double score)
    {
        String sql =
            "UPDATE post_rank SET score=? WHERE post_id=?";
        SqliteUtils.executeQuery(sql, new Object[] {
                score, postId
        }, "updatePostScore");
    }


    @Override
    public void visitAll(
        Visitor<Post> visitor)
    {
        if (!visitor.before())
            return;

        SQLiteManager sqliteManager = SQLiteManager.instance();
        Connection connection = null;
        PreparedStatement statement = null;

        boolean hasMore = true;
        boolean aborted = false;
        long id = 0;

        try
        {
            // NOTE: visitAll doesn't return post counter
            connection = sqliteManager.getConnection();
            final String sql =
                "SELECT p.*,pc.* FROM post p LEFT JOIN post_counter pc ON p.id=pc.post_id WHERE p.id>? limit "
                    + this.batchSize;

            statement =
                connection.prepareStatement(sql);
            while (hasMore && !aborted)
            {
                statement.setLong(1, id);
                ResultSet rs = statement.executeQuery();
                int count = 0;
                while (rs.next())
                {
                    count++;
                    id = rs.getLong("id");

                    Post post = new Post();
                    parseResultSet(post, rs);
                    if (!visitor.visit(post))
                    {
                        aborted = true;
                        break;
                    }
                }
                if (count != this.batchSize)
                    hasMore = false;
                rs.close();
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(
                "Fail to execute query \"visitAll\"",
                e);
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                // Only LOG as we can't do anything about it now
                LOG.error(
                    "Fail to close connection for visitAll",
                    e);
            }
        }

        visitor.after();
    }


    //
    // RepositoryAdmin
    //

    @Override
    public void exportData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public void importData(
        String fname)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }



    //
    // Table Creation
    //


    private void createPostTable(
        RepositoryConfig config)
    {
        final String createPostTableSql =
            "CREATE TABLE IF NOT EXISTS "
                + config.getTableName()
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                +
                "body TEXT," +
                "clazz TEXT NOT NULL DEFAULT 'PUBLIC'," +
                "create_time INTEGER," +
                "geo TEXT," +
                "group_id INTEGER NOT NULL," +
                "hashtags TEXT," +
                "medias TEXT," +
                "owner_id INTEGER NOT NULL," +
                "thread_id INTEGER," +
                "type TEXT," +
                "update_time INTEGER," +
                "status TEXT" +
                ") ";
        SqliteUtils.executeQuery(
            createPostTableSql,
            null,
            "CreatePostTable");
    }


    private void createPostCounterTable(
        RepositoryConfig config)
    {
        final String createPostCounterTableSql =
            "CREATE TABLE IF NOT EXISTS post_counter"
                +
                "(post_id INTEGER PRIMARY KEY NOT NULL,"
                +
                "num_comments INTEGER DEFAULT 0," +
                "num_downvotes INTEGER DEFAULT 0," +
                "num_flags INTEGER DEFAULT 0," +
                "num_shares INTEGER DEFAULT 0," +
                "num_upvotes INTEGER DEFAULT 0," +
                "num_views INTEGER DEFAULT 0" +
                " )";
        SqliteUtils.executeQuery(
            createPostCounterTableSql,
            null,
            "createPostCounterTable");
    }


    private void createPostFlagTable(
        RepositoryConfig config)
    {
        final String createPostFlagTableSql =
            "CREATE TABLE IF NOT EXISTS post_flag"
                +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                +
                " complaint_message TEXT," +
                " create_time INTEGER," +
                " post_id INTEGER NOT NULL," +
                " user_id INTEGER NOT NULL" +
                " )";
        SqliteUtils.executeQuery(
            createPostFlagTableSql,
            null,
            "createPostFlagTable");
    }


    private void createPostGeoRTreeTable(
        RepositoryConfig config)
    {
        final String createPostGeoRTreeTableSql =
            "CREATE VIRTUAL TABLE IF NOT EXISTS post_geo USING rtree"
                + "(id, minX, maxX, minY, maxY)";
        SqliteUtils.executeQuery(
            createPostGeoRTreeTableSql,
            null,
            "createPostGeoRTreeTable");
    }


    private void createPostRankTable(
        RepositoryConfig config)
    {
        final String createPostRankTableSql =
            "CREATE TABLE IF NOT EXISTS post_rank"
                +
                "(post_id INTEGER PRIMARY KEY NOT NULL,"
                +
                "score REAL" +
                " )";
        SqliteUtils.executeQuery(
            createPostRankTableSql,
            null,
            "createPostRankTable");
    }



    //
    // INTERNAL CLASSES
    //


    private static enum FindType
    {
        BY_GEO_POPULAR,
        BY_GEO_RECENT,
        BY_GLOBAL_RECENT,
        BY_GROUP_POPULAR,
        BY_GROUP_RECENT,
        BY_ID,
        BY_INDEX_RECENT_POPULARITY,
        BY_OWNER_RECENT,

        ;
    }


    //
    // INTERNAL METHODS
    //

    private Post addInternal(
        Post p,
        boolean skipGeoIndex)
    {
        String sql =
            "INSERT INTO post "
                + "(body, clazz, create_time, geo, group_id, hashtags, medias, owner_id, status, thread_id, type, update_time) "
                + " VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        Post newPost = new Post(p);

        // It is allowed to specify createTime and updateTime via AdminConsole
        if (newPost.getCreateTime() == null)
            newPost.setCreateTime(new Date());
        if (newPost.getUpdateTime() == null)
            newPost.setUpdateTime(new Date());

        newPost.setCount(new PostCount());

        // SQL statement parameters construction
        Object param[] = new Object[12];
        param[0] = newPost.getBody();
        param[1] = newPost.getClazz().name();
        param[2] = newPost.getCreateTime().getTime();
        if (null != newPost.getGeo())
            param[3] = newPost.getGeo().toStr();
        param[4] = newPost.getGroupId();
        if (0 < newPost.getNumHashtags())
            param[5] = StringUtils.join(p.getHashtags(), SP);
        if (newPost.hasMedias()) {
            param[6] = Medias.serializeMediaList(newPost.getMedias());
        }
        param[7] = newPost.getOwnerId();
        if (null != newPost.getStatus())
            param[8] = newPost.getStatus().toString();
        param[9] = newPost.getThreadId();
        param[10] = newPost.getType().name();
        param[11] = newPost.getUpdateTime().getTime();

        // Execute SQL
        long newId = SqliteUtils.executeInsertQuery(
            sql,
            param,
            "addPost",
            true);
        newPost.setId(newId);

        // Execute rest queries in one transaction
        // 1. By default (SQL schema), all post_counter are ZEROs
        String sql1 =
            "INSERT INTO post_counter "
                + "(post_id, num_comments, num_downvotes, num_flags, num_likes, num_shares, num_stars, num_upvotes, num_views)"
                + " VALUES "
                + "(?,?,?,?,?,?,?,?,?)";
        Object[] param1 = {
                newId, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L
        };

        // 2. Init post_rank with initial score
        String sql2 =
            "INSERT INTO post_rank (post_id, score) VALUES (?,?)";
        Object[] param2 = {
                newId, this.makeInitialScore(newId)
        };

        // 3. Insert post_geo
        String sql3 = null;
        Object[] param3 = null;
        if (!skipGeoIndex && null != newPost.getGeo())
        {
            GeoLocation geo =
                GeoLocation.fromDegrees(
                    newPost.getGeo().getLat(),
                    newPost.getGeo().getLon());

            // R-tree only store rectangle value, we create a tiny square centered at the 
            // specified post's geo, with configuration size (by default, as close as 1m)
            GeoLocation[] geos =
                geo.boundingCoordinates(Configs.getDouble(
                    SystemSetting.GEO_POST_BOUNDRECT_SIZE,
                    0.00001));
            Double minLat = geos[0].getLatitudeInDegrees();
            Double minLon = geos[0].getLongitudeInDegrees();
            Double maxLat = geos[1].getLatitudeInDegrees();
            Double maxLon = geos[1].getLongitudeInDegrees();

            // Meridian 180 within the tiny square
            if (minLon > maxLon) {
                maxLon = 180d;
            }

            sql3 = "INSERT INTO post_geo (id, minX, maxX, minY, maxY) VALUES (?,?,?,?,?)";
            param3 = new Object[] { newId, minLat, maxLat, minLon, maxLon };
        }

        if (sql3 != null)
        {
            SqliteUtils.executeTransactionalQueries(
                "addPostRelated",
                sql1,
                param1,
                sql2,
                param2,
                sql3,
                param3);
        }
        else
        {
            SqliteUtils.executeTransactionalQueries(
                "addPostRelated",
                sql1,
                param1,
                sql2,
                param2);
        }

        return newPost;
    }


    private List<Post> findGeneric(
        FindType type,
        Page page,
        Filter<Post> filter,
        Object... objects)
    {
        int n = page.size, m = 0;
        List<Post> result = new ArrayList<>();
        Page p = new Page(page);

        List<Long> filterGroupIds = null;

        // For now, we don't need compute larger batch size for index lookup
        // since it is relatively faster by accessing ram
        if (filter != null)
        {
            boolean needLargerBatch = false;
            Filter<Post> f = filter;
            while (f != null)
            {
                if (f instanceof FlagPostFilter)
                {
                    FlagPostFilter fpf = (FlagPostFilter) f;
                    needLargerBatch = needLargerBatch || fpf.hasIds();
                }
                else if (f instanceof SpecialPostStatusFilter)
                {
                    // If we only need filter ONE_O_ONE post that is extremely rare, we don't need larger batch of results
                    SpecialPostStatusFilter spsf = (SpecialPostStatusFilter) f;
                    needLargerBatch = needLargerBatch || !spsf.isFilterRareStatus();
                }
                else if (f instanceof PostGroupFilter)
                {
                    PostGroupFilter pgf = (PostGroupFilter) f;
                    needLargerBatch = needLargerBatch || pgf.hasGroupIds();
                    filterGroupIds = pgf.getGroupIds(); // save the filter for later use
                }
                // else if other type of filter

//                if (needLargerBatch)
//                    break;

                f = f.getNextFilter();
            }

            if (needLargerBatch)
            {
                // If we have filter, get larger batch of result to avoid query database
                // many times in case we don't have enough (page.size) items to return
                p.size = Math.min(p.size * 2, Math.max(100, p.size));
            }
        }

        do
        {
            List<Post> list = null;
            switch (type)
            {
            case BY_GEO_POPULAR:
                list = this.findByLatLngPopular((LatLng) objects[0], p);
                break;

            case BY_GEO_RECENT:
                list = this.findByLatLngRecent((LatLng) objects[0], p);
                break;

            case BY_GLOBAL_RECENT:
                list = this.findById(p, PostClass.PUBLIC);
                break;

            case BY_GROUP_POPULAR:
                {
                    long groupId = (long) objects[0];
                    InternalPostIndexGroupFilter ipigf = new InternalPostIndexGroupFilter(groupId);
                    ipigf.setOpposite(true); // Only ACCEPT the post with specified group ids
                    if (groupId == 2L) {
                        // 8/13/15
                        // Group 2 (ForMod) is a hot group, we do have a lot of posts here each day, hence
                        // we still keep using the recent-popular algorithm for generating this group's hot feed
                        list =
                            this.findByLDIndex(
                                Indexes.get(IndexType.POSTS_BY_RECENT_POPULARITY, LDIndex.class),
                                p,
                                ipigf);
                    }
                    else {
                        // For any other group, since it is relatively cold, we populate the hot feed using popularity
                        list =
                            this.findByLDIndex(
                                Indexes.get(IndexType.POSTS_IN_GROUP_BY_POPULARITY, LDIndex.class),
                                p,
                                ipigf);
                    }
                    break;
                }

            case BY_GROUP_RECENT:
                list = this.findByGroupRecent((Long) objects[0], p);
                break;

            case BY_ID:
                list = this.findById(p, null);
                break;

            case BY_INDEX_RECENT_POPULARITY:
                {
                    InternalPostIndexGroupFilter ipigf = null;
                    if (filterGroupIds != null && !filterGroupIds.isEmpty()) {
                        // Optimize, let's for group filter, we filter directly from index entries
                        ipigf = new InternalPostIndexGroupFilter(filterGroupIds);
                        ipigf.setOpposite(false);
                    }
                    list =
                        this.findByLDIndex(Indexes.get(IndexType.POSTS_BY_RECENT_POPULARITY, LDIndex.class), p, ipigf);
                    break;
                }

            case BY_OWNER_RECENT:
                list = this.findByOwnerRecent((Long) objects[0], p);
                break;
            }

            if (list == null || list.isEmpty())
            {
                // If list is empty, that means we reach to the end of the query and pagination
                break;
            }

            for (Post post : list)
            {
                if (filter == null || filter.accept(post))
                {
                    result.add(post);

                    m++;
                    if (m == n)
                        break;
                }
            }

            if (list.size() < n)
            {
                // The retrieved list has less items than specified, that means we reach to the end of query and pagination
                break;
            }

            // Apparently, we fail to fill up the result, and filter out a lot items
            if (m < n)
            {
                // Construct next Page
                p.boundary = GDUtils.getLast(list).getSortVal();
                p.position = Page.AFTER;
                p.exclusive = true;

                LOG.info("Find {} goes to next page b={} o={} sz={} k={}", type, p.boundary, p.order, p.size, p.key);
            }

        } while (m < n);

        return result;
    }


    private double makeInitialScore(
        long postId)
    {
        // The score is used to sort post by POPULARITY, each post's score is calculated based on weights of
        // different factors such as num_comments, num_likes, etc. If scores are equal, postId is compared.
        // Therefore, in order to make pagination/sort much easier, we invent the new score whose integer part
        // is the REAL computed score and fractional part is the post ID
        return Double.parseDouble("0." + postId);
    }


    private void parseFlagResultSet(
        Flag flag,
        ResultSet rs)
        throws SQLException
    {
        flag.setComplainMessage(rs.getString("complaint_message"));
        flag.setCreateTime(new Date(
            rs.getLong("create_time")));
        flag.setId(rs.getLong("id"));
        flag.setPostId(rs.getLong("post_id"));
        flag.setUserId(rs.getLong("user_id"));
    }


    private void parseResultSet(
        Post post,
        ResultSet rs)
        throws SQLException
    {
        // Post
        post.setBody(rs.getString("body"));
        post.setClazz(PostClass.valueOf(rs.getString("clazz")));
        post.setCreateTime(new Date(rs.getLong("create_time")));
        post.setId(rs.getLong("id"));

        if (StringUtils.isNotEmpty(rs.getString("hashtags")))
        {
            post.setHashtags(Arrays.asList(rs.getString("hashtags").split(SP)));
        }

        String geoStr = rs.getString("geo");
        if (StringUtils.isNotEmpty(geoStr))
        {
            post.setGeo(new Geo(geoStr));
        }

        post.setGroupId(rs.getLong("group_id"));
        post.setOwnerId(rs.getLong("owner_id"));
        if (StringUtils.isNotEmpty(rs.getString("status")))
            post.setStatus(new PostStatus(rs.getString("status")));
        long threadIdVal = rs.getLong("thread_id");
        if (threadIdVal > 0L)
            post.setThreadId(threadIdVal);
        post.setType(PostType.valueOf(rs.getString("type")));
        post.setUpdateTime(new Date(rs.getLong("update_time")));

        // Counters
        PostCount pc = new PostCount();
        pc.setNumComments(rs.getLong("num_comments"));
        pc.setNumDownvotes(rs.getLong("num_downvotes"));
        pc.setNumFlags(rs.getLong("num_flags"));
        pc.setNumShares(rs.getLong("num_shares"));
        pc.setNumUpvotes(rs.getLong("num_upvotes"));
        pc.setNumViews(rs.getLong("num_views"));
        pc.setNumStars(rs.getLong("num_stars"));
        post.setCount(pc);

        // Media, note that it is different per PostType
        String mediaStr = rs.getString("medias");
        post.setMedias(Medias.deserializeMediaList(post.getId(), mediaStr));
    }



    //
    // INTERNAL CLASSES
    //

    private class InternalPostIndexGroupFilter
        implements
            Filter<LDEntry>
    {
        private Set<Long> groupIdSet;

        /**
         * True if ONLY accept entries whose group IDs belong to the
         * set. False if reject those entries
         */
        private boolean opposite = false;


        InternalPostIndexGroupFilter(
            long groupId)
        {
            this.groupIdSet = new HashSet<>();
            this.groupIdSet.add(groupId);
        }


        InternalPostIndexGroupFilter(
            List<Long> groupIdList)
        {
            this.groupIdSet = new HashSet<>(groupIdList);
        }


        @Override
        public boolean accept(
            LDEntry entry)
        {
            if (entry == null)
                return false;

            // s2 of index entry of POSTS_BY_RECENT_POPULAR should contain group Id
            if (Strings.isNullOrEmpty(entry.s2))
                return false;

            long groupId = Long.parseLong(entry.s2);
            if (this.opposite ^ this.groupIdSet.contains(groupId))
                return false;

            return true;
        }


        @Override
        public Filter<LDEntry> chainFilter(
            Filter<LDEntry> filter)
        {
            return null;
        }


        @Override
        public Filter<LDEntry> getNextFilter()
        {
            return null;
        }


        public void setOpposite(
            boolean opposite)
        {
            this.opposite = opposite;
        }
    }
}
