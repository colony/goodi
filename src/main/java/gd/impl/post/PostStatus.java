
package gd.impl.post;


import java.util.BitSet;

import com.amazonaws.util.StringUtils;



public class PostStatus
{
    /*
     * The internal data structure to hold all boolean info. Each
     * Flag's ordinal corresponds to the index of bitset and the
     * boolean value of bitset means whether we have this asset or
     * not
     */
    protected BitSet bitSet;


    // Default constructor
    public PostStatus()
    {
        this.bitSet = new BitSet();
    }


    /**
     * Constructor
     * 
     * @param str
     *            A string with same format as the one returned by
     *            toString()
     */
    public PostStatus(
        String str)
    {
        if (StringUtils.isNullOrEmpty(str))
        {
            this.bitSet = new BitSet();
        }
        else
        {
            // Expect a string contains one Long
            try
            {
                long l = Long.parseLong(str);
                this.bitSet = BitSet.valueOf(new long[] {
                        l
                });
            }
            catch (NumberFormatException e)
            {
                throw new IllegalArgumentException(
                    "Unexpected format "
                        + str);
            }
        }
    }


    /**
     * Check whether at least one flag is set.
     * 
     * @return True if at least one flag is set. Otherwise, false
     */
    public boolean isEmpty()
    {
        return bitSet.isEmpty();
    }


    /**
     * Check whether the specified flag is set. In other words,
     * whether the corresponding asset is included
     * 
     * @param f
     *            the flag
     * @return true if set, false otherwise
     */
    public boolean isSet(
        Status s)
    {
        return bitSet.get(s.ordinal());
    }


    /**
     * Check whether if any of the specified flags is set.
     * 
     * @param flags
     * @return true if at least one of flags is set, false otherwise
     */
    public boolean isSetAny(
        Status... status)
    {
        for (Status s : status)
        {
            if (isSet(s))
                return true;
        }
        return false;
    }


    public void set(
        Status s,
        boolean b)
    {
        bitSet.set(s.ordinal(), b);
    }


    @Override
    public int hashCode()
    {
        return bitSet.hashCode();
    }


    @Override
    public boolean equals(
        Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PostStatus other = (PostStatus) obj;
        if (bitSet == null)
        {
            if (other.bitSet != null)
                return false;
        }
        else if (!bitSet.equals(other.bitSet))
            return false;
        return true;
    }


    @Override
    public String toString()
    {
        // BitSet could support more than 64 bits. If that's the case,
        // it will return an array of long value to store each 64 bits.
        // That is a rare case that we might support more than 64 special
        // assets, so one long is sufficient. If we do need go beyond this
        // limit, it's easy to make a comma-separated string of each long

        if (!bitSet.isEmpty())
        {
            long[] bits = bitSet.toLongArray();
            return String.valueOf(bits[0]);
        }
        else
        {
            return "";
        }
    }



    /**
     * An enum type of status. NOTE, all the status are stored as a
     * bitset, the ordinal CAN'T be changed hence, new status could
     * only be added in the end
     */
    public static enum Status
    {
        /**
         * Visible to public, but it is covered by a black caption
         * saying the post might consider inappropriate content, let
         * user proceed with caution
         */
        BLOCKED,

        /**
         * The first editorial post that stays at tail of
         * NEARBY_RECENT feed
         */
        @Deprecated
        ONE_O_ONE,

        /**
         * The post is created by fake user
         */
        FAKE,

        /**
         * Not visible to public, but the post still exists
         */
        HIDDEN,

        /**
         * This type of post, cannot be taken account into any
         * global-wide (i.e. popular) feed/index. It is only
         * displayable at the geo where it is created at
         */
        GEO_RESTRICTED,

        /**
         * The post is made specifically for announcement, shouldn't
         * be revealed anywhere other than announcement section
         */
        ANNOUNCEMENT,

        ;
    }

}
