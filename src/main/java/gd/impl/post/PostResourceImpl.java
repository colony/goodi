
package gd.impl.post;


import gd.api.v1.comment.CommentRepList;
import gd.api.v1.post.Geo;
import gd.api.v1.post.MediaType;
import gd.api.v1.post.PostForm;
import gd.api.v1.post.PostRep;
import gd.api.v1.post.PostResource;
import gd.api.v1.post.PostType;
import gd.api.v1.post.ThreadType;
import gd.api.v1.post.VoteStatus;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.activity.Activities;
import gd.impl.comment.Comment;
import gd.impl.comment.CommentRepository;
import gd.impl.config.Configs;
import gd.impl.config.DetailedConfigs;
import gd.impl.config.SystemSetting;
import gd.impl.group.Group;
import gd.impl.group.GroupCount;
import gd.impl.group.GroupRepository;
import gd.impl.group.Groups;
import gd.impl.identity.AccessManager;
import gd.impl.media.AudioMedia;
import gd.impl.media.FileManager;
import gd.impl.media.IconMedia;
import gd.impl.media.LinkMedia;
import gd.impl.media.Medias;
import gd.impl.media.PhotoMedia;
import gd.impl.media.TextMedia;
import gd.impl.media.VideoMedia;
import gd.impl.queue.Event;
import gd.impl.queue.EventQueue;
import gd.impl.queue.EventType;
import gd.impl.queue.QueueManager;
import gd.impl.queue.QueueType;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.user.UserRepository;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.UpdateConflictException;
import gd.support.jersey.BoolParam;
import gd.support.jersey.LongParam;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Path("/posts")
@Singleton
public class PostResourceImpl
    implements
        PostResource
{
    private static final Logger LOG =
        LoggerFactory.getLogger(PostResourceImpl.class);

    protected AccessManager accessMgr;

    protected CommentRepository cmtRepo;

    protected EventQueue eventQ;

    protected FileManager fileMgr;

    protected LLRelation flagsRel;

    protected GroupRepository groupRepo;

    protected LSRelation hasPostMetaRel;

    protected LLRelation hasThreadChildrenRel;

    protected LLRelation hasThreadParticipantRel;

    protected LLRelation isVotedByRel;

    protected PostRepository postRepo;

    protected UserRepository userRepo;

    protected LLRelation votesRel;


    public PostResourceImpl()
    {
        this.accessMgr = AccessManager.instance();
        this.fileMgr = FileManager.instance();

        RepositoryManager repoMgr = RepositoryManager.instance();
        this.cmtRepo =
            repoMgr.get(RepositoryType.COMMENT, CommentRepository.class);
        this.groupRepo =
            repoMgr.get(RepositoryType.GROUP, GroupRepository.class);
        this.postRepo =
            repoMgr.get(RepositoryType.POST, PostRepository.class);
        this.userRepo =
            repoMgr.get(RepositoryType.USER, UserRepository.class);

        RelationManager relMgr = RelationManager.instance();
        this.flagsRel = relMgr.get(RelationType.FLAGS, LLRelation.class);
        this.hasPostMetaRel = relMgr.get(RelationType.HAS_POST_METADATA, LSRelation.class);
        this.hasThreadChildrenRel = relMgr.get(RelationType.HAS_THREAD_CHILDREN, LLRelation.class);
        this.hasThreadParticipantRel = relMgr.get(RelationType.HAS_THREAD_PARTICIPANT, LLRelation.class);
        this.isVotedByRel = relMgr.get(RelationType.IS_VOTED_BY, LLRelation.class);
        this.votesRel = relMgr.get(RelationType.VOTES, LLRelation.class);

        QueueManager qMgr = QueueManager.instance();
        this.eventQ = qMgr.get(QueueType.EVENT, EventQueue.class);
    }


    @Override
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createPost(
        @Context HttpHeaders headers,
        @BeanParam PostForm postForm)
    {
        long userId = Rests.beginOperation(headers, "p:create");

        // Blocked user handling
        List<Long> blockedUserIds = DetailedConfigs.getBlockedUserIds();
        if (blockedUserIds.contains(userId)) {
            return Rests.appError("Oops, something bad happens, please try later.");
        }

        Geo geo = postForm.getGeo();
        if (geo == null) {
            // This happens for old app that send geo via header, new app will send geo via form
            geo = Rests.retrieveGeo(headers);
        }

        Long groupId = postForm.getGroupId();
        Group group = Groups.get(groupId);
        GDUtils.check(
            group != null,
            Status.NOT_FOUND,
            "No group with ID=" + groupId);

        // Prepare the post
        Post post = new Post();
        post.setClazz(PostClass.PUBLIC);
        String postTypeStr = StringUtils.defaultIfEmpty(postForm.getType(), "PHOTO");
        if (postTypeStr.equals("SIMPLE"))
            postTypeStr = "PHOTO"; // forward compatibility
        PostType postType = PostType.valueOf(postTypeStr);
        post.setType(postType);
        post.setOwnerId(userId);
        post.setBody(postForm.getBody());
        post.setGroupId(groupId);
        
        // Prepare media
        // !IMPORTANT, call this method must setPostType first
        this.prepareMedia(post, postForm);

        // If a group enabled privacy setting, the post and its comments won't reveal geo info
        // for now, the client will display geo's name before considering city, state and country
        // hence, we only need override the post's name information to protect geo info
        String covertGeoName = null;
        if (group.isPrivate()) {
            covertGeoName = DetailedConfigs.getRandomGeoCovertWord(groupId);
            geo.setName(covertGeoName);
        }
        post.setGeo(geo);

        // Thread stuff
        long createdThreadId = 0L;
        ThreadType createdThreadType = null;
        long participateThreadId = 0L;
        if (StringUtils.isNotEmpty(postForm.getCreateThreadType())) {
            createdThreadType = ThreadType.valueOf(postForm.getCreateThreadType());
            createdThreadId = Posts.createThreadId(createdThreadType);
            post.setThreadId(createdThreadId);
        }
        else if (postForm.getThreadId() != null) {
            participateThreadId = postForm.getThreadId();
            GDUtils.check(
                Posts.existThread(participateThreadId),
                Status.NOT_FOUND,
                "No thread with ID=" + participateThreadId);

            post.setClazz(PostClass.THREAD);
            post.setThreadId(participateThreadId);

            //=======HACK========
            // Thread post shouldn't belong to any group, also shouldn't add into geo index
            // Here, we set the thread post's group ID as its counterpart so that we could
            // avoid lookup them when quering group, also, we could efficiently populate 
            // their groupName when needed
            post.setGroupId(post.getGroupId() * -1);
            //=======HACK========
        }

        // ========CREATE POST========
        Post newPost = Posts.createPost(post, (covertGeoName != null || post.getClazz() != PostClass.PUBLIC));
        // ============================

        if (!newPost.getGeo().hasExtraInfo())
            LOG.warn("Post ID={} has incomplete geo info", newPost.getId());

        if (newPost.getClazz() == PostClass.THREAD) {
            this.hasThreadChildrenRel.addEdge(newPost.getThreadId(), newPost.getId());
        }

        if (createdThreadId > 0L && createdThreadType != null) {
            // Create thread data per type
            switch (createdThreadType)
            {
            case CARD_GAME:
                GDUtils.check(
                    newPost.getType() == PostType.CARD,
                    Status.BAD_REQUEST,
                    "CARD_GAME thread not allow post " + newPost.getId());

                CardMetadata cardMetadata = new CardMetadata(newPost.getBody());
                this.hasThreadChildrenRel.addSource(
                    new LEntry(createdThreadId)
                        .with(newPost.getId(), newPost.getOwnerId())
                        .with(
                            createdThreadType.name(),
                            ("1|" + cardMetadata.getDeckId() + "|" + newPost.getOwnerId() + "|0")));
                break;
            default:
            }
        }

        if (participateThreadId > 0L) {
            if (!this.hasThreadParticipantRel.hasEdge(participateThreadId, userId)) {
                this.hasThreadParticipantRel.addEdge(participateThreadId, userId);
            }

            Activities.asyncLogParticipateThread(userId, newPost.getId(), participateThreadId);
        }

        if (null != covertGeoName) {
            this.hasPostMetaRel.addEdge(newPost.getId(), new SEntry(Relations.HPM_COVERT_GEO).with(covertGeoName));
        }

        // Event for posterity
        Event evt = new Event(EventType.CREATE_POST);
        evt.lng1 = userId;
        evt.lng2 = newPost.getId();
        evt.lng3 = createdThreadId;
        evt.str1 = newPost.getType().name();
        this.eventQ.enq(evt);

        // Incr count for groups
        if (groupId > 1L && newPost.getClazz() == PostClass.PUBLIC)
            this.groupRepo.incrCount(groupId, GroupCount.Field.POST, 1L);

        PostRep rep = new PostRep();
        Reps.populatePost(rep, newPost, userId, true);
        return Response.ok(rep).build();
    }


    @Override
    @Path("/{id}")
    @DELETE
    @Produces("application/json")
    public Response deletePost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam)
    {
        long userId = Rests.beginOperation(headers, "p:delete:" + idParam.get());
        long id = idParam.get();

        // Retrieve the post
        Post post = this.postRepo.get(id);
        GDUtils.check(
            post != null,
            Status.NOT_FOUND,
            "No post with ID " + id);

        // You can only delete your post
        GDUtils.check(
            this.accessMgr.mayManagePost(userId, post),
            Status.FORBIDDEN,
            "You not allowed to perform this operation");

        Posts.deletePost(id);
        return Response.ok().build();
    }


    @Override
    @Path("/{id}")
    @GET
    @Produces("application/json")
    public Response getPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam)
    {
        long userId = Rests.beginOperation(headers, "p:get:" + idParam.get());
        long id = idParam.get();
        Post post = this.postRepo.get(id);

        GDUtils.check(
            post != null,
            Status.NOT_FOUND,
            "No post with ID " + id);

        PostRep rep = new PostRep();
        Reps.populatePost(rep, post, userId, false);

        // Record this event (owner doesn't count)
//        if (userId != post.getOwnerId())
//        {
//            Event event = new Event(EventType.VIEW_POST);
//            event.lng1 = userId;
//            event.lng2 = post.getId();
//            this.eventQ.enq(event);
//        }

        return Response.ok(rep).build();
    }


    @Override
    @Path("/{id}/web")
    @GET
    @Produces("application/json")
    public Response getWebPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginWebOperation(headers, "p:webget:" + idParam.get());
        long id = idParam.get();
        Post post = this.postRepo.get(id);

        GDUtils.check(post != null, Status.NOT_FOUND, "No post with ID " + id);

        PostRep rep = new PostRep();
        Reps.populatePostWeb(rep, post);

        List<Comment> cmtList = this.cmtRepo.findByPost(id, new Page(Page.ASC, "ID", Page.AFTER), null, true);
        CommentRepList cmtRepList = new CommentRepList();
        Reps.populateCommentListWeb(cmtRepList, cmtList);
        rep.comments = cmtRepList;

        return Response.ok(rep).build();
    }


    @Override
    @Path("/{id}/like")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response likePost(
        @Context HttpHeaders headers,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @FormParam("status") BoolParam statusParam)
    {
        long callerId = Rests.beginOperation(headers, "p:like:" + idParam.get() + ":" + statusParam.get());
        LOG.warn("Deprecated likePost callerId={}", callerId);
        return Rests.ok();
    }


    @Override
    @Path("/{id}/flag")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response flagPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("complaint_message") String complaintMessage)
    {
        long callerId = Rests.beginOperation(headers, "p:flag:" + idParam.get());

        long postId = idParam.get();
        Post post = Posts.get(postId);
        GDUtils.check(post != null, Status.NOT_FOUND, "No post with ID " + postId);

        Flag fp = new Flag();
        fp.setComplainMessage(complaintMessage);
        fp.setCreateTime(new Date());
        fp.setPostId(postId);
        fp.setUserId(callerId);
        this.postRepo.addFlag(fp);
        this.postRepo.incrCount(postId, PostCount.Field.FLAG, 1L);

        this.flagsRel.addEdge(callerId, postId);

        Event event = new Event(EventType.FLAG_POST);
        event.lng1 = callerId;
        event.lng2 = postId;
        event.lng3 = post.getOwnerId();
        this.eventQ.enq(event);

        // Certain flag will automatically trigger specific logic
        if (Configs.getBool(SystemSetting.AUTO_FLAG_LOGIC_ENABLED, false)) {
            if ("nudity".equalsIgnoreCase(complaintMessage)) {
                try {
                    Posts.setStatus(postId, PostStatus.Status.BLOCKED, false);
                    Groups.assignBlockGroup(postId, true);
                }
                catch (UpdateConflictException e) {
                    LOG.error("Fail to block the post ID={} from flag", postId, e);
                }
            }
        }

        return Rests.ok();
    }


    @Override
    @Path("/{id}/star")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response starPost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("num") @DefaultValue("0") LongParam numParam)
    {
        long callerId = Rests.beginOperation(headers, "p:star:" + idParam.get());
        LOG.warn("Deprecated starPost callerId={}", callerId);
        return Response.ok().build();
    }


    @Override
    @Path("/{id}/vote")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response votePost(
        @Context HttpHeaders headers,
        @PathParam("id") LongParam idParam,
        @FormParam("down") @DefaultValue("0") BoolParam isDownParam,
        @FormParam("up") @DefaultValue("0") BoolParam isUpParam,
        @FormParam("undown") @DefaultValue("0") BoolParam isUndownParam,
        @FormParam("unup") @DefaultValue("0") BoolParam isUnupParam)
    {
        boolean isDown = isDownParam.get();
        boolean isUp = isUpParam.get();
        boolean isUndown = isUndownParam.get();
        boolean isUnup = isUnupParam.get();

        long callerId = -1L;
        if (isDown || isUp) {
            callerId = Rests.beginOperation(headers, "p:vote:" + idParam.get() + ":" + (isDown ? "d" : "u"));
        }
        else if (isUndown || isUnup) {
            callerId = Rests.beginOperation(headers, "p:vote:" + idParam.get() + ":" + (isUndown ? "ud" : "uu"));
        }

        if (callerId < 0L)
            return Response.status(Status.BAD_REQUEST).entity("No vote is taken").build();

        long postId = idParam.get();
        Post post = Posts.get(postId);
        GDUtils.check(
            post != null,
            Status.NOT_FOUND,
            "No post with ID " + postId);

        if (isDown || isUp) {
            this.postRepo.incrCount(postId, isDown ? PostCount.Field.DOWNVOTE : PostCount.Field.UPVOTE, 1L);
            if (post.getGroupId() != 1L) {
                this.groupRepo.incrCount(
                    post.getGroupId(),
                    isDown ? GroupCount.Field.DOWNVOTE : GroupCount.Field.UPVOTE,
                    1L);
            }
        }
        if (isUndown || isUnup) {
            this.postRepo.incrCount(postId, isUndown ? PostCount.Field.DOWNVOTE : PostCount.Field.UPVOTE, -1L);
            if (post.getGroupId() != 1L) {
                this.groupRepo.incrCount(
                    post.getGroupId(),
                    isUndown ? GroupCount.Field.DOWNVOTE : GroupCount.Field.UPVOTE,
                    -1L);
            }
        }

        if (isDown || isUp) {
            VoteStatus status = isDown ? VoteStatus.DOWNVOTE : VoteStatus.UPVOTE;
            if (this.votesRel.hasEdge(callerId, postId)) {
                this.votesRel.modifyEdgeByData(
                    callerId,
                    postId,
                    null,
                    null,
                    null,
                    new Long(status.ordinal()), // l1
                    true, // assign l1
                    null,
                    false);
            }
            else {
                LEntry node = new LEntry(postId).with(status.ordinal());
                this.votesRel.addEdge(callerId, node);
                this.isVotedByRel.addEdge(postId, callerId);
            }
        }
        else if (isUndown || isUnup) {
            this.votesRel.deleteEdge(callerId, postId);
            this.isVotedByRel.deleteEdge(postId, callerId);
        }

        // If the caller vote on the post (instead of switching from previous votes)
        // record down the event, NOTE this could avoid incorrectly count num of voters
        // (inside logVotePost) if a user switches votes
        if ((isDown || isUp) && (!isUndown && !isUnup)) {
            Activities.asyncLogVotePost(callerId, postId);
        }

        int delta = 0;
        if (isDown)
            delta -= 1;
        if (isUnup)
            delta -= 1;
        if (isUndown)
            delta += 1;
        if (isUp)
            delta += 1;
        Event event = new Event(EventType.VOTE_POST);
        event.lng1 = callerId;
        event.lng2 = postId;
        event.lng3 = post.getOwnerId();
        event.int1 = delta;
        event.bool1 = (isUnup && !isDown) || (isUndown && !isUp);
        this.eventQ.enq(event);

        return Response.ok().build();
    }


    //
    // INTERNAL METHODS
    //

    private void prepareMedia(
        Post post,
        PostForm postForm)
    {
        // Persist the media
        String audioKey = null;
        if (null != postForm.getAudioStream())
        {
            AudioMedia.Format fmt =
                AudioMedia.Format.valueOf(postForm.getAudioFormat());
            long size = postForm.getAudioSize();
            try
            {
                // For now, the IOS client uses AVPlayer, it ONLY works if there's extension in URL
                audioKey =
                    this.fileMgr.store(
                        postForm.getAudioStream(),
                        MediaType.AUDIO,
                        fmt.getContentType(),
                        size,
                        null,
                        fmt.getExtension());
            }
            catch (IOException e)
            {
                throw new WebApplicationException(
                    "Fail to upload file for mediaType=AUDIO and size=" + size, e);
            }
        }

        String photoKey = null;
        if (null != postForm.getPhotoStream())
        {
            PhotoMedia.Format fmt =
                PhotoMedia.Format.valueOf(postForm.getPhotoFormat());
            long size = postForm.getPhotoSize();
            try
            {
                photoKey =
                    this.fileMgr.store(
                        postForm.getPhotoStream(),
                        MediaType.PHOTO,
                        fmt.getContentType(),
                        size,
                        null,
                        null);
            }
            catch (IOException e)
            {
                throw new WebApplicationException(
                    "Fail to upload file for mediaType=PHOTO and size=" + size, e);
            }
        }

        String videoKey = null;
        if (null != postForm.getVideoStream()) {
            VideoMedia.Format fmt =
                VideoMedia.Format.valueOf(postForm.getVideoFormat());
            long size = postForm.getVideoSize();
            try {
                videoKey =
                    this.fileMgr.store(
                        postForm.getVideoStream(),
                        MediaType.VIDEO,
                        fmt.getContentType(),
                        size,
                        null,
                        fmt.getExtension());
            }
            catch (IOException e) {
                throw new WebApplicationException(
                    "Fail to upload file for mediaType=VIDEO and size=" + size, e);
            }
        }
        
        if (null != audioKey) {
            AudioMedia.Format fmt =
                AudioMedia.Format.valueOf(postForm.getAudioFormat());
            AudioMedia media = new AudioMedia();
            media.setDuration(postForm.getAudioDuration());
            media.setFormat(fmt);
            media.setUrl(audioKey);
            post.addMedia(media);

            IconMedia audioBackground = new IconMedia();
            int i1 = RandomUtils.nextInt(0, Medias.AUDIO_BACK_SYSTEM_IDS.length);
            int i2 = RandomUtils.nextInt(0, Medias.AUDIO_BACK_COLOR_HEXES.length);
            audioBackground.setSystemId(Medias.AUDIO_BACK_SYSTEM_IDS[i1]);
            audioBackground.setColorHex(Medias.AUDIO_BACK_COLOR_HEXES[i2]);
            post.addMedia(audioBackground);
        }
        else if (null != postForm.getLink()) {
            LinkMedia.Format fmt =
                LinkMedia.Format.valueOf(postForm.getLinkEmbedItemFormat());
            LinkMedia media = new LinkMedia();
            media.setEmbedItemFormat(fmt);
            media.setEmbedItemHeight(postForm.getLinkEmbedItemHeight());
            media.setEmbedItemWidth(postForm.getLinkEmbedItemWidth());
            media.setEmbedItemUrl(postForm.getLinkEmbedItemUrl());
            media.setHost(Medias.resolveHostFromUrl(postForm.getLink()));
            media.setTitle(postForm.getLinkTitle());
            media.setDescription(postForm.getLinkDescription());
            media.setUrl(postForm.getLink());
            post.addMedia(media);
        }
        else if (post.getType() == PostType.PHOTO && null != photoKey) {
            // We might also upload photo for video, check the following logic
            PhotoMedia.Format fmt =
                PhotoMedia.Format.valueOf(postForm.getPhotoFormat());
            PhotoMedia media = new PhotoMedia();
            media.setFormat(fmt);
            media.setLarge(photoKey);
            media.setLargeHeight(postForm.getPhotoHeight());
            media.setLargeWidth(postForm.getPhotoWidth());
            media.setUploaded(postForm.isPhotoUploaded());
            post.addMedia(media);
        }
        else if (null != postForm.getText()) {
            TextMedia.Format fmt =
                TextMedia.Format.valueOf(postForm.getTextFormat());
            TextMedia media = new TextMedia();
            media.setFormat(fmt);
            media.setText(postForm.getText());
            media.setTextBackgroundColor(postForm.getTextBackgroundColor());
            media.setTextColor(postForm.getTextColor());
            post.addMedia(media);
        }
        else if (null != videoKey) {
            VideoMedia.Format fmt =
                VideoMedia.Format.valueOf(postForm.getVideoFormat());
            VideoMedia media = new VideoMedia();
            media.setFormat(fmt);
            media.setUrl(videoKey);
            post.addMedia(media);

            // NOTE, when creating a video, we pass the video thumbnail as photodata in form
            if (photoKey != null) {
                PhotoMedia videoThumbnailMedia = new PhotoMedia();
                videoThumbnailMedia.setFormat(PhotoMedia.Format.valueOf(postForm.getPhotoFormat()));
                videoThumbnailMedia.setLarge(photoKey);
                videoThumbnailMedia.setLargeHeight(postForm.getPhotoHeight());
                videoThumbnailMedia.setLargeWidth(postForm.getPhotoWidth());
                videoThumbnailMedia.setUploaded(false);
                post.addMedia(videoThumbnailMedia);
            }
        }
    }
}
