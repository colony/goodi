
package gd.impl.post;


/**
 * Enum describing a post class. Different than PostType that
 * defines how client display and interact the post content (media),
 * the class totally segregate posts, ideally, posts of different
 * class SHOULD NOT be shown together. Hence, technically speaking,
 * PostClass is higher dimension than group
 */
public enum PostClass
{
    /**
     * Class of posts visible in announcement section only
     */
    ANNOUNCEMENT,

    /**
     * Class of posts visible on all general-purpose
     * recent-or-popular feeds (including nearby, global, hashtag,
     * group, etc)
     */
    PUBLIC,

    /**
     * Class of posts visible in a thread only
     */
    THREAD,

    ;
}
