
package gd.impl.post;

import gd.impl.util.AbstractChainableFilter;
import gd.impl.util.Filter;

/**
 * File one particular post ID
 */
public class PostIdFilter
    extends
        AbstractChainableFilter<Post>
    implements
        Filter<Post>
{
    private long id;

    private Post filteredPost;
    

    public PostIdFilter(
        long id)
    {
        this.id = id;
    }


    @Override
    public boolean accept(
        Post p)
    {
        if (p == null)
            return false; // Never accept null
        
        if (p.getId() == this.id) {
            this.filteredPost = p;
            return false;
        }
        
        if (this.nextFilter != null && !this.nextFilter.accept(p))
            return false; // Chained filter
        
        return true;
    }
    
    
    public Post getFilteredPost()
    {
        return this.filteredPost;
    }
}
