
package gd.impl.post;


import gd.api.v1.LatLng;
import gd.api.v1.PaginationRep;
import gd.api.v1.admin.ManagePostForm;
import gd.api.v1.admin.ManagePostResource;
import gd.api.v1.comment.CommentRep;
import gd.api.v1.comment.CommentRepList;
import gd.api.v1.post.FlagRep;
import gd.api.v1.post.FlagRepList;
import gd.api.v1.post.Geo;
import gd.api.v1.post.MediaType;
import gd.api.v1.post.PostRep;
import gd.api.v1.post.PostRepList;
import gd.api.v1.post.PostSort;
import gd.api.v1.post.PostType;
import gd.api.v1.post.ThreadType;
import gd.impl.Reps;
import gd.impl.Rests;
import gd.impl.activity.Activities;
import gd.impl.comment.Comment;
import gd.impl.comment.CommentRepository;
import gd.impl.config.DetailedConfigs;
import gd.impl.group.Group;
import gd.impl.group.GroupCount;
import gd.impl.group.GroupRepository;
import gd.impl.group.Groups;
import gd.impl.media.AudioMedia;
import gd.impl.media.FileManager;
import gd.impl.media.IconMedia;
import gd.impl.media.LinkMedia;
import gd.impl.media.Media;
import gd.impl.media.Medias;
import gd.impl.media.PhotoMedia;
import gd.impl.media.TextMedia;
import gd.impl.media.VideoMedia;
import gd.impl.post.PostCount.Field;
import gd.impl.queue.Event;
import gd.impl.queue.EventQueue;
import gd.impl.queue.EventType;
import gd.impl.queue.QueueManager;
import gd.impl.queue.QueueType;
import gd.impl.relation.RL.LEntry;
import gd.impl.relation.RL.LLRelation;
import gd.impl.relation.RL.LSRelation;
import gd.impl.relation.RL.SEntry;
import gd.impl.relation.RL.SSRelation;
import gd.impl.relation.RelationManager;
import gd.impl.relation.RelationType;
import gd.impl.relation.Relations;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.user.User;
import gd.impl.user.UserStatus;
import gd.impl.user.Users;
import gd.impl.util.Filter;
import gd.impl.util.GDUtils;
import gd.impl.util.Page;
import gd.impl.util.PageParam;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;
import gd.impl.util.UpdateConflictException;
import gd.support.jersey.BoolParam;
import gd.support.jersey.LongParam;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Path("/adm/posts")
@Singleton
public class ManagePostResourceImpl
    implements
        ManagePostResource
{
    private static final Logger LOG = LoggerFactory.getLogger("admin");
    
    protected SSRelation anythingRel;

    protected CommentRepository cmtRepo;

    protected EventQueue eventQ;

    protected FileManager fileMgr;

    protected GroupRepository groupRepo;

    protected LSRelation hasPostMetaRel;

    protected LLRelation hasThreadChildrenRel;

    protected PostRepository postRepo;


    public ManagePostResourceImpl()
    {
        RepositoryManager repoMgr = RepositoryManager.instance();
        this.cmtRepo = repoMgr.get(RepositoryType.COMMENT, CommentRepository.class);
        this.groupRepo = repoMgr.get(RepositoryType.GROUP, GroupRepository.class);
        this.postRepo = repoMgr.get(RepositoryType.POST, PostRepository.class);
        this.fileMgr = FileManager.instance();

        RelationManager relMgr = RelationManager.instance();
        this.anythingRel = relMgr.get(RelationType.ANYTHING, SSRelation.class);
        this.hasPostMetaRel = relMgr.get(RelationType.HAS_POST_METADATA, LSRelation.class);
        this.hasThreadChildrenRel = relMgr.get(RelationType.HAS_THREAD_CHILDREN, LLRelation.class);

        QueueManager qMgr = QueueManager.instance();
        this.eventQ = qMgr.get(QueueType.EVENT, EventQueue.class);
    }


    @Override
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response createOrModifyPost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam ManagePostForm postForm)
    {
        String desc = (postForm.getId() != null) ? ("modify:" + postForm.getId()) : ("create:" + postForm.getOwnerId());
        Rests.beginAdminOperation(headers, "adm:mpost:" + desc);

        // Validation
        postForm.validate();
        long groupId = postForm.getGroupId(); //default 1L
        Group group = null;
        if (groupId != 0L) {
            // NOTE, we might create post under special group 0, for example, when we create
            // ANNOUNCEMENT post, we will first create post under group 0 then set its status
            // to ANNOUNCEMENT
            group = Groups.get(groupId);
            GDUtils.check(group != null, Status.NOT_FOUND, "No group with ID=" + groupId);
        }

        Post returnPost = null;
        Long id = postForm.getId();

        boolean isUpdate = (id != null);
        if (isUpdate)
        {
            // Update
            Post post = this.postRepo.get(id);
            GDUtils.check(post != null, Status.NOT_FOUND, "Post not found ID=" + id);

            PostType pt = PostType.valueOf(StringUtils.defaultIfEmpty(postForm.getType(), "PHOTO"));
            if (post.getType() != pt)
                return Response.status(Status.BAD_REQUEST)
                    .entity(
                        "Update a post but type mismatches, get \"" + pt + "\" but expect \"" + post.getType() + "\"")
                    .build();

            // Update groupId
            long oldGroupId = post.getGroupId();
            post.setGroupId(groupId);
            if (oldGroupId != groupId && group != null && group.isPrivate()) {
                // For now, we don't allow, move a post from non-private group to private group!
                return Response.status(Status.BAD_REQUEST)
                    .entity(
                        "Move from non-private group \"" + oldGroupId + "\" to private group \"" + groupId
                            + "\" not allowed")
                    .build();
            }

            // Update threadId (not allowed)
            Long oldThreadId = post.getThreadId();
            Long threadId = postForm.getThreadId();
            if (oldThreadId != threadId) {
                // For now, we don't allow move a post from one thread to another
                return Response.status(Status.BAD_REQUEST)
                    .entity("Move from thread \"" + oldThreadId + "\" to \"" + threadId + "\" not allowed")
                    .build();
            }

            // Only update not-null fields of the form

            if (StringUtils.isNotEmpty(postForm.getBody())) {
                post.setBody(postForm.getBody());
            }

            // Update geo
            if (null != postForm.getGeo())
                post.setGeo(postForm.getGeo());

            // Update media
            List<Media> replacedMedias = this.replaceMediaToPost(post, postForm);

            try
            {
                returnPost = this.postRepo.update(post);

                if (null != postForm.getGeo())
                    this.postRepo.updateGeo(id, postForm.getGeo());
            }
            catch (UpdateConflictException e)
            {
                throw new WebApplicationException(
                    "Fail to update post ID=" + id,
                    e);
            }

            // Remove old asset if any
            if (replacedMedias != null) {
                for (Media replacedMedia : replacedMedias) {
                    if (replacedMedia instanceof AudioMedia) {
                        this.fileMgr.deleteQuietly(((AudioMedia) replacedMedia).getUrl());
                    }
                    else if (replacedMedia instanceof PhotoMedia) {
                        this.fileMgr.deleteQuietly(((PhotoMedia) replacedMedia).getLarge());
                        this.fileMgr.deleteQuietly(((PhotoMedia) replacedMedia).getMedium());
                        this.fileMgr.deleteQuietly(((PhotoMedia) replacedMedia).getThumbnail());
                    }
                    else if (replacedMedia instanceof VideoMedia) {
                        this.fileMgr.deleteQuietly(((VideoMedia) replacedMedia).getUrl());
                    }
                }
            }

            // Reassign the group if different
            if (oldGroupId != groupId)
                Groups.assignGroup(id, groupId);
        }
        else
        {
            // Create
            if (null == postForm.getOwnerId())
                return Response.status(Status.BAD_REQUEST)
                    .entity("A fake user ID is needed to create a post")
                    .build();
            User user = Users.get(postForm.getOwnerId());
            if (null == user || user.getStatus() == null
                || !user.getStatus().isSetAny(UserStatus.Status.ADMIN, UserStatus.Status.FAKE))
            {
                return Response.status(Status.BAD_REQUEST)
                    .entity("The specified user ID \""
                        + postForm.getOwnerId()
                        + "\" doesn't exist or is not FAKE")
                    .build();
            }

            // Prepare the post
            Post post = new Post();
            post.setBody(postForm.getBody());
            post.setCreateTime(postForm.getCreateTime());
            post.setClazz(PostClass.PUBLIC);
            post.setGroupId(groupId);
            post.setOwnerId(postForm.getOwnerId());
            if (user.getStatus().isSet(UserStatus.Status.FAKE)) {
                PostStatus ps = new PostStatus();
                ps.set(PostStatus.Status.FAKE, true);
                post.setStatus(ps);
            }
            post.setType(PostType.valueOf(StringUtils.defaultIfEmpty(postForm.getType(), "PHOTO")));
            post.setUpdateTime(postForm.getCreateTime());

            // Prepare the media
            this.addMediaToPost(post, postForm);

            // Private group, covert geo
            String covertGeoName = null;
            if (group != null && group.isPrivate()) {
                covertGeoName = DetailedConfigs.getRandomGeoCovertWord(groupId);
                postForm.getGeo().setName(covertGeoName);
            }
            post.setGeo(postForm.getGeo());

            // Creat a thread post instead
            long participateThreadId = 0L;
            if (postForm.getThreadId() != null) {
                participateThreadId = postForm.getThreadId();
                GDUtils.check(
                    Posts.existThread(participateThreadId),
                    Status.NOT_FOUND,
                    "No thread with ID=" + participateThreadId);
                post.setClazz(PostClass.THREAD);
                post.setThreadId(participateThreadId);
                //=======HACK========
                post.setGroupId(groupId * -1);
                //=======HACK========
            }

            long createdThreadId = 0L;
            ThreadType createdThreadType = null;
            if (StringUtils.isNotEmpty(postForm.getCreateThreadType())) {
                createdThreadType = ThreadType.valueOf(postForm.getCreateThreadType());
                createdThreadId = Posts.createThreadId(createdThreadType);
                post.setThreadId(createdThreadId);
            }

            // ========CREATE POST========
            returnPost = Posts.createPost(post, covertGeoName != null);
            id = returnPost.getId();
            // ========CREATE POST========

            if (groupId > 1L)
                this.groupRepo.incrCount(groupId, GroupCount.Field.POST, 1L);

            if (group != null && group.isPrivate()) {
                this.hasPostMetaRel.addEdge(
                    returnPost.getId(),
                    new SEntry(Relations.HPM_COVERT_GEO).with(covertGeoName));
            }
            
            if (participateThreadId > 0L) {
                this.hasThreadChildrenRel.addEdge(participateThreadId,id);
            }

            if (createdThreadId > 0L) {
                if (StringUtils.isNotEmpty(postForm.getCreateThreadData())) {
                    this.hasThreadChildrenRel.addSource(
                        new LEntry(createdThreadId)
                            .with(id, returnPost.getOwnerId())
                            .with(createdThreadType.name(), postForm.getCreateThreadData()));
                }
            }

            if (postForm.getIsFeatured().get()) {
                this.anythingRel.addEdge(
                    Relations.ANYTHING_FEATURED_FEED, 
                    new SEntry(String.valueOf(id))
                    .with(postForm.getFeaturedTitle())
                    .with(postForm.getIsFeaturedTitleFormat().get() ? 1L : 0L));
            }
        }

        // Counters are only delta
        if (postForm.hasNumFlagsDelta())
            this.postRepo.incrCount(id, Field.FLAG, postForm.getNumFlagsDelta());
        if (postForm.hasNumDownvotesDelta())
            this.postRepo.incrCount(id, Field.DOWNVOTE, postForm.getNumDownvotesDelta());
        if (postForm.hasNumSharesDelta())
            this.postRepo.incrCount(id, Field.SHARE, postForm.getNumSharesDelta());
        if (postForm.hasNumUpvotesDelta())
            this.postRepo.incrCount(id, Field.UPVOTE, postForm.getNumUpvotesDelta());

        if (!isUpdate) {
            Event event = new Event(EventType.CREATE_POST);
            event.bool1 = true;
            event.lng1 = 1L;
            event.lng2 = returnPost.getId();
            event.str1 = returnPost.getType().name();
            this.eventQ.enq(event);
        }
        else {
            Event event = new Event(EventType.ADMIN_UPDATE_POST);
            event.bool1 = postForm.hasDelta();
            event.lng1 = id;
            this.eventQ.enq(event);
        }

        if (postForm.hasNumDownvotesDelta() || postForm.hasNumUpvotesDelta()) {
            // Everytime upvote/downvote changes from admin console, we treat that as ONE voter 
            Activities.asyncLogVotePost(1L, id);
        }

        PostRep rep = new PostRep();
        Reps.populatePostAdmin(rep, returnPost);
        return Response.ok(rep).build();
    }


    @Override
    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deletePost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam idParam)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:delete:" + idParam.get());
        Posts.deletePost(idParam.get());
        return Response.ok().build();
    }


    @Override
    @DELETE
    @Path("/group/{id}")
    @Produces("application/json")
    public Response deletePostsByGroup(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") LongParam groupIdParam,
        @QueryParam("exclude_ids") String excludeIdsStr)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:deletePostsByGroup:" + groupIdParam.get());
        long groupId = groupIdParam.get();

        if (groupId <= 1) {
            LOG.warn("Deny delete posts from group <= 1, too dangerous");
            return Response.ok().build();
        }

        List<String> excludeIds = new ArrayList<>();
        if (StringUtils.isNotEmpty(excludeIdsStr)) {
            excludeIds = Arrays.asList(excludeIdsStr.split(","));
        }

        final List<String> excludeIdsFinal = excludeIds;

        //TODO: hack for now
        String sql = "SELECT id FROM post WHERE group_id = " + groupId;
        SqliteUtils.executeSelectQuery(
            sql,
            null,
            "selectPostIdsFromGroup",
            new ResultSetParser() {
                @Override
                public void parse(
                    ResultSet rs)
                    throws SQLException
                {
                    while (rs.next()) {
                        long postId = rs.getLong("id");
                        if (postId <= 0)
                            continue;

                        if (excludeIdsFinal.contains(String.valueOf(postId)))
                            continue;

                        Posts.deletePost(postId);
                    }
                }
            });
        return Response.ok().build();
    }


    @Override
    @GET
    @Path("/flags")
    @Produces("application/json")
    public Response getFlagList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:getFlagList");
        Page p = pageParam.get();

        List<Flag> flagList = this.postRepo.findFlags(p, null);

        FlagRepList repList = new FlagRepList();
        for (Flag flag : flagList)
        {
            FlagRep rep = new FlagRep();
            rep.complaintMessage = flag.getComplainMessage();
            rep.createTime = flag.getCreateTime();
            rep.id = flag.getId();
            rep.postId = flag.getPostId();
            rep.userId = flag.getUserId();

            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(flagList),
                GDUtils.getLast(flagList),
                uriInfo);
        }

        return Response.ok(repList).build();
    }


    @Override
    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getPost(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:getPost:" + idParam.get());
        long postId = idParam.get();
        if (postId <= 0)
        {
            return Response.status(Status.BAD_REQUEST)
                .entity("Missing post ID")
                .build();
        }

        Post post = this.postRepo.get(postId);
        GDUtils.check(
            post != null,
            Status.NOT_FOUND,
            "No post with ID " + postId);

        PostRep rep = new PostRep();
        Reps.populatePostAdmin(rep, post);

        return Response.ok(rep).build();
    }


    @Override
    @GET
    @Path("/{id}/comments")
    @Produces("application/json")
    public Response getPostCommentList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:getPostCmtList:" + idParam.get());
        long postId = idParam.get();
        if (postId <= 0)
            return Response.status(Status.BAD_REQUEST)
                .entity("Missing post ID")
                .build();
        GDUtils.check(
            this.postRepo.exists(postId),
            Status.NOT_FOUND,
            "No post with ID " + postId);

        Page p = pageParam.get();
        List<Comment> cmtList = this.cmtRepo.findByPost(postId, p, null, false);
        CommentRepList repList = new CommentRepList();
        for (Comment cmt : cmtList)
        {
            CommentRep rep = new CommentRep();
            Reps.populateCommentAdmin(rep, cmt);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(cmtList),
                GDUtils.getLast(cmtList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }


    @Override
    @GET
    @Path("/{id}/flags")
    @Produces("application/json")
    public Response getPostFlagList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:getPostFlagList:" + idParam.get());
        long postId = idParam.get();
        if (postId <= 0)
        {
            return Response.status(Status.BAD_REQUEST)
                .entity("Missing post ID")
                .build();
        }

        List<Flag> flagList = this.postRepo.findFlagsByPost(postId);
        FlagRepList repList = new FlagRepList();
        for (Flag flag : flagList)
        {
            FlagRep rep = new FlagRep();
            rep.complaintMessage = flag.getComplainMessage();
            rep.createTime = flag.getCreateTime();
            rep.id = flag.getId();
            rep.postId = flag.getPostId();
            rep.userId = flag.getUserId();

            repList.add(rep);
        }

        return Response.ok(repList).build();
    }


    @Override
    @GET
    @Produces("application/json")
    public Response getPostList(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @QueryParam("geo") Geo geo,
        @QueryParam("group_id") LongParam groupIdParam,
        @BeanParam PageParam pageParam)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:getPostList");
        Page p = pageParam.get();

        List<Post> postList = null;
        Filter<Post> filter = null;

        String sortKey = StringUtils.defaultIfBlank(p.key, "id");
        PostSort sort = PostSort.fromAbbreviation(sortKey.toLowerCase());
        switch (sort)
        {
        case GLOBAL_POPULAR:
        case GLOBAL_RECENT:
            filter = new SpecialPostStatusFilter(
                PostStatus.Status.ANNOUNCEMENT,
                PostStatus.Status.GEO_RESTRICTED);
            List<Long> filterGroupIds = DetailedConfigs.getGlobalFeedFilterGroupIds();
            if (!filterGroupIds.isEmpty()) {
                filter.chainFilter(new PostGroupFilter(filterGroupIds));
            }
            postList = this.postRepo.find(p, filter);
            break;

        case GROUP_POPULAR:
        case GROUP_RECENT:
            if (groupIdParam == null)
                return Response.status(Status.BAD_REQUEST).entity("Miss groupId for sortkey " + sort).build();
            long groupId = groupIdParam.get();
            Group group = Groups.get(groupId);
            GDUtils.check(
                group != null,
                Status.NOT_FOUND,
                "No group with ID=" + groupId);
            postList = this.postRepo.findByGroup(groupId, p, null);
            break;

        case ID:
            postList = this.postRepo.find(p, null);
            break;

        case NEARBY_POPULAR:
        case NEARBY_RECENT:
            if (geo == null)
                return Response.status(Status.BAD_REQUEST).entity("Miss geo for sortkey " + sort).build();
            filter = new SpecialPostStatusFilter(PostStatus.Status.ANNOUNCEMENT);
            LatLng latlng = geo.toLatLng();
            postList = this.postRepo.findByLatLng(latlng, p, filter);
            break;

        default:
            return Response.status(Status.BAD_REQUEST).entity("Unrecognized sortkey " + sort).build();
        }

        PostRepList repList = new PostRepList();
        for (Post post : postList)
        {
            PostRep rep = new PostRep();
            Reps.populatePostAdmin(rep, post);
            repList.add(rep);
        }

        if (repList.hasData())
        {
            repList.pagination = new PaginationRep();
            Reps.populatePagination(
                repList.pagination,
                p,
                GDUtils.getFirst(postList),
                GDUtils.getLast(postList),
                uriInfo);
        }
        return Response.ok(repList).build();
    }


    @Override
    @POST
    @Path("/{id}/status")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response setPostStatus(
        @Context HttpHeaders headers,
        @Context UriInfo uriInfo,
        @PathParam("id") @DefaultValue("-1") LongParam idParam,
        @FormParam("status") PostStatus.Status status,
        @FormParam("unset") @DefaultValue("false") BoolParam unsetParam)
    {
        Rests.beginAdminOperation(headers, "adm:mpost:setPostStatus:" + status.name() + ":" + unsetParam.get());
        long postId = idParam.get();
        Post post = this.postRepo.get(postId);
        if (post == null)
            return Response.status(Status.NOT_FOUND).entity("Post not found id=" + postId).build();

        try
        {
            Posts.setStatus(postId, status, unsetParam.get());
            if (status == PostStatus.Status.BLOCKED) {
                Groups.assignBlockGroup(postId, !unsetParam.get());
            }
        }
        catch (UpdateConflictException e)
        {
            LOG.error("Fail to update the post id={}", postId, e);
            return Response.status(Status.CONFLICT).build();
        }
        return Response.ok().build();
    }


    //
    // INTERNAL METHOD
    //

    private void addMediaToPost(
        Post post,
        ManagePostForm postForm)
    {
        if (null != postForm.getAudioStream())
        {
            AudioMedia.Format fmt =
                AudioMedia.Format.valueOf(postForm.getAudioFormat());

            String audioKey = null;
            long size = postForm.getAudioSize();
            try
            {
                //TODO
                // For now, the IOS client uses AVPlayer, it ONLY works
                // If there's extension in URL
                audioKey =
                    this.fileMgr.store(
                        postForm.getAudioStream(),
                        MediaType.AUDIO,
                        fmt.getContentType(),
                        size,
                        null,
                        fmt.getExtension());
            }
            catch (IOException e)
            {
                throw new WebApplicationException(
                    "Fail to upload file for mediaType=AUDIO and size="
                        + size,
                    e);
            }

            AudioMedia media = new AudioMedia();
            media.setDuration(postForm.getAudioDuration());
            media.setFormat(fmt);
            media.setUrl(audioKey);
            post.addMedia(media);

            IconMedia audioBackground = new IconMedia();
            int i1 = RandomUtils.nextInt(0, Medias.AUDIO_BACK_SYSTEM_IDS.length);
            int i2 = RandomUtils.nextInt(0, Medias.AUDIO_BACK_COLOR_HEXES.length);
            audioBackground.setSystemId(Medias.AUDIO_BACK_SYSTEM_IDS[i1]);
            audioBackground.setColorHex(Medias.AUDIO_BACK_COLOR_HEXES[i2]);
            post.addMedia(audioBackground);
        }
        else if (post.getType() == PostType.PHOTO && null != postForm.getPhotoStream())
        {
            PhotoMedia.Format fmt =
                PhotoMedia.Format.valueOf(postForm.getPhotoFormat());

            String photoKey = null;
            long size = postForm.getPhotoSize();
            try
            {
                photoKey =
                    this.fileMgr.store(
                        postForm.getPhotoStream(),
                        MediaType.PHOTO,
                        fmt.getContentType(),
                        size,
                        null,
                        null);
            }
            catch (IOException e)
            {
                throw new WebApplicationException(
                    "Fail to upload file for mediaType=PHOTO and size="
                        + size,
                    e);
            }

            PhotoMedia media = new PhotoMedia();
            media.setFormat(fmt);
            media.setLarge(photoKey);
            media.setLargeHeight(postForm.getPhotoHeight());
            media.setLargeWidth(postForm.getPhotoWidth());
            media.setUploaded(postForm.getPhotoUploaded().get());
            post.addMedia(media);
        }
        else if (null != postForm.getText())
        {
            TextMedia.Format fmt =
                TextMedia.Format.valueOf(postForm.getTextFormat());
            TextMedia media = new TextMedia();
            media.setFormat(fmt);
            media.setText(postForm.getText());
            media.setTextBackgroundColor(Medias.getRandomAudioBackColorHex());
            media.setTextColor("ffffff");
            post.addMedia(media);
        }
        else if (null != postForm.getLink())
        {
            LinkMedia.Format fmt =
                LinkMedia.Format.valueOf(postForm.getLinkEmbedItemFormat());
            LinkMedia media = new LinkMedia();
            media.setEmbedItemFormat(fmt);
            media.setEmbedItemHeight(postForm.getLinkEmbedItemHeight());
            media.setEmbedItemWidth(postForm.getLinkEmbedItemWidth());
            media.setEmbedItemUrl(postForm.getLinkEmbedItemUrl());
            media.setHost(Medias.resolveHostFromUrl(postForm.getLink()));
            media.setTitle(postForm.getLinkTitle());
            media.setUrl(postForm.getLink());
            post.addMedia(media);
        }
        else if (null != postForm.getVideoStream())
        {
            VideoMedia.Format fmt =
                VideoMedia.Format.valueOf(postForm.getVideoFormat());
            String videoKey = null;
            long size = postForm.getVideoSize();
            try {
                videoKey =
                    this.fileMgr.store(
                        postForm.getVideoStream(),
                        MediaType.VIDEO,
                        fmt.getContentType(),
                        size,
                        null,
                        fmt.getExtension());
            }
            catch (IOException e) {
                throw new WebApplicationException(
                    "Fail to upload file for mediaType=VIDEO and size=" + size, e);
            }

            VideoMedia media = new VideoMedia();
            media.setFormat(fmt);
            media.setUrl(videoKey);
            post.addMedia(media);

            PhotoMedia.Format fmt2 =
                PhotoMedia.Format.valueOf(postForm.getPhotoFormat());

            String photoKey = null;
            long size2 = postForm.getPhotoSize();
            try
            {
                photoKey =
                    this.fileMgr.store(
                        postForm.getPhotoStream(),
                        MediaType.PHOTO,
                        fmt2.getContentType(),
                        size2,
                        null,
                        null);
            }
            catch (IOException e)
            {
                throw new WebApplicationException(
                    "Fail to upload file for mediaType=PHOTO and size="
                        + size,
                    e);
            }

            PhotoMedia videoThumbnailMedia = new PhotoMedia();
            videoThumbnailMedia.setFormat(fmt2);
            videoThumbnailMedia.setLarge(photoKey);
            videoThumbnailMedia.setLargeHeight(postForm.getPhotoHeight());
            videoThumbnailMedia.setLargeWidth(postForm.getPhotoWidth());
            videoThumbnailMedia.setUploaded(false);
            post.addMedia(videoThumbnailMedia);
        }
    }


    /**
     * Replace the post with new media data by form
     * 
     * @return The old media of the post if any
     */
    private List<Media> replaceMediaToPost(
        Post post,
        ManagePostForm postForm)
    {
        List<Media> oldMedias = null;

        switch (post.getType())
        {
        case AUDIO:
            if (null != postForm.getAudioStream()) {
                oldMedias = post.getMedias();
                post.setMedias(null);
                this.addMediaToPost(post, postForm);
            }
            break;

        case LINK:
            if (null != postForm.getLink()) {
                oldMedias = post.getMedias();
                post.setMedias(null);
                this.addMediaToPost(post, postForm);
            }
            break;

        case PHOTO:
            if (null != postForm.getPhotoStream()) {
                oldMedias = post.getMedias();
                post.setMedias(null);
                this.addMediaToPost(post, postForm);
            }
            else {
                // This is the special case, the admin client might not want to
                // replace the photo file but just toggle photoUploaded field
                if (post.hasMedias()) {
                    PhotoMedia pm = post.getMedia(0, PhotoMedia.class);
                    pm.setUploaded(postForm.getPhotoUploaded().get());
                }
            }
            break;

        case TEXT:
            TextMedia oldMedia = post.getMedia(0, TextMedia.class);
            if (!oldMedia.getText().equals(postForm.getText()))
            {
                oldMedias = post.getMedias();
                post.setMedias(null);
                this.addMediaToPost(post, postForm);
            }
            break;

        case VIDEO:
            if (null != postForm.getVideoStream()) {
                oldMedias = post.getMedias();
                post.setMedias(null);
                this.addMediaToPost(post, postForm);
            }

        default:
            break;
        }

        return oldMedias;
    }
}
