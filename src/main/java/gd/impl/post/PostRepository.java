
package gd.impl.post;


import gd.api.v1.LatLng;
import gd.api.v1.post.Geo;
import gd.impl.repository.Repository;
import gd.impl.repository.RepositoryAdmin;
import gd.impl.util.Filter;
import gd.impl.util.Page;

import java.util.List;


public interface PostRepository
    extends
        Repository<Long,Post>,
        RepositoryAdmin
{
    /**
     * A list of predefined page (sort) key for pagination and
     * different sorting, Check {@link PostSort}
     */

    /** Sort Key to paginate flags */
    public static final String FLAG_SK = "ID";


    /**
     * Same as {@link #add}, the difference is the geo will be
     * persisted, but the post won't be added into geo index so that
     * local feed can't locate the post. This is mainly used for
     * cases that the post is created for a private group, requiring
     * the geo of post is kept secret
     * 
     * @param p
     *            The post to be added
     * @return The created post
     */
    public Post addWithoutGeo(
        Post p);


    /**
     * Add a flag post (complaint statement) on a post
     * 
     * @param fp
     *            The specified flag content
     * @return The created flag post if succeed, otherwise null
     */
    public Flag addFlag(
        Flag fp);


    /**
     * Retrieve post related counters
     * 
     * @param postId
     *            ID of the post whose counts are fetched
     */
    public PostCount getCount(
        Long postId);


    /**
     * Increment a particular count of the post specified by postId
     * 
     * @param postId
     *            ID of the post whose counts will be updated
     * @param count
     *            The count field being updated
     * @param incr
     */
    public void incrCount(
        Long postId,
        PostCount.Field count,
        long incr);


    /**
     * Retrieve a list of posts (GLOBALLY) with specified pagination
     * and filter
     * 
     * @param page
     * @param filter
     * @return
     */
    public List<Post> find(
        Page page,
        Filter<Post> filter);


    /**
     * Retrieve a list of posts belong to a specific group. And
     * also, take account into the specified pagination and filter
     * 
     * @param groupId
     * @param page
     * @param filter
     * @return
     */
    public List<Post> findByGroup(
        Long groupId,
        Page page,
        Filter<Post> filter);


    /**
     * Retrieve a list of posts nearby the specified geo. And also,
     * take account into the specified pagination and filter
     * 
     * @param geo
     * @param page
     * @param filter
     * @return
     */
    public List<Post> findByLatLng(
        LatLng latlng,
        Page page,
        Filter<Post> filter);


    /**
     * Retrieve a list of posts of the specified owner. And also,
     * take account into the specified pagination and filter
     * 
     * @param ownerId
     * @param page
     * @param filter
     * @return
     */
    public List<Post> findByOwner(
        long ownerId,
        Page page,
        Filter<Post> filter);


    /**
     * Retrieve a list of flags (GLOBALLY) with specified pagination
     * and filter
     * 
     * @param page
     * @param filter
     * @return
     */
    public List<Flag> findFlags(
        Page page,
        Filter<Flag> filter);


    /**
     * Retrieve a list of flags reported to the specified post.
     * 
     * @param postId
     * @return
     */
    public List<Flag> findFlagsByPost(
        long postId);


    /**
     * Update the internal geo index for the specified post. Note,
     * it won't change the geo (used for display) of post object.
     * Usually, caller should call {@link #update(Post)} first to
     * update geo of post then call this method
     */
    public void updateGeo(
        long postId,
        Geo geo);


    public void updateScore(
        long postId,
        double score);
}
