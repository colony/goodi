
package gd.impl.post;


import gd.impl.user.Users;
import gd.impl.util.AbstractChainableFilter;
import gd.impl.util.Filter;

import java.util.HashSet;
import java.util.Set;


/**
 * Filter posts that have been flagged by viewer once 
 */
public class FlagPostFilter
    extends
        AbstractChainableFilter<Post>
    implements
        Filter<Post>
{
    /**
     * A set of post ids that has been flagged by this viewer
     */
    private HashSet<Long> ids;


    public FlagPostFilter(
        long viewerId)
    {
        ids = new HashSet<>();
        ids.addAll(Users.getFlaggedPostIds(viewerId));
    }


    @Override
    public boolean accept(
        Post p)
    {
        if (p == null)
            return false; // Never accept null

        if (ids.contains(p.getId()))
            return false; // The viewer has flagged it

        if (this.nextFilter != null && !this.nextFilter.accept(p))
            return false; // Chained filter

        return true;
    }
    
    
    public Set<Long> getIds()
    {
        return ids;
    }
    
    
    public boolean hasIds()
    {
        return ids != null && !ids.isEmpty();
    }
}
