
package gd.impl.post;


import gd.api.v1.post.MediaType;
import gd.api.v1.post.PostType;
import gd.impl.media.Media;
import gd.impl.media.PhotoMedia;
import gd.impl.media.TextMedia;
import gd.support.protobuf.PostProtos.PostProto;
import gd.support.protobuf.PostProtos.PostProto.Media.Builder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.Conversion;

import com.google.common.collect.AbstractIterator;


public class PostExporter
{
    public static Iterator<Post> read(
        InputStream in)

        throws IOException
    {
        int v = readHeader(in);
        switch (v)
        {
        case 1:
            return new V1Iterator(in);

        default:
            throw new IllegalStateException(
                "Unsupported version number: " + v);
        }
    }


    public static void write(
        OutputStream out,
        Iterator<Post> it)

        throws IOException
    {
        writeHeader(out);

        PostProto.Builder b = PostProto.newBuilder();
        while (it.hasNext())
        {
            Post p = it.next();
            if (p == null)
                continue;

            //
            // Flatten the media info into a serialized list of medias, per type, each position of 
            // the serialized list will hold a particular field, in case of NULL value, a special
            // placeholder with type (string) = "NULL" is stored, the RULES are:
            // SIMPLE: 
            ///  origin(0), sting(1)
            //

            // Common fields
            b.setBody(p.getBody());
            b.setCreateTime(p.getCreateTime().getTime());
            b.setId(p.getId());
            b.setOwnerId(p.getOwnerId());
            b.setType(p.getType().name());
            b.setUpdateTime(p.getUpdateTime().getTime());

//            switch (p.getType())
//            {
//            case SIMPLE:
                //TODO
          //      writeMedia(b, p.getPhoto());
//                break;
//
//            default:
//                throw new IOException("Unrecognized post type \""
//                    + p.getType() + "\"");
//            }

            b.build().writeDelimitedTo(out);

            // Clear inner state of builder before next loop
            b.clear();
        }
    }


    //
    // INTERNAL METHODS
    //


    private static int readHeader(
        InputStream in)

        throws IOException
    {
        byte[] b = new byte[4];
        in.read(b);
        int magic = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (magic != HEADER_MAGIC)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid magic number 0x%08x, expected 0x%08x",
                    magic,
                    HEADER_MAGIC));
        }

        b = new byte[4];
        in.read(b);
        int version = Conversion.byteArrayToInt(b, 0, 0, 0, 4);
        if (version <= 0 || version > HEADER_VERSION)
        {
            throw new IllegalStateException(
                String.format(
                    "Invalid version number %d, expected %d",
                    version,
                    HEADER_VERSION));
        }

        return version;
    }


    private static Media readMedia(
        PostProto.Media m)
    {
        if (m == null)
            return null;
        if (m.getType().equals(NULL))
            return null;

        MediaType mt =
            MediaType.valueOf(m.getType());
        switch (mt)
        {
        case PHOTO:
            PhotoMedia pm = new PhotoMedia();
            pm.setLarge(m.getLarge());
            pm.setMedium(m.getMedium());
            pm.setThumbnail(m.getThumbnail());
            pm.setFormat(PhotoMedia.Format.valueOf(m.getPhotoFormat()));
            if (m.hasLargeHW())
            {
                pm.setLargeHeight(Integer.valueOf(m.getLargeHW()
                    .split("x", 2)[0]));
                pm.setLargeWidth(Integer.valueOf(m.getLargeHW()
                    .split("x", 2)[1]));
            }
            if (m.hasMediumHW())
            {
                pm.setMediumHeight(Integer.valueOf(m.getMediumHW()
                    .split("x", 2)[0]));
                pm.setMediumWidth(Integer.valueOf(m.getMediumHW()
                    .split("x", 2)[1]));
            }
            if (m.hasThumbnailHW())
            {
                pm.setThumbnailHeight(Integer.valueOf(m.getThumbnailHW()
                    .split("x", 2)[0]));
                pm.setThumbnailWidth(Integer.valueOf(m.getThumbnailHW()
                    .split("x", 2)[1]));
            }
            return pm;

        case TEXT:
            TextMedia tm = new TextMedia();
            tm.setText(m.getText());
            tm.setFormat(TextMedia.Format.valueOf(m.getTextFormat()));
            return tm;

//        case AVATAR:
//            AvatarMedia am = new AvatarMedia();
//            am.setAvatar(m.getAvatar().substring(1));
//            am.setAvatarThumbnail(m.getAvatarThumbnail().substring(1));
//            am.setSystem(m.getAvatar().startsWith(
//                "1"));
//            return am;
        }
        return null;
    }


    private static void writeHeader(
        OutputStream out)

        throws IOException
    {
        out.write(Conversion.intToByteArray(
            HEADER_MAGIC,
            0,
            new byte[4],
            0,
            4));
        out.write(Conversion.intToByteArray(
            HEADER_VERSION,
            0,
            new byte[4],
            0,
            4));
    }


    private static void writeMedia(
        PostProto.Builder b,
        Media m)
    {
        // Add placeholder
        if (m == null)
            b.addMedia(PostProto.Media.newBuilder()
                .setType(NULL)
                .build());

        if (m instanceof PhotoMedia)
        {
            // Photo, note for dimension (width,height) 0 means NULL
            PhotoMedia pm = (PhotoMedia) m;
            Builder mb = PostProto.Media.newBuilder()
                .setType(pm.getType().name())
                .setLarge(pm.getLarge())
                .setMedium(pm.getMedium())
                .setThumbnail(pm.getThumbnail())
                .setPhotoFormat(pm.getFormat().name());

            if (pm.getLargeHeight() != 0
                && pm.getLargeWidth() != 0)
                mb.setLargeHW(pm.getLargeHeight() + "x"
                    + pm.getLargeWidth());

            if (pm.getMediumHeight() != 0
                && pm.getMediumWidth() != 0)
                mb.setMediumHW(pm.getMediumHeight()
                    + "x" + pm.getMediumWidth());

            if (pm.getThumbnailHeight() != 0
                && pm.getThumbnailWidth() != 0)
                mb.setThumbnailHW(pm.getThumbnailHeight()
                    + "x" + pm.getThumbnailWidth());
            b.addMedia(mb.build());
        }
        else if (m instanceof TextMedia)
        {
            // Text
            TextMedia tm = (TextMedia) m;
            b.addMedia(PostProto.Media.newBuilder()
                .setType(tm.getType().name())
                .setText(tm.getText())
                .setTextFormat(tm.getFormat().name())
                .build());
        }
//        else if (m instanceof AvatarMedia)
//        {
//            // Avatar
//            AvatarMedia am = (AvatarMedia) m;
//            b.addMedia(PostProto.Media.newBuilder()
//                .setType(am.getType().name())
//                .setAvatar((am.isSystem() ? "1" : "0")
//                    + am.getAvatar())
//                .setAvatarThumbnail((am.isSystem() ? "1" : "0")
//                    + am.getAvatarThumbnail())
//                .build());
//        }
        // Other media type goes here
    }



    //
    // INTERNAL CLASSES
    //

    static class V1Iterator
        extends
            AbstractIterator<Post>
    {
        PostProto.Builder b;

        InputStream in;


        public V1Iterator(
            InputStream in)
        {
            this.in = in;
            this.b = PostProto.newBuilder();
        }


        @Override
        protected Post computeNext()
        {
            boolean success;
            try
            {
                success = b.mergeDelimitedFrom(in);

                if (!success)
                {
                    // EOF or some other reason
                    return endOfData();
                }

                // Deserialize using Google protobuf
                PostProto pp = b.build();
                Post p = new Post();
                p.setType(PostType.valueOf(pp.getType()));
                p.setBody(pp.getBody());
                p.setCreateTime(new Date(pp.getCreateTime()));
                p.setId(pp.getId());
                p.setUpdateTime(new Date(pp.getUpdateTime()));

                switch (p.getType())
                {
                case PHOTO:
                    // 0 - origin
                    // 1 - sting
                    int c = pp.getMediaCount();
                    if (c >= 2)
                    {
                        //TODO
                       // p.setPhoto((PhotoMedia)readMedia(pp.getMedia(0)));
                    }
                    break;

                default:
                    throw new IOException(
                        "Unrecognized post type \"" + p.getType()
                            + "\"");
                }

                // Clean inner state of builder before next mergeFrom
                b.clear();
                return p;
            }
            catch (IOException e)
            {
                throw new RuntimeException(
                    "Error occurs when importing the data: "
                        + e.getMessage(),
                    e);
            }
        }

    }


    //
    // CONSTANTS
    //

    private static final String NULL = "NULL";

    public static final int HEADER_MAGIC = 0x19890828;

    public static final int HEADER_VERSION = 1;
}
