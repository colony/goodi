
package gd.impl.post;


import gd.impl.util.Sortable;

import java.util.Date;


public class Flag
    implements
        Sortable
{
    private long id;

    private String complaintMessage;

    private Date createTime;

    private long postId;

    /**
     * sortKey and sortVal are not persisted. Both fields are
     * normally null unless the post is part of collection (i.e.
     * feed). The fields indicate how the post is sorted.
     */
    private String sortKey;

    private String sortVal;

    private long userId;


    public Flag()
    {

    }


    public Flag(
        long id,
        long postId,
        long userId,
        String complainMessage,
        Date createTime)
    {
        this.id = id;
        this.postId = postId;
        this.userId = userId;
        this.complaintMessage = complainMessage;
        this.createTime = createTime;
    }


    public Flag(
        Flag fp)
    {
        this.id = fp.id;
        this.postId = fp.postId;
        this.userId = fp.userId;
        this.complaintMessage = fp.complaintMessage;
        this.createTime = fp.createTime;
    }


    public long getId()
    {
        return id;
    }


    public void setId(
        long id)
    {
        this.id = id;
    }


    public long getPostId()
    {
        return postId;
    }


    public void setPostId(
        long postId)
    {
        this.postId = postId;
    }


    public String getSortKey()
    {
        return sortKey;
    }


    public void setSortKey(
        String sk)
    {
        this.sortKey = sk;
    }


    public String getSortVal()
    {
        return sortVal;
    }


    public void setSortVal(
        String sv)
    {
        this.sortVal = sv;
    }


    public long getUserId()
    {
        return userId;
    }


    public void setUserId(
        long userId)
    {
        this.userId = userId;
    }


    public String getComplainMessage()
    {
        return complaintMessage;
    }


    public void setComplainMessage(
        String complainMessage)
    {
        this.complaintMessage = complainMessage;
    }


    public Date getCreateTime()
    {
        return createTime;
    }


    public void setCreateTime(
        Date createTime)
    {
        this.createTime = createTime;
    }

}
