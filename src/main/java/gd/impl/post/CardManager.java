
package gd.impl.post;


import gd.impl.config.Configs;
import gd.impl.post.CardsConfig.CardConfig;
import gd.impl.util.ResultSetParser;
import gd.impl.util.SqliteUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;


public class CardManager
{
    private CardsConfig config;


    private CardManager()
    {
        this.config = Configs.get(CardsConfig.class);
        String createCardTableSql =
            "CREATE TABLE IF NOT EXISTS card" +
                "(deck_id TEXT," +
                "seq INTEGER," +
                "text TEXT," +
                "type TEXT" +  // REQ, REP
                ")";
        SqliteUtils.executeQuery(createCardTableSql, null, "createCardTable");
        String createIndexSql =
            "CREATE UNIQUE INDEX IF NOT EXISTS " +
                "card_deckId_type_seq " +
                "ON card " +
                "(deck_id, type, seq)";
        SqliteUtils.executeQuery(createIndexSql, null, "createCardTableIndex");
    }


    /**
     * 
     * @param deckId
     * @param type
     *          String, enum, "REQ" or "REP"
     * @param count
     * @return
     */
    public List<String> drawCards(
        String deckId,
        String type,
        int count)
    {
        CardConfig cc = this.config.getCardConfig(deckId);
        if (cc == null)
            throw new IllegalArgumentException("Invalid deck ID \"" + deckId + "\"");
        
        int nr = type.equals("REQ") ? cc.getNumRequests() : cc.getNumResponses();
        final List<String> result = new ArrayList<>(Math.min(count, nr));
        if (count >= nr) {
            SqliteUtils.executeSelectQuery(
                "SELECT text FROM card WHERE deck_id = ? AND type = ?", 
                new Object[]{deckId, type}, 
                "selectCard", 
                new ResultSetParser() {
                    @Override
                    public void parse(
                        ResultSet rs)
                        throws SQLException
                    {
                        while (rs.next()) {
                            result.add(rs.getString(1));
                        }
                    }
            });
            Collections.shuffle(result);
        } else {
            Set<Integer> seqs = new HashSet<>();
            while (seqs.size() < count) {
                seqs.add(RandomUtils.nextInt(1, nr));
            }
            SqliteUtils.executeSelectQuery(
                "SELECT text FROM card WHERE deck_id = ? AND type = ? AND seq IN (" + StringUtils.join(seqs, ",") + ")", 
                new Object[]{deckId, type}, 
                "selectCard", 
                new ResultSetParser() {
                    @Override
                    public void parse(
                        ResultSet rs)
                        throws SQLException
                    {
                        while (rs.next()) {
                            result.add(rs.getString(1));
                        }
                    }
            });
        }
        return result;
    }
    

    public String nextDeckId()
    {
        List<String> deckIds = this.config.getAllDeckIds();
        int i = RandomUtils.nextInt(0, deckIds.size());
        return deckIds.get(i);
    }



    //
    // SINGLETON MANAGEMENT
    //

    private static class SingletonHolder
    {
        private static final CardManager INSTANCE = new CardManager();
    }


    public static CardManager instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
