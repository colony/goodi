
package gd.impl.post;


import gd.api.v1.post.MediaType;
import gd.api.v1.post.PostType;
import gd.impl.ComponentManager;
import gd.impl.media.FileManager;
import gd.impl.media.LinkMedia;
import gd.impl.media.LinkMedia.Format;
import gd.impl.queue.Event;
import gd.impl.queue.EventConsumer;
import gd.impl.queue.EventType;
import gd.impl.repository.RepositoryManager;
import gd.impl.repository.RepositoryType;
import gd.impl.util.UpdateConflictException;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LinkPostUploader
    implements
        EventConsumer
{
    private static final Logger LOG = LoggerFactory.getLogger(LinkPostUploader.class);

    private FileManager fileMgr;

    private HttpClient httpClient;

    private PostRepository postRepo;


    public LinkPostUploader()
    {
        this.fileMgr = FileManager.instance();
        this.httpClient = ComponentManager.instance().http().getClient();
        this.postRepo = RepositoryManager.instance().get(RepositoryType.POST, PostRepository.class);
    }


    @Override
    public boolean accept(
        EventType type)
    {
        return type == EventType.CREATE_POST;
    }


    @Override
    public void consume(
        Event event)
    {
        PostType type = PostType.valueOf(event.str1);
        if (type != PostType.LINK)
            return;

        Post p = Posts.get(event.lng2);
        if (p == null || p.getType() != PostType.LINK)
            return;

        LinkMedia lm = p.getMedia(0, LinkMedia.class);
        LinkMedia.Format fmt = lm.getEmbedItemFormat();
        if (fmt != Format.GIF && fmt != Format.IMAGE)
            return;

        //TODO: determine it is external?
        String externalMediaItemUrl = lm.getEmbedItemUrl();
        try {
            HttpGet httpget = new HttpGet(externalMediaItemUrl);
            HttpResponse response = this.httpClient.execute(httpget);

            int sc = response.getStatusLine().getStatusCode();
            if (sc != 200) {
                LOG.info(
                    "Fail to fetch post ID={}, link media url {}, status code: {}",
                    event.lng2,
                    externalMediaItemUrl,
                    sc);
                return;
            }

            HttpEntity entity = response.getEntity();
            InputStream contentStream = entity.getContent();

            String itemKey = this.fileMgr.store(
                contentStream,
                MediaType.LINK,
                fmt == Format.IMAGE ? "image/jpeg" : "image/gif",
                -1,
                null,
                null);
            LOG.debug("Transfer link media from {} to {}", externalMediaItemUrl, itemKey);
            
            // Update the post
            lm.setEmbedItemUrl(this.fileMgr.getFullUrl(itemKey));
            this.postRepo.update(p);
        }
        catch (IOException e) {
            LOG.error("Fail to fetch post ID={}, link media url {}", event.lng2, externalMediaItemUrl, e);
        }
        catch (UpdateConflictException e) {
            LOG.error("Fail to update (conflict) post ID={}", event.lng2, e);
        }
    }

}
