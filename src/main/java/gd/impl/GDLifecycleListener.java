package gd.impl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class GDLifecycleListener
    implements
        ServletContextListener
{
    /**
     * Called when the servlet context is first initialized
     * 
     * @param sce
     *            A ServletContextEvent containing the new context
     */
    @Override
    public void contextInitialized(
        ServletContextEvent sce)
    {
        GDLifecycle.start();
    }


    /**
     * Called when the servlet context is about to be shut down
     * 
     * @param sce
     *            A ServletContextEvent containing the dying context
     */
    @Override
    public void contextDestroyed(
        ServletContextEvent sce)
    {
        GDLifecycle.stop();
    }
}
