package gd.test.common;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class TestCommon
{
    public void er(Object o)
    {
        System.err.println(o);
    }
    
    public void pr()
    {
        pr(PRINT_SEP);
    }
    
    public void pr(Object o)
    {
        if (o instanceof Object[]) {
            pr ((Object[]) o);
        } else if (o instanceof Map) {
            pr ((Map<?,?>) o);
        } else {
            System.out.println(o);
        }
    }
    
    public void pr(Object[] ary)
    {
        if (ary != null)
        {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(StringUtils.join(ary, ","));
            sb.append("]");
            sb.append(" length=");
            sb.append(ary.length);
            pr(sb);
        }
    }
    
    public void pr(Object[] ary, String delim)
    {
        if (ary != null)
        {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(StringUtils.join(ary, delim));
            sb.append("]");
            sb.append(" length=");
            sb.append(ary.length);
            pr(sb);
        }
    }
    
    public void pr(Map<?,?> map)
    {
       for (Map.Entry<?,?> e : map.entrySet())
       {
           pr(String.format("%s=%s", e.getKey(), e.getValue()));
       }
    }
    
    public void pr(String name, Object o)
    {
        pr(PRINT_SEP + name);
        pr(o);
        pr(PRINT_SEP);
    }
    
    public void pr(String name, Object[] ary)
    {
        pr(PRINT_SEP + name);
        pr(ary);
        pr(PRINT_SEP);
    }
    
    public void t()
    {
        System.out.println(System.currentTimeMillis());
    }
    
    public static final String PRINT_SEP = "----------------";    
}
